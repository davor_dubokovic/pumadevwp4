/**
 * Set account Id and Product type. Hide columns based on epoch.
 */
function setAccountId(parentData) {
	accountId = parentData.accountId;
	orderData = parentData.orderData;
	basketId = parentData.basketId;
	deliveryType = parentData.orderData.deliveryType;
	if (parentData.supplyLocation !== '') {
		locationId = parentData.supplyLocation;
	} else {
		locationId = parentData.pickupLocation;
	}
	var orderTypeChanged = false;
	if ((productType !== parentData.productType) || (orderType !== parentData.orderType)) {
		productType = parentData.productType;
		orderType = parentData.orderType;
		orderTypeChanged = true;
		SPM.remoteAction({AccountId: accountId, ProductType: productType, OrderType: orderType, LocationId: locationId}, 'TrafiguraGetProductOptions', 'initialLoad');
	}
	if (parentData.epoch !== epoch || orderTypeChanged) {
		epoch = parentData.epoch;
		if (parentData.epoch === 'Current') {
			var columns = {};
			columns["Ambient Loaded Qty"] = false;
			columns["Ambient Del Qty"] = false;
			columns["Std Loaded Qty"] = false;
			columns["Std Del Qty"] = false;
			displayColumns(columns); 
    		jQuery('#headRow th:nth-child(3)').css('width','11%');
    		jQuery('#headRow th:nth-child(4)').css('width','5%');
    		jQuery('#headRow th:nth-child(5)').css('width','8%');
    		jQuery('#headRow th:nth-child(8)').css('width','5%');
    		jQuery('#headRow th:nth-child(9)').css('width','5%');
			
		} else {
			var columns = {};
			columns["Ambient Loaded Qty"] = true;
			columns["Ambient Del Qty"] = true;
			columns["Std Loaded Qty"] = true;
			columns["Std Del Qty"] = true;
			displayColumns(columns);
			jQuery('#headRow th:nth-child(3)').css('width','11%');
			jQuery('#headRow th:nth-child(4)').css('width','5%');
			jQuery('#headRow th:nth-child(5)').css('width','8%');
			jQuery('#headRow th:nth-child(6)').css('width','8%');
			jQuery('#headRow th:nth-child(7)').css('width','8%');
			jQuery('#headRow th:nth-child(15)').css('width','5%');
		}
	}
	if(orderType != 'Sales') {
	    jQuery('#pricingBtn').addClass('slds-hide');
	} 
	else {
	    jQuery('#pricingBtn').removeClass('slds-hide');
	}
    if(Pagination.size==1) {
        jQuery('#pagination').css('display','none');
    } 
    else {
        jQuery('#pagination').css('display','block');
    }
	
	userLanguage = parentData.userLanguage;
	translateMLE(userLanguage);
}

/**
 * Query product data. 
 */
function initialLoad(result) {
	
	mleProducts = result;
	mleProducts.productMap = {};
	for (var i = 0; i < mleProducts.stockLocation.length; i++) {
		if (locationId === mleProducts.stockLocation[i].Id) {
			locationId = mleProducts.stockLocation[i].Stock_Holding_Location__c;
		}
	}
	absoluteMaximum = 0;
	for (var i = 0; i < result.products.length; i++) {
		mleProducts.productMap[result.products[i].Name] = result.products[i];
		for (var j = 0 ; j < mleProducts.inventories.length; j++) {
			if (mleProducts.inventories[j].EP_Storage_Location__c === locationId && mleProducts.inventories[j].EP_Product__c === result.products[i].Product2.Id) {
				mleProducts.productMap[result.products[i].Name].Inventory = mleProducts.inventories[j].EP_Inventory_Availability__c;
			}
		}
		if(mleProducts.quantities) {
    		for (var j = 0; j < mleProducts.quantities.length; j++) {
    			if (mleProducts.quantities[j].EP_Product__c === result.products[i].Product2.Id && deliveryType === mleProducts.quantities[j].EP_Delivery_Type__c) {
    				mleProducts.productMap[result.products[i].Name].Max2 = mleProducts.quantities[j].EP_Total_Order_Max_Quantity__c;
    				if (mleProducts.quantities[j].EP_Total_Order_Max_Quantity__c > absoluteMaximum) {
    					//absoluteMaximum =  mleProducts.quantities[j].EP_Total_Order_Max_Quantity__c;
    				}
    			}
    		}
		}
	}
	var productNames = [];
	if (Object.keys(configurationMap).length > 1) {
		var firstName = configurationMap[Object.keys(configurationMap)[0]].Product;
		var prod = {};
		for (var i = 0; i < result.products.length; i++) {
			if (result.products[i].Name === firstName) {
				prod = result.products[i];
			}
		}
		if (!prod.Name) {
			prod = result.products[0];
		}
		for (var i = 0; i < result.products.length; i++) {
			if (prod.Product2.EP_Dirty_Product__c === result.products[i].Product2.EP_Dirty_Product__c) {
				productNames.push(result.products[i].Name);
			}
		}
	} else {
		for (var i = 0; i < result.products.length; i++) {
			productNames.push(result.products[i].Name);
		}
	}
	
	updateOptions('Product', productNames);
	setCalculations(null);
	getDefinedTolerance();
	
}

function setReadOnly(input) {
	var readOnlyColumns = ["Price", "List Price", "VAT", "Inventory", "Max", "Min", "Status", "Unit of Measure"];
	for (var i = 0; i < readOnlyColumns.length; i++) {
		var inputCells = jQuery('[data-id="' + readOnlyColumns[i] + '"]');
		if (inputCells.length > 0) {
			for (var j = 0; j < inputCells.length; j++) {
				jQuery(inputCells[j]).attr('readOnly', true);
			}
		}
	}
	for (var i = 0; i < jQuery('[data-id="Product"]').length; i++ ) {
		jQuery(jQuery('[data-id="Product"]')[i]).select2({width:'100%', matcher: matchCustom});
	}
	validateQuantity(null);
	if (isMobile.any()) {
		renderMobile();
	}
}

function findMaxQuantity() {
	absoluteMaximum = 0;
	var configurationGuids = Object.keys(configurationMap);
	var errorMessage = '';
	for (var i = 0; i < configurationGuids.length; i++) {
		var item = configurationMap[configurationGuids[i]];
		if (mleProducts.productMap && configurationMap[configurationGuids[i]]["Product"] !== 'Select a product' &&  mleProducts.productMap[configurationMap[configurationGuids[i]]["Product"]]) {
			var max = mleProducts.productMap[configurationMap[configurationGuids[i]]["Product"]]["Max2"];
			if (max && max !== '') {
				max = parseFloat(max);
				if (max > absoluteMaximum) {
					absoluteMaximum = max;
				}
			}
		}
	}
	for (var i = 0; i < mleProducts.products.length; i++) {
		mleProducts.productMap[mleProducts.products[i].Name].Max = absoluteMaximum;
	}
}

function changeMLEdisplay(pageNumber) {
	jQuery("#tableBody").empty();
	var index = pageNumber - 1;
	var configurationGuids = Object.keys(configurationMap);
	for (var i = index * pageSize; i < (index + 1) * (pageSize); i++) {
		if (configurationGuids[i]) {
			initRow(configurationMap[configurationGuids[i]]);
		}
	}
	setReadOnly(null);
	translateMLE(userLanguage);
}

function reloadOptions() {
	if (Object.keys(configurationMap).length < 3) {
		SPM.remoteAction({AccountId: accountId, ProductType: productType, OrderType: orderType, LocationId: locationId}, 'TrafiguraGetProductOptions', 'initialLoad');
	}
}

function getRandomInt() {
	var min = -2147483647;
	var max = 2147483647;
	//return Math.floor(Math.random() * (max - min + 1)) + min;
	return Math.floor(Math.random() * max) + 1;
}

function saveWithoutPricing() {
	blockUI();
	setMultipleConfiguration();
	var savePromise = getRemoteActionPromiseParams(SAVE_SINGLE, JSON.stringify(schemaObject), configurationId, definitionName);
	savePromise.then(
		function (response) {
			if (response!="500") {
				console.log("Saved.");
				configuration = schemaObject;
				SPM.executeOnParent({data:configurationMap, method:'saveDraftOrder'});
				unblockUI();
				
			}
		},
		function (err) {
			SPM.executeOnParent({data:configurationMap, method:'saveDraftOrder', error: err});
			console.log('***** Save error = ' + err);
			unblockUI();
		}
	);
}

function saveAndGetPricing() {
	blockUI();
	setMultipleConfiguration();
	var savePromise = getRemoteActionPromiseParams(SAVE_SINGLE, JSON.stringify(schemaObject), configurationId, definitionName);
	savePromise.then(
		function (response) {
			if (response!="500") {
				console.log("Saved.");
				configuration = schemaObject;
				if (orderType === 'Sales') {
					orderData.lineItems = [];
					var orderQuantity = 0;
					if (Object.keys(configurationMap).length > 0) {
						for (var i = 0; i < Object.keys(configurationMap).length; i++) {
							var lineItem = {};
							var configRow = configurationMap[Object.keys(configurationMap)[i]];
							lineItem.quantity = configRow["Qty"];
							orderQuantity += parseFloat(lineItem.quantity);
							lineItem.productCode = configRow["Product Code"];
							lineItem.companyCode = configRow["Company Code"];
							lineItem.lineItemId = getRandomInt();
							orderData.lineItems.push(lineItem);
						}
						orderData.quantity = orderQuantity;
						SPM.remoteAction({AccId: accountId, BasketId: basketId, OrderJSON: JSON.stringify(orderData)}, 'TrafiguraGetPrices', 'setPriceResponseAuto');
					} else {
						SPM.executeOnParent({data:configurationMap, method:'mleValidation', error: 'Please configure products before submit!'});
						unblockUI();
					}
				} else {
					SPM.executeOnParent({data:configurationMap, method:'mleValidation'});
					unblockUI();
				}
			}
		},
		function (err) {
			SPM.executeOnParent({data:configurationMap, method:'mleValidation', error: err});
			console.log('***** Save error = ' + err);
			unblockUI();
		}
	);
}

function buttonClick() {
	if (orderType === 'Sales') {
		jQuery(SPINNER).removeClass('slds-hide');
		orderData.lineItems = [];
		var orderQuantity = 0;
		if (Object.keys(configurationMap).length > 0) {
			for (var i = 0; i < Object.keys(configurationMap).length; i++) {
				var lineItem = {};
				var configRow = configurationMap[Object.keys(configurationMap)[i]];
				lineItem.quantity = configRow["Qty"];
				orderQuantity += parseFloat(lineItem.quantity);
				lineItem.productCode = configRow["Product Code"];
				lineItem.companyCode = configRow["Company Code"];
				lineItem.lineItemId = getRandomInt();
				orderData.lineItems.push(lineItem);
			}
			orderData.quantity = orderQuantity;
			SPM.remoteAction({AccId: accountId, BasketId: basketId, OrderJSON: JSON.stringify(orderData)}, 'TrafiguraGetPrices', 'setPriceResponse');
		} else {
			jQuery(SPINNER).addClass('slds-hide');
			iziToast.error({
				title: 'Error',
				message: 'Please add products before getting prices...',
				position: 'topCenter',
				progressBar: false
			});
		}
	}
}

function setScale(number, scale) {
	if (isNaN(number) || number === '') {
		number = 0;
	}
	if (isNaN(scale) || scale === '') {
		scale = 5;
	}
	number = parseFloat(number);
	return number.toFixed(scale);
}

function setPriceResponseAuto(result) {
	priceResponse = result;
	try {
		if (priceResponse && priceResponse.response) {
			priceResponse.response = priceResponse.response.replace(/[\n]/g, '');
			priceResponse.response = priceResponse.response.replace(/[\r]/g, '');
			priceResponse.prices = JSON.parse(priceResponse.response.replace(/&quot;/g, '"'));
			priceResponse.lineItemPrices = [];
			if (priceResponse.prices.Payload && priceResponse.prices.Payload.any0 && priceResponse.prices.Payload.any0.pricingResponse && 
				priceResponse.prices.Payload.any0.pricingResponse.priceDetails && priceResponse.prices.Payload.any0.pricingResponse.priceDetails.priceLineItems &&
				priceResponse.prices.Payload.any0.pricingResponse.priceDetails.priceLineItems.lineItem) {
				var lineItems = priceResponse.prices.Payload.any0.pricingResponse.priceDetails.priceLineItems.lineItem;
				for (var i = 0; i < lineItems.length; i++) {
					var lineItemPrice = {};
					
					lineItemPrice.errorCode = lineItems[i].error.ErrorCode;
					lineItemPrice.errorDescription = lineItems[i].error.ErrorDescription;
					if (lineItems[i].lineItemInfo) {
						lineItemPrice.unitPrice = lineItems[i].lineItemInfo.unitPrice;
						lineItemPrice.itemId =  lineItems[i].lineItemInfo.itemId;
						lineItemPrice.quantity = lineItems[i].lineItemInfo.quantity;
						if (lineItems[i].lineItemInfo.invoiceDetails && lineItems[i].lineItemInfo.invoiceDetails.invoiceComponent && lineItems[i].lineItemInfo.invoiceDetails.invoiceComponent.length > 0) {
							lineItemPrice.vat = lineItems[i].lineItemInfo.invoiceDetails.invoiceComponent[0].taxAmount;
						}
					}
					priceResponse.lineItemPrices.push(lineItemPrice);
				}
				setPricesAuto();
			} else {
				var savePromise = getRemoteActionPromiseParams(SAVE_SINGLE, JSON.stringify(schemaObject), configurationId, definitionName);
				savePromise.then(function() {
					var returnValue = {};
					returnValue.configurationMap = configurationMap;
					returnValue.error = 'Pricing not returned!';
					SPM.executeOnParent({data:returnValue, method:'mleValidation', error: 'Pricing not returned!'});
				});
				unblockUI();
			}	
		} else { 
			priceResponse = {};
			var savePromise = getRemoteActionPromiseParams(SAVE_SINGLE, JSON.stringify(schemaObject), configurationId, definitionName);
			savePromise.then(function() {
				var returnValue = {};
				returnValue.configurationMap = configurationMap;
				returnValue.error = 'Pricing not returned!';
				SPM.executeOnParent({data:returnValue, method:'mleValidation', error: 'Pricing not returned!'});
			});
			unblockUI();
		}
	} catch(error) {
		priceResponse = {};
		var savePromise = getRemoteActionPromiseParams(SAVE_SINGLE, JSON.stringify(schemaObject), configurationId, definitionName);
		savePromise.then(function() {
			var returnValue = {};
			returnValue.configurationMap = configurationMap;
			returnValue.error = 'Pricing not returned!';
			SPM.executeOnParent({data:returnValue, method:'mleValidation', error: 'Pricing not returned!'});
		});
		unblockUI();
	}
	
}

function setPricesAuto() {
	var configurationGuids = Object.keys(configurationMap);
	var errorMessage = '';
	pricesChanged = false;
	for (var i = 0; i < configurationGuids.length; i++) {
		var item = configurationMap[configurationGuids[i]];
		for (var j = 0; j < priceResponse.lineItemPrices.length; j++) {
			priceResponse.lineItemPrices[j].quantity = priceResponse.lineItemPrices[j].quantity.replace(/,/g , "");
			if (priceResponse.lineItemPrices[j].itemId === item["Product Code"] && parseFloat(priceResponse.lineItemPrices[j].quantity) == parseFloat(item["Quantity"])) {
				if (priceResponse.lineItemPrices[j].errorDescription !== "") {
					errorMessage += priceResponse.lineItemPrices[j].itemId + ' ' + priceResponse.lineItemPrices[j].errorDescription + '; ';
				}
				var currentPrice = item["List Price"];
				item["List Price"] = setScale(priceResponse.lineItemPrices[j].unitPrice, 5);
				if(item["List Price"] != currentPrice) {
				    pricesChanged = true;
				}
				item["VAT"] = setScale(priceResponse.lineItemPrices[j].vat, 5);
				jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="List Price"]').val(setScale(priceResponse.lineItemPrices[j].unitPrice, 5));
				jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="VAT"]').val(setScale(priceResponse.lineItemPrices[j].vat, 5));
			}
		}
	}
	calculatePrice();
	unblockUI();
    if(pricesChanged) {
	   if (confirm('Prices have changed or you did not click on "Get Price" before submitting. Are you sure you want to continue?')) {
        	if (errorMessage !== '') {
        	   blockUI();
               var savePromise = getRemoteActionPromiseParams(SAVE_SINGLE, JSON.stringify(schemaObject), configurationId, definitionName);
               savePromise.then(function() {
                    var returnValue = {};
                	returnValue.configurationMap = configurationMap;
                	returnValue.error = errorMessage;
                	SPM.executeOnParent({data:returnValue, method:'mleValidation', error: errorMessage});
                });
                unblockUI();
        	} else {
        		var savePromise = getRemoteActionPromiseParams(SAVE_SINGLE, JSON.stringify(schemaObject), configurationId, definitionName);
        		savePromise.then(function() {
        			SPM.executeOnParent({data:configurationMap, method:'mleValidation'});
        		});
        		unblockUI();
        	}
       } else {
         console.log('cancelled');
	   }
	}
	else {
	        if (errorMessage !== '') {
        	   blockUI();
               var savePromise = getRemoteActionPromiseParams(SAVE_SINGLE, JSON.stringify(schemaObject), configurationId, definitionName);
               savePromise.then(function() {
                    var returnValue = {};
                	returnValue.configurationMap = configurationMap;
                	returnValue.error = errorMessage;
                	SPM.executeOnParent({data:returnValue, method:'mleValidation', error: errorMessage});
                });
                unblockUI();
        	} else {
        		var savePromise = getRemoteActionPromiseParams(SAVE_SINGLE, JSON.stringify(schemaObject), configurationId, definitionName);
        		savePromise.then(function() {
        			SPM.executeOnParent({data:configurationMap, method:'mleValidation'});
        		});
        		unblockUI();
        	}
	}

}

function displayInfo(msg) {
	iziToast.info({
		title: 'Info',
		message: msg,
		position: 'topCenter',
		progressBar: false
	});
}

function setPriceResponse(result) {

	priceResponse = result;
	try {
		if (priceResponse && priceResponse.response) {
			priceResponse.response = priceResponse.response.replace(/[\n]/g, '');
			priceResponse.response = priceResponse.response.replace(/[\r]/g, '');
			priceResponse.prices = JSON.parse(priceResponse.response.replace(/&quot;/g, '"'));
			priceResponse.lineItemPrices = [];
			if (priceResponse.prices.Payload && priceResponse.prices.Payload.any0 && priceResponse.prices.Payload.any0.pricingResponse && 
				priceResponse.prices.Payload.any0.pricingResponse.priceDetails && priceResponse.prices.Payload.any0.pricingResponse.priceDetails.priceLineItems &&
				priceResponse.prices.Payload.any0.pricingResponse.priceDetails.priceLineItems.lineItem) {
				var lineItems = priceResponse.prices.Payload.any0.pricingResponse.priceDetails.priceLineItems.lineItem;
				for (var i = 0; i < lineItems.length; i++) {
					var lineItemPrice = {};
					
					lineItemPrice.errorCode = lineItems[i].error.ErrorCode;
					lineItemPrice.errorDescription = lineItems[i].error.ErrorDescription;
					if (lineItems[i].lineItemInfo) {
						lineItemPrice.unitPrice = lineItems[i].lineItemInfo.unitPrice;
						lineItemPrice.itemId =  lineItems[i].lineItemInfo.itemId;
						lineItemPrice.quantity = lineItems[i].lineItemInfo.quantity;
						if (lineItems[i].lineItemInfo.invoiceDetails && lineItems[i].lineItemInfo.invoiceDetails.invoiceComponent && lineItems[i].lineItemInfo.invoiceDetails.invoiceComponent.length > 0) {
							lineItemPrice.vat = lineItems[i].lineItemInfo.invoiceDetails.invoiceComponent[0].taxAmount;
						}
					}
					priceResponse.lineItemPrices.push(lineItemPrice);
				}
				setPrices();
			} else {
				jQuery(SPINNER).addClass('slds-hide');
				iziToast.error({
					title: 'Error',
					message: 'Could not get prices.',
					position: 'topCenter',
					progressBar: false
				});
			} 
		} else {
			priceResponse = {};
			jQuery(SPINNER).addClass('slds-hide');
			iziToast.error({
				title: 'Error',
				message: 'Could not get prices',
				position: 'topCenter',
				progressBar: false
			});
		}
	} catch(error) {
		priceResponse = {};
		jQuery(SPINNER).addClass('slds-hide');
		iziToast.error({
			title: 'Error',
			message: 'Could not get prices. Error message: ' + priceResponse.status,
			position: 'topCenter',
			progressBar: false
		});
	}
}

function setPrices() {
	var configurationGuids = Object.keys(configurationMap);
	var errorMessage = '';
	for (var i = 0; i < configurationGuids.length; i++) {
		var item = configurationMap[configurationGuids[i]];
		for (var j = 0; j < priceResponse.lineItemPrices.length; j++) {
			priceResponse.lineItemPrices[j].quantity = priceResponse.lineItemPrices[j].quantity.replace(/,/g , "");
			if (priceResponse.lineItemPrices[j].itemId === item["Product Code"] && parseFloat(priceResponse.lineItemPrices[j].quantity) == parseFloat(item["Qty"])) {
				if (priceResponse.lineItemPrices[j].errorDescription !== "") {
					errorMessage = priceResponse.lineItemPrices[j].itemId + ' ' + priceResponse.lineItemPrices[j].errorDescription + '; ';
				}
				item["List Price"] = setScale(priceResponse.lineItemPrices[j].unitPrice, 5);
				item["VAT"] = setScale(priceResponse.lineItemPrices[j].vat, 5);
				jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="List Price"]').val(setScale(priceResponse.lineItemPrices[j].unitPrice, 5));
				jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="VAT"]').val(setScale(priceResponse.lineItemPrices[j].vat, 5));
			}
		}
	}
	calculatePrice();
	jQuery(SPINNER).addClass('slds-hide');
	if (errorMessage !== '') {
		iziToast.error({
			title: 'Error',
			message: errorMessage,
			position: 'topCenter',
			progressBar: false
		});
	} else {
		iziToast.success({
			title: 'Success',
			message: 'Prices set.',
			position: 'topCenter',
			progressBar: false
		});
	}
}

function validateQuantity(input) {
	var configurationGuids = Object.keys(configurationMap);
	var inputId = null;
	var inputAttrName = null;
	if(input) {
	    try {
	        inputId = jQuery(input).parent().parent().parent().find('button[data-el=delete]').first().data('guid');
	        inputAttrName = jQuery(input).data('id');
	        if(inputId == undefined) {
	            inputId = input;
	            isNewRow = true;
	        }
	    }
	    catch(e) {
	        console.log('failed to retrieve row id.');
	    }
	}
	for (var i = 0; i < configurationGuids.length; i++) {
		var item = configurationMap[configurationGuids[i]];
		if (mleProducts && mleProducts.productMap && mleProducts.productMap[item.Product]) {
			var productItem = mleProducts.productMap[item.Product];
			if (item["Product"] !== 'Select a product') {
				jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Product"]').parent().removeClass('cst-required1');
			} else {
				jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Product"]').parent().addClass('cst-required1');
				validationMessages += 'Please select products.|';
			}
			if(inputId &&  inputAttrName == 'Qty' && inputId == configurationGuids[i] && jQuery(input).val()>0 && item["Price"]>0) {
				displayInfo('Change in quantity affects the price. Please click "Get Price" before submitting.');
			}
			if (item["Qty"] > 0 && item["Product"] !== 'Select a product') {
				item["Status"] = 'Valid';
				jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Status"]').val('Valid');
				jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Qty"]').parent().removeClass('cst-required1');
			} else {
				item["Status"] = 'Incomplete';
				jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Status"]').val('Incomplete');
				jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Qty"]').parent().addClass('cst-required1');
				if(inputId &&  inputAttrName == 'Qty' && inputId == configurationGuids[i]) {
				    validationMessages += 'Quantity should be greater than 0 for ' + item["Product"] + '|';
				}
				
			}
			if (epoch === 'Retrospective') {
				if ((item["Std Loaded Qty"] > 0 && item["Std Del Qty"] > 0 && item["Ambient Del Qty"] > 0 && item["Ambient Loaded Qty"] > 0 && item["Qty"] > 0 && item["Product"] !== 'Select a product') || (
					productItem.Product2.EP_Product_Sold_As__c !== 'Bulk')) {
					item["Status"] = 'Valid'; 
					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Status"]').val('Valid');
					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Qty"]').parent().removeClass('cst-required1');
					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Ambient Del Qty"]').parent().removeClass('cst-required1');
					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Ambient Loaded Qty"]').parent().removeClass('cst-required1');
					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Std Del Qty"]').parent().removeClass('cst-required1');
					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Std Loaded Qty"]').parent().removeClass('cst-required1');
				} else {
					item["Status"] = 'Incomplete';
					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Status"]').val('Incomplete');
					if(item["Std Loaded Qty"] <= 0) {
					    jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Std Loaded Qty"]').parent().addClass('cst-required1');
					    if(inputId &&  inputAttrName == 'Std Loaded Qty' && inputId == configurationGuids[i]) {
					        validationMessages += 'Standard Loaded Qty should be greater than 0 for ' + item["Product"] + '|';
					    }
					}
					else {
					    jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Std Loaded Qty"]').parent().removeClass('cst-required1');
					}
					if(item["Std Del Qty"] <= 0) {
					    jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Std Del Qty"]').parent().addClass('cst-required1');
					    if(inputId &&  inputAttrName == 'Std Del Qty' && inputId == configurationGuids[i]) {
					        validationMessages += 'Standard Delivered Qty should be greater than 0 for ' + item["Product"] + '|';
					    }
					}
					else {
					    jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Std Del Qty"]').parent().removeClass('cst-required1');
					}
					if(item["Ambient Del Qty"] <= 0) {
					   jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Ambient Del Qty"]').parent().addClass('cst-required1');
					   if(inputId &&  inputAttrName == 'Ambient Del Qty' && inputId == configurationGuids[i]) {
					        validationMessages += 'Ambient Delivered Qty should be greater than 0 for ' + item["Product"] + '|';
					   }
					}
					else {
					    jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Ambient Del Qty"]').parent().removeClass('cst-required1');
					}
					if(item["Ambient Loaded Qty"] <= 0) {
					   jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Ambient Loaded Qty"]').parent().addClass('cst-required1');
					   if(inputId &&  inputAttrName == 'Ambient Loaded Qty' && inputId == configurationGuids[i]) {
					        validationMessages += 'Ambient Loaded Qty should be greater than 0 for ' + item["Product"] + '|';
					   }
					}
					else {
					    jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Ambient Loaded Qty"]').parent().removeClass('cst-required1');
					}

				}
				if (productItem.Product2.EP_Product_Sold_As__c !== 'Bulk' && item["Qty"] <= 0) {
					item["Status"] = 'Incomplete';
					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Status"]').val('Incomplete');
					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Qty"]').parent().addClass('cst-required1');
					if(inputId &&  inputAttrName == 'Qty' && inputId == configurationGuids[i]) {
					    validationMessages += 'Quantity attributes should be greater than 0 for ' + item["Product"] + '|';
					}
				}
				if(item["Ambient Delivered Qty"]>0 && item["Quantity"]>0 && item["Standard Loaded Qty"]>0 && item["Ambient Delivered Qty"]>0) {
    				if(toleranceAcquired) {
    				    var errorVal = false;
    				    if(definedTolerance.ord_vs_del__q_inc && item["Ambient Delivered Qty"] && item["Ambient Delivered Qty"]!=0) {
                            if ((item["Ambient Delivered Qty"] - item["Quantity"])/item["Ambient Delivered Qty"] *100 > definedTolerance.ord_vs_del__q_inc) {
            				    errorVal = true;
            					validationMessages += 'Ambient Delivered Qty out of tolerance limits for ' + item["Product"] + '|';
            				}
    				    }
    				    if(definedTolerance.ord_vs_del__q_dec && item["Ambient Delivered Qty"] && item["Ambient Delivered Qty"]!=0) {
                            if ((item["Ambient Delivered Qty"] - item["Quantity"])/item["Ambient Delivered Qty"] *100 < definedTolerance.ord_vs_del__q_dec) {
            				    errorVal= true;
            					validationMessages += 'Ambient Delivered Qty out of tolerance limits for ' + item["Product"] + '|';
            				}
    				    }
    				    if(definedTolerance.ord_vs_load__q_inc) {
                            if ((item["Ambient Loaded Qty"] - item["Standard Loaded Qty"])/item["Standard Loaded Qty"] *100 > definedTolerance.ord_vs_load__q_inc) {
            				    errorVal = true;
            					validationMessages += 'Ambient Loaded Qty out of tolerance limits for ' + item["Product"] + '|';
            				}
                            if ((item["Standard Loaded Qty"] - item["Quantity"])/item["Standard Loaded Qty"] *100 > definedTolerance.ord_vs_load__q_inc) {
            				    errorVal= true;
            					validationMessages += 'Standard Loaded Qty out of tolerance limits for ' + item["Product"] + '|';
            				}
    				    }
    				    if(definedTolerance.ord_vs_load__q_dec) {
                            if ((item["Ambient Loaded Qty"] - item["Standard Loaded Qty"])/item["Standard Loaded Qty"] *100 < definedTolerance.ord_vs_load__q_dec) {
            				    errorVal= true;
            					validationMessages += 'Ambient Loaded Qty out of tolerance limits for ' + item["Product"] + '|';
            				}
                            if ((item["Standard Loaded Qty"] - item["Quantity"])/item["Standard Loaded Qty"] *100 < definedTolerance.ord_vs_load__q_dec) {
            				    errorVal= true;
            					validationMessages += 'Standard Loaded Qty out of tolerance limits for ' + item["Product"] + '|';
            				}   				
    				    }
    				    if(errorVal) {
        					item["Status"] = 'Incomplete';
        					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Status"]').val('Incomplete');
        					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Quantity"]').parent().addClass('cst-required1');
        					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Ambient Delivered Qty"]').parent().addClass('cst-required1');
        					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Ambient Loaded Qty"]').parent().addClass('cst-required1');
        					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Standard Delivered Qty"]').parent().addClass('cst-required1');
        					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Standard Loaded Qty"]').parent().addClass('cst-required1');
    				    }
    				    else {
        					item["Status"] = 'Valid';
        					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Status"]').val('Valid');
        					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Quantity"]').parent().removeClass('cst-required1');
        					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Ambient Delivered Qty"]').parent().removeClass('cst-required1');
        					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Ambient Loaded Qty"]').parent().removeClass('cst-required1');
        					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Standard Delivered Qty"]').parent().removeClass('cst-required1');
        					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Standard Loaded Qty"]').parent().removeClass('cst-required1');
    				    }
    				}
				}
			}
		}
	}
	minMaxValidation(inputId,inputAttrName);
}

function calculatePriceOnDelete() {
	var totalPrice = 0;
	var totalVat = 0;
	var totalList = 0;
	for (var j = 0; j < Object.keys(configurationMap).length; j++) {
		var productPrice = configurationMap[Object.keys(configurationMap)[j]]["List Price"];
		var vat = configurationMap[Object.keys(configurationMap)[j]]["VAT"];
		var quantity = configurationMap[Object.keys(configurationMap)[j]]["Qty"];
		if (!isNaN(productPrice) && productPrice !== undefined && productPrice !== '' && !isNaN(quantity) && quantity !== undefined && quantity !== '') {
			totalList += parseFloat(productPrice) * parseFloat(quantity);
			productPrice = (parseFloat(productPrice) + parseFloat(vat)) * parseFloat(quantity);
			configurationMap[Object.keys(configurationMap)[j]].Price = setScale(productPrice, 5);
			totalVat += parseFloat(vat) * parseFloat(quantity);
		} else {
			productPrice = 0;
			configurationMap[Object.keys(configurationMap)[j]].Price = productPrice;
		}
		totalPrice += productPrice;
	}
	var totalData = {};
	if (priceResponse && priceResponse.prices && priceResponse.prices.Payload && priceResponse.prices.Payload.any0 && priceResponse.prices.Payload.any0.pricingResponse 
		&& priceResponse.prices.Payload.any0.pricingResponse.priceDetails && priceResponse.prices.Payload.any0.pricingResponse.priceDetails.priceDetails 
		&& priceResponse.prices.Payload.any0.pricingResponse.priceDetails.priceDetails.currencyCode) {
		totalData.currency = priceResponse.prices.Payload.any0.pricingResponse.priceDetails.priceDetails.currencyCode
	}
	totalData.total = setScale(totalPrice, 2);
	totalData.list = setScale(totalList, 2);
	totalData.vat = setScale(totalVat, 2);
	SPM.executeOnParent({data:totalData, method:'setTotalData'});

}

function calculatePrice() {
	setMLERows() ;
	var totalPrice = 0;
	var totalVat = 0;
	var totalList = 0;
	for (var j = 0; j < Object.keys(configurationMap).length; j++) {
		var productPrice = configurationMap[Object.keys(configurationMap)[j]]["List Price"];
		var vat = configurationMap[Object.keys(configurationMap)[j]]["VAT"];
		var quantity = configurationMap[Object.keys(configurationMap)[j]]["Qty"];
		if (!isNaN(productPrice) && productPrice !== undefined && productPrice !== '' && !isNaN(quantity) && quantity !== undefined && quantity !== '') {
			totalList += parseFloat(productPrice) * parseFloat(quantity);
			productPrice = (parseFloat(productPrice) + parseFloat(vat)) * parseFloat(quantity);
			configurationMap[Object.keys(configurationMap)[j]].Price = setScale(productPrice, 5);
			jQuery('[data-guid="' + Object.keys(configurationMap)[j] + '"]').parent().parent().find('[data-id="Price"]').val(setScale(productPrice, 5));
			totalVat += parseFloat(vat) * parseFloat(quantity);
		} else {
			productPrice = 0;
			configurationMap[Object.keys(configurationMap)[j]].Price = productPrice;
			jQuery('[data-guid="' + Object.keys(configurationMap)[j] + '"]').parent().parent().find('[data-id="Price"]').val(productPrice);
		}
		totalPrice += productPrice;
	}
	//changeMLEdisplay(Pagination.page)
	var totalData = {};
	if (priceResponse && priceResponse.prices && priceResponse.prices.Payload && priceResponse.prices.Payload.any0 && priceResponse.prices.Payload.any0.pricingResponse 
		&& priceResponse.prices.Payload.any0.pricingResponse.priceDetails && priceResponse.prices.Payload.any0.pricingResponse.priceDetails.priceDetails 
		&& priceResponse.prices.Payload.any0.pricingResponse.priceDetails.priceDetails.currencyCode) {
		totalData.currency = priceResponse.prices.Payload.any0.pricingResponse.priceDetails.priceDetails.currencyCode
	}
	totalData.total = setScale(totalPrice, 2);
	totalData.list = setScale(totalList, 2);
	totalData.vat = setScale(totalVat, 2);
	SPM.executeOnParent({data:totalData, method:'setTotalData'});
}

function updateOptions(picklistName, values) {
	for (var i = 0; i < schema.attributes.length; i++) {
		if (schema.attributes[i].name === picklistName && schema.attributes[i].type === 'Picklist') {
			schema.attributes[i].options = values;
		}
	}
	var options = {};
	for (var i = 0; i < values.length; i++) {
		options[values[i]] = values[i];
	}
	var rows = jQuery('[data-id="' + picklistName + '"]');
	for (var i = 0; i < rows.length; i++) {
		var selectedOption = jQuery(rows[i]).find(":selected").text();
		if (selectedOption === 'Select a product') {
			selectedOption = '';
		}
		jQuery(rows[i]).empty();
		jQuery(rows[i]).append(jQuery("<option></option>").attr("value", 'Select a product').text('Select a product'));
		var valueFound = false;
		jQuery.each(options, function(key, value) {
			if (value === selectedOption) {
				valueFound = true;
				jQuery(rows[i]).append(jQuery("<option></option>")
					.attr("value", value).text(key)
					.attr('selected','selected'));
			} else {
				jQuery(rows[i]).append(jQuery("<option></option>")
					.attr("value", value).text(key));
			}
		}); 
		if (!valueFound) {
			jQuery(rows[i]).select2("val", "");
			var row = jQuery(rows[i]).parent().parent().parent();
			if(row[0]) {
			    jQuery(row[0]).find('input[data-id="Qty"]').val('0');
			    jQuery(row[0]).find('input[data-id="List Price"]').val('0');
			    jQuery(row[0]).find('input[data-id="VAT"]').val('0');
			    jQuery(row[0]).find('input[data-id="Price"]').val('0');
			    jQuery(row[0]).find('input[data-id="Min"]').val('');
			    jQuery(row[0]).find('input[data-id="Max"]').val('');
			    jQuery(row[0]).find('input[data-id="Inventory"]').val('0');
			    jQuery(row[0]).find('input[data-id="Ambient Loaded Qty"]').val('0');
			    jQuery(row[0]).find('input[data-id="Ambient Del Qty"]').val('0');
			    jQuery(row[0]).find('input[data-id="Std Del Qty"]').val('0');
			    jQuery(row[0]).find('input[data-id="Std Loaded Qty"]').val('0');
			    jQuery(row[0]).find('input[data-id="Unit of Measure"]').val('');
			}
		}
	}
	
	var picklistMap = {};
	for (var i = 0; i < values.length; i++) {
		picklistMap[values[i]] = values[i];
	}
	for (var i = 0; i < Object.keys(configurationMap).length; i++) {
		var confVal = configurationMap[Object.keys(configurationMap)[i]][picklistName];
		var selectedVal = jQuery('[data-guid="' + Object.keys(configurationMap)[i] + '"]').parent().parent().find('[data-id="Product"]').val();
		if (selectedVal !== confVal) {
			configurationMap[Object.keys(configurationMap)[i]][picklistName] = selectedVal;
		}
		if (!picklistMap[confVal] || confVal === 'Select a product') {
			configurationMap[Object.keys(configurationMap)[i]][picklistName] = '';
			configurationMap[Object.keys(configurationMap)[i]]["Status"] = 'Incomplete';
			jQuery('[data-guid="' + Object.keys(configurationMap)[i] + '"]').parent().parent().find('[data-id="Status"]').val('Incomplete');
		}
	}
}

function print(result) {
	console.log(result);
}

function setCalculations(input) {
	validationMessages = '';
	setMLERows();
	findMaxQuantity();
	var configurationGuids = Object.keys(configurationMap);
	for (var i = 0; i < configurationGuids.length; i++) {
		for (var j = 0; j < calculations.length; j++) {
			if (calculations[j].type === 'product') {
				if (mleProducts.productMap[configurationMap[configurationGuids[i]].Product]) {
					if (calculations[j].field.indexOf('.') > -1) {
						var fields = calculations[j].field.split('.');
						configurationMap[configurationGuids[i]][calculations[j].property] = mleProducts.productMap[configurationMap[configurationGuids[i]]['Product']][fields[0]][fields[1]];
						jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="'+ calculations[j].property + '"]').val(mleProducts.productMap[configurationMap[configurationGuids[i]]['Product']][fields[0]][fields[1]]);
					} else {
						configurationMap[configurationGuids[i]][calculations[j].property] = mleProducts.productMap[configurationMap[configurationGuids[i]]['Product']][calculations[j].field];
						jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="'+ calculations[j].property + '"]').val(mleProducts.productMap[configurationMap[configurationGuids[i]]['Product']][calculations[j].field]);
					}
				} else {
					configurationMap[configurationGuids[i]][calculations[j].property] = '';
					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="'+ calculations[j].property + '"]').val('');
				}
			}
		}
	}
	validateQuantity(input);
	//changeMLEdisplay(Pagination.page);
}

function setCalculationsOnChange(input) {
	isChange = true;
	setCalculations(input);
	isChange = false;
}

function matchCustom(params, data) {
	// If there are no search terms, return all of the data
	if ($.trim(params.term) === '' && data.text !== 'Select a product') {
	  return data;
	}

	// Do not display the item if there is no 'text' property
	if (typeof data.text === 'undefined') {
	  return null;
	}

	// `params.term` should be the term that is used for searching
	// `data.text` is the text that is displayed for the data object
	if (data.text.indexOf(params.term) > -1 || isProductInArray(findMatchingProducts(params.term), data.text)) {
	  var modifiedData = $.extend({}, data, true);
	  modifiedData.text += ' (matched)';

	  // You can return modified objects from here
	  // This includes matching the `children` how you want in nested data sets
	  return modifiedData;
	}

	// Return `null` if the term should not be displayed
	return null;
}

function findMatchingProducts(inputText) {
	var products = [];
	for (var i = 0; i < mleProducts.products.length; i++) {
		var productCode = mleProducts.products[i].ProductCode;
		var productName = mleProducts.products[i].Name;
		if (productCode.indexOf(inputText) > -1) {
			products.push(productName);
		}
	}
	return products;
}

function isProductInArray(array, product) {
	var inArray = false;
	for (var i = 0; i < array.length; i++) {
		if (array[i] === product) {
			inArray = true;
		}
	}
	return inArray;
}

function showSaveMessage() {
	var configurationGuids = Object.keys(configurationMap);
	var isSuccess = true;
	for (var i = 0; i < configurationGuids.length; i++) {
		if (configurationMap[configurationGuids[i]].Status === 'Incomplete') {
			isSuccess = false;
		}
	}
	if (isSuccess) {
		iziToast.success({
			title: 'Success',
			message: 'Saved.',
			position: 'topCenter',
			progressBar: false
		});
	} else {
		iziToast.error({
			title: 'Error',
			message: 'There are incomplete configurations...',
			position: 'topCenter',
			progressBar: false
		});
	}
}

function minMaxValidation(inputId,inputAttrName) {
	var configurationGuids = Object.keys(configurationMap);
	var quantityMap = {};
	for (var i = 0; i < configurationGuids.length; i++) {
		var itemName = configurationMap[configurationGuids[i]]["Product"];
		if (mleProducts.productMap && mleProducts.productMap[itemName] && mleProducts.productMap[itemName].Product2) {
			//var itemType = mleProducts.productMap[itemName].Product2.Family;
			// for now we will use all products
			var itemType = 'All';
			var itemQuantity = configurationMap[configurationGuids[i]]["Qty"];
			if (isNaN(itemQuantity) || itemQuantity === '') {
					itemQuantity = 0;
			} else {
				itemQuantity = parseFloat(itemQuantity);
			}
			if (quantityMap[itemType]) {
				quantityMap[itemType] = parseFloat(quantityMap[itemType]) + itemQuantity;
			} else {
				quantityMap[itemType] = itemQuantity;
			}
		}
	}
	for (var i = 0; i < configurationGuids.length; i++) {
		var item = configurationMap[configurationGuids[i]];
		var max = configurationMap[configurationGuids[i]]["Max"];
		var min = configurationMap[configurationGuids[i]]["Min"];
		var itemName = configurationMap[configurationGuids[i]]["Product"];
		var itemQuantity = configurationMap[configurationGuids[i]]["Qty"];
		if (mleProducts.productMap && mleProducts.productMap[itemName] && mleProducts.productMap[itemName].Product2) {
			if (isNaN(itemQuantity) || itemQuantity === '') {
				itemQuantity = 0;
			} else {
				itemQuantity = parseFloat(itemQuantity);
			}
			if (min !== '') {
				min = parseFloat(min);
				if (itemQuantity < min) {
					item["Status"] = 'Incomplete';
					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Status"]').val('Incomplete');
					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Qty"]').parent().addClass('cst-required1');
					if(inputId && configurationGuids[i] == inputId) {
					    validationMessages += 'Minimum quantity required for ' + item["Product"] + '|';
					}
				}
			}
			if (max !== '' && max > 0) {
				//var itemType = mleProducts.productMap[itemName].Product2.Family;
				// for now we will use all
				var itemType = 'All';
				var groupQuantity = quantityMap[itemType];
				max = parseFloat(max);
				if (groupQuantity > max) {
					item["Status"] = 'Incomplete';
					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Status"]').val('Incomplete');
					jQuery('[data-guid="' + configurationGuids[i] + '"]').parent().parent().find('[data-id="Qty"]').parent().addClass('cst-required1');
					if(inputId && configurationGuids[i] == inputId) {
					    validationMessages += 'Please reduce quantity of ' + itemType + ' products.|';
					}
				}
			}
		}
	}
	if (validationMessages !== '' && (isChange ||isNewRow)) {
		validationMessages = validationMessages.split('|');
		validationMessages = uniq(validationMessages);
		validationMessages = validationMessages.join(';');
		if (validationMessages.indexOf(';') == 0) {
			validationMessages = validationMessages.substring(1);
		}
		iziToast.error({
			title: 'Error',
			message: validationMessages,
			position: 'topCenter',
			progressBar: false
		});
		isNewRow = false;
	}
}


function uniq(a) {
	return a.sort().filter(function(item, pos, ary) {
		return !pos || item != ary[pos - 1];
	})
}

function renderMobile() {
	jQuery('.slds-table_bordered').addClass('slds-max-medium-table_stacked-horizontal2');
	jQuery('.slds-table_bordered').removeClass('slds-table_col-bordered');
	jQuery('.slds-table_bordered').removeClass('slds-table_col-bordered');
	jQuery('.slds-cell-shrink').removeClass('slds-cell-shrink');
	jQuery('.slds-truncate').addClass('cst-half');
	jQuery('td').css('padding', '10px');
	jQuery('.slds-table_bordered').css('width', '80%');
	jQuery('[data-id="Price"]').parent().parent().attr('data-label', 'Price');
	jQuery('[data-id="List Price"]').parent().parent().attr('data-label', 'List Price');
	jQuery('[data-id="Product"]').parent().parent().attr('data-label', 'Product');
	jQuery('[data-id="Qty"]').parent().parent().attr('data-label', 'Qty');
	jQuery('[data-id="VAT"]').parent().parent().attr('data-label', 'VAT');
	jQuery('[data-id="Min"]').parent().parent().attr('data-label', 'Min');
	jQuery('[data-id="Max"]').parent().parent().attr('data-label', 'Max');
	jQuery('[data-id="Inventory"]').parent().parent().attr('data-label', 'Inventory');
	jQuery('[data-el="delete"]').parent().attr('data-label', 'Delete');
}

function renderLogic() {
	if (!isMobile.any()) {
		if (jQuery(window).width() < 700) {
			var columns = {};
			columns["Inventory"] = false;
			columns["Max"] = false;
			columns["Min"] = false;
			columns["VAT"] = false;
			columns["List Price"] = false;
			columns["Unit of Measure"] = false;
			columns["Status"] = false;
			displayColumns(columns);
			renderMobile();
			jQuery('#exportCSV').hide();
			jQuery('.slds-file-selector__dropzone').parent().parent().hide();
		} else {
			jQuery('.slds-table_bordered').css('width', '100%');
			jQuery('.slds-truncate').removeClass('cst-half');
			jQuery('td').css('padding', '0');
			jQuery('.slds-table_bordered').removeClass('slds-max-medium-table_stacked-horizontal2');
			var columns = {};
			columns["Inventory"] = true;
			columns["Max"] = true;
			columns["Min"] = true;
			columns["List Price"] = true;
			columns["VAT"] = true;
			columns["Unit of Measure"] = true;
			columns["Status"] = true;
			displayColumns(columns);
			jQuery('#exportCSV').show()
			jQuery('.slds-table_bordered').addClass('slds-table_col-bordered');
			jQuery('.slds-file-selector__dropzone').parent().parent().show();
			jQuery('#headRow th:nth-child(3)').css('width','11%');
			jQuery('#headRow th:nth-child(4)').css('width','5%');
			jQuery('#headRow th:nth-child(5)').css('width','8%');
			jQuery('#headRow th:nth-child(6)').css('width','8%');
			jQuery('#headRow th:nth-child(7)').css('width','8%');
		}
	} else {
		var columns = {};
		columns["Inventory"] = false;
		columns["Max"] = false;
		columns["Min"] = false;
		columns["VAT"] = false;
		columns["List Price"] = false;
		columns["Unit of Measure"] = false;
		columns["Status"] = false;
		displayColumns(columns);
		renderMobile();
		jQuery('#exportCSV').hide();
		jQuery('.slds-file-selector__dropzone').parent().parent().hide();
		jQuery('.slds-table_bordered').removeClass('slds-table_col-bordered');
		jQuery('#headRow th:nth-child(3)').css('width','11%');
		jQuery('#headRow th:nth-child(4)').css('width','5%');
		jQuery('#headRow th:nth-child(5)').css('width','8%'); 
	}
	reduceColumns();
}

function reduceColumns() {
    var thRow = jQuery('#headRow')[0];
    var ths = jQuery(thRow).find('th div');
    for(var i = 0; i<ths.length; i++) {
        var txt = jQuery(ths[i]).text();
        if(txt == 'Min' || txt == 'Max') {
            jQuery(ths[i]).parent().css('width','5%'); 
        }
        else if(txt == 'Status') {
            jQuery(ths[i]).parent().css('width','7%'); 
        }
        else if(txt == 'Inventory') {
            jQuery(ths[i]).parent().css('width','7%'); 
        }
        else if(txt == 'Unit of Measure') {
            jQuery(ths[i]).parent().css('width','7%'); 
        }         
    }
}


function renderAsMobile() {
	setTimeout(function() {
		renderLogic();
	}, 50);
}

function definedToleranceResponse(result) {
    if(result) {
        definedTolerance = {};
        toleranceAcquired = true;
        if(result.st_vs_am_q) {
            definedTolerance.st_vs_am_q = result.st_vs_am_q;
        }
        if(result.ord_vs_load__q_inc) {
            definedTolerance.ord_vs_load__q_inc = result.ord_vs_load__q_inc;
        }
        if(result.ord_vs_load__q_dec) {
            definedTolerance.ord_vs_load__q_dec = result.ord_vs_load__q_dec;
        }
        if(result.ord_vs_del__q_inc) {
            definedTolerance.ord_vs_del__q_inc = result.ord_vs_del__q_inc;
        }
        if(result.ord_vs_del__q_dec) {
            definedTolerance.ord_vs_del__q_dec = result.ord_vs_del__q_dec;
        }        
    }
}

function getDefinedTolerance() {
    if(accountId && !toleranceAcquired) {
        SPM.remoteAction({AccountId: accountId}, 'TrafiguraGetDefinedTolerance', 'definedToleranceResponse');
    }
}

/**
 * Tranlates MLE elements to the parameter language
 */
function translateMLE(userLanguage) {

console.log('Translating MLE to: '+userLanguage);
	if(userLanguage === "es") {
		jQuery.each(jQuery("th"), function (index, value) {
			var hText = jQuery(value).text();
			if(hText == "Status") {
				jQuery(value).html("Estado");
			} else if(hText == "Product") {
				jQuery(value).html("Producto");
			} else if(hText == "Quantity") {
				jQuery(value).html("Cantidad");
			} else if(hText == "List Price") {
				jQuery(value).html("Precio de lista");
			} else if(hText == "VAT") {
				jQuery(value).html("IVA");
			} else if(hText == "Price") {
				jQuery(value).html("Precio");
			} else if(hText == "Min") {
				jQuery(value).html("Min");
			} else if(hText == "Max") {
				jQuery(value).html("Max");
			} else if(hText == "Inventory") {
				jQuery(value).html("Inventario");
			} else if(hText == "Unit of Measure") {
				jQuery(value).html("Unidad de medida");
			} 
		});

		jQuery("#addRowBtn").html(jQuery("#addRowBtn").html().replace("Add","AÃ±adir"));
		jQuery("#removeRowBtn").html(jQuery("#removeRowBtn").html().replace("Remove All","Eliminar todo"));
		jQuery("#file-selector-secondary-label").html(jQuery("#file-selector-secondary-label").html().replace("Import CSV","Importar CSV"));
		jQuery("#exportCSV").html(jQuery("#exportCSV").html().replace("Export CSV","Exportar CSV"));
		jQuery("#pricingBtn").html(jQuery("#pricingBtn").html().replace("Get Price","Obtener precio"));
		jQuery("#cancelBtnMultiple").html(jQuery("#cancelBtnMultiple").html().replace("Cancel","Anular"));
		jQuery("#saveBtnMultiple").html(jQuery("#saveBtnMultiple").html().replace("Save","Guardar"));

		//isTranslated = true;
	}
}


jQuery(window).resize(function() {
	renderLogic();
});



var deliveryType = '';
var mleProducts = [];
var accountId = '';
var productType = '';
var calculations = [];
var orderData = {};
var priceResponse = {};
var orderType = '';
var epoch = '';
var locationId = '';
var validationMessages = '';
var isChange = false;
var absoluteMaximum = 0;
var toleranceAcquired = false;
var definedTolerance;
var pricesChanged = false;
var userLanguage;
var isNewRow = false;

for (var i = 0; i < schema.attributes.length; i++) {
	if (schema.attributes[i].calculation) {
		var calc = {};
		calc.property = schema.attributes[i].name;
		calc.field = schema.attributes[i].calculation;
		calc.type = schema.attributes[i].calculationType;
		calculations.push(calc);
	}
}

registerHandler('executeAction', setAccountId);
registerHandler('executeAction', saveAndGetPricing);
registerHandler('executeAction', saveWithoutPricing);
registerHandler('remoteAction', initialLoad);
registerHandler('remoteAction', setPriceResponse);
registerHandler('remoteAction', setPriceResponseAuto);
registerHandler('onNewRow', reloadOptions);
registerHandler('onDelete', reloadOptions);
registerHandler('onDelete', renderAsMobile);
registerHandler('onDelete', calculatePriceOnDelete);
registerHandler('onRemoveAll', reloadOptions);
registerHandler('onRemoveAll', calculatePrice);
registerHandler('onCancel', reloadOptions);
registerHandler('onCancel', calculatePrice);
registerHandler('onSave', calculatePrice);
registerHandler('onSave', showSaveMessage);
registerHandler('onNewRow', setReadOnly);
registerHandler('onDelete', setReadOnly);
registerHandler('onRemoveAll', setReadOnly);
registerHandler('onRemoveAll', setReadOnly);
registerHandler('onCancel', setReadOnly);
registerHandler('onCancel', setReadOnly);
registerHandler('onInputChange', setCalculationsOnChange);
registerHandler('onNewRow', setCalculations);
registerHandler('onNewRow', findMaxQuantity);
registerHandler('onDelete', findMaxQuantity);
registerHandler('onInputChange', findMaxQuantity);
registerHandler('onInputChange', calculatePrice);
registerHandler('onNewRow', renderAsMobile);
registerHandler('onLoad', renderAsMobile);
registerHandler('remoteAction', definedToleranceResponse);
Visualforce.remoting.timeout = 120000;

var isMobile = {
	Android: function() {
		return navigator.userAgent.match(/Android/i);
	},
	BlackBerry: function() {
		return navigator.userAgent.match(/BlackBerry/i);
	},
	iOS: function() {
		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	},
	Opera: function() {
		return navigator.userAgent.match(/Opera Mini/i);
	},
	Windows: function() {
		return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
	},
	any: function() {
		return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	}
};

window.onload = function() {
	renderAsMobile();
};

if (isMobile.any()) {
	var columns = {};
	columns["Inventory"] = false;
	columns["Max"] = false;
	columns["Min"] = false;
	columns["VAT"] = false;
	columns["List Price"] = false;
	columns["Unit of Measure"] = false;
	columns["Status"] = false;
	displayColumns(columns);
	renderMobile();
	jQuery('#exportCSV').hide();
	jQuery('.slds-file-selector__dropzone').parent().parent().hide();
	jQuery('.slds-table_bordered').removeClass('slds-table_col-bordered');
} else {
	renderLogic();
}

var buttonHTML = '<button class="slds-button slds-button_neutral" id="pricingBtn" onClick="buttonClick()">Get Price</button>';
jQuery('#addRowBtn').parent().append(buttonHTML);
jQuery('.slds-button').css("color", "#00945b");
jQuery('.slds-button_brand').css("background-color", "#fff");
jQuery('.slds-button_brand').css("border", "1px solid #d8dde6");