function checkOrderStatus(prepaymentIsRequired,customerHasOverdueInvoices,creditLimitCheck,orderCreationStepIndex) {
	if ((prepaymentIsRequired == 'true' || customerHasOverdueInvoices == 'true' || creditLimitCheck == 'true') && orderCreationStepIndex == 5) {
		openPaymentConfirmationDialog(customerHasOverdueInvoices);
	}
}
function openPaymentConfirmationDialog(customerHasOverdueInvoices) { 
    var box = new SimpleDialog('hersh' + Math.random(), true);
    parent.box = box;
    box.createDialog();
    box.setWidth(350);
    box.setupDefaultButtons();
    // When account has overdue invoice then  exclude "proceed to payment" button
    if ( customerHasOverdueInvoices== 'true') {
        var boxContent = '<center>';
        boxContent += '<h1>There are overdue invoices against this account. Please ask the customer to settle these invoices before placing a new order.</h1><br/><br/>';
        boxContent += 'If you choose to continue, you will be able to progress with the order creation, however it will not be accepted by NAV for fulfillment. It will require a credit exception approval before it can be processed.<br/><br/>';
        boxContent += '<button class="btn" onclick="validateCancel(); return false;">Cancel Order</button>';
        boxContent += '&nbsp;';
        boxContent += '<button class="btn" onclick="saveOrderAsDraft();">Save as Draft</button>'; // added by Arpit
        boxContent += '</center>';
        box.setTitle('You have overdue invoices');
    }
    else{
        var boxContent = '<center>';
        boxContent += '<h1>There is not enough Credit available for this Account. Please ask the customer to prepay for this order or clear the dues if any.</h1><br/><br/>';
        boxContent += 'If you choose to Save as Draft, the order will not be accepted by NAV for fulfilment. It will require a credit exception approval before it can be processed.<br/><br/>';
        boxContent += '<button class="btn" onclick="validateOrderCancel(); return false;">Cancel Order</button>';
        boxContent += '&nbsp;';
        boxContent += '<button class="btn" onclick="saveOrderAsDraft();">Save as Draft</button>'; // added by Arpit
        boxContent += '</center>';
        box.setTitle('Prepayment required');
    }         
    box.setContentInnerHTML(boxContent);
    box.show();
} 
function exceptionApprovalMessageForOverDueInvoice() {
    var box = new SimpleDialog('exceptionApproval', true);
    var boxContent = '<center>';
    boxContent += '<h1>There are overdue invoices against this account. Please ask the customer to settle these invoices before placing a new order.</h1><br/><br/>';
    boxContent += 'If you choose to continue, you will be able to progress with the order creation, however it will not be accepted by NAV for fulfillment. It will require a credit exception approval before it can be processed.<br/><br/>';
    boxContent += '<button class="btn" onclick="stayOnAccount(); return false;">Cancel</button>';
    boxContent += '&nbsp;';
    boxContent += '<button class="btn" onclick="openOrderCreation(); return false;">Next</button>';
    boxContent += '</center>';
    parent.box = box;
    box.setTitle('You have overdue invoices');
    box.createDialog();
    box.setWidth(350);
    box.setContentInnerHTML(boxContent);
    box.setupDefaultButtons();
    box.show();
}

function exceptionApprovalMessageForOverDueInvoiceForNewOrderPage() {
    var box = new SimpleDialog('exceptionApproval', true);
    var boxContent = '<center>';
    boxContent += '<h1>There are overdue invoices against this account. Please ask the customer to settle these invoices before placing a new order.</h1><br/><br/>';
    boxContent += 'If you choose to continue, you will be able to progress with the order creation, however it will not be accepted by NAV for fulfillment. It will require a credit exception approval before it can be processed.<br/><br/>';
    boxContent += '<button class="btn" onclick="stayOnAccount(); return false;">Cancel</button>';
    boxContent += '&nbsp;';
    boxContent += '<button class="btn" onclick="openNewOrderCreation(); return false;">Next</button>';
    boxContent += '</center>';
    parent.box = box;
    box.setTitle('You have overdue invoices');
    box.createDialog();
    box.setWidth(350);
    box.setContentInnerHTML(boxContent);
    box.setupDefaultButtons();
    box.show();
}
function proceedToNext(){
	var exceptionApprovalBox  = document.getElementById("exceptionApproval");
	window.parent.exceptionApprovalBox = exceptionApprovalBox;
	window.parent.exceptionApprovalBox.hide();
	document.getElementsByClassName('overlayBackground')[0].hide();
	next();
}
function openOverdueInvoiceConfirmationDialogWhenSellToEqualsBillTo(localSupportNumber) {
    var box = new SimpleDialog('hersh' + Math.random(), true);
    var boxContent = '<center>';
    boxContent += '<h1>It appears that there are overdue invoices against your account. You must settle these invoices before you can submit a new order.</h1><br/><br/>';
    boxContent += 'You can use the option below to pay us online or you can contact our support team at';
    boxContent += localSupportNumber;
    boxContent += '.<br/><br/>';
    boxContent += '<button class="btn" onclick="validateCancel(); return false;">Cancel</button>';
    boxContent += '&nbsp;';
    boxContent += '<button class="btn" onclick="initiateInvoiceReview(); return false;">Review Overdue Invoices</button>';
    boxContent += '</center>';
    parent.box = box;
    box.setTitle('You have overdue invoices');
    box.createDialog();
    box.setWidth(350);
    box.setContentInnerHTML(boxContent);
    box.setupDefaultButtons();
    box.show();
}
function openOverdueInvoiceConfirmationDialogWhenSellToNotEqualsBillTo() {
    var box = new SimpleDialog('hersh' + Math.random(), true);
    var boxContent = '<center>';
    boxContent += '<h1>It appears that there are overdue invoices against your Bill-To account.</h1><br/><br/>';
    boxContent += '<h1>Please contact your Bill-To account.</h1><br/><br/>';
    boxContent += '<button class="btn" onclick="validateCancel(); return false;">Cancel</button>';
    boxContent += '</center>';
    parent.box = box;
    box.setTitle('You have overdue invoices');
    box.createDialog();
    box.setWidth(350);
    box.setContentInnerHTML(boxContent);
    box.setupDefaultButtons();
    box.show();
}
function openOverdueInvoiceConfirmationDialogWhenOnlyBillToAccount() {
    var box = new SimpleDialog('hersh' + Math.random(), true);
    var boxContent = '<center>';
    boxContent += '<h1>It appears that there are overdue invoices against your account.</h1><br/><br/>';
    boxContent += '<button class="btn" onclick="validateCancel(); return false;">Cancel</button>';
    boxContent += '&nbsp;';
    boxContent += '<button class="btn" onclick="initiateInvoiceReview(); return false;">Review Overdue Invoices</button>';
    boxContent += '</center>';
    parent.box = box;
    box.setTitle('You have overdue invoices');
    box.createDialog();
    box.setWidth(350);
    box.setContentInnerHTML(boxContent);
    box.setupDefaultButtons();
    box.show();
}
function openOverdueInvoiceConfirmationDialog() {
    var box = new SimpleDialog('hersh' + Math.random(), true);
    var boxContent = '<center>';
    boxContent += '<h1>It appears that there are overdue invoices against your account. You must settle these invoices before you can submit a new order.</h1><br/><br/>';
    boxContent += 'You can use the option below to pay us online or you can contact our support team at XXXXXX.<br/><br/>';
    boxContent += '<button class="btn" onclick="validateCancel(); return false;">Cancel</button>';
    boxContent += '&nbsp;';
    boxContent += '<button class="btn" onclick="initiateInvoiceReview(); return false;">Review Overdue Invoices</button>';
    boxContent += '</center>';
    parent.box = box;
    box.setTitle('You have overdue invoices');
    box.createDialog();
    box.setWidth(350);
    box.setContentInnerHTML(boxContent);
    box.setupDefaultButtons();
    box.show();
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
        return false;
    }    
    return true;
}
function validateSubmit() {
	var r = confirm('Are you sure you want to proceed with this order?');
	if (r == true) {
		initiateSubmit();
    }
}
function validateOrderCancel() {
    var r = confirm('Are you sure you want to cancel this order?');
    if (r == true) {
        initiateOrderCancel();
    }
}
function validateCancel() {
    var r = confirm('Are you sure you want to cancel?');
    if (r == true) {
        initiateCancel();
    }
}
function validateNextStep(index) {
            
    initiateNextAction();
}
        
        function validateBackStep(index) {
            if (index == '4') {
        var r = confirm('Updating the Ship-To or Pickup location will automatically remove all entered products from the order. Are you sure you want to continue?');
        if (r != true) {
            return null;
        }
    }
    refreshDatePicker();
    initiateBackAction();
}

function openPaymentPage() {
   // top.location = '/apex/EP_PaymentPage';
	makePayment();
}

function initiateOrderLineItemRemoval(item) {
    var r = confirm('Are you sure you want to remove this order line item?');
    
    if (r == true) {
        removeOrderLineItem(item);
    }
}
  function setOrderLineItemDetails(){
    var table = document.getElementById('orderManagementPage:mainOrderForm:orderCreationBlock:itemDetails');
    var inputs = document.getElementsByClassName('requiredInput');
    var filledValues = 0;
    for(i=0;i<inputs.length;i++){
        if(inputs[i].childNodes[1].value != ''){
                filledValues += 1; 
            }
        }
        if( inputs.length == filledValues){
            setOrderLineItemsDetails();
        }
    }
  
    function refreshDatePicker() {
        console.log(j$('.orderDateInputBox').datepicker("refresh")+'Entry');
    
    j$(".orderDateInputBox").datepicker("destroy");
    j$(".orderDateInputBox").datepicker({
        minDate: 1,
        dateFormat: 'dd-mm-yy'
    });
    j$(".orderDateInputBox").attr('readOnly', 'true');
    j$(".orderDateInputBox").datepicker("refresh");
    console.log(j$('.orderDateInputBox').val()+'EXIT');
}
	function stayOnAccount(){	
			
			//var node1 = document.getElementsByClassName('overlayBackground')[0];  // #defectFix PopUp Cancel Button Issue
			var node1 = document.querySelectorAll('.overlayBackground')[0]; // #defectFix PopUp Cancel Button Issue
			node1.parentNode.removeChild(node1); // #defectFix PopUp Cancel Button Issue
			var node = document.getElementById("exceptionApproval");
			console.log('--'+node.parentNode);
			node.parentNode.removeChild(node); 
			//var node1 = document.getElementsByClassName('overlayBackground')[0];
			//console.log('--'+node1);
	}
	
	function openOrderCreation(){
		var url = window.location.pathname;
    	var accountId = url.substring(url.lastIndexOf('/') + 1);
		var win = window.open('/apex/EP_PortalOrderPage?id='+accountId, '_self'); 
	}
	
	function openNewOrderCreation(){
		var url = window.location.pathname;
    	var accountId = url.substring(url.lastIndexOf('/') + 1);
		var win = window.open('/apex/EP_PortalOrderNew?id='+accountId, '_self'); 
	}