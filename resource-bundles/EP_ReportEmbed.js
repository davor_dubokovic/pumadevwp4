window.onload = function() {

var expiresOn = parseInt(expires_on);
var currentTime = Date.now()/1000;
        
   if(expiresOn <= currentTime || !accessToken)
    { 
        window.location.href = authURL;
    } else if((expiresOn - 2000) <= currentTime)
           {
                refreshAccessToken();
           }
 
var filter = {
      $schema: "http://powerbi.com/product/schema#basic"
    };
var models = window['powerbi-client'].models;
    
var embedConfiguration = {

    type: 'report',
    
    id: selectedReport,

    embedUrl: 'https://app.powerbi.com/reportEmbed?reportId='+selectedReport+'&groupId='+groupId,
    
    accessToken: accessToken,
    

    settings: {

            filterPaneEnabled: true,

            navContentPaneEnabled: true

        }
};
powerbi.accessToken = accessToken;
var element = document.getElementById('myReport');

var report = powerbi.embed(element, embedConfiguration);
}
