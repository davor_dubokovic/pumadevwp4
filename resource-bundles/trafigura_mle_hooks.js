'use strict';
console.log("trafigura_mle_hooks.js is loaded");
let userLanguage = null;

const intervalRef = setInterval(
	() => {
		if (!window.CSMLEAPI) {
			return;
		}
		
	//	var scope = window.myScope = angular.element(jQuery('[class="container ng-scope"]')[0]).scope();
		
	//	console.log(scope);

		clearInterval(intervalRef);

		const mleAPI = window.CSMLEAPI;

		if (window.parent) {
/*			mleAPI.onSaveSuccess(
				() => window.parent.postMessage(
					'mle-save-success',
					'*'
				)
			);

			mleAPI.onSaveError(
				() => window.parent.postMessage(
					'mle-save-fail',
					'*'
				)
			)

			mleAPI.onValidate(
				() => window.parent.postMessage(
					'mle-validate-success',
					'*'
				)
			)
*/
			window.addEventListener(
				'message',
				event => {
					if (event.data === 'mle-save-data') {
						var statusElements = document.getElementsByClassName("changed icon-remove-3");
						// if there are no items that are not saved in MLE do not execute saveAsync()
						if (statusElements == null || statusElements.length < 1) {
							window.parent.postMessage('mle-save-success', '*');
							return;
						}
						
						mleAPI
							.saveAsync()
							.then(
								() => window.parent.postMessage('mle-save-success', '*'),
								() => window.parent.postMessage('mle-save-fail', '*')
							).catch(
								() => window.parent.postMessage('mle-save-fail', '*')
							);
					} else if (event.data === 'mle-validate-all') {
						mleAPI
							.saveAsync()
							.then(
								() => window.parent.postMessage('mle-validate-success', '*'),
								() => window.parent.postMessage('mle-validate-fail', '*')
							).catch(
								() => window.parent.postMessage('mle-validate-fail', '*')
							);
					} else if (event.data.indexOf('mle-language-') === 0) {
						userLanguage = event.data.substring('mle-language-'.length);
					} else if (event.data === 'mle-get-prices') {
						var statusElements = document.getElementsByClassName("changed icon-remove-3");
						// if there are no items that are not saved in MLE do not execute saveAsync()
						if (statusElements == null || statusElements.length < 1) {
							window.parent.postMessage('mle-save-success', '*');
							return;
						}
						
						mleAPI
							.saveAsync()
							.then(
								() => window.parent.postMessage('mle-pricing-save-success', '*'),
								() => window.parent.postMessage('mle-pricing-save-fail', '*')
							).catch(
								() => window.parent.postMessage('mle-save-fail', '*')
							);
					}
				}
			);

			window.parent.postMessage(
				'mle-initialized',
				'*'
			);
		}
	},
	150
);

/*
 * translation interval
 */
const translationIntervalRef = setInterval(
	() => {
		if (!angular) {
			return;
		}

		const mleRootScope = angular.element('[ng-controller=EditorController]').scope();
		if (!mleRootScope || ! mleRootScope.columns || !window.CS || !window.CS.Service || !window.CS.Service.getProductIndex()['all'] || !Array.isArray(mleRootScope.columns) || !mleRootScope.columns.length || !userLanguage) {
			return;
		}

		clearInterval(translationIntervalRef);

		const productDefinition = window.CS.Service.getProductIndex()['all'][mleRootScope.productDefinitionId];

		let translationData = null;
		try {
			translationData = JSON.parse(productDefinition.Translations__c);
		} catch (e) {
			console.error('Error while reading translation data', e);
		}

		UserContext.language = userLanguage;

		mleRootScope.columns.forEach(
			column => {
				const attributeTranslations = translationData.attributes;
				let translation = attributeTranslations[column.field];
				if (!translation) {
					translation = attributeTranslations[column.name];
				}
				if (translation && translation.labels && translation.labels[UserContext.language]) {
					column.name = translation.labels[UserContext.language];
				}
			}
		);
		
		mleRootScope.$apply();
		//
        //translateMLEButtons = function() { 
        //const mleButtonsTranslations = translationData.buttons;

		//if (UserContext.language in mleButtonsTranslations['Clone']) { 
		//jQuery("[title='Clone Selected Rows']").text(mleButtonsTranslations['Clone'][UserContext.language]); 
		//} 
		//}; 


	},
	200
);
/***********************************************************************************************************************************************************************/