<apex:page tabStyle="EP_Tank__c" controller="EP_SelectTankDipSitePageExtClass_R1" action="{!retrieveSiteInformation}">
    <apex:stylesheet value="{!URLFOR($Resource.EP_StyleLib, 'css/overlay.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.EP_StyleLib, 'css/table.css')}" />
    
    <c:EP_PerformanceMonitor />
    
    <apex:sectionHeader subtitle="{!$Label.EP_Select_Site}" title="{!$Label.EP_Submit_Tank_Dips_Title}" id="sectionHeaderPanel" />

    <script>
        function selectSite() {
            // Fix for Defect 29809 - Start
            
            var optionBoxes = document.getElementsByName('selectedSite');
            var statusInputs = document.getElementsByName('selectedSiteStatus');
            var pageInputs = document.getElementsByName('selectedSitePage');
            var url;
            var status;
            
            for (var i = 0; i < optionBoxes.length; i++) {
                if (optionBoxes[i].checked) {
                    status = statusInputs[i].value;
                    
                    if (status == 'true') {
                        url = '/apex/EP_TankDipSubmitPage_R1?id=' + optionBoxes[i].value + '&pageNumber=' + pageInputs[i].value;
                    } else {
                        url = '/apex/EP_SelectTankDipDatePage_R1?id=' + optionBoxes[i].value + '&pageNumber=' + pageInputs[i].value;
                    }
                    
                    top.location = url;
                }
            } // End for
            
            // Fix for Defect 29809 - End
        }
    </script>

    <apex:form >
        
        <apex:actionStatus id="statusOverlay" startStyleClass="overlay"></apex:actionStatus> 
        
        <apex:outputPanel id="progressPanel">
            <center>
                <apex:image value="{!URLFOR($Resource.EP_TankDipLib, 'img/Tank_Dip_Wizard_Step1.png')}" />
            </center>
        </apex:outputPanel>

        <!-- No sites panel -->
        <apex:outputPanel rendered="{!(shipTos.size == 0)}">
            <div style="text-align: center;">
                <br />
                <apex:outputLabel >{!$Label.EP_No_Sites_Found_Label}</apex:outputLabel>
                <br />
                <br />
                <apex:commandButton value="{!$Label.EP_Cancel_Label}"
                    style="width: 80px;" action="{!cancel}" immediate="true" />
            </div>
        </apex:outputPanel>

        <apex:pageBlock id="mainTablePanel"
            title="{!$Label.EP_Available_Site_Label} [{!intNumberOfRecords}]" mode="maindetail"
            rendered="{!(shipTos.size > 0)}">
            <apex:pageBlockButtons id="buttonPanel" location="bottom">
                
                <!-- Hide Cancel button -->
                <apex:commandButton value="{!$Label.EP_Cancel_Label}"
                    style="width: 80px;" rendered="false" action="{!cancel}"
                    immediate="true" />
                <apex:commandButton value="{!$Label.EP_Next_Caption}"
                    style="width: 80px;" onclick="selectSite();return false;"
                    reRender="stockTablePanel,messagePanel,progressPanel,sectionHeaderPanel" />
                <apex:inputHidden />
                &nbsp;&nbsp;
                <apex:commandButton reRender="mainTablePanel" disabled="{!!hasPrevious}" style="min-width: 30px;" status="statusOverlay" action="{!goToFirst}" value="{!$Label.EP_First}"/>
                <apex:commandButton reRender="mainTablePanel" disabled="{!!hasPrevious}" style="min-width: 30px;" status="statusOverlay" action="{!previous}" value="{!$Label.EP_Previous}"/>
                <apex:commandButton reRender="mainTablePanel" disabled="{!!hasNext}" style="min-width: 30px;" status="statusOverlay" action="{!next}" value="{!$Label.EP_Next}"/>
                <apex:commandButton reRender="mainTablePanel" disabled="{!!hasNext}" style="min-width: 30px;" status="statusOverlay" action="{!goToLast}" value="{!$Label.EP_Last}"/>
                
            </apex:pageBlockButtons>
            
            <!-- Site Table -->
            <apex:pageBlockTable value="{!shipTos}" var="s">
                <apex:column headerValue="{!$Label.EP_Site_Label}">
                    <apex:outputPanel rendered="{!s.siteSelected}">
                        <input type="radio" name="selectedSite" value="{!s.siteID}"
                            checked="{!s.siteSelected}" />
                        &nbsp;
                        <apex:outputText value="{!s.siteName}" />
                    </apex:outputPanel>
                    <apex:outputPanel rendered="{!NOT(s.siteSelected)}">
                        <input type="radio" name="selectedSite" value="{!s.siteID}" />
                        &nbsp;
                        <apex:outputText value="{!s.siteName}" />
                    </apex:outputPanel>
                    <input type="hidden" name="selectedSiteStatus"
                        value="{!s.statusCount}" />
                    <input type="hidden" name="selectedSitePage"
                        value="{!s.pageNumber}" />
                </apex:column>
                <apex:column headerValue="{!$Label.EP_Status_Label}">
                    <apex:image value="{!URLFOR($Resource.EP_TankDipLib, 'img/EP_OK_Status_Icon.png')}"
                        rendered="{!s.siteShowOKStatus}" />
                    <apex:image value="{!URLFOR($Resource.EP_TankDipLib, 'img/EP_Caution_Status_Icon_Amber.gif')}"
                        rendered="{!s.siteShowWarningStatus}" />
                    <apex:image value="{!URLFOR($Resource.EP_TankDipLib, 'img/EP_Caution_Status_Icon.gif')}"
                        rendered="{!s.siteShowCautionStatus}" />
                </apex:column>
            </apex:pageBlockTable>
        </apex:pageBlock>
    </apex:form>
</apex:page>