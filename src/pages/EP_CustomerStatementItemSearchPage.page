<apex:page controller="EP_CustomerStatementItemSearchController" docType="html-5.0"  tabStyle="EP_Customer_Account_Statement__c" id="customerStatementItemSearchPage">
    <apex:stylesheet value="{!URLFOR($Resource.EP_StyleLib, 'css/overlay.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.EP_StyleLib, 'css/table.css')}" />
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css"></link>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="/resource/EP_CASISearch/css/style.css" type="text/css"></link>
    <script type="text/javascript" src="/resource/EP_CASISearch/js/CASISearch.js"></script>
    <script type="text/javascript">
        var j$ = jQuery.noConflict();
        j$(document).ready(function(){
            j$(".toDateInputBox").click();
        });
    </script>
    
    <c:EP_PerformanceMonitor />
    <apex:sectionHeader id="searchTitle" title="{!$Label.EP_Customer_Account_Statement_Item_Search_Page}"/>
    <apex:actionStatus id="statusOverlay" startStyleClass="overlay"></apex:actionStatus> 
    <apex:form id="casForm">        
        <apex:pageBlock title="{!$Label.EP_Customer_Account_Statement_Item_Subtitle}" id="searchpageBlock"> 
             <apex:pageBlockSection id="searchpageBlockSection" columns="3">
               <apex:outputPanel id="searchSectionItemBillToPanel" style="float: left;">
                <apex:pageBlockSectionItem id="searchpageBlockSectionItemBillTo" >
                    <apex:outputLabel id="searchpageBlockSectionItemBillToLabel" value="{!$Label.EP_Statement_Bill_To}"/>
                    <apex:outputLink id="searchpageBlockSectionItemBillToInput" value="/{!sBillToId}" style="text-align:center;margin-left:10px;">{!sBillToName}</apex:outputLink>
                </apex:pageBlockSectionItem>
               </apex:outputPanel>
               <apex:outputPanel id="searchSectionItemDateFromPanel" style="float:left;" >
                 <apex:pageBlockSectionItem id="searchpageBlockSectionItemDateFrom">
                    <apex:outputLabel id="searchpageBlockSectionItemDateFromLabel" value="{!$Label.EP_Statement_Date_From}"/>
                    <apex:input id="searchpageBlockSectionItemDateFromInputdatepicker" styleClass="fromDateInputBox" type="date" value="{!dateFrom}" onclick="setdatePicker();" style="margin-top: -5px;margin-left:10px;"/>
                </apex:pageBlockSectionItem>
                </apex:outputPanel>
                <apex:outputPanel id="searchSectionItemDateToPanel">
                 <apex:pageBlockSectionItem id="searchpageBlockSectionItemDateTo" >
                    <apex:outputLabel id="searchpageBlockSectionItemDateToLabel" value="{!$Label.EP_Statement_Date_To}" />
                    <apex:input id="searchpageBlockSectionItemDateToInput" styleClass="toDateInputBox" type="date" value="{!dateTo}" onclick="setdatePicker();" style="margin-top: -5px; margin-left:10px;"/> 
                </apex:pageBlockSectionItem>
                </apex:outputPanel>
             </apex:pageBlockSection>                      
        </apex:pageBlock> 
        <apex:commandButton id="searchButton" value="{!$Label.EP_Statement_Search}" action="{!searchCustomerStatement}" status="statusOverlay" style="overflow: visible; line-height: 120%; min-width: 80px; margin-bottom:10px;"  rerender="pageBlockOutput,msg"/> 
        <apex:outputPanel id="pageBlockOutput"> 
          <apex:pageBlock title="{!$Label.EP_Customer_Statement_Summary}" id="caspageBlock" rendered="{!isSearchStatement}">
            <apex:actionRegion >  
             <apex:outputPanel id="caspageBlockPanel" >
                 <apex:pageBlockTable value="{!lstWrapperCustAccStatement}" var="varline" id="tabId">
                        <!-- Start Code #45308
                        <apex:column style="text-align:left; width:75px;" id="OpeningBalanceColumn">
                            <apex:facet name="header">
                                 {!$Label.EP_Statement_Opening_Balance}
                            </apex:facet>
                            <apex:outputText id="OpeningBalanceColumnText" value="{0,number,###,###,###,##0.00}">
                                <apex:param value="{!varline.wOpeningBalance}"/>
                            </apex:outputText>
                        </apex:column>
                        End Code #45308-->
                         <apex:column style="text-align:left; width:75px;" id="TotalDebitsColumn" >
                            <apex:facet name="header">
                                 {!$Label.EP_Statement_Total_Debits}
                            </apex:facet>
                             <apex:outputText id="TotalDebitsColumnText" value="{0,number,###,###,###,##0.00}">
                                <apex:param value="{!varline.wTotalDebits}"/>
                            </apex:outputText>
                        </apex:column>
                        <apex:column style="text-align:left; width:75px;" id="TotalCreditsColumn" >
                            <apex:facet name="header">
                                 {!$Label.EP_Statement_Total_Credits}
                            </apex:facet>
                             <apex:outputText id="TotalCreditsColumnText" value="{0,number,###,###,###,##0.00}">
                                <apex:param value="{!varline.wTotalCredits}"/>
                            </apex:outputText>
                        </apex:column>
                        <apex:column style="text-align:left; width:75px;" id="ClosingBalanceColumn" >
                            <apex:facet name="header">
                                {!$Label.EP_Statement_Closing_Balance}
                            </apex:facet>
                             <apex:outputText id="ClosingBalanceColumnText" value="{0,number,###,###,###,##0.00}">
                                <apex:param value="{!varline.wClosingBalance}"/>
                            </apex:outputText>
                        </apex:column>
                    </apex:pageBlockTable>
                 </apex:outputPanel> 
              </apex:actionRegion>     
        </apex:pageBlock>
        <apex:pageBlock title="{!$Label.EP_Customer_Account_Statement_Item_Subtitle} Break Down" id="casipageBlock" rendered="{!isSearchStatementItem}"> 
                <apex:outputPanel id="casipageBlockPanel">
                    <apex:pageBlockTable value="{!lstWrapperCustAccStatementItem}" var="varline" id="tabId">
                        <apex:column style="text-align:left; width:75px;" id="DocumentNumberColumn">
                            <apex:facet name="header">
                               {!$Label.EP_Statement_Account_Entry_Document_Number}
                            </apex:facet>
                            <apex:outputLink id="DocumentNumberColumnText" value="/{!varline.wCustAccStatementItemId}" target="_blank">{!varline.wAccEntryDocNr}</apex:outputLink>
                        </apex:column>
                         <apex:column style="text-align:left; width:75px;" id="TypeColumn" >
                            <apex:facet name="header">
                               {!$Label.EP_Statement_Account_Entry_Type}
                            </apex:facet>
                            <apex:outputText id="TypeColumnText">{!varline.wAccEntryType}</apex:outputText>
                        </apex:column>
                        <apex:column style="text-align:right; width:75px;" id="CurrencyColumn">
                            <apex:facet name="header">
                               {!$Label.EP_Statement_Account_Entry_Currency}
                            </apex:facet>
                            <apex:outputText id="CurrencyColumnText">{!varline.wAccEntryCurr}</apex:outputText>
                        </apex:column>
                        <apex:column style="text-align:right; width:75px;" id="AmountColumn">
                            <apex:facet name="header">
                               {!$Label.EP_Statement_Account_Entry_Amount}                              
                            </apex:facet>
                            <apex:outputText id="AmountColumnText" value="{0,number,###,###,###,##0.00}">
                                <apex:param value="{!varline.wAccEntryAmount}"/>
                            </apex:outputText>
                        </apex:column>
                         <!-- Start code #45308
                        <apex:column style="text-align:right; width:75px;" id="IssueDate">
                            <apex:facet name="header">
                               {!$Label.EP_Statement_Account_Entry_Issue_Date}  
                            </apex:facet>
                            <apex:outputText id="IssueDateText">{!varline.wAccEntryIssueDate}</apex:outputText>
                        </apex:column>
                        End code #45308-->
                        <apex:column style="text-align:right; width:75px;" id="DueDateColumn">
                            <apex:facet name="header">
                               {!$Label.EP_Statement_Account_Entry_Due_Date}  
                            </apex:facet>
                            <apex:outputText id="DueDateColumnText">{!varline.wAccEntryDueDate}</apex:outputText>
                        </apex:column>
                        <apex:column style="text-align:left; width:75px;" id="DescriptionColumn">
                            <apex:facet name="header">
                               {!$Label.EP_Statement_Account_Entry_Description}  
                            </apex:facet>
                            <apex:outputText id="DescriptionColumnText">{!varline.wAccEntryDesc}</apex:outputText>
                        </apex:column>
                        <apex:column style="text-align:right; width:75px;" id="ClosingBalColumn" >
                            <apex:facet name="header">
                                {!$Label.EP_Statement_Closing_Balance} 
                            </apex:facet>
                             <apex:outputText id="ClosingBalColumnText" value="{0,number,###,###,###,##0.00}">
                                <apex:param value="{!varline.wClosingBalance}"/>
                            </apex:outputText>
                        </apex:column>
                    </apex:pageBlockTable>
                 </apex:outputPanel> 
          </apex:pageBlock>
       </apex:outputPanel>
       <apex:pageMessages id="msg"/> 
    </apex:form>

<!-- End Default Content REMOVE THIS -->
</apex:page>