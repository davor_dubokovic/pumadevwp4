<apex:page standardController="Lead" extensions="EP_CRM_LeadConvertController">
    <apex:outputPanel rendered="{!showForm}">
    
        <!-- Page section header -->
        <apex:sectionHeader title="Convert Lead" subtitle="{!Lead.name}" />
    
        <!-- Custom page header text -->
        To avoid creating duplicate contacts, select from the options below. When an existing contact is selected, 
        all open activities from the lead will be added to the contact and all new data will be added. 
        Lead data will not overwrite existing contact data. <br/><br/>    
    
    </apex:outputPanel>
        
    <!-- This component is used to display error messages on the page -->           
    <apex:pageMessages />
            
    <!-- This form tag must include any fields or buttons used in the Lead Convert operation -->
    <apex:form id="theForm" rendered="{!showForm}">
    
        <!-- This pageBlock component is a container that includes the visual elements of the Lead Convert page -->
        <!-- All custom Components should be included within this pageBlock unless you are using more advanced custom styling -->
        <apex:pageBlock mode="edit" id="pageBlock">
               
            <!-- This pageBlockButtons component is where you can add / remove custom buttons from the Lead Convert page -->   
            <apex:pageBlockButtons >      
                <apex:commandButton action="{!mergeContact}" value="Convert"/>
                <apex:commandButton action="{!cancel}" value="Cancel"/>               
            </apex:pageBlockButtons>  
            
            <!-- Lead Convert section -->    
            <apex:pageBlockSection id="pblockconvertLead" title="Convert Lead" collapsible="no" columns="1">
            
                <!-- Leads Record Owner -->
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Account Name" for="accountName" />
                    <apex:outputText value="{!bean.selectedAccountName}" id="accountName" />
                </apex:pageBlockSectionItem>
                     
                <apex:pageBlockSectionItem id="convertLeadContactBlockSectionItem" >
                    <!-- Contact Name picklist  -->
                    <apex:outputLabel value="Contact Name" for="contactList" />
                    <apex:outputPanel styleClass="requiredInput" layout="block" id="block">
                        <div class="requiredBlock"></div>
                        <apex:selectList value="{!bean.selectedContactId}" size="1" multiselect="false" id="contactList">  
                            <apex:actionSupport event="onchange" rerender="ViewLink" />  
                            <apex:selectOptions value="{!bean.contacts}"/>
                        </apex:selectList>
                        
                        <!-- This is a link which shows a view of the selected contact in a new pop up window -->                
                        <apex:commandLink id="ViewLink" value="View" onclick="javascript:if ('{!bean.selectedContactId}' != 'NEW' && '{!bean.selectedContactId}' != 'NONE' ) {var newWindow = window.open('/{!bean.selectedContactId}/p', 'contactview', 'top=40, left=40,scrollbars=yes, height=450, width=800');newwindow.focus();} else {alert('You can only view existing contacts.');}" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>        
            </apex:pageBlockSection>                
        
            <!-- Lead Convert section -->    
            <apex:pageBlockSection id="pblockoverwiteleadsource" title="Overwrite Lead Source" collapsible="no" columns="1">
                <!-- Checkbox indicating whether to send an email to the owner -->
                <apex:outputPanel >
                    <apex:inputCheckbox value="{!bean.overwriteLeadSource}" id="overwriteLeadSource" />
                    <apex:outputLabel value="Replace existing Lead Source with the value from this lead" for="overwriteLeadSource" />
                </apex:outputPanel>
            </apex:pageBlockSection>
        </apex:pageBlock>       
    </apex:form>  
</apex:page>