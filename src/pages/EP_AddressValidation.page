<!--
@Author <Ashok Arora>
@Name <EP_AddressValidation>
@CreateDate <02/5/2016>
@Description <This page is used for validating address details.
@Version <1.0>
 -->
<apex:page id="page"  controller="EP_AddressController"  docType="html-5.0" sidebar="true"  
            showHeader="true" standardStylesheets="false" action="{!fetchKey}">
    <!-- Defect : 46578 - Start -->
    <c:EP_PerformanceMonitor />
    <!-- Defect : 46578 - End -->
    <!-- <c:EP_PerformanceMonitor /> -->
    <apex:stylesheet value="{!URLFOR($Resource.EP_Visualstrap, 'css/visualstrap.css')}" id="visualstrapCSS"/>
    <apex:stylesheet value="{!URLFOR($Resource.EP_Visualstrap, 'css/visualstrap-common.css')}" id="vsCommonCSS"/>
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700"/>
    <style>
        .message.errorM3{
            margin : 0px; !Important
        }
    </style>
    <c:EP_VisualStrapBlock id="vBlock">
    <div id="errorBlock"></div> 
    <apex:outputPanel id="addrsForm">
        <apex:form id="form">
            <apex:outputPanel rendered="{!addressContext.showError}" styleClass="" layout="block" id="block">
                <apex:pagemessages id="pgmsgs"/><p></p>
            </apex:outputPanel>
            <div class="row" style="margin-bottom:10px">
                <div class="col-md-11">
                    <div class="form-group" style="margin-bottom:10px">
                        <input id="autocomplete" class="form-control" placeholder="Enter your address" onFocus="geolocate()" type="text"></input>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group" style="margin-bottom:10px">
                        <label for="InputStreet1">{!$Label.EP_Street_Number}</label>
                        <input type="text" class="form-control" id="street_number" placeholder="Street Number" disabled="true"/>
                    </div>
                    <div class="form-group">
                        <label for="InputStreet2">{!$Label.EP_Street_Name}</label>
                        <input type="text" class="form-control" id="route" placeholder="Route" disabled="true"/>
                    </div>
                    <div class="form-group">
                        <label for="InputCity">{!$Label.EP_City}</label>
                        <input type="text" class="form-control" id="locality" placeholder="City" disabled="true"/>
                    </div>
                    <div class="form-group">
                        <label for="InputState">{!$Label.EP_State}</label>
                        <input type="text" class="form-control" id="administrative_area_level_1" placeholder="State" disabled="true"/>
                    </div>
                    <div class="form-group">
                        <label for="InputCountry">{!$Label.EP_Country}</label>
                        <input type="text" class="form-control" id="country" placeholder="Country" disabled="true"/>
                    </div>
                    <div class="form-group">
                        <label for="InputZipCode">{!$Label.EP_Zip_Code}</label>
                        <input type="text" class="form-control" id="postal_code" placeholder="Zip Code" disabled="true"/>
                    </div>
                    <div class="form-group">
                        <label for="InputLat">{!$Label.EP_Latitude}</label>
                        <input type="text" class="form-control" id="latitude" placeholder="Latitude" disabled="true"/>
                    </div>
                    <div class="form-group">
                        <label for="InputLongt">{!$Label.EP_Longitude}</label>
                        <input type="text" class="form-control" id="longitude" placeholder="Longitude" disabled="true"/>
                    </div>
                    <div class="form-group">
                        <apex:inputhidden value="{!addressContext.address}" id="hiddenAdd"/>
                        <apex:commandButton value="{!$Label.EP_OK_Button}" action="{!updateAddress}" rerender="" id="okBtn"
                        onclick="if(event.keyCode==13){this.blur();actionFunction();}" styleclass="btn btn-success"
                        style="left:0px;position:relative;"/>
                        <apex:commandButton value="{!$Label.EP_Cancel_Label}" onclick="return(cancelWarn());" id="cancel"
                        action="{!cancel}" rerender="" styleclass="btn btn-default" style="left:10px;position:relative;"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div id="map" style="width:780px;height:600px;display:block;"> </div>
                </div>
            </div>
        </apex:form>
    </apex:outputPanel>
    
    <script src="../../soap/ajax/37.0/connection.js" type="text/javascript"></script>
    <script language="javascript">
        /* IE11 Fix for SP2010 */
        if (typeof(UserAgentInfo) != 'undefined' && !window.addEventListener) 
        {
            UserAgentInfo.strBrowser = 1; 
        } 
    </script>
    <script>
        var placeSearch, autocomplete, geocoder;
        var map, latlng, marker;
        var geoLat,geoLng;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };
        function initAutocomplete() {
            //ADDED BY ASHOK
            var cntrycode = '{!$CurrentPage.parameters.CntryCode}';
            var CR = '{!$CurrentPage.parameters.CR}';
            
            var code;
            if(cntrycode && cntrycode !== '' && cntrycode !== ' ' && (!CR || CR == 'No')){
                code = {'country':cntrycode};
            }
           
            // Create the autocomplete object, restricting the search to geographical location types
            autocomplete = new google.maps.places.Autocomplete(
                      (document.getElementById('autocomplete')),
                      {types: ['geocode']
                          });
            console.log('===code===='+code);
            console.log(autocomplete);
            if(code){
                autocomplete.setComponentRestrictions(code);
            }
            // Create a geocoder service
            geocoder = new google.maps.Geocoder();
            
            // When the user selects an address from the dropdown, populate the address fields in the form
            autocomplete.addListener('place_changed', fillInAddressFromAutocomplete);
            console.log(autocomplete);
            // Find running user current location
            //geolocate();
            
            // Initialise map
            initialiseMap();
            
            refreshMap();
        }
        function clearMarkers() {
            if (marker) {
                marker.setMap(null);
            }
        }
        function initialiseMap() {
            // Initialise the map if needed
            if (!map) {
                var mapCenter = {lat: 51.508742, lng: -0.120850};
                //var latPar = '{!$CurrentPage.parameters.lat}';
                //var longtPar = '{!$CurrentPage.parameters.longt}';
                var addPar = '{!$CurrentPage.parameters.address}';
                
                //ADDED BY ASHOK - ENDS
                var mapProp = {
                    center: mapCenter,
                    zoomControl: true,
                    mapTypeControl: true,
                    streetViewControl: true,
                    rotateControl: true,
                    fullscreenControl: true,
                    zoom:12,
                    mapTypeId:google.maps.MapTypeId.ROADMAP
                };
                
                map = new google.maps.Map(document.getElementById('map'), mapProp);
                // Add listeners
                google.maps.event.addListener(map, 'click', function(event) {
                    // Clear existing markers
                    clearMarkers();
                    // Place marker on map
                    placeMarker(event.latLng);
                    // Fill in address details
                    geocodePosition(event.latLng);
                });
                var addPar = '{!$CurrentPage.parameters.address}';
                    /*if(latPar && latPar !=='' && latPar !==' '  && longtPar !=='' && longtPar !==' '){*/
                   if(addPar && addPar!=='' && addPar!==' '){
                    //var location ={lat:Number(latPar) ,lng: Number(longtPar)};
                    var addPar = '{!$CurrentPage.parameters.address}';
                   //placeMarker(location) ;
                   //geocodePosition(location);
                    geoCodeAddress(addPar);
                    
                   
                }
                else{
                  
                    clearMarkers();
                    geolocate();
                }
                //google.maps.event.trigger(this.map, 'resize');         
            }
        }
        
        function refreshMap() {
            
            if (marker) {
                clearMarkers();
                placeMarker(marker.latLng);
            }
            
            google.maps.event.trigger(map, 'resize'); 

            center = map.getCenter();
            map.setCenter(center);     
            
        }
        
        function toggleSearchOnMap() {
            var mapDiv = document.getElementById('map');
            
            // Toggle the map depending on if it is visible or not
            if (map) {
                if (mapDiv.style.display == 'none') {
                    mapDiv.style.display = 'block';
                    refreshMap();
                } else {
                    mapDiv.style.display = 'none';                
                }
            }
        }
        
        function geocodePosition(pos) {
            if (geocoder) {
                geocoder.geocode({
                    latLng: pos
                    }, function(responses) {
                        if (responses && responses.length > 0) {
                            fillInAddress(responses[0]);
                        } else {
                            updateMarkerAddress('Cannot determine address at this location.');
                        }
                    });
            }
        }
        
        
        function geoCodeAddress(addPar) {
            var addcmpnt = '';
            var isLocality = false;
            geocoder.geocode( { 'address': addPar}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location,
                    label : 'A'   
                });
                
                for (var i = 0; i < results[0].address_components.length; i++) {
                var addressType = results[0].address_components[i].types[0];
                if(addressType === 'locality'){
                    isLocality = true;
                }
                if (componentForm[addressType]) {
                    
                    var val = results[0].address_components[i][componentForm[addressType]];
                    
                    document.getElementById(addressType).value = val;
                    addcmpnt += addressType + ':' + val + ',';
                    
                }
                else if(addressType === 'colloquial_area' && !isLocality){
                    var val1 = results[0].address_components[i][componentForm['locality']];
                    
                    document.getElementById('locality').value = val1;
                    addcmpnt += addressType + ':' + val1 + ',';
                }
            }
                
                document.getElementById('latitude').value = results[0].geometry.location.lat();
                document.getElementById('longitude').value = results[0].geometry.location.lng();
                addcmpnt += 'lat' + ':' + results[0].geometry.location.lat() + ',';
                addcmpnt += 'lng' + ':' + results[0].geometry.location.lng() + ',';
                geoLat = results[0].geometry.location.lat();
                geoLng = results[0].geometry.location.lng();
                document.getElementById('{!$Component.form.hiddenAdd}').value = addcmpnt;
                
              } else {
                var invalidLocMsg = '{!$Label.EP_Invalid_Address_Error_Message}';
                if(status == 'ZERO_RESULTS'){
                    status  = invalidLocMsg ;
                }
                
                var htmlContent = '<div class="alert alert-warning alert-dismissible" role="alert">'+
                                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                                  '<strong>Error!</strong>'+ status+
                                '</div>';
               document.getElementById('errorBlock').innerHTML= htmlContent ;
              }
            });
            
         }
        
        function fillInAddressFromAutocomplete() {
            // Get the place details from the autocomplete object
            
            var place = autocomplete.getPlace();
            
            fillInAddress(place);
        }
        
        function fillInAddress(place) {
            var addcomp = '';
            
            
            // Get each component of the address from the place details and fill the corresponding field on the form.
            if(place.address_components){
                for (var component in componentForm) {   
                    document.getElementById(component).value = '';
                    document.getElementById(component).disabled = true;
                    
                }
                
                
                document.getElementById('latitude').value = '';
                document.getElementById('longitude').value = '';
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    
                    if (componentForm[addressType]) {
                        
                        var val = place.address_components[i][componentForm[addressType]];
                        document.getElementById(addressType).value = val;
                        addcomp += addressType + ':' + val + ',';
                        
                    }
                }
                
                
                // Set marker and retrieve lan/lng
                if (place.geometry) {
                    // Clear existing markers
                    clearMarkers();    
                    placeMarker(place.geometry.location);
                    document.getElementById('latitude').value = place.geometry.location.lat();
                    addcomp += 'lat' + ':' + place.geometry.location.lat() + ',';
                    document.getElementById('longitude').value = place.geometry.location.lng();
                    addcomp += 'lng' + ':' + place.geometry.location.lng();
                }
                
                document.getElementById('{!$Component.form.hiddenAdd}').value = addcomp;
            }
            
        }
        
        function placeMarker(location) {
            if (map) {
                // Add new one
                marker = new google.maps.Marker({
                    map: map,
                    label :'N'
                });
                
                marker.setPosition(location);
         
                map.setCenter(location);
                
                
                document.getElementById('autocomplete').value = '';
            }
        }
        
        function geolocate() {
            
            if (navigator.geolocation) {
                var options = {
                    enableHighAccuracy: true,
                    maximumAge: 0
                };
                navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError, options);
            }
        }
        
        function geolocationSuccess(position) {
            document.getElementById('errorBlock').innerHTML= '';
            latlng = position.coords;
            
            // Bias the autocomplete object to the user geographical location
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            
            console.log(geolocation);
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            
            autocomplete.setBounds(circle.getBounds());
            
            // Check if map needs initialisation
            initialiseMap();
           //var latPar = '{!$CurrentPage.parameters.lat}';
            //var longtPar = '{!$CurrentPage.parameters.longt}';
            
            if(!(geoLng && geoLng !=='' && geoLng !==' ' && geoLat && geoLat !=='' && geoLat !==' ')){
                if (latlng) {                         
                    var userLocation = {lat: latlng.latitude, lng: latlng.longitude};
                    
                    map.setCenter(userLocation);
                    
                }
            }
            
            
            
        }
        
        function geolocationError(err) {
            // Check if map needs initialisation
            initialiseMap();
            console.log(err.message);
            var htmlContent = '<div class="alert alert-warning alert-dismissible" role="alert">'+
                                  '<strong>Warning!</strong> Unable to retrieve your current location.'+
                                '</div>';
            document.getElementById('errorBlock').innerHTML= htmlContent ;
            //alert(err.code + ' ' + err.message);
        }
        
        window.onload = function() {
            geolocate();
            document.getElementById("autocomplete").focus();
            sforce.connection.sessionId='{!GETSESSIONID()}';
            
        }
        
        
        function cancelWarn(){
            return confirm('Are you sure?');
        }
        function stopRKey(evt) 
        {
           var evt=(evt) ? evt : ((event) ? event : null);
           var node=(evt.target)?evt.target:((evt.srcElement)?evt.srcElement:null);
           if ((evt.keyCode == 13) && (node.type=="text")) {return false;}
        }

       document.onkeypress = stopRKey;
       
    </script>
    
     </c:EP_VisualStrapBlock>
     <!--CUSTOM LABEL CHANGE START-->
     <script src="{!$Label.EP_Map_API_URL}AIzaSyCLfYXePnOz0QhrFp4gPwwwIZAiYAQqHR8"></script><!--{!gKey}  -->
     <!--CUSTOM LABEL CHANGE END-->
</apex:page>