<apex:page showHeader="false" controller="EP_IntegrationErrorOnSidebarController" action="{!loadIntegrationFrameworkErrors}">

    <apex:stylesheet value="{!URLFOR($Resource.EP_IntegrationSidebarLib, 'style.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.EP_StyleLib, 'css/overlay.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.EP_StyleLib, 'css/table.css')}" />
    
    <c:EP_PerformanceMonitor />
    
    <script type="text/javascript">
        window.onload = function () { 
            if ('{!middlewareIntegrationErrorBoolean}' == 'true')
            {
                alert('{!$Label.EP_Middleware_Connectivity_Error}');
            }
        }
    </script>
    
    <apex:form >
        <!-- Actions -->
        <apex:actionFunction name="loadIntegrationFrameworkErrorsAction" status="statusOverlay" action="{!loadIntegrationFrameworkErrors}" reRender="error-table-panel,result-table-panel" />
        <apex:actionFunction name="dismissIntegrationErrorAction" status="statusOverlay" action="{!dismissIntegrationError}" oncomplete="loadIntegrationFrameworkErrorsAction()">
            <apex:param name="param1" value="" assignTo="{!selectedIntegrationRecordString}"/>
        </apex:actionFunction>
        
        <apex:actionStatus id="statusOverlay" startStyleClass="overlay"></apex:actionStatus> 
        
        <div>
            <div>
                <apex:outputPanel id="error-table-panel">
                    <apex:outputPanel rendered="{!(listTransportIntegrationErrors.size == 0 && listFunctionalIntegrationErrors.size == 0)}">
                        <div style="text-align: center;">
                            <apex:image value="{!URLFOR($Resource.EP_ImageLib, 'ok-48.png')}"/>
                            <br/><br/>
                            {!$Label.EP_No_Warnings_Label}
                            <br/><br/>
                            <apex:outputLabel rendered="{!(openMyErrorListView != NULL && totalNumberOfIntegrationErrors > 0)}">{!$Label.EP_Click_Error_Link}</apex:outputLabel>
                        </div>
                    </apex:outputPanel>
                    <apex:outputPanel rendered="{!(listTransportIntegrationErrors.size > 0 || listFunctionalIntegrationErrors.size > 0)}" >
                        <table style="height:180px; overflow-y: auto; display: block;">
                            <apex:repeat value="{!listTransportIntegrationErrors}" var="ir">
                                <tr>
                                    <td style="width: 10%;vertical-align:top;"><apex:image value="{!URLFOR($Resource.EP_IntegrationSidebarLib, '/img/essential_set_connectivity-16.png')}"/></td>
                                    <td style="max-width: 50px; word-wrap: break-word; TEXT-OVERFLOW: ellipsis;"><apex:outputLink value="{!ir.prRecordLink}" target="_blank">{!ir.objectNameString} {!ir.strRecordName}</apex:outputLink>:&nbsp;{!ir.strErrorDescription}</td>
                                    <td style="width: 10%;">
                                        <apex:outputLink onclick="dismissIntegrationErrorAction('{!ir.strRecordID}'); return false;" >
                                            <apex:image value="{!URLFOR($Resource.EP_IntegrationSidebarLib, '/img/essential_set_close-16.png')}"/>
                                        </apex:outputLink>
                                    </td>
                                </tr>
                            </apex:repeat>
                            <apex:repeat value="{!listFunctionalIntegrationErrors}" var="ir">
                                <tr>
                                    <td style="width: 10%;vertical-align:top;"><apex:image value="{!URLFOR($Resource.EP_IntegrationSidebarLib, '/img/essential_set_warning-16.png')}"/></td>
                                    <td style="max-width: 50px; word-wrap: break-word; TEXT-OVERFLOW: ellipsis;">
                                        {!ir.objectNameString}&nbsp;<apex:outputLink value="{!ir.prSourceRecordLink}" target="_blank"><b>{!ir.strRecordName}</b></apex:outputLink>:&nbsp;<apex:outputLink value="{!ir.prRecordLink}" target="_blank">{!ir.strErrorDescription}</apex:outputLink>
                                    </td>
                                    <td style="width: 10%;">
                                        <apex:outputLink onclick="dismissIntegrationErrorAction('{!ir.strRecordID}'); return false;" >
                                            <apex:image value="{!URLFOR($Resource.EP_IntegrationSidebarLib, '/img/essential_set_close-16.png')}"/>
                                        </apex:outputLink>
                                    </td>
                                </tr>
                            </apex:repeat>
                        </table>
                    </apex:outputPanel>
                </apex:outputPanel>
            </div>
            
        </div>
        <!--id="div-footer"-->
        <div >
            <apex:outputPanel id="result-table-panel">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align:left;">{!$Label.EP_Showing_Label}&nbsp;{!numberOfDisplayedIntegrationErrors}&nbsp;/&nbsp;{!totalNumberOfIntegrationErrors}</td>
                        <td style="text-align:right;"><apex:commandLink value="{!$Label.EP_All_Errors_Label}" onclick="top.location='{!openMyErrorListView}'; return false;" rendered="{!(openMyErrorListView != NULL && totalNumberOfIntegrationErrors > 0)}" target="_blank" /></td>
                    </tr>
                </table>
            </apex:outputPanel>
        </div>
    </apex:form>
</apex:page>