<apex:page standardController="Lead" extensions="EP_CRM_LeadConvertController">
    <!-- This allows for the lookup to Account but hides the text of the selected account name leaving the magnifying glass only -->
    <style type="text/css">
        input.hiddenInput {
            width:0;
            height:0;
            border:0;
            padding:0;
            margin:0;
        }
    </style>
    
    <apex:outputPanel rendered="{!showForm}">        
        <!-- Page section header -->
        <apex:sectionHeader title="Convert Lead" subtitle="{!Lead.Name}" />    
        <!-- Page Header Text -->
        Leads can be converted to accounts, contacts, opportunities, and followup tasks.<br/>
        You should only convert a lead once you have identified it as qualified.<br/>
        After this lead has been converted, it can no longer be viewed or edited as a lead, but can be viewed in lead reports.<br/><br/>    
    </apex:outputPanel>
    
    <!-- This component is used to display error messages on the page -->           
    <apex:pageMessages />
    
    <!-- This form tag must include any fields or buttons used in the Lead Convert operation -->
    <apex:form id="theForm" rendered="{!showForm}">
    
        <!-- This pageBlock component is a container that includes the visual elements of the Lead Convert page -->
        <!-- All custom Components should be included within this pageBlock unless you are using more advanced custom styling -->
        <apex:pageBlock mode="edit" id="pageBlock">
               
            <!-- This pageBlockButtons component is where you can add / remove custom buttons from the Lead Convert page -->   
            <apex:pageBlockButtons >      
                <apex:commandButton action="{!convertLead}" value="Convert"/>
                <apex:commandButton action="{!cancel}" value="Cancel"/>               
            </apex:pageBlockButtons>                  
        
            <!-- Lead Convert section -->    
            <apex:pageBlockSection id="pblockconvertLead" title="Convert Lead" collapsible="no" columns="1">
                <!-- Leads Record Owner -->
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Record Owner" for="recordOwner" />
                    <apex:inputField value="{!bean.contactObj.ownerID}" id="recordOwner" />
                </apex:pageBlockSectionItem>
                
                <!-- Checkbox indicating whether to send an email to the owner -->    
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Send Email to the Owner" for="sendOwnerEmail" />
                    <apex:inputCheckbox value="{!bean.sendOwnerEmail}" id="sendOwnerEmail" />
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem id="convertLeadAccountBlockSectionItem" >
                    <!-- Account Name picklist  -->
                    <apex:outputLabel value="Account Name" for="accountList" />
                    <apex:outputPanel styleClass="requiredInput" layout="block" id="block">
                        <div class="requiredBlock"></div>
                        <apex:selectList value="{!bean.selectedAccountId}" size="1" multiselect="false" id="accountList">
                            <apex:actionSupport event="onchange" action="{!bean.accountChanged}" rerender="opportunityName, ViewLink" />     
                            <apex:selectOptions value="{!bean.accounts}"/>
                        </apex:selectList>
                        
                        <!--  Account lookup to add existing accounts to the picklist -->
                        <apex:inputField styleClass="hiddenInput" value="{!bean.contactObj.AccountId}" id="accountId">
                            <!-- When an account is looked up, it is added to the select list -->                            
                            <apex:actionSupport event="onchange" action="{!bean.accountLookedUp}" rerender="accountList, opportunityName, ViewLink" />
                        </apex:inputField>
                        
                        <!-- This is a link which shows a view of the selected account in a new pop up window -->                
                        <apex:commandLink id="ViewLink"  value="View" onclick="javascript:if ('{!bean.selectedAccountId}' != 'NEW' && '{!bean.selectedAccountId}' != 'NONE' ) {var newWindow = window.open('/{!bean.selectedAccountId}/p', 'accountview', 'top=40, left=40,scrollbars=yes, height=450, width=800');newwindow.focus();} else {alert('You can only view existing accounts.');}" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <!-- Opportunity section -->
                <apex:pageBlockSectionItem id="oppSection" >
                    
                    <apex:outputPanel id="oppPanel" >
                        <apex:outputLabel value="Opportunity Name" for="opportunityName" />
                    </apex:outputPanel>
                    
                    <apex:outputPanel id="opportunityPanel" layout="block" styleClass="requiredInput">
                        <apex:outputPanel rendered="{!NOT(bean.doNotCreateOppty)}" id="block">
                            <div class="requiredBlock"></div>
                            <!-- Opportunity Name -->
                            <apex:inputField required="false" value="{!bean.opportunityObj.Name}" id="opportunityName"/>
                        </apex:outputPanel>
                            
                        <!--  This is only shown if the Do Not Create Opportunity is true -->    
                        <apex:inputText disabled="true" rendered="{!bean.doNotCreateOppty}" value="{!bean.opportunityObj.Name}" id="opportunityNameDisabled"/>
                        <apex:outputPanel layout="block" styleClass="requiredInput">
                            <apex:outputPanel layout="block" />
                            <!-- Checkbox indicating whether to create an Opportunity -->
                            <apex:inputCheckbox value="{!bean.doNotCreateOppty}" id="doNotCreateOppty">
                                <apex:actionSupport event="onchange" rerender="opportunityPanel" />
                            </apex:inputCheckbox>
                            Do not create a new opportunity upon conversion.
                        </apex:outputPanel> 
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <!-- Converted Status -->
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Converted Status" for="leadStatus" />
                    <apex:outputPanel styleClass="requiredInput" layout="block">
                        <div class="requiredBlock"></div>
                        <apex:selectList id="LeadStatusList" size="1" 
                            value="{!bean.LeadToConvert.Status}" multiselect="false" required="true">
                            <apex:selectOptions value="{!bean.LeadStatusOption}"/>
                        </apex:selectList> 
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
            </apex:pageBlockSection>
        </apex:pageBlock>       
    </apex:form>  
</apex:page>