/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationProspectToProspect>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 01-Prospect to 01-Prospect>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationProspectToProspect extends EP_AccountStateTransition {

    public EP_ASTStorageLocationProspectToProspect () {
        finalState = EP_AccountConstant.PROSPECT;
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationProspectToProspect',' isGuardCondition');        
        return true;
    }
}