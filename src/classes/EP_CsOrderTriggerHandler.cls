/*
@Author      CloudSense
@name        EP_CsOrderTriggerHandler
@CreateDate  11/05/2018
@Description Handler for the Trigger: EP_CsOrderTrigger
@Version     1.0
*/
public with sharing class EP_CsOrderTriggerHandler {

	public static void doAfterUpdate(Map<Id,csord__Order__c> orderMap){
		CSPOFA.Events.emit('update', orderMap.keySet());
	}
	
	public static void handleInvoiceCleanup(List<csord__Order__c> newList, Map<Id,csord__Order__c> oldMap){
		Map<String, Boolean> statesMap = new Map<String, Boolean>{
			EP_Common_Constant.Credit => true,
			EP_Common_Constant.COD => true
		};

		Set<Id> orderIdStateChangedSet = new Set<Id>();

		for(csord__Order__c newOrder : newList){
			if(statesMap.containsKey(newOrder.EP_Payment_Method_Name__c)
				&& newOrder.csord__Status2__c != 'draft'
				&& oldMap.containsKey(newOrder.Id)
				&& oldMap.get(newOrder.Id).EP_Payment_Method_Name__c != newOrder.EP_Payment_Method_Name__c){

				//If this order has an invoice, it should be removed
				orderIdStateChangedSet.add(newOrder.Id);
			}
		}

		if (!orderIdStateChangedSet.isEmpty()){
			//get attachments that are invoices
			List<Attachment> invoiceList = [SELECT Id FROM Attachment where Name = 'proformainvoice.pdf' and ParentId = :orderIdStateChangedSet];

			if (!invoiceList.isEmpty()){
				DELETE invoiceList;
			}
		}
	}
	
}