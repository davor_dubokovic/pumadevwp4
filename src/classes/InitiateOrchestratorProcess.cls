/*
   @Author          CloudSense
   @Name            InitiateOrchestratorProcess
   @CreateDate      02/07/2017
   @Description     This class is responsible for creating the Orchestrator process for Order after Submit
   @Version         1.1
 
*/
Public class InitiateOrchestratorProcess {
   
    // fetch Custom setting
    public static CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getOrgDefaults();
    /**
    * @Author       CloudSense
    * @Name         execute
    * @Date         02/09/2017
    * @Description  Method to Create Orchestrator process for the Order
    * @Param        list<OrderIds>
    * @return       NA
    */   
    
    @InvocableMethod
    public static void execute(List<Id> OrderIds){

        system.debug('OrderId List is :' +OrderIds);
        List<CSPOFA__Orchestration_Process__c> orderCreationProcessList = new List<CSPOFA__Orchestration_Process__c> ();
        List<CSPOFA__Orchestration_Process__c> newProcessList = new List<CSPOFA__Orchestration_Process__c> ();
        List<CSPOFA__Orchestration_Process__c> processesList;
        
        try {
            
                List<csord__Order__c> orders = [ SELECT ID, 
                                                    csord__Order_Number__c,
                                                    csord__Status2__c, 
                                                    EP_Is_Credit_Exception_Cancelled_Sync__c, 
                                                    Cancellation_Check_Done__c,
                                                    EP_Sync_with_NAV__c,
                                                    Sync_with_WINDMS__c,
                                                    RecordTypeId
                                                FROM csord__Order__c
                                                WHERE ID in :orderIds ];
                                              
            //fetch Custom setting
            CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();    
            set<String> OrchestrationProcessTemplateId = new set<String>{
                csOrderSetting.Orchestrator_Process_TemplateId__c,
                csOrderSetting.Orchestrator_Modify_Template__c }; 
                                      
            System.debug('Orders list is :' +orders);
            //Process List with Create and Modify Template
            processesList = [SELECT Id, Order__c FROM CSPOFA__Orchestration_Process__c WHERE Order__c =: orders[0].Id and CSPOFA__Orchestration_Process_Template__c in :OrchestrationProcessTemplateId];
            
            //Process List with Cancellation Template
            List<CSPOFA__Orchestration_Process__c> processesListCancel = [SELECT Id, Order__c FROM CSPOFA__Orchestration_Process__c WHERE Order__c =: orders[0].Id and CSPOFA__Orchestration_Process_Template__c =:csOrderSetting.Orchestrator_Cancel_Template__c]; 
            
            if (!orders.isEmpty() && orders[0].Cancellation_Check_Done__c && !processesList.isEmpty()) {
                delete processesList;
            }
            System.Debug('-->'+processesList);
          
            if(!orders.isEmpty()  && processesList.size() == 0 && processesListCancel.isEmpty()){
                orderCreationProcessList = createNewOrderProcesses(orders);
                newProcessList.addall(orderCreationProcessList);
            }
            
            
            //Delete Process Start                   
            if (!orders.isEmpty() && orders[0].Cancellation_Check_Done__c && processesListCancel.isEmpty()) {
                orderCreationProcessList = createNewOrderProcesses(orders);
                newProcessList.addall(orderCreationProcessList);
            }
            //Delete Process End
            
            
            system.debug('newProcessList list is :' +newProcessList);
            if(!newProcessList.isEmpty()) {
                upsert newProcessList;
            }
            //}
        }Catch(Exception exp){
                    system.debug('pep ' + exp);
             EP_loggingService.loghandledException(exp, EP_Common_Constant.EPUMA, 'execute', 'InitiateOrchestratorProcess',apexPages.severity.ERROR);
    }
}
    
    /**
    * @Author       CloudSense
    * @Name         createNewOrderProcesses
    * @Date         02/09/2017
    * @Description  Method to Set the Orchestrator Process fields
    * @Param        list<Order>
    * @return       NA
    */ 
  
    @TestVisible
    private  static List<CSPOFA__Orchestration_Process__c> createNewOrderProcesses(List<csord__Order__c> orders) {
        String processTemplateId;
        String contractRecordTypeId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Contract').getRecordTypeId();
        for(csord__Order__c order : orders){
             system.debug('order.Cancellation_Check_Done__c' +order.Cancellation_Check_Done__c);
              
            if((order.EP_Sync_with_NAV__c == true || order.Sync_with_WINDMS__c == true) && order.Cancellation_Check_Done__c == false){
                system.debug('Inside If');
                processTemplateId = csOrderSetting.Orchestrator_Modify_Template__c;
                 
            } else if (order.Cancellation_Check_Done__c == true){
                system.debug('Inside cancel');
                processTemplateId = csOrderSetting.Orchestrator_Cancel_Template__c;
            } else{
                system.debug('Inside create');

                if(order.RecordTypeId == contractRecordTypeId){
                    //Contract Order
                    processTemplateId = csOrderSetting.Contract_Orchestrator_Process_TemplateId__c;
                }else{
                    processTemplateId = csOrderSetting.Orchestrator_Process_TemplateId__c;
                }  
            }
        }
        system.debug('processTemplateId is :' +processTemplateId);
        List<CSPOFA__Orchestration_Process__c> processes = new List<CSPOFA__Orchestration_Process__c>();   
        for (csord__Order__c order : orders) {
            CSPOFA__Orchestration_Process__c process = new CSPOFA__Orchestration_Process__c();
            process.Order__c = order.Id;
            process.Name ='Orchestration_Process_Order_Num:' + order.csord__Order_Number__c;
            process.CSPOFA__Orchestration_Process_Template__c = processTemplateId;
            process.CSPOFA__Processing_Mode__c = 'Foreground';
            system.debug('process is :' +process);
            processes.add(process);
        }                                                                
        return processes;
    }

}