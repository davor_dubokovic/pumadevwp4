/* 
@Author      Accenture
@name        EP_SEP_AccountServiceellTo
@CreateDate  21/12/2016
@Description Service class for Account Object
@Version     1.0
*/
public with sharing class EP_AccountService{ 

    EP_AccountDomainObject accountDomainObj;
    EP_AccountType accountType;
    Account oldAccount;
    Account account;
    EP_AccountFactory accountFactory;
    
   /**
    * @author       Accenture
    * @name         EP_AccountService
    * @date         02/05/2017
    * @description  Constructor for setting accountDomain and initilizing accountory, accounttype object instance
    * @param        EP_AccountDomainObject
    * @return       NA
    */  
    public EP_AccountService(EP_AccountDomainObject accountDomainObject){
        this.accountDomainObj = accountDomainObject;
        this.account = accountDomainObject.getAccount();
        this.oldAccount = accountDomainObject.getOldRecord();
        this.accountFactory = new EP_AccountFactory(accountDomainObject);
        this.accountType = accountFactory.getAccountType(); 
        //Bulkification Implementation -- Start
        this.accountType.accountDomainObj = accountDomainObject;        
        //Bulkification Implementation -- End
    }
   
   /**
    * @author       Accenture
    * @name         doBeforeInsertHandle
    * @date         02/05/2017
    * @description  method to handle logic for all type of account on before insert event
    * @param        NA
    * @return       NA
    */  
    public void doBeforeInsertHandle(){
        EP_GeneralUtility.Log('Public','EP_AccountService','doBeforeInsertHandle');
        accountType.doBeforeInsertHandle(account);
        setOwnerAlias();
    }
    
    /**
    * @author       Accenture
    * @name         doAfterInsertHandle
    * @date         02/05/2017
    * @description  method to handle logic for all type of account on after insert event
    * @param        NA
    * @return       NA
    */
    public void doAfterInsertHandle(){
        EP_GeneralUtility.Log('Public','EP_AccountService','doAfterInsertHandle');
        accountType.doAfterInsertHandle(account);
    }
    
    /**
    * @author       Accenture
    * @name         doBeforeUpdateHandle
    * @date         02/05/2017
    * @description  method to handle logic for all type of account on before update event
    * @param        NA
    * @return       NA
    */
    public void doBeforeUpdateHandle(){
        EP_GeneralUtility.Log('Public','EP_AccountService','doBeforeUpdateHandle');
        accountType.dobeforeUpdateHandle(account, oldAccount);  
        setAddressInfo();
        // Changes made for CUSTOMER MODIFICATION L4 Start 
        resetIntegrationFlagsForActive();  
        // Changes made for CUSTOMER MODIFICATION L4 End
        //Code Changes for #45361 and #45362 Start
        resetIntegrationFlagsForBlockUnblock();  
        resetIntegrationFlagsForDeactivate();
         
        //Code Changes for #45361 and #45362 End
        if(isOwnerChanged()){ setOwnerAlias(); }
    }

    // Changes made for CUSTOMER MODIFICATION L4 Start
     public void resetIntegrationFlagsForActive(){

        if (accountDomainObj.isUpdatedFromNAV() && accountDomainObj.localAccount.EP_Status__c == EP_Common_Constant.STATUS_ACTIVE) {
            accountDomainObj.localAccount.EP_Synced_PE__c = false;
            accountDomainObj.localAccount.EP_Synced_NAV__c = true;
            accountDomainObj.localAccount.EP_Synced_winDMS__c = false;
        }
        else if(!accountDomainObj.IsChangeIntegrationStatus() && accountDomainObj.localAccount.EP_Status__c == EP_Common_Constant.STATUS_ACTIVE){
            accountDomainObj.localAccount.EP_Synced_PE__c = false;
            accountDomainObj.localAccount.EP_Synced_NAV__c = false;
            accountDomainObj.localAccount.EP_Synced_winDMS__c = false;
        }  
    }
    // Changes made for CUSTOMER MODIFICATION L4 End
    private boolean isIntegrationStatusChange(){
        return ((oldAccount!=null)?account.EP_Integration_Status__c != oldAccount.EP_Integration_Status__c:false);
    }
    
    
    /**
    * @author       Accenture
    * @name         doAfterUpdateHandle
    * @date         02/05/2017
    * @description  method to handle logic for all type of account on after update event
    * @param        NA
    * @return       NA
    */  
    public void doAfterUpdateHandle(){
        EP_GeneralUtility.Log('Public','EP_AccountService','doAfterUpdateHandle');
        accountType.doAfterUpdateHandle(account, oldAccount);
    }
    //# L4- 45526 Start
    /**
    * @author       Accenture
    * @name         doBeforeDeleteHandle
    * @date         02/05/2017
    * @description  method to handle logic for all type of account on before delete event
    * @param        NA
    * @return       NA
    */  
    public void doBeforeDeleteHandle(){
        EP_GeneralUtility.Log('Public','EP_AccountService','doBeforeDeleteHandle');
        accountType.doBeforeDeleteHandle(account);
    }
    //# L4- 45526 End
     
   /**
    * @author       Accenture
    * @name         isOwnerChanged
    * @date         02/05/2017
    * @description  Returns true if owner is updating
    * @param        NA
    * @return       NA
    */ 
    @TestVisible
    private boolean isOwnerChanged(){
        EP_GeneralUtility.Log('Private','EP_AccountService','isOwnerChanged');
        return (account.OwnerId != oldAccount.ownerId);
    }
    
    
   /**
    * @author       Accenture
    * @name         setOwnerAlias
    * @date         02/05/2017
    * @description  This method populates Sell-To OwNer Alias value in  Sell-To's EP_Cstmr_Owner_Alias__c field
    * @param        NA
    * @return       NA
    */  
    @TestVisible
    private void setOwnerAlias(){
        EP_GeneralUtility.Log('Private','EP_AccountService','setOwnerAlias');
        EP_UserMapper userMapperObj = new EP_UserMapper();
        List<User> userList = userMapperObj.getRecordsByIds(new Set<Id>{account.OwnerId});
        if(!userList.isEmpty()){
            account.EP_Cstmr_Owner_Alias__c = userList[0].Alias;
        }
    }
    
   /**
    * @author       Accenture
    * @name         isProductListAttached
    * @date         02/05/2017
    * @description  This method returns true if product list is attached to customer (Service for sellTo)
    * @param        NA
    * @return       Boolean
    */  
    /*L4_45352_Start - Deprecating this method as it is no longer required
    public boolean isProductListAttached(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isProductListAttached');
        return accountType.isProductListAttached(account);
    }
    L4_45352_End*/
    
    /**
    * @author       Accenture
    * @name         isCountryChange
    * @date         02/05/2017
    * @description  This method returns true if country has changed
    * @param        NA
    * @return       Boolean
    */
    @TestVisible
    private boolean isCountryChange(){
        EP_GeneralUtility.Log('Private','EP_AccountService','isCountryChange');
        return ((account.billingCountry != oldAccount.billingCountry) || (account.ShippingCountry != oldAccount.ShippingCountry));
    }
       
    /**
    * @author       Accenture
    * @name         isOverDueInvoice
    * @date         02/05/2017
    * @description  This method is used check if there are any overdue invoices for customer
    * @param        NA
    * @return       Boolean
    */   
    public boolean isOverDueInvoice(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isOverDueInvoice');
        return accountType.validateOverdueInvoice(account);
    }
    
   /**
    * @author       Accenture
    * @name         isBankAccountRequired
    * @date         02/20/2017
    * @description  This method is used check if bank account is required for customer
    * @param        NA
    * @return       Boolean
    */   
    public boolean isBankAccountRequired(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isBankAccountRequired');
        return accountType.isBankAccountRequired(account);
    }
    
   /**
    * @author       Accenture
    * @name         isProductListLinkedWithDifferentSellTo
    * @date         02/20/2017
    * @description  This method returns true if product list attached to customer is linked to any other customer
    * @param        NA
    * @return       Boolean
    */ 
    /*L4_45352_Start - Deprecating this method as it is no longer required
    public boolean isProductListLinkedWithDifferentSellTo(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isProductListLinkedWithDifferentSellTo');
        return accountType.isProductListLinkedWithDifferentSellTo(account);
    }
    L4_45352_End*/
    
   /**
    * @author       Accenture
    * @name         isCompanySameOnProducts
    * @date         02/20/2017
    * @description  This method is used check that Company on Producs added in Product List and Comapny on Product List is same
    * @param        NA
    * @return       Boolean
    */ 
    /*L4_45352_Start - Deprecating this method as it is no longer required
    public boolean isCompanySameOnProducts(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isCompanySameOnProducts');
        return accountType.hasSameCompanyOnProducts(account);
    }
     L4_45352_End*/
    /**
    * @author       Accenture
    * @name         setAddressInfo
    * @date         02/20/2017
    * @description  This method set EP_Is_Valid_Address__c to false. Populate country look also
    * @param        NA
    * @return       Boolean
    */ 
    
    @TestVisible
    private void setAddressInfo(){
        EP_GeneralUtility.Log('Private','EP_AccountService','setAddressInfo');
        if(isAddressChange()){
            account.EP_Is_Valid_Address__c = EP_Common_Util.isValidAddress;
            if(isCountryChange()){
                accountType.setCountryLookup(account);
            }   
        }
    }
    

   /**
    * @author       Accenture
    * @name         isAddressChange
    * @date         02/20/2017
    * @description  This method returns true if address is changed
    * @param        NA
    * @return       Boolean
    */ 
    @TestVisible
    private boolean isAddressChange(){
        EP_GeneralUtility.Log('Private','EP_AccountService','isAddressChange');
        return (account.billingStreet != oldAccount.billingStreet
        || account.billingCity != oldAccount.billingCity
        || account.billingState != oldAccount.billingState
        || account.billingCountry != oldAccount.billingCountry
        || account.billingPostalCode != oldAccount.billingPostalCode
        || account.ShippingStreet != oldAccount.ShippingStreet
        || account.ShippingCity != oldAccount.ShippingCity
        || account.ShippingState != oldAccount.ShippingState
        || account.ShippingCountry != oldAccount.ShippingCountry
        || account.ShippingPostalCode != oldAccount.ShippingPostalCode);
        
    }
    
    /**
    * @author       Accenture
    * @name         isCompanySameOnProductList
    * @date         02/20/2017
    * @description  This method is used check that Product List and Customer has same company
    * @param        NA
    * @return       Boolean
    */
    /*L4_45352_Start - Deprecating this method as it is no longer required
    public boolean isCompanySameOnProductList(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isCompanySameOnProductList');
        return accountType.hasSameCompanyOnProductList(account);
    }
    L4_45352_End*/
    
    /**
    * @author       Accenture
    * @name         isSupplyOptionAttached
    * @date         02/20/2017
    * @description  This method is used check that supply option is attached to customer
    * @param        NA
    * @return       Boolean
    */
    public boolean isSupplyOptionAttached(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isSupplyOptionAttached');
        return accountType.isSupplyOptionAttached(account);
    }
    
    /**
    * @author       Accenture
    * @name         doActionSyncCustomerToPricingEngine
    * @date         02/20/2017
    * @description  This method is used check that supply option is attached to customer
    * @param        NA
    * @return       NA
    */
    public void doActionSyncCustomerToPricingEngine(){
        EP_GeneralUtility.Log('Public','EP_AccountService','doActionSyncCustomerToPricingEngine');
        accountType.syncCustomerToPricingEngine(account);
    }
    
    /**
    * @author       Accenture
    * @name         isSetUpShipToAssociated
    * @date         02/20/2017
    * @description  This method is used check if a sell To has ship to associated whose status is Account Set-up
    * @param        NA
    * @return       Boolean
    */
    public boolean isSetUpShipToAssociated(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isSetUpShipToAssociated');
        return accountType.hasSetUpShipTo(account);
        
    }

   /**
    * @author       Accenture
    * @name         isDirectDebitPayment
    * @date         02/20/2017
    * @description  This method is used to check if payment method is of type Direct Debit
    * @param        NA
    * @return       Boolean
    */
    public boolean isDirectDebitPayment(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isDirectDebitPayment');
        return accountType.isDirectDebitPayment(account);
        
    }
    
   /**
    * @author       Accenture
    * @name         doActionSendCreateRequestToNavAndLomo
    * @date         02/20/2017
    * @description  This method is used to send customer create request to NAV and Lomosoft
    * @param        NA
    * @return       NA
    */
    public void doActionSendCreateRequestToNavAndLomo(){
        EP_GeneralUtility.Log('Public','EP_AccountService','doActionSendCreateRequestToNavAndLomo');
        accountType.sendCustomerCreateRequest(account);
    }
    
    /**
    * @author       Accenture
    * @name         doActionsendUpdateRequestToNavAndLomo
    * @date         02/20/2017
    * @description  This method is used to send customer update request to NAV and Lomosoft
    * @param        NA
    * @return       NA
    */
    public void doActionsendUpdateRequestToNavAndLomo(){
        EP_GeneralUtility.Log('Public','EP_AccountService','doActionsendUpdateRequestToNavAndLomo');
        /**** DEFECT-57217 Fix Start ***/
        if(!EP_IntegrationUtil.isCallout && !system.isBatch() && !isIntegrationStatusChange() && !account.EP_Is_Dummy__c){
            accountType.sendCustomerEditRequest(account);
        }
        /**** DEFECT-57217 Fix END ***/
    }

    /**
    * @author       Accenture
    * @name         getBillTo
    * @date         02/20/2017
    * @description  This method to get bill to from customer
    * @param        NA
    * @return       Account
    */
    public Account getBillTo(){
        EP_GeneralUtility.Log('Public','EP_AccountService','getBillTo');
        return accountType.getBillTo(account);
    }
    
    /**
    * @author       Accenture
    * @name         getSellTo
    * @date         02/20/2017
    * @description  This method to get sell To
    * @param        NA
    * @return       Account
    */
    public Account getSellTo(){
        EP_GeneralUtility.Log('Public','EP_AccountService','getSellTo');
        return accountType.getSellTo(account);
    }
    
    /**
    * @author       Accenture
    * @name         isBillToAttachedToCustomer
    * @date         02/20/2017
    * @description  This method to check if customer has bill to
    * @param        NA
    * @return       Account
    */
    public Boolean isBillToAttachedToCustomer(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isBillToAttachedToCustomer');
        return accountType.hasBillTo(account);
    }
    
    /**
    * @author       Accenture
    * @name         getSupplyLocations
    * @date         02/20/2017
    * @description  This method to get stock holding locations
    * @param        NA
    * @return       List<EP_Stock_Holding_Location__c>
    */
    public List<EP_Stock_Holding_Location__c> getSupplyLocations() {
        EP_GeneralUtility.Log('Public','EP_AccountService','getSupplyLocations');
        return accountType.getSupplyLocationList(account.Id);
    }
    
   /**
    * @author       Accenture
    * @name         isKYCReviewRejected
    * @date         02/20/2017
    * @description  This method returns true If KYC review is rejected.
    * @param        NA
    * @return       Boolean
    */
    public boolean isKYCReviewRejected(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isKYCReviewRejected');
        return accountType.isKYCReviewRejected(account);  
    }
    
    /**
    * @author       Accenture
    * @name         setCustomerActivationDate
    * @date         02/20/2017
    * @description  This method to set customer activation date on customer
    * @param        NA
    * @return       NA
    */
    public void setCustomerActivationDate(){
        EP_GeneralUtility.Log('Public','EP_AccountService','setCustomerActivationDate');
        accountType.setCustomerActivationDate(account);  
    }
    
    /**
    * @author       Accenture
    * @name         getInvoicingCustomer
    * @date         02/20/2017
    * @description  This method returns the account who should be invoiced for an order
    * @param        NA
    * @return       Account
    */
    public Account getInvoicingCustomer(){
        EP_GeneralUtility.Log('Public','EP_AccountService','getInvoicingCustomer');
        return this.isBillToAttachedToCustomer() ? this.getBillTo() : this.getSellTo();
    }
   
    /**
    * @author       Accenture
    * @name         isCompletedAllReviewActions
    * @date         02/20/2017
    * @description  Method to check if all reviews on shipto are completed or not
    * @param        NA
    * @return       Boolean
    */
    public boolean isCompletedAllReviewActions(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isCompletedAllReviewActions');
        return accountType.hasCompletedReviewActions(account);
    }   

    /**
    * @author       Accenture
    * @name         isPrimarySupplyOptionAttached
    * @date         02/20/2017
    * @description  Method to check if supply option is attached to a ship to
    * @param        NA
    * @return       Boolean
    */
    public boolean isPrimarySupplyOptionAttached(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isPrimarySupplyOptionAttached');
        return accountType.isPrimarySupplyOptionAttached(account);  
    }
    
    /**
    * @author       Accenture
    * @name         doActionInsertPlaceHolderTankDips
    * @date         02/20/2017
    * @description  Method to insert place holder tank dips to for a ship To
    * @param        NA
    * @return       Boolean
    */
    public void doActionInsertPlaceHolderTankDips(){
        EP_GeneralUtility.Log('Public','EP_AccountService','doActionInsertPlaceHolderTankDips');
        accountType.insertPlaceHolderTankDips(account);
    }
    
    /**
    * @author       Accenture
    * @name         doActionDeletePlaceHolderTankDips
    * @date         02/20/2017
    * @description  Method to delete place holder tank dips to for a ship To
    * @param        NA
    * @return       NA
    */
    public void doActionDeletePlaceHolderTankDips(){
        EP_GeneralUtility.Log('Public','EP_AccountService','doActionDeletePlaceHolderTankDips');
        accountType.deletePlaceHolderTankDips(account);
    }
    
    
    /**
    * @author       Accenture
    * @name         isShipToActive
    * @date         02/05/2017
    * @description  This method returns false , if there is any shipto with Error-Sync/Failure Integration Status
    * @param        NA
    * @return       boolean 
    */
    public boolean isShipToActive(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isShipToActive');
        return accountType.isShipToActive(account);
    }
    
    /**
    * @author       Accenture
    * @name         setSentNAVWINDMFlag
    * @date         02/05/2017
    * @description  This method updates EP_SENT_TO_NAV_WINDMS__c flag of sell-To and its child ship-To to true when user system send customer create request(Basic Data setup - Account Set-Up )
    * @param        NA
    * @return       NA
    */
    public void setSentNAVWINDMFlag(){
        EP_GeneralUtility.Log('Public','EP_AccountService','setSentNAVWINDMFlag');
        accountType.setSentNAVWINDMFlag(account);
    }

    /**
    * @author       Accenture
    * @name         getCustomerPONumber
    * @date         03/24/2017
    * @description  This method is used to get customer PO Number
    * @param        NA
    * @return       String
    */
    public string getCustomerPONumber(){
        EP_GeneralUtility.Log('Public','EP_AccountService','getCustomerPONumber');
        return accountType.getCustomerPONumber(account);
    }


    /**
    * @author       Accenture
    * @name         getPricebookId
    * @date         03/27/2017
    * @description  This method is used to get pricebookId
    * @param        NA
    * @return       Id
    */
    public Id getPricebookId(){
        EP_GeneralUtility.Log('Public','EP_AccountService','getPricebookId');
        return accountType.getPricebookId(account);
    }

    /**
    * @author       Accenture
    * @name         getPricebookEntries
    * @date         03/27/2017
    * @description  This method is used to get products associated with cusotmer
    * @param        NA
    * @return       Id
    */
    public Map<String,PriceBookEntry> getPricebookEntry(){
        return accountType.getPricebookEntry(account); 
    }

	/**
    * @author       Accenture
    * @name         getPricebookEntry
    * @date         03/27/2017
    * @description  This method is used to get products associated with cusotmer
    * @param        NA
    * @return       Id
    */
    public Map<String,PriceBookEntry> getPricebookEntry(string key){
        return accountType.getPricebookEntry(account,key); 
    }
    
    /**
    * @author       Accenture
    * @name         isDeliveryTypeChanged
    * @date         04/04/2017
    * @description  This method returns true if Delivery Type has changed
    * @param        NA
    * @return       Boolean
    */
    public boolean isDeliveryTypeChanged(){
        EP_GeneralUtility.Log('Private','EP_AccountService','isDeliveryTypeChanged');
        return ((oldAccount!=null)?(account.EP_Delivery_Type__c != oldAccount.EP_Delivery_Type__c):false);
    }
 
    // #routeSprintWork starts
    /**
    * @author       Accenture
    * @name         isRouteAllocationAvailable
    * @date         08/05/2017
    * @description  This method returns true if there is a record of ship to and selected default route
    * @param        NA
    * @return       Boolean
    */
    public boolean isRouteAllocationAvailable(){ // #routeSprintWork
        EP_GeneralUtility.Log('Private','EP_AccountService','isRouteAllocationAvailable');
        if(account.EP_Default_Route__c != NULL){
            return accountType.isRouteAllocationAvailable(account);
        }
        return true;
    }
   // #routeSprintWork Ends
    
   //L4_45352_start
   
    public Boolean isPickupEnabledSupplyOptionAttached(){
        EP_GeneralUtility.Log('Private','EP_AccountService','isPickupEnabledSupplyOptionAttached');
        return accountType.isPickupEnabledSupplyOptionAttached(account);
    }
    

    //Method to Send Customer Create request to NAV
    /**
    * @author       Accenture
    * @name         doActionSendCreateRequestToNav
    * @date         11/17/2017
    * @description  This method is used to send customer create request to NAV
    * @param        NA
    * @return       NA
    */
    public void doActionSendCreateRequestToNav(){
        EP_GeneralUtility.Log('Public','EP_AccountService','doActionSendCreateRequestToNavAndLomo');
        accountType.sendCustomerCreateRequestToNAV(account);
    }

    //Method to Send Customer Create request to NAV
    /**
    * @author       Accenture
    * @name         sendCustomerCreateRequestToWinDMS
    * @date         11/17/2017
    * @description  This method is used to send customer create request to WinDMS
    * @param        NA
    * @return       NA
    */
    public void doActionSendCreateRequestToWinDMS(){
        EP_GeneralUtility.Log('Public','EP_AccountService','doActionSendCreateRequestToWinDMS');
        accountType.sendCustomerCreateRequestToWinDMS(account);
    }

    public boolean isRecommendedCreditLimitRequired(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isRecommendedCreditLimitRequired');
        return accountType.isRecommendedCreditLimitRequired(account);
    }

    public boolean isSellToSynced(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isSellToSynced');
        return accountType.isSellToSynced(account);
    }
    
    public boolean isSellToActive(){
        EP_GeneralUtility.Log('Public','EP_AccountService','isSellToActive');
        return accountType.isSellToActive(account);
    }
    //Code changes for L4 #45362 Start
     public boolean hasInfligtOrders(){
        //TODO - Check if any inflight orders are exists for the ShipTo
        return false;
     }
    //Code changes for L4 #45362 End
    
    // Changes made for CUSTOMER MODIFICATION L4 Start
    public void doActionSendCustomerEditRequestToNAV(){
        EP_GeneralUtility.Log('Public','EP_AccountService','sendCustomerCreateRequestToNAV'); 
        accountType.sendCustomerEditRequestToNAV(account);
    }

    public void doActionSendCustomerEditRequestToWinDMS(){
        EP_GeneralUtility.Log('Public','EP_AccountService','sendCustomerCreateRequestToWinDMS');
        accountType.sendCustomerEditRequestToWinDMS(account); 
    }

    public void doActionSendRequest(){
        
        if(!accountDomainObj.isStatuschanged()){
            // Changes made for CUSTOMER MODIFICATION L4 Start
            if(!this.account.EP_Synced_PE__c){
                doActionSyncCustomerToPricingEngine();
            }
            else if(!this.account.EP_Synced_NAV__c){
                doActionSendCustomerEditRequestToNAV();
            }
            else if(!this.account.EP_Synced_WinDMS__C){
                doActionSendCustomerEditRequestToWINDMS();
            }
            // Changes made for CUSTOMER MODIFICATION L4 End
        }
    } 
    // Changes made for CUSTOMER MODIFICATION L4 End
    //Code Changes for #45361 and #45362 Start
    public void resetIntegrationFlagsForBlockUnblock() {
        if (accountDomainObj.isUnblocked() || accountDomainObj.isBlocked()) {
            accountDomainObj.localAccount.EP_Synced_PE__c = true;
            accountDomainObj.localAccount.EP_Synced_NAV__c = true;
            accountDomainObj.localAccount.EP_Synced_winDMS__c = false;
        }
    }
   
    public void resetIntegrationFlagsForDeactivate() {
        if (accountDomainObj.isStatuschanged() && EP_AccountConstant.DEACTIVATE.equalsignorecase(accountDomainObj.localAccount.EP_Status__c)) {
            accountDomainObj.localAccount.EP_Synced_PE__c = false;
            accountDomainObj.localAccount.EP_Synced_NAV__c = true;
            accountDomainObj.localAccount.EP_Synced_winDMS__c = false;
        }
    }
    //Code Changes for #45361 and #45362 End
    
    //L4#67333 Code changes Start
     /**
    * @author       Accenture
    * @name         setFinanceInfo
    * @date         02/27/2018
    * @param        NA
    * @return       NA
    */
    public void setFinanceInfo(){
        EP_GeneralUtility.Log('Public','EP_AccountService','setFinanceInfo'); 
        accountType.setFinanceInfo(account);
    }
    //L4#67333 Code changes End    
}