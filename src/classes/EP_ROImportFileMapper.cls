/**
    @Author         Accenture
    @Name           EP_ROImportFileMapper 
    @Createdate     12/28/2016
    @Description    This class contains all SOQLs related to File Object
    @Version        1.0
    @Reference      NA
*/
public without sharing class EP_ROImportFileMapper {
     
    /**
    * @author       Accenture
    * @name         getRecordsByFileNameAndCheckSumKey
    * @date         04/18/2018
    * @description  This method is used for get File Record by File NAme and Checksum Key
    * @param        string, string
    * @return       list<EP_File__c>
    */ 
    public list<EP_File__c> getRecordsByFileNameAndCheckSumKey(string fileName, string checkSumKey) {
    	return [select id from EP_File__c where Name =:fileName and EP_CheckSum_Key__c =:checkSumKey limit 1];
    }
    
    /**
    * @author       Accenture
    * @name         getFileRecordById
    * @date         04/18/2018
    * @description  This method is used for get File Record by File Id
    * @param        Id
    * @return       EP_File__c
    */ 
    public EP_File__c getFileRecordById(Id fieldId) {
    	return [select Id, Name, EP_CheckSum_Key__c,EP_Company__c,EP_Status__c,EP_Company_Code__c,EP_In_Process__c from EP_File__c where Id=:fieldId limit 1];
    }
    
    /**
    * @author       Accenture
    * @name         getValidRecordbyTransactionNr
    * @date         04/18/2018
    * @description  This method is used for get File Record by checksum key
    * @param        Id
    * @return       EP_File__c
    */ 
    public static list<EP_File__c> getValidRecordbyTransactionNr( string checkSumKey) { 
    	return [select id,EP_StagingErrorCount__c,EP_CheckSum_Key__c from EP_File__c where EP_CheckSum_Key__c =:checkSumKey  limit 1];
    }
}