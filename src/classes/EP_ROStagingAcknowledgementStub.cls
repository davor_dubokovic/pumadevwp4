public with sharing class EP_ROStagingAcknowledgementStub {
    public MSG MSG;
    public EP_ROStagingAcknowledgementStub(){
        MSG = new MSG();
    }
    public class MSG {
        public EP_MessageHeader HeaderCommon;
        public Payload Payload;
        public String StatusPayload;    
        
        public MSG(){
            HeaderCommon= new EP_MessageHeader();
            Payload= new Payload();
        }
    }
    
    public class Payload {
        public any0 any0;
        
        public Payload(){
            any0= new any0();
        }
    }
    public class any0 {
        public SalesData SalesData;
        
        public any0(){   
            SalesData = new SalesData();
        }
    }
    
    public class SalesData {
		public Header header;
		public Lines lines;
		
		public SalesData(){
			header = new Header();
			lines = new Lines();
		}
	}
    public class Header {
		public String companyCode;
		public String transactionNr;
		public String fileName;
		public String processingStatus;
		public String processingDesc;
	}
	public class Lines {
		public List<Line> line;
		/*Constructor*/
        public Lines(){
        	line = new list<Line>();
        }
	}
	
	public class Line {
		public String lineNr;
		public String processingStatus;
		public String preocessingDesc;
	}
}