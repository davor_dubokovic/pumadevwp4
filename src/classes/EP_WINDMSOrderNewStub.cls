/* 
   @Author <Accenture>
   @name <EP_WINDMSOrderNewStub>
   @CreateDate <02/27/2017>
   @Description <These are the stub classes to map JSON string for orders creation Rest Service Request from WINDMS> 
   @Version <1.0>
*/
/* Main stub Class for JSON parsing for VMI order Creation from WINDMS  */
public class EP_WINDMSOrderNewStub {
	
	/* Class for MSG */
    public MSG MSG;
    
    /* stub Class for MSG */
    public class MSG {
        public HeaderCommon HeaderCommon;
        public Payload Payload;
        public String StatusPayload;
    }

    /* stub Class for Any0 */
    public class Any0 {
        public Orders Orders;
    }
    
    /* stub Class for Orders */
    public class Orders {
        public orderWrapper[] Order;
    }
    
    /* stub Class for Order */
    public class orderWrapper {
        //Added New variable 
        public boolean isExistingOrder;
        public boolean hasError = false;
		public string errorCode;
		public string errorDescription;
        //End
        public OrderLines OrderLines;
        public Identifier Identifier;
        public String shiptoId;
        public String modifiedOn;
        public String modifiedBy;
        public String orderIdSF;
        public String clientId;
        public String custId;
        public String shipToCompositKey;
		public String sellToCompositKey;
        /* 
            init method to initialize the sfOrder 
        */
        public csord__Order__c sfOrder;
        public String transporterCode;
        public String seqId ;
        public String tripRefNr;
        public String orderStatusWinDms;
        public String orderCreditStatus;
        public String orderShiftDt;
        
    }
    
    /* stub Class for Identifier */
    public class Identifier {
        public String orderIdWinDMS;
        public String orderRefNr;
    }
   
    /* stub Class for OrderLine */
    public class OrderLine {
        public csord__Order_Line_Item__c orderLineItem;
        public Identifier_Z Identifier;
		public string errorCode;
		public string errorDescription;
        public String seqId ;
        public String ItemId;
        public String tankNr;
        public String uom;
        public String stockHldngLocId;
        public String qty;
    }

    /* stub Class for OrderLines */
    public class OrderLines {
        public OrderLine[] OrderLine;          
    }
    
    /* stub Class for HeaderCommon */
    public class HeaderCommon extends EP_MessageHeader { }
    
    /* stub Class for Payload */
    public class Payload {
        public Any0 Any0;
    }
    
    /* stub Class for Identifier_Z */
    public class Identifier_Z {
        public String lineNr;
    }
}