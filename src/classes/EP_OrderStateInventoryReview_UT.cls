@isTest
public class EP_OrderStateInventoryReview_UT
{
    public static final string OrderTransitionMsg = 'Order Transitions not allowed for : ';
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    
    static testMethod void getTextValue_test() {
        Test.startTest();
        String result = EP_OrderStateInventoryReview.getTextValue();
        Test.stopTest();
        System.AssertEquals(EP_OrderConstant.OrderState_Awaiting_Inventory_Review, result);
    }
    //Method has no implementation, hence adding dummy assert  
    static testMethod void doOnEntry_test() {
        EP_OrderStateInventoryReview localObj = new EP_OrderStateInventoryReview();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateInventoryReviewDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.assertEquals(true, true);
    }
    static testMethod void doOnExit_test() {
        EP_OrderStateInventoryReview localObj = new EP_OrderStateInventoryReview();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateInventoryReviewDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        csord__Order__c ordObj = obj.getOrder();
        System.assertEquals(Approval.isLocked(ordObj.id),true);
    }
    
    static testMethod void setOrderContext_test() {
        EP_OrderStateInventoryReview localObj = new EP_OrderStateInventoryReview();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateInventoryReviewDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        Test.startTest();
        localObj.setOrderContext(obj,oe);
        Test.stopTest();
        System.AssertEquals(true,localObj.order != null);
    }
    static testMethod void setOrderDomainObject_test() {
        EP_OrderStateInventoryReview localObj = new EP_OrderStateInventoryReview();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateInventoryReviewDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.setOrderDomainObject(obj);
        
        Test.stopTest();
        System.assertEquals(true, localObj.order == obj);
    }
}