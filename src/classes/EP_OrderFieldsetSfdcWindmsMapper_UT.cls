/**
   @Author Varsha Patil
   @name <EP_OrderFieldsetSfdcNavMapper_UT>
   @CreateDate 19/1/2017
   @Description This class will be use to test/cover unit test cases for EP_OrderFieldsetSfdcNavMapper class
   @Version <1.0>
   @reference <Referenced program names>
*/
@isTest
public class EP_OrderFieldsetSfdcWindmsMapper_UT {
    private static final Integer COUNT = 1;
    /**
       *  This method will used to get getOrderFieldsetSfdcWindmsRec records.
       *  @name <getOrderSfdcWindmsRec_PositiveTest>
       *  @param NA
       *  @return NA
       *  @throws NA
    */
    private static testMethod void getOrderSfdcWindmsRec_PositiveTest(){
       // prepare data
       List<EP_Order_Fieldset_Sfdc_Windms_Intg__c> orderCSList = new List<EP_Order_Fieldset_Sfdc_Windms_Intg__c>();
       EP_Order_Fieldset_Sfdc_Windms_Intg__c orderCS = new EP_Order_Fieldset_Sfdc_Windms_Intg__c();
        for (Integer i = 0; i < COUNT; i++){ 
              orderCS.Name ='Dummy ShipTo Id';
              orderCS.Respective_Tag_Name__c = 'shipToId'; 
              orderCS.Parent_Tag__c = ''; 
              orderCS.Order_sequence__c = 7; 
              orderCS.Object_Name__c ='Order'; 
              orderCS.Field_API_Name__c ='EP_Shipto_Storage_Location__c';
              orderCSList.add(orderCS);
        }
        insert orderCSList;
        system.runAs(new User(Id= Userinfo.getUserId())){
            EP_OrderFieldsetSfdcWindmsMapper OrderWindms = new EP_OrderFieldsetSfdcWindmsMapper();
            List <EP_Order_Fieldset_Sfdc_Windms_Intg__c> returnOrderWindms = OrderWindms.getOrderFieldsetSfdcWindmsRec(1);
            System.debug('****returnOrderWindms size =='+returnOrderWindms.size());
            system.assertEquals(orderCSList.size() , returnOrderWindms.size());
        }
    }   
}