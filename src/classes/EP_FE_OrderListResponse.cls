/* 
  @Author <Ram Rai>
   @name <EP_FE_OrderList >
   @CreateDate <21/04/2016>
   @Description <This class >  
   @Version <1.0>
*/
global with sharing class EP_FE_OrderListResponse extends EP_FE_Response {

    global static final Integer ERROR_NOT_VALID_OFFSET = -1;
    global static final Integer ERROR_NOT_VALID_LIMIT = -2;
    global static final Integer ERROR_NOT_VALID_TYPE = -3; 
    global static final Integer ERROR_NOT_VALID_ALERT = -5;     
    global static final Integer ERROR_FETCHING_ORDER_LIST = -4; 

    public List<EP_FE_OrderWrapper> orderList;
    public List<Contract> blanketContracts;
    public Map<String, List<csord__Order_Line_Item__c>> contractBlanketOrderItems;
    

    /*
     *  Default constructor
     */
    public EP_FE_OrderListResponse() {
        orderList = new List<EP_FE_OrderWrapper>();
        blanketContracts = new List<Contract>();
        contractBlanketOrderItems = new Map<String, List<csord__Order_Line_Item__c>>();        
    }
    
    global with sharing class EP_FE_Attachment{
        public string ParentId {get; set;}
        public string Name {get; set;}
        public string PdfLink {get; set;}
        }
    /*
     *  Order Wrapper, to isolate 'editability' and pricing info
     */
    global with sharing class EP_FE_OrderWrapper {
      public csord__Order__c record;
      public Boolean canSeePrices;
      public Boolean editable;
      public Boolean reorder;
      public Boolean cancellable;
      public Boolean containsPackagedProducts;
      public EP_FE_OrderProductData productData;
      public Account billTo; 
     public List<EP_FE_Attachment> orderAttachment;
      
    /*
     *  Order Wrapper constructor
     */
      public EP_FE_OrderWrapper() {
        editable = false;
        canSeePrices = True;//false;
        record = new csord__Order__c();
        productData = new EP_FE_OrderProductData();
      }
     /*
     *  Order Wrapper constructor
     */
      public EP_FE_OrderWrapper(csord__Order__c order, Boolean cancellable, Boolean canSeePrices, Boolean reorder, EP_FE_OrderProductData orderItems, Account billTo, List<EP_FE_Attachment> orderAttachment) {
        this.record = order;
        try {
            this.editable = cancellable && (order.EP_Is_Pre_Pay__c != true || order.Status__c == EP_Common_Constant.ORDER_DRAFT_STATUS);
        } catch (Exception e) {
            this.editable = cancellable;
        }
        /*Please remove when deploying*/
        this.editable = true;
        /*End*/
        this.reorder = reorder;
        this.cancellable = true;//cancellable;
        this.canSeePrices = True;//canSeePrices;
        this.productData = orderItems;
        this.containsPackagedProducts = EP_FE_OrderUtils.orderContainsPackagedProduct(order);
        this.billTo = billTo;
        this.orderAttachment = orderAttachment;         
      }
    }
}