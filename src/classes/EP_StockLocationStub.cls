/* 
   @Author          Accenture
   @name            EP_StockLocationStub 
   @CreateDate      15/12/2017
   @Description     Stub class to create or update stock Location i.e. inbound interface to reate or update stock Location
   @Version         1.0
*/
public class EP_StockLocationStub {
    public MSG MSG;
    
    /**
    * @author           Accenture
    * @name             EP_StockLocationStub
    * @date             20/12/2017
    * @description      Constructor
    * @param            NA
    * @return           NA
    */
    public EP_StockLocationStub(){
        MSG = new MSG();
    }
    
    /* 
    @Author          Accenture
    @name            inventoryLocs 
    @CreateDate      20/12/2017
    @Description     Stub class of inventoryLocs
    @Version         1.0
    */
    public class inventoryLocs{
        public list<inventoryLoc> inventoryLoc;
        /*Constructor*/
        public inventoryLocs(){
             inventoryLoc= new list<inventoryLoc>();
        }
    }
    
    /* 
    @Author          Accenture
    @name            MSG 
    @CreateDate      20/12/2017
    @Description     Stub class of MSG
    @Version         1.0
    */
    public class MSG {
        public EP_MessageHeader HeaderCommon; 
        public Payload Payload;
        public String StatusPayload;
        /*Constructor*/
        public MSG(){
            HeaderCommon = new EP_MessageHeader();
            Payload = new Payload();
        }
    }
    
    /* 
    @Author          Accenture
    @name            Identifier 
    @CreateDate      20/12/2017
    @Description     Stub class of Identifier
    @Version         1.0
    */
    public class Identifier {
        public String locationId;
        public String companyCode;
        public String entrprsId;
    }
    
    /* 
    @Author          Accenture
    @name            inventoryLoc 
    @CreateDate      20/12/2017
    @Description     Stub class of inventoryLoc
    @Version         1.0
    */
    public class inventoryLoc {
        public String seqId;
        public Identifier identifier;
        public String locType;
        public String name;
        public String name2;
        public String shortCode;
        public String address;
        public String address2;
        public String city;
        public String postCode;
        public String county;
        public String countryCode;
        public String phone;
        public String fax;
        public String email;
        public String isThirdParty;
        public String isBlocked;
        public String tmsIntgrtd;
        public String timeZone;
        public String pricingLocGrp;
        public String versionNr;
        /*Constructor*/
        public InventoryLoc(){
            identifier = new Identifier();
        }
    }

    /* 
    @Author          Accenture
    @name            Payload 
    @CreateDate      20/12/2017
    @Description     Stub class of Payload
    @Version         1.0
    */
    public class Payload {
        public Any0 any0;
        /*Constructor*/
        public Payload(){
            any0= new Any0();
        }
    }

    /* 
    @Author          Accenture
    @name            Any0 
    @CreateDate      20/12/2017
    @Description     Stub class of Any0
    @Version         1.0
    */
    public class Any0 {
        public inventoryLocs inventoryLocs;
        /*Constructor*/
        public Any0(){
            inventoryLocs = new inventoryLocs();
        }
        
    }

}