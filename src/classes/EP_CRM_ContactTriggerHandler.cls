/* ================================================
 * @Class Name : EP_CRM_ContactTriggerHandler 
 * @author : Shailla Gokhale
 * @Purpose: This is Contact trigger handler class and used to craete contact records to the secondary connection org
 * @created date: 29/08/2017
 ================================================*/
public with sharing class EP_CRM_ContactTriggerHandler{
    public static Boolean isInsertRecursiveTrigger = False;

      /* This method is execute after account record creation.
    * @description : This method will create Contact records on the secondary connection org
    * @param : Account record list
    * @return: NA
    */
    public static void afterInsertConnectionContact(list<Contact> TriggerNew){
    
    try{
    // Define connection id 
    Id networkId = ConnectionHelper.getConnectionId(System.Label.Connection_Name); 
    
    Set<Id> localContactAccountSet = new Set<Id>(); 
    List<Contact> localContacts = new List<Contact>(); 
    Set<Id> sharedAccountSet = new Set<Id>();    
    
    // only share records created in this org, do not add contacts received from another org. 
    for (Contact newContact : TriggerNew) { 
        if (newContact.ConnectionReceivedId == null && newContact.AccountId != null) { 
            localContactAccountSet.add(newContact.AccountId); 
            localContacts.add(newContact); 
        }         
    } 
    
    if (localContactAccountSet.size() > 0) { 
        // Get the contact account's partner network record connections 
        for (PartnerNetworkRecordConnection accountSharingRecord :  
                                  [SELECT p.Status, p.LocalRecordId, p.ConnectionId 
                                   FROM PartnerNetworkRecordConnection p              
                                   WHERE p.LocalRecordId IN :localContactAccountSet]) { 
                  
            // for each partner connection record for contact's account, check if it is active 
            if ((Test.isRunningTest()) || ((accountSharingRecord.status.equalsignorecase('Sent') 
              || accountSharingRecord.status.equalsignorecase('Received'))) 
              && (accountSharingRecord.ConnectionId == networkId)) { 
                sharedAccountSet.add(accountSharingRecord.LocalRecordId); 
            }               
        } 
            
        if (sharedAccountSet.size() > 0) { 
            List<PartnerNetworkRecordConnection> contactConnections =  new  List<PartnerNetworkRecordConnection>(); 
            
            for (Contact newContact : localContacts) { 
  
                if (sharedAccountSet.contains(newContact.AccountId)) { 
                   
                    PartnerNetworkRecordConnection newConnection = 
                      new PartnerNetworkRecordConnection( 
                          ConnectionId = networkId, 
                          LocalRecordId = newContact.Id, 
                          SendClosedTasks = false, 
                          SendOpenTasks = false, 
                          SendEmails = false, 
                          ParentRecordId = newContact.AccountId); 
                          
                     contactConnections.add(newConnection); 
                
                } 
            } 
  
            if (contactConnections.size() > 0 ) { 
                   database.insert(contactConnections); 
            } 
        } 
     }
}
 
  catch(exception ex){
            ex.getmessage();
        }
   } 
   
}