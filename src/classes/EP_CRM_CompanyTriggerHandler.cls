/* ================================================
 * @Class Name : EP_CRM_CompanyTriggerHandler
 * @author : Hasmukh Jain
 * @Purpose: This is Company trigger handler class is used to create records on the s2s connected org
 * @created date: 12/03/2018
 * @Modified Date:
 ================================================*/
public with sharing class EP_CRM_CompanyTriggerHandler{
    public static Boolean isInsertRecursiveTrigger = False;    

    /* This method is execute after company record creation.
    * @description : This method will create company records on the secondary connection org
    * @param : company record list
    * @return: NA
    */
    public static void afterInsertConnectionCompany(list<Company__c> TriggerNew){
    
    try{
   
    // Define connection id 
    Id networkId = ConnectionHelper.getConnectionId(System.Label.Connection_Name); 
    
    List<Company__c> localCompanies = new List<Company__c>(); 
    Set<Id> sharedAccountSet = new Set<Id>();    
    
    // only share records created in this org, do not add contacts received from another org. 
    for (Company__c newCompany : TriggerNew) { 
            localCompanies.add(newCompany);                
    } 

            List<PartnerNetworkRecordConnection> companyConnections =  new  List<PartnerNetworkRecordConnection>(); 
            
            for (Company__c newCompany : localCompanies) {   
                                  
                    PartnerNetworkRecordConnection newConnection = 
                      new PartnerNetworkRecordConnection( 
                          ConnectionId = networkId, 
                          LocalRecordId = newCompany.Id, 
                          SendClosedTasks = false, 
                          SendOpenTasks = false, 
                          SendEmails = false, 
                          RelatedRecords = 'EP_Payment_Method__c,EP_Payment_Term__c'                          
                          );                                                     
                          
                    companyConnections.add(newConnection);          
            } 
  
            if (companyConnections.size() > 0 ) { 
                   database.insert(companyConnections); 
            } 
           
        }
        catch(exception ex){
            ex.getmessage();
        }
    
            
    }
}