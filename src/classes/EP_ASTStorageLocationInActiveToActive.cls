/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationInActiveToActive>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 07-Inactive to 05-Active>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationInActiveToActive extends EP_AccountStateTransition {

    public EP_ASTStorageLocationInActiveToActive () {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationInActiveToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationInActiveToActive', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationInActiveToActive',' isGuardCondition');        
        return true;
    }

}