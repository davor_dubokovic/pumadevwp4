@isTest  
private class EP_OrderConfigMapper_UT{

	static testMethod void getRecordsByIdsCountry_test() {
        EP_OrderConfigMapper localObj = new EP_OrderConfigMapper();
        csord__Order__c orderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
        Product2 productObj =  EP_TestDataUtility.createTestRecordsForProduct();
        
        EP_Country__c  country = EP_TestDataUtility.createCountryRecord('Australia','AU','AU');
        country.EP_Mixing_Allowed__c = FALSE;  
        insert country;

        EP_Order_Configuration__c countryOrderConfig = EP_TestDataUtility.createOrderConfigRecord('countryOrderConfig1',EP_Common_Constant.COUNTRY_RANGE,1,1000,'LT',country.id,productObj.id);
        insert countryOrderConfig;
        
        EP_Order_Configuration__c orderConfObj = [SELECT  EP_Country__c,EP_Product__c ,EP_Min_Quantity__c FROM EP_Order_Configuration__c LIMIT 1];
        List<id> listProductd = new List<Id>{orderConfObj.EP_Product__c};
        String contryStr = orderConfObj.EP_Country__c;
        Test.startTest();
        list<EP_Order_Configuration__c> result = localObj.getRecordsByIdsCountry(listProductd,contryStr);
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
}