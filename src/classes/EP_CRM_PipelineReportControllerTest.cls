@isTest 
private class EP_CRM_PipelineReportControllerTest {
    static Map < String, User > userMap = new Map < String, User > ();
    static Map < String, UserRole > roleMap = new Map < String, UserRole > ();
    
    static { 
        for(User userObj: [SELECT Id, Name, EP_CRM_Geo1__c, EP_CRM_Geo2__c FROM User]) {
            userMap.put(userObj.Name, userObj);
        }
        for(UserRole roleObj: [SELECT Id, Name FROM UserRole]) {
            roleMap.put(roleObj.Name, roleObj);
        }
    }
    
    @testSetup 
    static void loadData() {
        UserRole testRole = EP_CRM_TestDataFactory.createUserRole('Belize Manager Retail', UserInfo.getUserRoleId());
        insert testRole;
        System.assertNotEquals(testRole, null);
        System.assertNotEquals(testRole.Id, null);
        
        testRole = EP_CRM_TestDataFactory.createUserRole('Belize Sales Retail', testRole.Id);
        insert testRole;
        System.assertNotEquals(testRole, null);
        System.assertNotEquals(testRole.Id, null);
        
        Id profileId = EP_CRM_TestDataFactory.getProfileIdByName('System Administrator');
        System.assertNotEquals(profileId, null);
        
        List < User > userList = new List < User > ();
        userList.add(EP_CRM_TestDataFactory.createTestUser(null, profileId, 'Sales', 'User'));
        userList.add(EP_CRM_TestDataFactory.createTestUser(null, profileId, 'Manager', 'User'));
        insert userList;
        System.assertNotEquals(userList.get(0).Id, null);
        System.assertNotEquals(userList.get(1).Id, null);
        
        User testUser = [SELECT Id, Name, EP_CRM_Geo1__c, EP_CRM_Geo2__c FROM User WHERE Name = 'Sales User' limit 1];
        System.assertNotEquals(testUser, null);
        System.assertNotEquals(testUser.Id, null);
        
        System.runAs(testUser) {
            // Create Puma Company
            Company__c pumaCompany = EP_CRM_TestDataFactory.createPumaCompany('Puma Company', new User());
            insert pumaCompany;
            System.assertNotEquals(pumaCompany, null);
            System.assertNotEquals(pumaCompany.Id, null);
            
            // Create Account List
            List < Account > accountList = new List < Account > ();
            for(Integer i = 1; i <= 2; i++) {
                accountList.add(EP_CRM_TestDataFactory.createAccount('Test Account - ' + i, pumaCompany));
            }
            insert accountList;
            
            //Added by Hasmukh Jain on 19-Feb-2018 to create Payment Method
            EP_Payment_Method__c paymentMtd = EP_CRM_TestDataFactory.createPaymentMethod(pumaCompany);            
            System.assertNotEquals(paymentMtd, null);
            System.assertNotEquals(paymentMtd.Id, null);
            
            //Added by Hasmukh Jain on 19-Feb-2018 to create Payment Term
            EP_Payment_Term__c paymentTerm = EP_CRM_TestDataFactory.createPaymentTerm(pumaCompany);            
            System.assertNotEquals(paymentTerm, null);
            System.assertNotEquals(paymentTerm.Id, null);
            
            // Create Open Opportunity List
            List < Opportunity > opptyList = new List < Opportunity > ();
            for(Account accountObj: accountList) {
                for(Integer i = 1; i <= 5; i++) {
                    opptyList.add(EP_CRM_TestDataFactory.createOpportunity('Test Opportunity - ' + i, accountObj,paymentMtd,paymentTerm));
                }
            } 
            insert opptyList;
            
            // Create Contract List
            List < Contract > contractList = new List < Contract > ();
            for(Opportunity opptyObj: opptyList) {
                for(Integer i = 1; i <= 5; i++) {
                    contractList.add(EP_CRM_TestDataFactory.createContract(opptyObj));
                }
            }
            insert contractList;
            
            // Update ContractList to Activated
            for(Contract contractObj: contractList) {
                contractObj.Status = 'Activated';
            }
            update contractList;
            
            // Update Opportunity to Closed Won
            List < Opportunity > wonOpptyList = new List < Opportunity > (); 
            for(Integer i = 1; i <= opptyList.size(); i++) {
                if(i <= 2) {
                    Opportunity opptyObj = opptyList.get(i);
                    opptyObj.StageName = 'Closed Won'; 
                    wonOpptyList.add(opptyObj);
                }
            }
            update wonOpptyList;
        }
    }
    
    @isTest 
    static void testNoRoleUser() {
        User testUser = userMap.get('Sales User');
        System.assertNotEquals(testUser, null);
        
        Test.startTest();
            System.runAs(testUser) {
                EP_CRM_PipelineReportController controller = new EP_CRM_PipelineReportController();
                System.assertEquals(controller.showForm, false);
            }
        Test.stopTest();
    }
    
    @isTest 
    static void testSalesRoleUser() {
        
        User testUser = userMap.get('Sales User');
        testUser.UserRoleId = roleMap.get('Belize Sales Retail').Id;
        update testUser;
        System.assertNotEquals(testUser.UserRoleId, null);
        
        Test.startTest();
            System.runAs(testUser) {
                EP_CRM_PipelineReportController controller = new EP_CRM_PipelineReportController();
                System.assertEquals(controller.showForm, true);         
                System.assertEquals(controller.isSelectedProductsWithSameUoM, true);    
                // Set different products with different UoM
                controller.bean.OpportunityObj.EP_CRM_Product_Interest_Group__c = 'Fuels;LPG;Bitumen;';
                System.assertEquals(controller.bean.getBitumenSelected(), true);
                controller.prepareUoM();
                System.assertEquals(controller.isSelectedProductsWithSameUoM, false);
                // Check for Information Message
                for(ApexPages.Message msg :  ApexPages.getMessages()) {
                    System.assertEquals(msg.getSummary().contains(EP_CRM_Constants.MULTIPLE_PRODUCTS_WITH_DIFFERENT_UOM_ERROR), true);
                }
                // Set different products with same UoM
                controller.bean.OpportunityObj.EP_CRM_Product_Interest_Group__c = 'Fuels;Fuel Card;';
                controller.prepareUoM();
                System.assertEquals(controller.isSelectedProductsWithSameUoM, true);
                // Clear Message List -- since we can not reRender backend
                ApexPages.getMessages().clear();
                // Set No UoM value -- Result into Error Message 
                controller.bean.selectedUoM = '';
                controller.generatePipelineReport();
                for(ApexPages.Message msg :  ApexPages.getMessages()) {
                    if(msg.getSeverity() == ApexPages.Severity.ERROR) {
                        System.assertEquals(msg.getSummary().contains(EP_CRM_Constants.UOM_VALUE_MISSING_ERROR), true);
                    }
                }               
            }
        Test.stopTest();
    }
    
    @isTest 
    static void testManagerRoleUser() {
        User testUser = userMap.get('Manager User');
        testUser.UserRoleId = roleMap.get('Belize Manager Retail').Id;
        update testUser;
        System.assertNotEquals(testUser.UserRoleId, null);
        
        testUser = userMap.get('Sales User');
        testUser.UserRoleId = roleMap.get('Belize Sales Retail').Id;
        update testUser;
        System.assertNotEquals(testUser.UserRoleId, null);

        Test.startTest();
            System.runAs(userMap.get('Manager User')) {
                EP_CRM_PipelineReportController controller = new EP_CRM_PipelineReportController();
                controller.bean.getTotalOpenOpportunityAmountMap();
                controller.bean.getTotalClosedOpportunityAmountMap();
                controller.bean.getCurrentMonthTotalClosedAmount();
                controller.bean.getTotalOpenOpportunityAmount();
                controller.bean.getTotalClosedOpportunityAmount();
                controller.bean.getTotalFullYearAmount();
            }
        Test.stopTest();
    }    
    
    @isTest 
    static void testWrapperDefaultValues() {
        Test.startTest();
            EP_CRM_PipelineReportWrapper wrapperObj = new EP_CRM_PipelineReportWrapper();
            EP_CRM_CommonException cmnExp = new EP_CRM_CommonException();
            System.assertNotEquals(wrapperObj.openOpportunityMap, null);
            System.assertNotEquals(wrapperObj.closedOpportunityMap, null);
            System.assertNotEquals(wrapperObj.opportunityNameList, null);
            System.assertEquals(wrapperObj.getOpportunityName(), '');
            System.assertEquals(wrapperObj.remainingForecastAmount, 0);
            System.assertEquals(wrapperObj.closedWonAmount, 0);
            System.assertEquals(wrapperObj.getFullYearAmount(), 0);
            
        Test.stopTest();
    }
}