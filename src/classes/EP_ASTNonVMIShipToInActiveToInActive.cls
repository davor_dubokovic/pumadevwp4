/*
*  @Author <Accenture>
*  @Name <EP_ASTNonVMIShipToInActiveToInActive>
*  @CreateDate <28/02/2017>
*  @Description <Handles VMI Ship To Account status change from 07-Inactive to 07-Inactive>
*  @Version <1.0>
*/
public class EP_ASTNonVMIShipToInActiveToInActive extends EP_AccountStateTransition {

    public EP_ASTNonVMIShipToInActiveToInActive () {
        finalState = EP_AccountConstant.INACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToInActiveToInActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToInActiveToInActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToInActiveToInActive','isGuardCondition');
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToInActiveToInActive','doOnExit');

    }
}