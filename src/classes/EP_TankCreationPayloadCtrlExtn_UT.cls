@isTest
public class EP_TankCreationPayloadCtrlExtn_UT
{
	static testMethod void checkPageAccess_test() {
	    Account acc = EP_TestDataUtility.getShipToPositiveScenario();
	    EP_Tank__c tank = [select id from EP_Tank__c where EP_Ship_To__c =: acc.Id LIMIT 1];
	    ApexPages.StandardController sc = new ApexPages.StandardController(new EP_Tank__c(Id=tank.Id));
	    EP_TankCreationPayloadCtrlExtn localObj = new EP_TankCreationPayloadCtrlExtn(sc);
	    Test.startTest();
	    PageReference result = localObj.checkPageAccess();
	    string shipToStatus = localObj.shipToStatus;
	    Test.stopTest();
	    System.AssertEquals(true,result != NULL);
	    System.AssertEquals(true,result.getURL().toUpperCase().contains(EP_Common_Constant.UNAUTHORIZEDACCESSPAGESTR.toUpperCase()));     
	}
}