/*
    @Author          Accenture
    @Name            EP_GenerateRequestXML
    @CreateDate      04/18/2017
    @Novasuite Fixes -- Required Documentation
    @Description     virtual class is used to generate outbound XML's to Sync with NAV and WINDMS
    @Version         1.0
    @Reference       NA
*/
public virtual class EP_GenerateRequestXML {
    protected DOM.Document doc = new DOM.Document();
    public dom.XmlNode MSGNode;
    public String messageId;
    public String messageType;
    public String companyCode;
    public boolean isEncryptionEnabled;
    public Id recordId;
    public EP_OrderDataStub order;
    
   /**
    * @author           Accenture
    * @name             createXML
    * @date             04/18/2017
    * @description      This method is used to create XML for outbound Message
    * @param            NA
    * @return           NA
    */
    public virtual string createXML(){
        EP_GeneralUtility.Log('Public','EP_GenerateRequestXML','createXML');
        init();
        createHeaderNode();
        createPayload();
        createStatusPayLoad();
        return doc.toXmlString();
    }
    /**
    * @author           Accenture
    * @name             init
    * @date             04/18/2017
    * @description      This method is used to fetch data from Database which will be require to wrapped into XML
    * @param            NA
    * @return           NA
    */
    public virtual void init(){
        EP_GeneralUtility.Log('Public','EP_GenerateRequestXML','init');
        MSGNode = doc.createRootElement(EP_OrderConstant.MSG, null, null);
    }
    
    /**
    * @author           Accenture
    * @name             encodeXML
    * @date             04/18/2017
    * @description      This is a virtual method to encode the payload for the request xml
    * @param            NA
    * @return           NA
    */
    public virtual String encodeXML(DOM.Document xmlStringForEncoding){
        EP_GeneralUtility.Log('Public','EP_GenerateRequestXML','encodeXML');
        String encodedXMLString;
        String payloadcontent = xmlStringForEncoding.toXmlString().replace(EP_OrderConstant.XML_BEGIN,EP_Common_Constant.BLANK);
        payloadcontent = payloadcontent.unescapeXml();
        
        //defaulting to unencrypted xml string
        encodedXMLString = payloadcontent;

        //checking the custom setting and encrypt the xml string
        if(isEncryptionEnabled){
            encodedXMLString = EncodingUtil.base64Encode(blob.valueOf(payloadcontent));
        }
        return encodedXMLString;
    }
    
     /**
    * @author           Accenture
    * @name             createHeaderNode
    * @date             04/18/2017
    * @description      This method is used to create Header XML Nodes
    * @param            NA
    * @return           NA
    */ 
    public virtual void createHeaderNode(){
        EP_GeneralUtility.Log('Public','EP_GenerateRequestXML','createHeaderNode');

        EP_CS_OutboundMessageSetting__c msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(messageType);
        EP_OutboundMessageHeader headerObj = EP_OutboundMessageUtil.setMessageHeader(msgSetting,messageId,companyCode);
        Dom.XMLNode HeaderCommonNode = MSGNode.addChildElement(EP_OrderConstant.HEADER_COMMON,null,null);
        
        //Repeat this for every Child node
        HeaderCommonNode.addChildElement(EP_OrderConstant.MsgID,null,null).addTextNode(messageId);
        HeaderCommonNode.addChildElement(EP_OrderConstant.InterfaceType,null,null).addTextNode(getValueforNode(headerObj.InterfaceType)); //Value for InterfaceType
        HeaderCommonNode.addChildElement(EP_OrderConstant.SourceGroupCompany,null,null).addTextNode(getValueforNode(headerObj.SourceGroupCompany));
        HeaderCommonNode.addChildElement(EP_OrderConstant.DestinationGroupCompany,null,null).addTextNode(getValueforNode(headerObj.DestinationGroupCompany));
        HeaderCommonNode.addChildElement(EP_OrderConstant.SourceCompany,null,null).addTextNode(getValueforNode(headerObj.SourceCompany )); //Value for SourceCompany
        HeaderCommonNode.addChildElement(EP_OrderConstant.DestinationCompany,null,null).addTextNode(getValueforNode(headerObj.DestinationCompany));
        HeaderCommonNode.addChildElement(EP_OrderConstant.CorrelationID,null,null).addTextNode(getValueforNode(headerObj.CorrelationID));
        HeaderCommonNode.addChildElement(EP_OrderConstant.DestinationAddress,null,null).addTextNode(getValueforNode(headerObj.DestinationAddress));
        HeaderCommonNode.addChildElement(EP_OrderConstant.SourceResponseAddress,null,null).addTextNode(getValueforNode(headerObj.SourceResponseAddress)); //Value for SourceResponseAddress
        HeaderCommonNode.addChildElement(EP_OrderConstant.SourceUpdateStatusAddress,null,null).addTextNode(getValueforNode(headerObj.SourceUpdateStatusAddress)); //Value for SourceUpdateStatusAddress
        HeaderCommonNode.addChildElement(EP_OrderConstant.DestinationUpdateStatusAddress,null,null).addTextNode(getValueforNode(headerObj.DestinationUpdateStatusAddress));
        HeaderCommonNode.addChildElement(EP_OrderConstant.MiddlewareUrlForPush,null,null).addTextNode(getValueforNode(headerObj.MiddlewareUrlForPush));
        HeaderCommonNode.addChildElement(EP_OrderConstant.EmailNotification,null,null).addTextNode(getValueforNode(headerObj.EmailNotification));
        HeaderCommonNode.addChildElement(EP_OrderConstant.ErrorCode,null,null).addTextNode(getValueforNode(headerObj.ErrorCode));
        HeaderCommonNode.addChildElement(EP_OrderConstant.ErrorDescription,null,null).addTextNode(getValueforNode(headerObj.ErrorDescription));
        HeaderCommonNode.addChildElement(EP_OrderConstant.ProcessingErrorDescription,null,null).addTextNode(getValueforNode(headerObj.ProcessingErrorDescription));
        HeaderCommonNode.addChildElement(EP_OrderConstant.ContinueOnError,null,null).addTextNode(getValueforNode(headerObj.ContinueOnError)); //Value for ContinueOnError
        HeaderCommonNode.addChildElement(EP_OrderConstant.ComprehensiveLogging,null,null).addTextNode(getValueforNode(headerObj.ComprehensiveLogging)); //Value for ComprehensiveLogging
        HeaderCommonNode.addChildElement(EP_OrderConstant.TransportStatus,null,null).addTextNode(getValueforNode(headerObj.TransportStatus)); //Value for TransportStatus
        HeaderCommonNode.addChildElement(EP_OrderConstant.ProcessStatus,null,null).addTextNode(getValueforNode(headerObj.ProcessStatus));
        HeaderCommonNode.addChildElement(EP_OrderConstant.UpdateSourceOnReceive,null,null).addTextNode(getValueforNode(headerObj.UpdateSourceOnReceive)); //Value for UpdateSourceOnReceive
        HeaderCommonNode.addChildElement(EP_OrderConstant.UpdateSourceOnDelivery,null,null).addTextNode(getValueforNode(headerObj.UpdateSourceOnDelivery)); //Value for UpdateSourceOnDelivery
        HeaderCommonNode.addChildElement(EP_OrderConstant.UpdateSourceAfterProcessing,null,null).addTextNode(getValueforNode(headerObj.UpdateSourceAfterProcessing)); //Value for UpdateSourceAfterProcessing
        HeaderCommonNode.addChildElement(EP_OrderConstant.UpdateDestinationOnDelivery,null,null).addTextNode(getValueforNode(headerObj.UpdateDestinationOnDelivery)); //Value for UpdateDestinationOnDelivery
        HeaderCommonNode.addChildElement(EP_OrderConstant.CallDestinationForProcessing,null,null).addTextNode(getValueforNode(headerObj.CallDestinationForProcessing)); //Value for CallDestinationForProcessing
        HeaderCommonNode.addChildElement(EP_OrderConstant.ObjectType,null,null).addTextNode(getValueforNode(headerObj.ObjectType)); //Value for ObjectType
        HeaderCommonNode.addChildElement(EP_OrderConstant.ObjectName,null,null).addTextNode(getValueforNode(headerObj.ObjectName)); //Value for ObjectName
        HeaderCommonNode.addChildElement(EP_OrderConstant.CommunicationType,null,null).addTextNode(getValueforNode(headerObj.CommunicationType)); //Value for CommunicationType
        
    }
    
   /**
    * @author           Accenture
    * @name             createXML
    * @date             04/18/2017
    * @description      This is a virtual method to createPayload
    * @param            NA
    * @return           NA
    */
    public virtual void createPayload(){
         EP_GeneralUtility.Log('Public','EP_GenerateRequestXML','createPayload');
    }
    
    /**
    * @author           Accenture
    * @name             createXML
    * @date             04/18/2017
    * @description      This is a virtual method to create Status Payload
    * @param            NA
    * @return           NA
    */
    public virtual void createStatusPayLoad(){
         EP_GeneralUtility.Log('Public','EP_GenerateRequestXML','createStatusPayLoad');
    }
    
       /**
    * @author           Accenture
    * @name             getValueforNode
    * @date             04/18/2017
    * @description      This method is used to get Value for Node if no value found then return empty value as a string
    * @param            string
    * @return           NA
    */
    public virtual string getValueforNode(string value){
        EP_GeneralUtility.Log('Public','EP_GenerateRequestXML','getValueforNodeString');
        return value != null?value:EP_Common_Constant.BLANK;
    }
    
    /**
    * @author           Accenture
    * @name             getValueforNode
    * @date             04/18/2017
    * @description      This method is used to get boolean Value as string for Node for boolen type data if no value found then return false as a string
    * @param            boolean
    * @return           NA
    */
    public virtual string getValueforNode(boolean value){
        EP_GeneralUtility.Log('Public','EP_GenerateRequestXML','getValueforNodebooelan');
        if(value == null)
            return EP_OrderConstant.FALSE_STR;
        return string.valueof(value);
    }
    
    /**
    * @author           Accenture
    * @name             getValueforNode
    * @date             04/18/2017
    * @description      This method is used to get decimal Value as a string for Node if no value found then return empty string
    * @param            decimal
    * @return           NA
    */
    public virtual string getValueforNode(decimal value){
        EP_GeneralUtility.Log('Public','EP_GenerateRequestXML','getValueforNodedecimal');
        if(value == null)
            return EP_Common_Constant.BLANK;
        return string.valueof(value);
    }
    
    /**
    * @author           Accenture
    * @name             formatDateAsString
    * @date             04/18/2017
    * @description      This method is used to format DateTime into String
    * @param            NA
    * @return           NA
    */
    public virtual String formatDateAsString(Datetime inputDateTime){
        EP_GeneralUtility.Log('Public','EP_GenerateRequestXML','formatDateAsStringDatetime');
        String returnDate = EP_Common_Constant.BLANK;
        if(inputDateTime != null){
            returnDate = EP_DateTimeUtility.formatDateAsString(inputDateTime);
        }
        return returnDate;
    }
    
    /**
    * @author           Accenture
    * @name             formatDateAsString
    * @date             04/18/2017
    * @description      This method is used to format Date into String
    * @param            NA
    * @return           NA
    */
    public virtual String formatDateAsString(Date inputDate){
        EP_GeneralUtility.Log('Public','EP_GenerateRequestXML','formatDateAsStringDate');
        String returnDate = EP_Common_Constant.BLANK;
        if(inputdate != null){
            returnDate = EP_DateTimeUtility.formatDateAsString(inputdate);
        }
        return returnDate;
    }
    /**
    * @author           Accenture
    * @name             transformBooleanValue
    * @date             04/18/2017
    * @param            boolean
    * @return           string
    */
    public virtual string transformBooleanValue(boolean value){
        EP_GeneralUtility.Log('Public','EP_GenerateRequestXML','transformBooleanValue');
        if(value){
            return 'YES';
        }
        return 'NO';
    }
}