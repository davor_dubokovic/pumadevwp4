/*
* Description: Test class to improve the coverage of EP_PeriodicDataPurgeBatchClass
* @Author: Sanchit Dua
* Email: sanchit.dua@accenture.com (sanchit.dua@accenture.com)
*/

@isTest 
public class EP_PeriodicDataPurgeBatchClassTest {
    
    private static final String STATICRESOURCENAME = 'EP_IntegrationRecords';
    private static final String ERROR_MESSAGE = 'Please initialize the Custom Setting named EP_PeriodicDataPurge__c';
    
    @isTest
    /*
     * @Author: Sanchit Dua
     * Date: 01/12/15
     * Description: test method to test the batch named EP_PeriodicDataPurgeBatchClass
    */
    static void itShould(){  
        
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            Test.startTest(); 
            EP_TestDataUtility.createStatusList();
            List<EP_IntegrationRecord__c> intRecords = Test.loadData(EP_IntegrationRecord__c.sobjectType, STATICRESOURCENAME );
            EP_TestDataUtility.createPeriodicSetting();
            
            database.executebatch(new EP_PeriodicDataPurgeBatchClass()); 
            Test.stopTest(); 
        }
    } 
    
    // @isTest
    // static void checkCustomSetting(){
        // try {
            // Database.executeBatch(new EP_PeriodicDataPurgeBatchClass());
        // } catch (Exception e) {
            //Assert Error Message
            // System.assert( e.getMessage().contains(ERROR_MESSAGE), 
            // e.getMessage() );
        // }
    // }
}