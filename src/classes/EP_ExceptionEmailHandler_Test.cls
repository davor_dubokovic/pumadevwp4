/* 
   @Author Accenture
   @name <EP_LoggingService>
   @CreateDate <23/11/2015>
   @Description <this is the test class for EP_ExceptionEmailHandler>
   @Version <1.0>
 
*/
@isTest
private class EP_ExceptionEmailHandler_Test{
    
    public static final String SUBJECT = 'testSubject';
    public static final String FROMADDRESS ='abc@xyz.c';
    public static final String FROMNAME = 'testName';
      /**************************************************************************
        *@Description : This  method is used to test the Email handler class on creation of successful exception logs creation.*
        *@Params      : none                                                      *
        *@Return      : void                                                      *    
     **************************************************************************/
    private static testMethod void testEmailHandler(){
    
        // Create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        // Create the email body
        email.subject = SUBJECT;
        email.fromname = FROMNAME;
        env.fromAddress = FROMADDRESS;
        email.plainTextBody ='Apex script unhandled trigger exception by 00D25000000DsGI Source organization: 00D24000000pNm2 (null) EP_AccountTrigger: execution of AfterUpdate caused by: System.NullPointerException: Attempt to de-reference a null object Class.EP_AccountTriggerHelper.handlePlaceholderTankDipsBasedOnShipToStatus: line 544, column 1 Class.EP_AccountTriggerHandler.doAfterUpdate: line 57, column 1 Trigger.EP_AccountTrigger: line 27, column 1';
        EP_ExceptionEmailHandler eHandler = new EP_ExceptionEmailHandler();
        EP_Error_Handler_CS__c setting = new EP_Error_Handler_CS__c();
        setting.Name = 'Threshold';
        setting.Severity__c = 'Threshold';
        setting.Value__c = 3;
        insert setting;  
        
        EP_Error_Handler_CS__c setting1 = new EP_Error_Handler_CS__c();
        setting1.Name = 'ERR';
        setting1.severity__c = 'ERR';
        setting1.value__c = 5;
        insert setting1;
        Test.startTest();
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;    
        System.debug('email   '+email);
        Messaging.InboundEmailResult result = eHandler.handleInboundEmail(email, env);
        Test.stopTest();
        System.assertEquals(result.success, true);
        List<EP_Exception_Log__c> lExcepLogs=[Select id,EP_Method_Name__c,EP_Class_Name__c,EP_Application_Name__c from EP_Exception_Log__c];
        System.Assert(lExcepLogs.Size()==1); 
        System.debug('lExcepLogs     '+lExcepLogs);
        System.assertEquals(lExcepLogs[0].EP_Class_Name__c,Label.EP_ExceptionClassForTest);//Asserting as per the label i.e ExceptionEmail 
        System.assertEquals(lExcepLogs[0].EP_Method_Name__c,Label.EP_ExceptionClassMethodForTest);
        System.assertEquals(lExcepLogs[0].EP_Application_Name__c,EP_LoggingService.returnCurrentApp());      
    }
    
    /**************************************************************************
    *@Description : This  method is used to test the Email handler class on creation of successful exception logs creation.*
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/
    private static testMethod void testEmailHandler2(){
    
        // Create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        // Create the email body
        email.subject = SUBJECT;
        email.fromname = FROMNAME;
        env.fromAddress = FROMADDRESS;
        email.plainTextBody = 'user/organization:  '+EP_Common_Constant.CUSTOM_VALIDATION+EP_Common_Constant.SPACE+EP_Common_Constant.APEX_TRIGGER_SCRIPT_EXCEPTION+EP_Common_Constant.SPACE+EP_Common_Constant.BATCH+EP_Common_Constant.SPACE+EP_Common_Constant.VISUALFORCE+EP_Common_Constant.SPACE+EP_Common_Constant.APEX_SCRIPT_EXCEPTION; //+' Apex script unhandled trigger exception by 00D25000000DsGI Source organization: 00D24000000pNm2 (null) EP_AccountTrigger: execution of AfterUpdate caused by: System.NullPointerException: Attempt to de-reference a null object Class.EP_AccountTriggerHelper.handlePlaceholderTankDipsBasedOnShipToStatus: line 544, column 1 Class.EP_AccountTriggerHandler.doAfterUpdate: line 57, column 1 Trigger.EP_AccountTrigger: line 27, column 1';
        EP_ExceptionEmailHandler eHandler = new EP_ExceptionEmailHandler();  
        EP_Error_Handler_CS__c setting = new EP_Error_Handler_CS__c();
        setting.Name = 'Threshold';
        setting.Severity__c = 'Threshold';
        setting.Value__c = 3;
        insert setting;   
        
        EP_Error_Handler_CS__c setting1 = new EP_Error_Handler_CS__c();
        setting1.Name = 'ERR';
        setting1.severity__c = 'ERR';
        setting1.value__c = 5;
        insert setting1;
        Test.startTest();
            //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;    
            System.debug('email   '+email);
            Messaging.InboundEmailResult result = eHandler.handleInboundEmail(email, env);
        Test.stopTest();
    }
    /**************************************************************************
    *@Description : This  method is used to test the Email handler class for empty email plain text body.*
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/
    private static testMethod void testEmailHandler3(){
    
        // Create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        // Create the email body
        email.subject = SUBJECT;
        email.fromname = FROMNAME;
        env.fromAddress = FROMADDRESS;
        email.plainTextBody = EP_Common_Constant.BLANK;
        EP_ExceptionEmailHandler eHandler = new EP_ExceptionEmailHandler();  
        EP_Error_Handler_CS__c setting = new EP_Error_Handler_CS__c();
        setting.Name = 'Threshold';
        setting.Severity__c = 'Threshold';
        setting.Value__c = 3;
        insert setting;   
        
        EP_Error_Handler_CS__c setting1 = new EP_Error_Handler_CS__c();
        setting1.Name = 'ERR';
        setting1.severity__c = 'ERR';
        setting1.value__c = 5;
        insert setting1;
        Test.startTest();
            //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;    
            System.debug('email   '+email);
            Messaging.InboundEmailResult result = eHandler.handleInboundEmail(email, env);
        Test.stopTest();
    }
}