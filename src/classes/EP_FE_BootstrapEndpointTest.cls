/*********************************************************************************************
    @Author <Soumya Raj>
    @name <EP_FE_BootstrapEndpointTest>
    @CreateDate <12/04/2016>
    @Description <This class is to test Bootstrap endpoint class  > 
    @Version <1.0>
*********************************************************************************************/
      @istest
      private class EP_FE_BootstrapEndpointTest{
      private static User usrd;
      private static Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Account; 
      private static Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
   // private static Account bilTo =  EP_TestDataUtility.createBillToAccount(); // returns bill to account
      private static EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();// returns freight matrix
      private static  Integer totalSellToCount = 4;
      private static csord__Order__c od;
      private static Decimal funds = 100 ;


/*********************************************************************************************
    @Author <Soumya Raj>
    @name <TestBootstrapEndpoint>
    @CreateDate <12/04/2016>
    @Description  <This Methode is to cover doGet mehode for EP_FE_BootstrapEndpoint   >  
    @Version <1.0>
*********************************************************************************************/    
static testMethod void TestBootstrapEndpoint(){
      usrd= EP_FE_TestDataUtility.getRunAsUser();   
      User usr = [select id,IsActive  from user where id = :usrd.id limit 1];
      system.runas(usr){
      createActiveOrders();
      List<Schema.RecordTypeInfo> recordTypeList = Account.SObjectType.getDescribe().getRecordTypeInfos();     
      Test.startTest();
      EP_FE_BootstrapResponse resp = new EP_FE_BootstrapResponse(); 
      RestRequest req = new RestRequest(); 
      RestResponse res = new RestResponse();
      req.requestURI = '/services/apexrest/FE/V1/Bootstrap';  //Request URL
      req.httpMethod = 'GET';//HTTP Request Type
      RestContext.request = req;
      RestContext.response= res;
      resp =EP_FE_BootstrapEndpoint.doGet();
      Test.stopTest(); 
      System.assertEquals(recordTypeList.size(),resp.recordtype.Account.size()); // To Test the number account recordtypes and actual number of recordtypes 
      //commented by manisha on 17-01-2018--System.assertEquals('GBP',resp.credit[0].currencyCode); //To check currency code of billto is fetched when sell to has bill to id populated.
          System.assertEquals('AUD',resp.credit[0].currencyCode); //To check currency code of billto is fetched when sell to has bill to id populated.
      }

}


/*********************************************************************************************
    @Author <Soumya Raj>
    @name <testSellTOShipToAccounts>
    @CreateDate <12/04/2016>
    @Description  <This Methode is to make test data for SellTo Accounts , ShipTo Accounts and Tank associated Tank dips    >  
    @Version <1.0>
*********************************************************************************************/   
static testMethod void testSellTOShipToAccounts(){
      Id tankDipPlaceholderRecordTypeId = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(EP_Common_Constant.PLACEHOLDER).getRecordTypeId();
      Account shipTo = null; 
      Account shipTo1 = null; 
      Id recordTypeId = null;
      Account sellTo = EP_TestDataUtility.createSellToAccount(null,freightMatrix.id);
      Database.insert (sellto);
      sellto.EP_Status__c=EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
      Database.upsert (sellTo);
      sellTo.EP_Status__c=EP_Common_Constant.STATUS_ACTIVE;
      sellTo.EP_Delivery_Type__c=EP_Common_Constant.DELIVERY;
      Database.upsert (sellTo);
      EP_Customer_Payment__c cust = EP_FE_TestDataUtility.CreateCustomer();
      cust.EP_Bill_To__c = sellTo.id;
      Database.insert (cust) ; 
      Test.startTest();
      //insert Shipto and associated tank/tankdips
      ID shipToRecordTYpeId = schema.SobjectType.Account.getRecordTypeInfosByName().get('VMI Ship To').getRecordTypeId();
      //Id tankDipPlaceholderRecordTypeIdd = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(EP_Common_Constant.PLACEHOLDER).getRecordTypeId();
      shipTo = EP_TestDataUtility.createShipToAccount(sellTo.id, shipToRecordTYpeId);
      shipTo.recordtypeId = shipToRecordTYpeId   ;
      Database.insert (shipto);
      EP_Tank__c tank = EP_TestDataUtility.createTestEP_Tank(shipTo);
      shipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
      Database.upsert (shipTo);
      //Need to have a tand associated with shipt to activate it 
      shipTo.EP_Status__c=EP_Common_Constant.STATUS_ACTIVE;
      shipTo.EP_Tank_Dips_Schedule_Time__c = '01:00';
      //shipTo.EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
      Database.upsert (shipTo);
      //Shipto Insert 
      shipTo1 = EP_TestDataUtility.createShipToAccount(sellTo.id, recordTypeId );
      Database.insert (shipto1);
      product2 prod = [Select id from product2 limit 1] ;
      EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(shipTo.Id,prod.Id);
      Database.insert (tankObj);
      EP_Tank_Dip__c tankDip1 = EP_TestDataUtility.createTestRecordsForTankDip(tankObj.Id); 
      tankDip1.recordtypeid = tankDipPlaceholderRecordTypeId ;
      Database.insert (tankDip1) ; 
      Test.stopTest();
      EP_FE_BootstrapResponse resp = new EP_FE_BootstrapResponse(); 
      RestRequest req = new RestRequest(); 
      RestResponse res = new RestResponse();
      req.requestURI = '/services/apexrest/FE/V1/Bootstrap';  //Request URL
      req.httpMethod = 'GET';//HTTP Request Type
      RestContext.request = req;
      RestContext.response= res;
      resp =EP_FE_BootstrapEndpoint.doGet();
      
      //system.assertequals(resp.dipSites[0].shipToId,shipTo.Id);
      //system.assertequals(resp.dipSites[0].tankDates[0].isMissingDips,True);  

}
  
/*********************************************************************************************
    @Author <Soumya Raj>
    @name <testStockholdingLocation>
    @CreateDate <12/04/2016>
    @Description  <This Methode is to make test data for Stockholding Location Test    >  
    @Version <1.0>
*********************************************************************************************/   
static testMethod void testStockholdingLocation(){
      EP_TestDataUtility.stopAccountTrigger();
      
      createActiveOrders();
      Id recordTypeId = null;
      //Insert Terminal
      
      Account terminal = EP_TestDataUtility.createStockHoldingLocation();
      terminal.EP_Status__c= EP_Common_Constant.STATUS_PROSPECT;
      Database.insert (terminal) ;
      terminal.EP_Status__c= EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
      Database.upsert (terminal);
      terminal.EP_Status__c=EP_Common_Constant.STATUS_ACTIVE;
      Database.upsert (terminal);
      //Insert Sellto 
      Account sellTo = EP_TestDataUtility.createSellToAccount(null,freightMatrix.id);
      Database.insert (sellto);
      sellto.EP_Status__c=EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
      Database.upsert (sellTo);
      sellTo.EP_Status__c=EP_Common_Constant.STATUS_ACTIVE;
      sellTo.EP_Delivery_Type__c=EP_Common_Constant.EX_RACK;
      sellTo.EP_Storage_Location__c=terminal.id;
      Database.upsert (sellTo);
      //Insert Shipto
      Account  shipTo = EP_TestDataUtility.createShipToAccount(sellTo.id, recordTypeId );
      shipTo.EP_Storage_Location__c=terminal.id;
      Database.insert (shipto);
      
      Company__c company = EP_TestDataUtility.createCompany( 'AUN1');
      Database.insert( company );
        
      Account venderAccount = new Account();
      venderAccount.RecordTypeId = [select Id from recordType where sObjectType = 'Account' and developername = 'EP_Vendor' limit 1][0].Id;
      /* TFS fix 45559,45560,45567,45568 start EP_NAV_ID__c deprecated, EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c and EP_Vendor_Type__c is replaced by EP_VendorType__c*/
      venderAccount.EP_Source_Entity_ID__c = 'TestVenderId';
      /* TFS fix 45559,45560,45567,45568 end*/
      venderAccount.EP_Puma_Company__c = company.Id;
      venderAccount.BillingCity = 'testCity';
      venderAccount.Name = 'testVender';
      insert venderAccount;
          
      //Insert Stockholding location 
      EP_Stock_Holding_Location__c stockList = EP_TestDataUtility.createStockLocation(null,true);
      stockList.EP_Sell_To__c =sellTo.id;
      stockList.Stock_Holding_Location__c=terminal.id;
      stockList.EP_Trip_Duration__c=100;
      //stockList.EP_Transporter__c = venderAccount.Id;
      Database.upsert (stockList);  
      
      Test.startTest();
      EP_FE_BootstrapResponse resp = new EP_FE_BootstrapResponse(); 
      RestRequest req = new RestRequest(); 
      RestResponse res = new RestResponse();
      req.requestURI = '/services/apexrest/FE/V1/Bootstrap';  //Request URL
      req.httpMethod = 'GET';//HTTP Request Type
      RestContext.request = req;
      RestContext.response= res;
      req.addparameter('content','fieldset,user,statusMap,sellToList,recordtype,permissions,accountOverallStatus,dipSites,dictionary,dashboard,credit,configuration');
      resp =EP_FE_BootstrapEndpoint.doGet();
      Test.stopTest(); 
}  
        
   

  /*********************************************************************************************
    *@Description : This is the Test method Create Order.                  
    *@Params      :                    
    *@Return      : Void                                                                             
  *********************************************************************************************/
    @testSetup static void testCreateOrder(){
    od = EP_FE_TestDataUtility.createNonVMIOrder();        
    
    }       


/*********************************************************************************************
    @Author <Soumya Raj>
    @name <createOrders>
    @CreateDate <12/04/2016>
    @Description  <This Methode is to cover getDashboard mehode for EP_FE_BootstrapEndpoint   >  
    @Version <1.0>
*********************************************************************************************/  
static testMethod void createActiveOrders(){

    csord__Order__c odd = [select id from csord__Order__c limit 1];
    odd.status__c = 'Activated' ;
    Database.update (odd) ;
    }
  /*********************************************************************************************
  @Author <Soumya Raj>
  @name <testIsPayementTermsTrue>
  @CreateDate <12/04/2016>
  @Description  <This Methode is to make test data for sell to with is pre payment terms true   >  
  @Version <1.0>
 *********************************************************************************************/   
    /*  static testMethod void testIsPayementTermsTrue(){
      Account shipTo = null; 
      Account bilTo =  EP_TestDataUtility.createBillToAccount(); // returns bill to account
      Database.insert (bilTo);
      bilTo.EP_Is_Valid_Address__c = true;
      Database.update (bilTo);
      bilTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
      Database.upsert (bilTo);
      bilTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;   
      Database.upsert (bilTo); 
      Id recordTypeId = null;
      Order odd = [select id from order limit 1];
      EP_Payment_Term__c  paymentTerms =  EP_TestDataUtility.createPaymentTerm();
      Database.insert (paymentTerms) ;
      paymentTerms.Name = 'pp';
      Database.update (paymentTerms);
      //Insert Customer 
      EP_Customer_Payment__c  cust = EP_FE_TestDataUtility.CreateCustomer();
      //Insert Sellto
      Account sellto =  EP_TestDataUtility.createSellToAccount(NULL, NULL);
      Database.insert (sellto);
      sellto.EP_Status__c=EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
      Database.upsert (sellTo);
      sellTo.EP_Status__c=EP_Common_Constant.STATUS_ACTIVE;
      sellTo.EP_Delivery_Type__c=EP_Common_Constant.EX_RACK;
      sellTo.EP_Payment_Term_Lookup__c = paymentTerms.id;
      sellTo.Ep_Bill_To_Account__c = bilTo.id;
      Database.update (sellTo);
      //Insert Ship To 
      shipTo = EP_TestDataUtility.createShipToAccount(sellTo.id, recordTypeId );
      Database.insert (shipto);
      //Need to have a tand associated with shipt to activate it 
      EP_Tank__c tank = EP_TestDataUtility.createTestEP_Tank(shipTo);
      shipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
      Database.upsert (shipTo);  
      shipTo.EP_Status__c=EP_Common_Constant.STATUS_ACTIVE;
      shipTo.EP_Tank_Dips_Schedule_Time__c = '01:00';
      //shipTo.EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
      Database.upsert (shipTo);    
      Test.startTest();
      EP_FE_BootstrapResponse resp = new EP_FE_BootstrapResponse(); 
      RestRequest req = new RestRequest(); 
      RestResponse res = new RestResponse();
      req.requestURI = '/services/apexrest/FE/V1/Bootstrap';  //Request URL
      req.httpMethod = 'GET';//HTTP Request Type
      RestContext.request = req;
      RestContext.response= res;
      resp =EP_FE_BootstrapEndpoint.doGet();
      Test.stopTest(); 
      system.assertequals(resp.sellToList[0].record.EP_Is_Pre_Pay__c,True);
        } */
        
     
/*********************************************************************************************
    @Author <Soumya Raj>
    @name <testOverdueInvoice>
    @CreateDate <12/04/2016>
    @Description  <This Methode is to cover account statement Item test utility  >  
    @Version <1.0>
********************************************************************************************  
      static testMethod Void testAccountStatementItem(){
      
      Account billTo =  EP_TestDataUtility.createBillToAccount(); // returns bill to account
      Database.insert (billTo);
      billTo.EP_Is_Valid_Address__c = true;
      billTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
      Database.upsert (billTo);
      billTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;   
      Database.upsert (billTo); 
      EP_FE_TestDataUtility.CreateCustomerAccountStatementItem(billTo);
      EP_FE_TestDataUtility.CreateCustomerAccountStatement(billTo);
      Account sellTo = EP_TestDataUtility.createSellToAccount(billTo.Id,freightMatrix.id);
      Database.insert (sellto);
      sellto.EP_Status__c=EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
      sellto.EP_Bill_To_Account__c = billTo.id;
      Database.upsert (sellTo);
      sellTo.EP_Status__c=EP_Common_Constant.STATUS_ACTIVE;
      Database.upsert (sellTo);
      //Create Test Finance user 
     // user financeUser = EP_FE_TestDataUtility.getFinanceRunAsUser(sellTo);
      //User usrdFinance = [select id from user where id = :financeUser.id limit 1];
      Id tankDipPlaceholderRecordTypeId = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(EP_Common_Constant.PLACEHOLDER).getRecordTypeId();
      Schema.DescribeSObjectResult objSchema = Schema.SObjectType.EP_Invoice__c  ; 
      Map<String,Schema.RecordTypeInfo> mapInvoiceIdInfo = objSchema.getRecordTypeInfosByName(); 
      EP_Invoice__c  invoiceObj =  EP_TestDataUtility.createInvoice(sellto);
      //invoiceObj.EP_Overdue__c = TRUE ;
      id recordtype = mapInvoiceIdInfo.get('Sales Invoice').getRecordTypeId();
      invoiceObj.EP_Is_Proforma_Invoice__c = FALSE  ; 
      invoiceObj.EP_Invoice_Due_Date__c=  Date.Today().addDays(-2) ;
      invoiceObj.EP_Remaining_Balance__c = 10;
      invoiceObj.EP_Invoice_Issue_Date__c = system.today();
      invoiceObj.EP_Bill_To__c = sellto.Id;
      invoiceObj.EP_Invoice_Total__c = 100 ;
      invoiceObj.recordTypeId =  recordtype  ;
      invoiceObj.EP_Invoice_Key__c ='414';
      Database.insert (invoiceObj);
      EP_Bank_Account__c bankobj = EP_TestDataUtility.createBankAccount(billto.id);
      Database.insert (bankobj) ;
      
      //create order 
      Order orderOb = EP_FE_TestDataUtility.createNonVMIOrder();
      //test data for EP_Order_Customer_Invoice__c
      EP_Order_Customer_Invoice__c custInvoice = new EP_Order_Customer_Invoice__c();
      custInvoice.EP_Customer_Invoices__c = invoiceObj.id;
      custInvoice.EP_Order__c = orderOb.id ;
      Database.insert (custInvoice) ;

      usrd= EP_FE_TestDataUtility.getRunAsUser();   
      User usr = [select id,IsActive  from user where id = :usrd.id limit 1];
      system.runas(usr){
      
      EP_CustomerAccountStatementUtility ob = new EP_CustomerAccountStatementUtility();
      EP_FE_BootstrapResponse resp = new EP_FE_BootstrapResponse(); 
      RestRequest req = new RestRequest(); 
      RestResponse res = new RestResponse();
      req.requestURI = '/services/apexrest/FE/V1/Bootstrap';  //Request URL
      req.httpMethod = 'GET';//HTTP Request Type
      RestContext.request = req;
      RestContext.response= res;
      
      resp =EP_FE_BootstrapEndpoint.doGet();
      Test.startTest();
      List<EP_FE_InvoiceDetail> invoiceList = EP_FE_BootstrapEndpoint.getOverdueInvoices(resp);  
      Test.stopTest(); 


      }
      
      }Commented by Manish*/
    
    
    
}