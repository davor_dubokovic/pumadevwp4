/***************************************************************
*  @Author <Accenture>                                         *
*  @Name <EP_User_CompanyDomainObject>                        *
*  @CreateDate <1/11/2017>                                     *
*  @Description <Domain object to store User Company Mapping and 
                    User Company Mapping related status>             *
*  @Version <1.0>                                              *
****************************************************************/
 public class EP_User_CompanyDomainObject{
    private EP_User_Company__c localUserCompany;
    private EP_UserCompanyService service;
    private EP_UserCompanyMapper UserCompanyMapper;
    


/****************************************************************
* @author       Accenture                                       *
* @name         EP_User_CompanyDomainObject                    *
* @description  Constructor of class to set new User Company Mapping   *
* @param        EP_User_Company__c                               *
* @return       NA                                              *
****************************************************************/ 
    public EP_User_CompanyDomainObject(EP_User_Company__c UserCompanyObj){
    	EP_GeneralUtility.Log('Public','EP_User_CompanyDomainObject','EP_User_CompanyDomainObject');
        this.localUserCompany = UserCompanyObj;
        this.service = new EP_UserCompanyService(this);
        this.UserCompanyMapper = new EP_UserCompanyMapper(this);
    }

    
/****************************************************************
* @author       Accenture                                       *
* @name         doActionAfterInsert                             *
* @description  This method is to be called thorugh trigger to 
                execute all after Insert event logic/actions    *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public void doActionAfterInsert(){
        EP_GeneralUtility.Log('Public','EP_User_CompanyDomainObject','doActionAfterUpdate');
        service.doAfterInsertHandle();  // add try catch and and logging methods
    }    

/****************************************************************
* @author       Accenture                                       *
* @name         doActionAfterUpdate                             *
* @description  This method is to be called thorugh trigger to 
                execute all after update event logic/actions    *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public void doActionAfterDelete(){
        EP_GeneralUtility.Log('Public','EP_User_CompanyDomainObject','doActionAfterUpdate');
        service.doAfterDeleteHandle(); 
    }




/****************************************************************
* @author       Accenture                                       *
* @name         getUserCompanyObject                          *
* @description  This method is used to get current credit 
                                Holding object                  *
* @param        NA                                              *
* @return       List User Company Mapping                             *
****************************************************************/     
    public EP_User_Company__c getUserCompanyObject(){
        EP_GeneralUtility.Log('Public','EP_User_CompanyDomainObject','getUserCompanyObject');
        return localUserCompany;
    }



}