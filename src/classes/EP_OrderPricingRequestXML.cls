/*
    @Author          Accenture
    @Name            EP_OrderPricingRequestXML
    @CreateDate      04/18/2017
    @Description     This class  is used to generate outbound XML's of order Pricing request With NAV
    @Version         1.0
    @Reference       NA
*/
public class EP_OrderPricingRequestXML extends EP_GenerateOrderRequestXML{
    //Changes for #60147
    public dom.XmlNode PayloadNode;
   /**
    * @author           Accenture
    * @name             createXML
    * @date             04/18/2017
    * @description      This method is used to create XML for Order to Sync with NAV
    * @param            NA
    * @return           NA
    */
    public override string createXML(){
        EP_GeneralUtility.Log('Public','EP_OrderPricingRequestXML','createXML');

        string pricingPayloadXML = super.createXML();
        return pricingPayloadXML.unescapeXml();
    } 
    
    /**
    * @author           Accenture
    * @name             init
    * @date             04/18/2017
    * @description      This method is used to fetch data from Database which will be require to wrapped into XML
    * @param            NA
    * @return           NA
    */
    public override virtual void init(){
        EP_GeneralUtility.Log('Public','EP_OrderPricingRequestXML','init');
        PayloadNode = doc.createRootElement(EP_OrderConstant.Payload, null, null);
        //MSGNode = doc.createRootElement(EP_OrderConstant.MSG, null, null);
        
        // Quering the Order object for the passed record id
     //   EP_OrderMapper ordermapper = new EP_OrderMapper();

       // orderobj = ordermapper.getRecordById(recordid);  
        
        // Initiating orderlines wrapper data
       // setOrderLineItems();
        
        // set order items
       // setOrderItems();
        
        // Get relevant custom setting
        isEncryptionEnabled = false;
        EP_CS_OutboundMessageSetting__c outboundMsgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(messageType);
        if(outboundMsgSetting != null){
            isEncryptionEnabled = outboundMsgSetting.Payload_Encoded__c;
        }
    }
	
	public static String generateSeqGuid() {
        Blob b = Crypto.GenerateAESKey(256);
        String h = EncodingUtil.ConvertTohex(b);
        return  h.SubString(0,18)+ '-' + h.SubString(18,32) + '-' + h.SubString(32,36);
    }
    
    /**
    * @author           Accenture
    * @name             createPayload
    * @date             04/18/2017
    * @description      This method is used to create Payload for Order to Sync with NAV
    * @param            NA
    * @return           NA
    */ 
    public override void createPayload(){
        EP_GeneralUtility.Log('Public','EP_OrderPricingRequestXML','createPayload');
       
        DateTime dt = DateTime.now();
        DOM.Document tempDoc = new DOM.Document();
        Dom.XMLNode pricingRequestNode = tempDoc.createRootElement(EP_OrderConstant.pricingRequest,null, null);   
        Dom.XMLNode requestHeaderNode = pricingRequestNode.addChildElement(EP_OrderConstant.requestHeader,null,null);
        CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();
		String seqGuid = generateSeqGuid();
        String onRun =order.run;
        String onOffRun;
        system.debug('onRun is :' +onRun);
        if(onRun!= null && onRun== 'On'){
            onOffRun= 'True';
        }else if(onRun== 'Off'){
            onOffRun= 'False';
        }else{
            onOffRun= '';
        }
        
        system.debug('onOffRun is :' +onOffRun);
        system.debug('messageId is :' +messageId);
        system.debug('csOrderSetting.Pricing_Message_Id__c is :' +csOrderSetting.Pricing_Message_Id__c);
        String seqid = EP_IntegrationUtil.reCreateSeqId(messageId, csOrderSetting.Pricing_Message_Id__c); // strict format
        system.debug('csOrderSetting.Pricing_Message_Id__c is :' +csOrderSetting.Pricing_Message_Id__c);
        requestHeaderNode.addChildElement(EP_OrderConstant.seqId,null,null).addTextNode(seqGuid); //Value for seqId
        system.debug('seqGuid is :' +seqGuid);
        
        try {
            requestHeaderNode.addChildElement(EP_OrderConstant.companyCode,null,null).addTextNode(getValueforNode(order.lineItems[0].companyCode)); //Value for companyCode
            system.debug('order.lineItems[0].companyCode is :' +order.lineItems[0].companyCode);
            requestHeaderNode.addChildElement(EP_OrderConstant.priceType,null,null).addTextNode('Fuel Price'); //Value for priceType // strict value
            system.debug('onOffRun is :' +onOffRun);
            requestHeaderNode.addChildElement(EP_OrderConstant.priceRequestSource,null,null).addTextNode('');
            system.debug('Fuel Price is Fuel Price');
            requestHeaderNode.addChildElement(EP_OrderConstant.currencyCode,null,null).addTextNode(getValueforNode(order.currencyCode));
            system.debug('order.currencyCode is :' +order.currencyCode);
            requestHeaderNode.addChildElement(EP_OrderConstant.deliveryType,null,null).addTextNode(getValueforNode(order.deliveryType)); //Value for deliveryType
            system.debug('order.deliveryType is :' +order.deliveryType);
            requestHeaderNode.addChildElement(EP_OrderConstant.customerId,null,null).addTextNode(getValueforNode(order.accountNumber)); //Value for customerId
            system.debug('order.accountNumber is :' +order.accountNumber);
            requestHeaderNode.addChildElement(EP_OrderConstant.shipToId,null,null).addTextNode(getValueforNode(order.shipTo)); //Value for shipToId
            system.debug('order.shipTo is :' +order.shipTo);
            requestHeaderNode.addChildElement(EP_OrderConstant.supplyLocationId,null,null).addTextNode(getValueforNode(order.supplyLocation)); //Value for supplyLocationId //41113
            system.debug('order.supplyLocation is :' +order.supplyLocation);
            requestHeaderNode.addChildElement(EP_OrderConstant.transporterId,null,null).addTextNode(getValueforNode(order.transporterId)); //Value for transporterId
            system.debug('order.transporterId is :' +order.transporterId);
            requestHeaderNode.addChildElement(EP_OrderConstant.supplierId,null,null).addTextNode(getValueforNode(order.supplierId));
            system.debug('order.supplierId is :' +order.supplierId);
            requestHeaderNode.addChildElement(EP_OrderConstant.priceDate,null,null).addTextNode(getValueforNode(order.requestDate)); //Value for priceDate // must be present
            system.debug('order.requestDate is :' +order.requestDate);
            requestHeaderNode.addChildElement(EP_OrderConstant.onRun,null,null).addTextNode(getValueforNode(onOffRun)); //Value for onRun
            system.debug('onOffRun is :' +onOffRun);
            requestHeaderNode.addChildElement(EP_OrderConstant.orderId,null,null).addTextNode(''); //Value for order id
            system.debug('EP_OrderConstant.orderId is :');
            requestHeaderNode.addChildElement(EP_OrderConstant.applyOrderQuantity,null,null).addTextNode(''); //Value for totalOrderQuantity
            system.debug('EP_OrderConstant.applyOrderQuantity is :');
            requestHeaderNode.addChildElement(EP_OrderConstant.totalOrderQuantity,null,null).addTextNode(getValueforNode(order.quantity)); //Value for totalOrderQuantity
            system.debug('order.quantity is :' +order.quantity);
            requestHeaderNode.addChildElement(EP_OrderConstant.priceConsolidationBasis,null,null).addTextNode('Summarised price'); /// strict value
            system.debug('EP_OrderConstant.priceConsolidationBasis is :' +EP_OrderConstant.priceConsolidationBasis);
            requestHeaderNode.addChildElement(EP_OrderConstant.versionNr,null,null).addTextNode(getValueforNode(dt.format('yyyyMMdd\'T\'hhmmss') + '.' + dt.millisecondGmt()));
            system.debug('EP_OrderConstant.versionNr is :' + (dt.format('yyyyMMdd\'T\'hhmmss') + '.' + dt.millisecondGmt()));

            system.debug('EP_OrderPricingRequestXML createPayload, requestHeaderNode = ' + requestHeaderNode);
            system.debug('EP_OrderPricingRequestXML companyCode = ' + order.lineItems[0].companyCode);

            Dom.XMLNode requestLinesNode = pricingRequestNode.addChildElement(EP_OrderConstant.requestLines,null,null);
			
			system.debug('EP_OrderPricingRequestXML pricingRequestNode = ' + pricingRequestNode);
			system.debug('EP_OrderPricingRequestXML requestLinesNode = ' + requestLinesNode);
			system.debug('EP_OrderPricingRequestXML order.lineItems = ' + order.lineItems);
        
            for(EP_OrderDataStub.LineItem orderLine: order.lineItems){
				system.debug('EP_OrderPricingRequestXML orderLine = ' + orderLine);
                Dom.XMLNode lineNode = requestLinesNode.addChildElement(EP_OrderConstant.line,null,null);
				system.debug('EP_OrderPricingRequestXML EP_OrderConstant.line = ' + EP_OrderConstant.line);
				system.debug('EP_OrderPricingRequestXML lineNode = ' + lineNode);
                lineNode.addChildElement(EP_OrderConstant.seqId,null,null).addTextNode(getValueforNode(seqGuid));
				system.debug('EP_OrderPricingRequestXML seqGuid = ' + seqGuid);
				system.debug('EP_OrderPricingRequestXML EP_OrderConstant.seqId = ' + EP_OrderConstant.seqId);
				system.debug('EP_OrderPricingRequestXML lineNode = ' + lineNode);
                lineNode.addChildElement(EP_OrderConstant.orderId,null,null).addTextNode('');
				system.debug('EP_OrderPricingRequestXML EP_OrderConstant.orderId = ' + EP_OrderConstant.orderId);
				system.debug('EP_OrderPricingRequestXML lineNode = ' + lineNode);
                lineNode.addChildElement(EP_OrderConstant.lineItemId,null,null).addTextNode(getValueforNode(orderLine.lineItemId)); // '0012300564'
				system.debug('EP_OrderPricingRequestXML orderLine.lineItemId = ' + orderLine.lineItemId);
				system.debug('EP_OrderPricingRequestXML EP_OrderConstant.lineItemId = ' + EP_OrderConstant.lineItemId);
				system.debug('EP_OrderPricingRequestXML lineNode = ' + lineNode);
                lineNode.addChildElement(EP_OrderConstant.itemId,null,null).addTextNode(getValueforNode(orderLine.productCode)); //Value for itemId  // strict format and value 2000005
				system.debug('EP_OrderPricingRequestXML orderLine.productCode = ' + orderLine.productCode);
				system.debug('EP_OrderPricingRequestXML EP_OrderConstant.itemId = ' + EP_OrderConstant.itemId);
				system.debug('EP_OrderPricingRequestXML lineNode = ' + lineNode);
                lineNode.addChildElement(EP_OrderConstant.quantity,null,null).addTextNode(getValueforNode(orderLine.quantity)); //Value for quantity
				system.debug('EP_OrderPricingRequestXML orderLine.quantity = ' + orderLine.quantity);
				system.debug('EP_OrderPricingRequestXML EP_OrderConstant.quantity = ' + EP_OrderConstant.quantity);
				system.debug('EP_OrderPricingRequestXML lineNode = ' + lineNode);
                lineNode.addChildElement(EP_OrderConstant.deliveryType,null,null).addTextNode(getValueforNode(order.deliveryType));
				system.debug('EP_OrderPricingRequestXML order.deliveryType = ' + order.deliveryType);
				system.debug('EP_OrderPricingRequestXML EP_OrderConstant.deliveryType = ' + EP_OrderConstant.deliveryType);
				system.debug('EP_OrderPricingRequestXML lineNode = ' + lineNode);
                lineNode.addChildElement(EP_OrderConstant.shipToId,null,null).addTextNode(getValueforNode(order.shipTo));
				system.debug('EP_OrderPricingRequestXML order.shipTo = ' + order.shipTo);
				system.debug('EP_OrderPricingRequestXML EP_OrderConstant.shipToId = ' + EP_OrderConstant.shipToId);
				system.debug('EP_OrderPricingRequestXML lineNode = ' + lineNode);
                lineNode.addChildElement(EP_OrderConstant.supplyLocationId,null,null).addTextNode(getValueforNode(order.supplyLocation));
				system.debug('EP_OrderPricingRequestXML order.supplyLocation = ' + order.supplyLocation);
				system.debug('EP_OrderPricingRequestXML EP_OrderConstant.supplyLocationId = ' + EP_OrderConstant.supplyLocationId);
				system.debug('EP_OrderPricingRequestXML lineNode = ' + lineNode);
                lineNode.addChildElement(EP_OrderConstant.onRun,null,null).addTextNode(getValueforNode(onOffRun));
				system.debug('EP_OrderPricingRequestXML onOffRun = ' + onOffRun);
				system.debug('EP_OrderPricingRequestXML EP_OrderConstant.onRun = ' + EP_OrderConstant.onRun);
				system.debug('EP_OrderPricingRequestXML lineNode = ' + lineNode);
                lineNode.addChildElement(EP_OrderConstant.priceDate,null,null).addTextNode(getValueforNode(order.requestDate));
				system.debug('EP_OrderPricingRequestXML order.requestDate = ' + order.requestDate);
				system.debug('EP_OrderPricingRequestXML EP_OrderConstant.priceDate = ' + EP_OrderConstant.priceDate);
				system.debug('EP_OrderPricingRequestXML lineNode = ' + lineNode);
            }

            //end of request lines node 
            Dom.XmlNode AnyNode = PayloadNode.addChildElement(EP_OrderConstant.any0,null,null);
            // Encoding payload by calling encode XML method in superclass
            AnyNode.addTextNode(encodeXML(tempDoc));
        } catch (Exception e) {
            System.Debug('Failed to construct Order Pricing Request XML. e = ' + e.getMessage());
        }
    }
    
    /**
    * @author           Accenture
    * @name             createStatusPayLoad
    * @date             04/18/2017
    * @description      This method is used to create Status XML tags
    * @param            NA
    * @return           NA
    */ 
    public override void createStatusPayLoad(){
        //#60147 User Story Start
        //Since for pricing Message Node and Status Node is not part of the XML Message hance override this method with empty implemetation 
        EP_GeneralUtility.Log('Public','EP_OrderPricingRequestXML','createStatusPayLoad');
        //MSGNode.addChildElement(EP_OrderConstant.STATUS_PAYLOAD,null,null);
        //#60147 User Story End
    }
    
    /**
    * @author           Accenture
    * @name             createStatusPayLoad
    * @date             04/18/2017
    * @description      This method is used to create Status XML tags
    * @param            NA
    * @return           NA
    */ 
    public override void createHeaderNode(){
        //#60147 User Story Start
        //Since for pricing Message Node and Status Node is not part of the XML Message hance override this method with empty implemetation 
        EP_GeneralUtility.Log('Public','EP_OrderPricingRequestXML','createHeaderNode');
        //MSGNode.addChildElement(EP_OrderConstant.STATUS_PAYLOAD,null,null);
        //#60147 User Story End
    }
	
}