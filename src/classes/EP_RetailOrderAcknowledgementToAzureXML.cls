public with sharing class EP_RetailOrderAcknowledgementToAzureXML {

  
  public EP_RetailOrderAcknowledgementToAzureXML ()
  {
     
  }
 
 /**
* @author           Accenture
* @name             createPayload 
* @date             13/Nov/2017
* @description      This method is used to create Payload for Account to Sync with NAVPE
* @param            NA
* @return           NA
*/ 
    public static void createPayload(){
        
        DOM.Document tempDoc = new DOM.Document();
        Dom.XMLNode MSGNode = tempDoc.createRootElement(EP_AccountConstant.MSG, null, null);
        EP_GeneralUtility.Log('Public','EP_CustomerCreationWithNAVPEXML','createPayload');
        Dom.XMLNode PayloadNode = MSGNode.addChildElement(EP_AccountConstant.payload ,null,null);
        Dom.XmlNode AnyNode = PayloadNode.addChildElement(EP_AccountConstant.any0,null, null);
        Dom.XmlNode salesData = AnyNode.addChildElement(EP_Common_Constant.salesData,null, null);
        
        Dom.XmlNode header = salesData.addChildElement(EP_Common_Constant.header,null,null);
        Dom.XmlNode lines  = salesData.addChildElement(EP_Common_Constant.lines,null,null);
        
        system.debug('lines+++++++++++++++++++'+lines);
        system.debug('header+++++++++++++++++++'+header);
        system.debug('EP_StagingRetailOrderStub+++++++++++++++++++'+header);
        
        
        
        //Identifier node
        /*identifierNode.addChildElement(EP_StagingRetailOrderStub.CUSTID,null,null).addTextNode(getValueforNode(objAccount.EP_Account_Id__c));
        identifierNode.addChildElement(EP_StagingRetailOrderStub.ENTRPSID,null,null).addTextNode(getValueforNode(objAccount.EP_EnterpriseId__c));  
        customerNode.addChildElement(EP_StagingRetailOrderStub.CUSTCATGRY,null,null).addTextNode(getValueforNode(objAccount.EP_Customer_Category__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.NAME,null,null).addTextNode(getValueforNode(objAccount.name));
        customerNode.addChildElement(EP_StagingRetailOrderStub.NAME2,null,null).addTextNode(getValueforNode(objAccount.EP_Account_Name_2__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.ADDRESS,null,null).addTextNode(getValueforNode(objAccount.BillingStreet));
        customerNode.addChildElement(EP_StagingRetailOrderStub.CITY,null,null).addTextNode(getValueforNode(objAccount.BillingCity));
        customerNode.addChildElement(EP_StagingRetailOrderStub.PHONE,null,null).addTextNode(getValueforNode(objAccount.phone));
        customerNode.addChildElement(EP_StagingRetailOrderStub.MOBILEPHONE,null,null).addTextNode(getValueforNode(objAccount.EP_Mobile_Phone__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.CURRENCYID,null,null).addTextNode(getValueforNode(objAccount.CurrencyIsoCode));
        customerNode.addChildElement(EP_StagingRetailOrderStub.LANGCODE,null,null).addTextNode(getValueforNode(objAccount.EP_Language_Code__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.CNTRYCODE,null,null).addTextNode(getValueforNode(objAccount.EP_Country_Code__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.BILLTOCUSTID,null,null).addTextNode(getValueforNode(objAccount.EP_BillToCustomerNr__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.FAX,null,null).addTextNode(getValueforNode(objAccount.Fax));
        
        customerNode.addChildElement(EP_StagingRetailOrderStub.POSTCODE,null,null).addTextNode(getValueforNode(objAccount.BillingPostalCode));
        customerNode.addChildElement(EP_StagingRetailOrderStub.COUNTY,null,null).addTextNode(getValueforNode(objAccount.BillingState));
        customerNode.addChildElement(EP_StagingRetailOrderStub.EMAIL,null,null).addTextNode(getValueforNode(objAccount.EP_Email__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.WEBSITE,null,null).addTextNode(getValueforNode(objAccount.Website));        
        customerNode.addChildElement(EP_StagingRetailOrderStub.RQSTDPMTMTHDID,null,null).addTextNode(getValueforNode(objAccount.EP_RequestedPaymentMethod__r.EP_Payment_Method_Code__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.REQPTMTRM,null,null).addTextNode(getValueforNode(objAccount.EP_RequestedPaymentTerms__r.EP_Payment_Term_Code__c)); 
        customerNode.addChildElement(EP_StagingRetailOrderStub.PCKGDRQSTDPYMNTTERM,null,null).addTextNode(getValueforNode(objAccount.EP_Requested_Packaged_Payment_Terms__r.EP_Payment_Term_Code__c)); 
        customerNode.addChildElement(EP_StagingRetailOrderStub.BILLBASIS,null,null).addTextNode(getValueforNode(objAccount.EP_Billing_Basis__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.BILLMTHD,null,null).addTextNode(getValueforNode(objAccount.EP_Billing_Method__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.BILLFRQNCY,null,null).addTextNode(getValueforNode(objAccount.EP_Billing_Frequency__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.BILLCONSOLBASIS,null,null).addTextNode(getValueforNode(objAccount.EP_Invoice_Consolidation_basis__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.DLVRYTYPE,null,null).addTextNode(getValueforNode(objAccount.EP_Delivery_Type__C));
        customerNode.addChildElement(EP_StagingRetailOrderStub.MANUALINVCINGALLOWED,null,null).addTextNode(getValueforNode(objAccount.EP_ManualInvoicingAllowed_NAV__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.SALESPERSONCODE,null,null).addTextNode(getValueforNode(objAccount.EP_Salesperson__r.EP_Salesperson_Code__c ));
        customerNode.addChildElement(EP_StagingRetailOrderStub.ISVATEXEMPTED,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_VAT_Exempted__c)));
        customerNode.addChildElement(EP_StagingRetailOrderStub.DEFERINVOICE,null,null).addTextNode(getValueforNode(objAccount.EP_Defer_Invoice__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.DEFERUPPERLMT,null,null).addTextNode(getValueforNode(objAccount.EP_Defer_Upper_Limit__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.ALLOWRLSONPYMNTPROOF,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Allow_release_on_proof_of_payment__c)));
        //Account statement Frequency node
        Dom.XMLNode acntStmntFrqncyNode = customerNode.addChildElement(EP_StagingRetailOrderStub.ACNTSTMNTFRQNCY,null,null);
        acntStmntFrqncyNode.addChildElement(EP_StagingRetailOrderStub.DAILY,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Sys_Account_Statement_Periodicity_D__c)));
        acntStmntFrqncyNode.addChildElement(EP_StagingRetailOrderStub.WEEKLY,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Sys_Account_Statement_Periodicity_W__c)));
        acntStmntFrqncyNode.addChildElement(EP_StagingRetailOrderStub.FORTNIGHTLY,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Sys_Account_Statement_Periodicity_F__c)));
        acntStmntFrqncyNode.addChildElement(EP_StagingRetailOrderStub.MONTHLY,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Sys_Account_Statement_Periodicity_M__c)));
        //Frequency node end
        customerNode.addChildElement(EP_StagingRetailOrderStub.STMNTTYPALL,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Sys_All_Items_Account_Statements__c)));
        customerNode.addChildElement(EP_StagingRetailOrderStub.STMNTTYPOPEN,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Sys_Open_Items_Account_Statements__c)));
        customerNode.addChildElement(EP_StagingRetailOrderStub.COMBINEDINVOICING,null,null).addTextNode(getValueforNode(objAccount.EP_Combined_Invoicing__c));
         customerNode.addChildElement(EP_StagingRetailOrderStub.PRICECONSOLBASIS,null,null).addTextNode(getValueforNode(objAccount.EP_Price_Consolidation_Basis__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.BUSINESSSEGMENT,null,null).addTextNode(getValueforNode(objAccount.EP_Business_Segment_Code__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.BUSINESSCHANNEL,null,null).addTextNode(getValueforNode(objAccount.EP_Business_Channel_Code__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.EXCISEDUTY,null,null).addTextNode(getValueforNode(objAccount.EP_Excise_duty__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.ISPOREQ,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Is_Customer_Reference_Mandatory__c)));
        customerNode.addChildElement(EP_StagingRetailOrderStub.POREQBY,null,null).addTextNode(getValueforNode(objAccount.EP_Customer_s_PO_number_required_by__c));
        //customerNode.addChildElement(EP_StagingRetailOrderStub.PONR,null,null).addTextNode(getValueforNode(objAccount.EP_Customer_PO_Number__c));////
        customerNode.addChildElement(EP_StagingRetailOrderStub.INVCDUEDTBSDON,null,null).addTextNode(getValueforNode(objAccount.EP_Invoice_Due_Date_Based_on__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.CUSTACTIVATIONDATE,null,null).addTextNode(formatDateAsString(objAccount.EP_Customer_Activation_Date__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.CUSTSTATUS,null,null).addTextNode(getValueforNode(objAccount.EP_NewStatus__c));
        /**TFDID-76105
        customerNode.addChildElement(EP_StagingRetailOrderStub.REASONFORSTATUSCHNG,null,null).addTextNode(getValueforNode(objAccount.EP_Reason_For_Deactivation__c));
        /**TFDID-76105*
        customerNode.addChildElement(EP_StagingRetailOrderStub.SEARCHNAME,null,null).addTextNode(getValueforNode(objAccount.EP_Search_Name__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.TRADINGNAME,null,null).addTextNode(getValueforNode(objAccount.EP_Trading_Name__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.TELEXNO,null,null).addTextNode(getValueforNode(objAccount.EP_Telex_No__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.ADDRESS2,null,null);
        customerNode.addChildElement(EP_StagingRetailOrderStub.LOGOID,null,null).addTextNode(getValueforNode(objAccount.EP_Logo_Id__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.ISCAPTIVCUST,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Customer_Of_Customer__c)));
        //L4-45352-SIT-FIX
        customerNode.addChildElement(EP_StagingRetailOrderStub.REQPMTCRDLMT,null,null).addTextNode(getValueforNode(objAccount.EP_Recommended_Credit_Limit__c)); 
        //L4-45352-SIT-FIX
        //E2E not required customerNode.addChildElement(EP_StagingRetailOrderStub.DISPLAYPRICE,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Display_Price__c)));
        customerNode.addChildElement(EP_StagingRetailOrderStub.REPRCNGONAVGPRDCPRCNG,null,null).addTextNode(getValueforNode(objAccount.EP_Repricing_Based_On__c));
        customerNode.addChildElement('cnsldtdInvcPrntOptn',null,null).addTextNode(getValueforNode(objAccount.EP_Consolidated_Invoice_Print_Options__c));
        //add kyc details 
        addKYCDetails(customerNode); 
        customerNode.addChildElement(EP_StagingRetailOrderStub.PRFRDBANKACC,null,null).addTextNode(getValueforNode(objAccount.EP_Preferred_Bank__r.EP_Bank_Code__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.VATREGNR,null,null).addTextNode(getValueforNode(objAccount.EP_Company_Tax_Number__c));
        customerNode.addChildElement(EP_StagingRetailOrderStub.custRefVsbl,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Is_Customer_Reference_Visible__c)));
        customerNode.addChildElement(EP_StagingRetailOrderStub.VERSIONNR,null,null);
        //ShiptoAddresses node
        Dom.XMLNode shipToAddressesNode = customerNode.addChildElement(EP_StagingRetailOrderStub.SHIPTOADDRESSES,null,null);
        addShipToAddresses(shipToAddressesNode);
        //shiptoaddressnode end
        //Bank node start
        Dom.XMLNode custBanksNode = customerNode.addChildElement(EP_StagingRetailOrderStub.CUSTBANKS,null,null);
        //need to loop and add banks here
        addCustBankNode(custBanksNode);
        //bank node end
        customerNode.addChildElement(EP_StagingRetailOrderStub.CLIENTID,null,null).addTextNode(getValueforNode(objAccount.EP_Puma_Company_Code__c));
        //customer node end
        
        // Encoding payload by calling encode XML method in superclass
        AnyNode.addTextNode(encodeXML(tempDoc));*/
    }

    
}