public with sharing class PumaOrderPageExtension {
    private static final String COMMUNITY_CHANNEL = 'customercommunity';
    private static final String CHANNEL_PARAM_NAME = 'channel';
    // Initialise
    public Account account { get; set; }
    public String url { get; set; }

    // Get the community id if there is one
    public String communityId {
        get {
            return Network.getNetworkId();
        }
    }

    // Return whether page is currently in communities context or not
    public Boolean isCommunity {
        get {
            return String.isNotBlank(communityId) || this.channel == COMMUNITY_CHANNEL;
        }
    }

    public String channel {
        get {
            if (this.channel == null) {
                this.channel = ApexPages.currentPage().getParameters().get(CHANNEL_PARAM_NAME);
            }
            return this.channel;
        }
        private set;
    }

    public String redirectURL {
        get {
            return Network.communitiesLanding().getUrl();
        }
    }

    // Constructor
    public PumaOrderPageExtension(ApexPages.StandardController controller) {
        // Get the account
        account = (Account) controller.getRecord();

        PageReference iframeUrlPage = Page.CreateBasketAndOffer;
        iframeUrlPage.getParameters().put('id', String.valueOf(account.id));
        iframeUrlPage.setRedirect(true);

        String urlInstance = String.valueof(System.URL.getSalesforceBaseURL()).replace('Url:[delegate=','').replace(']','');
        this.url = urlInstance + Site.getPathPrefix() + iframeUrlPage.getUrl();
    }
}