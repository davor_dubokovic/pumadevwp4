@isTest
Public class EP_SellToAsDeactivate_UT
{   static final string EVENT_NAME = '05-ActiveTo07-Deactivate'; 
     
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        }
   private static testmethod void doOnEntry()
   {   
       Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        EP_SellToAsDeactivate localObj = new EP_SellToAsDeactivate();
        Account newAccount = EP_TestDataUtility.getSellToASactiveDomainObject().getAccount();
        Account oldAccount = newAccount.clone();
        newAccount.EP_Status__c = EP_AccountConstant.DEACTIVATE;
        EP_AccountDomainObject obj = new EP_AccountDomainObject(newAccount,oldAccount);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.assertEquals(true, true);
   
   
   
   }         
            

}