/*
   @Author          Accenture
   @Name            EP_OutboundMessageAsyncExecution
   @CreateDate      02/07/2017
   @Description     To Handle Pricing Response in Synchronous way - #60147
   					Another way to do future callout. 
   					For VMI order Creation, A Pricing callout will be a future outbound call out and after successfully pricing sync, State machine will hit another future outbound callout for order sync with NAV and SFDC does not allow to do a future callout from futrue call out
   @Version         1.0
*/

public class EP_OutboundMessageAsyncExecution implements Queueable , Database.AllowsCallouts{
    private Id recordId;
    private string messageId; 
    private String messageType;
    private set<id> integrationRecords;
    private String companyCode;
    
    /**
    * @author           Accenture
    * @name             EP_OutboundMessageAsyncExecution
    * @date             06/4/2017
    * @description      This is a Constructor
    * @param            Id, string, string, set, string
    * @return           NA
    */
    public EP_OutboundMessageAsyncExecution(Id recordId,string messageId, String messageType, set<id> integrationRecords, String companyCode) {
        this.recordId = recordId;
        this.messageId = messageId;
        this.messageType = messageType;
        this.integrationRecords = integrationRecords;
        this.companyCode = companyCode;
    }
    
    /**
    * @author           Accenture
    * @name             execute
    * @date             06/4/2017
    * @description      This method will be called by message service class to send outbound message
    * @param            QueueableContext
    * @return           NA
    */
    public void execute(QueueableContext context) {
    	EP_OutboundMessageService.sendOutboundMessageAsync(this.recordId,this.messageId, this.messageType, this.integrationRecords, this.companyCode);
    }
}