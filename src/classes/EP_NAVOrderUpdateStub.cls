/* 
   @Author <Accenture>
   @name <EP_OrderUpdateMsgFromWINDMS>
   @CreateDate <03/07/2017>
   @Description <These are the stub classes to map JSON string for orders Update Rest Service Request from WINDMS> 
   @Version <1.0>
   */
public class EP_NAVOrderUpdateStub {
    /* Class for MSG */
    public MSG MSG;
    
    /* MSG stub class*/ 
    public class MSG {
        public HeaderCommon HeaderCommon;
        public Payload Payload;
        public String StatusPayload;
    }
    
    /* HeaderCommon stub class*/    
    public class HeaderCommon extends EP_MessageHeader {}
    
    /* Payload stub class*/
    public class Payload {
        public any0 any0;
    }
    
    /* Any0 stub class*/
    public class any0 {
        public OrderStatus orderStatus;
    }
    
    /* OrderWrapper stub class*/
    public class OrderWrapper {
        //Added New variable
        public csord__Order__c sfOrder;
        public string errorCode;
        public string errorDescription;
        public String versionNr;
        //END
        public string seqId;
        public Identifier identifier;
        public string reasoncode;
        public string description;
        public String status;
    }
    
    /* OrderStatus stub class*/
    public class OrderStatus {
        public OrderList orders;
    }

    public class OrderList {
        public List<OrderWrapper> order;
    }

    /* Identifier stub class*/
    public class Identifier {
        public string orderId;
        public String entrprsId;
    }
}