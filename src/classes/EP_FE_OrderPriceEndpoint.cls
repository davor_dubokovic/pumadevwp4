/* 
   @Author <Iegor Nechyporenko>
   @name <EP_FE_OrderPriceEndpoint>
   @CreateDate <23/04/2016>
   @Description <Track if price has been successfully calculated for a specific order>  
   @Version <1.0>
*/
@RestResource(urlMapping = '/FE/V1/order/price/tracking/*')
global with sharing class EP_FE_OrderPriceEndpoint {
    global final static String PARAM_KEY_ORDER_ID = 'orderId';
    
    global static final String CLASSNAME = 'EP_FE_OrderPriceEndpoint';
    global static final String API_METHOD_NAME = 'calculateOrderPrice';
    global static final String TRANSACTION_ID = 'OrderPriceTracking';
    global static final String ERROR = 'ERROR';
    global static final Integer ERROR_NO_ORDER_ID = -1;
    global static final Integer ERROR_NO_ORDER_FOUND = -2;
    global static final Integer ERROR_NO_ACCESS_TO_PRICES = -3;
    global static final Map<Integer,String> ERRORDESCRIPTIONMAP = new Map<Integer,String> {
        -1 => 'Order Id is not specified in API GET parameters',
        -2 => 'There is no accessible order found for specified order id',
        -3 => 'User don\'t have access to see order price'
    };
    
    @HttpGet
/* 
   @Author <Iegor Nechyporenko>
   @name <calculateOrderPrice>
   @CreateDate <23/04/2016>
   @Description <>  
   @Version <1.0>
*/
    global static EP_FE_OrderPriceResponse calculateOrderPrice(){
        EP_FE_OrderPriceResponse response = new EP_FE_OrderPriceResponse();
        
        response.isPriceCalculated = false;
        Map < String, Object > params = RestContext.request.params;
        String orderId = params.containsKey(PARAM_KEY_ORDER_ID) ? String.valueOf(params.get(PARAM_KEY_ORDER_ID)) : null; 
        response.draftOrderId = orderId;
        
        if (orderId == null){
            logPricingError(response, ERROR_NO_ORDER_ID);
        } else {
            csord__Order__c orderWithItems;
            try {
                orderWithItems = EP_FE_OrderUtils.fetchOrderWithAllPricingInformation(orderId);
            } catch (Exception e) {
                logPricingError(response, ERROR_NO_ORDER_FOUND);
            }
            
            if (!canSeePrices(orderWithItems)) {
                response.canSeePrices = false;
            } else if (!EP_FE_OrderUtils.isOrderPricingReady(orderWithItems)) {
                response.canSeePrices = true;
                response.isPriceCalculated = false;
            } else {
                response.canSeePrices = true;
                response.isPriceCalculated = true;
                response.priceData = EP_FE_OrderUtils.getOrderProductData(orderWithItems, response.canSeePrices);
            }
        }
        
        return response;
    }
/* 
   @Author <Iegor Nechyporenko>
   @name <logPricingError>
   @CreateDate <23/04/2016>
   @Description <>  
   @Version <1.0>
*/    
    private static void logPricingError(EP_FE_OrderPriceResponse response, Integer errorResponseId){
        EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(CLASSNAME, API_METHOD_NAME, 
                ERROR, TRANSACTION_ID, ERRORDESCRIPTIONMAP.get(errorResponseId)),
                response, errorResponseId);     
    }
/* 
   @Author <Iegor Nechyporenko>
   @name <canSeePrices>
   @CreateDate <23/04/2016>
   @Description <>  
   @Version <1.0>
*/       
    private static Boolean canSeePrices(csord__Order__c fetchedOrder){
        String shipToType = fetchedOrder != null && fetchedOrder.EP_ShipTo__r != null && fetchedOrder.EP_ShipTo__r.EP_Ship_To_Type__c != null 
                          ? fetchedOrder.EP_ShipTo__r.EP_Ship_To_Type__c : null;
        return EP_FE_OrderUtils.evaluatePriceVisibility(fetchedOrder, shipToType);
    }

    
}