/*
    @Author          Accenture
    @Name            EP_ROImportStagingFactory
    @CreateDate      
    @Description     Factory class is used to get and call service class for Import RO
    @Version         1.0
    @Reference       NA
*/
public without sharing class EP_ROImportStagingFactory {
	  
     /**
    * @author       Accenture
    * @name         ProcessStagingRecords
    * @date         04/18/2018
    * @description This method is used for initiate Staging Record Data Validation Process 
    * @param        Id, String
    * @return       NA
    */ 
	public static void ProcessStagingRecords(Id fileId, String recordTypeAPIName) {
		EP_GeneralUtility.Log('public','EP_ROImportStagingFactory','ProcessStagingRecords');
		EP_File__c fileRec = new EP_File__c(Id = fileId);
		fileRec.EP_In_Process__c = true;
		database.update(fileRec);
		
     	EP_ROImportStagingFactory.initProcessingRequest(fileId, recordTypeAPIName);
	}
	
	/**
    * @author       Accenture
    * @name         initProcessingRequest
    * @date         04/18/2018
    * @description  This method is used for initiate Staging Record Data Validation Process 
    * @param        Id, String
    * @return       NA
    */ 
	@future
	public static void initProcessingRequest(Id fileId, String recordTypeAPIName) {
		EP_GeneralUtility.Log('public','EP_ROImportStagingFactory','initProcessingRequest');
		try {
	     	Type classType = Type.forName(recordTypeAPIName);
	     	EP_ROImportStaging stagingService = (EP_ROImportStaging) classType.newInstance();
			stagingService.processStagingDataValidation(fileId);
     	} catch (exception exp ){
			EP_LoggingService.logHandledException(exp, EP_Common_Constant.EPUMA, 'ProcessStagingRecords',EP_ROImportStagingFactory.class.getName(), ApexPages.Severity.ERROR);
     	}
	}
}