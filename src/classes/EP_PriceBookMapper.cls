/**
   @Author          CR Team
   @name            EP_PriceBookMapper
   @CreateDate      12/21/2016
   @Description     This class contains all SOQLs related to EP_PriceBook Object
   @Version         1.0
   @reference       NA
   */
/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
    */
    public with sharing class EP_PriceBookMapper {
      
      
    /**
    *  Constructor. 
    *  @name            EP_PriceBookMapper
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */  
    public EP_PriceBookMapper() {
      
    }
    

   /**This method returns PriceBook2 Records.
    *  @name             getRecordsByIds
    *  @param            set<id>
    *  @return           list<PriceBook2>
    *  @throws           NA
    */
    public list<PriceBook2> getRecordsByIds(set<id> idSet) {
      EP_GeneralUtility.Log('Public','EP_PriceBookMapper','getRecordsByIds');
      list<PriceBook2> pbList = new list<PriceBook2>();
      for(list<PriceBook2> pbObjList: [ SELECT Id, EP_Company__c
        ,(SELECT Id
        , Product2.EP_Company_Lookup__c
        , Pricebook2Id
        , Product2.EP_Product_Sold_As__c
        FROM PricebookEntries
        )
        , (SELECT id FROM PriceBookAccounts__r)  
        FROM PriceBook2
        WHERE Id IN :idSet
        ]){
        pbList.addAll(pbObjList);
      }                     
      return pbList;
    } 
       
    /**This method returns number of  accounts related to same pricebook
    *  @name             getRecordsByIds
    *  @param            set<id> ,Set<Id>
    *  @return           list<Account>
    *  @throws           NA
    */
    public Integer getNumberOfAccountRelatedToPricebook(Set<Id> pricebookID, Set<Id> accountIDsToExclude){
      EP_GeneralUtility.Log('Public','EP_PriceBookMapper','getNumberOfAccountRelatedToPricebook');
      Integer count =  [Select count() from Account Where EP_PriceBook__c IN:pricebookID AND Id Not IN: accountIDsToExclude]; 
      system.debug('========count==='+count);
      return count;
    }   
  }