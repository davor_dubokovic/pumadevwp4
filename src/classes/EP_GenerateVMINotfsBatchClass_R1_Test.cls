/* 
@Author <Gautam Manchanda>
@name <EP_GenerateVMINotificatBatchClass_Test>
@CreateDate <17/12/2015>
@Description <This class is used for generation of the notification and dips for user entry>
@Version <1.0>

*/
@isTest
public class EP_GenerateVMINotfsBatchClass_R1_Test {
    
    private static EP_Tank__c TankInstance;
    private static EP_Country__c country;
    private static EP_Region__c region;
    private static String COUNTRY_NAME = 'Australia';
    private static String COUNTRY_CODE = 'AUN';
    private static String COUNTRY_REGION = 'Australia';
    private static String REGION_NAME = 'North-Australia';
    private static String Setting_Name = 'AUN';
    private static String Setting_Time = '15:00';
    private static EP_VMI_Notification_Configuration__c vmiConfig;
    
    
     /**************************************************************************
    *@Description : This method is used to test EP_GenerateVMINotfsBatchHelper_R1 and 
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/         
    private static testMethod void testExecute(){
        
        //Insert Country
        country = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
        insert country;
        
        //Insert Region
        region = EP_TestDataUtility.createCountryRegion( REGION_NAME, country.Id);  
        insert region;
        
        //Insert EP_VMI_Notification_Configuration__c Data
        vmiConfig = EP_TestDataUtility.createTestRecordsForCustomSetting( Setting_Name, Setting_Time);
        
        //Insert EP_Simulated__c Data
        EP_Simulated__c simulated = EP_Simulated__c.getInstance();
        simulated.EP_Simulated_Date_Time__c=System.now();
        insert simulated;
        
        //Insert User
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test4488', Email='test4488@pumaEne.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='Asia/Kolkata', UserName='test41111111488@pumaEne.com', EP_Default_Country_Code__c='AU');
        u.EP_User_UTC_Offset__c=5.50;
        insert u;
        
        Id recordTypeId =  EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO);
        EP_TestDataUtility.WrapperCustomerHierarchy customerHiearchy;
        customerHiearchy = EP_TestDataUtility.createCustomerHierarchyForNAV(1);
        customerHiearchy.lShipToAccounts[0].EP_Status__C = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        customerHiearchy.lShipToAccounts[0].RecordTypeId = recordTypeId;
        customerHiearchy.lShipToAccounts[0].EP_Ship_To_UTC_Timezone__c = 'UTC+05:30';
        customerHiearchy.lShipToAccounts[0].EP_Country__c = country.id;
        customerHiearchy.lShipToAccounts[0].EP_Tank_Dip_Entry_Mode__c = Label.EP_Portal_Dip_Entry_Mode_Value;
        customerHiearchy.lShipToAccounts[0].EP_Tank_Dips_Schedule_Time__c = String.valueof(system.now().addMinutes(60).hour())+':'+String.valueof(system.now().addMinutes(30).minute());
        customerHiearchy.lShipToAccounts[0].EP_Ship_To_Opening_Days__c ='Monday;Tuesday;Wednesday;Thursday;Friday;Saturday;Sunday' ;
        customerHiearchy.lShipToAccounts[0].Phone = '9015502179';
        customerHiearchy.lShipToAccounts[0].EP_Email__c = 'gautam.manchanda@accenture.com';
        customerHiearchy.lShipToAccounts[0].EP_Ship_To_UTC_Offset__c = 6.50; 
        update customerHiearchy.lShipToAccounts;
        
        customerHiearchy.lShipToAccounts[0].EP_Status__C = EP_Common_Constant.STATUS_ACTIVE;
        update customerHiearchy.lShipToAccounts;
        
        TankInstance =EP_TestDataUtility.createTestEP_Tank(customerHiearchy.lShipToAccounts[0]);
        TankInstance.EP_Tank_Status__c ='Basic Data Setup';
        update TankInstance;
        TankInstance.EP_Tank_Status__c ='Operational';
        TankInstance.EP_Last_Dip_Ship_To_Date_Time__c = System.now().adddays(-1);
        update TankInstance; 
        
        //Insert TankDip Data
        String recType = EP_Common_Util.fetchRecordTypeId('EP_Tank_Dip__c','Actual');
        EP_Tank_Dip__c tDip = new EP_Tank_Dip__c();
        tDip.EP_Tank__c = TankInstance.id;
        tDip.EP_Ambient_Quantity__c = 40;
        tDip.RecordTypeId = recType;
        tDip.EP_Reading_Date_Time__c = System.now();
        tDip.EP_Tank_Dip_Entered_In_Last_14_Days__c = true;
        insert tDip;
        
        //Query RollUp Summary Field--EP_Last_Dip_Reading_Date__c after inserting Tank dip records
        TankInstance = [Select id,EP_Last_Dip_Reading_Date__c,EP_Last_Dip_Ship_To_Date_Time__c,EP_Last_Placeholder_Ship_To_Date_Time__c,EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c,EP_Ship_To__r.EP_Is_Ship_To_Open_Today__c from EP_Tank__c where id=:TankInstance.id];
        System.assertEquals (TankInstance.EP_Last_Dip_Reading_Date__c, tDip.EP_Reading_Date_Time__c);
        
        Test.starttest();
        EP_GenerateVMINotificationsBatchClass_R1 gen = new EP_GenerateVMINotificationsBatchClass_R1 ();
        database.executebatch(gen); 
        Test.stoptest();
    }
    
}