/*
*  @Author <Accenture>
*  @Name <EP_ASTNonVMISTAccountSetupToAccountSetup>
*  @CreateDate <28/02/2017>
 *  Novasuite Fixes -- comments added
*  @Description <Handles VMI Ship To Account status change from 04-Account Set-up to 04-Account Set-up>
*  @Version <1.0>
*/
public class EP_ASTNonVMISTAccountSetupToAccountSetup extends EP_AccountStateTransition {
    /*
    *  @Author <Accenture>
    *  @Name constructor of class EP_ASTNonVMISTAccountSetupToAccountSetup
    *  @return boolean
    */
    public EP_ASTNonVMISTAccountSetupToAccountSetup () {
        finalState = EP_AccountConstant.ACCOUNTSETUP;
    }
    /*
    *  @Author <Accenture>
    *  @Name isTransitionPossible
    *  @return boolean
    */
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMISTAccountSetupToAccountSetup','isTransitionPossible');
        return super.isTransitionPossible();
    }
    /*
    *  @Author <Accenture>
    *  @Name isRegisteredForEvent
    *  @return boolean
    */
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMISTAccountSetupToAccountSetup','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
    /*
    *  @Author <Accenture>
    *  @Name isGuardCondition
    *  @return boolean
    */
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMISTAccountSetupToAccountSetup','isGuardCondition');
        EP_AccountService service =  new EP_AccountService(this.account);
        //L4_45352_Start
        /*
        system.debug('## 2. Checking all review action created for Ship to is completed .....');
        if(!service.isCompletedAllReviewActions()||!service.isCompletedReviewActionsOnTanks()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.label.EP_Review_Action_Pending_on_ShipTo_Error_Message);
            return false;
        }
        system.debug('## 3. Checking if primary supply option is attached'); 
        if(!service.isPrimarySupplyOptionAttached()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(System.label.EP_One_Primary_Location_Reqd); 
            return false;
        }    
        
        // #sprintRouteWork starts//
        if(!service.isRouteAllocationAvailable()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.label.EP_Route_Not_Allocated);
            return false;
        }
        // #sprintRouteWork ends//
        */
        //L4_45352_End       
        return true;
    }
    /*
    *  @Author <Accenture>
    *  @Name doOnExit
    */
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMISTAccountSetupToAccountSetup','doOnExit');

    }
}