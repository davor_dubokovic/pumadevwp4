/**
    * @author           Accenture
    * @name             EP_Import_Retail_Order_Helper
    * @date             16/04/2018
    * @description      Helper class used to parse XML and insert Order Import record.
    * @param            NA
    * @return           NA
    */
public class EP_Import_Retail_Order_Helper{
   
    
    
     /**
    * @author           Accenture
    * @name             EP_Import_Retail_Order_Helper
    * @date             16/04/2018
    * @description      Constructor
    * @param            NA
    * @return           NA
    */
    public EP_Import_Retail_Order_Helper(){
        

    }
    
    
     /**
    * @author           Accenture
    * @name             processRequest
    * @date             16/04/2018
    * @description      Method to initiate XML Parsing
    * @param            String
    * @return           NA
    */
    Public static void processRequest(string inputXML)
    {
        EP_StagingRetailOrderStub instStub = new  EP_StagingRetailOrderStub();
        if(!processXml(instStub,inputXML)){
            sendValuesToStagingAttribute(instStub);
        }
        
    }
    
     /**
    * @author           Accenture
    * @name             processXml
    * @date             16/04/2018
    * @description      Method to parse the XML - Header
    * @param            EP_StagingRetailOrderStub,string
    * @return           boolean
    */
    private static  boolean processXml(EP_StagingRetailOrderStub instStub,string inputXML){
        EP_GeneralUtility.Log('public','EP_Import_Retail_Order_Helper','parseHeaderXML'); 
        boolean isError = false;
     try{
            Dom.Document instXml = new Dom.Document();
            instXml.load(inputXML);
            Dom.XMLNode rootElement = instXml.getRootElement();
            system.debug('rootElement+++++++++++++++'+rootElement.getChildElements());
            Dom.XMLNode Payload = rootElement.getChildElement('Payload', null);
            system.debug('Payload+++++++++++++++'+Payload);
            Dom.XMLNode any0 = Payload.getChildElement('any0', null);
            system.debug('any0+++++++++++++++'+any0);
            Dom.XMLNode salesData = any0.getChildElement('salesData', null);
            system.debug('salesData+++++++++++++++'+salesData);
            Dom.XMLNode header = salesData.getChildElement('header', null);
            system.debug('header+++++++++++++++'+header);
            /*Dom.XMLNode header = rootElement.getChildElement('header', null);
             system.debug('header+++++++++++++++'+header);
            */
            
            EP_StagingRetailOrderStub.Header objHeader = new EP_StagingRetailOrderStub.Header();
            objHeader.companyCode       = header.getChildElement('companyCode', null).getText();
            objHeader.transactionNr     = header.getChildElement('transactionNr', null).getText();
            objHeader.sellToId          = header.getChildElement('sellToId', null).getText();
            objHeader.locationId        = header.getChildElement('locationId', null).getText();
            objHeader.posLocationId     = header.getChildElement('posLocationId', null).getText();
            objHeader.transactionDt     = header.getChildElement('transactionDt', null).getText();
            objHeader.createdDt         = header.getChildElement('createdDt', null).getText();
            objHeader.createdBy         = header.getChildElement('createdBy', null).getText();
            objHeader.fileName          = header.getChildElement('fileName', null).getText();
            objHeader.senderCode        = header.getChildElement('senderCode', null).getText();
            instStub.MSG.payload.any0.salesData.header = objHeader;
            parseLinesXML(instStub,salesData);
            
            
        }
        catch (exception exp){
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.EP_XML_IMPORT_ERROR_MSG));
        }
        return isError;
    }
    
    /**
    * @author           Accenture
    * @name             parseLinesXML
    * @date             16/04/2018
    * @description      Will parse the XML - Lines 
    * @param            EP_StagingRetailOrderStub , Dom.XMLNode
    * @return           Void
    */
    private static void parseLinesXML(EP_StagingRetailOrderStub instStub, Dom.XMLNode inputNode)
    {
        EP_GeneralUtility.Log('public','EP_Import_Retail_Order_Helper','parseLinesXML'); 
        system.debug('inputNode++++++++++++++'+inputNode);
        Dom.XMLNode lines =   inputNode.getChildElement('lines', null);
        system.debug('lines++++++++++++++'+lines);
        for(Dom.XMLNode instLines :lines.getChildElements())
        {    
            EP_StagingRetailOrderStub.Line objLine = new EP_StagingRetailOrderStub.Line();
            objLine.lineNr          = instLines.getChildElement('lineNr', null).getText();
            objLine.itemId          =instLines.getChildElement('itemId', null).getText();
            objLine.descreption     =instLines.getChildElement('desc', null).getText();
            objLine.qty             =instLines.getChildElement('qty', null).getText();
            objLine.unitPrice       =instLines.getChildElement('unitPrice', null).getText();
            instStub.MSG.payload.any0.salesData.lines.line.add(objLine);
        }
        
    }
    
    /**
    * @author           Accenture
    * @name             sendValuesToStagingAttribute
    * @date             16/04/2018
    * @description      Will populate the values for setStagingAttributes and CreateFileRecord from class EP_StagingRetailOrderStub  
    * @param            EP_StagingRetailOrderStub
    * @return           Void
    */
    private static  void sendValuesToStagingAttribute(EP_StagingRetailOrderStub instStub)
    {
        EP_GeneralUtility.Log('private','EP_Import_Retail_Order_Helper','sendValuesToStagingAttribute');
        Savepoint sp = Database.setSavepoint();
        
        boolean isError = false;
        if(EP_StagingRetailOrderHelper.ValidateFile(instStub.MSG.payload.any0.salesData.header))
        {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.EP_XML_Duplicacy_Info_MSG));
             return;
        }
        
        id fieldId = EP_StagingRetailOrderHelper.CreateFileRecord(instStub.MSG.payload.any0.salesData.header);
        list<EP_RO_Import_Staging__c> lstImportStagingRec = new list<EP_RO_Import_Staging__c>();
        for(EP_StagingRetailOrderStub.Line instLine : instStub.MSG.payload.any0.salesData.lines.line )
        {
            String strImportStagingRec = EP_StagingRetailOrderHelper.setStagingAttributes(instLine ,instStub.MSG.payload.any0.salesData.header,fieldId,lstImportStagingRec);
            system.debug('strImportStagingRec+++++++++++++++++'+strImportStagingRec);
            if(!String.isBlank(strImportStagingRec))
            {
                isError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Line Nr - '+instLine.lineNr + ':  '+strImportStagingRec));
            }
        }
        
        if(insertStagingRecords(lstImportStagingRec) || isError){
        
            Database.rollback(sp);
            
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,Label.EP_XML_Parse_Success_MSG));
        }
        
        
    }
    
    
      /**
    * @author           Accenture
    * @name             insertStagingRecords
    * @date             16/04/2018
    * @description      Method to insert Order Import Records and check errors
    * @param            list<EP_RO_Import_Staging__c>
    * @return           boolean
    */
    private static boolean insertStagingRecords(list<EP_RO_Import_Staging__c> lstStaging){ 
        EP_GeneralUtility.Log('private','EP_Import_Retail_Order_Helper','insertStagingRecords');
        boolean isErrorStaging = false;
        if(lstStaging.size()>0){
            List<Database.SaveResult> results =  DataBase.insert(lstStaging,false); 
            
            for(Integer count = 0; count < results.size(); count++){ 
                if(!results[count].isSuccess()){
                     isErrorStaging= true;
                     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Line Nr - '+lstStaging[count].EP_LineNr__c+ ': ',EP_GeneralUtility.getDMLErrorMessage(results[count])));
                    
                }                
            } 
        }
        return isErrorStaging;
    }    
    
    
}