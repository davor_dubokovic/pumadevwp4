/**
 * @author <Jai Singh>
 * @name <EP_ContractTriggerHandler>
 * @createDate <29/07/2016>
 * @description <This is handler class for Contract trigger>
 * @version <1.0>
 */
public with sharing class EP_ContractTriggerHandler{
	
	private static final String DO_AFTER_UPDATE = 'doAfterUpdate';
	
	/**
	 * @author <Jai Singh>
	 * @description <This method handles After Update Event from Contract Trigger>
	 * @name <doAfterUpdate>
	 * @date <29/07/2016>
	 * @param Map<Id, Contract>, Map<Id, Contract>
	 * @return void
	 */
	public static void doAfterUpdate(Map<Id, Contract> mapOldContractIdContract, Map<Id, Contract> mapNewContractIdContract){
		// getting record type id value for record type name 'EP_Purchase_Contract'
		Id purchaseContractRecordTypeId = EP_Common_Util.getRecordTypeIdForGivenSObjectAndName(EP_Common_Constant.EP_CONTRACT_OBJECT_API , EP_Common_Constant.EP_PURCHASE_CONTRACT_RT);
		Set<Id> setContractIds = new Set<Id>();
		try{
			for(Contract contractObj : mapNewContractIdContract.values()){
				if(contractObj.RecordTypeId == purchaseContractRecordTypeId && contractObj.Status == EP_Common_Constant.EP_CONTRACT_STATUS_ONHOLD && mapOldContractIdContract.get(contractObj.Id).Status != EP_Common_Constant.EP_CONTRACT_STATUS_ONHOLD){
					setContractIds.add(contractObj.Id);
				}
			}
			if(!setContractIds.isEmpty()){
				// calling 'checkOpenOrder' on 'EP_ContractTriggerHelper' apex class
				EP_ContractTriggerHelper.checkOpenOrder(setContractIds, mapNewContractIdContract);
			}
		}catch(Exception ex){
			EP_LoggingService.logHandledException (ex, EP_Common_Constant.EPUMA, DO_AFTER_UPDATE, EP_ContractTriggerHandler.class.getName(), ApexPages.Severity.ERROR);
		}
	}
}