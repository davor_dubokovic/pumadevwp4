/* 
   @Author          Accenture
   @name            EP_CustomerOtherAdjustmentHelper
   @CreateDate      05/04/2017
   @Description     Helper class of customer adjustment inbound interface. Used to set calculated and lookup attributes
   @Version         1.0
*/
public class EP_CustomerOtherAdjustmentHelper {
    
    /**
    * @author           Accenture
    * @name             setCustomerAdjustmentAttributes
    * @date             03/13/2017
    * @description      Method used to populate the fields of customer adjustments from the stub instance 
    * @param            List<EP_CustomerOtherAdjustmentStub.document>
    * @return           NA
    */ 
    public static void setCustomerAdjustmentAttributes(List<EP_CustomerOtherAdjustmentStub.document> documents){
        EP_GeneralUtility.Log('public','EP_CustomerOtherAdjustmentHelper','setCustomerAdjustmentAttributes');
        
        EP_AccountMapper accountMapper = new EP_AccountMapper();
        EP_PaymentTermMapper paymentTermMapper = new EP_PaymentTermMapper();
        EP_CustomerOtherAdjustmentMapper custAdjustMapper = new EP_CustomerOtherAdjustmentMapper();
        
        Set<String> setOfCustomerAdjustments = getCustomerAdjustmentSet(documents);
        set<string> setOfCustomerIDs = getCustomerIdSet(documents);
        Set<String> setOfPaymentTerms = getPaymentTermSet(documents);
         
        Map<String,Account> mapCompositeIDCustomers = accountMapper.getAccountsMapByCompositeKey(setOfCustomerIDs);
        Map<String,Id> mapPaymentTermPaymentIDs = paymentTermMapper.getPaymentTermsMapByCodes(setOfPaymentTerms); 
        Map<String,EP_Customer_Other_Adjustment__c> mapCustomerAdjustements = custAdjustMapper.getCustomerOtherAdjustmentMapByUniqueId(setOfCustomerAdjustments);
        for(EP_CustomerOtherAdjustmentStub.document doc : documents){ 
            setCustomerAdjustment(doc,mapCustomerAdjustements);
            setBillTo(doc,mapCompositeIDCustomers);
			//L4 #45312 Changes Start
            setSellTo(doc,mapCompositeIDCustomers);
            //L4 #45312 Changes End
            setCustomerAdjustmentFields(doc);   
            setPaymentTerm(doc,mapPaymentTermPaymentIDs);
        }
    }
    
    /**
    * @author           Accenture
    * @name             getCustomerAdjustmentSet
    * @date             05/05/2017
    * @description      Gets all the customer's adjustment set from the list of customers adjustments records in the stub instance 
    * @param            List<EP_CustomerOtherAdjustmentStub.document>
    * @return           set<string>
    */
    @TestVisible
    private static set<string> getCustomerAdjustmentSet(List<EP_CustomerOtherAdjustmentStub.document> documents){
        EP_GeneralUtility.Log('Private','EP_CustomerOtherAdjustmentHelper','getCustomerAdjustmentSet');
        set<string> setOfCustomerAdjustments = new set<string>();
        for(EP_CustomerOtherAdjustmentStub.document doc : documents) {
            if(!string.isBlank(doc.identifier.billTo)){
            	//L4 #45304 Changes Start
            	setOfCustomerAdjustments.add(EP_DocumentUtil.generateUniqueKey(new Set<String>{doc.identifier.clientId,doc.identifier.billTo,doc.identifier.docId,doc.identifier.entryNr}).toUpperCase());
            	//L4 #45304 Changes End
            }
        }
        return setOfCustomerAdjustments;
    }
    
    /**
    * @author           Accenture
    * @name             getCustomerIdSet
    * @date             03/13/2017
    * @description      Gets all the customer's composite key from the list of customer adjustments records in the stub instance 
    * @param            List<EP_CustomerOtherAdjustmentStub.document>
    * @return           set<string>
    */
    @TestVisible
    private static set<string> getCustomerIdSet(List<EP_CustomerOtherAdjustmentStub.document> documents){
        EP_GeneralUtility.Log('private','EP_CustomerOtherAdjustmentHelper','getCompositeIDs');
        set<string> setOfCustomerIDs = new set<string>();
        for(EP_CustomerOtherAdjustmentStub.document doc : documents) {
            if(!string.isBlank(doc.identifier.billTo)){
                setOfCustomerIDs.add(EP_GeneralUtility.getCompositeKey(doc.identifier.clientId,doc.identifier.billTo).toUpperCase());
            }
			//L4 #45312 Changes Start
            if(!string.isBlank(doc.sellTo)){
                setOfCustomerIDs.add(EP_GeneralUtility.getCompositeKey(doc.identifier.clientId,doc.sellTo).toUpperCase());
            }
            //L4 #45312 Changes End
        }
        return setOfCustomerIDs;
    }
    
    /**
    * @author           Accenture
    * @name             getPaymentTermSet
    * @date             03/13/2017
    * @description      Gets all the customer's payment term key from the list of customer adjustments records in the stub instance 
    * @param            List<EP_CustomerOtherAdjustmentStub.document>
    * @return           set<string>
    */
    @TestVisible
    private static set<string> getPaymentTermSet(List<EP_CustomerOtherAdjustmentStub.document> documents){
        EP_GeneralUtility.Log('private','EP_CustomerOtherAdjustmentHelper','getPaymentTermSet');
        set<string> setOfPaymentTerms = new set<string>();
        for(EP_CustomerOtherAdjustmentStub.document doc : documents) {
            if(!string.isBlank(doc.paymentTerm)){
                setOfPaymentTerms.add(doc.paymentTerm.toUpperCase());
            }
        }
        return setOfPaymentTerms;
    }
    
    /**
    * @author           Accenture
    * @name             setCustomerAdjustment
    * @date             05/05/2017
    * @description      Sets the customer adjustment instance by querying the database based on the composite key, if not found then creates a new instance. 
    * @param            EP_CustomerOtherAdjustmentStub.document,Map<String,EP_Customer_Other_Adjustment__c>
    * @return           NA
    */
    @TestVisible
    private static void setCustomerAdjustment(EP_CustomerOtherAdjustmentStub.document document, Map<String,EP_Customer_Other_Adjustment__c> mapCustomerAdjustements){
        EP_GeneralUtility.Log('private','EP_CustomerOtherAdjustmentHelper','setCustomerAdjustment');
        //L4 #45304 Changes Start
        string compositeKey = EP_DocumentUtil.generateUniqueKey(new Set<String>{document.identifier.clientId,document.identifier.billTo,document.identifier.docId, document.identifier.entryNr}).toUpperCase();
        //L4 #45304 Changes End
        if(mapCustomerAdjustements.containsKey(compositeKey)) {
            document.SFCustomerOtherAdjustment = mapCustomerAdjustements.get(compositeKey);
        } else {
            document.SFCustomerOtherAdjustment = new EP_Customer_Other_Adjustment__c();
        }
    }
    
    /**
    * @author           Accenture
    * @name             setBillTo
    * @date             03/13/2017
    * @description      Sets the account id by querying the database based on the composite key, if not found, adds the error message. 
    * @param            EP_CustomerOtherAdjustmentStub.document,Map<String,Account>
    * @return           NA
    */
    @TestVisible
    private static void setBillTo(EP_CustomerOtherAdjustmentStub.document document, Map<String,Account> mapCompositeIDCustomers){
        EP_GeneralUtility.Log('private','EP_CustomerOtherAdjustmentHelper','setCustomerAdjustment');
        string compositeKey = EP_GeneralUtility.getCompositeKey(document.identifier.clientId,document.identifier.billTo).toUpperCase();
        if(mapCompositeIDCustomers.containsKey(compositeKey)) {
            document.SFCustomerOtherAdjustment.EP_Bill_To__c = mapCompositeIDCustomers.get(compositeKey).Id;
        } else {
            string errorMessage = EP_COMMON_CONSTANT.CUSTOMER.capitalize() +EP_Common_CONSTANT.SPACE +  document.identifier.billTo + EP_Common_CONSTANT.SPACE + EP_COMMON_CONSTANT.NOT_FOUND;
            document.errorDescription = string.isBlank(document.errorDescription) ? errorMessage : document.errorDescription + EP_COMMON_CONSTANT.SEMICOLON + errorMessage;
        }
    }
	//L4 #45312 Changes Start
    /**
    * @author           Accenture
    * @name             setSellTo
    * @date             24/01/2018
    * @description      Sets the account id by querying the database based on the composite key, if not found, adds the error message. 
    * @param            EP_CustomerOtherAdjustmentStub.document,Map<String,Account>
    * @return           NA
    */
    @TestVisible
    private static void setSellTo(EP_CustomerOtherAdjustmentStub.document document, Map<String,Account> mapCompositeIDCustomers){
        EP_GeneralUtility.Log('private','EP_CustomerOtherAdjustmentHelper','setCustomerAdjustment');
        string compositeKey = EP_GeneralUtility.getCompositeKey(document.identifier.clientId,document.sellTo).toUpperCase();
        if(mapCompositeIDCustomers.containsKey(compositeKey)){
            document.SFCustomerOtherAdjustment.EP_Sell_To__c = mapCompositeIDCustomers.get(compositeKey).Id;
        }
    }
    //L4 #45312 Changes End
    
    /**
    * @author           Accenture
    * @name             setCustomerAdjustmentFields
    * @date             05/05/2017
    * @description      Method used to populate the customer adjustment fields from the stub instance 
    * @param            EP_CustomerOtherAdjustmentStub.document
    * @return           NA
    */
    @TestVisible
    private static void setCustomerAdjustmentFields(EP_CustomerOtherAdjustmentStub.document document){
        EP_GeneralUtility.Log('private','EP_CustomerOtherAdjustmentHelper','setCustomerAdjustmentFields');
        Id devRecordTypeId = Schema.SObjectType.EP_Customer_Other_Adjustment__c.getRecordTypeInfosByName().get(EP_COMMON_CONSTANT.RECORD_TYPE_OTHER_CUSTOMER_ADJUSTMENT).getRecordTypeId();
        document.SFCustomerOtherAdjustment.Name = string.isBlank(document.identifier.docId) ? null : document.identifier.docId;
        document.SFCustomerOtherAdjustment.EP_Payment_Method__c = string.isBlank(document.paymentMethod) ? null : document.paymentMethod;
        document.SFCustomerOtherAdjustment.EP_Document_Date__c = string.isBlank(document.docDate) ? null : Date.valueOf(document.docDate);
        document.SFCustomerOtherAdjustment.EP_Amount_Paid__c = string.isBlank(document.amount) ? null : Decimal.valueOf(document.amount.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK));
        document.SFCustomerOtherAdjustment.CurrencyIsoCode = string.isBlank(document.currencyId) ? null : document.currencyId;
        document.SFCustomerOtherAdjustment.EP_Payment_Reference_Number__c = string.isBlank(document.referenceNr) ? null : document.referenceNr;
        document.SFCustomerOtherAdjustment.RecordTypeId = devRecordTypeId;
        document.SFCustomerOtherAdjustment.EP_Submission_Date__c = string.isBlank(document.submissionDate) ? null : Date.valueOf(document.submissionDate);
        document.SFCustomerOtherAdjustment.EP_Remaining_Amount__c = string.isBlank(document.remainingAmount) ? null : Decimal.valueOf(document.remainingAmount.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK));
        document.SFCustomerOtherAdjustment.EP_Description__c = string.isBlank(document.description) ? null : document.description;
        document.SFCustomerOtherAdjustment.EP_Reason_Code__c = string.isBlank(document.reasonCode) ? null : document.reasonCode;
        document.SFCustomerOtherAdjustment.EP_Docstate__c = string.isBlank(document.docState) ? null : document.docState; 
        document.SFCustomerOtherAdjustment.EP_Client_ID__c = string.isBlank(document.identifier.clientId) ? null : document.identifier.clientId;
        document.SFCustomerOtherAdjustment.EP_Source_Seq_Id__c = string.isBlank(document.seqId) ? null : document.seqId;
    	
    	//L4 #45304 Changes Start
    	document.SFCustomerOtherAdjustment.EP_Entry_Number__c = string.isBlank(document.identifier.entryNr) ? null : document.identifier.entryNr;
    	document.SFCustomerOtherAdjustment.EP_Is_Reversal__c = document.isReversal != null && document.isReversal.equalsIgnoreCase(EP_COMMON_CONSTANT.YES) ? true : false ;
    	//L4 #45304 Changes End
    	//L4 #45318 Changes Start
    	document.SFCustomerOtherAdjustment.EP_Bad_Debt_Written_Off__c = document.isBadDebtWriteOff != null && document.isBadDebtWriteOff.equalsIgnoreCase(EP_COMMON_CONSTANT.YES) ? true : false ;
    	//L4 #45318 Changes End
		
		//L4 #45312 Changes Start
        document.SFCustomerOtherAdjustment.EP_Due_Date__c = string.isBlank(document.dueDate) ? null : Date.valueOf(document.dueDate);
        //L4 #45312 Changes End
    }
    
    /**
    * @author           Accenture
    * @name             setPaymentTerm
    * @date             03/13/2017
    * @description      Method to populate  payment term lookup. If payment term does not exist then adds error message
    * @param            EP_CustomerOtherAdjustmentStub.document, Map<String,Id>
    * @return           NA
    */
    @TestVisible
    private static void setPaymentTerm(EP_CustomerOtherAdjustmentStub.document document, Map<String,Id> mapPaymentTermPaymentIDs){
        EP_GeneralUtility.Log('Private','EP_CustomerOtherAdjustmentHelper','setPaymentTerm');
        if(string.isNotBlank(document.paymentTerm)) {
            if(mapPaymentTermPaymentIDs.containsKey(document.paymentTerm.toUpperCase())){
                document.SFCustomerOtherAdjustment.EP_Payment_Term__c = mapPaymentTermPaymentIDs.get(document.paymentTerm.toUpperCase());
            } else {
                string errorMessage = EP_Common_CONSTANT.PYMNT_TRM + EP_Common_CONSTANT.SPACE +  document.paymentTerm + EP_Common_CONSTANT.SPACE + EP_COMMON_CONSTANT.NOT_FOUND;
                document.errorDescription = string.isBlank(document.errorDescription) ? errorMessage : document.errorDescription + EP_COMMON_CONSTANT.SEMICOLON + errorMessage;
            }
        } else {
            document.SFCustomerOtherAdjustment.EP_Payment_Term__c = null;
        }
    }
}