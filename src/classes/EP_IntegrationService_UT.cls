@isTest
public class EP_IntegrationService_UT
{
    static String MESSAGE_TYPE = 'NAV_TO_SFDC_CUSTOMER_UPDATE';
    @testSetup static void init() {
      List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
    }
	static testMethod void handleRequest_Postivetest() {
		EP_IntegrationService localObj = new EP_IntegrationService();
		Account accountObj = EP_TestDataUtility.getSellTo();
		EP_CustomerUpdateStub stub = EP_TestDataUtility.createInboundCustomerMessageWithMultipleBanks();
        List<EP_CustomerUpdateStub.customer> customers = stub.MSG.payload.any0.customers.customer;
        EP_CustomerUpdateStub.customer customer = customers[0];
        customer.billToCustId = string.valueOf(accountObj.AccountNumber);
        customer.clientId = string.valueOf(accountObj.EP_Account_Company_Name__c);
        customer.identifier.custId = string.valueOf(accountObj.AccountNumber);        
        List<EP_CustomerUpdateStub.custBank> custBanks = customer.custBanks.custBank;
        EP_CustomerUpdateStub.custBank custBank = custBanks[0];
        custBank.identifier.custId = string.valueOf(accountObj.AccountNumber);        
        string requestBody = JSON.serialize(stub);		
		
		Test.startTest();
			String result = localObj.handleRequest(MESSAGE_TYPE,requestBody);
		Test.stopTest();
		
		System.assertEquals(true, result != null);
	}
	static testMethod void handleRequest_Negativetest() {
		EP_IntegrationService localObj = new EP_IntegrationService();
		
		Test.startTest();
			String result = localObj.handleRequest(MESSAGE_TYPE);
		Test.stopTest();
		
		System.assertEquals(true, result!= null);
	}
	//Code changes for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
	static testMethod void handleRequest_isIdempotentTest() {
		EP_IntegrationService localObj = new EP_IntegrationService();
		string MsgID = '170529123107367-{042A388A-188D-4217-8B98-6372BD798305}';
		string requestBody = '{"MSG":{"HeaderCommon":{"MsgID":"'+MsgID+'","InterfaceType":"NAV","SourceGroupCompany":"AAF","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtraflon2k202.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"SKU Staging-SF","CommunicationType":"Async"},"Payload":null,"StatusPayload":"StatusPayload"}}';
    	EP_IntegrationRecord__c integrationRecord = EP_TestDataUtility.createIntegrationRecforInboundRequest(MsgID, MESSAGE_TYPE, requestBody);
    	database.insert(integrationRecord);
		
		Test.startTest();
			String result = localObj.handleRequest(MESSAGE_TYPE,requestBody);
		Test.stopTest();
		
		system.AssertEquals(integrationRecord.EP_Process_Result__c, result);
	}
	static testMethod void isIdempotent_PositiveTest() {
		string MsgID = '170529123107367-{042A388A-188D-4217-8B98-6372BD798305}';
		string requestBody = '{"MSG":{"HeaderCommon":{"MsgID":"'+MsgID+'","InterfaceType":"NAV","SourceGroupCompany":"AAF","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtraflon2k202.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"SKU Staging-SF","CommunicationType":"Async"},"Payload":null,"StatusPayload":"StatusPayload"}}';
    	EP_IntegrationRecord__c integrationRecord = EP_TestDataUtility.createIntegrationRecforInboundRequest(MsgID, MESSAGE_TYPE, requestBody);
    	database.insert(integrationRecord);
    	
    	Test.startTest();
			boolean result = EP_IntegrationService.isIdempotent(MsgID);
		Test.stopTest();
		
		EP_IntegrationRecord__c resultRec = [select EP_Attempt__c from EP_IntegrationRecord__c where Id=:integrationRecord.Id];
		system.AssertEquals(true, result);
		system.AssertEquals(2, resultRec.EP_Attempt__c);
	}
	static testMethod void isIdempotent_NegativeTest() {
		string MsgID = '170529123107367-{042A388A-188D-4217-8B98-6372BD798305}';
		string requestBody = '{"MSG":{"HeaderCommon":{"MsgID":"'+MsgID+'","InterfaceType":"NAV","SourceGroupCompany":"AAF","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtraflon2k202.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"SKU Staging-SF","CommunicationType":"Async"},"Payload":null,"StatusPayload":"StatusPayload"}}';
    	
    	Test.startTest();
			boolean result = EP_IntegrationService.isIdempotent(MsgID);
		Test.stopTest();		
		system.AssertEquals(false, result);
	}
	static testMethod void logInboundRequest_PositiveTest() {
		string MsgID = '170529123107367-{042A388A-188D-4217-8B98-6372BD798305}';
		string requestBody = '{"MSG":{"HeaderCommon":{"MsgID":"'+MsgID+'","InterfaceType":"NAV","SourceGroupCompany":"AAF","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtraflon2k202.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"SKU Staging-SF","CommunicationType":"Async"},"Payload":null,"StatusPayload":"StatusPayload"}}';
    	Map<String, String> mapOfHeaderCommonDetails = EP_IntegrationService.getRequestHeaderDetails(requestBody);
    	
    	Test.startTest();
			EP_IntegrationRecord__c intRecord = EP_IntegrationService.logInboundRequest(MESSAGE_TYPE,mapOfHeaderCommonDetails,requestBody);
		Test.stopTest();
		
		system.AssertEquals(intRecord.EP_Message_ID__c, MsgID);
	}
	static testMethod void logInboundRequest_NegativeTest() {
		string MsgID = '170529123107367-{042A388A-188D-4217-8B98-6372BD798305}';
		string requestBody = '{"MSG":{"HeaderCommon":{"MsgID":"'+MsgID+'","InterfaceType":"NAV","SourceGroupCompany":"AAF","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtraflon2k202.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"SKU Staging-SF","CommunicationType":"Async"},"Payload":null,"StatusPayload":"StatusPayload"}}';
    	Map<String, String> mapOfHeaderCommonDetails = null;
    	
    	Test.startTest();
			EP_IntegrationRecord__c intRecord = EP_IntegrationService.logInboundRequest(MESSAGE_TYPE,mapOfHeaderCommonDetails,requestBody);
		Test.stopTest();
		
		system.AssertEquals(null, intRecord.EP_Message_ID__c);
	}
	static testMethod void logInboundRequest_ACKTest() {
		string MsgID = '170529123107367-{042A388A-188D-4217-8B98-6372BD798305}';
		string requestBody = '{"MSG":{"HeaderCommon":{"MsgID":"'+MsgID+'","InterfaceType":"NAV","SourceGroupCompany":"AAF","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtraflon2k202.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"SKU Staging-SF","CommunicationType":"Async"},"Payload":null,"StatusPayload":"StatusPayload"}}';
    	Map<String, String> mapOfHeaderCommonDetails = new Map<String, String>();
    	
    	Test.startTest();
			EP_IntegrationRecord__c intRecord = EP_IntegrationService.logInboundRequest(EP_Common_Constant.COMMON_ACKNOWLEDGEMENT,mapOfHeaderCommonDetails,requestBody);
		Test.stopTest();
		
		system.AssertEquals(null, intRecord);
	}
	static testMethod void getRequestHeaderDetails_PositiveTest() {
		string MsgID = '170529123107367-{042A388A-188D-4217-8B98-6372BD798305}';
		string requestBody = '{"MSG":{"HeaderCommon":{"MsgID":"'+MsgID+'","InterfaceType":"NAV","SourceGroupCompany":"AAF","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtraflon2k202.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"SKU Staging-SF","CommunicationType":"Async"},"Payload":null,"StatusPayload":"StatusPayload"}}';
    	
    	Test.startTest();
			Map<String, String> mapOfHeaderCommonDetails = EP_IntegrationService.getRequestHeaderDetails(requestBody);
		Test.stopTest();
		
		system.AssertEquals(MsgID, mapOfHeaderCommonDetails.get(EP_Common_Constant.MSG_ID));
	}
	static testMethod void getRequestHeaderDetails_NegativeTest() {
		string MsgID = '170529123107367-{042A388A-188D-4217-8B98-6372BD798305}';
		string requestBody = '{"MSG_1":{"HeaderCommon":{"MsgID":"'+MsgID+'","InterfaceType":"NAV","SourceGroupCompany":"AAF","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtraflon2k202.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"SKU Staging-SF","CommunicationType":"Async"},"Payload":null,"StatusPayload":"StatusPayload"}}';
    	
    	Test.startTest();
			Map<String, String> mapOfHeaderCommonDetails = EP_IntegrationService.getRequestHeaderDetails(requestBody);
		Test.stopTest();
		
		system.AssertEquals(0, mapOfHeaderCommonDetails.keyset().size());
	}
	
	static testMethod void getHeaderDetails_Test() {
		string MsgID = '170529123107367-{042A388A-188D-4217-8B98-6372BD798305}';
		string responseBody = '{"HeaderCommon":{"MsgID":"'+MsgID+'","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","TransportStatus":"Delivered","SourceUpdateStatusAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceResponseAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceGroupCompany":"AAF","SourceCompany":"AAF","ProcessStatus":"Success","ProcessingErrorDescription":"","ObjectType":"Table","ObjectName":"SKU Staging-SF","MiddlewareUrlForPush":"https://dtraflon2k202.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","InterfaceType":"NAV","ErrorDescription":"","ErrorCode":"","EmailNotification":"","DestinationUpdateStatusAddress":"","DestinationGroupCompany":"","DestinationCompany":"","DestinationAddress":"","CorrelationID":"170529123107367-{042A388A-188D-4217-8B98-6372BD798305}","ContinueOnError":"true","ComprehensiveLogging":"true","CommunicationType":"Async","CallDestinationForProcessing":"true"},"acknowledgement":null,"StatusPayload":""}';    	
    	Map<String, Object> mapOfRequestJson = (Map<String, Object>)JSON.deserializeUntyped(responseBody);
    	
    	Test.startTest();
			Map<String, String> mapOfHeaderCommonDetails = EP_IntegrationService.getHeaderDetails(mapOfRequestJson);
		Test.stopTest();
		
		system.AssertEquals(MsgID, mapOfHeaderCommonDetails.get(EP_Common_Constant.MSG_ID));
	}
	static testMethod void  logProcessResponse_PositiveTest() {
		string MsgID = '170529123107367-{042A388A-188D-4217-8B98-6372BD798305}';
		string responseBody = '{"HeaderCommon":{"MsgID":"'+MsgID+'","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","TransportStatus":"Delivered","SourceUpdateStatusAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceResponseAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceGroupCompany":"AAF","SourceCompany":"AAF","ProcessStatus":"Success","ProcessingErrorDescription":"","ObjectType":"Table","ObjectName":"SKU Staging-SF","MiddlewareUrlForPush":"https://dtraflon2k202.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","InterfaceType":"NAV","ErrorDescription":"","ErrorCode":"","EmailNotification":"","DestinationUpdateStatusAddress":"","DestinationGroupCompany":"","DestinationCompany":"","DestinationAddress":"","CorrelationID":"170529123107367-{042A388A-188D-4217-8B98-6372BD798305}","ContinueOnError":"true","ComprehensiveLogging":"true","CommunicationType":"Async","CallDestinationForProcessing":"true"},"acknowledgement":null,"StatusPayload":""}';    	
        EP_IntegrationRecord__c integrationRecord = EP_TestDataUtility.createIntegrationRecforInboundRequest(MsgID, MESSAGE_TYPE, responseBody);
    	//database.insert(integrationRecord);
    	Test.startTest();
			EP_IntegrationService.logProcessResponse(integrationRecord, responseBody);
		Test.stopTest();
		
		EP_IntegrationRecord__c resultRec = [select EP_Process_Status__c,EP_Process_Result__c from EP_IntegrationRecord__c where Id=:integrationRecord.Id];
		system.AssertEquals('Success', resultRec.EP_Process_Status__c);
		system.AssertNotEquals(null, resultRec.EP_Process_Result__c);
	}
	static testMethod void  logProcessResponse_NegativeTest() {
		string MsgID = '170529123107367-{042A388A-188D-4217-8B98-6372BD798305}';
		string responseBody = '{"HeaderCommon":{"MsgID":"'+MsgID+'","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","TransportStatus":"Delivered","SourceUpdateStatusAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceResponseAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceGroupCompany":"AAF","SourceCompany":"AAF","ProcessStatus":"Success","ProcessingErrorDescription":"","ObjectType":"Table","ObjectName":"SKU Staging-SF","MiddlewareUrlForPush":"https://dtraflon2k202.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","InterfaceType":"NAV","ErrorDescription":"","ErrorCode":"","EmailNotification":"","DestinationUpdateStatusAddress":"","DestinationGroupCompany":"","DestinationCompany":"","DestinationAddress":"","CorrelationID":"170529123107367-{042A388A-188D-4217-8B98-6372BD798305}","ContinueOnError":"true","ComprehensiveLogging":"true","CommunicationType":"Async","CallDestinationForProcessing":"true"},"acknowledgement":null,"StatusPayload":""}';    	
        EP_IntegrationRecord__c integrationRecord = null;
    	
    	Test.startTest();
			EP_IntegrationService.logProcessResponse(integrationRecord, responseBody);
		Test.stopTest();
		
		list<EP_IntegrationRecord__c> resultRec = [select EP_Process_Status__c,EP_Process_Result__c from EP_IntegrationRecord__c];
		system.AssertEquals(true, resultRec.isEmpty());
	}
	
	static testMethod void  logProcessResponse_ExceptionTest() {
		string MsgID = '170529123107367-{042A388A-188D-4217-8B98-6372BD798305}';
		string responseBody = '{"HeaderCommon_1":{"MsgID":"'+MsgID+'","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","TransportStatus":"Delivered","SourceUpdateStatusAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceResponseAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceGroupCompany":"AAF","SourceCompany":"AAF","ProcessStatus":"Success","ProcessingErrorDescription":"","ObjectType":"Table","ObjectName":"SKU Staging-SF","MiddlewareUrlForPush":"https://dtraflon2k202.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","InterfaceType":"NAV","ErrorDescription":"","ErrorCode":"","EmailNotification":"","DestinationUpdateStatusAddress":"","DestinationGroupCompany":"","DestinationCompany":"","DestinationAddress":"","CorrelationID":"170529123107367-{042A388A-188D-4217-8B98-6372BD798305}","ContinueOnError":"true","ComprehensiveLogging":"true","CommunicationType":"Async","CallDestinationForProcessing":"true"},"acknowledgement":null,"StatusPayload":""}';    	
        EP_IntegrationRecord__c integrationRecord = EP_TestDataUtility.createIntegrationRecforInboundRequest(MsgID, MESSAGE_TYPE, responseBody);
    	
    	Test.startTest();
			EP_IntegrationService.logProcessResponse(integrationRecord, responseBody);
		Test.stopTest();
		
		list<EP_IntegrationRecord__c> resultRec = [select EP_Process_Status__c,EP_Process_Result__c from EP_IntegrationRecord__c];
		system.AssertEquals(true, resultRec.isEmpty());
	}
	
	static testMethod void isInboundMessageLoggerDisable_PositiveTest() {
		EP_CS_Communication_Settings__c communicationSettings = new EP_CS_Communication_Settings__c();
		communicationSettings.Name = EP_Common_Constant.DISABLE_INBOUND_LOG;
		communicationSettings.Disable__c = true;
		database.insert(communicationSettings);
		
		Test.startTest();
			boolean isDisable = EP_IntegrationService.isInboundMessageLoggerDisable();
		Test.stopTest();
		
		system.AssertEquals(true, isDisable);
	}
	
	static testMethod void isInboundMessageLoggerDisable_NegativeTest() {
		Test.startTest();
			boolean isDisable = EP_IntegrationService.isInboundMessageLoggerDisable();
		Test.stopTest();
		
		system.AssertEquals(false, isDisable);
	}
}