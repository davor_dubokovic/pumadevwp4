@isTest
public class EP_EmailTemplateMapper_UT
{    
    public static final String TEMPLATE_DEV_NAME = 'EP_Test_Email_Template';
    public static final String TEMPLATE_NAME = 'dummyTemplate';
    public static final String TEMPLATE_TYPE = 'Text';
    public static final String TEMPLATE_SUBJECT = 'testTemplate';
    
    static testMethod void getEmailTemplateAsPerName_UT() {
        EP_EmailTemplateMapper emailMapper = new EP_EmailTemplateMapper();
        EmailTemplate empTemplate = EP_TestDataUtility.createEmailTemplate(TEMPLATE_DEV_NAME,TEMPLATE_TYPE,TEMPLATE_NAME,TEMPLATE_SUBJECT);
        Test.startTest();
        EmailTemplate e = emailMapper.getEmailTemplateAsPerName(TEMPLATE_DEV_NAME);    
        Test.stopTest();      
        System.AssertEquals(TEMPLATE_SUBJECT,e.subject);
    }
    
    static testMethod void getEmailTemplatesById_UT() {
        EmailTemplate empTemplate = EP_TestDataUtility.createEmailTemplate(TEMPLATE_DEV_NAME,TEMPLATE_TYPE,TEMPLATE_NAME,TEMPLATE_SUBJECT);
        Test.startTest();
        EmailTemplate e = EP_EmailTemplateMapper.getEmailTemplatesById(empTemplate.id);    
        Test.stopTest();      
        System.AssertEquals(TEMPLATE_SUBJECT,e.subject);
    }
   

}