@isTest
public with sharing class EP_ShipToSyncWithWINDMSXML_UT {
    static testMethod void init_testPositive() {
    	EP_AccountDomainObject objAccDomain = EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario();        
        EP_ShipToSyncWithWINDMSXML objLocal = new EP_ShipToSyncWithWINDMSXML();
        objLocal.recordId = objAccDomain.localAccount.Id;
        Test.startTest();
        objLocal.init();
        Test.stopTest();        
        System.assert(objLocal.objAccount!=null);         
    }
	static testMethod void createPayload_testPositive() {
        EP_TestDataUtility.createCommunicationSettingsCS();
    	EP_AccountDomainObject objAccDomain = EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        EP_ShipToSyncWithWINDMSXML objLocal = new EP_ShipToSyncWithWINDMSXML();
        objLocal.recordId = objAccDomain.localAccount.Id;
        Test.startTest();
        objLocal.init();
        objLocal.isEncryptionEnabled = true;
        objLocal.createPayload();
        Test.stopTest();
        System.assertEquals( objAccDomain.localAccount.Id,objLocal.objAccount.Id);        
    }	

    static testMethod void getGeoCordinateValue_test() {
        EP_TestDataUtility.createCommunicationSettingsCS();
        EP_AccountDomainObject objAccDomain = EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        EP_ShipToSyncWithWINDMSXML objLocal = new EP_ShipToSyncWithWINDMSXML();
        objLocal.recordId = objAccDomain.localAccount.Id;
        Test.startTest();
        decimal dec = 5.56782;
        decimal result = objLocal.getGeoCordinateValue(dec);
        Test.stopTest();
        System.assertEquals( result,dec);        
    }

    static testMethod void getGeoCordinateValue_negtest() {
        EP_TestDataUtility.createCommunicationSettingsCS();
        EP_AccountDomainObject objAccDomain = EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        EP_ShipToSyncWithWINDMSXML objLocal = new EP_ShipToSyncWithWINDMSXML();
        objLocal.recordId = objAccDomain.localAccount.Id;
        Test.startTest();
        decimal dec = null;
        decimal result = objLocal.getGeoCordinateValue(dec);
        Test.stopTest();
        System.assertEquals( result,0.00000);        
    }
}