/*   
     @Author Aravindhan Ramalingam
     @name <EP_OrderStatePlanning.cls>     
     @Description <Non VMI Non Consignment Order State for Planning status>   
     @Version <1.1> 
     */

     public class EP_OrderStatePlanning extends EP_OrderState {

        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder){
            super.setOrderDomainObject(currentOrder);
        }
        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_OrderStatePlanning','doOnEntry');
        }
        
        public override void doOnExit(){
            EP_GeneralUtility.Log('Public','EP_OrderStatePlanning','doOnExit');
        }

        public static String getTextValue(){
            return EP_OrderConstant.OrderState_Planning;
        }
    }