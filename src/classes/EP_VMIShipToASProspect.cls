/*
 *  @Author <Accenture>
 *  @Name <EP_VMIShipToASProspect>
 *  @CreateDate <>
 *  @Description <VMI Ship To Account State for 01-Prospect Status>
 *  @Version <1.0>
 */
 public with sharing class EP_VMIShipToASProspect extends EP_AccountState{
     
    public EP_VMIShipToASProspect() {
        
    }

    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASProspect','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }

    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASProspect','doTransition');
        return super.doTransition();
    }

    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASProspect','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    }

}