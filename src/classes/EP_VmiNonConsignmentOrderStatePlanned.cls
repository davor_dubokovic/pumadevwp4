/*   
     @Author Aravindhan Ramalingam
     @name <EP_VmiNonConsignmentOrderStatePlanned.cls>     
     @Description <For Vmi Non consignment, Order State for Planned status>   
     @Version <1.1> 
     */

     public class EP_VmiNonConsignmentOrderStatePlanned extends EP_OrderState {
        
        public EP_VmiNonConsignmentOrderStatePlanned (){
            
        }
        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            super.setOrderDomainObject(currentOrder);
        }
        
        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_VmiNonConsignmentOrderStatePlanned ','doOnEntry');
            order.setOrderStockHoldingLocation();
            /*if(!order.isInventoryLow() && String.isBlank(this.order.getOrder().EP_Pricing_Status__c)){
                 this.orderService.calculatePrice();
            }*/   
        }
        
        public override void doOnExit(){
            EP_GeneralUtility.Log('Public','EP_VmiNonConsignmentOrderStatePlanned ','doOnExit');
           /* system.debug('Pricing status is ' + this.order.getOrder().EP_Pricing_Status__c);
            system.debug('Order credit status is ' + this.order.getOrder().EP_Order_Credit_Status__c);
            system.debug('Order credit status is ' + this.order.getOrder().EP_Credit_Status__c);
            system.debug('Order sync flag is ' + this.order.getOrder().EP_Sync_with_NAV__c);*/
            if(EP_Common_constant.PRICING_STATUS_PRICED.equalsignorecase(this.order.getOrder().EP_Pricing_Status__c) && 
                (EP_Common_Constant.Blank.equalsignorecase(this.order.getOrder().EP_Order_Credit_Status__c)) && !this.order.getOrder().EP_Sync_with_NAV__c){
                system.debug('In doOnExit doSyncStatusWithNav');
                this.orderService.doSyncStatusWithNav();
            }

            if(EP_Common_constant.Credit_Okay.equalsignorecase(this.order.getOrder().EP_Order_credit_status__c) 
                && EP_Common_constant.OK.equalsignorecase(this.order.getOrder().EP_Credit_Status__c) 
                && EP_Common_constant.PRICING_STATUS_PRICED.equalsignorecase(this.order.getOrder().EP_Pricing_Status__c) 
                && this.order.getOrder().EP_Sync_with_NAV__c){
                this.orderService.doSyncCreditCheckWithWINDMS();
            }
        }

    public static String getTextValue()
    {
        return EP_OrderConstant.OrderState_Planned;
    }
    

}