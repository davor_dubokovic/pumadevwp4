/*
    @Author          Accenture
    @Name            EP_GenerateOrderRequestXML
    @CreateDate      04/18/2017
    @Description     virtual class is used to generate outbound XML's of Order to Sync with NAV and WINDMS
    @Version         1.0
    @Reference       NA
*/
public virtual with sharing class EP_GenerateOrderRequestXML extends EP_GenerateRequestXML {
    
    public csord__Order__c orderObj;
	public String orderDataJSON; 
    public list<orderLineItemWrapper> orderLineItemsWrapper = new list<orderLineItemWrapper>();
    public list<csord__Order_Line_Item__c> orderLineItems = new list<csord__Order_Line_Item__c>();
    
    /**
    * @author           Accenture
    * @name             init
    * @date             04/18/2017
    * @description      This method is used to fetch data from Database which will be require to wrapped into XML
    * @param            NA
    * @return           NA
    */
    public  override virtual void init(){
        EP_GeneralUtility.Log('Public','EP_GenerateOrderRequestXML','init');
        MSGNode = doc.createRootElement(EP_OrderConstant.MSG, null, null);
        
        // Quering the Order object for the passed record id
        EP_OrderMapper ordermapper = new EP_OrderMapper();
        orderobj = ordermapper.getCSRecordById(recordid);  
        
        // Initiating orderlines wrapper data
        setOrderLineItems();
        
        // set order items
        setOrderItems();
        
        // Get relevant custom setting
        isEncryptionEnabled = false;
        EP_CS_OutboundMessageSetting__c outboundMsgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(messageType);
        if(outboundMsgSetting != null){
            isEncryptionEnabled = outboundMsgSetting.Payload_Encoded__c;
        }
    }
    
    /**
    * @author           Accenture
    * @name             createStatusPayLoad
    * @date             04/18/2017
    * @description      This method is used to create status payload
    * @param            NA
    * @return           NA
    */
    public override virtual void createStatusPayLoad(){
        EP_GeneralUtility.Log('Public','EP_GenerateOrderRequestXML','createStatusPayLoad');
        MSGNode.addChildElement(EP_OrderConstant.STATUS_PAYLOAD,null,null).addTextNode(getValueforNode(orderobj.Status__c)); 
        
    }
    
    /**
    * @author           Accenture
    * @name             setOrderLineItems
    * @date             02/08/2017
    * @description      Gets the order line items along with the invoice line items and sets values in wrapper
    * @param            NA
    * @return           NA
    */     
    public void setOrderLineItems() {
        EP_GeneralUtility.Log('Public','EP_GenerateOrderRequestXML','setOrderLineItems');
        this.orderLineItemsWrapper = new list<orderLineItemWrapper>();
        EP_OrderItemMapper ordItemMapper = new EP_OrderItemMapper();
        orderLineItemWrapper ordLineWrp = new orderLineItemWrapper();
        for(csord__Order_Line_Item__c orderLineitem : ordItemMapper.getCSOrderLineWithInvoiceItem(new set<id>{OrderObj.Id})) {
            ordLineWrp = new orderLineItemWrapper();
            ordLineWrp.orderLineItem = orderLineitem;
            ordLineWrp.orderLineSeqId = EP_IntegrationUtil.reCreateSeqId(messageId, orderLineitem.Id);
            ordLineWrp.invoiceLineItems = new list<csord__Order_Line_Item__c>();
            /* mategr
            if(!orderLineitem.Order_Products__r.isEmpty()) {
                ordLineWrp.invoiceLineItems.addAll(orderLineitem.Order_Products__r);
            }
            */
            this.orderLineItemsWrapper.add(ordLineWrp);
        }
    }
    
    /**
    * @author           Accenture
    * @name             setOrderItems
    * @date             02/08/2017
    * @description      Gets the order line items for the specified order
    * @param            NA
    * @return           NA
    */  
    public void setOrderItems() {
        EP_GeneralUtility.Log('Public','EP_GenerateOrderRequestXML','setOrderItems');
        EP_OrderItemMapper ordItemMapper = new EP_OrderItemMapper();
        this.orderLineItems = ordItemMapper.getCSOrderLineWithInvoiceItem(new set<id>{this.OrderObj.Id});
    }
    
   /* 
    @Author          Accenture
    @name            orderLineItemWrapper
    @CreateDate      02/08/2017
    @Description     Wrapper class to hold orderLineSeqId with its order line items and invoice line items
    @Version         1.0
    */              
    public with sharing class orderLineItemWrapper {
        public csord__Order_Line_Item__c orderLineItem {get;set;}
        public string orderLineSeqId {get;set;}
        public list<csord__Order_Line_Item__c> invoiceLineItems {get;set;}
    }    
    
    //L4_45352_START
    // Removing Mehods -formatDateAsString(Datetime),formatDateAsString(Date), as no longer use of these method here.   
    //L4_45352_END

    /**
    * @author           Accenture
    * @name             getShipToOfOrder
    * @date             04/18/2017
    * @description      This method is used to create XML for Order to Sync with NAV
    * @param            NA
    * @return           NA
    */
    public String getShipToOfOrder(){
        EP_GeneralUtility.Log('Public','EP_GenerateOrderRequestXML','getShipToOfOrder');
        String shiptovalue = EP_Common_Constant.BLANK;
        if(OrderObj.EP_Delivery_Type__c == Label.Delivery_Type || OrderObj.EP_Delivery_Type__c == Label.Consumption_Type){
            shiptovalue = getValueforNode(OrderObj.EP_Ship_To_ID__c);
        }else{
            //shiptovalue = getValueforNode(OrderObj.EP_Pickup_Location_ID__c);
            shiptovalue='';
        }
        return shiptovalue;
    }
    /*public String getShipToOfOrder(){
        EP_GeneralUtility.Log('Public','EP_GenerateOrderRequestXML','getShipToOfOrder');
        String shiptovalue = EP_Common_Constant.BLANK;
        if(EP_Common_Constant.TRANSFER_ORDER_RECORD_TYPE_NAME.equalsignorecase(OrderObj.EP_Order_Record_Type__c)){
            shiptovalue = getValueforNode(OrderObj.EP_Shipto_Storage_Location__c);
        }else{
            shiptovalue = getValueforNode(OrderObj.EP_Ship_To_ID__c);
        }
        return shiptovalue;
    }*/
}