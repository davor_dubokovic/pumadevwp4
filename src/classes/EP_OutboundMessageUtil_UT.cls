@isTest
public class EP_OutboundMessageUtil_UT
{
	
	static String MESSAGE_ID = 'SFD-GBL-CPE-24012017-16:00:20-024242';

    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    static testMethod void isAuthorized_PositiveScenariotest() {
		String secretCode = EP_Common_Constant.PARAM_SECRETCODE;
		Test.startTest();
		Boolean result = EP_OutboundMessageUtil.isAuthorized(secretCode);
		Test.stopTest();
		System.AssertEquals(true,result);
	}
	static testMethod void isAuthorized_NegativeScenariotest() {
		String secretCode;
		Test.startTest();
		Boolean result = EP_OutboundMessageUtil.isAuthorized(secretCode);
		Test.stopTest();
		System.AssertEquals(false,result);
	}
	static testMethod void getSObjectIdSet_test() {
		List<SObject> sObjectList = new List<SObject>{EP_TestDataUtility.getsellto()};
		Test.startTest();
		Set<Id> result = EP_OutboundMessageUtil.getSObjectIdSet(sObjectList);
		Test.stopTest();
		System.assertEquals(1,result.size());
	}
	static testMethod void setMessageHeader_test() {		
		List<EP_CS_OutboundMessageSetting__c> msgSettingList = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');		
		Test.startTest();
		EP_OutboundMessageHeader result = EP_OutboundMessageUtil.setMessageHeader(msgSettingList[0],MESSAGE_ID,EP_Common_Constant.AAF_COMPANY);
		Test.stopTest();
		System.assertEquals(true, result != null);
		System.assertEquals(true, result.MsgID == MESSAGE_ID);
		System.assertEquals(true, result.SourceCompany == EP_Common_Constant.AAF_COMPANY);
	}
	static testMethod void getPayloadXML_test() {
		List<EP_CS_OutboundMessageSetting__c> msgSettingList = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');		
		Id recordId = EP_TestDataUtility.getsellto().id;
		Test.startTest();
		String result = EP_OutboundMessageUtil.getPayloadXML(msgSettingList[0],recordId,MESSAGE_ID,EP_Common_Constant.EPUMA);
		Test.stopTest();
		System.assertEquals(true, result!= null);
	}
	static testMethod void redirectToErrorPage_test() {
		Test.startTest();
		PageReference result = EP_OutboundMessageUtil.redirectToErrorPage();
		Test.stopTest();
		System.AssertEquals(true,result != NULL);
        System.AssertEquals(true,result.getURL().toUpperCase().contains(EP_Common_Constant.UNAUTHORIZEDACCESSPAGESTR.toUpperCase()));
	}
}