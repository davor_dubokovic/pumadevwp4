/* 
   @Author          Accenture
   @name            EP_CustomerFundsUpdateHelper
   @CreateDate      03/10/2017
   @Description     Handler class for inbound account available funds update interface
   @Version         1.0
*/
public class EP_CustomerFundsUpdateHelper {
    
    /**
    * @author           Accenture
    * @name             setCustomerAttributes
    * @date             03/13/2017
    * @description      Populates all the lookup fields in the wrapper instance and returns the same instance
    * @param            EP_CustomerFundsUpdateStub
    * @return           EP_CustomerFundsUpdateStub
    */
    public static void setCustomerAttributes(List<EP_CustomerFundsUpdateStub.customer> customers){
        EP_GeneralUtility.Log('public','EP_CustomerFundsUpdateHelper','setCustomerAttributes');
        set<string> setOfCustomerIDs = getCustomerIdSet(customers);
        EP_AccountMapper accountMapper = new EP_AccountMapper();
        Map<String,Account> mapCompositeIDCustomers = accountMapper.getAccountsMapByCompositeKey(setOfCustomerIDs);
        for(EP_CustomerFundsUpdateStub.Customer cust : customers){ 
        	setAccount(cust,mapCompositeIDCustomers);
        	setAccountFields(cust);   
            //L4# 67333 Code Changes Start
            if(!String.isBlank(cust.creditHoldingInfo.creditHoldingId) && cust.creditHoldingInfo.creditHoldingId <> cust.identifier.billTo){
                setHoldingAccount(cust,mapCompositeIDCustomers);
                setHoldingAccountFields(cust,cust.holdingAccount);
            }else{
                setHoldingAccountFields(cust,cust.SFAccount);
            }
            //L4# 67333 Code Changes End 
        }
    }
    /**
    * @author           Accenture
    * @name             setAccountFields
    * @date             03/13/2017
    * @description      Mapping all account fields in the wrapper Variables
    * @param            EP_CustomerFundsUpdateStub.Customer
    * @return           NA
    */
    @TestVisible
    private static void setAccountFields(EP_CustomerFundsUpdateStub.Customer cust){
    	EP_GeneralUtility.Log('private','EP_CustomerFundsUpdateHelper','setAccountFields');
    	cust.SFAccount.EP_Source_Seq_Id__c = cust.seqId;
        /*CAM 2.7 fix start*/
            //cust.SFAccount.EP_Available_Funds_LCY__c = (cust.availableAmt!=null ?  cust.availableAmt : null);//(cust.overdueAmt!=null ? cust.overdueAmt : null);
            cust.SFAccount.EP_Overdue_Invoice_LCY__c = ((cust.overdueAmt !=EP_Common_Constant.BLANK && isNumeric(cust.overdueAmt.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK))) ? decimal.valueOf(cust.overdueAmt.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK)) : null );
            cust.SFAccount.EP_Overdue_Invoice__c = ((cust.overdueAmtCustCurr!=EP_Common_Constant.BLANK && isNumeric(cust.overdueAmtCustCurr.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK))) ? decimal.valueOf(cust.overdueAmtCustCurr.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK)) : null );
            cust.SFAccount.EP_Balance_LCY__c = ((cust.balance!=EP_Common_Constant.BLANK && isNumeric(cust.balance.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK))) ? decimal.valueOf(cust.balance.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK)) : null );
            cust.SFAccount.EP_Balance__c = ((cust.balanceCustCurr!= EP_Common_Constant.BLANK && isNumeric(cust.balanceCustCurr.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK))) ? decimal.valueOf(cust.balanceCustCurr.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK)) : null);
            cust.SFAccount.EP_Version_Number__c = (cust.versionNr!= EP_Common_Constant.BLANK ? cust.versionNr : null );
        /*CAM 2.7 fix end*/
    	cust.SFAccount.CurrencyIsoCode = cust.currencyId;
    }
    
    /**
    * @author           Accenture
    * @name             isNumeric
    * @date             03/13/2017
    * @description      Returns Boolean if string value converting to decimal else if return false
    * @param            String
    * @return           Boolean
    */
    public static Boolean isNumeric(String CustInfo){
        Boolean ReturnValue = false;
        try{
            Decimal.valueOf(CustInfo);
            ReturnValue = TRUE; 
        } catch (Exception e) {
            ReturnValue = FALSE;
        }
        return ReturnValue;
    }
    /**
    * @author           Accenture
    * @name             getCustomerIdSet
    * @date             03/13/2017
    * @description      returns the set of customer ids from the wrapper instance
    * @param            List<EP_CustomerFundsUpdateStub.Customer>
    * @return           set<string>
    */
    @TestVisible
    private static set<string> getCustomerIdSet(List<EP_CustomerFundsUpdateStub.Customer> customers){
        EP_GeneralUtility.Log('Private','EP_CustomerFundsUpdateHelper','getCompositeIDs');
        set<string> setOfCustomerIDs = new set<string>();
        for(EP_CustomerFundsUpdateStub.Customer cust : customers) {
            if(!string.isBlank(cust.identifier.billTo)){
                setOfCustomerIDs.add(EP_GeneralUtility.getCompositeKey(cust.identifier.clientId,cust.identifier.billTo));
            }
            //L4# 67333 Code Changes Start 
            if(!string.isBlank(cust.creditHoldingInfo.creditHoldingId)){
                setOfCustomerIDs.add(EP_GeneralUtility.getCompositeKey(cust.identifier.clientId,cust.creditHoldingInfo.creditHoldingId));
            }
             //L4# 67333 Code Changes End
        }
        return setOfCustomerIDs;
    }
    
    /**
    * @author           Accenture
    * @name             setAccount
    * @date             03/13/2017
    * @description      Populates the account id in the wrapper instance, if not found adds error message
    * @param            EP_CustomerFundsUpdateStub.Customer, Map<String,Account>
    * @return           NA
    */
    @TestVisible
    private static void setAccount(EP_CustomerFundsUpdateStub.Customer customer, Map<String,Account> mapCompositeIDCustomers){
        EP_GeneralUtility.Log('private','EP_CustomerFundsUpdateHelper','setAccount');
        string compositeKey = EP_GeneralUtility.getCompositeKey(customer.identifier.clientId,customer.identifier.billTo).toUpperCase();
        if(mapCompositeIDCustomers.containsKey(compositeKey)) {
            customer.SFAccount = mapCompositeIDCustomers.get(compositeKey);
        } else {
        	customer.SFAccount = new Account();
            string errorMessage = EP_COMMON_CONSTANT.CUSTOMER.capitalize() +EP_Common_CONSTANT.SPACE +  customer.SFAccount.EP_Composite_Id__c + EP_Common_CONSTANT.SPACE + EP_COMMON_CONSTANT.NOT_FOUND;
            customer.errorDescription = string.isBlank(customer.errorDescription) ? errorMessage : customer.errorDescription + EP_COMMON_CONSTANT.SEMICOLON + errorMessage;
        }
    }
    
    //L4 #67333 Code Changes Start
     /**
    * @author           Accenture
    * @name             setHoldingAccountFields
    * @date             02/28/2018
    * @description      Mapping all holding account fields in the wrapper Variables
    * @param            EP_CustomerFundsUpdateStub.Customer
    * @return           NA
    */
    @TestVisible
    private static void setHoldingAccountFields(EP_CustomerFundsUpdateStub.Customer cust, Account accountObj){
        EP_GeneralUtility.Log('private','EP_CustomerFundsUpdateHelper','setHoldingAccountFields');
        accountObj.EP_Available_Funds_LCY__c = ((!String.isBlank(cust.creditHoldingInfo.availableAmt) && isNumeric(cust.creditHoldingInfo.availableAmt.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK))) ? decimal.valueOf(cust.creditHoldingInfo.availableAmt.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK)) : null );
        accountObj.EP_AvailableFunds__c = ((!String.isBlank(cust.creditHoldingInfo.availableAmtCustCurr) && isNumeric(cust.creditHoldingInfo.availableAmtCustCurr.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK))) ? decimal.valueOf(cust.creditHoldingInfo.availableAmtCustCurr.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK)) : null );
        accountObj.EP_Holding_Overdue_Balance_LCY__c = ((!String.isBlank(cust.creditHoldingInfo.overDueBalance) && isNumeric(cust.creditHoldingInfo.overDueBalance.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK))) ? decimal.valueOf(cust.creditHoldingInfo.overDueBalance.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK)) : null );
        accountObj.EP_Holding_Overdue_Balance_HCY__c = ((!String.isBlank(cust.creditHoldingInfo.overDueBalanceCustCurr) && isNumeric(cust.creditHoldingInfo.overDueBalanceCustCurr.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK))) ? decimal.valueOf(cust.creditHoldingInfo.overDueBalanceCustCurr.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK)) : null );
    }
    
      /**
    * @author           Accenture
    * @name             setHoldingAccount
    * @date             02/28/2018
    * @description      Populates the holding account id in the wrapper instance
    * @param            EP_CustomerFundsUpdateStub.Customer, Map<String,Account>
    * @return           NA
    */
    @TestVisible
    private static void setHoldingAccount(EP_CustomerFundsUpdateStub.Customer customer, Map<String,Account> mapCompositeIDCustomers){
        EP_GeneralUtility.Log('private','EP_CustomerFundsUpdateHelper','setHoldingAccount');
        string compositeKey = EP_GeneralUtility.getCompositeKey(customer.identifier.clientId,customer.creditHoldingInfo.creditHoldingId).toUpperCase();
        if(mapCompositeIDCustomers.containsKey(compositeKey)) {
            customer.holdingAccount = mapCompositeIDCustomers.get(compositeKey);
        }else {
            customer.holdingAccount = new Account();
            string errorMessage = EP_COMMON_CONSTANT.CUSTOMER.capitalize() +EP_Common_CONSTANT.SPACE +  customer.holdingAccount.EP_Composite_Id__c + EP_Common_CONSTANT.SPACE + EP_COMMON_CONSTANT.NOT_FOUND;
            customer.errorDescription = string.isBlank(customer.errorDescription) ? errorMessage : customer.errorDescription + EP_COMMON_CONSTANT.SEMICOLON + errorMessage;
        } 
    }
    //L4# 67333 Code Changes End
}