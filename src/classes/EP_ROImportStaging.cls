/*
    @Author          Accenture
    @Name            EP_ROImportStaging
    @CreateDate      04/18/2018
    @Description     virtual class is used to RO Import Staging Object
    @Version         1.0
    @Reference       NA
*/
public virtual without sharing class EP_ROImportStaging {
    public Id fileId;
    public list<EP_RO_Import_Staging__c> stagingRecords = new list<EP_RO_Import_Staging__c>();
    public map<string,list<EP_RO_Import_Staging__c>> consolidatedRecordsMap = new map<string,list<EP_RO_Import_Staging__c>>();
    public map<string, string> deliveryDocNoWithOrdNo = new map<string, string>();
    public map<string, Account> SellToShipToCompositeKeyMap = new map<string, Account>();
    public map<string, Account> NavStockLocationMap = new map<string, Account>();
    public map<string, csord__Order__c> OrderNumberMap = new map<string, csord__Order__c>();
    public map<string, Product2> productCodeMap = new map<string, Product2>();
    public map<string, Account> supplierMap = new map<string, Account>();
    public map<string, Account> transporterMap = new map<string, Account>();
    public map<string, Contract> ContractNumberMap = new map<string, Contract>();
    public boolean isError = false;
    
    /**
    * @author       Accenture
    * @name         processStagingDataValidation
    * @date         04/18/2018
    * @description  Method to start data Validation on Staging records for given file Id
    * @param        Id
    * @return       NA
    */ 
    public virtual void processStagingDataValidation(Id fileId){
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','processStagingDataValidation');
    	try {
	    	init(fileId);
	    	masterDataVerification();
	    	updateStagingRecords();
	    	updateFileRecord();
    	} catch(exception exp) {
    		EP_LoggingService.logHandledException(exp, EP_Common_Constant.EPUMA, 'processStagingDataValidation',EP_ROImportStaging.class.getName(), ApexPages.Severity.ERROR);
    	}
    }
	
	/**
    * @author       Accenture
    * @name         masterDataVerification
    * @date         04/18/2018
    * @description  virtual method for master data validation, must be implemented by extended class 
    * @param        NA
    * @return       NA
    */ 
    public virtual void masterDataVerification(){}
    
    /**
    * @author       Accenture
    * @name         init
    * @date         04/18/2018
    * @description  This method is used to inti the request by setting the file Id and start Data set process
    * @param        Id
    * @return       NA
    */ 
    public virtual void init(Id fileId){
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','init');
    	this.fileId = fileId;
    	stagingRecords = new EP_ROImportStagingMapper().getRecordByFileId(this.fileId);
    	createDataSets();
    }
    
    /**
    * @author       Accenture
    * @name         updateStagingRecords
    * @date         04/18/2018
    * @description  This method is used to update Staging Records releated to given file
    * @param        NA
    * @return       NA
    */ 
    public virtual void updateStagingRecords() {
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','updateStagingRecords');
    	setConsolidatedStatus();
    	//Do not run the Staging trigger. The trigger should run only for the User Context.
    	EP_CheckRecursive.run = false;
    	database.update(stagingRecords);
    }
	
	/**
    * @author       Accenture
    * @name         setConsolidatedStatus
    * @date         04/18/2018
    * @description  This method is used to set Consolidated Status for all Staging Records releated to given file
    * @param        EP_RO_Import_Staging__c,EP_RO_Import_Staging__c
    * @return       NA
    */ 
    public virtual void setConsolidatedStatus() {
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','updateConsolidatedStatus');
    	for(string consolKey : getConsolidatedKeyForErrors()){
    		system.debug('***consolKey***' +consolKey);
    		for(EP_RO_Import_Staging__c stagingRecord : consolidatedRecordsMap.get(consolKey)){
    			system.debug('***stagingRecord***' +stagingRecord);
				stagingRecord.EP_Consolidated_Status__c = EP_Common_Constant.ERROR;
			}
    	}
    }
    
    /**
    * @author       Accenture
    * @name         updateFileRecord
    * @date         04/18/2018
    * @description  This method is used to update File Records status as Processed and In-Progress checkbox as false
    * @param        NA
    * @return       NA
    */ 
    public virtual void updateFileRecord() {
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','updateFileRecord');
		EP_File__c fileRec = new EP_File__c(Id = fileId);
		fileRec.EP_Status__c = EP_Common_Constant.STATUS_PROCESSED;
		fileRec.EP_In_Process__c = false;
    	database.update(fileRec);
    }
    
    /**
    * @author       Accenture
    * @name         resetFields
    * @date         04/18/2018
    * @description  This method is used to init or reset Fields
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    public virtual void resetFields(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','resetFields');
	    stagingRec.EP_Status__c = EP_Common_Constant.STATUS_PENDING;
	    stagingRec.EP_Consolidated_Status__c = EP_Common_Constant.STATUS_PENDING;
    }
    
    /**
    * @author       Accenture
    * @name         verifyDeliveryDocket
    * @date         04/18/2018
    * @description  This method is used to validate Delivery Docket,add error if Delivery Docket is already exits on Orders
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    public virtual void verifyDeliveryDocket(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','verifyDeliveryDocket');
    	if(isError) return;
    	//TODO - Update code to get EP_Delivery_Docket_Number__c field on Order Object in Mapper Class
    	if(deliveryDocNoWithOrdNo.containsKey(stagingRec.EP_Delivery_Docket_Number__c)) {
    		addError(stagingRec, getErrorMsg(new list<string>{deliveryDocNoWithOrdNo.get(stagingRec.EP_Delivery_Docket_Number__c)}, system.label.EP_Delivery_Docket_Error_Msg));
    	}
    }
    
    /**
    * @author       Accenture
    * @name         duplicateDataCheck
    * @date         04/18/2018
    * @description  This method is used to check duplicate Records which in a file Records
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
	public virtual void duplicateDataCheck(EP_RO_Import_Staging__c stagingRec) {
		EP_GeneralUtility.Log('public','EP_ROImportStaging','duplicateDataCheck');
    	if(stagingRec.EP_Order_Number__c == null) {
    		for(EP_RO_Import_Staging__c existingStagingRec : stagingRecords) {
	    		if(existingStagingRec.Id != stagingRec.id && stagingRec.EP_Duplicate_Identifier__c == existingStagingRec.EP_Duplicate_Identifier__c) {
	    			setDuplicateStatus(stagingRec, existingStagingRec);
	    		}
	    	}
    	}
    }
    
    /**
    * @author       Accenture
    * @name         verifySupplyLocation
    * @date         04/18/2018
    * @description  This method is used to validate SupplyLocation Number, Add error if SupplyLocation Number is not valid
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    public virtual void verifySupplyLocation(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','verifySupplyLocation');
    	//Reset the Field value null if Staging records updated with new value and its match not found then set the lookup as null
    	if(isError) return;
    	stagingRec.EP_SupplyLocation__c = null;
    	
    	if(NavStockLocationMap.containsKey(stagingRec.EP_Supply_Location_Code__c)) {
    		stagingRec.EP_SupplyLocation__c = NavStockLocationMap.get(stagingRec.EP_Supply_Location_Code__c).id;
    	} else {
    		addError(stagingRec, getErrorMsg(new list<string>{stagingRec.EP_Supply_Location_Code__c}, system.label.EP_SupplyLocation_Error_Msg));
    	}
    }
    
    /**
    * @author       Accenture
    * @name         verifySellTo
    * @date         04/18/2018
    * @description  This method is used to validate SellTo Number, Add error if SellTo Number is not valid
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    public virtual void verifySellTo(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','verifySellTo');
    	if(isError) return;
    	
    	stagingRec.EP_Sell_To__c = null;
    	if(SellToShipToCompositeKeyMap.containsKey(stagingRec.EP_Sell_To_Composit_key__c)) {
    		stagingRec.EP_Sell_To__c = SellToShipToCompositeKeyMap.get(stagingRec.EP_Sell_To_Composit_key__c).id;
    	} else {
    		addError(stagingRec, getErrorMsg(new list<string>{stagingRec.EP_CompanyCode__c}, system.label.EP_SellTo_Error_Msg));
    	}
    }
     
    /**
    * @author       Accenture
    * @name         verifyShipTo
    * @date         04/18/2018
    * @description  This method is used to validate ShipTo Number, Add error if ShipTo Number is not valid
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    public virtual void verifyShipTo(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','verifyShipTo');
    	if(isError) return;
    	stagingRec.EP_Ship_To__c = null;
    	
    	if(SellToShipToCompositeKeyMap.containsKey(stagingRec.EP_Ship_To_Composit_Key__c)) {
    		stagingRec.EP_Ship_To__c = SellToShipToCompositeKeyMap.get(stagingRec.EP_Ship_To_Composit_Key__c).id;
    	} else {
    		addError(stagingRec, getErrorMsg(new list<string>{stagingRec.EP_CompanyCode__c}, system.label.EP_ShipTo_Error_Msg));
    	}
    }
    
    /**
    * @author       Accenture
    * @name         verifySellToShipToRelationship
    * @date         04/18/2018
    * @description  This method is used to validate the sellTo , ShipTo relationship, Add error if relationship is not valid
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    public virtual void verifySellToShipToRelationship(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','verifySellToShipToRelationship');
    	if(isError) return;
    	stagingRec.EP_Ship_To__c = null;
    	
    	if(SellToShipToCompositeKeyMap.containsKey(stagingRec.EP_Ship_To_Composit_Key__c)) {
    		stagingRec.EP_Ship_To__c = SellToShipToCompositeKeyMap.get(stagingRec.EP_Ship_To_Composit_Key__c).id;
    	} else {
    		addError(stagingRec, system.label.EP_SellTo_and_ShipTo_Validation_Error_Msg);
    	}
    }
    
    /**
    * @author       Accenture
    * @name         verifyProductCode
    * @date         04/18/2018
    * @description  This method is used to validate the Product Code and Populate the Product lookup, Add error if Product Code is not valid
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    public virtual void verifyProductCode(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','verifyProductCode');
    	if(isError) return;
    	stagingRec.EP_Product__c = null;
    	
    	if(productCodeMap.containsKey(stagingRec.EP_Product_Unique_Key__c)) {
    		stagingRec.EP_Product__c = productCodeMap.get(stagingRec.EP_Product_Unique_Key__c).id;
    	} else {
    		addError(stagingRec, getErrorMsg(new list<string>{stagingRec.EP_Product_Code__c}, system.label.EP_Product_Code_Error_Msg));
    	}
    }
    
    /**
    * @author       Accenture
    * @name         addError
    * @date         04/18/2018
    * @description  This method is used to set the error message and status on staging record
    * @param        EP_RO_Import_Staging__c,string
    * @return       NA
    */ 
    protected void addError(EP_RO_Import_Staging__c stagingRec, string errorMsg) {
    	EP_GeneralUtility.Log('protected','EP_ROImportStaging','addError');
    	stagingRec.EP_Remark_Reason__c = errorMsg;
		stagingRec.EP_Status__c = EP_Common_Constant.ERROR;
		isError = true;
    }
    
    /**
    * @author       Accenture
    * @name         createDataSets
    * @date         04/18/2018
    * @description  This method is used to create sets of data from staging Object to retrive the records from various Objects
    * @param        NA
    * @return       NA
    */ 
    @testVisible 
    private void createDataSets() {
    	EP_GeneralUtility.Log('private','EP_ROImportStaging','createDataSets');
    	set<string> acCompositeKeySet = new set<string>();
    	set<string> deliveryDocketSet = new set<string>();
    	set<string> orderNumberSet = new set<string>();
    	set<string> navStockLocationSet = new set<string>();//i.	Supply location 
    	set<string> productCodeSet = new set<string>();//EP_Composite_Id__c
    	set<string> transporterSet = new set<string>();//EP_Vendor_Unique_Id__c 
    	set<string> supplierNumberSet = new set<string>();//EP_Vendor_Unique_Id__c
    	set<string> contractNumberSet  = new set<string>();//
    	
    	for(EP_RO_Import_Staging__c stagingRec : stagingRecords) {
    		acCompositeKeySet.add(stagingRec.EP_Sell_To_Composit_key__c);
    		acCompositeKeySet.add(stagingRec.EP_Ship_To_Composit_Key__c);
    		deliveryDocketSet.add(stagingRec.EP_Delivery_Docket_Number__c);
    		orderNumberSet.add(stagingRec.EP_Order_Number__c);
    		navStockLocationSet.add(stagingRec.EP_Supply_Location_Code__c);
    		productCodeSet.add(stagingRec.EP_Product_Unique_Key__c);
    		transporterSet.add(stagingRec.EP_Transporter_Number__c);
    		supplierNumberSet.add(stagingRec.EP_Supplier_Number__c);
    		contractNumberSet.add(stagingRec.EP_Contract_Number__c);
    		createConsolidatedRecordsMap(stagingRec);
    	}
    	createAccountReleatedMaps(acCompositeKeySet,navStockLocationSet, supplierNumberSet, transporterSet);
    	createOrderReleatedMaps(deliveryDocketSet,orderNumberSet);
    	productCodeMap = new EP_ProductMapper().getRecordsByNAVProductCompany(productCodeSet);
    	ContractNumberMap = new EP_ContractMapper().mapContractNumber(contractNumberSet);
    }
    
    /**
    * @author       Accenture
    * @name         createAccountReleatedMaps
    * @date         04/18/2018
    * @description  This method is used to created maps of data which are related to Account from the Account Mapper class
    * @param        set<string>,set<string>, set<string>, set<string>, set<string>
    * @return       NA
    */ 
    @testVisible 
    private void createAccountReleatedMaps(set<string> acCompositeKeySet, set<string> navStockLocationSet, set<string> supplierNumberSet,set<string> transporterSet ) {
    	EP_GeneralUtility.Log('private','EP_ROImportStaging','createAccountReleatedMaps');
    	EP_AccountMapper accMapper = new EP_AccountMapper();
    	SellToShipToCompositeKeyMap = accMapper.getCustomersMapByCompositeKey(accompositeKeySet);
    	NavStockLocationMap = accMapper.getNavStockLocation(navStockLocationSet);
    	supplierMap = accMapper.getSupplierBysupplierNumber(supplierNumberSet);
    	transporterMap = accMapper.getTransporter(transporterSet);
    }
    
    /**
    * @author       Accenture
    * @name         createOrderReleatedMaps
    * @date         04/18/2018
    * @description  This method us used to created maps of data which are related to order from the Order Mapper class
    * @param        set<string>,set<string>, set<string>
    * @return       NA
    */ 
    @testVisible 
    private void createOrderReleatedMaps(set<string> deliveryDocketSet, set<string> orderNumberSet) {
    	EP_GeneralUtility.Log('private','EP_ROImportStaging','createOrderReleatedMaps');
    	EP_OrderMapper ordMapper = new EP_OrderMapper();
    	deliveryDocNoWithOrdNo = ordMapper.getDeliveryDockets(deliveryDocketSet);
    	OrderNumberMap = ordMapper.getAcceptedOrderByOrderNumber(orderNumberSet);
    }
    
    /**
    * @author       Accenture
    * @name         createConsolidatedRecordsMap
    * @date         04/18/2018
    * @description  This method is used create Consolidation Record Map on Consolidation ket with list of Staging records
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    @testVisible
    private void createConsolidatedRecordsMap(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('private','EP_ROImportStaging','createConsolidatedRecordsMap');		
		if(consolidatedRecordsMap.containsKey(stagingRec.EP_Staging_Order_Identifier__c)){
			 consolidatedRecordsMap.get(stagingRec.EP_Staging_Order_Identifier__c).add(stagingRec);
		} else {
			consolidatedRecordsMap.put(stagingRec.EP_Staging_Order_Identifier__c, new List<EP_RO_Import_Staging__c> {stagingRec});
		}
    }
    
    /**
    * @author       Accenture
    * @name         getConsolidatedKeyForErrors
    * @date         04/18/2018
    * @description  This method is used get Consolidated Key For staging Errors having any error
    * @param        NA
    * @return       set<string>
    */ 
    @testVisible
    private set<string> getConsolidatedKeyForErrors() {
    	EP_GeneralUtility.Log('private','EP_ROImportStaging','getConsolidatedKeyForErrors');
    	set<string> consolidatedErrorKeySet = new set<string>();
    	for(String consolKey : consolidatedRecordsMap.keySet()){
    		for(EP_RO_Import_Staging__c stagingRecord : consolidatedRecordsMap.get(consolKey)){
				if(EP_Common_Constant.ERROR.equalsIgnoreCase(stagingRecord.EP_Status__c) || 
				EP_Common_Constant.STATUS_DUPLICATE.equalsIgnoreCase(stagingRecord.EP_Status__c)) {
					consolidatedErrorKeySet.add(consolKey);
					break;
				}
			}
    	}
    	return consolidatedErrorKeySet;
    }
    
    /**
    * @author       Accenture
    * @name         getErrorMsg
    * @date         04/18/2018
    * @description  This method is used to create error message from custom lable by replacing replacment token by given list of values
    * @param        list<string>,string
    * @return       string
    */ 
    @testVisible
    protected string getErrorMsg(list<string> replacementTokens, string customLabel) {
    	EP_GeneralUtility.Log('protected','EP_ROImportStaging','getErrorMsg');
    	return EP_OrderImportHelper.createErrorMessage(replacementTokens, customLabel);
    }
    
    /**
    * @author       Accenture
    * @name         setDuplicateStatus
    * @date         04/18/2018
    * @description  method to set duplicate status for duplicate staging records
    * @param        EP_RO_Import_Staging__c,EP_RO_Import_Staging__c
    * @return       NA
    */ 
    @testVisible
    private void setDuplicateStatus(EP_RO_Import_Staging__c stagingRec,EP_RO_Import_Staging__c existingStagingRec){
	    stagingRec.EP_Remark_Reason__c = getErrorMsg(new list<string>{existingStagingRec.Name, stagingRec.Name}, system.label.EP_Duplicate_Record_Error_Msg);
		stagingRec.EP_Status__c = EP_Common_Constant.STATUS_DUPLICATE;
		stagingRec.EP_Consolidated_Status__c = EP_Common_Constant.ERROR;
    }
}