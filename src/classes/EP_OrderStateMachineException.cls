/* 
	@Author			Accenture
	@name			EP_OrderStateMachineException
	@CreateDate 	2/10/2017
  	@Description	Custom Exception class to handle state machine exceptions.
	@Version		1.0
*/
public class EP_OrderStateMachineException extends Exception{

}