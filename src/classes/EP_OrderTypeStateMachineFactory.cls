/*
 *  @Author <Aravindhan Ramalingam>
 *  @Name <EP_OrderState>
 *  @CreateDate <03/02/2017>
 *  @Description <OrderTypeStateMachineFactory provides right state machine based on order record type and ship to type>
 *  @Version <1.0>
 */

 public class EP_OrderTypeStateMachineFactory {
    static EP_OrderDomainObject localorder;
    static EP_OrderStateMachine orderStatemachine;
    
    private static final String PUMA_SYNTAX = 'EP_';
    private static final String STATEMACHINE = 'SM';
    
    public EP_OrderTypeStateMachineFactory(EP_OrderDomainObject currentOrder)
    {
        localorder = currentOrder;
    }
    
    public EP_OrderTypeStateMachineFactory()
    {

    }
    
    // a Static method to return the state in some invocation scenarios     
    public static EP_OrderStateMachine getOrderStateMachine(){
        EP_GeneralUtility.Log('Public','EP_OrderTypeStateMachineFactory','getOrderStateMachine');
        return getOrderStateMachineInstance(localorder);
    }
    
    public EP_OrderStateMachine getOrderStateMachine(EP_OrderDomainObject currentOrder){
        EP_GeneralUtility.Log('Public','EP_OrderTypeStateMachineFactory','getOrderStateMachine');
        return getOrderStateMachineInstance(currentOrder);
    }
    @TestVisible
    private static EP_OrderStateMachine getOrderStateMachineInstance(EP_OrderDomainObject currentOrder)
    {
        EP_GeneralUtility.Log('Private','EP_OrderTypeStateMachineFactory','getOrderStateMachineInstance');
        localorder = currentOrder;
        String stateMachineType = PUMA_SYNTAX + currentOrder.getOrderTypeClassification() + STATEMACHINE;
        system.debug('stateMachineType' + stateMachineType);
        
        EP_OrderStateMachine ost;
        try{
            Type t = Type.forName(stateMachineType);
            ost = (EP_OrderStateMachine)t.newInstance();
            ost.setOrderDomainObject(localorder);
            //ost.orderEvent = OrderEvent;     
            orderStatemachine = ost;        
            }catch(Exception e){
                throw new EP_OrderStateMachineException('Order State Machine not available for ' +stateMachineType);
            }

            return ost; 
        }
    }