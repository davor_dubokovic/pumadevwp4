/*
  @Author <Accenture>
   @name <EP_ScheduleVMINotificationsBatchClass_R1>
   @Description <This batch class is scheuled for VMI notifications > 
   @Version <1.0>
*/
global class EP_ScheduleVMINotificationsBatchClass_R1 implements Schedulable {
   
   /*
   @Description:Execute() of Schedulable Interface
   */
   global void execute(SchedulableContext SC) {
      EP_GenerateVMINotificationsBatchClass_R1 bc = new EP_GenerateVMINotificationsBatchClass_R1(); 
      database.executebatch(bc);
    /*
        String CRON_EXP = '0 0 * * * ?';
       EP_ScheduleVMINotificationsBatchClass sch = new EP_ScheduleVMINotificationsBatchClass ();
       system.schedule('Schedule Dips Generation Hourly', CRON_EXP, sch);
    */
   }
}