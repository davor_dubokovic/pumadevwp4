/* 
   @Author <Accenture>
   @name <EP_CreditExceptionUpdateStub>
   @CreateDate <03/14/2017>
   @Description <These are the stub classes to map JSON string for Credit Exception Status update Rest Service Request from NAV> 
   @Version <1.0>
*/
/* Main stub Class for JSON parsing for VMI order Creation from WINDMS  */
public with sharing class EP_CreditExceptionUpdateStub {
	/* Class for MSG */
	public MSG MSG;
	
	 /* stub Class for MSG */
	public with sharing  class MSG {
		public EP_MessageHeader HeaderCommon;
		public Payload Payload;
	}
	
	/* stub Class for payload */
	public with sharing class Payload {
		public Any0 any0;
	}
	
	/* stub Class for Any0 */
	public with sharing class Any0 {
		public CreditExceptionStatusesType creditExceptionStatus;
	}
	
	/* stub Class for  CreditExceptionStatuses */
	public with sharing class CreditExceptionStatusesType {
		public List<CreditExceptionStatusType> status;
	}
	
	/* stub Class for CreditExceptionStatusType */
	public with sharing class CreditExceptionStatusType {
		//Added New variables
		public boolean isProcess;
		public string failureReason;
		public String seqId;
        public Identifier identifier;
		public EP_Credit_Exception_Request__c creditExcpObj {
			get {
        		if (creditExcpObj == null){
            	    creditExcpObj = new EP_Credit_Exception_Request__c();
            	} 
            	return creditExcpObj;
        	}
        	set;
		}
		
        /*Defect 83186 start*/
        public String approvalStatus {
            get{
                return creditExcpObj.EP_Status__c;
            }
            set{
               if(value == EP_Common_Constant.CREDIT_EXC_STATUS_AUTO_APPROVED){
                  creditExcpObj.EP_Status__c = EP_Common_Constant.CREDIT_EXC_STATUS_APPROVED; 
               }
               else {
                   creditExcpObj.EP_Status__c = value;
               }
            }
        }
        /*Defect 83186 end*/
        //45435 Perform Credit Exception Review --Start 
        public String reasonForRejection {
        	get{
                return creditExcpObj.EP_Reason__c;
            }
            set{
                creditExcpObj.EP_Reason__c = value;
            }
        }
        public String approverId {
        	get{
                return creditExcpObj.EP_ApproverId__c;
            }
            set{
                creditExcpObj.EP_ApproverId__c = value;
            }
        }
        public String modifiedDt {
        	get{
                return creditExcpObj.EP_Modified_Date_in_NAV__c;
            }
            set{
                creditExcpObj.EP_Modified_Date_in_NAV__c = value;
            }
        }
        public String modifiedBy {
        	get{
                return creditExcpObj.EP_Modified_By_in_NAV__c;
            }
            set{
                creditExcpObj.EP_Modified_By_in_NAV__c = value;
            }
        }
        public String versionNr;
        //45435 Perform Credit Exception Review --End
	}
	/* stub Class for Identifier */
	public with sharing class Identifier {
		public String exceptionNo;
		public String billTo;
		public String clientId;
	}
}