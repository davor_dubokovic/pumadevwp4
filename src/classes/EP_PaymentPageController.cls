/*   
     @Author <Accenture>
     @name <EP_PaymentPageController.cls>     
     @Description <This class is used as a controller class of payment>   
     @Version <1.1> 
*/
public without sharing class EP_PaymentPageController {

     /*
        This method is used to pay the order
    */
    public static PageReference makePayment() {
        System.debug('inside payment');
        PageReference ref = NULL;
        Id orderId = ApexPages.currentPage().getParameters().get(EP_Common_Constant.ID);
 
       Order newOrderRecord = [Select id, Status from Order where id =: orderId limit : EP_Common_Constant.ONE ];
        if (newOrderRecord != NULL) {
            newOrderRecord.Status = EP_Common_Constant.SUBMITTED;

            try {
                Database.Update(newOrderRecord);
                ref = new PageReference(EP_Common_Constant.SLASH + newOrderRecord.Id);
            } catch (Exception ex) {
                ApexPages.addMessages(ex);
            }
        }
     

        return ref;
}

 /*
        This method is used to cancel the order when available funds is not available
    */
    public PageReference cancelPayment() {
        System.debug('inside cancel payment');
        PageReference ref = NULL;
        try{
            Id orderId = ApexPages.currentPage().getParameters().get(EP_Common_Constant.ID);
            Order newOrderRecord = [Select id, Status from Order where id =: orderId limit : EP_Common_Constant.ONE];
            if (orderId != NULL) {
                ref = new PageReference(EP_Common_Constant.SLASH + newOrderRecord.Id);
            } else {
                ref = new PageReference(EP_Common_Constant.HOME_PAGE_URL);
            }
        }catch( Exception ex ){
            ApexPages.addMessages(ex);    
        }
        return ref;
    }
}