/*
    @Author          Accenture
    @Name            EP_Third_Party_Order
    @CreateDate      04/18/2018
    @Description     class is used to RO Import Staging Object for Third_Party_Order
    @Version         1.0
    @Reference       NA
*/
public with sharing class EP_Third_Party_Order extends EP_ROImportStaging {
    public map<String,list<EP_RO_Import_Staging__c>> orderStagingRecordsMap = new map<String,list<EP_RO_Import_Staging__c>>();
    
    /**
    * @author       Accenture
    * @name         masterDataVerification
    * @date         04/18/2018
    * @description  This method is used for master data validation on staging records
    * @param        NA
    * @return       NA
    */ 
	public override void masterDataVerification(){
		EP_GeneralUtility.Log('public','EP_Third_Party_Order','masterDataVerification');
    	for(EP_RO_Import_Staging__c stagingRec : stagingRecords) {
    		isError = false;
    		resetFields(stagingRec);
	    	verifyDeliveryDocket(stagingRec);//Global Validation
	    	duplicateDataCheck(stagingRec);//With in a File Records
	    	verifySellTo(stagingRec);
			verifyShipTo(stagingRec);
			verifySellToShipToRelationship(stagingRec);
			verifyProductCode(stagingRec);
	    	verifySupplyLocation(stagingRec);
    		verifyTransporter(stagingRec);
			verifySupplier(stagingRec);
			verifyContract(stagingRec);
    		orderDataVerification(stagingRec);
    	}
    	//Consolidation checkes
    	verifyNumberofOrderLines();
		verifyOrderLinesSupplyLocation();
    }
    
    /**
    * @author       Accenture
    * @name         orderDataVerification
    * @date         04/18/2018
    * @description  This method is used for order releated data validation on staging records
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    @testVisible
    private void orderDataVerification(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('private','EP_Third_Party_Order','orderDataVerification');
    	if(stagingRec.EP_Order_Number__c != null) {
	    	verifyOrderNumber(stagingRec);
	    	verifyOrderSellTo(stagingRec);
			verifyOrderShipTo(stagingRec);
			verifyOrderProduct(stagingRec);
			createStagingOrderMap(stagingRec);
    	}
    }
    
    /**
    * @author       Accenture
    * @name         verifyOrderNumber
    * @date         04/18/2018
    * @description  This method is used to validate the Order Number
    * @param        NA
    * @return       NA
    */ 
    @testVisible
    private void verifyOrderNumber(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('private','EP_Third_Party_Order','verifyOrderNumber');
    	if(isError) return;
    	
    	if(!OrderNumberMap.containsKey(stagingRec.EP_Order_Number__c)) {
			addError(stagingRec, getErrorMsg(new list<string>{stagingRec.EP_Order_Number__c}, system.label.EP_Order_Number_Error_Msg));
    	}
    }
    
    /**
    * @author       Accenture
    * @name         verifyOrderSellTo
    * @date         04/18/2018
    * @description  This method is used to validate sellTo on existing Order
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    @testVisible
    private void verifyOrderSellTo(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('private','EP_Third_Party_Order','verifyOrderSellTo');
    	if(isError) return;
    	
    	if(OrderNumberMap.containsKey(stagingRec.EP_Order_Number__c)) {
    		if(OrderNumberMap.get(stagingRec.EP_Order_Number__c).csord__Account__c != stagingRec.EP_Sell_To__c) {
    			addError(stagingRec, getErrorMsg(new list<string>{stagingRec.EP_Order_Number__c}, system.label.EP_SellTo_for_Order_Error_Msg));
    		}
    	}
    }
    
    /**
    * @author       Accenture
    * @name         verifyOrderShipTo
    * @date         04/18/2018
    * @description  This method is used to validate ShipTo on existing Order
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    @testVisible
    private void verifyOrderShipTo(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('private','EP_Third_Party_Order','verifyOrderShipTo');
    	if(isError) return;
    	
    	if(OrderNumberMap.containsKey(stagingRec.EP_Order_Number__c)) {
    		if(OrderNumberMap.get(stagingRec.EP_Order_Number__c).EP_ShipTo__c != stagingRec.EP_Ship_To__c) {
    			addError(stagingRec, getErrorMsg(new list<string>{stagingRec.EP_Order_Number__c}, system.label.EP_ShipTo_for_Order_Error_Msg));
    		}
    	}
    }
    
    /**
    * @author       Accenture
    * @name         verifyOrderProduct
    * @date         04/18/2018
    * @description  This method is used to validate Product Code for againest each order Line
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    @testVisible
    private void verifyOrderProduct(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('private','EP_Third_Party_Order','verifyOrderProduct');
    	if(isError) return;
    	
    	boolean productFound = false;
    	if(OrderNumberMap.containsKey(stagingRec.EP_Order_Number__c)) {
    		for(csord__Order_Line_Item__c orderItem : OrderNumberMap.get(stagingRec.EP_Order_Number__c).csord__Order_Line_Items__r) {
    			if(stagingRec.EP_Product_Code__c.equalsIgnorecase(orderItem.EP_Product_Code__c)) {
    				productFound = true;
    				break;
    			}
    		}
    		if(!productFound){
    			addError(stagingRec, getErrorMsg(new list<string>{stagingRec.EP_Product_Code__c}, system.label.EP_Order_Product_Code_Error_Msg));
    		}
    	}
    }
    
    /**
    * @author       Accenture
    * @name         verifyTransporter
    * @date         04/18/2018
    * @description  This method is used for validate the Transport number
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    @testVisible
    private void verifyTransporter(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('private','EP_Third_Party_Order','verifyTransporter');
    	if(isError) return;
    	
    	stagingRec.EP_Transporter__c = null;
    	if(transporterMap.containsKey(stagingRec.EP_Transporter_Number__c)) {
    		stagingRec.EP_Transporter__c = transporterMap.get(stagingRec.EP_Transporter_Number__c).id;
    	} else {
    		addError(stagingRec, getErrorMsg(new list<string>{stagingRec.EP_Transporter_Number__c}, system.label.EP_Transporter_Error_Msg));
    	}
    }
    
    /**
    * @author       Accenture
    * @name         verifySupplier
    * @date         04/18/2018
    * @description  This method is used for validate the Supplier Number
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    @testVisible
    private void verifySupplier(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('private','EP_Third_Party_Order','verifySupplier');
    	if(isError) return;
    	
    	stagingRec.EP_Supplier__c = null;
    	if(supplierMap.containsKey(stagingRec.EP_Supplier_Number__c)) {
    		stagingRec.EP_Supplier__c = supplierMap.get(stagingRec.EP_Supplier_Number__c).id;
    	} else {
			addError(stagingRec, getErrorMsg(new list<string>{stagingRec.EP_Supplier_Number__c}, system.label.EP_Supplier_Error_Msg));
    	}
    }
   	
   	/**
    * @author       Accenture
    * @name         verifyContract
    * @date         04/18/2018
    * @description  This method is used to validate the Contract Number
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    @testVisible
    private void verifyContract(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('private','EP_Third_Party_Order','verifyContract');
    	if(isError) return;
    	
    	stagingRec.EP_Contract__c = null;
    	if(ContractNumberMap.containsKey(stagingRec.EP_Contract_Number__c)) {
    		stagingRec.EP_Contract__c = ContractNumberMap.get(stagingRec.EP_Contract_Number__c).id;
    	} else {
			addError(stagingRec, getErrorMsg(new list<string>{stagingRec.EP_Contract_Number__c}, system.label.EP_Contract_Error_Msg));
    	}
    }
    
    /**
    * @author       Accenture
    * @name         verifyNumberofOrderLines
    * @date         04/18/2018
    * @description  This method is validate Number of Order Lines in staging records with Number of Order Line in existing Order
    * @param        NA
    * @return       NA
    */ 
    @testVisible
    private void verifyNumberofOrderLines() {
    	EP_GeneralUtility.Log('private','EP_Third_Party_Order','verifyNumberofOrderLines');
    	if(isError) return;
    	
    	list<csord__Order_Line_Item__c> orderLineItems  = new list<csord__Order_Line_Item__c>();
    	for(String orderNumber : orderStagingRecordsMap.keySet()){
    		if(OrderNumberMap.containsKey(orderNumber)) {
    			orderLineItems = OrderNumberMap.get(orderNumber).csord__Order_Line_Items__r;
    			if(orderLineItems.size() != orderStagingRecordsMap.get(orderNumber).size()){
    				for(EP_RO_Import_Staging__c stagingRec : orderStagingRecordsMap.get(orderNumber)){
    					addError(stagingRec, getErrorMsg(new list<string>{string.valueof(orderLineItems.size()), string.valueof(orderStagingRecordsMap.get(orderNumber).size())}, system.label.EP_Number_of_lines_Error_Msg));
    				}
    			}
	    	}
    	}
    }
    
    /**
    * @author       Accenture
    * @name         verifyOrderLinesSupplyLocation
    * @date         04/18/2018
    * @description  This method is used for validate all Supply Location for all order Line , All Order lines should have same supply location
    * @param        NA
    * @return       NA
    */ 
    @testVisible
    private void verifyOrderLinesSupplyLocation() {
    	EP_GeneralUtility.Log('private','EP_Third_Party_Order','verifyOrderLinesSupplyLocation');
    	if(isError) return;
    	set<string> supplyLocationSet = new set<string>();
    	string supplyLoc;
    	for(String orderNumber : orderStagingRecordsMap.keySet()){
    		if(orderStagingRecordsMap.containsKey(orderNumber)) {
    			supplyLoc =  orderStagingRecordsMap.get(orderNumber).get(0).EP_Supply_Location_Code__c;
    			for(EP_RO_Import_Staging__c stagingRec : orderStagingRecordsMap.get(orderNumber)){
    				if(supplyLoc != stagingRec.EP_Supply_Location_Code__c) {
						addError(stagingRec, system.label.EP_Order_SupplyLocation_Error_Msg);
    				}
				}
	    	}
    	}
    }
    
    /**
    * @author       Accenture
    * @name         createStagingOrderMap
    * @date         04/18/2018
    * @description  This method is used to create Map on Order Number with List of Staging Records
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    @testVisible
    private void createStagingOrderMap(EP_RO_Import_Staging__c stagingRec) {
    	EP_GeneralUtility.Log('private','EP_Third_Party_Order','createStagingOrderMap');
		if(orderStagingRecordsMap.containsKey(stagingRec.EP_Order_Number__c)){
			 orderStagingRecordsMap.get(stagingRec.EP_Order_Number__c).add(stagingRec);
		} else {
			 orderStagingRecordsMap.put(stagingRec.EP_Order_Number__c, new List<EP_RO_Import_Staging__c> {stagingRec});
		}
    }
}