/* 
@Author      CloudSense
@name        CreateBasketAndOfferFromOrderController
@CreateDate  03/05/2018
@Description Class for creating the basket in the background and create MLE URL for the iframe from Order
@Version     1.0
*/
public with sharing class CreateBasketAndOfferFromOrderController {
	// fetch Custom setting
    public CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();

    public String configurationId;
    public String basketId;
    public csord__Order__c order;
    public Account account;
    public cscfga__Product_Configuration__c parentConfiguration;
    public cscfga__Product_Basket__c basket;

    // Populate offerId from custom setting
    public String offerId = csOrderSetting.OfferTemplateId__c;

	public CreateBasketAndOfferFromOrderController(ApexPages.StandardController controller) {
		EP_GeneralUtility.Log('Public','CreateBasketAndOfferFromOrderController','CreateBasketAndOfferFromOrderController');
        
        order = [SELECT Id, Name,csord__Account__c FROM csord__Order__c WHERE Id = :controller.getRecord().Id];

        if (order.csord__Account__c != null){
			account = [SELECT Id, Name FROM Account WHERE Id = :order.csord__Account__c];
        }

	}

	/**
    * @author       CloudSense
    * @name         CreateBasketAndOfferController
    * @date         03/05/2018
    * @description  Create the basket, add the offer and launch the product configuration screen
    * @param        NA
    * @return       NA
    */  
    public PageReference createBasketForOpenDrawdownOrder()
    {
        EP_GeneralUtility.Log('Public','CreateBasketAndOfferFromOrderController','createBasketForOpenDrawdropOrder');
        // Create the basket add the offer and update the basket name
    
        
        basketId = ProductConfigurationCloner.createBasketAndCloneOfferToBasket(account.Id, offerId);
        basket = [SELECT Id, Name, csbb__Account__c FROM cscfga__product_basket__c WHERE Id = :basketId];
        basket.Name = 'Order for ' + account.Name;
        UPDATE basket;

        // Set attributes that connect the child product with the parent product
        connectConfigurations();
        System.debug('CreateBasketAndOfferFromOrderController - createBasketForOpenDrawdropOrder - basketId is ' + basketId);

        PageReference configPage = Page.customConfiguration;
        Map<String, String> configPageParams = configPage.getParameters();
        configPageParams.put('configId', parentConfiguration.id);
        configPageParams.put('linkedId', basketId);
        configPageParams.put('parentOrderId', order.Id);
        configPage.setRedirect(true);

        return configPage;
    }

    /**
    * @author       CloudSense
    * @name         CreateBasketAndOfferController
    * @date         23/07/2017
    * @description  Set attributes that connect the child product with the parent product
    * @param        NA
    * @return       NA
    */  
        //
    public void connectConfigurations()
    {
        EP_GeneralUtility.Log('Public','CreateBasketAndOfferController','createBasket');
      
        // Get the parent and child product configurations
        parentConfiguration = [select id, name, cscfga__Product_Basket__c
                                from cscfga__product_configuration__c
                                where name = : csOrderSetting.Parent_Order_Name__c and cscfga__Product_Basket__c = :basketId];
        system.debug('connectConfigurations mategr c2 parentConfiguration ' + parentConfiguration);
    }
}