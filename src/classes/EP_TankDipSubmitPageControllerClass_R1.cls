/* 
  @Author <Spiros Markantonatos>
   @name <EP_TankDipSubmitPageControllerClass>
   @CreateDate <06/11/2014>
   @Description <This class is used by the tank dip page that allows users to verify tank stock>
   @Version <1.0>

*/
public with sharing class EP_TankDipSubmitPageControllerClass_R1 {
    
    private static final string STEP_INDEX_FIVE = '5';
    private static final string STEP_INDEX_THREE = '3';
    private static final string DATE_STRING = 'date';
    private static final string RET_STRING = 'ret';
    private static final string GREEN_STRING = 'Green';
    private static final string DATE_FORMAT = 'dd MMM yyyy';
    private static final string BLANK_SPACE = '  ';
    private static final string ACCOUNT_STRING = EP_Common_Constant.ACCOUNT_OBJ;
    private static final string PLACEHOLDER_STRING = 'Placeholder';
    private static final string TANK_DIP_SUBMIT_PAGE_CTRL = 'EP_TankDipSubmitPageControllerClass_R1 ';
    private static final string BACK = 'back';
    private static final string TOGGLE_SITE_INPUT_CTRLS = 'toggleSiteInputControls';
    private static final string UPDATE_TANK_DIP_READ_TIME = 'updateTankDipReadingTime';
    private static final string CANCEL = 'cancel';
    
    // Properties
    public String shipToName {get;set;}
    public List<EP_Tank__c> tanks {get;set;}
    public Boolean blnShowEdit {get;set;}
    public Boolean blnShowSave {get;set;}
    public Boolean blnShowBack {get;set;}
    public Boolean blnShowStatusColumn {get;set;} // Variable used to show the tank status icon when the user is entering tank dips for a specific date
    public Boolean blnShowTankEndNext {get;set;} // Variable used to show the next button when a tank dip has been submitted and the user has access to multiple sites
    public Boolean blnShowTankStopBack {get;set;} // Variable used to show the Back button when there are no operational tanks for the selected site
    public Boolean blnShowTankTable {get;set;} // Variable used to hide the tanks table if there are no operational tanks
    public Boolean blnShowTankToggle {get;set;} // Variable used to hide the "Select All" and "Deselect All" buttons
    public Boolean blnShowCancel {get;set;}
    public Boolean blnShowFinish {get;set;} // Variable used to show the "Finish" button in the confirmation panel once tank dips are saved
    public Boolean blnShowEntryBack {get;set;} // Variable used to show the "Back" button when in step 1
    public Boolean blnShowNext {get;set;} // Variable used to hide the next button if no tank is selected
    public String strStepIndex {get;set;} // Variable used to determine the step of the process that the user is currently in
    public String strSelectedShipToID {get;set;}
    public String strSelectedTanksIDs {get;set;}
    public Date dSelectedDate {get;set;} // Variable used to select a specific tank dip date
    public String strFilterDate {get;set;} // Variable used to navigate back to tank dip date selection page
    public Boolean blnEnterTodaysDips {get;set;} // Variable used to flag when the user is actually entering today's dips
    public DateTime dtQueryDateTime {get;set;}
    public Date dQueryDate {get;set;}
    public String strSelectedDate {get;set;} // Variable used to capture the date from the URL
    public String strFormattedSelectedDate {get;set;} // Variable used to display a date title, when user is entering dips for a specific date
    public DateTime dtSelectedDateTime {get;set;}
    public List<tankDipRecordClass> newTankDipRecords {get;set;}
    private List<Account> shipTos {get;set;}
    
    public Double dblUserOffset {get;set;} // Variable used to set the offset of the user
    public Double dblSiteOffset {get;set;} // Variable used to set the offset of the site
    public Double dblOffset {get;set;} // Variable used to return the offset variance between the site and the running user
    
    // Page number navigation variable
    public Integer intPageNumber {
        get {
            if (intPageNumber == NULL)
            {
                if (ApexPages.currentPage().getParameters().get(EP_Common_Constant.PAGE_NUMBER) != NULL)
                {
                    if (ApexPages.currentPage().getParameters().get(EP_Common_Constant.PAGE_NUMBER).isNumeric())
                        intPageNumber = Integer.valueOf(ApexPages.currentPage().getParameters().get(EP_Common_Constant.PAGE_NUMBER));
                }
            }
            return intPageNumber;
        }
        set;
    }
    // Time filter variables
    public Integer intUserSelectedHour {get;set;}
    public Integer intUserSelectedMinute {get;set;}
    
    public Boolean blnShipToHasMissingOlderTankDips {
        get {
            String recType= Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(PLACEHOLDER_STRING).getRecordTypeId();
            List<EP_Tank_Dip__c> missingOlderTankDips = [SELECT ID FROM EP_Tank_Dip__c 
                                                            WHERE EP_Tank__r.EP_Ship_To__c = :strSelectedShipToID AND
                                                            EP_Tank_Dip_Entered_In_Last_14_Days__c = TRUE AND
                                                            EP_Tank_Dip_Entered_Today__c = FALSE AND
                                                            RecordTypeId =: recType
                                                            LIMIT 1];
            return (!missingOlderTankDips.isEmpty());
        }
    }
    
    public Boolean blnShipToIsMissingTankDipsForToday {
        get {
            String recType= Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(PLACEHOLDER_STRING).getRecordTypeId();
            List<EP_Tank_Dip__c> missingTodaysTankDips = [SELECT ID FROM EP_Tank_Dip__c 
                                                            WHERE EP_Tank__r.EP_Ship_To__c = :strSelectedShipToID AND
                                                            EP_Tank_Dip_Entered_Today__c = TRUE AND
                                                            RecordTypeId =: recType
                                                            LIMIT 1];
            return (!missingTodaysTankDips.isEmpty());
        }
    }
    
    // This property returns if the user has navigated to the page from the Account page
    public Boolean blnRedirectToAccountPage {
        get {
            Boolean blnResult = FALSE;
            if (ApexPages.currentPage().getParameters().get(RET_STRING) != NULL) {
                blnResult = (ApexPages.currentPage().getParameters().get(RET_STRING) == ACCOUNT_STRING);
            }
            return blnResult; 
        }
    }
    
    // This property returns if there are other missing tankd dips for the sites of the user
    public Boolean missingTankDips {
        get {
            if (strSelectedShipToID != NULL) {
                List<EP_Tank__c> tanks = [SELECT ID FROM EP_Tank__c WHERE EP_Ship_To__c = :strSelectedShipToID 
                                                            AND EP_Missing_Tank_Dip_Nr__c > 0 LIMIT 5];
                
                missingTankDips = (!tanks.isEmpty());
            } // End selected Ship-To ID check            
            return missingTankDips;
        }
        set;
    }
    
    // This property returns if there any other active sites available to the running user
    public Boolean multipleShipTosAvailable {
        get {
            List<Account> accounts = [SELECT ID FROM Account 
                    WHERE RecordType.DeveloperName = :EP_Common_Constant.VMI_SHIP_TO_RECORD_TYPE_DEVELOPER_NAME LIMIT 10];
            
            multipleShipTosAvailable = (accounts.size() > 1);
            
            return multipleShipTosAvailable;
        }
        set;
    }
    
    public List<tankDipRecordClass> displayedTankDipLines {
        get {
            displayedTankDipLines = new List<tankDipRecordClass>();
            
            for (tankDipRecordClass tdr : newTankDipRecords) {
                if (blnShowEdit || tdr.blnAddTankDip) {
                    displayedTankDipLines.add(tdr);
                }
            } // End for
            
            return displayedTankDipLines;
        }
        set;
    }
    
    /* 
       @Description: Constructor
    */
    public EP_TankDipSubmitPageControllerClass_R1() {
        
        // Fix for Defect 29809 - Start
        
        initialiseTankDipData();
        
        adjustButtonVisibility();
        
        generateTankDipsForSite();
            
        // Fix for Defect 29809 - End
            
    }
    
    // Functions
    
    /* 
       @Description: This method is used to initilise the tank dip date parameters
    */    
    public void initialiseTankDipData() {
        Integer queryRows ;
        tanks = new List<EP_Tank__c>();
        newTankDipRecords = new List<tankDipRecordClass>();
        
        // Initialise values
        strSelectedShipToID = NULL;
        strStepIndex = STEP_INDEX_THREE;
        
        // Check if the page is specifying depot ID
        if (ApexPages.currentPage().getParameters().get(EP_Common_Constant.ID) != NULL) {
            strSelectedShipToID = ApexPages.currentPage().getParameters().get(EP_Common_Constant.ID);
            
            // Retrieve selected date from URL
            if (ApexPages.currentPage().getParameters().get(DATE_STRING) != NULL) {
                strSelectedDate = ApexPages.currentPage().getParameters().get(DATE_STRING);
                
                // Convert string to date
                if (strSelectedDate.length() == 14) {
                    try {
                        strFilterDate = strSelectedDate;
                        Integer intYear = Integer.valueOf(strSelectedDate.substring(0, 4));
                        Integer intMonth = Integer.valueOf(strSelectedDate.substring(4, 6));
                        Integer intDay = Integer.valueOf(strSelectedDate.substring(6, 8));
                        Integer intHours = Integer.valueOf(strSelectedDate.substring(8, 10));
                        Integer intMinutes = Integer.valueOf(strSelectedDate.substring(10, 12));
                        Integer intSeconds = Integer.valueOf(strSelectedDate.substring(12, 14));
                        
                        dtSelectedDateTime = EP_PortalLibClass_R1.returnLocationDateTimeFromInteger(intYear,
                                                     intMonth, intDay, intHours, intMinutes, intSeconds);
                    } catch (exception e) {
                        dSelectedDate = NULL;
                    }
                }
            } // End date check
            
            Date dNow = EP_PortalLibClass_R1.returnLocalDate(System.Now());
            queryRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
            // Get tank name and current site time from accounts to cover the case where there are no operational tanks
            // Retrieve existing customer ship-tos
            shipTos = [SELECT ID, Name, EP_Ship_To_Current_Date_Time__c FROM Account WHERE ID = :strSelectedShipToID AND
                                     RecordType.DeveloperName = :EP_Common_Constant.VMI_SHIP_TO_RECORD_TYPE_DEVELOPER_NAME Limit :queryRows];
            
            if (!shipTos.isEmpty()) {
                shipToName = shipTos[0].Name;
                dNow = EP_PortalLibClass_R1.returnLocalDate(shipTos[0].EP_Ship_To_Current_Date_Time__c);
                // Set the filter time picklist to the current ship-to time
                intUserSelectedHour = shipTos[0].EP_Ship_To_Current_Date_Time__c.Hour();
                intUserSelectedMinute = shipTos[0].EP_Ship_To_Current_Date_Time__c.Minute();
                
            }
            
            if (dtSelectedDateTime == NULL) {
                dtSelectedDateTime = dNow;
            }
            
            dtQueryDateTime = EP_PortalLibClass_R1.returnLocalDateTime(dtSelectedDateTime);
            
            // Convert local date/time to site date/time
            if (dblSiteOffset == NULL) dblSiteOffset = EP_PortalLibClass_R1.retrieveSiteOffset(strSelectedShipToID);
            // Calculate offset from local site time to user time
            if (dblUserOffset == NULL) dblUserOffset = EP_PortalLibClass_R1.calculateUTCOffsetOfRunningUser();
            dblOffset = dblSiteOffset - dblUserOffset;
            
            if (dblSiteOffset <= 0 && dblUserOffset <= 0) {
                if (dblSiteOffset > dblUserOffset){
                     dblOffset = -dblUserOffset + dblSiteOffset;
                }
                if (dblSiteOffset < dblUserOffset){
                     dblOffset = -dblSiteOffset + dblUserOffset;
                }
            }
            
            if (dblSiteOffset > 0 && dblUserOffset > 0) {
                if (dblSiteOffset > dblUserOffset){
                     dblOffset = dblSiteOffset - dblUserOffset;
                }
                if (dblSiteOffset < dblUserOffset) {
                dblOffset = dblUserOffset - dblSiteOffset;
                }
            }
            
            if (dblSiteOffset > 0 && dblUserOffset < 0) {
                dblOffset = -dblSiteOffset + dblUserOffset;
            }
            
            if (dblSiteOffset < 0 && dblUserOffset > 0) {
                dblOffset = dblSiteOffset - dblUserOffset;
            }
            // Calculate the display date/time
            DateTime dtDisplayDateTime = dtQueryDateTime;
            dSelectedDate = EP_PortalLibClass_R1.returnLocalDate(dtSelectedDateTime);
            strFormattedSelectedDate = BLANK_SPACE + dtDisplayDateTime.format(DATE_FORMAT);
            
            // Convert the query date to UTC
            dQueryDate = EP_PortalLibClass_R1.returnLocalDate(dtQueryDateTime);
            
            // Determine if the user is trying to enter tank dips for today
            blnEnterTodaysDips = (dNow == dSelectedDate || ApexPages.currentPage().getParameters().get(DATE_STRING) == NULL);
            
        } // End site ID check
    }
    
    /* 
       @Description: This method is used for adjusting button visibility
    */    
    private void adjustButtonVisibility() {
        blnShowFinish = ((strStepIndex == STEP_INDEX_FIVE) && !(blnShipToHasMissingOlderTankDips || (!blnEnterTodaysDips && blnShipToIsMissingTankDipsForToday)) && (!multipleShipTosAvailable));
        blnShowCancel = FALSE; // Hide the "Cancel" button  //((strStepIndex != '5') && (!tanks.isEmpty()));
        blnShowEntryBack = ((multipleShipTosAvailable || missingTankDips) && (!newTankDipRecords.isEmpty()) && (strStepIndex == STEP_INDEX_THREE) && ((dSelectedDate != NULL) || (strSelectedShipToID != NULL)));
        blnShowEdit = ((strStepIndex == EP_Common_Constant.STEP_THREE) && (!tanks.isEmpty()));
        blnShowTankToggle = ((strStepIndex == EP_Common_Constant.STEP_THREE) && (!tanks.isEmpty()));
        blnShowTankStopBack = ((multipleShipTosAvailable) && (strStepIndex == EP_Common_Constant.STEP_THREE) && (strSelectedShipToID != NULL));
        blnShowSave = ((strStepIndex == EP_Common_Constant.STEP_FOUR) && (!tanks.isEmpty()));
        blnShowBack = (strStepIndex == EP_Common_Constant.STEP_FOUR);
        blnShowNext = ((strSelectedShipToID != NULL) && (strStepIndex == STEP_INDEX_THREE) && (!tanks.isEmpty()));
        blnShowTankTable = ((strStepIndex != STEP_INDEX_FIVE) && (!tanks.isEmpty()));
        blnShowTankEndNext = ((strStepIndex == STEP_INDEX_FIVE) && (!blnShowFinish) && ((dSelectedDate != NULL) || (strSelectedShipToID != NULL)));    
        blnShowStatusColumn = (dSelectedDate != NULL && strStepIndex == STEP_INDEX_THREE);
        
    }
    
    /* 
       @Description: This method is used for generation of tank dips for site
    */    
    private void generateTankDipsForSite() {
        String strShipToID;
        Integer queryRows;
        newTankDipRecords = new List<tankDipRecordClass>();
        
        if (strSelectedShipToID != NULL)
        {
            queryRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
            tanks = [SELECT Id, Name, EP_Product__r.Name, EP_Ship_To__r.Name, EP_Tank_Missing_Dip_For_Today__c,
                            EP_Unit_Of_Measure__c,EP_Deadstock__c, EP_Safe_Fill_Level__c, EP_Tank_Status__c, 
                            EP_Product__r.EP_Fuel_Conversion_Ratio__c, EP_Tank_Code__c, EP_Ship_To__r.EP_Ship_To_UTC_Timezone__c,EP_UnitOfMeasure__c
                                    FROM EP_Tank__c 
                                    WHERE EP_Ship_To__c = :strSelectedShipToID
                                    AND EP_Tank_Status__c != :EP_Common_Constant.TANK_DECOMISSIONED_STATUS LIMIT :queryRows];  // Replace OPERATIONAL to Not equals to DECOMISSIONED
            // Get the depot name
            if (!tanks.isEmpty()) { 
                shipToName = tanks[0].EP_Ship_To__r.Name;
                strShipToID = tanks[0].EP_Ship_To__c; 
            }
            
            // Get missing tank dips if a specific date has been selected
            Map<String, String> missingTankDipMap = new Map<String, String>();
           
            if (!blnEnterTodaysDips) {
                
                if (dblUserOffset == NULL) dblUserOffset = EP_PortalLibClass_R1.retrieveUserOffset(UserInfo.getUserID());
                
                DateTime dtQueryMin = EP_PortalLibClass_R1.returnLocalDateTimeFromFull(dQueryDate.Year(), 
                                            dQueryDate.Month(), dQueryDate.Day(), 0, 0, 0); // YYYY-MM-DD 00:00
                DateTime dtQueryMax = EP_PortalLibClass_R1.returnLocalDateTimeFromFull(dQueryDate.Year(), 
                                            dQueryDate.Month(), dQueryDate.Day(), 23, 59, 59); // YYYY-MM-DD 23:59
                Double dblOffset = EP_PortalLibClass_R1.returnSiteUserUTCOffset(strShipToID, dblUserOffset);
                
                // Fix for Defect 30169 - Start
                
                //if (dblOffset != NULL){ 
                    //dtQueryMin = EP_PortalLibClass_R1.addOffsetToDateTime(dtQueryMin, dblOffset);
                //}
                //if (dblOffset != NULL){
                    //dtQueryMax = EP_PortalLibClass_R1.addOffsetToDateTime(dtQueryMax, dblOffset);
                //}
                
                String recType = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(PLACEHOLDER_STRING).getRecordTypeId();
                
                System.debug('MIN: ' + dtQueryMin);
                System.debug('Max: ' + dtQueryMax);
                queryRows = EP_Common_Util.getQueryLimit();
                for (EP_Tank_Dip__c td : [SELECT EP_Tank__c FROM EP_Tank_Dip__c WHERE 
                                                            EP_Tank__r.EP_Ship_To__c = :strSelectedShipToID AND
                                                            EP_Tank_Dip_Date_Time_Ship_To_Time_Zone__c >= :dtQueryMin AND
                                                            EP_Tank_Dip_Date_Time_Ship_To_Time_Zone__c <= :dtQueryMax AND
                                                                    RecordTypeId =: recType LIMIT :queryRows]) {
                    missingTankDipMap.put(td.EP_Tank__c, td.EP_Tank__c);
                } // End for
                // Fix for Defect 30169 - End
                
            } // End selected date check
            
            tankDipRecordClass rc;
            Integer intCounter = 0;
            
            // Create tank dip records
           for (EP_Tank__c t : tanks) { 
                rc = new tankDipRecordClass();
                rc.shipToID = strSelectedShipToID;
                rc.tank = t;
                rc.strTankName = t.Name;
                rc.strTankCode = t.EP_Tank_Code__c;
                rc.strProductName = t.EP_Product__r.Name;
                rc.dblProductConversationRatio = t.EP_Product__r.EP_Fuel_Conversion_Ratio__c;
                rc.strTankID = String.valueOf(intCounter);
                
                // Reset status flag values
                rc.dipShowOKStatus = FALSE;
                rc.dipShowWarningStatus = FALSE;
                rc.dipShowCautionStatus = FALSE;
                
                if (!blnEnterTodaysDips) {
                    rc.dipShowCautionStatus = (missingTankDipMap.containsKey(t.Id));
                } else {
                    rc.dipShowWarningStatus = (t.EP_Tank_Missing_Dip_For_Today__c); 
                }
                
                rc.dipShowOKStatus = (!rc.dipShowCautionStatus && !rc.dipShowWarningStatus);
                
                // If there is a tank dip for the day, then do not select by default
                rc.blnAddTankDip = ((dSelectedDate == NULL) || (!rc.dipShowOKStatus));
                if(rc.tank.EP_Tank_Status__c == EP_Common_Constant.TANK_STOPPED_STATUS) {
                   rc.blnAddTankDip = False;
                }
                
                intCounter++;
                
                // Create tank dip record
                rc.tankDip = new EP_Tank_Dip__c();
                rc.tankDip.EP_Tank__c = t.Id;
                rc.tankDip.EP_Unit_Of_Measure__c = t.EP_Unit_Of_Measure__c;
                rc.tankDip.EP_Tank_Capacity_At_Tank_Dip_Entry__c = rc.tank.EP_Safe_Fill_Level__c;
                
                if (dtSelectedDateTime != NULL) {
                    rc.tankDip.EP_Reading_Date_Time__c = dtSelectedDateTime;
                }
                
                rc.eDateEvent = new Event(ActivityDate = dSelectedDate);
                
                rc.strColour = GREEN_STRING;
                newTankDipRecords.add(rc);
            } // End for
            
            // Move to step 3 of the process
            strStepIndex = STEP_INDEX_THREE;
            adjustButtonVisibility();
        }
    }
    
    /* 
       @Description: This method is used for page navigation when back is selected
    */    
    public PageReference back() {
        PageReference ref = NULL;
        String strURL;
        try{
        if (strStepIndex == STEP_INDEX_THREE) {
            // Fix for Defect 29809 - Start
            
            strURL = EP_Common_Constant.TANKDIP_SITE_PAGE + strSelectedShipToID;
            
            if (strFilterDate != NULL) {
                strURL = EP_Common_Constant.TANKDIP_DATE_PAGE + strSelectedShipToID + EP_Common_Constant.DATE_PARAM  + strFilterDate;
            }
            
            if (intPageNumber != NULL) {
                strURL += EP_Common_Constant.PAGE_NUM_PARAM + intPageNumber;
            }
            
            // Fix for Defect 29809 - End
            
            // If the user navigated to this page from the multi-site-select page
            // We need to navigate the user back, if there are more sites with missing tank dips
            // Fix for Defect 30169 - Start
            if (blnRedirectToAccountPage && strSelectedShipToID != NULL && strFilterDate == NULL) {
                strURL = EP_Common_Constant.SLASH + strSelectedShipToID;
            }
            // Fix for Defect 30169 - End
            
            ref = new PageReference(strURL);
            
            return ref;
        }
        
        strStepIndex = STEP_INDEX_THREE;
        
        adjustButtonVisibility();
        }
        catch(Exception handledException){
         EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, 
                BACK, TANK_DIP_SUBMIT_PAGE_CTRL, ApexPages.Severity.ERROR);
        }
        return ref;
    }
    
    /*
       @Description: This method returns number of tank dips entered
    */    
    private Boolean checkNumberOfTankDipsEntered() {
        Boolean blnAtLeastOneTankDipIsEntered = FALSE;
        
        for (tankDipRecordClass rc : newTankDipRecords) {
            if (rc.blnAddTankDip) {        
                blnAtLeastOneTankDipIsEntered = TRUE;
                break;
            } // End selected tank dip check
        } // End for
        return blnAtLeastOneTankDipIsEntered;
    }
    
    /*
    @Description: Prepare records for Insert
    */
    private List<EP_Tank_Dip__c> prepareRecordsForInsert() {
        List<EP_Tank_Dip__c> tankDipRecords = new List<EP_Tank_Dip__c>();
        EP_Tank_Dip__c newTankDip;
        Time tSelectedTime;
        
        System.debug('SELECT VALUE ' + strSelectedTanksIDs);
        
        for (tankDipRecordClass rc : newTankDipRecords) {
            rc.blnAddTankDip = FALSE;
            
            if (strSelectedTanksIDs != NULL) {
                System.debug('SELECT TANK ID ' + rc.strTankID);
                
                if (strSelectedTanksIDs.indexOf(EP_Common_Constant.SEMICOLON + rc.strTankID + EP_Common_Constant.SEMICOLON) > -1) {
                    newTankDip = rc.tankDip.clone();
                    newTankDip.Id = NULL;
                    newTankDip.EP_Reading_Date_Time__c = NULL;
                    newTankDip.EP_Tank_Status__c = rc.tank.EP_Tank_Status__c;
                    
                    if (rc.intHour != NULL && rc.intMinute != NULL)  {
                        tSelectedTime = Time.newInstance(rc.intHour, rc.intMinute, 0, 0);
                        newTankDip.EP_Reading_Date_Time__c = EP_PortalLibClass_R1.combineDateAndTime(dSelectedDate, tSelectedTime);
                        rc.tankDip.EP_Reading_Date_Time__c = newTankDip.EP_Reading_Date_Time__c;
                    }
                    
                    tankDipRecords.add(newTankDip);
                    rc.blnAddTankDip = TRUE;
                    
                }
            } // End selected tank ID string check
        } // End for
        //System.debug('prepareRecordsForInsert  '+tankDipRecords);
        return tankDipRecords;
    }
    
    /*
       @Description: This method is used for saving stock records
    */   
    public PageReference saveStockRecords() {
        List<EP_Tank_Dip__c> tankDipRecords = prepareRecordsForInsert();
        //System.debug('saveStockRecords'+tankDipRecords );
        if (!tankDipRecords.isEmpty()) {
            //System.debug('saveStockRecords1  '+tankDipRecords);
            Savepoint sp = Database.setSavepoint();
                            
            try {
                Database.insert(tankDipRecords);
                
                // Set page control parameters
                strStepIndex = STEP_INDEX_FIVE;
                adjustButtonVisibility();
            } catch(exception e) {
                // Set page control parameters
                strStepIndex = EP_Common_Constant.STEP_FOUR;
                adjustButtonVisibility();
                Database.rollback(sp);
            }
        }
        
        return NULL;
    }
    
    /*
    @Description:Initialise the show next button to FALSE
    */
    public PageReference toggleSiteInputControls() {
        // Initialise the show next button to FALSE
        try{
        blnShowNext = (strSelectedShipToID != NULL);
        }catch(Exception handledException){
         EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, 
                TOGGLE_SITE_INPUT_CTRLS, TANK_DIP_SUBMIT_PAGE_CTRL, ApexPages.Severity.ERROR);
        }
        return NULL;
    }
    
    /* 
       @Description: This method is used for page navigation when next is selected
    */    
    public PageReference next() {
        PageReference ref = NULL;
        String strURL;
        
        // Validate tank dip
        if (strStepIndex == STEP_INDEX_THREE) {
            // Ensure that at least 1 tank dip has been entered
            if (checkNumberOfTankDipsEntered()) {
                // Attempt to save to fire the validation rules
                Savepoint sp = Database.setSavepoint();
                
                try {
                    List<EP_Tank_Dip__c> tankDipRecords = prepareRecordsForInsert();
                    
                    if(EP_Common_Constant.isErrorOccoured){
                        Database.rollback(sp);
                    }
                       
                    if (!tankDipRecords.isEmpty()) {
                        Database.insert(tankDipRecords);
                    }
                    
                    Database.rollback(sp);
                    
                    strStepIndex = EP_Common_Constant.STEP_FOUR;
                    adjustButtonVisibility();
                    
                    
                } catch(exception e) {
                    // If one of the validation rules fires, then return back to the edit page
                    // Set page control parameters
                    Database.rollback(sp);
                    strStepIndex = STEP_INDEX_THREE;
                    adjustButtonVisibility();
                }
            } // End single tank dip check
        } // End step 3 check
        
        if (strStepIndex == STEP_INDEX_FIVE) {
            ref = new PageReference(EP_Common_Constant.HOME_PAGE_URL);
            
            // If the user has navigated from the Account page, then return the user to the same page
            if (strSelectedShipToID != NULL) {
                ref = new PageReference(EP_Common_Constant.SLASH + strSelectedShipToID);
            }
            
            // If the user navigated to this page from the multi-site-select page
            // We need to navigate the user back, if there are more sites with missing tank dips
            if (!blnRedirectToAccountPage) {
                strURL = EP_Common_Constant.TANKDIP_SITE_PAGE + strSelectedShipToID;
                
                if (intPageNumber != NULL)
                {
                    strURL += EP_Common_Constant.PAGE_NUM_PARAM + intPageNumber;
                }
                ref = new PageReference(strURL);
            }
            
            // If the user came from the the tank dip date selector, then send back
            // If more dates are missing for other than today then send back to the date page, otherwise back to the home page
            
            if (blnShipToHasMissingOlderTankDips || (!blnEnterTodaysDips && blnShipToIsMissingTankDipsForToday)) {
                strURL = EP_Common_Constant.TANKDIP_DATE_PAGE + strSelectedShipToID + EP_Common_Constant.DATE_PARAM + strSelectedDate;
                
                if (intPageNumber != NULL)
                {
                    strURL += EP_Common_Constant.PAGE_NUM_PARAM + intPageNumber;
                }
                
                // Update the URL variable if the user has navigated to this page from the account page
                if (blnRedirectToAccountPage){
                    strURL += EP_Common_Constant.ACCOUNT_PARAM;
                }
                ref = new PageReference(strURL);
            } // End missing tank dips check
            
        } // End step 5 check
        
        return ref;
    }
    
    /*
       @Description: This method is used for page navigation when cancel is selected
    */    
    public PageReference cancel() {
        PageReference ref = new PageReference(EP_Common_Constant.HOME_PAGE_URL);
        try{
            if (strSelectedShipToID != NULL) {
            ref = new PageReference(EP_Common_Constant.SLASH + strSelectedShipToID);
            }
        }
        catch(Exception handledException){
         EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, 
                CANCEL, TANK_DIP_SUBMIT_PAGE_CTRL, ApexPages.Severity.ERROR);
        }
       return ref;
    }
    
    /* 
       @Description: This method is used for updation of Tank dip reading time
    */    
    public void updateTankDipReadingTime() {
        try{
            if (intUserSelectedHour != NULL && intUserSelectedMinute != NULL) {
                if(displayedTankDipLines != NULL && !displayedTankDipLines.isEmpty()) {
                    for(tankDipRecordClass wrap : displayedTankDipLines) {
                        wrap.intHour = intUserSelectedHour;
                        wrap.intMinute = intUserSelectedMinute;
                    } // End for
                } // End wrapper check
            } // End selected time check
        }
        catch(Exception handledException){
         EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, 
                UPDATE_TANK_DIP_READ_TIME, TANK_DIP_SUBMIT_PAGE_CTRL, ApexPages.Severity.ERROR);
        }
    }
    
    /* 
       @Description: Inner class
    */
    public without sharing class tankDipRecordClass {
        public String strTankID {get;set;}
        public EP_Tank__c tank {get;set;}
        public EP_Tank_Dip__c tankDip {get;set;}
        public Boolean blnAddTankDip {get;set;}
        public String shipToID {get;set;}
        public String strTankName {get;set;}
        public String strTankCode {get;set;}
        public String strProductName {get;set;}
        public Double dblProductConversationRatio {get;set;}
        public Boolean dipShowOKStatus {get;set;}
        public Boolean dipShowWarningStatus {get;set;}
        public Boolean dipShowCautionStatus {get;set;}
        public String strColour {get;set;}
        public Time ReadingTime{get;set;}
        public Event eDateEvent {get;set;}
        public Integer intHour {get;set;}
        public Integer intMinute {get;set;}
    }
    
}