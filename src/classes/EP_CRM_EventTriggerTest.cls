/* ================================================
 * @Class Name : EP_CRM_EventTriggerTest
 * @author : Kamendra Singh
 * @Purpose: This class is used to test EP_CRM_EventTrigger apex trigger.
 * @created date: 08/07/2016
 ================================================*/
@isTest
private class EP_CRM_EventTriggerTest{
     /* This method is used to create all test data and test before insert event schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testEventInsertUpdateTrigger(){
        try{
            //Run as Sales User
            Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' LIMIT 1]; 
            User u=new User();
            u.firstName='Test First';
            u.lastname='Sales CRM';
            u.profileid=p.id;
            u.Alias = 'salesman';
            u.Email='standardusercrm@accenture.com'; 
            u.EmailEncodingKey='UTF-8';
            u.LanguageLocaleKey='en_US'; 
            u.LocaleSidKey='en_AU';
            u.TimeZoneSidKey='Australia/Brisbane';
            u.UserName='standardusercrm@accenture.com';
            u.CurrencyIsoCode='AUD';
            u.EP_CRM_Geo1__c='Africa';
            u.EP_CRM_Geo2__c='Ghana';
            
            Database.SaveResult saveUser = Database.insert(u);
            System.runAs(u){
                
             // Insert Account
            Account acc=new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';
            acc.EP_CRM_Geo1__c='Africa';
            acc.EP_CRM_Geo2__c='Ghana';
            
            Database.SaveResult saveAcc = Database.insert(acc);
            
            //Insert Opportunity
            Opportunity opp=new Opportunity();
            opp.name='Test';
            //opp.Stagename='Closed Won';
            opp.Stagename='Prospecting';
            opp.AccountId=acc.Id;
            opp.LeadSource='Campaign';
            //opp.EP_CRM_Credit_Approval_Received__c = true;
            opp.CloseDate=Date.Today() + 5;
            opp.EP_CRM_Primary_Product_Interest__c = 'Aviation';
            opp.EP_CRM_Current_Supplier__c = 'Allied';
            opp.EP_CRM_Industry__c = 'Agriculture';
            //opp.EP_CRM_KYC_Done__c = true;
            opp.EP_CRM_Margin_unit_of_measure__c = 1;
            opp.EP_CRM_Price_Type__c = 'Formula Price';
            //opp.EP_CRM_Pricing_Approval__c = true;
            opp.EP_CRM_Product_Interest_Group__c = 'Aviation';
            opp.EP_CRM_Volume__c = 1;
            opp.EP_CRM_Geo1__c='Africa';
            opp.EP_CRM_Geo2__c='Ghana';
               // opp.EP_CRM_Annual_Volume__c=10.00;
                //opp.EP_CRM_Requested_Payment_Term__c='Prepaid';
                //opp.EP_CRM_Current_Supplier_Agreement__c='No';
                // opp.EP_CRM_Customer_Segment__c='B2B';
            //opp.EP_CRM_Credit_Approval_Requested__c= true;
            //opp.EP_CRM_Contract_Signed__c=true;
            //opp.EP_CRM_Credit_Approval_Requested_Date__c =Date.newInstance(2017,07,02);
            
            
            Database.SaveResult saveOpp = Database.insert(opp);
            
            Event tempEvent = new Event ();
            tempEvent.OwnerId = UserInfo.getUserId();
            tempEvent.Subject='Donni';
            tempEvent.whatId = opp.Id;
            tempEvent.DurationInMinutes = 10;
            tempEvent.ActivityDateTime = system.now();
            tempEvent.Type='Meeting';
            Database.SaveResult saveEvent = Database.insert(tempEvent);
            //Check System assert---------
            Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id limit 1];
            
            //validate through system Assert 
            
            //update Event........
            tempEvent.Subject = 'test subject';
            
            Database.SaveResult updateEvent = Database.update(tempEvent);
             Test.startTest();
             //Check System assert---------
            Opportunity tempOpp1 = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id limit 1];
            
            //validate through system Assert 
                
            test.stopTest();
            System.Assert(tempOpp1.EP_CRM_Last_Activity_Date__c != null);
            System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c != null);

            }
        }
        catch(exception ex){
            ex.getmessage();
        }
    }
    
     /* This method is used to create all test data and test before delete event schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testBulkEventDeleteTrigger(){
        try{
            //Run as Sales User
            Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' LIMIT 1]; 
            User u=new User();
            u.firstName='Test First';
            u.lastname='Sales CRM';
            u.profileid=p.id;
            u.Alias = 'salesman';
            u.Email='standardusercrm@accenture.com'; 
            u.EmailEncodingKey='UTF-8';
            u.LanguageLocaleKey='en_US'; 
            u.LocaleSidKey='en_AU';
            u.TimeZoneSidKey='Australia/Brisbane';
            u.UserName='standardusercrm@accenture.com';
            u.CurrencyIsoCode='AUD';
            u.EP_CRM_Geo1__c='Africa';
           	u.EP_CRM_Geo2__c='Ghana';
            Database.saveResult saveUser = Database.insert(u);
            System.runAs(u){
                
                 // Insert Account
                Account acc=new Account();
                acc.name='Test Accenture';
                acc.BillingCity = 'test City';
                acc.BillingStreet  = 'test street';
                acc.BillingState = 'test state';
                acc.BillingPostalCode = '201301';
                acc.BillingCountry = 'test';
                acc.EP_CRM_Geo1__c='Africa';
           		acc.EP_CRM_Geo2__c='Ghana';
                
                Database.SaveResult saveAcc = Database.insert(acc);
                //Insert Opportunity
                Opportunity opp=new Opportunity();
                opp.name='Test';
                opp.Stagename='Prospecting';
                opp.AccountID=acc.Id;
                opp.LeadSource='Campaign';
                //opp.EP_CRM_Credit_Approval_Received__c = true;
                opp.CloseDate=Date.Today() + 5;
                opp.EP_CRM_Primary_Product_Interest__c = 'Aviation';
                opp.EP_CRM_Current_Supplier__c = 'Allied';
                opp.EP_CRM_Industry__c = 'Agriculture';
                //opp.EP_CRM_KYC_Done__c = true;
                opp.EP_CRM_Margin_unit_of_measure__c = 1;
                opp.EP_CRM_Price_Type__c = 'Formula Price';
                //opp.EP_CRM_Pricing_Approval__c = true;
                opp.EP_CRM_Product_Interest_Group__c = 'Aviation';
                opp.EP_CRM_Volume__c = 1;
                opp.EP_CRM_Geo1__c='Africa';
           		opp.EP_CRM_Geo2__c='Ghana';
                
                Database.SaveResult saveopp = Database.insert(opp);
                
                //insert Event..............
                list<event> lstEvent = new list<Event>();
                Event tempEvent = new Event ();
                tempEvent.OwnerId = UserInfo.getUserId();
                tempEvent.Subject='Donni';
                tempEvent.whatId = opp.Id;
                tempEvent.DurationInMinutes = 10;
                tempEvent.ActivityDateTime = system.now();
                tempEvent.Type='Meeting';
                
                Event tempEvent1 = new Event ();
                tempEvent1.OwnerId = UserInfo.getUserId();
                tempEvent1.Subject='Donni';
                tempEvent1.whatId = opp.Id;
                tempEvent1.DurationInMinutes = 10;
                tempEvent1.ActivityDateTime = system.now();
                tempEvent1.Type='Meeting';
                
                lstEvent.add(tempEvent);
                lstEvent.add(tempEvent1);
                
                Database.SaveResult[] savelstEvent = Database.insert(lstEvent);
                //delete Event........
                
                Database.DeleteResult deletetempEvent = Database.delete(tempEvent);
                
                Test.startTest();
                //Check System assert---------
                Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id limit 1];
                
                //validate through system Assert 
                test.stopTest();
                System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c != null);
            }
        }
        catch(exception ex){
            ex.getmessage();
        }
    }
    
     /* This method is used to create all test data and test before delete event schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testSingleEventDeleteTrigger(){
        try{
            //Run as Sales User
            Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' LIMIT 1]; 
            User u=new User();
            u.firstName='Test First';
            u.lastname='Sales CRM';
            u.profileid=p.id;
            u.Alias = 'salesman';
            u.Email='standardusercrm@accenture.com'; 
            u.EmailEncodingKey='UTF-8';
            u.LanguageLocaleKey='en_US'; 
            u.LocaleSidKey='en_AU';
            u.TimeZoneSidKey='Australia/Brisbane';
            u.UserName='standardusercrm@accenture.com';
            u.CurrencyIsoCode='AUD';
            u.EP_CRM_Geo1__c='Africa';
           	u.EP_CRM_Geo2__c='Ghana';
            Database.SaveResult saveUser = Database.insert(u);
            
            System.runAs(u){
               
                // Insert Account
                Account acc=new Account();
                acc.name='Test Accenture';
                acc.BillingCity = 'test City';
                acc.BillingStreet  = 'test street';
                acc.BillingState = 'test state';
                acc.BillingPostalCode = '201301';
                acc.BillingCountry = 'test';
                acc.EP_CRM_Geo1__c='Africa';
           		acc.EP_CRM_Geo2__c='Ghana';
                Database.SaveResult saveacc = Database.insert(acc);
                
                //Insert Opportunity
                Opportunity opp=new Opportunity();
                opp.name='Test';
                opp.Stagename='Prospecting';
                opp.AccountId=acc.Id;
                opp.LeadSource='Campaign';
                //opp.EP_CRM_Credit_Approval_Received__c = true;
                opp.CloseDate=Date.Today() + 5;
                opp.EP_CRM_Primary_Product_Interest__c = 'Aviation';
                opp.EP_CRM_Current_Supplier__c = 'Allied';
                opp.EP_CRM_Industry__c = 'Agriculture';
                //opp.EP_CRM_KYC_Done__c = true;
                opp.EP_CRM_Margin_unit_of_measure__c = 1;
                opp.EP_CRM_Price_Type__c = 'Formula Price';
                //opp.EP_CRM_Pricing_Approval__c = true;
                opp.EP_CRM_Product_Interest_Group__c = 'Aviation';
                opp.EP_CRM_Volume__c = 1;
                opp.EP_CRM_Geo1__c='Africa';
           		opp.EP_CRM_Geo2__c='Ghana';
                Database.SaveResult saveopp = Database.insert(opp);
                //insert Event..............
               
                Event tempEvent = new Event ();
                tempEvent.OwnerId = UserInfo.getUserId();
                tempEvent.Subject='Donni';
                tempEvent.whatId = opp.Id;
                tempEvent.DurationInMinutes = 10;
                tempEvent.ActivityDateTime = system.now();
                tempEvent.Type='Meeting';
                
                Database.SaveResult savetempEvent = Database.insert(tempEvent);
                
                //delete Event........
                Database.DeleteResult deletetempEvent = Database.delete(tempEvent);
                
                Test.startTest();
                //Check System assert---------
                Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id limit 1];
                
                //validate through system Assert 
                test.stopTest();
                System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c == null);

            }
        }
        catch(exception ex){
            ex.getmessage();
        }
    }
}