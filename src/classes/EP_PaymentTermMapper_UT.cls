@isTest
public class EP_PaymentTermMapper_UT
{
    static testMethod void getRecordsByName_test() {
        EP_PaymentTermMapper localObj = new EP_PaymentTermMapper ();
        EP_Payment_Term__c  PaymentTerm = EP_TestDataUtility.createPaymentTerm();
        insert PaymentTerm ;         
        String paymentTermName ='TestPaymentTerm' ;
        Test.startTest();
        EP_Payment_Term__c  result = localObj.getRecordsByName(paymentTermName);
        Test.stopTest();
        System.AssertEquals(paymentTermName ,result.Name);
    }
    static testMethod void getRecordsByPayTermCodes_Test() {
        EP_PaymentTermMapper localObj = new EP_PaymentTermMapper ();
        EP_Payment_Term__c  PaymentTerm = EP_TestDataUtility.createPaymentTerm();
        insert PaymentTerm ;
        set<string> stringSet = new set<string>();         
        stringSet .add(PaymentTerm.EP_Payment_Term_Code__c); 
        Test.startTest();
        List<EP_Payment_Term__c>  result = localObj.getRecordsByPayTermCodes(stringSet);
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
    static testMethod void getAllPaymentTerms_Test() {
        EP_PaymentTermMapper localObj = new EP_PaymentTermMapper ();
        EP_Payment_Term__c  PaymentTerm = EP_TestDataUtility.createPaymentTerm();
        insert PaymentTerm ;        
        Integer nRows = EP_Common_Constant.ONE;         
        Test.startTest();
        List<EP_Payment_Term__c>  result = localObj.getAllPaymentTerms(nRows );
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
    
    static testMethod void getPaymentTermsMapByCodes_Test() {
        EP_PaymentTermMapper localObj = new EP_PaymentTermMapper ();
        EP_Payment_Term__c  PaymentTerm = EP_TestDataUtility.createPaymentTerm();
        insert PaymentTerm ;
        set<string> stringSet = new set<string>();         
        stringSet .add(PaymentTerm.EP_Payment_Term_Code__c);        
        Test.startTest();
        Map<String,Id> result = localObj.getPaymentTermsMapByCodes(stringSet);
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
 }