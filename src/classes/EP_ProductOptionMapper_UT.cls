/* 
      @Author <Kamendra Singh>
       @name <EP_ProductOptionMapper_UT>
       @CreateDate <25/11/2016>
       @Description  <This is apex test class for EP_ProductOptionMapper> 
       @Version <1.0>
    */
    @isTest
    public class EP_ProductOptionMapper_UT{
        /**
        * @author kamendra.singh@accenture.com
        * @date 25/11/2017
        * @description create test data for all required object in test methods
        */
        @TestSetup
        static void initData(){
            //create Price Book 
            PriceBook2 tempPricebook = EP_TestDataUtility.createPriceBookWithCompany();
            
            //Create Sell To Account
            Account tempSellToAcc = EP_TestDataUtility.createSellToAccount(null,null);
            tempSellToAcc.EP_Puma_Company__c = temppricebook.EP_Company__c;
            tempSellToAcc.EP_Status__c= EP_Common_Constant.STATUS_ACTIVE;
            tempSellToAcc.CurrencyIsoCode = 'AUD';
            database.insert(tempSellToAcc);
            
            //create Ship To Account
            Account tempShipToAcc = EP_TestDataUtility.createShipToAccount(tempSellToAcc.Id,EP_TestDataUtility.VMI_SHIP_RT );
            database.insert(tempShipToAcc);
            
            //create Product2 
            Product2 tempProduct = EP_TestDataUtility.createProduct2(true); 
            
            //Create PriceBookEntry
            PricebookEntry tempPriceBookEntry = EP_TestDataUtility.createPricebookEntry(tempProduct.id,tempPricebook.id);
            tempPriceBookEntry.EP_Attached_to_Standard_Product_List__c = true;
            tempPriceBookEntry.EP_Is_Sell_To_Assigned__c = false;
            database.insert(tempPriceBookEntry);
            
            //Create Tank...
            EP_Tank__c  tempTank = EP_TestDataUtility.createTestEP_Tank(tempShipToAcc.id,tempProduct.id);
            database.insert(tempTank);
            
            //Create Product Option
            EP_Product_Option__c objProdOption = EP_TestDataUtility.createProductOption(tempSellToAcc.Id,temppricebook.id);
        }
        /**
        * @author kamendra.singh@accenture.com
        * @date 25/11/2017
        * @description Test to check positive scenario of getProductOptionLinkedToPriceBook method.
        */
        static testMethod void getProductOptionLinkedToPriceBook_PositiveTest(){
            Id PriceBookId = [SELECT Id FROM PriceBook2].Id;
            
            Test.startTest();
            list<EP_Product_Option__c> result = EP_ProductOptionMapper.getProductOptionLinkedToPriceBook(PriceBookId);
            Test.stopTest();
            //Asert to check result
            System.assertEquals(result.size(),1);
        }
        /**
        * @author kamendra.singh@accenture.com
        * @date 25/11/2017
        * @description Test to check Negative scenario of getProductOptionLinkedToPriceBook method.
        */
        static testMethod void getProductOptionLinkedToPriceBook_NegativeTest(){
            Id PriceBookId;
            
            Test.startTest();
            list<EP_Product_Option__c> result = EP_ProductOptionMapper.getProductOptionLinkedToPriceBook(PriceBookId);
            Test.stopTest();
            //Asert to check result
            System.assertEquals(result.size(),0);
        }
        
        /**
        * @author kamendra.singh@accenture.com
        * @date 25/11/2017
        * @description Test to check Positive scenario of getProductOptionLinkedToPriceBook method.
        */
        static testMethod void getProductOptionLinkedToPriceBook_Positive_Test(){
            set<Id> ProcebookIds = new set<Id>();
            Id PriceBookId = [SELECT Id FROM PriceBook2].Id;
            ProcebookIds.add(PriceBookId);
            
            Test.startTest();
            map<Id,list<EP_Product_Option__c>> result = EP_ProductOptionMapper.getProductOptionLinkedToPriceBook(ProcebookIds);
            Test.stopTest();
            //Asert to check result
            System.assertEquals(result.values().size(),1);
        }
        
        /**
        * @author kamendra.singh@accenture.com
        * @date 25/11/2017
        * @description Test to check Negative scenario of getProductOptionLinkedToPriceBook method.
        */
        static testMethod void getProductOptionLinkedToPriceBook_Negative_Test(){
            set<Id> ProcebookIds = new set<Id>();
            Test.startTest();
            map<Id,list<EP_Product_Option__c>> result = EP_ProductOptionMapper.getProductOptionLinkedToPriceBook(ProcebookIds);
            Test.stopTest();
            //Asert to check result
            System.assertEquals(result.values().size(),0);
        }
        
        /**
        * @author kamendra.singh@accenture.com
        * @date 25/11/2017
        * @description Test to check Positive scenario of getProductOptionLinkedToPriceBook method.
        */
        static testMethod void getProductOptionLinkedToSellTo_Positive_Test(){
            Id tempAccountId  =  [select id from Account where recordTypeId =:EP_TestDataUtility.SELLTO_RT].id;
            
            Test.startTest();
             list<EP_Product_Option__c> result = EP_ProductOptionMapper.getProductOptionLinkedToSellTo(tempAccountId);
            Test.stopTest();
            //Asert to check result
            System.assertEquals(result.size(),1);
        }
        /**
        * @author kamendra.singh@accenture.com
        * @date 25/11/2017
        * @description Test to check Negative scenario of getProductOptionLinkedToPriceBook method.
        */
        static testMethod void getProductOptionLinkedToSellTo_Negative_Test(){
            Id tempAccountId;
            
            Test.startTest();
             list<EP_Product_Option__c> result = EP_ProductOptionMapper.getProductOptionLinkedToSellTo(tempAccountId);
            Test.stopTest();
            //Asert to check result
            System.assertEquals(result.size(),0);
        }
    }