/*
@Author      CloudSense
@name        EP_CsOrderTriggerHandler
@CreateDate  11/05/2018
@Description Handler for the Trigger: EP_CsOrderTrigger
@Version     1.0
*/
public class EP_CsOrderLineItemTriggerHandler {
	
	public static void contractOrderUpdate(List<csord__Order_Line_Item__c> orderLineItemList) {
		//Name of the NAV Integration User, which is stored in a Custom Label:
		String NAV_UserName = Label.NAV_Integration_User_Name;
		
		//Name of current User:
		String currentUser = UserInfo.getName();
		
		//Get the Id of the Record Type: Contract of the Order custom object
		String contractId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Contract').getRecordTypeId();
		
		//Set to store IDs of Orders (DrawDown Orders or Contracts):
		Set<Id> orderSet = new Set<Id>();
		
		//List to store Orders of every Order Line Item:
		//These Orders could be: DrawDown Orders or Orders (Record Type: 'Contract')
		List<csord__Order__c> orderList = new List<csord__Order__c>();

		//Set of Order Ids with Record Type: 'Contract':
		Set<Id> contractSet = new Set<Id>();
		
		//Set of DrawDown Order Ids:
		Set<Id> drawDownOrdertSet = new Set<Id>();
		
		//List to store Orders with Record Type: 'Contract':
		List<csord__Order__c> contractList = new List<csord__Order__c>();
		
		//List to store Orders 'Contract' to be Updated:
		List<csord__Order__c> contractsToUpdateList = new List<csord__Order__c>();
		
		//Only perform Update if NAV_UserName != currentUser:
		if(NAV_UserName != currentUser) {
			for(csord__Order_Line_Item__c orderLineItem : orderLineItemList) {
				orderSet.add(orderLineItem.csord__Order__c);
			}
		}
		
		if(!orderSet.isEmpty()) {
			
			//0) List of Orders (DrawDown Orders or Contracts) linked to the Order Line Items:
			orderList = [SELECT Id, csord__Primary_Order__c, RecordTypeId, Order_Line_Items_Quantity__c
						FROM csord__Order__c
						WHERE Id IN :orderSet
						LIMIT 1000];
			
			//1) Loop through the list of Orders and check if it is a DrawDown Order or an Order (Record Type: 'Contract')
			for(csord__Order__c order : orderList) {
				//If the field: Primary Order is not Empty, this is a DrawDown Order:
				if(order.csord__Primary_Order__c != NULL){
					//Add Primary Order of the DrawDown to the Set of Orders 'Contract':
					contractSet.add(order.csord__Primary_Order__c);
				}//Check if the Order has Record Type: 'Contract':
				else if(order.RecordTypeId == contractId){
					//contractList.add(order);
					contractSet.add(order.Id);
				}
			}
			
			//1.1) Add all of the Primary Orders from DrawDown Orders to the Contract list:
			contractList.add([SELECT Id, csord__Primary_Order__c, RecordTypeId, Order_Line_Items_Quantity__c
							FROM csord__Order__c
							WHERE Id IN :contractSet
							LIMIT 1000]);

			//1.2) Select DrawDown Orders linked to the Orders 'Contract' from the contractSet:
			List<csord__Order__c> allDrawDownOrdersList = [SELECT Id, csord__Primary_Order__c, RecordTypeId, Order_Line_Items_Quantity__c
															FROM csord__Order__c
															WHERE csord__Primary_Order__c IN :contractSet AND RecordTypeId != :contractId
															LIMIT 1000];
			
			//1.3) Add DrawDown Orders Ids to the Set:
			for(csord__Order__c drawDown : allDrawDownOrdersList){
				drawDownOrdertSet.add(drawDown.Id);
			}
			
			//2) Map: Order ('Contract')  VS  Set of DrawDown Orders
			Map <csord__Order__c, Set<Id>> orderMap = new Map <csord__Order__c, Set<Id>>();
			for(csord__Order__c order : contractList) {
				//Set of DrawDown Orders Ids:
				Set<Id>  drawDownOrderFilteredSet = new Set<Id>();
				//Check if the Map doesn't have duplicate keys:
				if(!orderMap.containsKey(order)) {
					for(csord__Order__c drawDownOrder : allDrawDownOrdersList){
						if (drawDownOrder.csord__Primary_Order__c == order.Id) {
							drawDownOrderFilteredSet.add(drawDownOrder.Id);
						}
					}
					orderMap.put(order, drawDownOrderFilteredSet);
				}
			}
			
			//2.1) Select Order Line Items linked to:
			// - DrawDown Orders from drawDownOrdertSet
			// - or Orders 'Contract' from the contractSet:
			List<csord__Order_Line_Item__c> allOrderLineItemsList = [SELECT Id, csord__Order__c, Quantity__c, isDeleted
															FROM csord__Order_Line_Item__c
															WHERE (csord__Order__c IN :drawDownOrdertSet) OR csord__Order__c IN :contractSet
															LIMIT 1000];
			
			//3) Map: Order ('Contract')  VS  List of Order Line Items:
			Map <csord__Order__c, List<csord__Order_Line_Item__c>> orderLineItemsMap = new Map <csord__Order__c, List<csord__Order_Line_Item__c>>();
			//Loop through the Contracts in the orderMap:
			for(csord__Order__c order : orderMap.keySet()) {
				//List of Order Line Items:
				List<csord__Order_Line_Item__c> orderLineItemsFilteredList = new List<csord__Order_Line_Item__c>();
				//Check if the Map doesn't have duplicate keys:
				if(!orderLineItemsMap.containsKey(order)) {
					Set<Id> drawDownSet = orderMap.get(order);
					//Loop through the List of Order LIne Items and check if they're linked to:
					// - the Order 'Contract' in question
					// -  or to any of the DrawDown Orders of that Order:
					for(csord__Order_Line_Item__c orderLineItem : allOrderLineItemsList){
						if(orderLineItem.csord__Order__c == order.Id || drawDownSet.contains(orderLineItem.csord__Order__c)){
							orderLineItemsFilteredList.add(orderLineItem);
						}
					}
					orderLineItemsMap.put(order, orderLineItemsFilteredList);
				}
			}
			
			//3.1) Loop through the Map and check if each Order 'Contract' has any DrawDown Orders:
			for(csord__Order__c order : orderLineItemsMap.keySet()) {
				//Variable to store the SUM of the values of the field: Order_Line_Items_Quantity__c
				Decimal deliveredQuantity = 0;
				//Check if the Order has Order Line Items:
				if (orderLineItemsMap.get(order) != null && orderLineItemsMap.get(order).size() > 0){
					//Loop through the Order Line Items of each key in Map:
					for(csord__Order_Line_Item__c orderLineItem : orderLineItemsMap.get(order)) {
						//Add the value of the field: Quantity__c
						deliveredQuantity += orderLineItem.Quantity__c;
					}
					order.EP_Delivered_Quantity__c = deliveredQuantity;
				}
				contractsToUpdateList.add(order);
			}
		}
		if(!contractsToUpdateList.isEmpty()){
			UPDATE contractsToUpdateList;
		}
	}
	
	public static void contractOrderUpdateAfterDelete(List<csord__Order_Line_Item__c> deletedOrderLineItemsList) {
				//Name of the NAV Integration User, which is stored in a Custom Label:
		String NAV_UserName = Label.NAV_Integration_User_Name;
		
		//Name of current User:
		String currentUser = UserInfo.getName();
		
		//Get the Id of the Record Type: Contract of the Order custom object
		String contractId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Contract').getRecordTypeId();
		
		//Set to store IDs of Orders (DrawDown Orders or Contracts):
		Set<Id> orderSet = new Set<Id>();
		
		//List to store Orders of every Order Line Item:
		//These Orders could be: DrawDown Orders or Orders (Record Type: 'Contract')
		List<csord__Order__c> orderList = new List<csord__Order__c>();

		//Set of Order Ids with Record Type: 'Contract':
		Set<Id> contractSet = new Set<Id>();
		
		//Set of DrawDown Order Ids:
		Set<Id> drawDownOrdertSet = new Set<Id>();
		
		//List to store Orders with Record Type: 'Contract':
		List<csord__Order__c> contractList = new List<csord__Order__c>();
		
		//List to store Orders 'Contract' to be Updated:
		List<csord__Order__c> contractsToUpdateList = new List<csord__Order__c>();
		
		//Only perform Update if NAV_UserName != currentUser:
		if(NAV_UserName != currentUser) {
			for(csord__Order_Line_Item__c orderLineItem : deletedOrderLineItemsList) {
				orderSet.add(orderLineItem.csord__Order__c);
			}
		}
		
		if(!orderSet.isEmpty()) {
			//0) List of Orders (DrawDown Orders or Contracts) linked to the Order Line Items:
			orderList = [SELECT Id, csord__Primary_Order__c, RecordTypeId, Order_Line_Items_Quantity__c
						FROM csord__Order__c
						WHERE Id IN :orderSet
						LIMIT 1000];
			
			//1) Loop through the list of Orders and check if it is a DrawDown Order or an Order (Record Type: 'Contract')
			for(csord__Order__c order : orderList) {

				//If the field: Primary Order is not Empty, this is a DrawDown Order:
				if(order.csord__Primary_Order__c != NULL){
					//Add Primary Order of the DrawDown to the Set of Orders 'Contract':
					contractSet.add(order.csord__Primary_Order__c);
				}//Check if the Order has Record Type: 'Contract':
				else if(order.RecordTypeId == contractId){
					//contractList.add(order);
					contractSet.add(order.Id);
				}

			}
			
			
			//1.1) Add all of the Primary Orders from DrawDown Orders to the Contract list:
			contractList.add([SELECT Id, csord__Primary_Order__c, RecordTypeId, Order_Line_Items_Quantity__c, EP_Delivered_Quantity__c
							FROM csord__Order__c
							WHERE Id IN :contractSet
							LIMIT 1000]);
			
			//1.2) Select DrawDown Orders linked to the Orders 'Contract' from the contractSet:
			List<csord__Order__c> allDrawDownOrdersList = [SELECT Id, csord__Primary_Order__c, RecordTypeId, Order_Line_Items_Quantity__c
															FROM csord__Order__c
															WHERE csord__Primary_Order__c IN :contractSet AND RecordTypeId != :contractId
															LIMIT 1000];
			
			//1.3) Add DrawDown Orders Ids to the Set:
			for(csord__Order__c drawDown : allDrawDownOrdersList){
				drawDownOrdertSet.add(drawDown.Id);
			}
			
			//2) Map: Order ('Contract')  VS  Set of DrawDown Orders
			Map <csord__Order__c, Set<Id>> orderMap = new Map <csord__Order__c, Set<Id>>();
			for(csord__Order__c order : contractList) {
				//Set of DrawDown Orders Ids:
				Set<Id>  drawDownOrderFilteredSet = new Set<Id>();
				//Check if the Map doesn't have duplicate keys:
				if(!orderMap.containsKey(order)) {
					for(csord__Order__c drawDownOrder : allDrawDownOrdersList){
						if (drawDownOrder.csord__Primary_Order__c == order.Id) {
							drawDownOrderFilteredSet.add(drawDownOrder.Id);
						}
					}
					orderMap.put(order, drawDownOrderFilteredSet);
				}
			}
			
			//3) Map: Order ('Contract')  VS  List of Deleted Order Line Items:
			Map <csord__Order__c, List<csord__Order_Line_Item__c>> deletedOrderLineItemsMap = new Map <csord__Order__c, List<csord__Order_Line_Item__c>>();
			//Loop through the Contracts in the orderMap:
			for(csord__Order__c order : orderMap.keySet()) {
				//List of Order Line Items:
				List<csord__Order_Line_Item__c> orderLineItemsFilteredList = new List<csord__Order_Line_Item__c>();
				//Check if the Map doesn't have duplicate keys:
				if(!deletedOrderLineItemsMap.containsKey(order)) {
					Set<Id> drawDownSet = orderMap.get(order);
					//Loop through the List of Order LIne Items (From the TRIGGER) and check if they're linked to:
					// - the Order 'Contract' in question
					// -  or to any of the DrawDown Orders of that Order:
					for(csord__Order_Line_Item__c orderLineItem : deletedOrderLineItemsList){
						if(orderLineItem.csord__Order__c == order.Id || drawDownSet.contains(orderLineItem.csord__Order__c)){
							orderLineItemsFilteredList.add(orderLineItem);
						}
					}
					deletedOrderLineItemsMap.put(order, orderLineItemsFilteredList);
				}
			}
			
			//3.1) Loop through the Map and check if each Order 'Contract' has any DrawDown Orders:
			for(csord__Order__c order : deletedOrderLineItemsMap.keySet()) {
				//Variable to store the SUM of the values of the field: Order_Line_Items_Quantity__c
				Decimal deliveredQuantityToSubtract = 0;
				//Check if the Order has Order Line Items:
				if (deletedOrderLineItemsMap.get(order) != null && deletedOrderLineItemsMap.get(order).size() > 0){
					//Loop through the Order Line Items of each key in Map:
					for(csord__Order_Line_Item__c orderLineItem : deletedOrderLineItemsMap.get(order)) {
						//Add the value of the field: Quantity__c
						deliveredQuantityToSubtract += orderLineItem.Quantity__c;
					}
					order.EP_Delivered_Quantity__c = order.EP_Delivered_Quantity__c - deliveredQuantityToSubtract;
				}
				contractsToUpdateList.add(order);
			}
		}
		if(!contractsToUpdateList.isEmpty()){
			UPDATE contractsToUpdateList;
		}
	}
	
}