/* 
   @Author          Accenture
   @name            EP_StagingRetailOrderHandler
   @CreateDate      13/04/2018
   @Description     Handler class to handle the Retail Order inbound interface
   @Version         1.0
*/
public class EP_StagingRetailOrderHandler extends EP_InboundHandler{ 
    /**
    * @author           Accenture
    * @name             processRequest
    * @date             13/04/2018
    * @description      Will process the  Retail Order request and sends back the response
    * @param            string
    * @return           string 
    */
    public override string processRequest(string jsonRequest){ 
        EP_GeneralUtility.Log('Public','EP_StagingRetailOrderHandler','processRequest');
        string jsonResponse = EP_Common_Constant.BLANK;
        List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
        EP_MessageHeader HeaderCommon = new EP_MessageHeader();  
        EP_IntegrationUtil.isCallout = true;
        try {
        	jsonResponse = EP_AcknowledgementHandler.createAcknowledgement(EP_Common_Constant.AZURE_TO_SFDC_RETAIL_ORDER_RDW_SYNC,false,EP_Common_Constant.BLANK,HeaderCommon,ackDatasets);
             //EP_ROImportStagingFactory.ProcessStagingRecords(newfileRecord.Id,'EP_Retail_Order');
        } catch(Exception ex){          
            jsonResponse = EP_AcknowledgementHandler.createAcknowledgement(EP_Common_Constant.AZURE_TO_SFDC_RETAIL_ORDER_RDW_SYNC,true,ex.getMessage(),HeaderCommon,ackDatasets);
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, EP_Common_Constant.NAV_TO_SFDC_STOCK_LOCATION_SYNC,EP_StockLocationCreateUpdateHandler.class.getName(), ApexPages.Severity.ERROR);
        }
        return jsonResponse;
    }
}