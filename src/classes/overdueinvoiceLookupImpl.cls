global with sharing class overdueinvoiceLookupImpl extends cscfga.ALookupSearch {
    private static final String BASKET_ID_FIELD = 'Product Basket ID';
    private static final String ACCOUNT_ID_FIELD = 'AccountId';

    global override List<Object> doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionId) {
        String accountId = searchFields.get(ACCOUNT_ID_FIELD);
        String basketId = searchFields.get(BASKET_ID_FIELD);
        String invstatus = 'ok';

        List<EP_Invoice__c> result = [
            select id,name ,EP_Overdue__c from EP_Invoice__c where EP_Bill_To__c= :accountId
        ];
        if (result.size()>0){
            for(EP_Invoice__c a : result){
                if (a.EP_Overdue__c){
                    invstatus = 'nok';
                    break;
                }
            }
        }

        return new List<cscfga__Product_Configuration__c> {
            new cscfga__Product_Configuration__c(
                overdue_invoice__c = invstatus
            )
        };

 }

    global override String getRequiredAttributes() {
        return '["' + BASKET_ID_FIELD + '","' + ACCOUNT_ID_FIELD + '"]';
    }
}