/*   
     @Author Aravindhan Ramalingam
     @name <EP_RetroVMIConsignmentSM.cls>     
     @Description <Retro VMI Consignment Statemachine >   
     @Version <1.1> 
     */

     public class EP_RetroVMIConsignmentSM extends EP_OrderStateMachine {
     	
     	public EP_RetroVMIConsignmentSM(){

     	}
     	
     	public override EP_OrderState getOrderState(EP_OrderEvent currentEvent){
     		EP_GeneralUtility.Log('Public','EP_RetroVMIConsignmentSM','getOrderState');
     		return super.getOrderState(currentEvent);
     	}
     }