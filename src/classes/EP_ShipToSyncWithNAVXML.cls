/* 
   @Author          Accenture
   @name            EP_ShipToSyncWithNAVXML
   @CreateDate      11/18/2017
   @Description     This class is use to created shipTo payload XML for NAV. This will be used for syncing single shipto  
   @Novasuite fix - Required Documentation
   @Version         1.0
*/

public class EP_ShipToSyncWithNAVXML extends EP_AccountRequestXML{
	/**
    * @author           Accenture
    * @name             init
    * @date             11/18/2017
    * @description      retrive the records to bind the values in XML
    * @param            NA
    * @return           NA
    */
	public  override void init(){
		EP_GeneralUtility.Log('Public','EP_ShipToSyncWithNAVXML','init');		
		MSGNode = doc.createRootElement(EP_AccountConstant.MSG, null, null);
		EP_AccountMapper objAccMap = new EP_AccountMapper();   
		objAccount = objAccMap.getAccountRecord(recordId);
        super.setEncryption();
	}
	/**
    * @author           Accenture
    * @name             createPayload
    * @date             11/18/2017
    * @description      This method is used to create Payload for Ship to Account Sync with NAV
    * @param            NA
    * @return           NA
    */
	public override void createPayload(){
		EP_GeneralUtility.Log('Public','EP_ShipToSyncWithNAVXML','createPayload');	
		Dom.XMLNode PayloadNode = MSGNode.addChildElement(EP_AccountConstant.PAYLOAD,null,null);
        Dom.XmlNode AnyNode = PayloadNode.addChildElement(EP_AccountConstant.ANY0,null, null);        
        DOM.Document tempDoc = new DOM.Document();
		//ShiptoAddresses node
		Dom.XMLNode shipToAddressesNode = tempDoc.createRootElement(EP_AccountConstant.SHIPTOADDRESSES,null,null);
		Dom.XMlNode shipToAddressNode = shipToAddressesNode.addChildElement(EP_AccountConstant.SHIPTOADDRESS,null,null);
		String seqid = EP_IntegrationUtil.reCreateSeqId(messageId, objAccount.id); 
		shipToAddressNode.addChildElement(EP_AccountConstant.SEQID,null,null).addTextNode(getValueforNode(seqid));
		//Identifier node
		Dom.XMLNode identifierNode = shipToAddressNode.addChildElement(EP_AccountConstant.IDENTIFIER,null,null);
		identifierNode.addChildElement(EP_AccountConstant.SHIPTOID,null,null).addTextNode(getValueforNode(objAccount.EP_Account_Id__c));
		identifierNode.addChildElement(EP_AccountConstant.CUSTID,null,null).addTextNode(getValueforNode(objAccount.Parent.EP_Account_Id__c));
		identifierNode.addChildElement(EP_AccountConstant.ENTRPRSID,null,null).addTextNode(getValueforNode(objAccount.EP_EnterpriseId__c));
		shipToAddressNode.addChildElement(EP_AccountConstant.NAME,null,null).addTextNode(getValueforNode(objAccount.Name));
		shipToAddressNode.addChildElement(EP_AccountConstant.NAME2,null,null).addTextNode(getValueforNode(objAccount.EP_Account_Name_2__c));
		shipToAddressNode.addChildElement(EP_AccountConstant.CNTRYCODE,null,null).addTextNode(getValueforNode(objAccount.EP_Country_Code__c));
		shipToAddressNode.addChildElement(EP_AccountConstant.ADDRESS,null,null).addTextNode(getValueforNode(objAccount.ShippingStreet));
		shipToAddressNode.addChildElement(EP_AccountConstant.POSTCODE,null,null).addTextNode(getValueforNode(objAccount.ShippingPostalCode));
		shipToAddressNode.addChildElement(EP_AccountConstant.CITY,null,null).addTextNode(getValueforNode(objAccount.ShippingCity));
		shipToAddressNode.addChildElement(EP_AccountConstant.PHONE,null,null).addTextNode(getValueforNode(objAccount.Phone));
		//shipToAddressNode.addChildElement(EP_AccountConstant.MOBILEPHONE,null,null).addTextNode(getValueforNode(objAccount.EP_Mobile_Phone__c));//
		shipToAddressNode.addChildElement(EP_AccountConstant.WEBSITE,null,null).addTextNode(getValueforNode(objAccount.Website));//
		shipToAddressNode.addChildElement(EP_AccountConstant.FAX,null,null).addTextNode(getValueforNode(objAccount.Fax));
		shipToAddressNode.addChildElement(EP_AccountConstant.EMAIL,null,null).addTextNode(getValueforNode(objAccount.EP_Email__c));
		shipToAddressNode.addChildElement(EP_AccountConstant.COUNTY,null,null).addTextNode(getValueforNode(objAccount.ShippingState));
		shipToAddressNode.addChildElement(EP_AccountConstant.ISVMI,null,null).addTextNode(getValueforNode(objAccount.EP_Is_VMI_ShipTo__c));
		shipToAddressNode.addChildElement(EP_AccountConstant.SHIPTOTYPE,null,null).addTextNode(getValueforNode(objAccount.EP_Ship_To_Type__c));
		//shipToAddressNode.addChildElement(EP_AccountConstant.BLOCKEDTAG,null,null).addTextNode(getValueforNode(objAccount.EP_Block_Status__c)); //check for mapping value
		shipToAddressNode.addChildElement(EP_AccountConstant.STATUS,null,null).addTextNode(getValueforNode(objAccount.EP_ShipTo_Status_NAV__c)); 		
		shipToAddressNode.addChildElement(EP_AccountConstant.VERSIONNR,null,null);
        // Encoding payload by calling encode XML method in superclass
        AnyNode.addTextNode(encodeXML(tempDoc));
	}	
}