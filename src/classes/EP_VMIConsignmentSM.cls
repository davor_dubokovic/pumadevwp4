/*   
     @Author Aravindhan Ramalingam
     @name <EP_VMIConsignmentSM.cls>     
     @Description <VMI Consignment Statemachine >   
     @Version <1.1> 
     */

     public class EP_VMIConsignmentSM extends EP_OrderStateMachine {
     	
     	public EP_VMIConsignmentSM(){

     	}
     	
     	public override EP_OrderState getOrderState(EP_OrderEvent currentEvent){
     		EP_GeneralUtility.Log('Public','EP_VMIConsignmentSM','getOrderState');
     		return super.getOrderState(currentEvent);
     	}
     }