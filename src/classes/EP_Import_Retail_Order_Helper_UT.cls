/*
    @Author          Accenture
    @Name            EP_Import_Retail_Order_Helper_UT
    @CreateDate      18/04/2018
    @Description     This is a Unit test class for EP_Import_Retail_Order_Helper.
    @Version         1.0
    @Reference       NA
*/

@isTest
public class EP_Import_Retail_Order_Helper_UT {

static testMethod void Import_Retail_Order_Helper_test(){
string sampleXml = '<?xml version="1.0"?><salesData><header><companyCode>AAF</companyCode><transactionNr>05</transactionNr><sellToId>614007444</sellToId><locationId>20727</locationId><posLocationId>20727</posLocationId><transactionDt>2018-02-12</transactionDt><createdDt>2018-02-21</createdDt><createdBy>Puma Energy</createdBy><fileName>20180323185924_614007444_20727_216305.xml</fileName></header><lines><line><lineNr>1</lineNr><itemId>1008008</itemId><desc>SALES COMMISSION</desc><SenderCode>123</SenderCode><qty>1</qty><unitPrice>893.7400</unitPrice></line></lines></salesData>';  
EP_Import_Retail_Order_Helper localObj = new EP_Import_Retail_Order_Helper(sampleXml); 
system.assert(True);//Dummy Assert
}
      
}