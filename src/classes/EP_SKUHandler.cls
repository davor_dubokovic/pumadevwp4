/* 
@Author <Jai Singh>
@name <EP_SKUHandler>
@CreateDate <22/04/2016>
@Description <This is handler class for SKV Sync from NAV to SFDC> 
@Version <1.0>
*/
public without sharing class EP_SKUHandler{

	// Static string Value Declaration
	private static final string CLASSNAME = 'EP_SKUSyncNAVtoSFDC';    
	private static final string SKU_SYNC = 'SKU SYNC FROM NAV TO SFDC';
	private static final string SKUs = 'SKUs';
	private static final string CREATEDATASET = 'createDataSet';
	
	public static final String CONTENT_TYPE= 'Content-Type';
	public static final String APPLICATION_JSON = 'application/json';
	public static final string ITEMID_BLANK = 'Product Code OR Company Code is blank, ';
	public static final string CLIENTID_BLANK = 'Supply Location is blank, ';
	public static final string NAME_BLANK = 'name is blank,';
	static final String PARSE_MTD = 'parse';
	private static String JSONString = '';
	/**
	* @author <Jai Singh>
	* @date <22/06/2016>
	* @name <parse>
	* @description <This method is a parser method>
	* @param String
	* @return SKUwrapper
	*/ 
	public static SKUwrapper parse(String jsonReq){

		SKUwrapper SKUwrapper = new SKUwrapper();
		SKUwrapper.listSKUInventories = new list<SKUInventoryWrapper>();
		
        JSONString = jsonReq;
		try{
			if(string.isNotBlank(jsonReq)){
				EP_DebugLogger.printDebug(' ------- JSON Request Received ----- ' + jsonReq);
				JSONParser parser = JSON.createParser(jsonReq);
				//Parse JSON Request
				while(parser.nextToken() != null){
					if(parser.getCurrentToken() == JSONTOKEN.FIELD_NAME){
						//Get Message ID to be used as correlation Id in Acknowledgment
						if(EP_Common_Constant.HEADER_COMMON.equalsIgnoreCase(parser.getCurrentName())){
							parser.nextToken();
							SKUwrapper.headerCommon = (EP_AcknowledmentGenerator.cls_HeaderCommon)parser.readValueAs(EP_AcknowledmentGenerator.cls_HeaderCommon.class);
							parser.skipChildren();
						}

						//Get customers from requests
						else {
							if(EP_Common_Constant.SKUS.equalsIgnoreCase(parser.getCurrentName())){
								parser.nextToken();
								//If list of customers
								if(parser.getCurrentToken() == JSONTOKEN.START_ARRAY){
									SKUwrapper.listSKUInventories = (List<SKUInventoryWrapper>)parser.readValueAs(List<SKUInventoryWrapper>.class);
								}
								//else if single customer
								else {
									if(parser.getCurrentToken() == JSONTOKEN.START_OBJECT){
										SKUwrapper.listSKUInventories.add((SKUInventoryWrapper)parser.readValueAs(SKUInventoryWrapper.class));
									}
								}   
								parser.skipChildren();
							}
						}
					}
				}
			}
		}
		catch(Exception handledException){
			EP_LoggingService.futureLogException( UserInfo.getOrganizationId(), EP_Common_Constant.EPUMA, CLASSNAME,PARSE_MTD,EP_COMMON_CONSTANT.ERROR_SYNC_SENT_STATUS );                                         
		}

		return SKUwrapper;
	}

	/**
	* @author <Jai Singh>
	* @date <22/06/2016>
	* @name <newInventory>
	* @description <This method create Inventory Object>
	* @param none
	* @return EP_Inventory__c
	*/ 
	private static EP_Inventory__c newInventory(){
		return new EP_Inventory__c();
	}

	/**
	* @author <Jai Singh>
	* @date <22/06/2016>
	* @name <newProduct>
	* @description <This method create Product2 Object>
	* @param String
	* @return product2
	*/ 
	private static product2 newProduct(string foreignKey){
		return new product2(EP_NAV_Product_Company__c = foreignKey);
	}

	/**
	* @author <Jai Singh>
	* @date <22/06/2016>
	* @name <newAccount>
	* @description <This method create Account Object>
	* @param String
	* @return Account
	*/ 
	private static Account newAccount(string foreignKey){
		return new Account(EP_Nav_Stock_Location_Id__c = foreignKey);
	}

	/**
	* @author <Jai Singh>
	* @date <22/06/2016>
	* @name <createUpdateSKU>
	* @description <This method to insert the records to Inventory Object>
	* @param SKUwrapper
	* @return void
	*/ 
	public static void createUpdateSKU(SKUwrapper SKUwrapper){
		map<String,String> mapHeaderCommonConstants = new map<String,String>();
		List<EP_AcknowledmentGenerator.cls_dataset> listDataSets = new list<EP_AcknowledmentGenerator.cls_dataset>();
		//List<Account> accountsToUpdate = new List<Account>();
		set<String> setSequenceIDs = new set<String>();
		EP_AcknowledmentGenerator.cls_dataset dataSet;
		RestRequest request = RestContext.request;
		RestContext.response.addHeader(CONTENT_TYPE, APPLICATION_JSON);
		String processStatus = EP_Common_Constant.BLANK;
		// String reqBody = request.requestBody.toString();
		string response = EP_Common_Constant.Blank;
		String errorDescription = EP_Common_Constant.BLANK;
		String messageId = EP_Common_Constant.BLANK;
		Boolean success;        
		Integer queryRowLimit;

		EP_DebugLogger.printDebug(' ------- JSON Request Parsed N ----- ' + SKUwrapper);     
		List<EP_Inventory__c> listInventories = new List<EP_Inventory__c>();
		//Code changes for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
		//1: Get Header Details from the JSON String
        Map<String, String> mapOfHeaderCommonDetails = EP_IntegrationService.getRequestHeaderDetails(JSONString);
        
		//2: Check for Idempotency - Check the message Id in integration Record if already exists then return old Response
        if(EP_IntegrationService.isIdempotent(mapOfHeaderCommonDetails.get(EP_Common_Constant.MSG_ID))) {
        	RestContext.response.responseBody = blob.valueOf(EP_IntegrationService.processResponse);
        	return;
        }
        //3: Log Request - Log the JSON String and other details from Header into the integration record for inbound Request        
        EP_IntegrationRecord__c integrationRecord = EP_IntegrationService.logInboundRequest('SKU_SYNC',mapOfHeaderCommonDetails,JSONString);
        //Code changes END for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
        try{
            processStatus = EP_Common_Constant.SUCCESS;
            for(SKUInventoryWrapper skuRecord : SKUwrapper.listSKUInventories){
                errorDescription = EP_Common_Constant.BLANK;
                success = true;
                setSequenceIDs.add(skuRecord.seqId);
                IdentifierWrapper skuIdentifier = skuRecord.identifier;
                EP_Inventory__c inventoryRecord = newInventory();
                if(string.isNotBlank(skuIdentifier.itemNr) || string.isNotBlank(skuIdentifier.clientId)){
                    String itmUniqueKey = skuIdentifier.itemNr + EP_Common_Constant.STRING_HYPHEN + skuIdentifier.clientId;
                    product2 productExtId = newProduct(itmUniqueKey);
                    inventoryRecord.EP_Product__r = productExtId;
                }
                else{
                    success = false;                    
                    errorDescription += ITEMID_BLANK;
                }               
                if(string.isNotBlank(skuIdentifier.supplyLoc)){               
                    Account supplyLocationUniqueKey = newAccount(skuIdentifier.supplyLoc); 
                    inventoryRecord.EP_Storage_Location__r = supplyLocationUniqueKey;
                }
                else{
                    success = false;                    
                    errorDescription += CLIENTID_BLANK;
                }

                inventoryRecord.EP_Source_Seq_Id__c = String.isNotBlank(skuRecord.seqId)?skuRecord.seqId:EP_Common_Constant.BLANK; 
                inventoryRecord.EP_SKU_Unique_Key__c = skuIdentifier.itemNr + EP_Common_Constant.STRING_HYPHEN + skuIdentifier.clientId + EP_Common_Constant.STRING_HYPHEN + skuIdentifier.supplyLoc;
                
                inventoryRecord.EP_Company_Code__c = skuIdentifier.clientId;//Added This line-Under L4 Change
                //L4 #45520 Change Start
                EP_CompanyMapper companyMapperObj = new EP_CompanyMapper();
                inventoryRecord.EP_Company__c = companyMapperObj.getCompanyDetailsByCompanyCode(skuIdentifier.clientId)<> null?companyMapperObj.getCompanyDetailsByCompanyCode(skuIdentifier.clientId).Id:null;
                //L4 #45520 Change End
                if(success){
                    listInventories.add(inventoryRecord);
                }
                else{
                    processStatus = EP_Common_Constant.FAILURE;
                    listDataSets.add(createDataSet(SKUs
                    ,skuRecord.seqId
                    ,EP_Common_Constant.ERROR_SYNC_SENT_STATUS
                    ,errorDescription.removeEnd(EP_Common_Constant.COMMA)));
                }
            }

            if( !listInventories.isEmpty() ){
                Schema.DescribeFieldResult inventoryField = EP_Inventory__c.EP_SKU_Unique_Key__c.getDescribe();
                Schema.sObjectField skuExternalId = inventoryField.getSObjectField();

                list<Database.UpsertResult> listUpsertResult = database.upsert(listInventories,skuExternalId,false);
                for( integer index = 0 ; index < listUpsertResult.size() ; index++ ){
                    dataSet = createDataSet(SKUs
                    ,listInventories[index].EP_Source_Seq_Id__c
                    ,EP_Common_Constant.BLANK
                    ,EP_Common_Constant.BLANK);
                    if(!(listUpsertResult[index].isSuccess())){
                        processStatus = EP_Common_Constant.FAILURE;
                        for( Database.Error error : listUpsertResult[index].getErrors() ){
                            if(!dataSet.errorCode.contains(error.getStatusCode().name())){
                                dataSet.errorCode += error.getStatusCode().name() + EP_Common_Constant.COMMA;
                            }
                            if(!dataSet.errorDescription.contains(error.getMessage())){
                                dataSet.errorDescription += error.getMessage() + EP_Common_Constant.COMMA;
                            }
                            EP_DebugLogger.printDebug(' ----- Error -------- ' +  error.getMessage());                        
                        }
                        dataSet.errorCode = dataSet.errorCode.removeEnd(EP_Common_Constant.COMMA);
                        dataSet.errorDescription = dataSet.errorDescription.removeEnd(EP_Common_Constant.COMMA);
                    }
                    listDataSets.add(dataSet);
                }
            }
        }
        catch(Exception handledException){
            // Exception Handler code
            EP_LoggingService.logHandledException( handledException, EP_Common_Constant.EPUMA, SKU_SYNC,CLASSNAME, ApexPages.Severity.ERROR );
            processStatus = EP_Common_Constant.FAILURE;
            for(String sequenceId : setSequenceIDs){ 
                listDataSets.add(createDataSet(SKUs,sequenceId,EP_Common_Constant.EXCEPTION_Response,handledException.getMessage())); 
            }
        }

        try{
            //Create message id
            EP_Message_Id_Generator__c msgIdGenerator = new EP_Message_Id_Generator__c();
            database.insert(msgIdGenerator);
            queryRowLimit = Limits.getLimitQueryRows() - Limits.getQueryRows();
            msgIdGenerator = [SELECT Name 
                                FROM EP_Message_Id_Generator__c 
                                WHERE Id = :msgIdGenerator.Id
                                LIMIT :queryRowLimit];
            messageId = EP_IntegrationUtil.getMessageId(EP_Common_Constant.SFD
            ,EP_Common_Constant.GBL
            ,EP_PROCESS_NAME_CS__c.getValues(EP_Common_Constant.SKU_CRTN_FRM_NAV).EP_Process_Name__c
            ,DateTime.now()
            ,MsgIdgenerator.name);

            //Create header map
            mapHeaderCommonConstants.put(EP_Common_Constant.MSG_ID,messageId);
            mapHeaderCommonConstants.put(EP_Common_Constant.PROCESS_STATUS,processStatus);

            mapHeaderCommonConstants.put(EP_Common_Constant.INTERFACE_TYPE, SKUwrapper.headerCommon.InterfaceType);
            mapHeaderCommonConstants.put(EP_Common_Constant.SOURCE_GROUP_COMPANY, SKUwrapper.headerCommon.SourceGroupCompany);
            mapHeaderCommonConstants.put(EP_Common_Constant.DESTINATION_GROUP_COMPANY, SKUwrapper.headerCommon.DestinationGroupCompany);
            mapHeaderCommonConstants.put(EP_Common_Constant.SOURCE_COMPANY, SKUwrapper.headerCommon.SourceCompany);
            mapHeaderCommonConstants.put(EP_Common_Constant.DESTINATION_COMPANY, SKUwrapper.headerCommon.DestinationCompany);
            mapHeaderCommonConstants.put(EP_Common_Constant.CORR_ID, SKUwrapper.headerCommon.msgId);
            mapHeaderCommonConstants.put(EP_Common_Constant.DESTINATION_ADDRESS, SKUwrapper.headerCommon.DestinationAddress);
            mapHeaderCommonConstants.put(EP_Common_Constant.SOURCE_RESP_ADD, SKUwrapper.headerCommon.SourceResponseAddress);
            mapHeaderCommonConstants.put(EP_Common_Constant.SOURCE_UPDATE_STTS_ADD, SKUwrapper.headerCommon.SourceUpdateStatusAddress);
            mapHeaderCommonConstants.put(EP_Common_Constant.DESTINATION_UPDATE_STATUS_ADDRESS, SKUwrapper.headerCommon.DestinationUpdateStatusAddress);
            mapHeaderCommonConstants.put(EP_Common_Constant.MIDDLEWARE_URL_FOR_PUSH, SKUwrapper.headerCommon.MiddlewareUrlForPush);
            mapHeaderCommonConstants.put(EP_Common_Constant.EMAIL_NOTIFICATION, SKUwrapper.headerCommon.EmailNotification);
            mapHeaderCommonConstants.put(EP_Common_Constant.ACK_ERROR_CODE, SKUwrapper.headerCommon.ErrorCode);
            mapHeaderCommonConstants.put(EP_Common_Constant.ERROR_DESCRIPTION, SKUwrapper.headerCommon.ErrorDescription);
            mapHeaderCommonConstants.put(EP_Common_Constant.PROCESSING_ERROR_DESCRIPTION, SKUwrapper.headerCommon.ProcessingErrorDescription);
            mapHeaderCommonConstants.put(EP_Common_Constant.CONT_ON_ERR_STTS, SKUwrapper.headerCommon.ContinueOnError);
            mapHeaderCommonConstants.put(EP_Common_Constant.COMP_LGNG, SKUwrapper.headerCommon.ComprehensiveLogging);
            mapHeaderCommonConstants.put(EP_Common_Constant.TRANSPORT_STATUS, SKUwrapper.headerCommon.TransportStatus);
            mapHeaderCommonConstants.put(EP_Common_Constant.UPDT_SRC_ON_RCV, SKUwrapper.headerCommon.UpdateSourceOnReceive);
            mapHeaderCommonConstants.put(EP_Common_Constant.UPDT_SRC_ON_DLVRY, SKUwrapper.headerCommon.UpdateSourceOnDelivery);
            mapHeaderCommonConstants.put(EP_Common_Constant.UPDT_SRC_AFTER_PRCSNG, SKUwrapper.headerCommon.UpdateSourceAfterProcessing);
            mapHeaderCommonConstants.put(EP_Common_Constant.UPDT_DEST_ON_DLVRY, SKUwrapper.headerCommon.UpdateDestinationOnDelivery);
            mapHeaderCommonConstants.put(EP_Common_Constant.CALL_DEST_FOR_PRCSNG, SKUwrapper.headerCommon.CallDestinationForProcessing);
            mapHeaderCommonConstants.put(EP_Common_Constant.OBJECT_TYPE, SKUwrapper.headerCommon.ObjectType);
            mapHeaderCommonConstants.put(EP_Common_Constant.OBJECT_NAME, SKUwrapper.headerCommon.ObjectName);
            mapHeaderCommonConstants.put(EP_Common_Constant.COMMUNICATION_TYPE, SKUwrapper.headerCommon.CommunicationType);
            mapHeaderCommonConstants.put(EP_Common_Constant.OBJECT_NAME, SKUwrapper.headerCommon.ObjectName);

            //Create acknowledgment with header
            response = EP_AcknowledmentGenerator.createacknowledgement( mapHeaderCommonConstants, listDataSets,EP_Common_Constant.BLANK);
            //Code changes for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
            //4: Log the Proccessing Response
            EP_IntegrationService.logProcessResponse(integrationRecord, response);
            RestContext.response.responseBody = blob.valueOf(response);
        }catch(Exception handledException){
            EP_LoggingService.logHandledException( handledException, EP_Common_Constant.EPUMA, SKU_SYNC,CLASSNAME, ApexPages.Severity.ERROR );
        }
    }

    /**
    * @author <Jai Singh>
    * @date <22/06/2016>
    * @name <createDataSet>
    * @description <This method creates dataset for acknowledgement>
    * @param String, String, String, String
    * @return EP_AcknowledmentGenerator.cls_dataset
    */ 
    public static EP_AcknowledmentGenerator.cls_dataset createDataSet(String Name, String sequenceId, String errorCode, String errorDescription){
        EP_AcknowledmentGenerator.cls_dataset dataSet;
        //try{
            dataSet = new EP_AcknowledmentGenerator.cls_dataset();
            dataSet.name = name;
            dataSet.seqId = sequenceId;
            dataSet.errorCode = errorCode;
            dataSet.errorDescription = errorDescription;
        //}catch(Exception handledException){
            // exception handling logic
            //EP_LoggingService.logHandledException( handledException, EP_Common_Constant.EPUMA, CREATEDATASET,CLASSNAME, ApexPages.Severity.ERROR );
        //}
        return dataset;
    }

    /* 
    @Author <Jai Singh>
    @name <SKUwrapper>
    @CreateDate <22/04/2016>
    @Description <Wrapper class for SKU Data > 
    @Version <1.0>
    */
    public without sharing class SKUwrapper {
        public List<SKUInventoryWrapper> listSKUInventories; 
        public EP_AcknowledmentGenerator.cls_HeaderCommon  headerCommon;        
    }

    /* 
    @Author <Jai Singh>
    @name <SKUInventoryWrapper>
    @CreateDate <22/04/2016>
    @Description <Wrapper class for SKU Data> 
    @Version <1.0>
    */
    public without sharing class SKUInventoryWrapper {
        public String seqId;    //1232323
        public IdentifierWrapper identifier;
        public String description;  //description
    }

    /* 
    @Author <Jai Singh>
    @name <IdentifierWrapper>
    @CreateDate <22/04/2016>
    @Description <Wrapper class for SKU Data> 
    @Version <1.0>
    */
    public without sharing class IdentifierWrapper {
        public String supplyLoc;    //supplyLoc
        public String itemNr;   //itemNr
        public String clientId; //clientId
    }    
}