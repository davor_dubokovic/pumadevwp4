/*
*  @Author <Accenture>
*  @Name <EP_BusinessChannelSegmentMapper_UT>
*  @CreateDate <16/03/2017>
*  @Description <Test class for EP_BusinessChannelSegmentMapper class>
*  @Version <1.0>
*/

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
                                                                                         
    *************************************************************************************************************
*/
@isTest
public with sharing class EP_BusinessChannelSegmentMapper_UT{ 
   
        static testmethod void getRecordsByIDsAsMap_Test(){       
    
        EP_BusinessChannelSegmentMapper busMapperObj = new EP_BusinessChannelSegmentMapper();
        // **** IMPLEMENT THIS SECTION ~@~ *****
       List<EP_Business_Channel_Segment__c> busSegChnList = EP_TestDataUtility.createBusChnlSegmnt(100);
        insert busSegChnList;
        Set<Id> sBusSegChnlIds = (new Map<Id,SObject>(busSegChnList)).keySet();
        Map<Id,EP_Business_Channel_Segment__c> returnedBusSegMap;
        // **** TILL HERE ~@~ *****  
        Test.startTest(); 
        returnedBusSegMap = busMapperObj.getRecordsByIDsAsMap(sBusSegChnlIds);                 
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        System.assertEquals(100,returnedBusSegMap.size());
        // **** TILL HERE ~@~ *****     
    } 
}