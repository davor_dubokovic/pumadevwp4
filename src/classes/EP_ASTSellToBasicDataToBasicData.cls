/*
 *  @Author <Accenture>
 *  @Name <EP_ASTSellToBasicDataToBasicData>
 *  @CreateDate <16/02/2017>
 *  @Description <Handles Account status change from Basic Data setup to Basic Data setup>
 *  @Version <1.0>
 */
 public with sharing class EP_ASTSellToBasicDataToBasicData  extends EP_AccountStateTransition{
    /**
    * @Author <Accenture>
    * @Name EP_ASTSellToBasicDataToBasicData
    **/
    public EP_ASTSellToBasicDataToBasicData() {
        finalState = EP_AccountConstant.BASICDATASETUP;
    }
    /**
    * @Author <Accenture>
    * @Name isTransitionPossible
    **/
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBasicDataToBasicData','isTransitionPossible');
        return super.isTransitionPossible();
    }
    /**
    * @Author <Accenture>
    * @Name isRegisteredForEvent
    **/ 
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBasicDataToBasicData','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

	//L4_45352_Start
    /**
    * @Author <Accenture>
    * @Name isGuardCondition
    **/
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToProspectToBasicDataSetup','isGuardCondition');
        system.debug('Checking Guard conditions for Prospect to Basic Data Setup.......');
        system.debug('## 1. Checking if Company on ProductList is same as that on Sell To......');
        EP_AccountService service = new EP_AccountService(this.account);
        /*If(service.isProductListAttached()){
            if(!service.isCompanySameOnProductList()){
                accountEvent.isError = true;
                accountEvent.setEventMessage(system.label.EP_Company_Mismatch_For_Sell_To_and_ProductList);
                return false;
            }
            system.debug('## 2. Checking if Company on Products is same as that on Sell To.....');
            if(!service.isCompanySameOnProducts()){
                accountEvent.isError = true;
                accountEvent.setEventMessage(system.label.EP_Productlist_has_Product_with_different_Company);
                return  false;
            }
            system.debug('## 3. Checking if Product list is attached to any other customer.....');
            if(service.isProductListLinkedWithDifferentSellTo()){
                accountEvent.isError = true;
                accountEvent.setEventMessage('Product List attached to different company');
                return  false;
            }
            
            system.debug('## 4. Checking if product list attached to sell to has Packaged Product. If Yes, then Req_Packaged_Payment_Term is required .....');
            if(service.isPackagedProductAttached() && !service.isPackagedPaymentTermProvided()){
                accountEvent.isError = true;
                accountEvent.setEventMessage(system.label.EP_Req_Packaged_Payment_Term_Required_If_Product_List_Has_Packaged_Prroduct);
                return  false;
            }
        }
        */
        system.debug('## 1. Checking Recommened Credit is required.....');
        system.debug('service.isRecommendedCreditLimitRequired() = '+service.isRecommendedCreditLimitRequired());
        if(service.isRecommendedCreditLimitRequired()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.label.EP_Recommended_Credit_Limit_Message);
            return  false;
        }
        system.debug('## 2. Checking if supply option is attached.....');
        if(!service.isSupplyOptionAttached()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.label.EP_One_Stock_Holding_Loc_Reqd);
            return  false;
        }
        system.debug('## 3. Checking if bank is attached.....');
        if(service.isBankAccountRequired()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.Label.EP_NOActiveBankAcc);
            return false;   
        }        
        system.debug('## 4. Checking if  if Delivery type is Delivery, then one Ship To with  Account set setup status is required');
        if(EP_AccountConstant.DELIVERY.equalsIgnoreCase(this.account.localAccount.EP_Delivery_Type__c)){
            if(!service.isSetUpShipToAssociated()){
                accountEvent.isError = true;
                accountEvent.setEventMessage(System.Label.EP_No_Set_Up_Ship_To);
                return false;
            }    
        }         
        system.debug('## 5. Checking if Delivery type is Ex rack, then one pickupenebled supply option should be attached To with  Account set setup status is required');
        if(EP_AccountConstant.EXRACK.equalsIgnoreCase(this.account.localAccount.EP_Delivery_Type__c)){
            if(!service.isPickupEnabledSupplyOptionAttached()){
                accountEvent.isError = true;
                accountEvent.setEventMessage(System.Label.EP_PickupEnabledSupplyOption);
                return false;
            }    
        }
        return true;
    }
	//L4_45352_End  
}