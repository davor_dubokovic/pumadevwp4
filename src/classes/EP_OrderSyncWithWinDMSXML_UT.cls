@isTest
private class EP_OrderSyncWithWinDMSXML_UT {
   
   private static final string MESSAGE_TYPE = 'SFDC_TO_NAV_ORDER_SYNC'; 

    @testSetup
    public static void init(){
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
    	Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
    }

   static testMethod void createXML_Test() {
       EP_OrderSyncWithWinDMSXML localObj = new EP_OrderSyncWithWinDMSXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       String result = localObj.createXML();  
       Test.stopTest();
       System.assert(!String.isBlank(result));
   }

   static testMethod void createPayload_Test() {
       EP_OrderSyncWithWinDMSXML localObj = new EP_OrderSyncWithWinDMSXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       localObj.init();
       localObj.createPayload();  
       Test.stopTest();
       System.assert(true, localObj.MSGNode != null);
   }

   static testMethod void getOrderUrgency_PositiveTest() {
       EP_OrderSyncWithWinDMSXML localObj = new EP_OrderSyncWithWinDMSXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       Test.startTest();
       String result = localObj.getOrderUrgency();  
       Test.stopTest();
       System.assert(!String.isBlank(result));
   }

   static testMethod void getOrderUrgency_NegativeTest() {
       EP_OrderSyncWithWinDMSXML localObj = new EP_OrderSyncWithWinDMSXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.OrderObj.EP_Urgent_Order__c = 'Yes';
       Test.startTest();
       String result = localObj.getOrderUrgency();  
       Test.stopTest();
       System.assert(!String.isBlank(result));
   }

   static testMethod void getCreditIndictorValueIf_Test() {
       EP_OrderSyncWithWinDMSXML localObj = new EP_OrderSyncWithWinDMSXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.OrderObj.EP_Exception_Number__c = '12345';
       Test.startTest();
       String result = localObj.getCreditIndictorValue();  
       Test.stopTest();
       System.assert(!String.isBlank(result));
   }

   static testMethod void getCreditIndictorValueElseIF_Test() {
       EP_OrderSyncWithWinDMSXML localObj = new EP_OrderSyncWithWinDMSXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.OrderObj.EP_Payment_Term__c = EP_Common_Constant.PREPAYMENT;
       Test.startTest();
       String result = localObj.getCreditIndictorValue();  
       Test.stopTest();
       System.assert(!String.isBlank(result));
   }

   static testMethod void getCreditIndictorValueElse_Test() {
       EP_OrderSyncWithWinDMSXML localObj = new EP_OrderSyncWithWinDMSXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       Test.startTest();
       String result = localObj.getCreditIndictorValue();  
       Test.stopTest();
       System.assert(!String.isBlank(result));
   }
}