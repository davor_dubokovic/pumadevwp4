/*********************************************************************************************
   @Author <>
   @name <TQUploadResponse >
   @CreateDate <>
   @Description < >  
   @Version <1.0>
*********************************************************************************************/
global with sharing class TQUploadResponse extends TQWebServiceResponse {
    public static final String OPERATIONDELETE = 'DELETE';
    public static final String OPERATIONUPSERT = 'UPSERT';
    
    public List<TQUploadResponseItem> body {get; set;}
/*********************************************************************************************
   @Author <>
   @name <TQUploadResponse >
   @CreateDate <>
   @Description < >  
   @Version <1.0>
*********************************************************************************************/
    public TQUploadResponse(){
        super();
        body = new List<TQUploadResponseItem>();
    }
/*********************************************************************************************
   @Author <>
   @name <TQUploadResponse >
   @CreateDate <>
   @Description < >  
   @Version <1.0>
*********************************************************************************************/
    global with sharing class TQUploadResponseItem
    {
        public String objectApiName{get; set;}  
        //TODO find a way how to put there xsd:anyType
        public sObject record {get; set;}
        public String responseId {get; set;}
        public String requestId {get; set;}
        public String operation {get; set;}
        public Boolean success {get; set;}
        public Boolean executed {get; set;}
        public String changeDate {get; set;}
        public List<String> errorList{get; set;}
/*********************************************************************************************
   @Author <>
   @name <TQUploadResponse >
   @CreateDate <>
   @Description < >  
   @Version <1.0>
*********************************************************************************************/        
        public TQUploadResponseItem(String objectApiName, String requestId){
            this.errorList = new List<String>();
            this.success = true;
            this.objectApiName = objectApiName;
            this.requestId = requestId;
        }
    }
}