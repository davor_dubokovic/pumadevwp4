@isTest
private class EP_CreditExceptionTriggerHandler_UT
{
	@testSetup static void init() {
        List<EP_CS_Communication_Settings__c> lCummunicationSetting = Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_INTEGRATION_CUSTOM_SETTING__c> lIntegrationCustomSetting = Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.sObjectType, 'EP_INTEGRATION_CUSTOM_SETTING_TESTDATA');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
    }

	@isTest
	static void EP_CreditExceptionTriggerHandler_doAfterInsert() {
		EP_Credit_Exception_Request__c creditExp = EP_TestDataUtility.createCreditExceptionRequest();
		EP_CreditExceptionTriggerHandler.doAfterInsert(new map<id,EP_Credit_Exception_Request__c>{creditExp.Id => creditExp});
		List<EP_IntegrationRecord__c> lstIntegrationRecs = [SELECT Id FROM EP_IntegrationRecord__c];
		System.assert(lstIntegrationRecs.size() > 0);
	}

	@isTest
	static void EP_CreditExceptionRequestTrigger_doAfterUpdate() {
		EP_Credit_Exception_Request__c creditExpNew = EP_TestDataUtility.createCreditExceptionRequest();
		creditExpNew.EP_Status__c = EP_OrderConstant.CANCELLED_STATUS;
		Update creditExpNew;
		EP_Credit_Exception_Request__c creditExpOld = EP_TestDataUtility.createCreditExceptionRequest();
		EP_CreditExceptionTriggerHandler.doAfterUpdate(new map<id,EP_Credit_Exception_Request__c>{creditExpNew.Id => creditExpNew},new map<id,EP_Credit_Exception_Request__c> {creditExpOld.Id => creditExpOld});
		List<EP_IntegrationRecord__c> lstIntegrationRecs = [SELECT Id FROM EP_IntegrationRecord__c];
		System.assert(lstIntegrationRecs.size() > 0);
        
       //Approve Credit Exception OrderStatusUpdate Test Case
        Test.startTest();
        
        //Approved Test Case
        creditExpNew.EP_Status__c = EP_Common_Constant.CREDIT_EXC_STATUS_APPROVED;
        Update creditExpNew;
        /*EP_Credit_Exception_Request__c creditExpApprove = EP_TestDataUtility.createCreditExceptionRequest();
        EP_CreditExceptionTriggerHandler.doAfterUpdate(new map<id,EP_Credit_Exception_Request__c>{creditExpNew.Id => creditExpNew},new map<id,EP_Credit_Exception_Request__c> {creditExpApprove.Id => creditExpApprove});*/

        csord__Order__c objOrder = [
                SELECT
                        Id, CE_Approved_Flag__c
                FROM
                        csord__Order__c
                WHERE
                        Id =: creditExpNew.EP_CS_Order__c
                LIMIT 1
        ];
        System.assertEquals(objOrder.CE_Approved_Flag__c, true);
        
        //Rejected Test Case
        creditExpNew.EP_Status__c = EP_Common_Constant.CREDIT_EXC_STATUS_REJECTED;
        Update creditExpNew;
        /*EP_Credit_Exception_Request__c creditExpRejected = EP_TestDataUtility.createCreditExceptionRequest();
        EP_CreditExceptionTriggerHandler.doAfterUpdate(new map<id,EP_Credit_Exception_Request__c>{creditExpNew.Id => creditExpNew},new map<id,EP_Credit_Exception_Request__c> {creditExpRejected.Id => creditExpRejected});*/
        
        csord__Order__c objOrderReject = [
                SELECT
                        Id, csord__Status2__c
                FROM
                        csord__Order__c
                WHERE
                        Id =: creditExpNew.EP_CS_Order__c
                LIMIT 1
        ];
        System.assertEquals(objOrderReject.csord__Status2__c, EP_Common_Constant.ORDER_DRAFT_STATUS);
        
        //Cancelled Test Case
        creditExpNew.EP_Status__c = EP_Common_Constant.CANCELLED_STATUS;
        Update creditExpNew;
        /*EP_Credit_Exception_Request__c creditExpCancelled = EP_TestDataUtility.createCreditExceptionRequest();
        EP_CreditExceptionTriggerHandler.doAfterUpdate(new map<id,EP_Credit_Exception_Request__c>{creditExpNew.Id => creditExpNew},new map<id,EP_Credit_Exception_Request__c> {creditExpCancelled.Id => creditExpCancelled});*/
        
        csord__Order__c objOrderCancel = [
                SELECT
                        Id, csord__Status2__c
                FROM
                        csord__Order__c
                WHERE
                        Id =: creditExpNew.EP_CS_Order__c
                LIMIT 1
        ];
        System.assertEquals(objOrderCancel.csord__Status2__c, EP_Common_Constant.CANCELLED_STATUS);
        
        //Exception Test
        creditExpNew.EP_CS_Order__c = null;
        Update creditExpNew;
        
        Test.stopTest();
	}
}