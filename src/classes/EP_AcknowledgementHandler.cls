/*
*  @Author <Accenture>
*  @Name <EP_AcknowledgementHandler>
*  @CreatedDate <28/02/2017>
*  @Description <This class will be used to handle inbound Acknowledgement and also will be used to create acknowledgement to be sent to external systems>
*  @Version <1.0>
*  @Assumption <Any class will be replaced with AnyO>
*  The method to parse acknowledgement will be used in acknowledgement web service once we finalize that the datasets class will contain array of dataset 
*  even if there is only one dataset in it. 
*/
public class EP_AcknowledgementHandler extends EP_InboundHandler { 
	//Changes for #60147
    @testVisible private static string acknowledgementType = '';
    /*
	*  @Author <Accenture>
	*  @Name <processAcknowledgement>
	*  @CreatedDate <03/01/2017>
	*  @Description <TThis method will deserialize the acknowledgement which will be received when outbound message service is made. 
		This will deserialze the JSON and and will update the integration records>
	*  @Version <1.0>
	*/
    public override string processRequest(string jsonstring){
    	string response='';
        try {
	        EP_GeneralUtility.Log('Public','EP_AcknowledgementHandler','processRequest');
	        EP_IntegrationUtil.isCallout = true;
	        EP_AcknowledgementStub acknowledgement = (EP_AcknowledgementStub) System.JSON.deserialize(jsonstring, EP_AcknowledgementStub.class);
	        List<EP_IntegrationRecord__c> integrationRecordsToUpdate = processIntegrationRecords(acknowledgement); 
	        //Changes for #60147
	        updateIntegrationRecords(integrationRecordsToUpdate, jsonstring);
	        response = EP_Common_Constant.SUCCESS;
        } catch(Exception ex){
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, EP_Common_Constant.COMMON_ACKNOWLEDGEMENT,EP_AcknowledgementHandler.class.getName(), ApexPages.Severity.ERROR);
            response = EP_Common_Constant.EXCEPTION_Response;
        }
        return response;
    }
    
    /* Changes for #60147
	*  @Author <Accenture>
	*  @Name <updateIntegrationRecords>
	*  @CreatedDate <03/01/2017>
	*  @Description <This method will process the integration records and will update the same>
	*  @Version <1.0>
	*/
    @TestVisible
    private void updateIntegrationRecords(List<EP_IntegrationRecord__c> integrationRecordsToUpdate, string jsonstring) {
    	for(EP_IntegrationRecord__c intObj : integrationRecordsToUpdate) {
    		//Start Changes for #59186
    		if(intObj.EP_ParentNode__c == null) {
    			if(EP_Common_Constant.STAGING.equalsIgnoreCase(acknowledgementType) 
    				|| EP_Common_Constant.TRANSPORT_STATUS.equalsIgnoreCase(acknowledgementType)) {
    				//intObj.EP_Response_Message__c = jsonstring;
    				intObj.EP_Staging_Response_Message__c = jsonstring;
    			} else {
    				//intObj.EP_Staging_Response_Message__c = jsonstring;
    				intObj.EP_Response_Message__c = jsonstring;
    			}
    		}
    		//End Changes for #59186
    	}
    	if(!integrationRecordsToUpdate.isEmpty()){
            Database.update(integrationRecordsToUpdate,false);
        }
    }
    
    /*
	*  @Author <Accenture>
	*  @Name <processIntegrationRecords>
	*  @CreatedDate <03/01/2017>
	*  @Description <This method will process the integration records and will update the same>
	*  @Version <1.0>
	*/
    @TestVisible
    private static List<EP_IntegrationRecord__c> processIntegrationRecords(EP_AcknowledgementStub acknowledgement){
        EP_GeneralUtility.Log('Private','EP_AcknowledgementHandler','processIntegrationRecords');
        List<EP_IntegrationRecord__c> integrationRecordsToUpdate = new List<EP_IntegrationRecord__c>();  
        //Start Changes for #59186      
        if(acknowledgement.MSG.Payload.any0 == null) {
        	integrationRecordsToUpdate = processTransportStatus(acknowledgement);
        } else {
        	integrationRecordsToUpdate = processBusinessAcknowledgement(acknowledgement);
        }
        //End Changes for #59186
        return integrationRecordsToUpdate;
    }
    
    /*
	*  @Author <Accenture>
	*  @Name <processTransportStatus>
	*  @CreatedDate <21/08/2017>
	*  @Description <This method will use to Set Transport Status from Acknowledgement Header - Changes for #59186>
	*  @Version <1.0>
	*/
    @TestVisible
    private static List<EP_IntegrationRecord__c> processTransportStatus(EP_AcknowledgementStub acknowledgement) {
        EP_MessageHeader HeaderCommon = acknowledgement.MSG.HeaderCommon;
    	EP_IntegrationRecordMapper intMapper = new EP_IntegrationRecordMapper();
        List<EP_IntegrationRecord__c> integrationRecordsToUpdate = intMapper.getRecordsByMessageIdOrSeqId(HeaderCommon.MsgID,new set<string>());
    	string errorDesc = '';
    	acknowledgementType = EP_Common_Constant.TRANSPORT_STATUS;
    	for(EP_IntegrationRecord__c intRec : integrationRecordsToUpdate) {
    		intRec.EP_Transport_Status__c = HeaderCommon.TransportStatus;
    		errorDesc = HeaderCommon.ErrorDescription;
    		if(errorDesc != null && errorDesc.length() > 250){
                errorDesc = errorDesc.subString(0,250);
            }
    		intRec.EP_Error_Description__c  = errorDesc;
    		intRec.EP_Integration_Error_Type__c = '';
    		if(String.isNotBlank(intRec.EP_Error_Description__c )) {
    			intRec.EP_Integration_Error_Type__c = EP_Common_Constant.MIDDLEWARE_TRANS_ERROR;
    			intRec.EP_Status__c = EP_Common_Constant.ERROR_SENT_STATUS;
    		}
    	}
    	return integrationRecordsToUpdate;
    }
    
    /*
	*  @Author <Accenture>
	*  @Name <processBusinessAcknowledgement>
	*  @CreatedDate <21/08/2017>
	*  @Description <This method will use to process Business Responses- Changes for #59186>
	*  @Version <1.0>
	*/
    @TestVisible
    private static List<EP_IntegrationRecord__c> processBusinessAcknowledgement(EP_AcknowledgementStub acknowledgement){
    	List<EP_IntegrationRecord__c> integrationRecordsToUpdate = new List<EP_IntegrationRecord__c>();
        EP_MessageHeader HeaderCommon = acknowledgement.MSG.HeaderCommon;
    	EP_AcknowledgementStub.acknowledgement objAcknowledgement= acknowledgement.MSG.Payload.any0.acknowledgement;
        string finalErrorDescription ='';
        string ackType ='';
        if(objAcknowledgement.ackType!=null){
            ackType = objAcknowledgement.ackType;
            //Changes for #60147
            acknowledgementType = ackType;
        }
        Map<string,EP_AcknowledgementStub.dataset> mapDataset = new Map<string,EP_AcknowledgementStub.dataset>();
        mapDataset = fillDatasetMap(objAcknowledgement.dataSets.dataset,ackType);
        
        if(EP_PROCESS_NAME_CS__c.getValues(EP_COMMON_CONSTANT.LS_Processes).EP_Process_Name__c.contains(string.isblank(HeaderCommon.CorrelationID) ? EP_Common_Constant.BLANK : HeaderCommon.CorrelationID.Substring(8,11)) && EP_Common_Constant.SUCCESS.equalsIgnoreCase(HeaderCommon.ProcessStatus)){
            integrationRecordsToUpdate = getIntegrationRecords(HeaderCommon,EP_Common_Constant.SYNC_STATUS,EP_Common_Constant.BLANK,mapDataset,true);
        }
        else if(EP_Common_Constant.FAILURE.equalsIgnoreCase(HeaderCommon.ProcessStatus) || EP_Common_Constant.FAILURE.equalsIgnoreCase(HeaderCommon.TransportStatus) || !string.isblank(HeaderCommon.ErrorCode)){
        	//Error description comes in dataset in WINDMS case
        	string errorDesc = (objAcknowledgement.dataSets.dataset!=null ? objAcknowledgement.dataSets.dataset[0].errorDescription : EP_Common_Constant.BLANK);
        	finalErrorDescription = getErrorDescription(HeaderCommon.ProcessStatus,HeaderCommon.TransportStatus,HeaderCommon.ErrorCode,string.isNotBlank(HeaderCommon.ErrorDescription) ? HeaderCommon.ErrorDescription.left(250) : errorDesc);
        	integrationRecordsToUpdate = getIntegrationRecords(HeaderCommon,EP_Common_Constant.STAGING.equalsIgnoreCase(ackType) ? EP_Common_Constant.ERR_ACKNWLDGD : EP_Common_Constant.ERROR_SYNC_STATUS,finalErrorDescription,mapDataset,true);
        }
        else {
            integrationRecordsToUpdate = getIntegrationRecords(HeaderCommon,EP_Common_Constant.BLANK,EP_Common_Constant.BLANK,mapDataset,false);
        }
        return integrationRecordsToUpdate;
    }
    
    /*
	*  @Author <Accenture>
	*  @Name <fillDatasetMap>
	*  @CreatedDate <03/01/2017>
	*  @Description <This method will fill the dataset based on the acknowledgement type>
	*  @Version <1.0>
	*/
    @TestVisible
    private static Map<string,EP_AcknowledgementStub.dataset> fillDatasetMap(List<EP_AcknowledgementStub.dataset> pDataset, string ackType){
        EP_GeneralUtility.Log('Private','EP_AcknowledgementHandler','fillDatasetMap');
        Map<string,EP_AcknowledgementStub.dataset> mapDataset = new Map<string,EP_AcknowledgementStub.dataset>();
        if(pDataset!=null){
            for(EP_AcknowledgementStub.dataset lDataset : pDataset){
            	if(string.isNotBlank(lDataset.seqId)){
	                if(EP_Common_Constant.PROCESSING.equalsIgnoreCase(ackType) && string.isBlank(lDataset.errorDescription)){
	                    lDataset.finalStatus= EP_Common_Constant.SYNC_STATUS;
	                } else if(EP_Common_Constant.PROCESSING.equalsIgnoreCase(ackType) && !string.isBlank(lDataset.errorDescription)){
	                    lDataset.finalStatus= EP_Common_Constant.ERROR_SYNC_STATUS;
	                } else if(EP_Common_Constant.STAGING.equalsIgnoreCase(ackType) && string.isBlank(lDataset.errorDescription)){
	                    lDataset.finalStatus= EP_Common_Constant.ACKNWLDGD;
	                } else if(EP_Common_Constant.STAGING.equalsIgnoreCase(ackType) && !string.isBlank(lDataset.errorDescription)){
	                    lDataset.finalStatus= EP_Common_Constant.ERR_ACKNWLDGD;
	                }
	                mapDataset.put(lDataset.seqId,lDataset);
            	}
            }
        }
        return mapDataset;
    }
    
    /*
	*  @Author <Accenture>
	*  @Name <getErrorDescription>
	*  @CreatedDate <03/01/2017>
	*  @Description <This method will get the error description in the case of failure>
	*  @Version <1.0>
	*/
    @TestVisible
    private static string getErrorDescription(string processStatus, string transportStatus, string errorCode, string errorDescription){
    	EP_GeneralUtility.Log('Private','EP_AcknowledgementHandler','getErrorDescription');
    	string finalErrorDesciption='';
    	if(EP_Common_Constant.FAILURE.equalsIgnoreCase(processStatus)){
    		finalErrorDesciption = string.isBlank(errorDescription) ?  EP_Common_Constant.PROCESS_STATUS : errorDescription;
    	} else if (EP_Common_Constant.FAILURE.equalsIgnoreCase(TransportStatus)){
    		finalErrorDesciption = string.isBlank(errorDescription) ?  EP_Common_Constant.TRANSPORT_STATUS : errorDescription;
    	} else if(!string.isblank(ErrorCode)){
    		finalErrorDesciption = errorDescription;
    	}
    	return finalErrorDesciption;
    }
    
    /*
	*  @Author <Accenture>
	*  @Name <getIntegrationRecords>
	*  @CreatedDate <03/01/2017>
	*  @Description <This method will get the integration records from the database>
	*  @Version <1.0>
	*/
    @TestVisible
    private static List<EP_IntegrationRecord__c> getIntegrationRecords(EP_MessageHeader HeaderCommon, string pStatus,string pErrorDesciption,Map<string,EP_AcknowledgementStub.dataset> lmapDataset, boolean isFailure){
        EP_GeneralUtility.Log('Private','EP_AcknowledgementHandler','getIntegrationRecords');
        List<EP_IntegrationRecord__c> integrationRecordsToUpdate = new List<EP_IntegrationRecord__c>();
        string pMessageId = HeaderCommon.CorrelationID;
        EP_IntegrationRecordMapper intMapper = new EP_IntegrationRecordMapper();
        integrationRecordsToUpdate = intMapper.getRecordsByMessageIdOrSeqId(pMessageId,lmapDataset.keyset());
        
        for(EP_IntegrationRecord__c integrationRecord : integrationRecordsToUpdate){
        	//DO Not process and update the record if Already got the processing Ack i.e. Synced or Error_Synced because sometime we are getting stagning response after processing response from NAV
            if(EP_Common_Constant.SYNC_STATUS.equalsIgnoreCase(integrationRecord.EP_Status__c) || EP_Common_Constant.ERROR_SYNC_STATUS.equalsIgnoreCase(integrationRecord.EP_Status__c)) continue;
            integrationRecord.EP_Status__c = pStatus;
            integrationRecord.EP_Transport_Status__c = HeaderCommon.TransportStatus;
            integrationRecord.EP_Error_Description__c = pErrorDesciption;
            if(lmapDataset!=null && lmapDataset.containsKey(integrationRecord.EP_SeqId__c)){
                EP_AcknowledgementStub.dataset lRecord = lmapDataset.get(integrationRecord.EP_SeqId__c);
                integrationRecord.EP_Status__c = (isFailure) ? pStatus : lRecord.finalStatus;
                if(string.isNotBlank(lRecord.errorDescription) && (EP_Common_Constant.PROCESS_STATUS.equalsIgnoreCase(pErrorDesciption) || string.isBlank(pErrorDesciption)) ) {
                	integrationRecord.EP_Error_Description__c = lRecord.errorDescription;
                }
                //integrationRecord.EP_Error_Description__c = string.isBlank(integrationRecord.EP_Error_Description__c) ? lRecord.errorDescription : integrationRecord.EP_Error_Description__c;
            }
        }
        return integrationRecordsToUpdate;
    }
    
    //**************************************************************** Create Acknowledgement starts ****************************************************************************************
    
    /*
	*  @Author <Accenture>
	*  @Name <createAcknowledgement>
	*  @CreatedDate <03/01/2017>
	*  @Description <This method will create the Acknowledgement which needs to be sent across for all inbound messages>
	*  @Version <1.0>
	*/
    public static string createAcknowledgement(string messageType, Boolean processingFailed, string failureReason,EP_MessageHeader headerCommon, List<EP_AcknowledgementStub.dataset> listDataset){
        EP_GeneralUtility.Log('Public','EP_AcknowledgementHandler','createAcknowledgement');
        string jsonToSend = '';
        try{
            //Assign the header tag to the Ackowledgement which will be prepared
            EP_AcknowledgementStub.MSG acknowledgement = new EP_AcknowledgementStub.MSG();
            acknowledgement.HeaderCommon = headerCommon;
            acknowledgement.HeaderCommon.CorrelationID =  headerCommon.MsgID;
            
            //generate message id by using the message type parameter
            String sMsgID = generateMessageId(messageType);
            //set the MessageId in the header tag
            acknowledgement.HeaderCommon.MsgID=  sMsgID;
            
            //check if there are any failures in the datatset
            boolean partialSuccess = checkForPartialSuccess(listDataset);
            
            //failure case
            if(processingFailed){
                acknowledgement.HeaderCommon.ProcessStatus=  EP_Common_Constant.FAILURE;
                acknowledgement.HeaderCommon.ErrorDescription=  failureReason;
                acknowledgement.Payload.any0.acknowledgement.dataSets.dataset = listDataset;
            } else if(partialSuccess){
                acknowledgement.HeaderCommon.ProcessStatus=  EP_Common_Constant.FAILURE;
                acknowledgement.Payload.any0.acknowledgement.dataSets.dataset = listDataset;
            } else {
                acknowledgement.HeaderCommon.ProcessStatus=EP_Common_Constant.SUCCESS;
                acknowledgement.Payload.any0.acknowledgement.dataSets.dataset = listDataset;
            }
            jsonToSend = JSON.serialize(acknowledgement);
        } catch(Exception ex){
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, EP_Common_Constant.COMMON_ACKNOWLEDGEMENT, EP_AcknowledgementHandler.class.getName(), ApexPages.Severity.ERROR);
        }
        return jsonToSend;
    }
    
    /*
	*  @Author <Accenture>
	*  @Name <generateMessageId>
	*  @CreatedDate <03/01/2017>
	*  @Description <This method will generate the message id for the acknowledgement to be created>
	*  @Version <1.0>
	*/
    @TestVisible
    private static string generateMessageId(string messageType){
        EP_GeneralUtility.Log('Private','EP_AcknowledgementHandler','generateMessageId');
        String strMsgGeneratorName = EP_GeneralUtility.createMessageGeneratorRecord();
        //get the values from the custom setting for the interface for which acknowledgement has to be created and generate message Id
        EP_CS_InboundMessageSetting__c msgSetting = EP_CS_InboundMessageSetting__c.getValues(messageType);
        return EP_IntegrationUtil.getMessageId(msgSetting.Source__c, msgSetting.Location__c,msgSetting.ProcessName__c, System.NOW(), strMsgGeneratorName);
    }
    
    /*
	*  @Author <Accenture>
	*  @Name <checkForPartialSuccess>
	*  @CreatedDate <03/01/2017>
	*  @Description <This method will check in the dataset if there is any failure in it, and if yes, then it sends the boolean as true.>
	*  @Version <1.0>
	*/
    @TestVisible
    private static boolean checkForPartialSuccess(List<EP_AcknowledgementStub.dataset> listDataset){
        EP_GeneralUtility.Log('Private','EP_AcknowledgementHandler','checkForPartialSuccess');
        boolean partialSuccess = false;
        for(EP_AcknowledgementStub.dataset dataset : listDataset){
            if(!string.isBlank(dataset.errorDescription)){
                partialSuccess = true;
                break;
            }
        }
        return partialSuccess;
    }
}