@isTest
public class EP_ASTSellToBasicDataToBasicData_UT
{
    static final string EVENT_NAME = '02-BasicDataSetupTo02-BasicDataSetup';
    static final string INVALID_EVENT_NAME = '08-ProspectTo02-Basic Data Setup';
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    static testMethod void isTransitionPossible_positive_test() {
        //EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        Account acc =  EP_TestDataUtility.getSellToASProspectDomainObjectPositiveScenario().localAccount;
        EP_Payment_Term__c payTerm = EP_AccountTestDataUtility.getCreditPaymentTerm();
        acc.EP_Requested_Packaged_Payment_Terms__c = payTerm.id;
        acc.EP_RequestedPaymentTerms__c = payTerm.id;
        acc.EP_Recommended_Credit_Limit__c = 5000000;
        update acc;
        EP_AccountDomainObject obj = new EP_AccountDomainObject(acc.Id);
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToBasicDataToBasicData ast = new EP_ASTSellToBasicDataToBasicData();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isTransitionPossible_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME );
        EP_ASTSellToBasicDataToBasicData ast = new EP_ASTSellToBasicDataToBasicData();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isRegisteredForEvent_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToBasicDataToBasicData ast = new EP_ASTSellToBasicDataToBasicData();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isRegisteredForEvent_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME );
        EP_ASTSellToBasicDataToBasicData ast = new EP_ASTSellToBasicDataToBasicData();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isGuardCondition_Step0_test() {
        Account acc =  EP_TestDataUtility.getSellToASProspectDomainObjectPositiveScenario().localAccount;
        EP_Payment_Term__c payTerm = EP_AccountTestDataUtility.getCreditPaymentTerm();
        acc.EP_Requested_Packaged_Payment_Terms__c = payTerm.id;
        acc.EP_RequestedPaymentTerms__c = payTerm.id;
        update acc;
        EP_AccountDomainObject obj = new EP_AccountDomainObject(acc.Id);
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToBasicDataToBasicData ast = new EP_ASTSellToBasicDataToBasicData();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result); 
    }
    static testMethod void isGuardCondition_Step1_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        Account acc = obj.localAccount;
        EP_Payment_Term__c payTerm = EP_AccountTestDataUtility.getCreditPaymentTerm();
        acc.EP_Delivery_Type__c = EP_AccountConstant.EXRACK;
        acc.EP_Requested_Packaged_Payment_Terms__c = payTerm.id;
        acc.EP_RequestedPaymentTerms__c = payTerm.id;
        acc.EP_Recommended_Credit_Limit__c = 5000000;
        update acc;
        EP_Stock_Holding_Location__C sch = [select id,EP_Is_Pickup_Enabled__c from EP_Stock_Holding_Location__C Where EP_Sell_To__c =: acc.Id Limit 1];
        sch.EP_Is_Pickup_Enabled__c = false;
        update sch;
        EP_AccountDomainObject obj2 = new EP_AccountDomainObject(acc.Id);
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToBasicDataToBasicData ast = new EP_ASTSellToBasicDataToBasicData();
        ast.setAccountContext(obj2,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isGuardCondition_Step2_test() {
        EP_AccountDomainObject obj1 = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        Account acc = EP_TestDataUtility.createSellToAccountWithDirectPaymentSelected();        
        EP_Payment_Term__c payTerm = EP_AccountTestDataUtility.getCreditPaymentTerm();
        acc.EP_Delivery_Type__c = EP_AccountConstant.DELIVERY;
        acc.EP_Requested_Packaged_Payment_Terms__c = payTerm.id;
        acc.EP_RequestedPaymentTerms__c = payTerm.id;
        acc.EP_Recommended_Credit_Limit__c = 5000000;
        update acc;
        EP_AccountDomainObject obj2 = new EP_AccountDomainObject(acc.Id);
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToBasicDataToBasicData ast = new EP_ASTSellToBasicDataToBasicData();
        ast.setAccountContext(obj2,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isGuardCondition_Step4_test() {
        Account acc =  EP_TestDataUtility.getSellToPositiveScenario();        
        EP_Stock_Holding_Location__C sch = [select id,EP_Is_Pickup_Enabled__c from EP_Stock_Holding_Location__C Where EP_Sell_To__c =: acc.Id Limit 1];
        delete sch;
        EP_AccountDomainObject obj = new EP_AccountDomainObject(acc.Id);
        system.debug('====EP_Requested_Packaged_Payment_Terms__c==='+obj.localAccount.EP_Requested_Packaged_Payment_Terms__r.name);
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToBasicDataToBasicData ast = new EP_ASTSellToBasicDataToBasicData();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isGuardCondition_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME );
        EP_ASTSellToBasicDataToBasicData ast = new EP_ASTSellToBasicDataToBasicData();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
}