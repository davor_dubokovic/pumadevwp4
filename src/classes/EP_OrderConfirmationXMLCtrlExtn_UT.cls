@isTest
public class EP_OrderConfirmationXMLCtrlExtn_UT
{
    static testMethod void getCompanyDetails_test() {
        csord__Order__c ord = [SELECT Id, EP_Puma_Company_Code__c FROM csord__Order__c WHERE Id =:EP_TestDataUtility.getConsumptionOrderPositiveScenario().id];
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        Company__c result = localObj.getCompanyDetails();
        Test.stopTest();
        Company__c company = [select id,EP_Company_Code__c from Company__c where id=: result.Id];
        System.AssertEquals(true,result != NULL && company.EP_Company_Code__c.Equals(ord.EP_Puma_Company_Code__c));
    }
    static testMethod void setOrderDetails_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        localObj.setOrderDetails();
        Test.stopTest();
        System.AssertEquals(true,localObj.docTitle.equalsIgnoreCase(EP_OrderConfirmationConstant.ORDER_QUOTE_TITLE));
    }
    static testMethod void setRelatedAccountDetails_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        localObj.setRelatedAccountDetails();
        Test.stopTest();
        System.AssertEquals(true,localObj.relatedAccountsMap.size() > 0); 
    }
    static testMethod void getStorageLocAcct_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        //localObj.setRelatedAccountDetails();
        Test.startTest();
        Account result = localObj.getStorageLocAcct();
        Test.stopTest();
        EP_OrderMapper ordMapObj = new EP_OrderMapper();
        list<csord__Order__c> ordList = ordMapObj.getCSRecordsByIds(new set<id> {ord.Id});
        System.AssertEquals(true,ordList.get(0).Stock_Holding_Location__r.Stock_Holding_Location__c.equals(result.Id));
    }
    static testMethod void getShipToAcct_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        Account result = localObj.getShipToAcct();
        Test.stopTest();
        EP_OrderMapper ordMapObj = new EP_OrderMapper();
        list<Order> ordList = ordMapObj.getRecordsByIds(new set<id> {ord.Id});
        System.AssertEquals(true,ordList.get(0).EP_ShipTo__c.equals(result.Id));
    }
    static testMethod void getTransporterAcct_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        Account vendor = EP_TestDataUtility.getVendor();
        ord.EP_Transporter__c= vendor.Id;
        update ord;
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        localObj.setRelatedAccountDetails();
        Test.startTest();
        Account result = localObj.getTransporterAcct();
        Test.stopTest();
        //EP_OrderMapper ordMapObj = new EP_OrderMapper();
        //set<Id> setOrderIds = new set<Id>();
        //setOrderIds.add(ord.Id);
        //list<Order> ordList = ordMapObj.getRecordsByIds(setOrderIds);
        system.debug('ordList==================' + ord);
        System.AssertEquals(true,ord.EP_Transporter__c.equals(result.Id));
    }
    static testMethod void getOrderLineItems_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        LIST<csord__Order_Line_Item__c> result = localObj.getOrderLineItems();
        Test.stopTest();
        System.AssertEquals(true,!result.isEmpty());
    }
    static testMethod void getSellToAcct_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        Account result = localObj.getSellToAcct();
        Test.stopTest();
        EP_OrderMapper ordMapObj = new EP_OrderMapper();
        list<csord__Order__c> ordList = ordMapObj.getCSRecordsByOrderNumber(new set<String> {ord.orderNumber__c});
        System.AssertEquals(true,ordList.get(0).EP_Sell_To__c.equals(result.Id));
    }
    static testMethod void setDocTitle_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        localObj.setDocTitle();
        Test.stopTest();
        System.AssertEquals(true,localObj.docTitle.equalsIgnoreCase(EP_OrderConfirmationConstant.ORDER_QUOTE_TITLE));
    }
    static testMethod void checkPageAccess_WithoutSecretCode() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        PageReference result = localObj.checkPageAccess();
        Test.stopTest();
        System.AssertEquals(true,result != NULL);
        System.AssertEquals(true,result.getURL().toUpperCase().contains(EP_Common_Constant.UNAUTHORIZEDACCESSPAGESTR.toUpperCase()));
    }
    static testMethod void checkPageAccess_WithSecretCode() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        PageReference result = localObj.checkPageAccess();
        Test.stopTest();
        System.AssertEquals(true,result == NULL);
    }
}