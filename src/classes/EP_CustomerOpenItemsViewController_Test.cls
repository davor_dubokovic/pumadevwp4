/* 
   @Author <Vinay Jaiswal>
   @name <EP_CustomerOpenItemsViewController_Test>
   @CreateDate <07/10/2016>
   @Description <This Class used to increased the code coverage of EP_CustomerOpenItemsViewController class>
   @Version <1.0>
*/
@isTest
private class EP_CustomerOpenItemsViewController_Test {
    
    private static Profile admin = [Select id from Profile
                                    Where Name =: EP_Common_Constant.ADMIN_PROFILE
                                    limit :EP_Common_Constant.ONE];
    private static User adminUser = EP_TestDataUtility.createUser(admin.id);
    private static EP_Customer_Account_Statement_Item__c oCASItem ,oCASItem1;
    private static EP_Invoice__c oInvoice;
    private static EP_Customer_Credit_Memo__c oCreditMemo;
    private static Account billToAccount, sellToAccount;
    private static Id pricebookId = Test.getStandardPricebookId();
    private static EP_CustomerOpenItemsViewController oCustOpenItemCtrl;
    private static Id shpToRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,
                                                                           EP_Common_Constant.NON_VMI_SHIP_TO);
    private static Id recordTypeCreditMemoId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMERACCOUNTSTATEMENTITEM_OBJECT, 
                                                                       EP_Common_Constant.CREDITMEMO_RECORD_TYPE_NAME);
    private static Id recordTypeCustomerInvoice = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMERACCOUNTSTATEMENTITEM_OBJECT, 
                                                                       EP_Common_Constant.Customer_Invoice);
    private static EP_Freight_Matrix__c freightMatrix;
    private static string strOrderNumber;
    private static Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();

    private static final String sCreditMemo = 'Credit Memo';
    private static final String sCustomerInvoice = 'Customer Invoice';
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        System.runAs(adminUser){
            Test.startTest();
                createTestData();
            Test.stopTest();

        }
        System.currentPagereference().getParameters().put('id', sellToAccount.Id);
        System.assert(System.currentPagereference().getParameters().get('id') == sellToAccount.Id); 
        
        oCustOpenItemCtrl = new EP_CustomerOpenItemsViewController();
             
    }
    
     /**
     * @author <Vinay Jaiswal>
     * @name <createTestData>
     * @date <21/06/2016>
     * @description <This method is used create test data for multiple Invoice>
     * @version <1.0>
     */
   static void createTestData() {
        Product2  productObj = new product2(Name = 'Diesel', CurrencyIsoCode = 'GBP', isActive = TRUE,
            family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true, ProductCode='30005');
        Product2  taxProd= new product2(Name = 'TaxProduct', CurrencyIsoCode = 'GBP', isActive = TRUE);                           
        List<Product2> prodList = new List<Product2>{productObj,taxProd};                   
        Database.insert(prodList);
        EP_Country__c contry1 = EP_TestDataUtility.createCountryRecord('India','IN', 'Asia');
        Database.insert(contry1);
        EP_Region__c reg ;
        reg = EP_TestDataUtility.createRegion('East',contry1.id );
        database.insert(reg); 
        List<PricebookEntry> pbEntryList = new List<PricebookEntry>();
       // Id pricebookId = Test.getStandardPricebookId();
        Pricebook2 customPriceBook1 = new Pricebook2(
            Name = 'customPricebook1', IsActive = true
        );
        insert customPriceBook1;
        PricebookEntry standardPrice = new PricebookEntry(
        Pricebook2Id = customPriceBook1.Id, Product2Id = productObj.Id,
        UnitPrice = 10000, IsActive = false);
        
        pbEntryList.add(standardPrice);
        PricebookEntry taxPrice = new PricebookEntry(
        Pricebook2Id = customPriceBook1.Id, Product2Id = taxProd.Id,
        UnitPrice = 10000, IsActive = false);
        pbEntryList.add(taxPrice);
        
        Database.insert(pbEntryList);
        
        billToAccount = EP_TestDataUtility.createBillToAccount();
        billToAccount.EP_Delivery_Pickup_Country__c = contry1.id;
        billToAccount.EP_Cntry_Region__c = reg.id;
        billToAccount.EP_Country__c = contry1.id;
        Database.insert(billToAccount);
        billToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(billToAccount);
        billToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        billToAccount.AccountNumber = '10000';
        Database.update(billToAccount);     
        // create sell to Account
        sellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
        sellToAccount.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
        sellToAccount.EP_Bill_To_Account__c = billToAccount.id;
        sellToAccount.AccountNumber = '20000';
        sellToAccount.EP_Delivery_Pickup_Country__c = contry1.id;
        sellToAccount.EP_Cntry_Region__c = reg.id;
        sellToAccount.EP_Country__c = contry1.id;
        /*CAM 2.7 start*/
        sellToAccount.EP_AvailableFunds__c = 40000000;
        /*CAM 2.7 end*/
        Database.insert(sellToAccount);
        EP_Stock_holding_location__c SHL1 = EP_TestDataUtility.createSellToStockLocation(sellToAccount.id,true,null,null); //Add one SHL
        Database.insert(SHL1,false);
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Sell TO status
        Database.update(sellToAccount);
        freightMatrix = EP_TestDataUtility.createFreightMatrix();  
        Database.insert(freightMatrix);
          // create non vmi ship to account
        Account nonVmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, shpToRecordTypeId );
        nonVmiShipToAccount.EP_Freight_Matrix__c = freightMatrix.id;
        nonVmiShipToAccount.EP_Delivery_Type__c =  EP_Common_Constant.DELIVERY;
        nonVmiShipToAccount.AccountNumber = '30000';
        nonVmiShipToAccount.EP_PriceBook__c = pricebookId;
        nonVmiShipToAccount.EP_Ship_To_Type__c = 'Non-Consignment';
        nonVmiShipToAccount.EP_Transportation_Management__c = 'Own';
        Database.insert(nonVmiShipToAccount);
        
        Account storageLocAccPrimary = EP_TestDataUtility.createStockHoldingLocation();
        storageLocAccPrimary.EP_Nav_Stock_Location_Id__c = '41001';
        Database.insert(storageLocAccPrimary);
        
        ID sellToSHLRTID= EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c', 'Sell-To Supply Location');
        
        EP_Stock_Holding_Location__c primaryStockHolding = EP_TestDataUtility.createSellToStockLocation(sellToAccount.id,true,null,sellToSHLRTID); //Add one SHL
        Database.insert(primaryStockHolding);
        
        /*
        EP_Stock_Holding_Location__c primaryStockHolding = new EP_Stock_Holding_Location__c(
            EP_Ship_To__c=nonVmiShipToAccount.id,Stock_Holding_Location__c=storageLocAccPrimary.id);
        Database.insert(primaryStockHolding);
        */  
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(sellToAccount);
        
        List<EP_Tank__c> tanksToBeInsertedList = new List<EP_Tank__c>();
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(nonVmiShipToAccount.Id,productObj.Id);
        tanksToBeInsertedList.add(tankObj);        
        Database.insert(tanksToBeInsertedList);
        
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(nonVmiShipToAccount);
        // Make non Vmi account active
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(nonVmiShipToAccount);
        
        Order objOrder = EP_TestDataUtility.createOrder(sellToAccount.Id, orderRecTypeId, pricebookId);
        objOrder.Status = 'Draft';

        Database.insert(objOrder);
        objOrder.Status = 'Planning';
        
        Database.update(objOrder);
        
        Order oOrder = [select Id, Name, Status,  OrderNumber, EP_Integration_Status__c, EP_WinDMS_Order_Number__c,
                             EP_SeqId__c, EP_WinDMS_Status__c, AccountID  from Order where id = : objOrder.id]; 
                   
        strOrderNumber = string.valueOf(oOrder.OrderNumber);
        EP_Payment_Term__c paymentTerm = new EP_Payment_Term__c(Name='PrePayment',EP_Payment_Term_Code__c='PP');
        Database.insert(paymentTerm);
        
        oInvoice = EP_TestDataUtility.createInvoice(sellToAccount);
        oInvoice.EP_Invoice_Issue_Date__c = System.Today() - 10;
        oInvoice.EP_Invoice_Due_Date__c = System.Today() + 10;
        oInvoice.EP_Payment_Term__c = paymentTerm.Id;
        oInvoice.Name = 'XXXXX';
        oInvoice.EP_Remaining_Balance__c = 10.0;
        oInvoice.EP_Invoice_Pull_Status__c = EP_Common_Constant.COMPLETED; //L4# 78534 - Test Class Fix
        Database.insert(oInvoice);
        
        oCreditMemo = new EP_Customer_Credit_Memo__c(EP_Bill_To__c = sellToAccount.Id, EP_Credit_Memo_Issue_Date__c = System.Today() - 9,
                EP_Credit_Memo_Pull_Status__c = 'Completed', EP_Credit_Memo_Remaining_Balance__c = 10.0, EP_Credit_Memo_Total__c = 10.0,
                CurrencyIsoCode = 'GBP', EP_Credit_Memo_Number__c = '1000' );
        Database.insert(oCreditMemo);        
                     
        oCASItem  = EP_TestDataUtility.createCASItemForSearch(sellToAccount.Id, sCustomerInvoice);
        oCASItem.EP_Customer_Invoice__c = oInvoice.Id;
        //L4# 78534 - Test Class Fix - Start
        oCASItem.EP_Entry_Number__c = oInvoice.Id; 
        oCASItem.EP_Document_Type__c = EP_Common_Constant.DOCUMENTTYPE_INVOICE; 
        //L4# 78534 - Test Class Fix - End 
        Database.insert(oCASItem);
        
        oCASItem1  = EP_TestDataUtility.createCASItemForSearch(sellToAccount.Id, sCreditMemo);
        oCASItem1.EP_Customer_Credit_Memo__c = oCreditMemo.Id;
        //L4# 78534 - Test Class Fix - Start 
        oCASItem1.EP_Entry_Number__c = oCreditMemo.Id; 
        oCASItem1.EP_Document_Type__c = EP_Common_Constant.DOCUMENTTYPE_CREDITMEMO;
        //L4# 78534 - Test Class Fix - End 
        Database.insert(oCASItem1);
        
            
    }
}