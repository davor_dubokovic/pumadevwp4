/*
*  @Author <Accenture>
*  @Name <EP_BusinessHoursMapper_UT>
*  @CreateDate <08/01/2018>
*  @Description <Test class for EP_BusinessHoursMapper class>
*  @Version <1.0>
*/

@isTest
public class EP_BusinessHoursMapper_UT{    

   static testMethod void getRecordsByIds_test(){
     
        BusinessHours stdBusinessHours = [select id from BusinessHours where isdefault= true];
        Test.startTest();
        BusinessHours obj = EP_BusinessHoursMapper.getRecordsByIds(stdBusinessHours.id);
        Test.stopTest();
        System.assertNotEquals(obj, Null);
        
    }
}