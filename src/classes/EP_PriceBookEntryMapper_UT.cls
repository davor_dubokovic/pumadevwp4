@isTest
public class EP_PriceBookEntryMapper_UT{

    private final static String objName = 'ABP';
    static testMethod void getActiveRecordsByPriceBookId_test() {
        EP_PriceBookEntryMapper localObj = new EP_PriceBookEntryMapper();
        Account accountObj = EP_TestDataUtility.getSellTo();
        Id priceBookId = [SELECT Id FROM Pricebook2 LIMIT 1].id;
        Test.startTest();
        List<PriceBookEntry> result = localObj.getActiveRecordsByPriceBookId(priceBookId);
        Test.stopTest();
        System.Assert(result.size() > 0);
    }

    static testMethod void getRecordsByPricebookId_test() {
        EP_PriceBookEntryMapper localObj = new EP_PriceBookEntryMapper();
        Account accountObj = EP_TestDataUtility.getSellTo();
        Id priceBookId = [SELECT Id FROM Pricebook2 LIMIT 1].id;
        Set<Id> setPriceBook = new Set<ID>{priceBookId};
        Test.startTest();
        List<PriceBookEntry> result = localObj.getRecordsByPricebookIds(setPriceBook);
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    
    static testMethod void getRecordsByFilterSet_1_test() {
        EP_PriceBookEntryMapper localObj = new EP_PriceBookEntryMapper();
        Account accountObj = EP_TestDataUtility.getSellTo();
        Account tempAccount = EP_TestDataUtility.createSellToAccountAndPriceBook();            
        //Pricebook2 priceBookIObj = [SELECT Id ,CurrencyIsoCode FROM Pricebook2 where ID=:accountObj.EP_PriceBook__c];
        Set<Id> setPriceBook = new Set<ID>{tempAccount.EP_PriceBook__c};
        Set<String> setCurrencyIsoCode = new Set<String>{tempAccount.CurrencyIsoCode };
        Set<String> setProductSoldAs = new Set<String>{EP_Common_Constant.BULK_ORDER_PRODUCT_CAT};
        Set<Id> availableProductIds = new Set<Id>();
        Order orderObj =  new Order();
        csord__Order__c csOrder = new csord__Order__c();
        Test.startTest();
        List<PriceBookEntry> result = localObj.getRecordsByFilterSet_1(setPriceBook,setCurrencyIsoCode,setProductSoldAs,true);
        list<PriceBookEntry> lstPriceBookEntry = localObj.getPriceBookEntriesforExRack(availableProductIds,tempAccount.EP_PriceBook__c,orderObj);
        list<PriceBookEntry> lstPriceBookEntryv1 = localObj.getCsPriceBookEntriesforExRack(availableProductIds,tempAccount.EP_PriceBook__c,csOrder);
        Test.stopTest();
        System.Assert(result.size() >= 0);
    }

    static testMethod void getRecordsByPricebookEntry_test() {
        EP_PriceBookEntryMapper localObj = new EP_PriceBookEntryMapper();
        Account accountObj = EP_TestDataUtility.getSellTo();
        PriceBookEntry priceBookEntryObj = [SELECT Id ,CurrencyIsoCode FROM PriceBookEntry LIMIT 1];
        Set<Id> setPricebookEntryIds = new Set<ID>{priceBookEntryObj.id};
        Test.startTest();
        List<PriceBookEntry> result = localObj.getRecordsByPricebookEntry(setPricebookEntryIds);
        Test.stopTest();
        System.Assert(result.size() > 0);

    }  

    static testMethod void getNumberOfPackagedProducts_test() {
        EP_PriceBookEntryMapper localObj = new EP_PriceBookEntryMapper();
        Account accountObj = EP_TestDataUtility.getSellTo();
        PricebookEntry  pricebookEntryObj = [SELECT Id ,Pricebook2Id ,Product2.EP_Product_Sold_As__c FROM PricebookEntry LIMIT 1];
        Test.startTest();
        Integer result = localObj.getNumberOfPackagedProducts(pricebookEntryObj.Pricebook2Id);
        Test.stopTest();
        System.Assert(result > 0);

    } 

    static testMethod void getCountOfProductsRelatedToOtherCompany_test() {
        EP_PriceBookEntryMapper localObj = new EP_PriceBookEntryMapper();
        Account accountObj = EP_TestDataUtility.getSellTo();
        Account tempAccount = EP_TestDataUtility.createSellToAccountAndPriceBook();     
        Company__c companyObj = EP_TestDataUtility.createCompany(objName);
        insert companyObj;
        Product2 prodObject = EP_TestDataUtility.createProduct(objName);
        insert prodObject;
        PricebookEntry pbEntryObj = EP_TestDataUtility.createPricebookEntry(prodObject.Id,tempAccount.EP_Pricebook__c);
        pbEntryObj.EP_Is_Sell_To_Assigned__c = true;
        insert pbEntryObj;
        Test.startTest();
        Integer result = localObj.getCountOfProductsRelatedToOtherCompany(accountObj);
        List<PriceBookEntry> lstPriceBookEntry = localObj.getALLRecordsByPriceBookId(tempAccount.EP_Pricebook__c);
        Test.stopTest();
        System.Assert(result >= 0);

    }  

    static testMethod void getPriceBookEntries_test() {
        EP_PriceBookEntryMapper localObj = new EP_PriceBookEntryMapper();
        Account accountObj = EP_TestDataUtility.getSellTo();
        PriceBookEntry priceBookEntryObj = [Select Id ,ProductCode,CurrencyIsoCode,Pricebook2Id FROM PriceBookEntry LIMIT 1];
        Set<String> setProductCode = new Set<String>{priceBookEntryObj.ProductCode };
        Set<Id> setPriceBook2Id = new Set<Id>{priceBookEntryObj.Pricebook2Id};
        Set<String> setCurrencyISOCode = new Set<String>{priceBookEntryObj.CurrencyIsoCode };
        Test.startTest();
        list<PriceBookEntry> result = localObj.getPriceBookEntries(setProductCode,setPriceBook2Id,setCurrencyISOCode);
        Test.stopTest();
        System.AssertEquals(1,result.size());

    } 
}