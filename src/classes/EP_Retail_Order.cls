/*
    @Author          Accenture
    @Name            EP_Retail_Order
    @CreateDate      18/04/2018
    @Description     class is used to RO Import Staging Object for EP_Retail_Order
    @Version         1.0
    @Reference       NA
*/
public with sharing class EP_Retail_Order extends EP_ROImportStaging {
	
	public override void masterDataVerification(){
		EP_GeneralUtility.Log('public','EP_Retail_Order','masterDataVerification');
    	for(EP_RO_Import_Staging__c stagingRec : stagingRecords) {
    		isError = false;
    		resetFields(stagingRec);
	    	verifySellTo(stagingRec);
			verifyShipTo(stagingRec);
			verifySellToShipToRelationship(stagingRec);
			verifyProductCode(stagingRec);
	    	verifySupplyLocation(stagingRec);
    	}
    }
    
}