/**
  * @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
  * @name        : EP_Consumption
  * @CreateDate  : 16/02/2017
  * @Description : This is Class Contains logic Consumption Order
  * @Version     : <1.0>
  * @reference   : N/A
  */
  public with sharing class EP_ConsumptionOrder extends EP_OrderType {

    public EP_ConsumptionOrder(){
    }
    
    public EP_ConsumptionOrder(EP_OrderDomainObject orderDomainObject) {
      loadStrategies(orderDomainObject);
    }

    /** checks if Order Date is Valid or not
    *  @date      25/02/2017
    *  @name      isValidOrderDate
    *  @param     NA
    *  @return    NA
    *  @throws    NA
    */
    public override Boolean isValidOrderDate(csord__Order__c orderObj) {
      EP_GeneralUtility.Log('Public','EP_ConsumptionOrder','isValidOrderDate');
      return epochType.isValidOrderDate(orderObj);
    }

    /* This method is returns Consignment Accounts for Consignment Order
     *  @date      15/02/2017
     *  @name      getShipTos
     *  @param     accountId
     *  @return    List<Account>
     *  @throws    NA
     */
     public override List<Account> getShipTos(Id accountId) {
      EP_GeneralUtility.Log('Public','EP_ConsumptionOrder','getShipTos');

      System.debug('**********accountId:-'+accountId);
      List<Account> lstAccount = new List<Account>();

        //filter based on deliverytype

        lstAccount = deliverytype.getShipTos(accountId);

        // filter based on Order Epoch
        lstAccount = epochType.getShipTos(lstAccount,productSoldAsType);
        System.debug('**********lstAccount:-'+lstAccount);
        
        
        List<Account> lstShipTos = new List<Account>();

        for (Account objAccount : lstAccount) {

          if (objAccount.EP_Ship_To_Type__c == EP_Common_Constant.CONSIGNMENT) {

            lstShipTos.add(objAccount);
          }
        }       
        System.debug('**********lstShipTos:-'+lstShipTos);
        return lstShipTos;
      }


    /* returns record recordype for the Order
      *  @date      22/02/2017
      *  @name      findRecordType
      *  @param     NA
      *  @return    Id
      *  @throws    NA
      */
      public override Id findRecordType() {
        EP_GeneralUtility.Log('Public','EP_ConsumptionOrder','findRecordType');

        return EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER, EP_Common_Constant.CONSUMPTION_ORDER_RECORD_TYPE_NAME);
      }

    /* This method is returns the applicable delivery type for the consumption order.
     *  @date      26/02/2017
     *  @name      getApplicableDeliveryTypes
     *  @param     NA
     *  @return    List<String>
     *  @throws    NA
     */ 
     public override List<String> getApplicableDeliveryTypes(){
      EP_GeneralUtility.Log('Public','EP_ConsumptionOrder','getApplicableDeliveryTypes');

      List<String> listOfPaymentTerms = new List<String>();
      listOfPaymentTerms.add(EP_Common_Constant.CONSUMPTION);
      return listOfPaymentTerms;
    }
    
    
    /* This method returns Pricbook Entries for the Order
      *  @date      24/02/2017
      *  @name      getApplicablePaymentTerms
      *  @param     NA
      *  @return    List<PriceBookEntry>
      *  @throws    NA
      */
      public override List<PriceBookEntry> findPriceBookEntries(Id pricebookId,csord__Order__c objOrder) {
        EP_GeneralUtility.Log('Public','EP_ConsumptionOrder','findPriceBookEntries');

        return deliverytype.findPriceBookEntries(pricebookId,objOrder);
      }

    /* This method returns Pricbook Entries for the Order
      *  @date      24/02/2017
      *  @name      getApplicablePaymentTerms
      *  @param     NA
      *  @return    List<String>
      *  @throws    NA
      */
      public override List<String> getDeliveryTypes() {
        EP_GeneralUtility.Log('Public','EP_ConsumptionOrder','getDeliveryTypes');

        List<String> listDeliveryTypes = new List<String>();
        listDeliveryTypes.add(EP_Common_Constant.CONSUMPTION);
        return listDeliveryTypes;
      }

  /** This method find if Order Entry is Valid
   *  @date      07/03/2017
   *  @name      isOrderEntryValid
   *  @param     NA
   *  @return    NA
   *  @throws    NA
   */
   public override Boolean isOrderEntryValid(integer numOfTransportAccount){
    EP_GeneralUtility.Log('Public','EP_ConsumptionOrder','isOrderEntryValid');
    if (orderObj.EP_Order_Date__c == NULL) {
      return false;
    }
    return deliverytype.isOrderEntryValid(numOfTransportAccount);
  }

  /** This method sets Consumption values on fields
   *  @date      07/03/2017
   *  @name      setConsumptionOrderDetails
   *  @param     NA
   *  @return    NA
   *  @throws    NA
   */
   public override void setConsumptionOrderDetails() {
    EP_GeneralUtility.Log('Public','EP_ConsumptionOrder','setConsumptionOrderDetails');

    orderObj.RecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER, EP_Common_Constant.CONSUMPTION_ORDER_RECORD_TYPE_NAME);
    orderObj.EP_Actual_Delivery_Date_Time__c = EP_GeneralUtility.convertDateTimeToDecimal(String.valueOf(orderObj.EP_Order_Date__c)); 
    orderObj.EP_Loading_Date__c = orderObj.EP_Order_Date__c;
    orderObj.EP_Expected_Loading_Date__c = EP_PortalLibClass_R1.returnLocalDate(Date.valueOf(orderObj.EP_Order_Date__c));
    
	//Defect Fix Start - #57788 - Set Price Consolidation Basis field value from Account for Order Pricing
	EP_AccountMapper accountMapper = new EP_AccountMapper();
	Account objAccount = accountMapper.getAccountRecordById(orderObj.AccountId__c);
    // Check If Bill Account Exist
    if(objAccount != null
        && objAccount.EP_Bill_To_Account__c != null ) {
    	orderObj.EP_Price_Consolidation_Basis__c = objAccount.EP_Price_Consolidation_Basis__c;
    }
    else if(objAccount != null) {
    	orderObj.EP_Price_Consolidation_Basis__c = objAccount.EP_Price_Consolidation_Basis__c;
    }
    //Defect Fix End - #57788
  }

  /** calculates Price for the Order
  *  @date      28/02/2017
  *  @name      calculatePrice
  *  @param     NA
  *  @return    NA
  *  @throws    NA
  */
  public override void calculatePrice() {
    EP_GeneralUtility.Log('Public','EP_SalesOrder','calculatePrice');
    EP_NonConsignment NonConsignment = new EP_NonConsignment();
    NonConsignment.calculatePrice(orderObj);
  }

  /** This perform check Order Credit
  *  @date      05/02/2017
  *  @name      hasCreditIssue
  *  @param     NA
  *  @return    NAs
  *  @throws    NA
  */
  public override Boolean hasCreditIssue() {
    EP_GeneralUtility.Log('Public','EP_ConsumptionOrder','hasCreditIssue');
    //Consumption orders are always consignment and credit check is applicable for only non consignment order
    //Hence calling without the factory
    EP_NonConsignment nonConsignment = new EP_NonConsignment();
    Boolean hasCreditIssues = nonConsignment.hasCreditIssue(orderObj);
    return hasCreditIssues;
  }
}