public class EP_ASTSellToBlockedToActive extends EP_AccountStateTransition {
    
    public EP_ASTSellToBlockedToActive() {
        finalstate = EP_AccountConstant.ACTIVE;
    }
    
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBlockedToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBlockedToActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
    
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBlockedToActive','isGuardCondition');
        //No guard conditions for Blocked to Active
        return true;
    }
}