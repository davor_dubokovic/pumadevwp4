/* 
    @Author <Ram Rai>
    @name <EP_FE_ErrorLogEndpoint>
    @CreateDate <25/04/2016>
    @Description <This is API class use to log error in exception object>  
    @Version <1.0>
*/

@RestResource(urlMapping='/FE/V1/error/log/*')    
/* 
    @Author <Ram Rai>
    @name <EP_FE_ErrorLogEndpoint>
    @CreateDate <25/04/2016>
    @Description <This is API class use to log error in exception object>  
    @Version <1.0>
*/      
global with sharing class EP_FE_ErrorLogEndpoint{
    
    @HttpPost
/* 
    @Author <Ram Rai>
    @name <EP_FE_ErrorLogEndpoint>
    @CreateDate <25/04/2016>
    @Description <This is API class use to log error in exception object>  
    @Version <1.0>
*/
    global static EP_FE_ErrorLogResponse logError(List<EP_FE_ErrorLogRequest> objLogReqList ) {
    
    EP_FE_ErrorLogResponse response = new EP_FE_ErrorLogResponse();
    //EP_FE_ErrorLogRequest objLogReq1 = new EP_FE_ErrorLogRequest();
    
    //Check any network issue / malformed request
    try{  
        if(objLogReqList.isEmpty()) {
            response.status = EP_FE_ErrorLogResponse.ERROR_NOT_VALID_POST_REQUEST;
        } else {                  
            EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(objLogReqList));                               
        }
    } catch(Exception ex) {
        EP_FE_Utils.logError(EP_FE_Constants.ERROR_CLASS_NAME, EP_FE_Constants.ERROR_CLASS_METHOD_NAME, EP_FE_Constants.ERROR_API_NAME, EP_FE_Constants.ERROR_API_EXCEPTION, 
        EP_FE_Constants.ERROR_API_EXCEPTION_TYPE, Ex, EP_FE_ErrorLogResponse.ERROR_NOT_VALID_POST_REQUEST, response);
    } 
    
    return response;  
    }    
}