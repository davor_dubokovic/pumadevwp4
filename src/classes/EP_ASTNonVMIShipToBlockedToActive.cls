/*
*  @Author <Accenture>
*  @Name <EP_ASTNonVMIShipToBlockedToActive>
*  @CreateDate <28/02/2017>
*  @Description <Handles VMI Ship To Account status change from 06-Blocked to 05-Active>
*  @Version <1.0>
*/
public class EP_ASTNonVMIShipToBlockedToActive extends EP_AccountStateTransition {

    public EP_ASTNonVMIShipToBlockedToActive () {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToBlockedToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToBlockedToActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToBlockedToActive','isGuardCondition');
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToBlockedToActive','doOnExit');

    }
}