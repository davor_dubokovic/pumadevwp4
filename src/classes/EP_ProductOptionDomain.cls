/* 
   @Author      Accenture
   @name        EP_ProductOptionDomain
   @CreateDate  10/11/2017
   @Description This is the domain class for Product Option Object
   @Version     1.1
*/
public class EP_ProductOptionDomain{
     
    public list<EP_Product_Option__c> lstNewProductOption;
    public map<Id,EP_Product_Option__c> triggerOldMap;
    EP_ProductOptionService ProductOptionService;
    
    /**
    * @author     Accenture
    * @name      EP_ProductOptionDomain
    * @date      10/11/2017
    * @description   Constructor for setting new Product Option objects list which will be passed by the trigger
    * @param     list of new version Product Option records
    * @return     NA
    */  
    public EP_ProductOptionDomain(list<EP_Product_Option__c> triggerNewPrdOption){
        this.lstNewProductOption = triggerNewPrdOption;
        this.ProductOptionService = new EP_ProductOptionService(this);
    }
    /**
    * @author     Accenture
    * @name      EP_ProductOptionDomain
    * @date      15/11/2017
    * @description   Constructor for setting new Product Option objects list and old map which will be passed by the trigger
    * @param     list of new version Product Option and old map records
    * @return     NA
    */  
    public EP_ProductOptionDomain(list<EP_Product_Option__c> triggerNewPrdOption,map<Id,EP_Product_Option__c> OldMap){
        this.lstNewProductOption = triggerNewPrdOption;
        this.triggerOldMap = OldMap;
        this.ProductOptionService = new EP_ProductOptionService(this);
    }
    
    /**
    * @author     Accenture
    * @name      doActionBeforeInsert
    * @date       10/11/2017
    * @description   This method is to be called through trigger to execute all before insert event logic/actions
    * @param     NA
    * @return     NA
    */ 
    public void doActionBeforeInsert(){ //move to service class // Logger entries in method
        try{
            EP_GeneralUtility.Log('Public','EP_ProductOptionDomain','doActionBeforeInsert');
            ProductOptionService.doBeforeInsertHandle();          
        }
        catch(Exception ex){
          EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doActionBeforeInsert' , 'EP_ProductOptionDomain', ApexPages.Severity.ERROR);
        }  
    }
    /**
    * @author     Accenture
    * @name      doActionBeforeUpdate
    * @date       10/11/2017
    * @description   This method is to be called through trigger to execute all before update event logic/actions
    * @param     NA
    * @return     NA
    */ 
    public void doActionBeforeUpdate(){
      try{
            EP_GeneralUtility.Log('Public','EP_ProductOptionDomain','doActionBeforeUpdate');
            ProductOptionService.doBeforeUpdateHandle();     
        }
        catch(Exception ex){
          EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doActionBeforeUpdate' , 'EP_ProductOptionDomain', ApexPages.Severity.ERROR);
        }  
    }
    /**
    * @author     Accenture
    * @name      doActionOnBeforeDelete
    * @date       10/11/2017
    * @description   This method is to be called through trigger to execute all before delete event logic/actions
    * @param     NA
    * @return     NA
    */ 
    public void doActionOnBeforeDelete(){
      try{
            EP_GeneralUtility.Log('Public','EP_ProductOptionDomain','doActionOnBeforeDelete');
            ProductOptionService.doBeforeDeleteHandle();     
        }
        catch(Exception ex){
          EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doActionOnBeforeDelete' , 'EP_ProductOptionDomain', ApexPages.Severity.ERROR);
        }  
    }
}