/* 
   @Author Spiros Markantonatos
   @name <EP_RetrieveIntegrationRecordsV4>
   @CreateDate <04/11/2016>
   @Description <This class will be used by an external system to export integration records for reporting and monitoring purposes.
   				This version of the interface does not rely on the isExported flag to determine which records to export>
   @Version <1.0>
 
*/
@RestResource(urlMapping = '/v4/IntegrationRecords/*')
global with sharing class EP_RetrieveIntegrationRecordsV4 {
    
    // Const
    private static final String RETRIEVE_INTEGRATION_LOGS = 'retrieveIntegrationRecords';
    private static final String EP_RETRIEVE_INTEGRATION_LOGS = 'EP_RetrieveIntegrationRecords';
    private static final String EP_NAME_STR = 'NAME';
    private static final String EP_Attempt = 'EP_Attempt__c';
    private static final String EP_Company = 'EP_Company__c';
    private static final String EP_Country = 'EP_Country__c';
    private static final String EP_Country_Code = 'EP_Country_Code__c';
    private static final String EP_DT_RECEIVED = 'EP_DT_RECEIVED__c';
    private static final String EP_DT_SENT = 'EP_DT_SENT__c';
    private static final String EP_DT_SYNC = 'EP_DT_SYNC__c';
    private static final String EP_Endpoint_URL = 'EP_Endpoint_URL__c';
    private static final String EP_Error_Code = 'EP_Error_Code__c';
    private static final String EP_Error_Description = 'EP_Error_Description__c';
    private static final String EP_External_ID = 'EP_External_ID__c';
    private static final String EP_Initiated_By = 'EP_Initiated_By__c';
    private static final String EP_Integration_Error_Type = 'EP_Integration_Error_Type__c';
    private static final String EP_Integration_Failed = 'EP_Integration_Failed__c';
    private static final String EP_Is_Error_Resolved = 'EP_Is_Error_Resolved__c';
    private static final String EP_IsLatest = 'EP_IsLatest__c';
    private static final String EP_Is_My_Integration_Record = 'EP_Is_My_Integration_Record__c';
    private static final String EP_Is_Notification_Dismissed = 'EP_Is_Notification_Dismissed__c';
    private static final String EP_Is_Within_User_Region = 'EP_Is_Within_User_Region__c';
    private static final String EP_Message_ID = 'EP_Message_ID__c';
    private static final String EP_Object_ID = 'EP_Object_ID__c';
    private static final String EP_Object_Record_Name = 'EP_Object_Record_Name__c';
    private static final String EP_Object_Type = 'EP_Object_Type__c';
    private static final String EP_ParentNode = 'EP_ParentNode__c';
    private static final String EP_Query_Object_ID = 'EP_Query_Object_ID__c';
    private static final String EP_Quick_Source_Record_Link = 'EP_Quick_Source_Record_Link__c';
    private static final String EP_Resend = 'EP_Resend__c';
    private static final String EP_SeqId = 'EP_SeqId__c';
    private static final String EP_SLA_Status = 'EP_SLA_Status__c';
    private static final String EP_Source = 'EP_Source__c';
    private static final String EP_Status = 'EP_Status__c';
    private static final String EP_Target = 'EP_Target__c';
    private static final String EP_Transaction_ID = 'EP_Transaction_ID__c';
    private static final String EP_XML_Message = 'EP_XML_Message__c';
    private static final String STR_BLANK = EP_Common_Constant.BLANK;
    private static final String EP_FROM_DATE_WHERE_CLAUSE = 'CreatedDate >= :dtFrom';
    private static final String EP_TO_DATE_WHERE_CLAUSE = 'CreatedDate <= :dtTo';
    private static final String EP_LIMIT_CLAUSE = 'LIMIT';
    
    // Properties
    public static String strIntegrationObjectName {
    	get {
    		if (strIntegrationObjectName == NULL) strIntegrationObjectName = 'EP_IntegrationRecord__c';
    		return strIntegrationObjectName;
    	}
    	set;
    }
    
    // Methods
    
    /*************************************************************************
    *@Description : This method is used to construct and return the 
    				SELECT clause of the query used by the interface
    *@Params      : none                                                      *
    *@Return      : String (SELECT Statement)                                 *    
    **************************************************************************/      
    public static String getObjectFullSelectStatement() {
        String strSelectStatement = STR_BLANK;
        
        strSelectStatement += EP_NAME_STR + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Attempt + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Company + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Country + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Country_Code + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_DT_RECEIVED + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_DT_SENT + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_DT_SYNC + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Endpoint_URL + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Error_Code + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Error_Description + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_External_ID + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Initiated_By + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Integration_Error_Type + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Integration_Failed + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Is_Error_Resolved + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_IsLatest + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Is_My_Integration_Record + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Is_Notification_Dismissed + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Is_Within_User_Region + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Message_ID + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Object_ID + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Object_Record_Name + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Object_Type + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_ParentNode + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Query_Object_ID + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Quick_Source_Record_Link + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Resend + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_SeqId + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_SLA_Status + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Source + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Status + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Target + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_Transaction_ID + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_XML_Message;
        
        return strSelectStatement;
        
    }
    
    /**************************************************************************
    *@Description : This get method is used to retrieve integration records
                    by querying from the exception  
                    log object based on the date variabls passed through 
                    the url
                    This version of the interface will not rely/update the
    				isExported flag on the exception record 
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/    
    @HttpGet
    global static void retrieveIntegrationRecords() {
        String strResponse = EP_Common_Constant.BLANK;
        
        // Initialise the list that will be used to mark succesfully exported records in SFDC
        List <EP_IntegrationRecord__c> lIntegrationRecordUpdate = new List <EP_IntegrationRecord__c> ();
        
        try {
            // Retrieve filter from date from the URL
            DateTime dtFrom = EP_MonitorExportLibClassV3.returnQueryDateFilterFromRequest(EP_Common_Constant.FROM_DATE_TIME_FILTER_NAME);
            // Retrieve filter to date from the URL
            DateTime dtTo = EP_MonitorExportLibClassV3.returnQueryDateFilterFromRequest(EP_Common_Constant.TO_DATE_TIME_FILTER_NAME);
            // Retrieve limit value from the URL
            Integer intLimit = EP_MonitorExportLibClassV3.returnQueryLimitFromRequest(EP_Common_Constant.limitException);
            
            if(dtFrom != NULL)
            {
                //Query EP_IntegrationRecord__c records based on date and isExported value should be FALSE
                String strQuery = EP_Common_Constant.SELECT_STRING + 
                                    EP_Common_Constant.SPACE_STRING +
                                        getObjectFullSelectStatement() +
                                            EP_Common_Constant.SPACE_STRING +
                                                EP_Common_Constant.FROM_STRING +
                                                    strIntegrationObjectName + 
                                                        EP_Common_Constant.SPACE_STRING +
                                                            EP_Common_Constant.WHERE_STRING +
                                                                EP_Common_Constant.SPACE_STRING +
                                                                    EP_FROM_DATE_WHERE_CLAUSE;
                
                // Append option to date where clause filter
                if (dtTo != NULL)
                {
                	strQuery += EP_Common_Constant.SPACE_STRING + 
                					EP_Common_Constant.AND_STRING + 
                						EP_Common_Constant.SPACE_STRING + 
                							EP_TO_DATE_WHERE_CLAUSE;       
                }                                       
                
                // Append LIMIT clause
                strQuery += EP_Common_Constant.SPACE_STRING + EP_LIMIT_CLAUSE + EP_Common_Constant.SPACE_STRING + intLimit;        
                                                          
                List <EP_IntegrationRecord__c> lIntegrationRecords = Database.query(String.escapeSingleQuotes(strQuery));
                
                strResponse = EP_MonitorExportLibClassV3.returnPayloadBody(lIntegrationRecords,
                                                                            EP_Common_Constant.INTEGRATION_LOGS,
                                                                                EP_Common_Constant.INTEGRATION_LOGS + 
                                                                                    EP_Common_Constant.NULL_VAR);
                
            } else {
                strResponse = EP_Common_Constant.GENERIC_FILTER_RESPONSE_FALSE;
            }
        } catch (Exception ex) {
            EP_LoggingService.logServiceException(ex, 
                                                    UserInfo.getOrganizationId(), 
                                                        EP_Common_Constant.EPUMA, 
                                                            RETRIEVE_INTEGRATION_LOGS, 
                                                                EP_RETRIEVE_INTEGRATION_LOGS, 
                                                                    EP_Common_Constant.ERROR, 
                                                                        UserInfo.getUserId(), 
                                                                            EP_Common_Constant.TARGET_SF, 
                                                                                EP_Common_Constant.BLANK, 
                                                                                    EP_Common_Constant.BLANK);
            strResponse = EP_Common_Constant.RESPONSE_FALSE;
        }
        
        RestContext.response.responseBody = Blob.valueOf(strResponse);
    }
    
}