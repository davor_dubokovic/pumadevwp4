///* 
//  @Author <Abhay Aror>
//   @name <EP_OrderPageExtension >
//   @CreateDate <10/16/2015>
//   @Description <This class is used by the users to test the order submission>
//   @Version <1.0>
// 
//*/
//@istest(seeAllData=true)
public class EP_OrderPageExtension_Test{
//
//    private static List<Account> accList; 
//    private static List<Account> selltoAccList;
//    private static List<Account> shiptoAccList;
//    private static List<EP_Tank_Dip__c> tankDipList; 
//    private static EP_Tank__c TankInstance; 
//    private static Order orderInstance;
//    private static OrderItem orderItem;
//    private static Product2 product2 ; 
//    private static Account currentAccount; 
//    private static Account sellToAccount;
//    private static Account shipToAccount;
//    private Static Account stocklocationAccount;
//    private static user currentrunningUser;
//
//    
//    /**************************************************************************
//    *@Description : This method is used to create Data.                       *
//    *@Params      : none                                                      *
//    *@Return      : void                                                      *    
//    **************************************************************************/    
//    private static void init(){
//         currentrunningUser=EP_TestDataUtility.createTestRecordsForUser();
//         
//         
//         selltoAccList= EP_TestDataUtility.createSelltoTestAccount(new List<Account>{new Account()});
//         sellToAccount=selltoAccList.get(0);
//         EP_Tank__c tankObj = EP_TestDataUtility.createTestEP_Tank(sellToAccount);
//         sellToAccount.EP_Status__c =Label.EP_Active_Ship_To_Status_Label;
//         update sellToAccount;
//
//         shiptoAccList = EP_TestDataUtility.createTestAccount(new List<Account>{new Account(parentid=sellToAccount.id)});
//         shipToAccount=shiptoAccList.get(0);
//         TankInstance= EP_TestDataUtility.createTestEP_Tank(shipToAccount);
//         shipToAccount.EP_Status__c =Label.EP_Active_Ship_To_Status_Label;
//         EP_Tank__c tankObj1 = EP_TestDataUtility.createTestEP_Tank(shipToAccount);
//         shipToAccount.EP_Status__c =Label.EP_Active_Ship_To_Status_Label;
//         update shipToAccount;
//         orderInstance=EP_TestDataUtility.createTestRecordsForOrder(String.valueof(shipToAccount.id));
//               
//         
//    }
//    
//    /**************************************************************************
//    *@Description : This method is used to test edit order                    *
//    *@Params      : none                                                      *
//    *@Return      : void                                                      *    
//    **************************************************************************/        
//     private static testMethod void testEditOrder(){ 
//        init();   
//        Test.startTest();
//         system.runas(currentrunningUser){     
//
//            PageReference pageRef = Page.EP_OrderPage;
//            Test.setCurrentPage(pageRef);//Applying page context here    
//            ApexPages.currentPage().getParameters().put('id', orderInstance.Id);
//            ApexPages.currentPage().getParameters().put('accountId', shipToAccount.Id);
//            ApexPages.StandardController sc = new ApexPages.StandardController(orderInstance);
//            EP_OrderPageExtension  ctrl= new EP_OrderPageExtension  (sc); 
//            if(ctrl.lstOrderLineItems!=null && ctrl.lstOrderLineItems.size()>0){
//                ctrl.lstOrderLineItems.get(0).lineitem.EP_Unit__c=10;
//            }
//            ctrl.SaveAndNew();
//            list<order> lstOrder= new list<order>([select id from order]);
//            system.assertNotEquals(lstOrder.size(),0);
//            ctrl.close();
//         }
//        Test.stopTest();  
//      
//    }
//     
//    /**************************************************************************
//    *@Description : This method is used to test New order  entry              *
//    *@Params      : none                                                      *
//    *@Return      : void                                                      *    
//    **************************************************************************/   
//     private static testMethod void testNewOrder(){ 
//        init();   
//        Test.startTest();
//         system.runas(currentrunningUser){     
//
//            PageReference pageRef = Page.EP_OrderPage;
//            Test.setCurrentPage(pageRef);//Applying page context here  
//            ApexPages.currentPage().getParameters().put('accountId', shipToAccount.Id);
//            ApexPages.StandardController sc = new ApexPages.StandardController(new order(EP_Order_Number__c=1234));
//            EP_OrderPageExtension  ctrl= new EP_OrderPageExtension  (sc);                 
//            ctrl.strSellToNumber='1345';
//            ctrl.strShipToNumber='1345';
//             ctrl.searchAccountRecords();
//            if(ctrl.lstOrderLineItems!=null && ctrl.lstOrderLineItems.size()>0){
//                ctrl.lstOrderLineItems.get(0).lineitem.EP_Unit__c=10;
//            }
//
//            ctrl.calculateRequestedDeliveryEndDate();
//            ctrl.SaveAndNew();
//            shipToAccount.EP_Status__c=Label.EP_Inactive_Ship_To_Status_Label;
//            ctrl.clearAccountRecords();
//        }
//        Test.stopTest();
//      
//    }
}