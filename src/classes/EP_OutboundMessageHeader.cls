/* 
  @Author : Accenture
  @name : EP_OutboundMessageHeader
  @CreateDate :02/08/2017
  @Description :This class is use to Hold the property of the Header for Outbound Messages 
  @Version : 1.0
*/
public virtual class EP_OutboundMessageHeader {
    public String MsgID {get;set;}
    public String InterfaceType {get;set;}
    public String SourceGroupCompany {get;set;}
    public String DestinationGroupCompany {get;set;}
    public String SourceCompany {get;set;}
    public String DestinationCompany {get;set;}
    public String CorrelationID {get;set;}
    public String DestinationAddress {get;set;}
    public String SourceResponseAddress {get;set;}
    public String SourceUpdateStatusAddress {get;set;}
    public String DestinationUpdateStatusAddress {get;set;}
    public String MiddlewareUrlForPush {get;set;}
    public String EmailNotification {get;set;}
    public String ErrorCode {get;set;}
    public String ErrorDescription {get;set;}
    public String ProcessingErrorDescription {get;set;}
    public Boolean ContinueOnError {get;set;}
    public Boolean ComprehensiveLogging {get;set;}
    public String TransportStatus {get;set;}
    public String ProcessStatus {get;set;}
    public Boolean UpdateSourceOnReceive {get;set;}
    public Boolean UpdateSourceOnDelivery {get;set;}
    public Boolean UpdateSourceAfterProcessing {get;set;}
    public Boolean UpdateDestinationOnDelivery {get;set;}
    public Boolean CallDestinationForProcessing {get;set;}
    public String ObjectType {get;set;}
    public String ObjectName {get;set;}
    public String CommunicationType {get;set;}

    /**
	* @Author : Accenture
	* @Name : EP_OutboundMessageHeader
	* @Date : 02/08/2017
	* @Description : Class Contracture to set default values for Header Message
	* @Param :
	* @return :
	*/
    public EP_OutboundMessageHeader() {
    	this.ContinueOnError = true;
        this.ComprehensiveLogging = true;
        this.UpdateDestinationOnDelivery = true;
        this.CallDestinationForProcessing = true;
        this.UpdateSourceOnDelivery = false;
        this.UpdateSourceOnReceive = false;
        this.UpdateSourceAfterProcessing = false;
        this.ObjectType = EP_Common_Constant.TABLE_STRING;
        this.CommunicationType = EP_Common_Constant.ASYNC;
        this.TransportStatus = EP_Common_Constant.NEW_STR;
    }
}