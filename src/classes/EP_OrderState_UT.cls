@isTest
public class EP_OrderState_UT {
    
    public static EP_OrderState orderState;
    public static String EVENT_NAME = 'Submitted To Accepted';
    public static String WINDMS_STATUS = 'Accepted';
    public static String STATUS = 'Submitted';

    @testSetup static void init() { 
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    
    /* Do transitions testmethod 
    static testmethod void testDoTransition(){
        orderState = new EP_OrderState();
        EP_OrderDomainObject orderDomain = EP_TestDataUtility.getOrderStateSubmittedDomainObjectPositiveScenario();
        //Test class Fix Start
        orderDomain.setStatus('Planned');       
        EP_OrderEvent orderEvent = new EP_OrderEvent('Planned To Loaded');
        //Test class Fix End
        orderState.setOrderContext(orderDomain, orderEvent);
        orderState.doOnExit();
        orderState.doOnEntry();
        boolean result = orderState.doTransition();
        system.assert(result, true);
    } */
    
    // test setOrderContext method
    static testmethod void testsetOrderContext(){
        orderState = new EP_OrderState();
        //Test class Fix Start
        //EP_OrderDomainObject orderDomain = new EP_OrderDomainObject('801p00000009jlU'); 
        csord__Order__c orderObj = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        upsert orderObj;
        EP_OrderDomainObject orderDomain = new EP_OrderDomainObject(orderObj.id);
        //Test class Fix End    
        EP_OrderEvent orderEvent = new EP_OrderEvent('Planned To Loaded');
        orderState.setOrderContext(orderDomain, orderEvent);
        system.assert((orderState.orderEvent != null && orderState.order !=null), true);  
    }
    
    //test ConvertCustomSettingsToObject method
    static testmethod void testConvertCustomSettingsToObject(){
        orderState = new EP_OrderState();
        //Test class Fix Start
        //EP_OrderDomainObject orderDomain = new EP_OrderDomainObject('801p00000009jlU');
        csord__Order__c orderObj = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        upsert orderObj;
        EP_OrderDomainObject orderDomain = new EP_OrderDomainObject(orderObj.id);
        //Test class Fix End
        orderDomain.setWinDMSStatus('Loaded');
        orderDomain.setStatus('Planned');       
        EP_OrderEvent orderEvent = new EP_OrderEvent('Planned To Loaded');
        orderState.setOrderContext(orderDomain, orderEvent);
        //List<EP_OrderStateTransition> listTransition = orderState.ConvertCustomSettingsToObject('Planned'); 
        //system.assert((listTransition != null), true);
    }
    
    
    //Method has no implementation, hence adding dummy assert 
    static testMethod void doOnEntry_Test(){
        EP_OrderState localObj = new EP_OrderState();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Draft);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.assertEquals(true, true);
    }
    //Method has no implementation, hence adding dummy assert 
    static testMethod void doOnExit(){
        EP_OrderState localObj = new EP_OrderState();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Draft);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();  
        System.assertEquals(true, true);     
    }
       
    // test setOrderContext method
    static testmethod void setOrderContext_test(){
        EP_OrderState localObj = new EP_OrderState();
        EP_OrderDomainObject orderDomain = EP_TestDataUtility.getOrderStateSubmittedDomainObjectPositiveScenario();
        EP_OrderEvent orderEvent = new EP_OrderEvent(EVENT_NAME);
        localObj.setOrderContext(orderDomain, orderEvent);
        system.assert((localObj.orderEvent != null && localObj.order !=null), true);  
    }
    
    //test ConvertCustomSettingsToObject method
    static testmethod void convertCustomSettingsToObject_test(){
        orderState = new EP_OrderState();
        EP_OrderDomainObject orderDomain = EP_TestDataUtility.getSalesOrderDomainObject();
        orderDomain.setWinDMSStatus(WINDMS_STATUS);
        //orderDomain.setStatus('Submi');       
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        orderState.setOrderContext(orderDomain, oe);
        //List<EP_OrderStateTransition> listTransition = orderState.ConvertCustomSettingsToObject('Draft'); 
        //system.assert(listTransition.size()> 0);
    }

   
}