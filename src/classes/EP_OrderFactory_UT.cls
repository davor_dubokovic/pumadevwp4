@isTest
public class EP_OrderFactory_UT
{

    static testMethod void getType_test() {
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderFactory localObj = new EP_OrderFactory(orderDomainObj);
        Test.startTest();
        EP_OrderType result = localObj.getType();
        Test.stopTest();
        System.AssertEquals(true,result!=NULL);
        set<String>  resultSet = new set<String>();
        resultSet.addAll(result.getDeliveryTypes());
        System.AssertEquals(true,resultSet != NULL && resultSet.contains(EP_Common_Constant.EX_RACK));
    }
    static testMethod void getVMIType_test() {
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderFactory localObj = new EP_OrderFactory(orderDomainObj);
        Test.startTest();
        EP_VendorManagement result = localObj.getVMIType();
        System.AssertEquals(true,result!=NULL);
    }
    static testMethod void getEpochType_test() {
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderFactory localObj = new EP_OrderFactory(orderDomainObj);
        Test.startTest();
        EP_OrderEpoch result = localObj.getEpochType();
        Test.stopTest();
        System.AssertEquals(true,result!=NULL);
        System.AssertEquals(true,result instanceof EP_Retrospective);
    }
    static testMethod void getConsignmentType_test() {
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderFactory localObj = new EP_OrderFactory(orderDomainObj);
        Test.startTest();
        EP_ConsignmentType result = localObj.getConsignmentType();
        Test.stopTest();
        System.AssertEquals(true,result!=NULL);
        System.AssertEquals(true,result instanceof EP_NonConsignment);
    }
    static testMethod void getProductSoldAsType_test() {
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderFactory localObj = new EP_OrderFactory(orderDomainObj);
        Test.startTest();
        EP_ProductSoldAs result = localObj.getProductSoldAsType();
        Test.stopTest();
        System.AssertEquals(true,result!=NULL);
        System.AssertEquals(true,result instanceof EP_BulkProducts);
    }
    static testMethod void getDeliveryType_test() {
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getConsumptionOrderDomainObject();
        EP_OrderFactory localObj = new EP_OrderFactory(orderDomainObj);
        Test.startTest();
        EP_DeliveryType result = localObj.getDeliveryType();
        Test.stopTest();
        System.AssertEquals(true,result!=NULL);
        System.AssertEquals(true,result instanceof EP_Consumption);
    }
}