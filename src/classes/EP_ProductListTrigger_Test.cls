@isTest
public with sharing class EP_ProductListTrigger_Test {
    private static testMethod void addDuplicatePriceBook_InsertTest(){
		System.runAs(EP_TestDataUtility.createRunAsUser()) {
			Pricebook2 priceBookObj = EP_TestDataUtility.createPricebook2(true);			
			try {
				test.startTest();
					Pricebook2  priceBookObj1 = EP_TestDataUtility.createPricebook2(true);
				test.stopTest();
			} catch(exception exp) {
				Boolean expectedExceptionThrown =  exp.getMessage().contains(system.label.EP_ProductList_Name_Duplicate_Error_Message) ? true : false;
				System.AssertEquals(expectedExceptionThrown, true);
			}
			
		}
	}
	private static testMethod void addDuplicatePriceBook_UpdateTest(){
		System.runAs(EP_TestDataUtility.createRunAsUser()) {
			Pricebook2 priceBookObj = EP_TestDataUtility.createPricebook2(true);
			Pricebook2  priceBookObj1 = EP_TestDataUtility.createPricebook2(false);
			priceBookObj1.Name = 'PriceBook B';
			insert priceBookObj1;
			EP_ProductListTriggerHandler.isExecuteBeforeUpdate = false;
			try {
				test.startTest();
					priceBookObj1.Name = priceBookObj.Name;
					update priceBookObj1;
				test.stopTest();
			} catch(exception exp) {
				Boolean expectedExceptionThrown =  exp.getMessage().contains(system.label.EP_ProductList_Name_Duplicate_Error_Message) ? true : false;
				System.AssertEquals(expectedExceptionThrown, true);
			}
		}
	}
}