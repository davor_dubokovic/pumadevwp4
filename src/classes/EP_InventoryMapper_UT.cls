@isTest
public class EP_InventoryMapper_UT
{
    static testMethod void getInventoryByProdId_test() {
        EP_InventoryMapper localObj = new EP_InventoryMapper();
        Account sellToObj = EP_TestDataUtility.getSellToPositiveScenario();
        EP_Inventory__c invetoryObj = [SELECT id,EP_Storage_Location__c,EP_Product__c,EP_Inventory_Availability__c FROM EP_Inventory__c LIMIT 1];
        Set<Id> setProdIds = new Set<id>{invetoryObj.EP_Product__c};
        Id stocHoldLocId = invetoryObj.EP_Storage_Location__c;
        Test.startTest();
        list<EP_Inventory__c> result = localObj.getInventoryByProdId(setProdIds,stocHoldLocId);
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
    
    static testMethod void getInventoryByLocationId_test() {
        EP_InventoryMapper localObj = new EP_InventoryMapper();
        Account sellToObj = EP_TestDataUtility.getSellToPositiveScenario();
        EP_Inventory__c invetoryObj = [SELECT id,EP_Storage_Location__c,EP_Product__c,EP_Inventory_Availability__c FROM EP_Inventory__c LIMIT 1];
        Id storageLocationId = invetoryObj.EP_Storage_Location__c;
        Product2 productObj = [SELECT Id , EP_Product_Sold_As__c  FROM Product2 WHERE Id =:invetoryObj.EP_Product__c];
        String productSoldAs = productObj.EP_Product_Sold_As__c;
        Test.startTest();
        list<EP_Inventory__c> result = localObj.getInventoryByLocationId(storageLocationId,productSoldAs);
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
    
    static testMethod void getInventoryByStockHoldingLocationId_test() {
        EP_InventoryMapper localObj = new EP_InventoryMapper();
        Account sellToObj = EP_TestDataUtility.getSellToPositiveScenario();
        EP_Inventory__c invetoryObj = [SELECT id,EP_Storage_Location__c,EP_Product__c,EP_Inventory_Availability__c FROM EP_Inventory__c LIMIT 1];
        Id storageLocationId = invetoryObj.EP_Storage_Location__c;
        Test.startTest();
        list<EP_Inventory__c> result = localObj.getInventoryByStockHoldingLocationId(storageLocationId);
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
    
    static testMethod void getSuppliers_test() {
        EP_InventoryMapper localObj = new EP_InventoryMapper();
        Account sellToObj = EP_TestDataUtility.getSellToPositiveScenario();
        EP_Inventory__c invetoryObj = [SELECT id,EP_Storage_Location__c,EP_Product__c,EP_Inventory_Availability__c FROM EP_Inventory__c LIMIT 1];
        Id storageLocationId = invetoryObj.EP_Storage_Location__c;
        Test.startTest();
        list<EP_Inventory__c> result = localObj.getSuppliers(storageLocationId);
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
}