@isTest
public class EP_ASTNonVMIShipToBasDatToBasDat_UT
{
    static final string EVENT_NAME = '02-BasicDataSetupTo02-BasicDataSetup';
    static final string INVALID_EVENT_NAME = '08-ProspectTo02-Basic Data Setup';
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    
    static testMethod void isTransitionPossible_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTNonVMIShipToBasicDataToBasicData ast = new EP_ASTNonVMIShipToBasicDataToBasicData();
        Delete [Select id from EP_Action__c];
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void isTransitionPossible_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME );
        EP_ASTNonVMIShipToBasicDataToBasicData ast = new EP_ASTNonVMIShipToBasicDataToBasicData();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isRegisteredForEvent_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTNonVMIShipToBasicDataToBasicData ast = new EP_ASTNonVMIShipToBasicDataToBasicData();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isRegisteredForEvent_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME );
        EP_ASTNonVMIShipToBasicDataToBasicData ast = new EP_ASTNonVMIShipToBasicDataToBasicData();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isGuardCondition_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTNonVMIShipToBasicDataToBasicData ast = new EP_ASTNonVMIShipToBasicDataToBasicData();
        Delete [Select id from EP_Action__c];
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isGuardCondition_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME );
        EP_Action__c cscreview = EP_TestDataUtility.createCSCNewAction(obj.getAccount());
        database.insert(cscreview);
        EP_ASTNonVMIShipToBasicDataToBasicData ast = new EP_ASTNonVMIShipToBasicDataToBasicData();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    /* #routeSprintWork starts
     static testMethod void isGuardCondition_negative_route_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario();
         list<EP_Stock_Holding_Location__c> stockList = [select id from EP_Stock_Holding_Location__c];
        database.delete(stockList);
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTNonVMIShipToBasicDataToBasicData ast = new EP_ASTNonVMIShipToBasicDataToBasicData();
        ast.setAccountContext(obj,ae);
        
        Account account = obj.localAccount;
        Map<Id,EP_Route__c> routeRec = EP_TestDataUtility.getRouteObjects(1);
        EP_Route__c route = routeRec.values()[0];
        route.EP_Company__c = account.EP_Puma_Company__c;
        update route;
        account.EP_Default_Route__c = route.id;
        delete [Select Id from EP_Action__c];
    
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
     #routeSprintWork ends
     */

    static testMethod void doOnExit_test(){
        EP_ASTNonVMIShipToBasicDataToBasicData ast = new EP_ASTNonVMIShipToBasicDataToBasicData();
        ast.doOnExit();
        // No Processing Logic, hence adding a dummy assert.
        system.Assert(true); 
    }
   
}