/**
  * @author       Accenture                                       
  * @name         EP_ProformaInvoiceEvent_Subscription                           
  * @Created Date 17/01/2018                                      
  * @description  Quote Email Notification Class                   
  */
public with sharing class EP_ProformaInvoiceEvent_Subscription extends EP_Event_Subscription{
	public EP_ProformaInvoiceEvent_Subscription(){
		subscriptionEventObj.eventType = EP_Common_Constant.NOTIFICATION_CODE_PROFORMA;
	}
	
}