/* 
   @Author <>
   @name <EP_FE_CancelOrderInfoResponse >
   @CreateDate <>
   @Description <>  
   @Version <1.0>
*/
global with sharing class EP_FE_CancelOrderInfoResponse extends EP_FE_Response {

    // Error list
    global static final Integer ERROR_MISSING_ORDERID = -1;
    global static final Integer ERROR_UPDATE_FAILED = -2;
    global static final Integer ERROR_NOT_CANCELLABLE_ORDER = -3;
    global static final Integer ERROR_MISSING_REASONDETAIL = -4;
    global static final Integer ERROR_ORDER_NOT_AVAILABLE_FOR_DELETING = -5;

    global csord__Order__c order;

}