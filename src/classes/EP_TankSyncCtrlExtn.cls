/* 
   @Author 			Accenture
   @name 			EP_TankSyncCtrlExtn
   @CreateDate 		02/08/2017
   @Description		Controller extension to generate XML for Tank
   @Version 		1.0
*/
public with sharing class EP_TankSyncCtrlExtn{ 
    
    private string messageId;
    private string messageType;
    private string companyCode;
    public String encodedPayload {get;set;}
    public EP_Tank__c TankObject {get;set;}
    public EP_OutboundMessageHeader headerObj {get;set;}
    private EP_CS_OutboundMessageSetting__c msgSetting;
    private string secretCode;
    
	/**
	* @author 			Accenture
	* @name				EP_TankSyncCtrlExtn
	* @date 			03/13/2017
	* @description 		Constructor for extension class, This will get the TankObject by using the getRecord method of standard controller This will setup the data in the header Object and will setup the data in payload.
	* @param 			ApexPages.StandardController
	* @return 			NA
	*/
    public EP_TankSyncCtrlExtn(ApexPages.StandardController stdController) {
        secretCode = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_SECRETCODE);
        if (!Test.isRunningTest()) stdController.addFields(new List<String>{'EP_Ship_To__r.EP_Status__c'}); 
        this.TankObject = (EP_Tank__c ) stdController.getRecord();
        this.messageType = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGE_TYPE);
        this.messageId =  ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGEID);
        this.companyCode =  ApexPages.currentPage().getParameters().get(EP_Common_Constant.SOURCE_COMPANY);
        this.msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(this.messageType);
        this.headerObj = new EP_OutboundMessageHeader();
        this.setHeader();
        this.setPayload();
    }
    
	/**
	* @author 			Accenture
	* @name				setHeader
	* @date 			03/13/2017
	* @description 		This method will be used to setup the values for Header in XML message
	* @param 			NA
	* @return 			NA
	*/
    public void setHeader() {
        EP_GeneralUtility.Log('Public','EP_TankSyncCtrlExtn','setHeader');
        this.headerObj = EP_OutboundMessageUtil.setMessageHeader(msgSetting, this.messageId, this.companyCode);
    }
    
	/**
	* @author 			Accenture
	* @name				setPayload
	* @date 			03/13/2017
	* @description 		This method will be used to setup the values in payload
	* @param 			NA
	* @return 			NA
	*/
    @TestVisible
	private void setPayload() {
        EP_GeneralUtility.Log('Private','EP_TankSyncCtrlExtn','setPayload');
        this.encodedPayload = EP_OutboundMessageUtil.getPayloadXML(msgSetting,this.TankObject.id, this.messageId,this.companyCode);
    }
    
	/**
	* @author 			Accenture
	* @name				checkPageAccess
	* @date 			03/13/2017
	* @description 		This method will be use to redirect user at Error Page if they are trying to access this page without passing secret Code.
	* @param 			NA
	* @return 			PageReference
	*/
	public PageReference checkPageAccess() {
        EP_GeneralUtility.Log('Public','EP_TankSyncCtrlExtn','checkPageAccess');
        PageReference pageRef = null;
        if(! EP_OutboundMessageUtil.isAuthorized(this.secretCode)) {
            pageRef =  EP_OutboundMessageUtil.redirectToErrorPage();
        }
        return pageRef;
    }
}