/*
   @Author          CS
   @name            EP_RetrospectiveOrderRequestHandler
   @CreateDate      23/04/2018
   @Description     Inbound handler NAV to SFDC communication. Creates Retrospective Order based on XML input from NAV
   @Version         1.0
*/
public class EP_RetrospectiveOrderRequestHandler extends EP_InboundHandler {

	@TestVisible
	private static List<EP_AcknowledgementStub.dataSet> ackResponseList = new list <EP_AcknowledgementStub.dataSet> ();

	@TestVisible
	private static boolean processingFailed = false;

	public override string processRequest(string jsonRequest) {

		EP_GeneralUtility.Log('public', 'EP_RetrospectiveOrderRequestHandler', 'processRequest');
		EP_MessageHeader headerCommon = new EP_MessageHeader();
		string failureReason;

		csord__Order__c csOrder = new csord__Order__c();
		List<csord__Order_Line_Item__c> olis = new List<csord__Order_Line_Item__c>();

		try {

			EP_RetrospectiveOrderRequestStub obj = (EP_RetrospectiveOrderRequestStub)JSON.deserialize(jsonRequest, EP_RetrospectiveOrderRequestStub.class);

			csOrder = EP_RetrospectiveOrderRequestHandler.mapValuesToCloudSenseOrder(obj.msg.payload.any0.order);

			csOrder.csord__Status2__c = 'Order Submitted';
			csOrder.EP_Order_Epoch__c = 'Retrospective';

			insert csOrder;

			olis = EP_RetrospectiveOrderRequestHandler.mapValuesToCloudSenseOLI(obj.msg.payload.any0.order, csOrder.Id);

			insert olis;

			Id processTemplateId = EP_RetrospectiveOrderRequestHandler.getDrowDownOrderOrchestrationProcessTemplateId();
			EP_RetrospectiveOrderRequestHandler.startOrchestrationProcess(csOrder, processTemplateId);

		} catch (Exception ex) {

			failureReason = ex.getMessage();
			EP_LoggingService.logServiceException(ex, UserInfo.getOrganizationId(), EP_Common_constant.EPUMA, 'processRequest', 'EP_RetrospectiveOrderRequestHandler',  EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF, EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);

			createResponse(null, ex.getTypeName(), ex.getMessage());
		}

		return EP_AcknowledgementHandler.createAcknowledgement(EP_Common_Constant.NAV_ORDER_NEW, processingFailed, failureReason, headerCommon, ackResponseList);
	}

	private static csord__Order__c mapValuesToCloudSenseOrder(EP_RetrospectiveOrderRequestStub.Order order) {

		csord__Order__c csOrder = new csord__Order__c();

		List<Account> accounts = [SELECT Id FROM Account WHERE (AccountNumber = :order.sellToId OR AccountNumber = :order.shipToId)];
		List<csord__Order__c> orders = [SELECT Id FROM csord__Order__c WHERE Account_Number__c = :order.sellToId]; // OrderNumber__c = :order.salesContractNr

		/*
		Delivery Type : Ex-rack
		• Order Type: Sales
		• Order Epoch: Retrospective
		• Contract No. : As synchronized along with Delivery confirmation details
		• Sell-to Customer Account No. : As synchronized along with Delivery confirmation details
		• Supply Location (Pick-up Location) : As synchronized along with Delivery confirmation details
		• Payment terms: As defined in the customer card
		• Product : As synchronized along with Delivery confirmation details
		• Order Quantity : As synchronized along with Delivery confirmation details
		• Order Date: Same as Loading date (Synchronized along with Delivery confirmation details)
		• Loading Date: As synchronized along with Delivery confirmation details
		• Delivery Date: Same as Loading date (Synchronized along with Delivery confirmation details)
		• Truck : As synchronized along with Delivery confirmation details
		• Driver : As synchronized along with Delivery confirmation details
		*/

		csOrder.EP_Delivery_Type__c = '	Ex-Rack'; // order.deliveryType; // Picklist
		csOrder.EP_Order_Epoch__c = 'Retrospective'; // order.orderEpoch; // Picklist
		// Contract number
		csOrder.EP_Sell_To__c = accounts[0].Id;
		// csOrder.Pickup_Supply_Location__c =
		// Payment terms
		// Product
		csOrder.EP_Total_Order_Quantity__c = order.totalOrderQty; // Text
		// Order Date
		csOrder.EP_Expected_Loading_Date__c = order.loadingDt != null ? Date.newinstance(order.loadingDt.year(), order.loadingDt.month(), order.loadingDt.day()) : null;
		csOrder.Actual_Delivery_Date__c = order.dlvryDt != null ? Date.newinstance(order.dlvryDt.year(), order.dlvryDt.month(), order.dlvryDt.day()) : null;
		// Truck
		// Driver
		csOrder.csord__Status2__c = 'Order Submitted';
		csOrder.csord__Primary_Order__c = orders[0].Id;
		// csOrder.Type__c = 'Sales'; // order.orderType; // Picklist
		csOrder.EP_Order_Comments__c = order.comment; // Text
		csOrder.csord__Identification__c = 'Draw Down Order';

		// csOrder.EP_Sell_To__c = order.sellToId; // Loookup to Account
		// csOrder.EP_ShipTo__c = order.shipToId; // Lookup to Account
		// csOrder.EP_WinDMS_Trip_Reference_Number__c = order.tripId; // Text
		// csOrder.EP_Stock_Holding_Location__c = order.logisticStockHldngLocId; // Lookup to Account

		// orderOrigination
		// salesContractNr
		// vehicleId
		// driverId
		// driverName

		return csOrder;
	}

	private static List<csord__Order_Line_Item__c> mapValuesToCloudSenseOLI(EP_RetrospectiveOrderRequestStub.Order order, Id orderId) {

		List<csord__Order_Line_Item__c> olis = new List<csord__Order_Line_Item__c>();

		for (EP_RetrospectiveOrderRequestStub.OrderLine navOli : order.orderLines.orderLine) {

			csord__Order_Line_Item__c oli = new csord__Order_Line_Item__c();

			oli.csord__Order__c = orderId;
			oli.EP_SeqId__c = navOli.seqId; // Text
			oli.Quantity__c = navOli.qty; // Decimal
			oli.EP_Ambient_Loaded_Quantity__c = navOli.loadedAmbientQty; // Decimal
			oli.EP_Standard_Delivered_Quantity__c = navOli.deliveredAmbientQty; // Decimal
			oli.EP_Standard_Loaded_Quantity__c = navOli.loadedStandardQty; // Decimal
			oli.EP_Ambient_Delivered_Quantity__c = navOli.deliveredStandardQty; // Decimal
			oli.UnitPrice__c = navOli.unitPrice; // Decimal
            oli.csord__Identification__c = 'Draw Down Order Item';

			// oli.EP_Product_Id__c = internal.EP_Product_Code__c;
			// oli.csord__Identification__c = internal.EP_Staging_Order_Identifier__c;

			/*
			    public Identifier identifier { get; set; }
			    public String itemId { get; set; }
			    public String uom { get; set; }
			    public Bols bols { get; set; }
			*/

			olis.add(oli);
		}

		return olis;
	}

	/**
	* @Author       Accenture
	* @Name         createResponse
	* @Date         03/25/2017
	* @Description
	* @Param
	* @return
	*/
	@TestVisible
	private static void createResponse(string seqId, string errorCode, string errorDescription) {
		EP_GeneralUtility.Log('Private', 'EP_RetrospectiveOrderRequestHandler', 'createResponse');
		if (String.isNotBlank(errorDescription)) processingFailed = true; {
			ackResponseList.add(EP_AcknowledgementUtil.createDataSet(EP_Common_Constant.NAV_ORDER_NEW, seqId, errorCode, errorDescription));
		}
	}

	private static void startOrchestrationProcess(csord__Order__c currentOrder, Id processTemplateId) {

		List<CSPOFA__Orchestration_Process_Template__c> orchTemplates = [SELECT Id, Name
		        FROM CSPOFA__Orchestration_Process_Template__c
		        WHERE Id = :processTemplateId];

		List<CSPOFA__Orchestration_Process__c> orchProcessesToCreate = new List<CSPOFA__Orchestration_Process__c>();

		// create process for every order
		orchProcessesToCreate.add(new CSPOFA__Orchestration_Process__c(Order__c = currentOrder.Id));

		// populate correctly Orch Processes which are going to be inserted
		for (CSPOFA__Orchestration_Process__c orchp : orchProcessesToCreate) {

			orchp.CSPOFA__Orchestration_Process_Template__c = orchTemplates[0].Id;
			orchp.Name = 'Drow Down Order';
			orchp.CSPOFA__Priority__c = '2 - Normal';
			orchp.CSPOFA__Processing_Mode__c = 'Foreground';
			orchp.CSPOFA__target_date__c = System.today() + 1;
			orchp.CSPOFA__target_date_time__c = System.today() + 1;
		}

		if (orchProcessesToCreate.size() > 0) {
			insert orchProcessesToCreate;
		}
	}

	private static Id getDrowDownOrderOrchestrationProcessTemplateId() {

		CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();

		Id drowDownOrderTemplateId = csOrderSetting.Drow_Down_Order_Orchestration_Process_Id__c;

		if (String.isBlank(drowDownOrderTemplateId)) {
			throw new EP_IntegrationException('Invalid CS_ORDER_SETTINGS__c orchestration Id!');
		}

		return drowDownOrderTemplateId;
	}
}