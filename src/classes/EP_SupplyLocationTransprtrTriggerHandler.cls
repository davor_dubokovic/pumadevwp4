/*  
@Author <Jai Singh>
@name <EP_SupplyLocationTransprtrTriggerHandler>
@CreateDate <16/06/2016>
@Description <Handler class for trigger EP_SupplyLocationTransporterTrigger on object EP_Supply_Location_Transporter__c>  
@Version <1.0>
*/
public without sharing class EP_SupplyLocationTransprtrTriggerHandler 
{
	private static final string CLASSNAME = 'EP_SupplyLocationTransprtrTriggerHandler';
	private static final string BEFORE_INSERT_METHOD = 'doBeforeInsert';
	private static final string BEFORE_UPDATE_METHOD = 'doBeforeUpdate';
	private static final string DO_BEFORE_DELETE = 'doBeforeDelete';
	private static final string ERROR_MESSAGE = 'Please ensure atleast one default transporter is associated with this Supply Location';

	public static Boolean isExecuteUpdate = true;

	/**
	* @author <Jai Singh>
	* @date <16/06/2016>
	* @name <doBeforeInsert>
	* @description <This method handles before insert>
	* @param list<EP_Supply_Location_Transporter__c>
	* @return void
	*/  
	public static void  doBeforeInsert( list<EP_Supply_Location_Transporter__c> listSupplyLocations ) {
		try{        
			map<Id,list<EP_Supply_Location_Transporter__c>> mapSupplyLocationIdSupplyLocations = new map<Id,list<EP_Supply_Location_Transporter__c>>();
			set<Id> setSupplyLocationIDs = new set<Id>();
			list<EP_Supply_Location_Transporter__c> listNonDefaultSupplyLocations = new list<EP_Supply_Location_Transporter__c>();
			list<EP_Supply_Location_Transporter__c> listSupplyLocationTransporters = null;
			for( EP_Supply_Location_Transporter__c supplyLocationObject : listSupplyLocations ){
				supplyLocationObject.EP_Supply_Location_Transporter_Key__c = supplyLocationObject.EP_Supply_Location__c + EP_Common_Constant.STRING_HYPHEN + supplyLocationObject.EP_Transporter__c;
				if( supplyLocationObject.EP_Is_Default__c ){
					if( !mapSupplyLocationIdSupplyLocations.containsKey( supplyLocationObject.EP_Supply_Location__c ) ){    
						listSupplyLocationTransporters = new list<EP_Supply_Location_Transporter__c>();
						mapSupplyLocationIdSupplyLocations.put( supplyLocationObject.EP_Supply_Location__c, listSupplyLocationTransporters);
					}
					mapSupplyLocationIdSupplyLocations.get( supplyLocationObject.EP_Supply_Location__c ).add( supplyLocationObject );
				}
				else{
					setSupplyLocationIDs.add( supplyLocationObject.EP_Supply_Location__c );
					listNonDefaultSupplyLocations.add( supplyLocationObject );
				}
			}

			if( !mapSupplyLocationIdSupplyLocations.keyset().isEmpty() ){
				EP_SupplyLocationTransprtrTriggerHelper.handleDefaultTransporterChange( mapSupplyLocationIdSupplyLocations );
			}
			if( !listNonDefaultSupplyLocations.isEmpty() ){
				EP_SupplyLocationTransprtrTriggerHelper.checkExistingDefaultTransporter( setSupplyLocationIDs, listNonDefaultSupplyLocations ); 
			}
		}
		catch(Exception handledException){
			EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA, BEFORE_INSERT_METHOD, CLASSNAME, ApexPages.Severity.ERROR);    
		}
	}

	/**
	* @author <Jai Singh>
	* @date <16/06/2016>
	* @name <doBeforeUpdate>
	* @description <This method handles before update>
	* @param map<Id,EP_Supply_Location_Transporter__c>, map<Id,EP_Supply_Location_Transporter__c> 
	* @return void
	*/  
	public static void doBeforeUpdate( map<Id,EP_Supply_Location_Transporter__c> mapOldSupplyLocationIdSupplyLocations, map<Id,EP_Supply_Location_Transporter__c> mapNewSupplyLocationIdSupplyLocations ) {
		try{
			map<Id,list<EP_Supply_Location_Transporter__c>> mapSupplyLocationIdSupplyLocations = new map<Id,list<EP_Supply_Location_Transporter__c>>();
			list<EP_Supply_Location_Transporter__c> listSupplyLocationTransporters = null;
			for( EP_Supply_Location_Transporter__c supplyLocationObject : mapNewSupplyLocationIdSupplyLocations.values() ) {
				if( !supplyLocationObject.EP_Is_Default__c && mapOldSupplyLocationIdSupplyLocations.get( supplyLocationObject.Id ).EP_Is_Default__c ) {
					supplyLocationObject.addError( ERROR_MESSAGE );
				}
				else if( supplyLocationObject.EP_Is_Default__c && !mapOldSupplyLocationIdSupplyLocations.get( supplyLocationObject.Id ).EP_Is_Default__c ) {
					if( !mapSupplyLocationIdSupplyLocations.containsKey( supplyLocationObject.EP_Supply_Location__c ) ) {
						listSupplyLocationTransporters = new list<EP_Supply_Location_Transporter__c>();
						mapSupplyLocationIdSupplyLocations.put( supplyLocationObject.EP_Supply_Location__c,  listSupplyLocationTransporters);
					}
					mapSupplyLocationIdSupplyLocations.get( supplyLocationObject.EP_Supply_Location__c ).add( supplyLocationObject );
				}
			}
			EP_SupplyLocationTransprtrTriggerHelper.handleDefaultTransporterChange( mapSupplyLocationIdSupplyLocations );
		}
		catch(Exception handledException){
			EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA, BEFORE_UPDATE_METHOD, CLASSNAME, ApexPages.Severity.ERROR);    
		}
	}

	/**
	* @author <Jai Singh>
	* @date <16/06/2016>
	* @name <doBeforeDelete>
	* @description <This method handles before delete>
	* @param map<Id,EP_Supply_Location_Transporter__c>
	* @return void
	*/ 
	public static void  doBeforeDelete( map<Id,EP_Supply_Location_Transporter__c> mapOldSupplyLocationIdSupplyLocations ) {
		try{
			for( EP_Supply_Location_Transporter__c supplyLocationObject : mapOldSupplyLocationIdSupplyLocations.values() ) {
				if( supplyLocationObject.EP_Is_Default__c ) {
					supplyLocationObject.addError( ERROR_MESSAGE );
				}
			}
		}
		catch(Exception handledException){
			EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA, DO_BEFORE_DELETE, CLASSNAME, ApexPages.Severity.ERROR);    
		}
	}
}