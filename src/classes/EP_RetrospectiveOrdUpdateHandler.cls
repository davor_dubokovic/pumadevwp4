/*
   @Author          CS
   @name            EP_RetrospectiveOrdUpdateHandler
   @CreateDate      26/04/2018
   @Description     Retrospective Order Update Handler
   @Version         1.0
*/
public class EP_RetrospectiveOrdUpdateHandler extends EP_InboundHandler {

	private static List<EP_AcknowledgementStub.dataSet> ackResponseList = new list <EP_AcknowledgementStub.dataSet> ();
	private static boolean processingFailed = false;

	public override string processRequest(string jsonRequest) {

		Map<String, Decimal> contractQuantity = new Map<String, Decimal>();

		EP_GeneralUtility.Log('public', 'EP_RetrospectiveOrdUpdateHandler', 'processRequest');
		EP_MessageHeader headerCommon = new EP_MessageHeader();
		string failureReason;

		try {

			EP_RetrospectiveOrdUpdateStub obj = (EP_RetrospectiveOrdUpdateStub)JSON.deserialize(jsonRequest, EP_RetrospectiveOrdUpdateStub.class);

			// Create contract - quantity map
			for (EP_RetrospectiveOrdUpdateStub.SalesContract contract : obj.msg.payload.any0.salesContracts.salesContract) {

				Decimal totalPerContract = 0;

				for (EP_RetrospectiveOrdUpdateStub.Line contractLineItem : contract.lines.line) {

					totalPerContract = totalPerContract + contractLineItem.consumedQty;
				}

				contractQuantity.put(contract.identifier.contractNr, totalPerContract);
			}


            Set<Id> contractsThatShouldBeClosed = new Set<Id>();

			// Update Contract quantity
			if (!contractQuantity.isEmpty()) {

				List<csord__Order__c> contracts = [SELECT Id, Name, RecordType.Name, OrderNumber__c, EP_Total_Order_Quantity__c
                                                    FROM csord__Order__c 
                                                    WHERE RecordType.Name = 'Contract' AND 
                                                        OrderNumber__c IN :contractQuantity.keySet() AND 
                                                            csord__Primary_Order__c = null];
                                                            
                // There should be only one Contract
				for (csord__Order__c ct : contracts) {

                    if (!contractQuantity.containsKey(ct.OrderNumber__c)) {
                        continue;
                    }

                    Decimal previousValue = Decimal.valueOf(ct.EP_Total_Order_Quantity__c);

                    if (previousValue < contractQuantity.get(ct.OrderNumber__c)) {
                        contractsThatShouldBeClosed.add(ct.Id);
                    }

					ct.EP_Total_Order_Quantity__c = String.valueOf(contractQuantity.get(ct.OrderNumber__c));
				}

				update contracts;

				// Orchestration process should be added to close respective order

				Id processTemplateId = EP_RetrospectiveOrdUpdateHandler.getCloseContractOrchestratorProcessTemplateId();

                // There should be only one Contract
                for (csord__Order__c ct : contracts) {

                    // Orchestration process should close the respective order
                    if (contractsThatShouldBeClosed.contains(ct.Id)) {                    
                        EP_RetrospectiveOrdUpdateHandler.startOrchestrationProcess(ct, processTemplateId);
                    }
                }
			}

		} catch (Exception ex) {

			failureReason = ex.getMessage();
			EP_LoggingService.logServiceException(ex, UserInfo.getOrganizationId(), EP_Common_constant.EPUMA, 'processRequest', 'EP_WINDMSOrderNewHandler',  EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF, EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);

			createResponse(null, ex.getTypeName(), ex.getMessage());
		}

		return EP_AcknowledgementHandler.createAcknowledgement(EP_Common_Constant.NAV_TO_SFDC_CONTRACT_QUANTITY_UPDATE, processingFailed, failureReason, headerCommon, ackResponseList);
	}

	private static void createResponse(string seqId, string errorCode, string errorDescription) {

		EP_GeneralUtility.Log('Private', 'EP_RetrospectiveOrdUpdateHandler', 'createResponse');

		if (String.isNotBlank(errorDescription)) processingFailed = true; {
			ackResponseList.add(EP_AcknowledgementUtil.createDataSet(EP_Common_Constant.NAV_TO_SFDC_CONTRACT_QUANTITY_UPDATE, seqId, errorCode, errorDescription));
		}
	}

	private static void startOrchestrationProcess(csord__Order__c currentOrder, Id processTemplateId) {

		List<CSPOFA__Orchestration_Process_Template__c> orchTemplates = [SELECT Id, Name
		        FROM CSPOFA__Orchestration_Process_Template__c
		        WHERE Id = :processTemplateId];

		List<CSPOFA__Orchestration_Process__c> orchProcessesToCreate = new List<CSPOFA__Orchestration_Process__c>();

		// create process for every order
		orchProcessesToCreate.add(new CSPOFA__Orchestration_Process__c(Order__c = currentOrder.Id));

		// populate correctly Orch Processes which are going to be inserted
		for (CSPOFA__Orchestration_Process__c orchp : orchProcessesToCreate) {

			orchp.CSPOFA__Orchestration_Process_Template__c = orchTemplates[0].Id;
			orchp.Name = 'Drow Down Quantity';
			orchp.CSPOFA__Priority__c = '2 - Normal';
			orchp.CSPOFA__Processing_Mode__c = 'Foreground';
			orchp.CSPOFA__target_date__c = System.today() + 1;
			orchp.CSPOFA__target_date_time__c = System.today() + 1;
		}

		if (orchProcessesToCreate.size() > 0) {
			insert orchProcessesToCreate;
		}
	}

	private static Id getCloseContractOrchestratorProcessTemplateId() {

		CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();

		Id closeContractOrchestration = csOrderSetting.Contract_Close_Orchestrator_Process_Id__c;

		if (String.isBlank(closeContractOrchestration)) {
			throw new EP_IntegrationException('Invalid CS_ORDER_SETTINGS__c orchestration Id!');
		}

		return closeContractOrchestration;
	}
}