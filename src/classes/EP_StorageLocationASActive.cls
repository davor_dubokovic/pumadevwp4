/*
 *  @Author <Accenture>
 *  @Name <EP_StorageLocationASActive >
 *  @CreateDate <6/3/2017>
 *  @Description < Account State for  05-Active Status>
 *  @Version <1.0>
 */
 public with sharing class EP_StorageLocationASActive extends EP_AccountState{
     
    public EP_StorageLocationASActive() {
        
    }

    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASActive','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }

    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASActive','doOnEntry');
        
    }  

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASActive','doOnExit');
        
    }
    
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASActive','doTransition');
        return super.doTransition();
    }

    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASActive','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    } 
}