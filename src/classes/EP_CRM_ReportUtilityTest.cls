@isTest 
private class EP_CRM_ReportUtilityTest {
    @testSetup 
    static void dataSetup() {
        
    }
    
    @isTest 
    static void testPickValues() {
        List < SelectOption > options = null;
        Test.startTest();
            options = EP_CRM_ReportUtility.getPickValues((new Opportunity()), 'StageName', '--None--');
        Test.stopTest();
        System.assertNotEquals(options, null);
    }
    
    @isTest 
    static void testChildRoleUsers() {
        UserRole testRole1 = EP_CRM_TestDataFactory.createUserRole('Global Executive', UserInfo.getUserRoleId());
        insert testRole1;
        System.assertNotEquals(testRole1, null);
        System.assertNotEquals(testRole1.Id, null);
        
        UserRole testRole2 = EP_CRM_TestDataFactory.createUserRole('US Executive', testRole1.Id);
        insert testRole2;
        System.assertNotEquals(testRole2, null);
        System.assertNotEquals(testRole2.Id, null);
        
        UserRole testRole3 = EP_CRM_TestDataFactory.createUserRole('AUS Executive', testRole2.Id);
        insert testRole3;
        System.assertNotEquals(testRole3, null);
        System.assertNotEquals(testRole3.Id, null);
        
        Id profileId = EP_CRM_TestDataFactory.getProfileIdByName('System Administrator');
        System.assertNotEquals(profileId, null);
        
        User testUser = EP_CRM_TestDataFactory.createTestUser(testRole3.Id, profileId, 'Global', 'User');
        insert testUser;
        System.assertNotEquals(testUser, null);
        System.assertNotEquals(testUser.Id, null);
        
        testUser = EP_CRM_TestDataFactory.createTestUser(testRole2.Id, profileId, 'Regional', 'User');
        insert testUser;
        System.assertNotEquals(testUser, null);
        System.assertNotEquals(testUser.Id, null);
        
        testUser = EP_CRM_TestDataFactory.createTestUser(testRole1.Id, profileId, 'Country', 'User');
        insert testUser;
        System.assertNotEquals(testUser, null);
        System.assertNotEquals(testUser.Id, null);
        
        System.runAs(testUser) {
            Test.startTest();
                Map < Id, User > childUsers = EP_CRM_ReportUtility.getChildRoleUsers(testRole1.Id, true);
                System.assertNotEquals(childUsers, null);           
            Test.stopTest();
        }
    }
    
    @isTest 
    static void testReportTargetDate() {
        UserRole testRole = EP_CRM_TestDataFactory.createUserRole('AUS Sales Retail', null);
        insert testRole;
        System.assertNotEquals(testRole, null);
        System.assertNotEquals(testRole.Id, null);
        
        Id profileId = EP_CRM_TestDataFactory.getProfileIdByName('System Administrator');
        System.assertNotEquals(profileId, null);
        
        User testUser = EP_CRM_TestDataFactory.createTestUser(testRole.Id, profileId, 'Sales', 'User');
        insert testUser;
        System.assertNotEquals(testUser, null);
        System.assertNotEquals(testUser.Id, null);
        
        System.runAs(testUser) {
            Company__c pumaCompany = EP_CRM_TestDataFactory.createPumaCompany('Puma Company', testUser);
            insert pumaCompany;
            System.assertNotEquals(pumaCompany, null);
            System.assertNotEquals(pumaCompany.Id, null);
        
            Account accountObj = EP_CRM_TestDataFactory.createAccount('Test Account', pumaCompany);
            insert accountObj;
            System.assertNotEquals(accountObj, null);
            System.assertNotEquals(accountObj.Id, null);
            
            //Added by Hasmukh Jain on 19-Feb-2018
            EP_Payment_Method__c paymentMtd = EP_CRM_TestDataFactory.createPaymentMethod(pumaCompany);
            //insert paymentMtd;
            System.assertNotEquals(paymentMtd, null);
            System.assertNotEquals(paymentMtd.Id, null);
            
            //Added by Hasmukh Jain on 19-Feb-2018
            EP_Payment_Term__c paymentTerm = EP_CRM_TestDataFactory.createPaymentTerm(pumaCompany);
            //insert paymentTerm;
            System.assertNotEquals(paymentTerm, null);
            System.assertNotEquals(paymentTerm.Id, null);
            
            Opportunity opptyObj = EP_CRM_TestDataFactory.createOpportunity('Test Opportunity', accountObj,paymentMtd,paymentTerm);
            insert opptyObj;
            System.assertNotEquals(opptyObj, null);
            System.assertNotEquals(opptyObj.Id, null);
            
            Contract contractObj = EP_CRM_TestDataFactory.createContract(opptyObj);
            insert contractObj;
            System.assertNotEquals(opptyObj, null);
            System.assertNotEquals(opptyObj.Id, null);
            
            contractObj.Status = 'Activated';
            update contractobj;
            
            opptyObj = [SELECT Id, Name, CloseDate, (SELECT Id, StartDate, EP_CRM_Contract_Expiry_Date__c FROM Contracts__r WHERE StatusCode IN ('Activated')) FROM Opportunity limit 1]; 
            // System.assertNotEquals(opptyObj.Contracts__r[0], null);
            // System.assertNotEquals(opptyObj.Contracts__r[0].StartDate, System.today() - 5);
            Test.startTest();
                Date supplyDate = EP_CRM_ReportUtility.getReportTargetDate(opptyObj);
                System.assertEquals(supplyDate, System.today());        
                Integer remainingMonth = EP_CRM_ReportUtility.getForcastMonthCount(opptyObj);
            Test.stopTest();
        }
    }
    
    @isTest 
    static void testMultipleSelectedValues() {
        String multiSelectValue = 'Fuels;LPG;';
        Test.startTest();
            multiSelectValue = EP_CRM_ReportUtility.getMultipleSelectedValues(multiSelectValue);        
        Test.stopTest();
        System.assertEquals(multiSelectValue, '\'Fuels\',\'LPG\'');
    }
    
    @isTest 
    static void testConvertionRate() {
        // Set fromUoM to LITRE
        String fromUoM = EP_CRM_Constants.BLANK;
        String toUoM = EP_CRM_Constants.BLANK;
        Test.startTest();
            // Convert BLANK to BLANK (OTHER)
            Decimal convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 1);
            
            // Convert LITRE to BLANK
            toUoM = EP_CRM_Constants.BLANK;
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 1);
                    
            // Set fromUoM to LITRE
            fromUoM = EP_CRM_Constants.LITRE;       
                    
            // Convert LITRE to KILOGRAM
            toUoM = EP_CRM_Constants.KILOGRAM;
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 1);    
            
            // Convert LITRE to GALLON
            toUoM = EP_CRM_Constants.GALLON;    
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 0.26417205);
            
            // Convert LITRE to CUBIC_MTR
            toUoM = EP_CRM_Constants.CUBIC_MTR; 
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 0.001);
            
            // Convert LITRE to OTHER
            toUoM = EP_CRM_Constants.UNIT;  
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 1);
            
            // Set fromUoM to KILOGRAM
            fromUoM = EP_CRM_Constants.KILOGRAM;
            
            // Convert KILOGRAM to LITRE
            toUoM = EP_CRM_Constants.LITRE;
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 1);
            
            // Convert KILOGRAM to METRIC_TON
            toUoM = EP_CRM_Constants.METRIC_TON;
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 0.001);
            
            // Convert KILOGRAM to UNIT
            toUoM = EP_CRM_Constants.UNIT;
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 1);
            
            // Set fromUoM to METRIC_TON
            fromUoM = EP_CRM_Constants.METRIC_TON;
            
            // Convert METRIC_TON to KILOGRAM
            toUoM = EP_CRM_Constants.KILOGRAM;
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 1000);
            
            // Convert METRIC_TON to UNIT
            toUoM = EP_CRM_Constants.UNIT;
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 1);
            
            // Set fromUoM to GALLON
            fromUoM = EP_CRM_Constants.GALLON;
            
            // Convert GALLON to LITRE
            toUoM = EP_CRM_Constants.LITRE;
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 3.7854);
            
            // Convert GALLON to KILOGRAM
            toUoM = EP_CRM_Constants.KILOGRAM;
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 3.7854);
            
            // Convert GALLON to METRIC_TON
            toUoM = EP_CRM_Constants.METRIC_TON;
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 0.0038);
            
            // Convert GALLON to CUBIC_MTR
            toUoM = EP_CRM_Constants.CUBIC_MTR;
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 0.0038);
            
            // Convert GALLON to UNIT
            toUoM = EP_CRM_Constants.UNIT;
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 1);
            
            // Set fromUoM to CUBIC_MTR
            fromUoM = EP_CRM_Constants.CUBIC_MTR;
            
            // Convert CUBIC_MTR to LITRE
            toUoM = EP_CRM_Constants.LITRE;
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 1000);
            
            // Convert CUBIC_MTR to GALLON
            toUoM = EP_CRM_Constants.GALLON;
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 264.172052);
            
            // Convert CUBIC_MTR to UNIT
            toUoM = EP_CRM_Constants.UNIT;
            convertedValue = EP_CRM_ReportUtility.getConvertionRate(fromUoM, toUoM);
            System.assertEquals(convertedValue, 1);
        Test.stopTest();
    }
}