/* 
  @Author <Gautam Manchanda>
   @name <EP_GenerateVMINotificatBatchClass_Test>
   @CreateDate <17/12/2015>
   @Description <This class is used for generation of the notification and dips for user entry>
   @Version <1.0>
 
*/
@istest
private class EP_GenerateVMINotificatBatchClass_Test {
 
    private static List<Account> accList; 
    private static List<Account> shipToList;
    private static List<EP_Tank_Dip__c> tankDipList; 
    private static EP_Tank__c TankInstance; 
    private static Account currentAccount; 
    private static EP_VMI_Notification_Configuration__c setting;
    private static user currentrunningUser;

    
    /**************************************************************************
    *@Description : This method is used to create Data.                       *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/    
     private static void init(){
         
         EP_Country__c coun = new EP_Country__c();
         coun.Name = 'Australia';
         coun.EP_Country_Code__c = 'AU';
         insert coun; 
         currentrunningUser=EP_TestDataUtility.createTestRecordsForUser();
         //setting= EP_TestDataUtility.createTestRecordsForCustomSetting('AU','05:00');
         System.runas(currentrunningUser){  
            setting= EP_TestDataUtility.createTestRecordsForCustomSetting('AU',String.valueof(system.now().addMinutes(-60).hour())+':'+String.valueof(system.now().minute()));
            System.debug('setting---'+setting.EP_Time__c);
         }
         //User currentuser = new user(id=UserInfo.getUserId());
         //currentuser.EP_User_UTC_Offset__c =0;
         //update currentuser;
         Id recordTypeId =  EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ
                                                                ,EP_Common_Constant.VMI_SHIP_TO );
         EP_TestDataUtility.WrapperCustomerHierarchy customerHiearchy;
         customerHiearchy = EP_TestDataUtility.createCustomerHierarchyForNAV(1);
         customerHiearchy.lShipToAccounts[0].EP_Status__C = EP_Common_Constant.STATUS_ACTIVE ;
         customerHiearchy.lShipToAccounts[0].RecordTypeId = recordTypeId;
         customerHiearchy.lShipToAccounts[0].EP_Ship_To_UTC_Timezone__c = 'UTC+05:30';
         customerHiearchy.lShipToAccounts[0].EP_Country__c = coun.id;
         customerHiearchy.lShipToAccounts[0].EP_Tank_Dip_Entry_Mode__c = Label.EP_Portal_Dip_Entry_Mode_Value;
         customerHiearchy.lShipToAccounts[0].EP_Tank_Dips_Schedule_Time__c = String.valueof(system.now().addMinutes(60).hour())+':'+String.valueof(system.now().addMinutes(30).minute());
         customerHiearchy.lShipToAccounts[0].EP_Ship_To_Opening_Days__c ='Monday;Tuesday;Wednesday;Thursday;Friday;Saturday;Sunday' ;
         customerHiearchy.lShipToAccounts[0].Phone = '9015502179';
         customerHiearchy.lShipToAccounts[0].EP_Email__c = 'gautam.manchanda@accenture.com';
         
         //customerHiearchy.lShipToAccounts[0].EP_Ship_To_Current_Date_Time__c = System.now();
         customerHiearchy.lShipToAccounts[0].EP_Ship_To_UTC_Offset__c = 5.50; 
        
         
         Database.update(customerHiearchy.lShipToAccounts);
         customerHiearchy.lShipToAccounts[0].EP_Status__C = EP_Common_Constant.STATUS_ACTIVE;
         Database.update(customerHiearchy.lShipToAccounts);
         System.debug('value is'+customerHiearchy.lShipToAccounts[0].EP_Ship_To_Current_Date_Time__c);
         System.debug('value is '+ customerHiearchy.lShipToAccounts[0].EP_Tank_Dips_Schedule_Time__c);
         System.debug('value is'+customerHiearchy.lShipToAccounts[0].RecordType.DeveloperName);
         System.debug('value is'+customerHiearchy.lShipToAccounts[0].EP_Is_Ship_To_Open_Today__c );  
         /*accList= EP_TestDataUtility.createTestAccount(new List<Account>{new Account()});
         Account acc =accList.get(0);*/
         TankInstance =EP_TestDataUtility.createTestEP_Tank(customerHiearchy.lShipToAccounts[0]);
         TankInstance.EP_Capacity__c=200;
         TankInstance.EP_Tank_Dip_Entry_Mode__c='Portal' ;
         
         update TankInstance;
         currentAccount = customerHiearchy.lShipToAccounts[0];
         /*acc.
         acc.
         acc.EP_Status__c = Label.EP_Active_Ship_To_Status_Label;
         acc.EP_Tank_Dips_Schedule_Time__c = String.valueof(system.now().hour())+':'+String.valueof(system.now().minute());
         Datetime dt = System.now();
         acc.EP_Ship_To_Opening_Days__c = dt.format('EEEE');
         insert acc;*/
         //tankDipList = EP_TestDataUtility.createTestEP_Tank_Dip(List<EP_Tank_Dip__c> ,TankInstance.id)
         tankDipList = EP_TestDataUtility.createTestEP_Tank_Dip (new List<EP_Tank_Dip__c>{new EP_Tank_Dip__c(EP_Tank__c=TankInstance.id)});
         // Delete existing placeholder tank dips
         List<EP_Tank_Dip__c> lExistingPlaceholderTankDips = [SELECT ID FROM EP_Tank_Dip__c WHERE EP_Tank__r.EP_Ship_To__c = :customerHiearchy.lShipToAccounts[0].Id];
         Database.Delete(lExistingPlaceholderTankDips);
     }
    
    private static testMethod void testDipCreation(){ 
       test.StartTest(); 
       init();
        test.StopTest();
        
    }
     
    /**************************************************************************
    *@Description : This method is used to test scheduler                     *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/     
    private static testMethod void testBatch4Scendule1(){ 
       init();   
       integer hour = system.now().hour()-1;
       currentAccount.EP_Tank_Dips_Schedule_Time__c = String.valueof(hour)+':'+String.valueof(system.now().minute());   //'12:00';
       currentAccount.EP_Ship_To_UTC_Offset__c = 5.50;//Decimal.valueof(0);//03.00;
       update currentAccount;
       test.StartTest();
         system.runas(currentrunningUser){    
              List<EP_Tank_Dip__c> lExistingPlaceholderTankDips = [SELECT ID FROM EP_Tank_Dip__c WHERE EP_Tank__r.EP_Ship_To__c = :currentAccount.Id];
         Database.Delete(lExistingPlaceholderTankDips); 
             String CRON_EXP = '0 0 * * * ?';
           EP_ScheduleVMINotificationsBatchClass_R1 sch = new EP_ScheduleVMINotificationsBatchClass_R1 ();
           
           system.schedule('Schedule Dips Generation Hourly test1', CRON_EXP, sch);
        }
        test.stopTest();
     }
     
      /**************************************************************************
    *@Description : This method is used to test scheduler                     *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/     
    private static testMethod void testBatch4Scendule2(){ 
       init();   
       integer hour = system.now().hour();
       currentAccount.EP_Tank_Dips_Schedule_Time__c=String.valueof(hour)+':'+String.valueof(system.now().minute());   //'12:00';
       currentAccount.EP_Ship_To_UTC_Offset__c=5.50;//Decimal.valueof(0);//03.00;
       update currentAccount;
       test.StartTest();
         system.runas(currentrunningUser){    
              List<EP_Tank_Dip__c> lExistingPlaceholderTankDips = [SELECT ID FROM EP_Tank_Dip__c WHERE EP_Tank__r.EP_Ship_To__c = :currentAccount.Id];
         Database.Delete(lExistingPlaceholderTankDips); 
             String CRON_EXP = '0 0 * * * ?';
           EP_ScheduleVMINotificationsBatchClass_R1 sch = new EP_ScheduleVMINotificationsBatchClass_R1 ();
           system.schedule('Schedule Dips Generation Hourly test1', CRON_EXP, sch);
        }
        test.stopTest();
     }
     
     
     public static testMethod void testExecute(){ 
       init();   
       test.StartTest();
       system.runas(currentrunningUser){              
           EP_GenerateVMINotificationsBatchClass_R1 gen = new EP_GenerateVMINotificationsBatchClass_R1();
           database.executebatch(gen); 
       }
        test.stopTest();
     } 
}