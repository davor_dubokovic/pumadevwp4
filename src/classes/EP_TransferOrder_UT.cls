@isTest
public class EP_TransferOrder_UT
{
    // This method is belong to Supper class , its covered in EP_OrderType_UT
    /*static testMethod void doSubmitActions_test() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObject();
        localObj.setOrderDomain(obj);
        Test.startTest();
        localObj.doSubmitActions();
        Test.stopTest();
        Account accountObj = [SELECT Id , EP_Email__c , EP_Payment_Term_Lookup__c,EP_Bill_To_Account__r.EP_Payment_Term_Lookup__c , EP_Bill_To_Account__r.EP_Email__c FROM Account WHERE id =: obj.getOrder().AccountId ];
        System.AssertEquals(obj.getOrder().EP_Email__c ,accountObj.EP_Email__c); 
        System.AssertEquals(obj.getOrder().EP_Payment_Term_Value__c , accountObj.EP_Payment_Term_Lookup__c);
    }*/
    static testMethod void setOrderDomain_test() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObject();
        Test.startTest();
        localObj.setOrderDomain(obj);
        Test.stopTest();
        System.AssertEquals(obj,localObj.orderDomainObj);
       
    }
    static testMethod void hasCreditIssue_positivetest() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObjectPositiveScenario();
        localObj.setOrderDomain(obj);
        Test.startTest();
        Boolean result = localObj.hasCreditIssue();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void hasCreditIssue_negativetest() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObjectNegativeScenario();
        localObj.setOrderDomain(obj);
        Test.startTest();
        Boolean result = localObj.hasCreditIssue();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void getShipTos_test() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObject();
        localObj.setOrderDomain(obj);
        csord__Order__c orderObj = obj.getOrder();
        Id accountId = orderObj.accountid__c;
        Test.startTest();
        List<Account> result = localObj.getShipTos(accountId);
        Test.stopTest();
        System.AssertEquals(true, result.size()>0);
    }

    // This method is belong to Supper class , its covered in EP_OrderType_UT
    /*static testMethod void getOperationalTanks_test() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObject();
        localObj.setOrderDomain(obj);
        Id accountId = obj.getOrder().EP_ShipTo__c;
        Test.startTest();
        Map<Id, EP_Tank__c> result = localObj.getOperationalTanks(accountId);
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);
       
    }*/
    static testMethod void isValidLoadingDate_postivetest() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObjectPositiveScenario();
        localObj.setOrderDomain(obj);
        csord__Order__c ord = obj.getOrder();
        Test.startTest();
        Boolean result = localObj.isValidLoadingDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isValidLoadingDate_negativetest() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObjectNegativeScenario();
        localObj.setOrderDomain(obj);
        csord__Order__c ord = obj.getOrder();
        Test.startTest();
        Boolean result = localObj.isValidLoadingDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isValidExpectedDate_postivetest() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObjectPositiveScenario();
        localObj.setOrderDomain(obj);
        csord__Order__c ord = obj.getOrder();
        Test.startTest();
        Boolean result = localObj.isValidExpectedDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isValidExpectedDate_negativetest() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObjectNegativeScenario();
        localObj.setOrderDomain(obj);
        csord__Order__c ord = obj.getOrder();
        Test.startTest();
        Boolean result = localObj.isValidExpectedDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void loadStrategies_test() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObject();
        localObj.setOrderDomain(obj);
        //EP_OrderDomainObject orderDomainObject;
        Test.startTest();
        localObj.loadStrategies(obj);
        Test.stopTest();
        System.AssertNotEquals(Null,localObj.consignmentType);
    }
    static testMethod void getAllRoutesOfShipToAndLocation_test() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObject();
        localObj.setOrderDomain(obj);
        csord__Order__c orderObj = obj.getorder();
        Id shipToId = orderObj.EP_ShipTo__c;
        Id stockholdinglocationId = orderObj.EP_Stock_Holding_Location__c;
        Test.startTest();
        List<EP_Route__c> result = localObj.getAllRoutesOfShipToAndLocation(shipToId,stockholdinglocationId);
        Test.stopTest();
        //Test Class Fix Start
        System.AssertEquals(1,result.size());
    }
    static testMethod void showCustomerPO_positivetest() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObjectPositiveScenario();
        localObj.setOrderDomain(obj);
        Test.startTest();
        Boolean result = localObj.showCustomerPO();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void showCustomerPO_negativetest() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObjectNegativeScenario();
        localObj.setOrderDomain(obj);
        Test.startTest();
        Boolean result = localObj.showCustomerPO();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void findRecordType_test() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObject();
        localObj.setOrderDomain(obj);
        Test.startTest();
        Id result = localObj.findRecordType();
        Test.stopTest();
        Id nonVMIRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER, EP_Common_Constant.TRANSFER_ORDER_RECORD_TYPE_NAME);
        System.AssertEquals(nonVMIRecordTypeId,result);
    }
    // This method is belong to Supper class , its covered in EP_OrderType_UT
    /*static testMethod void getApplicablePaymentTerms_test() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObject();
        localObj.setOrderDomain(obj);
        Account accountObject = [SELECT id,EP_Payment_Term_Lookup__c, EP_Package_Payment_Term__c FROM Account WHERE Id =: obj.getOrder().AccountId ];
        system.debug('*********'+accountObject.EP_Payment_Term_Lookup__c+'888'+accountObject.EP_Package_Payment_Term__c);
        Test.startTest();
        List<String> result = localObj.getApplicablePaymentTerms(accountObject);
        Set<String>resultSet = new Set<String>();
        resultSet.addAll(result);
        Test.stopTest();
        System.AssertEquals(true,resultSet.size()>0);
    } */
    static testMethod void getApplicableDeliveryTypes_test() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObject();
        localObj.setOrderDomain(obj);
        Test.startTest();
        List<String> result = localObj.getApplicableDeliveryTypes();
        Set<String> resultSet = new Set<String>();
        resultSet.addAll(result);
        Test.stopTest();
        System.AssertEquals(true,resultSet.contains(EP_Common_Constant.DELIVERY));
    }

    static testMethod void findPriceBookEntries_test() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObject();
        localObj.setOrderDomain(obj);
        csord__Order__c ordObj = obj.getOrder();
        Account accObj = [SELECT Id,EP_Pricebook__c FROM Account WHERE Id =:ordObj.AccountId__c LIMIT 1];
        Id pricebookId = accObj.EP_Pricebook__c;
        Test.startTest();
        List<PriceBookEntry> result = localObj.findPriceBookEntries(pricebookId,ordObj);
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);
    }
    static testMethod void isValidOrderDate_positivetest() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObjectPositiveScenario();
        localObj.setOrderDomain(obj);
        csord__Order__c ord = obj.getOrder();
        Test.startTest();
        Boolean result = localObj.isValidOrderDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isValidOrderDate_negativetest() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObjectNegativeScenario();
        localObj.setOrderDomain(obj);
        csord__Order__c ord = obj.getOrder();
        Test.startTest();
        Boolean result = localObj.isValidOrderDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void setRequestedDateTime_test() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObject();
        localObj.setOrderDomain(obj);
        csord__Order__c ord = obj.getOrder();
        String selectedDate = '25-03-2017';
        String availableSlots = '11:30:43';
        Test.startTest();
        csord__Order__c result = localObj.setRequestedDateTime(ord,selectedDate,availableSlots);
        Test.stopTest();
        System.AssertEquals(result.EP_Requested_Delivery_Date__c ,EP_GeneralUtility.convertSelectedDateToDateInstance(selectedDate));
        System.AssertEquals(result.EP_Requested_Pickup_Time__c ,String.valueOf(EP_GeneralUtility.convertSelectedHourToTime(availableSlots)).subString(0,8));
        
    }
    static testMethod void getDeliveryTypes_test() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObject();
        localObj.setOrderDomain(obj);
        Test.startTest();
        List<String> result = localObj.getDeliveryTypes();
        Test.stopTest();
        System.AssertEquals(1,result.size());

    }
    static testMethod void checkForNoTransporter_test() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObject();
        localObj.setOrderDomain(obj);
        String transporterName = EP_Common_Constant.NO_TRANSPORTER;
        Test.startTest();
        csord__Order__c result = localObj.checkForNoTransporter(transporterName);
        Test.stopTest();
        System.AssertEquals(EP_Common_Constant.PIPELINE,obj.getOrder().EP_Delivery_Type__c );
        
    }
    static testMethod void isOrderEntryValid_positivetest() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObjectPositiveScenario();
        localObj.setOrderDomain(obj);
        Integer numOfTransportAccount = EP_Common_Constant.ONE;
        Test.startTest();
        Boolean result = localObj.isOrderEntryValid(numOfTransportAccount);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isOrderEntryValid_negativetest() {        
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObjectNegativeScenario();
        EP_TransferOrder localObj = new EP_TransferOrder(obj);
        localObj.setOrderDomain(obj);
        Integer numOfTransportAccount= EP_Common_Constant.Zero;
        Test.startTest();
        Boolean result = localObj.isOrderEntryValid(numOfTransportAccount);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void setConsumptionOrderDetails_test() {
        EP_TransferOrder localObj = new EP_TransferOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getTransferOrderDomainObject();
        localObj.setOrderDomain(obj);
        Test.startTest();
        localObj.setConsumptionOrderDetails();
        Test.stopTest();
        System.AssertEquals(true,obj.getOrder().RecordTypeId != null);    

    }
}