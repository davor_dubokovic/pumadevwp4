/*
    @Author          Accenture
    @Name            EP_OrderCreditCheckXML
    @CreateDate      04/18/2017
    @Description     This class  is used to generate outbound XML's of Order Credit Check to Sync with WINDMS
    @Version         1.0
    @Reference       NA
*/
public class EP_OrderCreditCheckXML extends EP_GenerateOrderRequestXML{
    private final string SEND_ORDER_CREDIT_CHECK_IN_PROGRESS = 'SEND_ORDER_CREDIT_CHECK_IN_PROGRESS';
    private String creditStatus = EP_Common_Constant.BLANK;
    private String creditReason = EP_Common_Constant.BLANK;
    
    /**
    * @author           Accenture
    * @name             createXML
    * @date             04/18/2017
    * @description      This method is used to create XML for Order to Sync with NAV
    * @param            NA
    * @return           NA
    */
    public override string createXML(){
        EP_GeneralUtility.Log('Public','EP_OrderCreditCheckXML','createXML');
        return super.createXML();
    }
    
    /**
    * @author           Accenture
    * @name             createPayload
    * @date             04/18/2017
    * @description      This method is used to create Payload for Order to Sync with NAV
    * @param            NA
    * @return           NA
    */ 
    public override void createPayload(){
        EP_GeneralUtility.Log('Public','EP_OrderCreditCheckXML','createPayload');
        setCreditStatus();
        DOM.Document tempDoc = new DOM.Document();
        Dom.XMLNode creditChecksNode = tempDoc.createRootElement(EP_OrderConstant.creditChecks,null, null);
        Dom.XMLNode creditCheckNode = creditChecksNode.addChildElement(EP_OrderConstant.creditCheck,null,null);
        
        String seqid = EP_IntegrationUtil.reCreateSeqId(messageId, OrderObj.Id);
        creditCheckNode.addChildElement(EP_OrderConstant.seqId,null,null).addTextNode(getValueforNode(seqid)); //Value for seqId
        
        Dom.XMLNode IdentifierNode = creditCheckNode.addChildElement(EP_OrderConstant.Identifier,null,null);
        IdentifierNode.addChildElement(EP_OrderConstant.orderIdWinDMS,null,null).addTextNode(getValueforNode(orderobj.EP_WinDMS_Order_Number__c));
        IdentifierNode.addChildElement(EP_OrderConstant.orderIdSF,null,null).addTextNode(getValueforNode(orderobj.OrderNumber__c));
        IdentifierNode.addChildElement(EP_OrderConstant.orderRefNr,null,null).addTextNode(getValueforNode(orderobj.EP_WinDMS_Trip_Reference_Number__c));
        
        //Defect Fix Start : #57388- Check Null for the fields value by calling getValueforNode 
        creditCheckNode.addChildElement(EP_OrderConstant.creditStatus,null,null).addTextNode(getValueforNode(creditStatus)); //Value for creditStatus
        creditCheckNode.addChildElement(EP_OrderConstant.reasonCode,null,null).addTextNode(getValueforNode(creditReason)); //Value for reasonCode
        //Defect Fix End : #57388
        
        //End of Any node   
        
        //String payloadcontent = tempDoc.toXmlString().replace(EP_OrderConstant.XML_BEGIN,EP_Common_Constant.BLANK); 
        //system.debug('Payload is--' + payloadcontent + '--');
       
        Dom.XMLNode PayloadNode = MSGNode.addChildElement(EP_OrderConstant.Payload,null,null);
        Dom.XmlNode AnyNode = PayloadNode.addChildElement(EP_OrderConstant.any0,null,null);
        // Encoding payload by calling encode XML method in superclass
        AnyNode.addTextNode(encodeXML(tempDoc));   
    }
    
    /**
    * @author           Accenture
    * @name             createStatusPayLoad
    * @date             04/18/2017
    * @description      This method is used to create Status XML tags
    * @param            NA
    * @return           NA
    */ 
    public override void createStatusPayLoad(){
        EP_GeneralUtility.Log('Public','EP_OrderCreditCheckXML','createStatusPayLoad');
        MSGNode.addChildElement(EP_OrderConstant.STATUS_PAYLOAD,null,null);
    }
    
    /**
    * @author           Accenture
    * @name             setCreditStatus
    * @date             04/18/2017
    * @description      This method is used to set Credit Status for XML tags
    * @param            NA
    * @return           NA
    */ 
     @TestVisible
    private void setCreditStatus(){
        EP_GeneralUtility.Log('Private','EP_OrderCreditCheckXML','setCreditStatus');
        if(SEND_ORDER_CREDIT_CHECK_IN_PROGRESS.equalsignorecase(messageType)){
            creditStatus = EP_Common_Constant.CREDIT_IN_PROGRESS;
        }else{
            creditStatus = orderobj.EP_Credit_Check_Status_For_XML__c;
            creditReason = orderobj.Credit_Check_Reason_Code_XML__c;
        }
    }
    
}