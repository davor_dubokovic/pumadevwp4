@isTest
public class EP_IntegrationRecordMapper_UT
{

	static testMethod void getParentsRecords_test() {
		EP_IntegrationRecordMapper localObj = new EP_IntegrationRecordMapper();
		Account acc = EP_TestDataUtility.getSellTo();
		EP_IntegrationRecord__c intRecordNew = EP_TestDataUtility.createIntegrationRec(acc.Id,EP_Common_Constant.ACCOUNTS,DateTime.now(),EP_Common_Constant.AAF_COMPANY,EP_Common_Constant.SENT_STATUS);
		Database.insert(intRecordNew); 
		Set<id> integrationRecords = new Set<id>();
		integrationRecords.add(intRecordNew.id);
		Test.startTest();
		LIST<EP_IntegrationRecord__c> result = localObj.getParentsRecords(integrationRecords);
		Test.stopTest();
		System.assertEquals(1,result.size());
	}
	static testMethod void getRecordsByMessageIdOrSeqId_test() {
		EP_IntegrationRecordMapper localObj = new EP_IntegrationRecordMapper();
		Account acc = EP_TestDataUtility.getSellTo();
		EP_IntegrationRecord__c intRecordNew = EP_TestDataUtility.createIntegrationRec(acc.Id,EP_Common_Constant.ACCOUNTS,DateTime.now(),EP_Common_Constant.AAF_COMPANY,EP_Common_Constant.SENT_STATUS);
		Database.insert(intRecordNew); 
		String messageId = intRecordNew.EP_Message_ID__c;
		Set<string> setSeqIds;
		Test.startTest();
		LIST<EP_IntegrationRecord__c> result = localObj.getRecordsByMessageIdOrSeqId(messageId,setSeqIds);
		Test.stopTest();
		System.assertEquals(1,result.size());
	}
}