/* 
   @Author Accenture
   @name <EP_LoggingService>
   @CreateDate <20/11/2015>
   @Description <this is the utility class for creating EP_Exception_Log__c record if any exception occurs>
   @Version <1.0>
*/
public with sharing class EP_LoggingService
{
    public static Map<String, EP_Error_Handler_CS__c> errCustomSetting = EP_Error_Handler_CS__c.getAll(); 
    public static String errorDescription_NAV;
    // Variables to hold already logged exception
    private static Boolean isExceptionLogged
    {
        get {
            return (isExceptionLogged == NULL? false : isExceptionLogged);
        }
        set {
            isExceptionLogged = value;
        }
    }
    /*
    *************************************************************************
    *@Description : This method inserts the Exception Log record              *
    *@Params      :  EP_Exception_Log__c                                      *
    *@Return      : void                                                      *    
    *************************************************************************
    */            
    public static void logException(EP_Exception_Log__c exLogObj) 
    {       
        try { 
               if (exLogObj != null) 
               {
                  // Insert exception to ExceptionLog object
                  Database.Insert(exLogObj);
               }
        } 
        catch (DMLException e)
        {
            // Send an email or do something but don’t try to write to Exception Log again.
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            EP_ExceptionEmailTo__c emCus= EP_ExceptionEmailTo__c.getOrgDefaults();
            String[] toAddresses = new String[] {emCus.Email__C};
            mail.setToAddresses(toAddresses);
            mail.setSubject(Label.ExceptionEmailSubject);
            mail.setPlainTextBody(Label.ExceptionEmailBody+e.getMessage());
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }  
    /*
    *************************************************************************
    *@Description : This method returns the current application name          *
    *@Params      : none                                                      *
    *@Return      : string                                                    *    
    *************************************************************************
    */     
    public static string returnCurrentApp(){ 
        try{
            String appName = EP_Common_Constant.BLANK;
            // Get tab set describes for each app
            List<Schema.DescribeTabSetResult> tabSetDesc = Schema.describeTabs();    
            // Iterate through each tab set describe for each app and display the info
            boolean selected = false;
            for(Schema.DescribeTabSetResult tsr : tabSetDesc) {
                if (tsr.isSelected()) {
                    appname = tsr.getLabel();
                    break;
                }
            }
            return appName;
        }
        catch (DMLException ex)
        {
            errorDescription_NAV = ex.getMessage();
            return null;
        }
    }
    
    /*
    *************************************************************************
    *@Description : This method returns the value of severity                 *
    *@Params      : String                                                    *
    *@Return      : decimal                                                   *    
    *************************************************************************
    */  
    public static Decimal valueOfSeverity(String Severity){
        try{
            if(errCustomSetting.containsKey(severity)){
                return errCustomSetting.get(severity).value__c;
            }
        }
        catch (DMLException ex)
        {
            return 0;
        }
        return 0;
    }

    /*
    *************************************************************************
    *@Description : This method is for the web service exceptions FATAL/ERROR *
    * severity                                                                *                                                                     
    * Parses the exception and populates the ExceptionLog object by calling   *
    * logException method                                                     *
    *@Params      : exception, string                                         *
    *@Return      : void                                                      *    
    *************************************************************************
    */ 
    Public static void logServiceException(Exception ex, String orgID, String applicationName, 
                                                string methodName, string className, String severity, String ws_user, String ws_application, 
                                                string ws_callingArea, string ws_transactionID ) {
                  
        // Check if the severity matches certain threshold
        try{
            if(!isExceptionLogged) {
                isExceptionLogged = true;
                If (valueOfSeverity(severity) > errCustomSetting.get(EP_Common_Constant.THRESHOLD).value__c)
	            {      
	                // Instantiate a new ExceptionLog object
	                EP_Exception_Log__c exLogObj = new  EP_Exception_Log__c();
	                // Parse the exception and match the data to corresponding fields.
	                exLogObj.EP_exception_Type__c = ex.getTypeName();
	                exLogObj.EP_Application_Name__c = returnCurrentApp();
	                exLogObj.EP_OrgID__c = UserInfo.getOrganizationId();
	                exLogObj.EP_Class_Name__c = className;
	                exLogObj.EP_Method_Name__c = methodName;
	                exLogObj.EP_Severity__c = severity;
	                exLogObj.EP_Running_User__c = ws_user;
	                exLogObj.EP_WS_Transaction_ID__c = ws_transactionID;
	                exLogObj.EP_Handled__c = TRUE; // API changed
	                exLogObj.EP_WS_Calling_Application__c = ws_application; // API changed
	                exLogObj.EP_WS_Calling_Area__c = ws_callingArea;
	                
	                // Updated: 18/10/2016
	                // Update by: smark
	                // Defect: 27700
	                // Change to ensure that all fields of the Exception Log object are populated
	                exLogObj.EP_Component__c = className;
	                exLogObj = populateGenericExceptionFields(exLogObj, ex);
	                // End defect fix
	                logException(exLogObj);
	            }
            }
        }
        catch(Exception handledException){
            errorDescription_NAV = handledException.getMessage();
        }
    }  
    
    /*
    *************************************************************************
    *@Description : This method is used to populate the exception log object  *
    * with the details of the standard SFDC exception                         *
    *@Params      : EP_Exception_Log__c, Exception                            *
    *@Return      : void                                                      *    
    *************************************************************************
    */ 
    private static EP_Exception_Log__c populateGenericExceptionFields(EP_Exception_Log__c exLogObj, Exception ex) {
        if (exLogObj != NULL)
        {
            if (exLogObj.EP_OrgID__c == NULL)
                exLogObj.EP_OrgID__c = UserInfo.getOrganizationId();
            if (exLogObj.EP_Exception_Log_Timestamp__c == NULL)
                exLogObj.EP_Exception_Log_Timestamp__c = System.Now();
            if (exLogObj.EP_User_Session_ID__c == NULL)
            {
                if (UserInfo.getSessionId() != NULL)
                {
                    String strSession = UserInfo.getSessionId();
                    if (strSession.Length() > 500)
                        strSession = strSession.Left(500);
                    exLogObj.EP_User_Session_ID__c = strSession;
                }
            }
            if (exLogObj.EP_User_Role_ID__c == NULL)
                exLogObj.EP_User_Role_ID__c = UserInfo.getUserRoleId();
            if (exLogObj.EP_User_Profile_ID__c == NULL)
                exLogObj.EP_User_Profile_ID__c = UserInfo.getProfileId();
            if (exLogObj.EP_User_Locale__c == NULL)
                exLogObj.EP_User_Locale__c = UserInfo.getLocale();
                
            if (ex != NULL)
            {
                if (exLogObj.EP_Exception_Code__c == NULL)
                    exLogObj.EP_Exception_Code__c = ex.getTypeName();
                if (exLogObj.EP_Exception_Type__c == NULL)
                    exLogObj.EP_Exception_Type__c = ex.getTypeName();
                if (exLogObj.EP_Exception_Description__c == NULL)
                {
                    if (ex.getMessage() != NULL)
                    {
                        String strMessage = ex.getMessage();
                        if (strMessage.length() > 255)
                            strMessage = strMessage.Left(255);                          
                        exLogObj.EP_Exception_Description__c = strMessage;
                    }
                }
                if (exLogObj.EP_Exception_Details__c == NULL)
                {
                    if (ex.getStackTraceString() != NULL)
                    {
                        String strTrace = ex.getStackTraceString();
                        if (strTrace.length() > 20000)
                            strTrace = strTrace.Left(20000);            
                        exLogObj.EP_Exception_Details__c = strTrace;        
                    }
                }
            }
            if (exLogObj.EP_WS_Audit_User__c == NULL)
                exLogObj.EP_WS_Audit_User__c = UserInfo.getUserId();
        }
        
        return exLogObj;
    }
    
    /*
    *************************************************************************
    *@Description : This method is used in the catch block for any class and
                    deal with the handled exception occuring  
                    Parses the exception and populates the ExceptionLog object 
                    by calling logException method                            *
    *@Params      : Exception, string, ApexPages.Severity                     *
    *@Return      : void                                                      *    
    *************************************************************************
    */
    public static void  logHandledException(Exception ex, String applicationName, String methodName, 
                                            String className, ApexPages.Severity severity) 
    {
         try{
             if(!isExceptionLogged) {
                isExceptionLogged = true;
	             String sev = severity.name();
	             // Check if the severity matches certain threshold
	             If( (severity.equals(ApexPages.Severity.FATAL)) || (severity.equals(ApexPages.Severity.ERROR)))
	             {
	                 // parse the exception and match the data to corresponding fields.
	                 EP_Exception_Log__c exLogObj = new EP_Exception_Log__c();
	                 exLogObj.EP_exception_Type__c = ex.getTypeName();
	                 exLogObj.EP_Class_Name__c = className;
	                 exLogObj.EP_Method_Name__c = methodName;
	                 exLogObj.EP_Severity__c = sev;
	                 exLogObj.EP_Handled__c = true;//changed
	                 exLogObj.EP_OrgID__c = UserInfo.getOrganizationId();
	                 exLogObj.EP_Running_User__c =  UserInfo.getUserId();
	                 String excp = ex.getMessage();
	                 if(excp.length() >= 255)
	                 {
	                     excp = excp.Left(254);
	                 }
	                 exLogObj.EP_Exception_Description__c = excp.substring(0,excp.length());
	                 exLogObj.EP_Exception_Details__c = ex.getStackTraceString();
	                 exLogObj.EP_Application_Name__c = returnCurrentApp();
	                 exLogObj.EP_Exception_Log_Timestamp__c = System.Now();
	                 
	                 // Updated: 18/10/2016
	                 // Update by: smark
	                 // Defect: 27700
	                 // Change to ensure that all fields of the Exception Log object are populated
	                 exLogObj.EP_Component__c = className;
	                 exLogObj = populateGenericExceptionFields(exLogObj, ex);
	                 // End defect fix
	                 
	                 logException(exLogObj);
	             }
             }
         }
         catch(Exception handledException){
            errorDescription_NAV = handledException.getMessage();
         }
    }

    /*
    ***************************************************************************************
    *@Description : This method is used by track handled errors (not exceptions           *
    *@Params      : logHanledError, string                                               *
    *@Return      : void                                                                  *    
    ***************************************************************************************
    */        
    public static void  logHandledError(string errorType, string errorDescription, string applicationName, 
                                        string methodName, string className, ApexPages.Severity severity) 
    {
         try{
             if(!isExceptionLogged) {
                 isExceptionLogged = false;
                 String sev = severity.name();
	             // Check if the severity matches certain threshold
	             If( (severity.equals(ApexPages.Severity.FATAL)) || (severity.equals(ApexPages.Severity.ERROR)))
	             {
	                 // parse the exception and match the data to corresponding fields.
	                 EP_Exception_Log__c exLogObj = new  EP_Exception_Log__c();
	                 exLogObj.EP_exception_Type__c  = errorType;
	                 exLogObj.EP_Class_Name__c = className;
	                 exLogObj.EP_Method_Name__c = methodName;
	                 exLogObj.EP_Severity__c = sev;
	                 exLogObj.EP_Handled__c = true;//changed
	                 exLogObj.EP_OrgID__c = UserInfo.getOrganizationId();
	                 exLogObj.EP_Running_User__c =  UserInfo.getUserId();
	                 String excp = errorDescription;
	                 if(excp.length() >= 255) {
	                     excp = excp.substring(0,254);
	                 }
	                 exLogObj.EP_Exception_Description__c = excp.substring(0, excp.length());
	                 exLogObj.EP_Application_Name__c = returnCurrentApp();
	                 exLogObj.EP_Exception_Log_Timestamp__c = System.Now();
	                 
	                 // Updated: 18/10/2016
	                 // Update by: smark
	                 // Defect: 27700
	                 // Change to ensure that all fields of the Exception Log object are populated
	                 exLogObj.EP_Component__c = className;
	                 exLogObj = populateGenericExceptionFields(exLogObj, NULL);
	                 // End defect fix
	                 
	                 logException(exLogObj);
	             }
             }
         }
         catch(Exception handledException){
            errorDescription_NAV = handledException.getMessage();
         }
    }

    /*
    *************************************************************************
    *@Description : This method is used by Exception Email Handler            *
    *@Params      : EP_Exception_Log__c, string                               *
    *@Return      : void                                                      *    
    *************************************************************************
    */        
    public static void  logUnhandledException(EP_Exception_Log__c exLogObj, String severity) {
        try{
            if(!isExceptionLogged) {
                isExceptionLogged = true;
                // Check if the severity matches certain threshold
                If (valueOfSeverity(severity) > errCustomSetting.get(EP_Common_Constant.THRESHOLD).value__c){
                    return;
                }
                logException(exLogObj);
            }
        }
        catch(Exception handledException){
            errorDescription_NAV = handledException.getMessage();
        }
    }  

    /*
    *************************************************************************
    *@Description : This is the future method for asynchronous call method 
                    Populates the ExceptionLog object asynchronously by calling 
                    logException method   *
    *@Params      : String                                                    *
    *@Return      : void                                                      *    
    *************************************************************************
    */
    @future
    public static void futureLogException(string orgID, string applicationName, string classname,
                                              string methodName, String severity) {
        try{                                          
            if(!isExceptionLogged) {
                isExceptionLogged = true;
                // Check if the severity matches certain threshold
	            If (valueOfSeverity(severity) > errCustomSetting.get(EP_Common_Constant.THRESHOLD).value__c){
	                return;
	            }
	        
	            // instantiate a set values for a new ExceptionLog record
	            EP_Exception_Log__c exLogObj = new  EP_Exception_Log__c();
	            exLogObj.EP_Class_Name__c = className;
	            exLogObj.EP_Method_Name__c = methodName;
	            exLogObj.EP_Severity__c = severity;
	            exLogObj.EP_Handled__c = true; // API changed
	            exLogObj.EP_OrgID__c = orgID;
	            exLogObj.EP_Application_Name__c = applicationName;
	            
	            // Updated: 18/10/2016
	            // Update by: smark
	            // Defect: 27700
	            // Change to ensure that all fields of the Exception Log object are populated
	            exLogObj.EP_Component__c = className;
	            exLogObj = populateGenericExceptionFields(exLogObj, NULL);
	            // End defect fix
	            
	            logException(exLogObj);
            }
        }
         catch(Exception handledException){
            errorDescription_NAV = handledException.getMessage();
         }
    }
    
    /*
    *************************************************************************
    *@Description : This is the method to insert list of Exceptions           *
    *@Params      : EP_Exception_Log__c                                       *
    *@Return      : void                                                      *    
    *************************************************************************
    */
    public static void logListException(List <EP_Exception_Log__c> exceptionLogList) {
    
        try { 
            if(!isExceptionLogged) {
                isExceptionLogged = true;
                if (exceptionLogList.size() > 0) {
	                // Insert exception list to ExceptionLog object
	                Database.Insert(exceptionLoglist, false);
	            }
            }
            
        } catch (DMLException e) {
            //Send an email or do something but don’t try to write to Exception Log again
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            EP_ExceptionEmailTo__c emCus= EP_ExceptionEmailTo__c.getOrgDefaults();
            String[] toAddresses = new String[] {emCus.Email__C};
            mail.setToAddresses(toAddresses);
            mail.setSubject(Label.ExceptionEmailSubject);
            mail.setPlainTextBody(Label.ExceptionEmailBody+e.getMessage());
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        }
   }
   
   /*
    *************************************************************************
    *@Description : This method is used when a bulk data is being driven and
                    exception occurs.
                    Parses the exception and populates the ExceptionLog object 
                    synchronously by calling futureLogListException method    *
    *@Params      : Exception,string                                          *
    *@Return      : void                                                      *    
    *************************************************************************
    */
    public static void createListException (List<Exception> exList, string orgID, string applicationName,
                                                string classname,string method,  List<String> severityList, string remoteUser) {
        try{
            if(!isExceptionLogged) {
                isExceptionLogged = true;
                if (exList.size() > 0) {
	                List<string> exceptionCode = new List<String>();
	                List<string> exceptionType = new List<String>();
	                List<string> exceptionDetails = new List<String>();
	                List<string> exceptionDescription = new List<String>();
	                List<string> severity = new List<String>(); 
	                List<string> classNamerec = new List<String>();
	                List<string> methodNamerec = new List<String>();
	                Integer index;
	                
	                for (index =0 ; index < exList.size(); index++) 
	                {
	                    // Check if the severity matches certain threshold
	                    If (valueOfSeverity(severityList[index]) < errCustomSetting.get(EP_Common_Constant.THRESHOLD).value__c){
	                        continue;
	                    }
	                    //Parse the exception object and match the data to corresponding arrays.
	                    exceptionType.add(exList[index].getTypeName());
	                    classNamerec.add( Classname);
	                    methodnamerec.add(Method);
	                    severity.add(severityList[index]);        
	                
	                }
	                SyncLogListException(exceptionType, classNamerec, methodnamerec, severity); 
	            }
            }
        }
         catch(Exception handledException){
            errorDescription_NAV = handledException.getMessage();
         }
    }  

    /*
    *************************************************************************
    *@Description : This is the future method for asynchronous call list method *
    *@Params      : String                                                      *
    *@Return      : void                                                        *    
    *************************************************************************
    */
    public static void SyncLogListException (List<string> exceptionType, List<string> classname, 
                                                           List<string> method, string[] severity) {
        EP_Exception_Log__c exLogObj;
        try{
            if(exceptionType.size() > 0)
            {
                List<EP_Exception_Log__c> errLogObjectList = new List<EP_Exception_Log__c>();
                for ( integer index = 0; index < exceptionType.size(); index++) {
                     // Check if the severity matches certain threshold
                     If (valueOfSeverity(severity[index]) < errCustomSetting.get(EP_Common_Constant.THRESHOLD).value__c){
                          continue;
                     }
                    // Instantiate a set values for a new ExceptionLog record
                    exLogObj = new  EP_Exception_Log__c();
                    exLogObj.EP_Exception_Type__c = exceptionType[index];
                    exLogObj.EP_Method_Name__c = classname[index];
                    exLogObj.EP_Method_Name__c = method[index];
                    exLogObj.EP_Severity__c = severity[index];
                    exLogObj.EP_Handled__c = TRUE;
                    
                    // Updated: 18/10/2016
                    // Update by: smark
                    // Defect: 27700
                    // Change to ensure that all fields of the Exception Log object are populated
                    exLogObj.EP_Component__c = className[index];
                    exLogObj = populateGenericExceptionFields(exLogObj, NULL);
                    // End defect fixing
                    
                    errLogObjectList.add(exLogObj);
               }
               logListException(errLogObjectList); 
            }
        }
         catch(Exception handledException){
            errorDescription_NAV = handledException.getMessage();
         }
    }
    
    /*
    *************************************************************************
    *@Description : This method inserts list of the Exception Log record            *
    *@Params      :  list of EP_Exception_Log__c objetcs                            *
    *@Return      : void                                                            *    
    *************************************************************************
    */            
    private static void logException(list<EP_Exception_Log__c> exLogList) 
    {       
        try { 
           if (!exLogList.isEmpty()) {
              // Insert exception to ExceptionLog object
              Database.Insert(exLogList);
           }
        } 
        catch (DMLException e)
        {
            // Send an email or do something but don’t try to write to Exception Log again.
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            EP_ExceptionEmailTo__c emCus= EP_ExceptionEmailTo__c.getOrgDefaults();
            String[] toAddresses = new String[] {emCus.Email__C};
            mail.setToAddresses(toAddresses);
            mail.setSubject(Label.ExceptionEmailSubject);
            mail.setPlainTextBody(Label.ExceptionEmailBody+e.getMessage());
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}