@isTest
/*   
     @Author <Jyotsna Tiwari>
     @name <EP_FE_SubmitOrderEndpointTest.cls>     
     @Description <This class is the test class for EP_FE_SubmitOrderEndpoint>   
     @Version <1.1> 
*/
Private class EP_FE_SubmitOrderEndpointTest {
    /*
    private static Account billTo =  EP_TestDataUtility.createBillToAccount(); // returns bill to account
    private static EP_Freight_Matrix__c freightMatrix =EP_TestDataUtility.createFreightMatrix();// returns freight matrix
    private static User sysAdmUser = EP_FE_TestDataUtility.getRunAsUser();
    */

    /*********************************************************************************************
    @Author <Jyotsna Tiwari>
    @name <testSubmitCall>
    @CreateDate <17/05/2016>
    @Description  <This Method is to test Submit order when Order status is draft  >  
    @Version <1.0>
    *********************************************************************************************/
    /*
    static testMethod void testSubmitCall(){
        
        EP_FE_OrderSubmissionRequest objSubReq = new EP_FE_OrderSubmissionRequest();
        //EP_FE_SubmitOrderEndpoint objSubmitEndPoint = new EP_FE_SubmitOrderEndpoint();   
        EP_FE_OrderSubmissionResponse response = new EP_FE_OrderSubmissionResponse(); 

        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        Test.loadData(EP_Order_Fieldset_Sfdc_Nav_Intg__c.SobjectType,'EP_OrderStatusUpdate_FieldSet');
        Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING'); 

        System.runAs(sysAdmUser){
            Test.startTest();
            csord__Order__c newOrder = EP_FE_TestDataUtility.createNonVMIOrder();   
            newOrder.Status__c = EP_Common_Constant.ORDER_DRAFT_STATUS;
            Database.update (newOrder);         
                          
            objSubReq.draftOrderId = newOrder.id;        
            objSubReq.truckPlate = 'TR1244';
            objSubReq.customerReferenceNumber = 'CRN122345'; 
            objSubReq.comments = 'Order Update';
            
            response = EP_FE_SubmitOrderEndpoint.updateOrder(objSubReq);
            Test.stopTest();
            System.assertequals(newOrder.id,response.newOrderId,'Order updated');
            System.assertequals(True,response.isSuccess,'Order updation is successful');

            RestRequest req = new RestRequest();
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated 
            req.requestURI = '/services/apexrest/FE/V1/order/submit/' ;      
            RestContext.request = req;  
        }          
    }
    
    */
    /*********************************************************************************************
    @Author <Jyotsna Tiwari>
    @name <testDraftIdNull>
    @CreateDate <17/05/2016>
    @Description  <This Method tests Submit Order if the draftOrderId is Null  >  
    @Version <1.0>
    *********************************************************************************************/
    /*
    static testMethod void testDraftIdNull(){
        
        EP_FE_OrderSubmissionRequest objSubReq = new EP_FE_OrderSubmissionRequest();
        //EP_FE_SubmitOrderEndpoint objSubmitEndPoint = new EP_FE_SubmitOrderEndpoint();      
        EP_FE_OrderSubmissionResponse response = new EP_FE_OrderSubmissionResponse();   
        
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        Test.loadData(EP_Order_Fieldset_Sfdc_Nav_Intg__c.SobjectType,'EP_OrderStatusUpdate_FieldSet');
        Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING'); 

        System.runAs(sysAdmUser){
            Test.startTest();
            response = EP_FE_SubmitOrderEndpoint.updateOrder(objSubReq);
            Test.stopTest();
            System.assertequals(-1,response.status,'Missing OrderId');

            RestRequest req = new RestRequest();  
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated 
            req.requestURI = '/services/apexrest/FE/V1/order/submit/' ;      
            RestContext.request = req;  
        }          
    }
    */
    /*********************************************************************************************
    @Author <Jyotsna Tiwari>
    @name <testRequestNullParam>
    @CreateDate <17/05/2016>
    @Description  <This Method tests Submit Order when the Request is Null >  
    @Version <1.0>
    *********************************************************************************************
    /*
    
    static testMethod void testRequestNullParam(){
        
        EP_FE_OrderSubmissionRequest objSubReq = null;     
        //EP_FE_SubmitOrderEndpoint objSubmitEndPoint = new EP_FE_SubmitOrderEndpoint();
        EP_FE_OrderSubmissionResponse response = new EP_FE_OrderSubmissionResponse(); 
        
        System.runAs(sysAdmUser){
            Test.startTest();
            response = EP_FE_SubmitOrderEndpoint.updateOrder(objSubReq);
            Test.stopTest();
            System.assertequals(-2,response.status,'Missing Request');
                    
            RestRequest req = new RestRequest(); 
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated 
            req.requestURI = '/services/apexrest/FE/V1/order/submit/' ;      
            RestContext.request = req; 
        }           
    }   

*/
    /*********************************************************************************************
    @Author <Jyotsna Tiwari>
    @name <testRestCall>
    @CreateDate <02/05/2016>
    @Description  <This Method is to test Submit Order if the status of Order is not 'Draft' >  
    @Version <1.0>
    *********************************************************************************************/
    /*
    static testMethod void testOrderWithSubmittedStatus(){
        
        EP_FE_OrderSubmissionRequest objSubReq = new EP_FE_OrderSubmissionRequest();  
        //EP_FE_SubmitOrderEndpoint objSubmitEndPoint = new EP_FE_SubmitOrderEndpoint();
        EP_FE_OrderSubmissionResponse response = new EP_FE_OrderSubmissionResponse(); 
        
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        Test.loadData(EP_Order_Fieldset_Sfdc_Nav_Intg__c.SobjectType,'EP_OrderStatusUpdate_FieldSet');
        Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING'); 

        System.runAs(sysAdmUser){
            Test.startTest();
            
            csord__Order__c newOrder = EP_FE_TestDataUtility.createNonVMIOrder(); 
            newOrder.Status__C = EP_Common_Constant.SUBMITTED;
            Database.update (newOrder);
           
            objSubReq.draftOrderId = newOrder.id;        
            objSubReq.truckPlate = 'TR12345';
            objSubReq.customerReferenceNumber = 'CRN23456';
            objSubReq.comments = 'Order Update';

            response = EP_FE_SubmitOrderEndpoint.updateOrder(objSubReq);
            Test.stopTest();
            System.assertequals(-3,response.status,'Order Status should be DRAFT for updating the Order');

            RestRequest req = new RestRequest();                 
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated 
            req.requestURI = '/services/apexrest/FE/V1/order/submit/' ;      
            RestContext.request = req; 
        }           
    }  
    */    
}