/* 
 	@Author			Accenture
   	@name			EP_IntegrationException
   	@CreateDate		18/01/2016
   	@Description	Custom Exception class defined for error code support for integration services 
   	@Version		1.0
*/
public class EP_IntegrationException extends Exception {
    @testVisible private EP_IntegrationErrorCode code;
    public enum EP_IntegrationErrorCode {
        //Cannot instantiate integration service
        CANNOT_CREATE_INTEGRATION_SERVICE,
        //Apex class not found for integration service
        INTEGRATION_SERVICE_NOT_FOUND,
        //Apex Callout Exception
        CALLOUT_EXCEPTION,
        //Insert update excpetion
        CANNOT_INSERT_UPDATE_RECORD,
        //generic Exceptions
        GENERIC_EXCEPTION
    }
      
	/**
	* @author      	Accenture
	* @Name        	EP_IntegrationException
	* @date        	18/01/2016
	* @description 	Constructor will set error message and error code
	* @param		EP_IntegrationErrorCode, String
	* @return
	*/ 
    public EP_IntegrationException (EP_IntegrationErrorCode code,String message) {
        this.setMessage(message);
        this.code = code;
    }
    
 	/**
	* @author      	Accenture
	* @Name        	EP_IntegrationException
	* @date        	18/01/2016
	* @description 	Constructor will set error message and error code
	* @param       	EP_IntegrationErrorCode, String, Exception
	* @return		NA
	*/ 
    public EP_IntegrationException ( EP_IntegrationErrorCode code, String message, Exception cause) {
        this.setMessage(message);
        this.code = code;
        this.initCause(cause);
    }
}