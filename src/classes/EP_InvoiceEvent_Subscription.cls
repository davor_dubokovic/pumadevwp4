/**
  * @author       Accenture                                       
  * @name         EP_InvoiceEvent_Subscription                           
  * @Created Date 02/07/2018                                      
  * @description  Invoice Email Notification Class                   
*/
public with sharing class EP_InvoiceEvent_Subscription extends EP_Event_Subscription{
	 
	/**
     * @description Class Constructor
     * @param NA
     * @return NA
    */
	public EP_InvoiceEvent_Subscription(){
		subscriptionEventObj.eventType = EP_Common_Constant.NOTIFICATION_CODE_INVOICE;
	}	
}