global class EditOrderPortalController {
    private Id orderId;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public EditOrderPortalController() {
        orderId = ApexPages.currentPage().getParameters().get('id');
    }

    public PageReference editOrder() {
        List<csord__Order__c> orderList = [SELECT csord__Identification__c FROM csord__Order__c WHERE Id = :orderId LIMIT 1];

        List<cscfga__Product_Configuration__c> pumaEnergyConfigs = [select id from cscfga__Product_Configuration__c 
                                                                    where cscfga__product_definition__r.name = :EP_Common_Constant.pumaEnergyOrder //'Puma Energy Order' 
                                                                    and cscfga__product_basket__c = :orderList[0].csord__Identification__c];

        PageReference configPage = Page.customConfiguration;
        Map<String, String> configPageParams = configPage.getParameters();
        configPageParams.put('configId', pumaEnergyConfigs[0].id);
        configPageParams.put('linkedId', orderList[0].csord__Identification__c);
        configPageParams.put('portal', 'true');
        configPage.setRedirect(true);
        return configPage;
    }
}