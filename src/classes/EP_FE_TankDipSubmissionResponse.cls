/* 
   @Author <Cihan Sunar>
   @name <EP_FE_TankDipSubmissionResponse>
   @CreateDate <03/05/2016>
   @Description <Response wrapper for the tank dip submission API>  
   @Version <1.0>
*/
global with sharing class EP_FE_TankDipSubmissionResponse extends EP_FE_Response {

    /*
    *   List of possible errors
    */
    global static final Integer ERROR_MISSING_REQUEST = -1;
    global static final Integer ERROR_MISSING_TANKDIPS_REQUEST = -2;
    global static final Integer ERROR_MISSING_INVALID_TANK_DIPS = -3;
    global static final Integer ERROR_MISSING_ERROR_SAVING_TANK_DIPS = -4;
    global static final Integer ERROR_TANK_DIPS_NOT_SAVED = -5;


    global static final String CLASSNAME = 'EP_FE_TankDipSubmissionEndpoint';
    global static final String METHOD = 'submitTankDip';
    global final static String SEVERITY = 'ERROR';
    global final static String TRANSACTION_ID = 'PE-537';
    global final static String DESCRIPTION1 = 'Missing request';
    global final static String DESCRIPTION2 = 'Missing tank dip request';
    global final static String DESCRIPTION3 = 'Invalid tank dip request';
    global final static String AREA4 = 'Tank dip insert';
    global final static String DESCRIPTION4 = 'Inserting tank dips';
/* 
   @Author <Cihan Sunar>
   @name <EP_FE_Field_Error >
   @CreateDate <03/05/2016>
   @Description <>  
   @Version <1.0>
*/
    public with sharing class EP_FE_Field_Error {
        public List<String> fields;
        public String message;
        public Datetime measurementTime;
/* 
   @Author <Cihan Sunar>
   @name <EP_FE_Field_Error >
   @CreateDate <03/05/2016>
   @Description <>  
   @Version <1.0>
*/
        public EP_FE_Field_Error(String message, List<String> fields, Datetime measurementTime) {
            this.message = message;
            this.fields = fields;
            this.measurementTime = measurementTime;
        }
    }

    public Map<String, List<EP_FE_Field_Error>> tankDipErrors {get;set;}
/* 
   @Author <Cihan Sunar>
   @name <EP_FE_Field_Error >
   @CreateDate <03/05/2016>
   @Description <>  
   @Version <1.0>
*/
    public EP_FE_TankDipSubmissionResponse() {
        tankDipErrors = new Map<String, List<EP_FE_Field_Error>>();
    }

}