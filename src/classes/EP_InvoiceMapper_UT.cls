@isTest
private class EP_InvoiceMapper_UT {    
    private static testMethod void getOverdueInvoices_test() {
        EP_InvoiceMapper localObj = new EP_InvoiceMapper();
        EP_Freight_Matrix__c freightMatrix=EP_TestDataUtility.createFreightMatrix();
        Account parentAcc = EP_TestDataUtility.createSellToAccount(null,freightMatrix.id);
        csord__Order__c orderObj = EP_TestDataUtility.getSalesOrderWithInvoice();
        EP_Invoice__c invoiceObj = EP_TestDataUtility.createInvoice(parentAcc);
        System.debug('@@ invoiceObj +'+invoiceObj);
        set<id> idSet = new Set<id>{invoiceObj.EP_Bill_To__c };
        Test.StartTest();
        list<EP_Invoice__c> result= localObj.getOverdueInvoices(idSet);
        Test.StopTest();
        System.AssertEquals(false,result.size() > 0);
    }
}