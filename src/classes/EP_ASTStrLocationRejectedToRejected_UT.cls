@isTest
public class EP_ASTStrLocationRejectedToRejected_UT
{
	static final string EVENT_NAME = '08-RejectedTo08-Rejected';
    static final string INVALID_EVENT_NAME = '08-RejectedTo02-BasicDataSetup';
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    
	static testMethod void isTransitionPossible_Positivetest() {
		EP_ASTStorageLocationRejectedToRejected localObj = new EP_ASTStorageLocationRejectedToRejected();
		EP_AccountDomainObject obj = EP_TestDataUtility.getASTStorageLocationRejectedToRejectedDomainObjectPositiveScenario();
		EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,ae);
		Test.startTest();
		Boolean result = localObj.isTransitionPossible();
		Test.stopTest();
		System.AssertEquals(true,result);
	}
	
	static testMethod void isTransitionPossible_Negativetest() {
		EP_ASTStorageLocationRejectedToRejected localObj = new EP_ASTStorageLocationRejectedToRejected();
		EP_AccountDomainObject obj = EP_TestDataUtility.getASTStorageLocationRejectedToRejectedDomainObjectNegativeScenario();
		EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
		localObj.setAccountContext(obj,ae);
		Test.startTest();
		Boolean result = localObj.isTransitionPossible();
		Test.stopTest();
		System.AssertEquals(false,result);
	}
	
	static testMethod void isRegisteredForEvent_Positivetest() {
		EP_ASTStorageLocationRejectedToRejected localObj = new EP_ASTStorageLocationRejectedToRejected();
		EP_AccountDomainObject obj = EP_TestDataUtility.getASTStorageLocationRejectedToRejectedDomainObjectPositiveScenario();
		EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,ae);
		Test.startTest();
		Boolean result = localObj.isRegisteredForEvent();
		Test.stopTest();
		System.AssertEquals(true,result);
	}
	
	static testMethod void isRegisteredForEvent_Negativetest() {
		EP_ASTStorageLocationRejectedToRejected localObj = new EP_ASTStorageLocationRejectedToRejected();
		EP_AccountDomainObject obj = EP_TestDataUtility.getASTStorageLocationRejectedToRejectedDomainObjectNegativeScenario();
		EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
		localObj.setAccountContext(obj,ae);
		Test.startTest();
		Boolean result = localObj.isRegisteredForEvent();
		Test.stopTest();
		System.AssertEquals(false,result);
	}
	
	static testMethod void isGuardCondition_Positivetest() {
		EP_ASTStorageLocationRejectedToRejected localObj = new EP_ASTStorageLocationRejectedToRejected();
		EP_AccountDomainObject obj = EP_TestDataUtility.getASTStorageLocationRejectedToRejectedDomainObjectPositiveScenario();
		EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,ae);
		Test.startTest();
		Boolean result = localObj.isGuardCondition();
		Test.stopTest();
		System.AssertEquals(true,result);
	}

}