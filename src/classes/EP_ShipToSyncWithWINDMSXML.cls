/**
* @author           Accenture
* @name             EP_ShipToSyncWithWINDMSXML
* @NOvasuite Fixes - Required Documentation
**/
public class EP_ShipToSyncWithWINDMSXML extends EP_AccountRequestXML{
      /*Defect 78513 start*/
    Public List<EP_Tank__c> ListTanks = new List<EP_Tank__c>();
    /*Defect 78513 end*/
    /**
    * @author           Accenture
    * @name             init
    * @date             11/18/2017
    * @description      retrive the records to bind the values in XML
    * @param            NA
    * @return           NA
    */
	public  override void init(){
		EP_GeneralUtility.Log('Public','EP_ShipToSyncWithWINDMSXML','init');
		system.debug('**RecordID-->'+recordId);
		MSGNode = doc.createRootElement(EP_AccountConstant.MSG, null, null);
		EP_AccountMapper objAccMap = new EP_AccountMapper();   
        EP_TankMapper TankMapper = new EP_TankMapper();
		objAccount = objAccMap.getShipToWithTanksById(recordId); 
        /*Defect 78513 start*/
        ListTanks = TankMapper.getTanksRecordsByShipToId(objAccount);
        /*Defect 78513 end*/
        super.setEncryption();
	}
	/**
    * @author           Accenture
    * @name             createPayload
    * @date             11/18/2017
    * @description      This method is used to create Payload for shipTo Sync with WinDMS
    * @param            NA
    * @return           NA
    */ 
	public override void createPayload(){
		Dom.XMLNode payloadNode = MSGNode.addChildElement(EP_AccountConstant.PAYLOAD,null,null);
        Dom.XmlNode anyNode = payloadNode.addChildElement(EP_AccountConstant.ANY0,null, null);
        DOM.Document tempDoc = new DOM.Document();
        Dom.XMLNode shipToAddressesNode = tempDoc.createRootElement(EP_AccountConstant.SHIPTOADDRESSES,null, null);
		Dom.XMLNode shipToAddressNode = shipToAddressesNode.addChildElement(EP_AccountConstant.SHIPTOADDRESS,null,null);    
		String seqid = EP_IntegrationUtil.reCreateSeqId(messageId, objAccount.id); 
		shipToAddressNode.addChildElement(EP_AccountConstant.SEQID,null,null).addTextNode(getValueforNode(seqid));
		Dom.XMLNode IdentifierNode = shipToAddressNode.addChildElement(EP_AccountConstant.IDENTIFIER,null,null);
		IdentifierNode.addChildElement(EP_AccountConstant.SHIPTOID,null,null).addTextNode(getValueforNode(objAccount.EP_Account_Id__c));
		string custId = objAccount.ParentId==null?objAccount.EP_Account_Id__c:objAccount.Parent.EP_Account_Id__c ;
		IdentifierNode.addChildElement(EP_AccountConstant.CUSTID,null,null).addTextNode(getValueforNode(custId));
		IdentifierNode.addChildElement(EP_AccountConstant.ENTRPRSID,null,null).addTextNode(objAccount.EP_EnterpriseId__c);
		shipToAddressNode.addChildElement(EP_AccountConstant.NAME,null,null).addTextNode(getValueforNode(objAccount.Name));
		shipToAddressNode.addChildElement(EP_AccountConstant.NAME2,null,null).addTextNode(getValueforNode(objAccount.EP_Account_Name_2__c));
		shipToAddressNode.addChildElement(EP_AccountConstant.CNTRYCODE,null,null).addTextNode(getValueforNode(objAccount.EP_Country_Code__c));
		shipToAddressNode.addChildElement(EP_AccountConstant.LANGCODE,null,null).addTextNode(getValueforNode(objAccount.EP_Language_Code__c));
		string address = objAccount.ParentId!=null?objAccount.ShippingStreet:objAccount.BillingStreet;
		shipToAddressNode.addChildElement(EP_AccountConstant.ADDRESS,null,null).addTextNode(getValueforNode(address));
		string strPostCode = objAccount.ParentId!=null?objAccount.ShippingPostalCode:objAccount.BillingPostalCode;
		shipToAddressNode.addChildElement(EP_AccountConstant.POSTCODE,null,null).addTextNode(getValueforNode(strPostCode)); 
		string strCity = objAccount.ParentId!=null?objAccount.ShippingCity:objAccount.BillingCity;
		shipToAddressNode.addChildElement(EP_AccountConstant.CITY,null,null).addTextNode(getValueforNode(strCity));
		shipToAddressNode.addChildElement(EP_AccountConstant.PHONE,null,null).addTextNode(getValueforNode(objAccount.Phone));
		shipToAddressNode.addChildElement(EP_AccountConstant.FAX,null,null).addTextNode(getValueforNode(objAccount.Fax));
		shipToAddressNode.addChildElement(EP_AccountConstant.EMAIL,null,null).addTextNode(getValueforNode(objAccount.EP_Email__c));
		shipToAddressNode.addChildElement(EP_AccountConstant.SHIPTOTYPE,null,null).addTextNode(getValueforNode(objAccount.EP_Ship_To_Type__c)); 
		//code changes for L4 #45362 Start
		//shipToAddressNode.addChildElement(EP_AccountConstant.STATUS,null,null).addTextNode(getValueforNode(getShipToStatus(objAccount.EP_Status__c))); 
        shipToAddressNode.addChildElement(EP_AccountConstant.STATUS,null,null).addTextNode(getValueforNode(objAccount.EP_ShipTo_Status_WINDMS__c)); 
        //code changes for L4 #45362 End
		shipToAddressNode.addChildElement(EP_AccountConstant.ISVMI,null,null).addTextNode(getValueforNode(objAccount.EP_Is_VMI_ShipTo__c));
		shipToAddressNode.addChildElement(EP_AccountConstant.DLVRYTYPE,null,null).addTextNode(getValueforNode(objAccount.EP_Delivery_Type__c));
		shipToAddressNode.addChildElement(EP_AccountConstant.MODIFYON,null,null).addTextNode(formatDateAsString((objAccount.LastModifiedDate)));
        shipToAddressNode.addChildElement(EP_AccountConstant.MODIFYBY,null,null).addTextNode(getValueforNode(objAccount.LastModifiedBy.Alias));
        //SIT-Bug-74051
        shipToAddressNode.addChildElement(EP_AccountConstant.XPOS,null,null).addTextNode(getValueforNode(getGeoCordinateValue(objAccount.EP_Position__longitude__s)));
		shipToAddressNode.addChildElement(EP_AccountConstant.YPOS,null,null).addTextNode(getValueforNode(getGeoCordinateValue(objAccount.EP_Position__latitude__s)));
        //SIT-Bug-74051
		shipToAddressNode.addChildElement(EP_AccountConstant.VERSIONNR,null,null);
		//Bind Tanks
        /*Defect 78513 start*/
        if(ListTanks.size()>0)
        bindTankRecordsInXML(shipToAddressNode,ListTanks);
        /*Defect 78513 end*/
        //Bind Supply Options
        bindSupplyLocationRecordsInXML(shipToAddressNode,objAccount.Stock_Holding_Locations1__r);
        shipToAddressNode.addChildElement(EP_AccountConstant.CLIENTID,null,null).addTextNode(getValueforNode(objAccount.EP_Puma_Company_Code__c));
        // Encoding payload by calling encode XML method in superclass
        AnyNode.addTextNode(encodeXML(tempDoc));
	}	
	
	/**
    * @author           Accenture
    * @name             bindTankRecordsInXML
    * @date             14/Nov/2017
    * @description      create the xml ralated to Tanks
    * @param            Dom.XMLNode ,list<EP_Tank__c>
    * @return           NA
    */  
	private void bindTankRecordsInXML(Dom.XMLNode shipToAddressNode, list<EP_Tank__c> lstTanks){
	Dom.XMLNode tanksNode = shipToAddressNode.addChildElement(EP_AccountConstant.TANKS,null,null);
		for(EP_Tank__c objTank :lstTanks){  
		   Dom.XMLNode tankNode = tanksNode.addChildElement(EP_AccountConstant.TANK,null,null);      
		   String seqid = EP_IntegrationUtil.reCreateSeqId(messageId, objTank.id); 
			tankNode.addChildElement(EP_AccountConstant.SEQID,null,null).addTextNode(getValueforNode(seqid));
			Dom.XMLNode tankIdentifierNode = tankNode.addChildElement(EP_AccountConstant.IDENTIFIER,null,null);
		   tankIdentifierNode.addChildElement(EP_AccountConstant.SHIPTOID,null,null).addTextNode(getValueforNode(objTank.EP_Ship_To_Nr__c));
		   tankIdentifierNode.addChildElement(EP_AccountConstant.TANKID,null,null).addTextNode(getValueforNode(objTank.EP_Tank_Code__c));           
           //76105-Sending Tank Alias
		   tankNode.addChildElement(EP_AccountConstant.TANKALIAS,null,null).addTextNode(getValueforNode(objTank.EP_Tank_Alias__c));
		   tankNode.addChildElement(EP_AccountConstant.UOM,null,null).addTextNode(getValueforNode(objTank.EP_UnitOfMeasure__c));
		   tankNode.addChildElement(EP_AccountConstant.PRODUCTID,null,null).addTextNode(getValueforNode(objTank.EP_Product_Code__c));
		   tankNode.addChildElement(EP_AccountConstant.CAPACITY,null,null).addTextNode(getValueforNode(objTank.EP_Capacity__c));
		   tankNode.addChildElement(EP_AccountConstant.SAFEFILLLEVEL,null,null).addTextNode(getValueforNode(objTank.EP_Safe_Fill_Level__c));
		   tankNode.addChildElement(EP_AccountConstant.DEADSTOCK,null,null).addTextNode(getValueforNode(objTank.EP_Deadstock__c));
		   tankNode.addChildElement(EP_AccountConstant.TANKSTATUS,null,null).addTextNode(getValueforNode(objTank.EP_TANK_STATUS_TO_LS__c));
		   tankNode.addChildElement(EP_AccountConstant.TANKDIPENTRYMODE,null,null).addTextNode(getValueforNode(objTank.EP_Tank_Dip_Entry_Mode__c)); 
		   tankNode.addChildElement(EP_AccountConstant.MODIFYON,null,null).addTextNode(formatDateAsString(objTank.LastModifiedDate));
		   tankNode.addChildElement(EP_AccountConstant.MODIFYBY,null,null).addTextNode(getValueforNode(objTank.EP_Last_Modified_By_Name__c)); 
		   tankNode.addChildElement(EP_AccountConstant.ACTIVEFROM,null,null); 
		   tankNode.addChildElement(EP_AccountConstant.ACTIVETO,null,null);
		   tankNode.addChildElement(EP_AccountConstant.AVAILABLESTOCKLASTUPDATED,null,null);  
		    //for data versioning
            tankNode.addChildElement(EP_AccountConstant.VERSIONNR,null,null);
		}
	}
    
    /**
    * @author           Accenture
    * @name             bindSupplyLocationRecordsInXML
    * @date             14/Nov/2017
    * @description      create the xml ralated to Supply locations
    * @param            Dom.XMLNode ,list<EP_Stock_Holding_Location__c>
    * @return           NA
    */ 
	private void bindSupplyLocationRecordsInXML(Dom.XMLNode shipToAddressNode, list<EP_Stock_Holding_Location__c> lstStockHldingLocations){
	Dom.XMLNode supplyLocationsNode = shipToAddressNode.addChildElement(EP_AccountConstant.SUPPLYLOCATIONS,null,null);
		For(EP_Stock_Holding_Location__c objSHLocations :lstStockHldingLocations){    
		   Dom.XMLNode supplyLocNode = supplyLocationsNode.addChildElement(EP_AccountConstant.SUPPLYLOCATION,null,null);    
			String seqid = EP_IntegrationUtil.reCreateSeqId(messageId, objSHLocations.id);  
			supplyLocNode.addChildElement(EP_AccountConstant.SEQID,null,null).addTextNode(getValueforNode(seqid)); 
			Dom.XMLNode supplyLocIdentifierNode = supplyLocNode.addChildElement(EP_AccountConstant.IDENTIFIER,null,null);
			   supplyLocIdentifierNode.addChildElement(EP_AccountConstant.STOCKHLDNGLOCID,null,null).addTextNode(getValueforNode(objSHLocations.EP_Stock_Location_Id__c));
			   supplyLocIdentifierNode.addChildElement(EP_AccountConstant.SHIPTOID,null,null).addTextNode(getValueforNode(objSHLocations.EP_Ship_To_Id__c));           
			   supplyLocNode.addChildElement(EP_AccountConstant.DISTANCE,null,null).addTextNode(getValueforNode(objSHLocations.EP_Distance__c));
			   supplyLocNode.addChildElement(EP_AccountConstant.UOM,null,null).addTextNode(getValueforNode(objSHLocations.EP_UOM__c));
			   supplyLocNode.addChildElement(EP_AccountConstant.DEFAULTSTR,null,null).addTextNode(getValueforNode(objSHLocations.EP_Default__c));
			   supplyLocNode.addChildElement(EP_AccountConstant.MODIFYON,null,null).addTextNode(formatDateAsString(objSHLocations.LastModifiedDate)); 
			   supplyLocNode.addChildElement(EP_AccountConstant.MODIFYBY,null,null).addTextNode(getValueforNode(objSHLocations.EP_Last_Modified_By_Name__c));
			   //for data versioning
           	   supplyLocNode.addChildElement(EP_AccountConstant.VERSIONNR,null,null);
		}
	}    
    
    /**
    * @author           Accenture
    * @name             getGeoCordinateValue
    * @description      this method will return zero as default cordinate if cordinate is null
    * @param            Decimal cordinate
    * @return           decimal
    */ 
    @TestVisible
    private Decimal getGeoCordinateValue(Decimal cordinate){
        return (cordinate == null)?0.00000:cordinate;
    }
}