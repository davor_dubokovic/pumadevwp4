@isTest
private class EP_PaymentPageController_Test {
 
  private static Account nonVmiShipToAccount = new Account();
    private static Account nonVmiShipToAccountConsignment = new Account();
    private static order order1 = new Order();
    private static Account sellToAccount = new Account();
    private static Account billToAccount = new Account();
    private static Account lStogrageLocation = new Account();
    private static EP_Stock_Holding_Location__c lSupplyLocation = new EP_Stock_Holding_Location__c();
    private static EP_Stock_Holding_Location__c lSupplyLocation2 = new EP_Stock_Holding_Location__c();
    private static EP_Inventory__c primaryInventory = new EP_Inventory__c();
    private static EP_Inventory__c primaryInventory2 = new EP_Inventory__c();
    private static Pricebook2 customPB = new Pricebook2();
    private static Product2 productPetrolObj = new Product2();
    private static PricebookEntry pbEntryPetrol = new PricebookEntry();
    private static Company__c comp;
    private static final string DELIVERYTYPE_SUPPLY_LOCATION_RT = 'Ship-To Supply Location';
    private static final string VENDORTYPE = 'Transporter';
    private static final String NAV_VENDOR_ID_1 = '00001';
    private static String orderType;
    private static Id supplyLocationId;
    private static String OrderStatus = EP_Common_Constant.ORDER_DRAFT_STATUS;
    private static string VENDOR_ACCOUNT_RT = 'Vendor';
    private static Product2 testProduct;
    private static Id strNonVMIShipToRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,     EP_Common_Constant.NON_VMI_SHIP_TO);
    private static List<orderItem> orderItems = new List<orderItem>();
    
    private static Pricebook2 testPricebook;
    private static PricebookEntry testPricebookEntry;
    private static Product2 testTaxProduct;
    private static Product2 testFreightProduct;
    private static EP_Route__c route1;
    private static  EP_Run__c run1;
    private static EP_Route_Allocation__c routeAllocation;
    private static final String COMP_CODE = 'AUN';
    private static final String CANCELL_FAIL_ERROR_CSC = 'Order can be cancelled only from status Draft, Submitted, Accepted, Awaiting Payment, and Planning by CSC';
    private static final String MODIFY_FAIL_ERROR_CSC = 'You are unable to modify this order as it has moved beyond the Planning stage. Please call the scheduling team';
    private static final String REQ_DATE_ERROR = 'You are unable to submit this order as the requested delivery date is in the past or the requested delivery date is outside the planning cutoff limit. Please review the date and try again';
    // Test constance variables
    private static string CSC_PROFILE_NAME = 'EP_CSC';
    
    // Properties returning test data
    private static Profile cscAgentProfile {
        get { 
            if (cscAgentProfile == NULL) {
                List<Profile> aCSCAgentProfiles = [Select ID FROM Profile WHERE Name = :CSC_PROFILE_NAME LIMIT 1];
                System.assertEquals(1, aCSCAgentProfiles.size());
                cscAgentProfile = aCSCAgentProfiles[0];
            }
            return cscAgentProfile; 
        }
        set;
    }
 
 static void createTestData() 
    {
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');        
        Test.loadData(EP_OrderStatusNAVMapping__c.SobjectType,'EP_OrderStatusUpdateNAV');
        Test.loadData(EP_Order_Fieldset_Sfdc_Nav_Intg__c.SobjectType,'EP_OrderStatusUpdate_FieldSet');
        Test.loadData(EP_Order_Fieldset_Sfdc_Windms_Intg__c.sObjectType, 'TestData_EP_Order_Fieldset_Sfdc_Windms_Intg');
        comp = EP_TestDataUtility.createCompany('AUN');
        Database.insert(comp);
        EP_Country__c contry1 = EP_TestDataUtility.createCountryRecord('Australia','AU', 'Australia');
        database.insert(contry1);
        EP_Region__c reg = EP_TestDataUtility.createCountryRegion('North-Australia',contry1.id );
        database.insert(reg);
        
        customPB = new Pricebook2(Name='AU Pricebook', isActive=true, EP_Company__c= comp.Id );
        Database.insert(customPB);
        system.debug( 'customPB='+customPB);
        
        productPetrolObj = new product2(    Name = 'petrol',  isActive = TRUE, //CurrencyIsoCode = 'GBP',
                                                    family='Hydrocarbon Liquid', Eligible_for_Rebate__c = true, EP_Blended__c = false,
                                                    EP_Company_Lookup__c= comp.id
                                                );                           
        Database.insert(productPetrolObj);        
        system.debug( 'productPetrolObj='+productPetrolObj);
        /*
        Id pricebookId = Test.getStandardPricebookId();
        
        // insert pricebook entry for petrol product
        PricebookEntry standardPrice2 = new PricebookEntry(
            Pricebook2Id = pricebookId, 
            Product2Id = productPetrolObj.Id,
            UnitPrice = 100, 
            IsActive = true, 
            //CurrencyIsoCode = 'GBP'
        );
        database.insert(standardPrice2,false);
        */
        pbEntryPetrol = new PricebookEntry(
            Pricebook2Id = customPB.Id, 
            Product2Id = productPetrolObj.Id,
            UnitPrice = 120, 
            IsActive = false
            //CurrencyIsoCode = 'GBP'
        );
        database.insert(pbEntryPetrol);  
        system.debug( 'pbEntryPetrol='+pbEntryPetrol);
        
        //testTaxProduct = EP_TestDataUtility.createTestRecordForTaxProduct(customPB.Id);
      //  testFreightProduct = EP_TestDataUtility.createTestRecordForFreightPriceProduct( customPB.Id );
         
        //CREATE FREIGHT MATRIX
        EP_Freight_Matrix__c freightMatrix;
        freightMatrix = EP_TestDataUtility.createFreightMatrix();
        Database.insert(freightMatrix);
        
        
        // create freight price
        EP_Freight_Price__c  FreightPrice = EP_TestDataUtility.createFreight(freightMatrix.id,productPetrolObj.id);     
        FreightPrice.EP_Freight_Price__c = 100;
        FreightPrice.EP_Max_Distance__c = 100;
        FreightPrice.EP_Max_Volume__c = 100;
        FreightPrice.EP_Min_Distance__c = 1;
        FreightPrice.EP_Min_Volume__c = 1;
        //FreightPrice.CurrencyIsoCode = 'GBP';
        FreightPrice.EP_Volume_UOM__c = 'LT';  
        Database.insert(FreightPrice);
        
        Id orderConfigMixingRecordType = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER_CONFIG_OBJ,
                                                                            EP_Common_Constant.MIXING_RANGE);
        Id orderConfigCountryRangeRecordType = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER_CONFIG_OBJ,
                                                                            EP_Common_Constant.COUNTRY_RANGE);
        
        // create country range order configuration record
        EP_Order_Configuration__c orderConfig =  new EP_Order_Configuration__c();
        orderConfig.RecordTypeId = orderConfigCountryRangeRecordType;
        orderConfig.EP_Country__c = contry1.id;
        orderConfig.EP_Max_Quantity__c = 100;
        orderConfig.EP_Min_Quantity__c = 1;
        orderConfig.EP_Product__c = productPetrolObj.id;
        orderConfig.EP_Volume_UOM__c = 'LT';
        Database.insert(orderConfig);
        
        
        //create non mixing order configuration record
        EP_Order_Configuration__c orderConfig1 =  new EP_Order_Configuration__c();
        orderConfig1.RecordTypeId = orderConfigMixingRecordType ;
        orderConfig1.EP_Country__c = contry1.id;
        orderConfig1.EP_Max_Quantity__c = 100;
        orderConfig1.EP_Min_Quantity__c = 1;
        orderConfig1.EP_Product__c = productPetrolObj.id;
        orderConfig1.EP_Volume_UOM__c = 'LT';
        //Database.insert(orderConfig1);        
        
        //create payment term
        EP_Payment_Term__c paymentTerm = new EP_Payment_Term__c(
            EP_Payment_Term_Code__c = 'NET45',
            Name = 'NET45'
        );
        database.insert( paymentTerm );
        
        //CREATE BILL TO
        billToAccount = EP_TestDataUtility.createBillToAccount();
        billToAccount.EP_Country__c = contry1.id;
        billToAccount.EP_Delivery_Pickup_Country__c = contry1.id;
        billToAccount.EP_Cntry_Region__c = reg.id;
        billToAccount.EP_Payment_Term_Lookup__c = paymentTerm.id;
       // billToAccount.CurrencyIsoCode = 'GBP';
        /*CAM 2.7 start*/
        billToAccount.EP_AvailableFunds__c = 10000000;
         /*CAM 2.7 end*/
        billToAccount.EP_Puma_Company__c = comp.Id;
        billToAccount.BillingCity = 'testCity';
        billToAccount.BillingCountry = 'testCountry';
        Database.insert(billToAccount);
        
        //Set Bill To Status as basic data setup
        billToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(billToAccount);
        
        //COMPLETE ACTIONS
        EP_TestDataUtility.CmpltApproveActions();
        
        //SETTING BILL TO STATUS TO ACTIVE
        billToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(billToAccount);
        
        for(Pricebookentry pcb : [select id , Product2.EP_Company_Lookup__c from Pricebookentry where Pricebook2Id =:customPB.Id]){
            system.assertequals(comp.id,pcb.Product2.EP_Company_Lookup__c); 
            system.debug('--------comp.id------------'+comp.id);
            system.debug('--------pcb.Product2.EP_Company_Lookup__c-------------'+pcb.Product2.EP_Company_Lookup__c);
        }
        
        //CREATE A CUSTOMER ACCOUNTS WITH ACTIVE BILL TO ACCOUNT
        sellToAccount = EP_TestDataUtility.createSellToAccount(billToAccount.id, freightMatrix.id);
        sellToAccount.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
        sellToAccount.EP_Freight_Matrix__c= freightMatrix.id;
        sellToAccount.EP_PriceBook__c  = customPB.Id;
        sellToAccount.EP_Payment_Term_Lookup__c = paymentTerm.id;
        /*CAM 2.7 start*/
        sellToAccount.EP_AvailableFunds__c = 10000000;
        /*CAM 2.7 end*/
        sellToAccount.EP_Country__c = contry1.id;
        //sellToAccount.CurrencyIsoCode = 'GBP';
        sellToAccount.EP_Delivery_Pickup_Country__c = contry1.id;
        sellToAccount.EP_Cntry_Region__c = reg.id;
        sellToAccount.EP_Bill_To_Account__c = billToAccount.Id;
        sellToAccount.EP_Puma_Company__c = comp.Id;
        sellToAccount.BillingCity = 'testCity'; 
        sellToAccount.BillingCountry = 'testCountry';
        Database.insert(sellToAccount);
        
        //Set Sell To Status as basic data setup
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(sellToAccount);
        
        //create Storage location
        lStogrageLocation = EP_TestDataUtility.createStockHoldingLocation();
        lStogrageLocation.EP_Puma_Company__c = comp.Id;
        Database.insert(lStogrageLocation);
        
        //Set Storage location Status as basic data setup
        lStogrageLocation.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(lStogrageLocation);
        
        //COMPLETE ACTIONS
        EP_TestDataUtility.CmpltApproveActions();
        
        //SETTING Sell TO STATUS TO ACTIVE
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE; 
        Database.update(sellToAccount);
        
        //SETTING STORAGE LOCATION STATUS TO ACTIVE
        lStogrageLocation.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(lStogrageLocation);
        
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
        nonVmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId);
        nonVmiShipToAccount.EP_Pumps__c= 10;
        nonVmiShipToAccount.EP_Freight_Matrix__c= freightMatrix.id;
        nonVmiShipToAccount.EP_PriceBook__c = customPB.Id;
        nonVmiShipToAccount.EP_Ship_To_Type__c = EP_Common_Constant.NON_CONSIGNMENT;
        nonVmiShipToAccount.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
       // nonVmiShipToAccount.CurrencyIsoCode = 'GBP';
        nonVmiShipToAccount.EP_Country__c = contry1.id;
        nonVmiShipToAccount.EP_Delivery_Pickup_Country__c = contry1.id;
        nonVmiShipToAccount.EP_Cntry_Region__c = reg.id;
        nonVmiShipToAccount.EP_Transportation_Management__c = '3rd party';
        nonVmiShipToAccount.EP_Email__c = 'test@test.com';
        nonVmiShipToAccount.EP_Puma_Company__c = comp.Id;
        database.insert( nonVmiShipToAccount);
        
        nonVmiShipToAccountConsignment = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId);
        nonVmiShipToAccountConsignment.EP_Pumps__c= 10;
        nonVmiShipToAccountConsignment.EP_Freight_Matrix__c= freightMatrix.id;
        nonVmiShipToAccountConsignment.EP_PriceBook__c = customPB.Id;
        nonVmiShipToAccountConsignment.EP_Ship_To_Type__c = EP_Common_Constant.CONSIGNMENT;
        nonVmiShipToAccountConsignment.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
        nonVmiShipToAccountConsignment.EP_Transportation_Management__c = '3rd party';
        nonVmiShipToAccountConsignment.EP_Country__c = contry1.id;
        nonVmiShipToAccountConsignment.EP_Delivery_Pickup_Country__c = contry1.id;
        nonVmiShipToAccountConsignment.EP_Cntry_Region__c = reg.id;
        nonVmiShipToAccountConsignment.EP_Email__c = 'test@test.com';
        //nonVmiShipToAccountConsignment.CurrencyIsoCode = 'GBP';
        database.insert( nonVmiShipToAccountConsignment );
        
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(nonVmiShipToAccount);
        
        nonVmiShipToAccountConsignment.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(nonVmiShipToAccountConsignment);
        
        //COMPLETE ACTIONS
        EP_TestDataUtility.CmpltApproveActions();
        
        
    }
 static void createsupplyLocationData(Account vendorAccount) 
    {
        
        
        Company__c company = new Company__c(id= comp.id);
        company.EP_Route_Enabled__c = true;
        update company;
           
        route1 =  new EP_Route__c(EP_Status__c = 'Active');
        route1.EP_Storage_Location__c = lStogrageLocation.Id;
        route1.EP_Company__c = company.id;
          
        Database.insert(route1);
        system.debug('route1>>'+route1);
        
        /*run1 = new EP_Run__c();
        run1.EP_Route__c = route1.Id;
        run1.EP_Run_End_Date__c = System.Today() + 60;
        run1.EP_Run_Start_Date__c = System.Today();
        system.debug('run1>>'+run1);
        Database.Insert(run1);
        system.debug('run1>>'+run1);
        
        routeAllocation = new EP_Route_Allocation__c();
        routeAllocation.Delivery_Window_End_Date__c = 10;
        routeAllocation.EP_Delivery_Window_Start_Date__c = 5;
        routeAllocation.EP_Route__c = route1.Id;
        routeAllocation.EP_Ship_To__c = nonVmiShipToAccount.Id;
        Database.Insert(routeAllocation); 
        
        ID sellToSHLRTID= EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c', 'Sell-To Supply Location');
        
        EP_Stock_Holding_Location__c lSupplyLocationSellTo = EP_TestDataUtility.createSellToStockLocation( sellToAccount.Id,
                                                                                                                true,
                                                                                                                    lStogrageLocation.Id,
                                                                                                                        sellToSHLRTID );
        Database.insert( lSupplyLocationSellTo );
        
        //create primary stock holding location  
        Id DeliverySupplyLocationRT = Schema.SObjectType.EP_Stock_Holding_Location__c.getRecordTypeInfosByName().get(DELIVERYTYPE_SUPPLY_LOCATION_RT).getRecordTypeId();
        lSupplyLocation = EP_TestDataUtility.createShipToStockLocation( nonVmiShipToAccount.Id, true, lStogrageLocation.Id,vendorAccount.Id, DeliverySupplyLocationRT  );
        lSupplyLocation.EP_Default_Route__c = route1.Id;
        Database.insert(lSupplyLocation);
        
        lSupplyLocation2 = EP_TestDataUtility.createShipToStockLocation( nonVmiShipToAccountConsignment.Id, true, lStogrageLocation.Id,vendorAccount.Id, DeliverySupplyLocationRT  );
        Database.insert(lSupplyLocation2);
        
        supplyLocationId = lSupplyLocation.Id;
        
        primaryInventory = new EP_Inventory__c(
            
            EP_Storage_Location__c = lStogrageLocation.id, 
            EP_Product__c = productPetrolObj.id,
            EP_Stock_Label__c = 'Third_Party',
            EP_Inventory_Availability__c = 'Good',
            EP_SKU_Unique_Key__c = '12345678'
        );
        Database.insert(primaryInventory);*/
        
    }
    static Order createOrder( Id shipToId, Id vendorId )
    {
        /*String orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get( EP_Common_Constant.NONVMI_ORDER_RECORD_TYPE_NAME ).getRecordTypeId();
        Id pricebookId = customPB.Id;
        order1 = EP_TestDataUtility.createOrder(sellToAccount.Id,orderRecType, pricebookId);
        order1.Status = OrderStatus;//EP_Common_Constant.ORDER_DRAFT_STATUS;    
        order1.EP_Transporter__c = vendorId;
        order1.EP_Delivery_Type__c = 'Delivery';
        order1.EP_ShipTo__c = shipToId;   
        order1.Stock_Holding_Location__c = supplyLocationId;
        //order1.CurrencyIsoCode = 'GBP';
        order1.Type = orderType;
        order1.EP_Route__c = route1.Id;
        //order1.EP_Run__c =  run1.Id;
        database.insert( order1 );
        system.assertNotEquals(null,order1.Id);
        
        OrderItem otobj = new OrderItem();
        otobj.EP_Is_Standard__c = true;
        otobj.PricebookEntryId = pbEntryPetrol.Id;
        otobj.UnitPrice = 1.00;
        otobj.Quantity = 1;
        otobj.OrderId = order1.Id;
        
        database.insert( otobj );
        system.assertNotEquals(null,otobj.Id); */
        return new order(); 
    
    }
     static EP_Supply_Location_Transporter__c createSupplyLocationTransporter( Id TransporterId, Id supplyLocId, Boolean isDefault ) 
    {
        EP_Supply_Location_Transporter__c SLT = new EP_Supply_Location_Transporter__c( 
                EP_Is_Default__c = isDefault,
                EP_Transporter__c = TransporterId,
                EP_Supply_Location__c = supplyLocId
            );
        Database.insert( SLT );
        return SLT;
    }
     static void activateShipto() 
    {
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(nonVmiShipToAccount);
        
        nonVmiShipToAccountConsignment.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(nonVmiShipToAccountConsignment);
    }
    
 public static Account createTestVendor(String vendorTypee, String NAV_VendorId,String COMPANY_CODE ,Company__c company)  {
     Id VENDOR_RT = Schema.SObjectType.Account.getRecordTypeInfosByName().get(VENDOR_ACCOUNT_RT).getRecordTypeId();
        Account vendorAcc = new Account( 
                                Name = 'Transporter 1',
                                EP_Status__c = '05-Active', 
                                RecordTypeId = VENDOR_RT, 
                                /* TFS fix 45559,45560,45567,45568 start EP_NAV_ID__c deprecated, EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c and EP_Vendor_Type__c is replaced by EP_VendorType__c*/ 
                                EP_VendorType__C = vendorTypee,
                                /* TFS fix 45559,45560,45567,45568 end*/
                                EP_Source_Entity_ID__c = NAV_VendorId,
                                EP_Puma_Company__c = company.Id,
                                BillingStreet = '123 st',
                                BillingCity = 'Gurgaon',
                                BillingState = 'Haryana',
                                BillingPostalCode = '123456',
                                BillingCountry = 'India',
                                Phone = '12345'
                            );
        return vendorAcc ;
    }
 static testMethod void modifyDraftSalesOrderPayment_success_1(){
        
        createTestData();
        Account vendorAccount = createTestVendor( VENDORTYPE, NAV_VENDOR_ID_1,COMP_CODE,comp );
        Database.insert( vendorAccount,false );
        system.assertNotEquals(null,vendorAccount.Id); 
        //CAM 2.7 start 
        billToAccount.EP_AvailableFunds__c= 0;
        
        sellToAccount.EP_AvailableFunds__c= 0;
        //CAM 2.7 end
        Database.update( billToAccount,sellToAccount );
        test.startTest();
        User cscUser = EP_TestDataUtility.createUser( cscAgentProfile.Id );
       // system.runas( cscUser )
      //  {
            createsupplyLocationData(vendorAccount);
      //  }
        //activateShipto();
        //createSupplyLocationTransporter( vendorAccount.Id, lSupplyLocation.Id,true);
            Order ord1;       
            orderType = EP_Common_Constant.NON_CONSIGNMENT; 
            //supplyLocationId = lSupplyLocation.Id;
            ord1 = createOrder( nonVmiShipToAccount.Id, vendorAccount.Id );
        
            Pagereference pr2 = Page.EP_PaymentPage; 
            pr2.getParameters().put('id',ord1.Id);
            test.setCurrentPage( pr2 );
            
            EP_PaymentPageController pgCont = new EP_PaymentPageController();
            EP_PaymentPageController.makePayment();
            pgCont.cancelPayment();
            
            Order ord = [select Id, Status from order where Id =: ord1.Id ];
            system.assertEquals( ord.Status, EP_Common_Constant.SUBMITTED);
          
         test.stopTest();
    }
    static testMethod void modifyDraftSalesOrderPayment_success_NegativeTest(){
        
        createTestData();
        Account vendorAccount = createTestVendor( VENDORTYPE, NAV_VENDOR_ID_1,COMP_CODE,comp );
        Database.insert( vendorAccount,false );
        system.assertNotEquals(null,vendorAccount.Id); 
        //CAM 2.7 start 
        billToAccount.EP_AvailableFunds__c= 0;
        
        sellToAccount.EP_AvailableFunds__c= 0;
        //CAM 2.7 end
        Database.update( billToAccount,sellToAccount );
        test.startTest();
        User cscUser = EP_TestDataUtility.createUser( cscAgentProfile.Id );
       // system.runas( cscUser )
      //  {
            createsupplyLocationData(vendorAccount);
      //  }
        //activateShipto();
        //createSupplyLocationTransporter( vendorAccount.Id, lSupplyLocation.Id,true);
            Order ord1 = new Order();    
            orderType = EP_Common_Constant.NON_CONSIGNMENT; 
            //supplyLocationId = lSupplyLocation.Id;
            //ord1 = createOrder( nonVmiShipToAccount.Id, vendorAccount.Id );
        
            Pagereference pr2 = Page.EP_PaymentPage; 
            pr2.getParameters().put('id',ord1.id);
            test.setCurrentPage( pr2 );
            try{
                 EP_PaymentPageController pgCont = new EP_PaymentPageController();
                //EP_PaymentPageController.makePayment();
                pgCont.cancelPayment();
                
                Order ord = [select Id, Status from order where Id =: ord1.Id ];
                system.assertEquals( ord.Status, EP_Common_Constant.SUBMITTED);
            }catch(exception ex){}
    
         test.stopTest();
    }

    }