/* 
  @Author <Rahul Jain>
   @name <EP_OrderConstant>
   @CreateDate <01/04/2017>
   @Description <This class will define the static values which will be used across the code in EP_CreateUpdateSyncOrder_SFtoNAV and EP_OrderSyncWithNav class>
   @Version <1.0>
 
*/
public with sharing class EP_OrderConstant { 
    public static final String invoiceComponent_tag = 'invoiceComponent';
    public static final String invoiceComponents_tag = 'invoiceComponents';
    public static final String lineComponents_tag = 'lineComponents';
    public static final String acctComponents_tag = 'acctComponents';
    public static final String OrderSyncWithNav_ClassName = 'OrderSyncWithNav';
    public static final String errorMessage_UpdateFailed = 'Update Failed';
    public static final String errorMessage_InsertFailed = 'Insert Failed';
    public static final String acctComponent_XMLtagOpen = '<acctComponent>';
    public static final String acctComponent_XMLtagClose = '</acctComponent>';
    public static final String componentCode_tag = 'componentCode';
    public static final String glAccountNr_tag = 'glAccountNr';
    public static final String baseAmount_tag = 'baseAmount';
    public static final String taxRate_tag = 'taxRate';
    public static final String taxAmount_tag = 'taxAmount';
    public static final String totalAmount_tag = 'totalAmount';
    public static final String isVAT_tag = 'isVAT';
    public static final String orderEpoch_tag = 'orderEpoch';
    public static final String orderOrigination_tag = 'orderOrigination';
    public static final String orderRefNr_tag = 'orderRefNr';
    public static final String processingStatus_tag = 'processingStatus';
    public static final String sellToRecordType = 'EP_Sell_To';
    public static final String billToRecordType = 'EP_Bill_To';
    public static final String vmiRecType = 'EP_VMI_SHIP_TO';
    public static final String nonVmiRecType = 'EP_NON_VMI_SHIP_TO';
    public static final String errMsg = 'No matching order records found in the database';
    public static final String noRecFound_errMsg = 'No Record found';
    public static final String SYNC_ORDERS = 'syncOrders';
    public static final String timeZoneNotation = 'T';
    public static final String colon = ':';
    public static final String nonVmiRecordType = 'EP_Non_VMI_Orders';
    public static final String INVENTORYAPPROVED = 'Inventory Approved';
    public static final String INVENTORYREJECTED = 'Inventory Rejected';
    public static final String productTypeBulk = 'Bulk';
    public static final String productTypeService = 'Service';
    public static final String productTypePackaged = 'Packaged';
    public static final String salesOrderType = 'Sales';
    public static final String recordType = 'recordType';
    public static final String salesOrderTy= 'Sales Order';
    public static final String OrderTy= 'Order';
    /**********************Constants Moved From Helper***********************/
    public static final String ORDER_UPDATE = 'ORDER_UPDATE';
    public static final String SFDC_TO_WINDMS_SYNC = 'SFDC_TO_WINDMS_SYNC';
    public static final String ORDER_WINDMS = EP_Common_Constant.ORDER_WINDMS;
    public static final String CLASSNAME  = 'EP_OrderTriggerHelper';
    public static final string NAV = 'NAV';
    public static final string LS = 'LS';//Defect #28272
    public static final string TABLE = 'Table';
    public static final string ASYNC = 'Async'; 
    public static final string NO = 'No';
    public static final string CUSTOMERSTR = 'Customer';
    public static final string REQUEST = 'Request';
    public static final String LOCKED_STATUS  ='Locked';
    public static final String PLANNED_STATUS  ='planned';
    public static final String UNLOCKED_STATUS  ='Un-locked';
    public static final String CANCELLED_STATUS  ='Cancelled';
    public static final String PLANNING_STATUS  ='Planning';
    public static final String LOADING_COMPLETED_STATUS  ='Loading completed';
    public static final String DELIVERY_COMPLETED_STATUS  ='Delivery completed';
    public static final String OSN_STRING  = 'OSN';
    public static final String ORDERSTATUSBEFOREINSERT  = 'updatePaymentTermAndMethod'; 
    public static final Set<String> CSC_PROFILES  = new Set<String>{'Puma CSC Agent_R1','Puma CSC Manager_R1'};
    public static final Set<String> CUSTOMER_PROFILES  = new Set<String>{'EP_Portal_Super_User','EP_Portal_Basic_Access_User','EP_Portal_Basic_Ordering_Access_User','EP_Portal_Manage_Access_User','EP_Portal_Finance_Access_User','EP - Retail VMI Site User_R1','Customer Community Plus User'};
    public static final Set<String> ADMIN_PROFILES  = new Set<String>{'System Admin Temp','System Administrator','EP_CRM_Admin'};//defect 20824
    public static final Set<String> BSMGM_PROFILES  = new Set<String>{'Puma BSM/GM Agent_R1'};
    public static final Set<String> APIUSER_PROFILES  = new Set<String>{'EP - Interface User'};//defect 20824
    public static final Set<String> CANCELLABLE_ORDER_STATUS_FOR_CSC  = new Set<String>{'Draft','Quote','Submitted','Accepted','Awaiting Payment','Planning','Awaiting Credit Review'};
    public static final Set<String> CANCELLABLE_ORDER_STATUS_FOR_CUSTOMER  = new Set<String>{'Draft','Quote','Submitted','Accepted','Awaiting Payment','Planning','Awaiting Credit Review'};
    public static final String WINDMS_STATUS_DELETED = 'Deleted';//defect #29683 new
    public static final String WINDMS_STATUS_DELETE_REQUESTED = 'Delete Requested';//defect #29683 new
    public static final String STRERR_REASON_FCHANGE = 'Please provide the reason for this change';
    public static final String STRERR_REASON_CHORDER = 'Please specify the other reason for changing this order in the appropriate text area';
    public static final String STRERR_ORDCANCEL_STATUS = 'Order can be cancelled only from status Draft, Submitted, Accepted, Awaiting Payment, and Planning by CSC';
    public static final String STRERR_ORDCANCEL_STATUS_CUS = 'Order can be cancelled only from status Draft, Submitted, Accepted, Awaiting Payment, and Planning by Customer';
    public static final String STRERR_ORD_CAN_ANY = 'Order can only be cancelled by Admin, CSC or Customer';
    public static final String WINDMS_UPDATE_ORDER = 'updateOrderStatusNonVMIwinDMS';
    public static final String STR_SYNCORDERSTATUSWITHNAV = 'syncorderstatuswithNav';
    // DEFECT PE-2422 - Start
    public static final string ordNum = '{!Order.OrderNumber}';
    public static final string ordReasonNonDel = '{!Order.EP_Reason_For_Non_Delivery__c}';
    public static final string strSubject = 'Non-Delivery Notification - Action Required';
    // DEFECT PE-2422 - End
    
    public static final string METHOD_SELLTO_BILLTO = 'setSellToBillTo';
    public static final string METHOD_UPDATE_PRICING_FAIL = 'updatePricingFailure';
    public static final string METHOD_CREDITCRED_EXC = 'createCredExcForRoOrder';
    public static final string METHOD_ORDER_CREDIT_CHK = 'orderCreditCheck';
    public static final string METHOD_ORDER_APPROVAL = 'orderApproval';
    public static final string METHOD_SUPPLY_PRICING = 'setSupplyLocPricing'; 
    
    //Aravind added below constants
    //Order status constants begins

    public static final String OrderEvent_ToString=' To ';
    
    public static final String OrderState_Draft='Draft';
    public static final String OrderState_Awaiting_Credit_Review='Awaiting Credit Review';
    public static final String OrderState_Awaiting_Inventory_Review='Awaiting Inventory Review';
    public static final String OrderState_Awaiting_Payment='Awaiting Payment';
    public static final String OrderState_Submitted='Submitted';
    public static final String OrderState_Cancelled='Cancelled';
    public static final String OrderState_Accepted='Accepted';
    public static final String OrderState_Planning='Planning';
    public static final String OrderState_Planned='Planned';
    public static final String OrderState_Loaded='Loaded';  
    public static final String OrderState_Delivered='Delivered';
    public static final String OrderState_Invoiced='Invoiced';
    
    // Order record type constants
    public static final String Order_Type_NonVMI='Non-VMI Orders';
    public static final String Order_Shipto_RecordType_NonVMI='Non-VMI Ship To';
    public static final String Order_Shipto_RecordType_VMI='VMI Ship To';
    public static final String Order_Epoch_Current='Current';  
    public static final String Order_Epoch_Retro='Retrospective';      
    public static final String Order_Shipto_Non_Consignment_Type='Non-Consignment';
    public static final String Order_Shipto_Consignment_Type='Consignment';

    
    //Order status constants ends
    
    // Constants used in Statemachine;
    public static final String ORDER_RETRO='Retro';
    
    //Order type definitions starts
    public static final String NON_VMI_NON_CONSIGNMENT='NonVMINonConsignment';
    public static final String RETRO_NON_VMI_NON_CONSIGNMENT='RetroNonVMINonConsignment';
    
    //Order type definitions ends
    
    // Order Events
    public static final String USER_SUBMIT='UserSubmit';
    public static final String USER_EDIT='UserEdit';
    public static final String USER_CANCEL='UserCancel';
    public static final String USER_EXCEPTION_ACKNOWLEDGED='UserExceptionAcknowledged';
    public static final String UserExceptionApproved='UserExceptionApproved';
    public static final String UserExceptionRejected='UserExceptionRejected';
    public static final String APPROVALPROCESS='Approval Process';
    public static final String VMINEWORDER= 'VMInewOrder';
    public static final String ACKNOWLEDGED='Acknowledged';
    public static final String SUBMITTEDACKNOWLEDGED = 'SubmittedAcknowledged';
    //Defect Fix Start #57729
    public static final String ACCEPTEDACKNOWLEDGED = 'AcceptedAcknowledged';
    //Defect Fix End #57729
    
    //Defect Fix Start #57591
    public static final String IMPORTEDORDERPRICINGRESPONSE = 'ImportedOrderPricingResponse';
    //Defect Fix End #57591
    
    //Defect Fix Start #57734
    public static final String IMPORTEDORDERSUMITTED = 'ImportedOrderSubmitted';
    //Defect Fix End #57734
    
    // Order Events Message
    public static final String OVERDUE_MESSAGE='Overdue';
    public static final String CREDITISSUE_MESSAGE='CreditIssue';
    public static final String INVENTORY_ISSUE='InventoryIssue';
    public static final String PAYMENT_ISSUE='PaymentIssue';
    
    //   
    public static integer SCHEDULE_RECORD_COUNT = 0; 

    //For Order Domain
    public static String INTEGERATION = 'integeration';
    
    public static final String SHIP_TO_BLOCKED = 'Ship to is blocked';
    public static final String SELL_TO_BLOCKED = 'Sell to is blocked';
    public static final String FUNDS_NOT_AVAILABLE = 'Funds Not Available';
    public static final String PRICING_REQUEST_FAILED ='PRICING REQUEST FAILED';
    public static final String TANK_BLOCKED ='Tank Blocked';
    public static final String TANK_STOPPED = 'Tank Stopped';
    public static final String CREDIT_FAIL = 'Credit Fail';

    /* Outbound Intergertation constants used by Apex class */
    public static final string XML_BEGIN = '<?xml version="1.0" encoding="UTF-8"?>';
    public static final string MSG = 'MSG';
    public static final string HEADER_COMMON = 'HeaderCommon';
    public static final string STATUS_PAYLOAD = 'StatusPayload';

    // XML Header tags
    public static final String MsgID = 'MsgID';
    public static final String InterfaceType = 'InterfaceType';
    public static final String SourceGroupCompany = 'SourceGroupCompany';
    public static final String DestinationGroupCompany = 'DestinationGroupCompany';
    public static final String SourceCompany = 'SourceCompany';
    public static final String DestinationCompany = 'DestinationCompany';
    public static final String CorrelationID = 'CorrelationID';
    public static final String DestinationAddress = 'DestinationAddress';
    public static final String SourceResponseAddress = 'SourceResponseAddress';
    public static final String SourceUpdateStatusAddress = 'SourceUpdateStatusAddress';
    public static final String DestinationUpdateStatusAddress = 'DestinationUpdateStatusAddress';
    public static final String MiddlewareUrlForPush = 'MiddlewareUrlForPush';
    public static final String EmailNotification = 'EmailNotification';
    public static final String ErrorCode = 'ErrorCode';
    public static final String ErrorDescription = 'ErrorDescription';
    public static final String ProcessingErrorDescription = 'ProcessingErrorDescription';
    public static final String ContinueOnError = 'ContinueOnError';
    public static final String ComprehensiveLogging = 'ComprehensiveLogging';
    public static final String TransportStatus = 'TransportStatus';
    public static final String ProcessStatus = 'ProcessStatus';
    public static final String UpdateSourceOnReceive = 'UpdateSourceOnReceive';
    public static final String UpdateSourceOnDelivery = 'UpdateSourceOnDelivery';
    public static final String UpdateSourceAfterProcessing = 'UpdateSourceAfterProcessing';
    public static final String UpdateDestinationOnDelivery = 'UpdateDestinationOnDelivery';
    public static final String CallDestinationForProcessing = 'CallDestinationForProcessing';
    public static final String ObjectType = 'ObjectType';
    public static final String ObjectName = 'ObjectName';
    public static final String CommunicationType = 'CommunicationType';

    // payload xml constants
    public static final String creditChecks = 'creditChecks';
    public static final String creditCheck = 'creditCheck';
    public static final String seqId = 'seqId';
    public static final String contractStartDt = 'contractStartDt';
    public static final String contractEndDt = 'contractEndDt';
    public static final String priceApplicability = 'priceApplicability';
    public static final String salesContractNr = 'salesContractNr';
    public static final String operationType = 'operationType';
    public static final String contractEnabled = 'contractEnabled';
    public static final String amountLCY = 'amountLCY';
    public static final String totalAmount = 'totalAmount';
    public static final String totalAmountLCY = 'totalAmountLCY';
    public static final String taxAmountLCY = 'taxAmountLCY';
    public static final String baseAmount = 'baseAmount';
    public static final String baseAmountLCY = 'baseAmountLCY';
    public static final String taxRate = 'taxRate';
    public static final String isVAT = 'isVAT';
    public static final String Identifier = 'Identifier';
    public static final String identifier_windmsxml = 'identifier';
    public static final String orderIdWinDMS = 'orderIdWinDMS';
    public static final String orderIdSF = 'orderIdSF';
    public static final String orderRefNr = 'orderRefNr';
    public static final String customerIntRefNr = 'customerIntRefNr';
    public static final String creditStatus = 'creditStatus';
    public static final String reasonCode = 'reasonCode';
    public static final String Payload = 'Payload';
    public static final String any0 = 'any0';
    public static final String productType = 'productType';
    public static final String pctQntChange = 'pctQntChange';
    public static final String loadCode = 'loadCode';
    public static final String route = 'route';

    public static final String Orders = 'Orders';
    public static final String Order = 'order';
    public static final String orders_windmsxml = 'orders';
    public static final String order_windmsxml = 'order';
    public static final String orderId = 'orderId';
    public static final String entrprsId = 'entrprsId';
    public static final String sellToId = 'sellToId';
    public static final String shipToId = 'shipToId';
    public static final String reqDlvryDt = 'reqDlvryDt';
    public static final String expctdDlvryDt = 'expctdDlvryDt';
    public static final String orderStartDt = 'orderStartDt';
    public static final String loadingDt = 'loadingDt';
    public static final String componentCode = 'componentCode';
    public static final String glAccountNr = 'glAccountNr';
    public static final String dlvryDt = 'dlvryDt';
    public static final String deliveryType = 'deliveryType';
    public static final String deliveryExRackWithSch = 'Ex-Rack with Scheduling';
    public static final String transportManagmnt = 'transportManagmnt';
    public static final String docStatus = 'docStatus';
    public static final String orderType = 'orderType';
    public static final String totalOrderQty = 'totalOrderQty';
    public static final String paymentTerm = 'paymentTerm';
    public static final String paymentMethod = 'paymentMethod';
    public static final String paymentType = 'paymentType';
    public static final String currencyId = 'currencyId';
    public static final String pricingDate = 'pricingDate';
    public static final String priceValidityPeriod = 'priceValidityPeriod';
    public static final String pricingStockHldngLocId = 'pricingStockHldngLocId';
    public static final String logisticStockHldngLocId = 'logisticStockHldngLocId';
    public static final String comment = 'comment';
    public static final String exceptionNr = 'exceptionNr';
    public static final String pricingTransporterCode = 'pricingTransporterCode';
    public static final String logisticTransporterCode = 'logisticTransporterCode';
    public static final String tripId = 'tripId';
    public static final String orderEpoch = 'orderEpoch';
    public static final String orderOrigination = 'orderOrigination';
    public static final String calculatePrice = 'calculatePrice';
    public static final String onRun = 'onRun';
    public static final String runId = 'runId';
    public static final String versionNr = 'versionNr';
    public static final String contractId = 'contractId';
    public static final String bulkOrderNr = 'bulkOrderNr';
    public static final String clientId = 'clientId';
    public static final String OrderLines = 'orderLines';
    public static final String OrderLine = 'orderLine';
    public static final String OrderLines_windmsxml = 'orderLines';
    public static final String OrderLine_windmsxml = 'orderLine';
    public static final String lineId = 'lineId';
    public static final String itemId = 'itemId';
    public static final String qty = 'qty';
    public static final String uOm = 'uom';
    public static final String unitPrice = 'unitPrice';
    public static final String loadedAmbientQty = 'loadedAmbientQty';
    public static final String loadedStandardQty = 'loadedStandardQty';
    public static final String deliveredAmbientQty = 'deliveredAmbientQty';
    public static final String deliveredStandardQty = 'deliveredStandardQty';
    public static final String bols = 'bols';
    public static final String bol = 'bol';
    public static final String bolId = 'bolId';
    public static final String supplierId = 'supplierId';
    public static final String lineComponents = 'lineComponents';
    public static final String invoiceComponents = 'invoiceComponents';
    public static final String invoiceComponent = 'invoiceComponent';
    public static final String TYPE = 'type';
    public static final String name = 'name';
    public static final String amount = 'amount';
    public static final String taxPercentage = 'taxPercentage';
    public static final String taxAmount = 'taxAmount';
    public static final String acctComponents = 'acctComponents';
    public static final String acctComponent = 'acctComponent';
    public static final String pymntTerm = 'pymntTerm';
    public static final String prodcutSoldAs = 'prodcutSoldAs';

    public static final String purchaseOrder = 'purchaseOrder';
    public static final String transporterCode = 'transporterCode';
    public static final String stockHldngLocId = 'stockHldngLocId';
    public static final String shiftDate = 'shiftDate';
    public static final String shiftId = 'shiftId';
    public static final String orderFromDt = 'orderFromDt';
    public static final String urgentOrder = 'urgentOrder';
    public static final String remark1 = 'remark1';
    public static final String remark2 = 'remark2';
    public static final String orderDlvrNr = 'orderDlvrNr';
    public static final String modifyOn = 'modifyOn';
    public static final String modifyBy = 'modifyBy';
    public static final String creditIndctr = 'creditIndctr';
    public static final String action = 'action';
    public static final String description = 'description';
    public static final String tankId = 'tankId';
    public static final String isTankFill = 'isTankFill';
    public static final String UOM_SMALL = 'uom';
    public static final String transportManagement = 'transportManagement';

    public static final String requestHeader = 'requestHeader';
    public static final String pricingRequest = 'pricingRequest';
    public static final String priceRequestSource = 'priceRequestSource';
    public static final String companyCode = 'companyCode';
    public static final String priceType = 'priceType';
    public static final String priceDate = 'priceDate';
    public static final String totalOrderQuantity = 'totalOrderQuantity';
    public static final String applyOrderQuantity = 'applyOrderQuantity';
    public static final String customerId = 'customerId';
    public static final String supplyLocationId = 'supplyLocationId';
    public static final String transporterId = 'transporterId';
    public static final String priceConsolidationBasis = 'priceConsolidationBasis';
    public static final String requestLines = 'requestLines';
    public static final String line = 'line';
    public static final String lineItemId = 'lineItemId';
    public static final String quantity = 'quantity';
    public static final String currencyCode = 'currencyCode';
    
    //Credit exception request xml constants
    public static final String creditExceptionRequests = 'creditExceptionRequests';
    public static final String creditExceptionRequest = 'creditExceptionRequest';
    public static final String exceptionNo = 'exceptionNo';
    public static final String billTo = 'billTo';
    public static final String requestedDate = 'requestedDate';
    public static final String balanceCreditLimit = 'balanceCreditLimit';
    public static final String orderValue = 'orderValue';
    public static final String approvalStatus = 'approvalStatus';
    public static final String rejectionReason = 'rejectionReason';
    public static final String openOrderValueAmount = 'openOrderValueAmount';
    public static final String invoiceOverDue = 'invoiceOverDue';
    public static final String isRetrospective = 'isRetrospective';
    //45435 Perform Credit Exception Review --Start
    public static final String createdDate = 'createdDt';
    public static final String orderPymntType = 'orderPymntType';
    public static final String deliveryDt = 'deliveryDt';
    public static final String senderId = 'senderId';
    public static final String salesPrsnCd = 'salesPrsnCd';
    public static final String requestedOn = 'requestedOn';
    public static final String requestStatus = 'requestStatus';
    //45435 Perform Credit Exception Review --End

    public static final String FALSE_STR = 'false';
    
        //L4 #45419 start
     public static final string CS_TAG = 'CS';
     public static final string PUMAORDERLINEDEFNAME = 'Puma Energy Order Line';
     public static final string ON_STR = 'On';
     public static final string OFF_STR = 'Off';
     public static final string TRUE_STR = 'True';
     public static final string FALSE_TAG = 'False';
     public static final string NO_TAG = 'NO';
     public static final string OK_TAG = 'OK';
     
     //L4-#45424,#45426,#45431 code changes Start
     public static final String entityType = 'entityType';
     //L4-#45424,#45426,#45431 code changes End
}