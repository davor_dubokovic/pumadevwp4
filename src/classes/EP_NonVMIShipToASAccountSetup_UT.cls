@isTest
public class EP_NonVMIShipToASAccountSetup_UT
{
    static final string EVENT_NAME = '04-AccountSet-upTo04-AccountSet-up';
    static final string INVALID_EVENT_NAME = '08-RejectedTo02-BasicDataSetup';
    
    /*  
    @description: method to intialise data
    */
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    
    static testMethod void setAccountDomainObject_test() {
        EP_NonVMIShipToASAccountSetup localObj = new EP_NonVMIShipToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASAccountSetupDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        System.AssertEquals(obj,localObj.account);
        
    }
    static testMethod void doOnEntry_test() {
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());        
        EP_NonVMIShipToASAccountSetup localObj = new EP_NonVMIShipToASAccountSetup();
        Account newAccount = EP_TestDataUtility.getNonVMIShipToASAccountSetupDomainObject().getAccount();
        Account sellToAccount = [select id,EP_Status__c from Account Where Id =: newAccount.ParentId Limit 1];
        sellToAccount.EP_Status__c = EP_AccountConstant.ACTIVE;
        update sellToAccount;
        Account oldAccount = newAccount.clone();
        oldAccount.EP_Status__c = EP_AccountConstant.BASICDATASETUP;
        EP_AccountDomainObject obj  = new EP_AccountDomainObject(newAccount, oldAccount);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        system.debug('------oldAccount.EP_Status__c = EP_AccountConstant.BASICDATASETUP; '+ oldAccount.EP_Status__c );        
        System.AssertEquals(EP_AccountConstant.BASICDATASETUP,oldAccount.EP_Status__c);  
    }
    static testMethod void doOnEntry_test2() {
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());        
        EP_NonVMIShipToASAccountSetup localObj = new EP_NonVMIShipToASAccountSetup();
        Account newAccount = EP_TestDataUtility.getNonVMIShipToASAccountSetupDomainObject().getAccount();
        Account sellToAccount = [select id,EP_Status__c from Account Where Id =: newAccount.ParentId Limit 1];
        sellToAccount.EP_Status__c = EP_AccountConstant.ACTIVE;
        update sellToAccount;
        newAccount.EP_Synced_PE__c = true;
        Account oldAccount = newAccount.clone();
        oldAccount.EP_Status__c = EP_AccountConstant.BASICDATASETUP;
        EP_AccountDomainObject obj  = new EP_AccountDomainObject(newAccount, oldAccount);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        system.debug('------oldAccount.EP_Status__c = EP_AccountConstant.BASICDATASETUP; '+ oldAccount.EP_Status__c );        
        System.AssertEquals(EP_AccountConstant.BASICDATASETUP,oldAccount.EP_Status__c);  
    }
    static testMethod void doOnEntry_test3() {
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());        
        EP_NonVMIShipToASAccountSetup localObj = new EP_NonVMIShipToASAccountSetup();
        Account newAccount = EP_TestDataUtility.getNonVMIShipToASAccountSetupDomainObject().getAccount();
        Account sellToAccount = [select id,EP_Status__c from Account Where Id =: newAccount.ParentId Limit 1];
        sellToAccount.EP_Status__c = EP_AccountConstant.ACTIVE;
        update sellToAccount;
        newAccount.EP_Synced_PE__c = true;
         newAccount.EP_Synced_NAV__c = true;
        Account oldAccount = newAccount.clone();
        oldAccount.EP_Status__c = EP_AccountConstant.BASICDATASETUP;
        EP_AccountDomainObject obj  = new EP_AccountDomainObject(newAccount, oldAccount);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        system.debug('------oldAccount.EP_Status__c = EP_AccountConstant.BASICDATASETUP; '+ oldAccount.EP_Status__c );        
        System.AssertEquals(EP_AccountConstant.BASICDATASETUP,oldAccount.EP_Status__c);  
    }
    static testMethod void doOnExit_test() {
        EP_NonVMIShipToASAccountSetup localObj = new EP_NonVMIShipToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASAccountSetupDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        //Method has no implementation, hence adding dummy assert
		system.assert(true); 
    }
    static testMethod void doTransition_PositiveScenariotest() {
        EP_NonVMIShipToASAccountSetup localObj = new EP_NonVMIShipToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASAccountSetupDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_NonVMIShipToASAccountSetup localObj = new EP_NonVMIShipToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASAccountSetupDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        List<Exception> excList = new List<Exception>();
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
            }
        catch(Exception e){
            excList.add(e);
        }
        Test.stopTest();
        System.Assert(excList.size() > 0);
    }
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_NonVMIShipToASAccountSetup localObj = new EP_NonVMIShipToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASAccountSetupDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
 
}