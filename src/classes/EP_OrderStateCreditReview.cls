/*   
     @Author Aravindhan Ramalingam
     @name <EP_OrderStateCreditReview.cls>     
     @Description <Order State for Awaiting credit review status, common for all order types. Create a seperate classes if logic differs based on order type>   
     @Version <1.1> 
     */

    public class EP_OrderStateCreditReview extends EP_OrderState {
        
        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            super.setOrderDomainObject(currentOrder);
        }
        
        
        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_OrderStateCreditReview','doOnEntry');
            
        }

        public static String getTextValue()
        {
            return EP_OrderConstant.OrderState_Awaiting_Credit_Review;
        }
    }