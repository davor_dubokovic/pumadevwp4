/* 
  @Author <Jai Singh>
   @name <EP_ProductSyncNAVtoSFDC_Test>
   @CreateDate <22/04/2016>
   @Description <This is apex test class for EP_ProductSyncNAVtoSFDC and EP_ProductSyncHandler> 
   @Version <1.0>
*/
@isTest
private class EP_ProductSyncNAVtoSFDC_Test {
    
    private static final string COMPANY_CODE1 = 'AUN';
    private static final string COMPANY_CODE2 = 'EGL';
    private static final string COMPANY_CODE_NEW = 'TEST_COMP';
    
    private static final string PRODUCTCODE1 = 'CODE1';
    private static final string PRODUCTCODE2 = 'CODE2';
    
    private static final string PRODUCTNAME1 = 'Name1';
    private static final string PRODUCTNAME2 = 'Name2';
    private static final string PRODUCTNAME_NEW = 'Update Name';
    
    private static final string PRODUCTFAMILY1 = 'Family1';
    private static final string PRODUCTFAMILY2 = 'Family2';
    private static final string PRODUCTFAMILY_NEW = 'Update Family';
    
    private static final string SEQID1 = '112345';
    private static final string SEQID2 = '212345';
    
    private static final string DESCRIPTION1 = 'Test Description 1';
    private static final string DESCRIPTION2 = 'Test Description 2';
    private static final string DESCRIPTION_NEW = 'Update Description';
    
    private static final string ITEMSOLDAS1 = 'Packaged';
    private static final string ITEMSOLDAS2 = 'Bulk';
    
    private static final string SFDCBASEURL = URL.getSalesforceBaseUrl().toExternalForm();
    private static final string PRODUCTSYNCENDPOINT = '/services/apexrest/v1/ProductSync/*';
    private static Company__c company1;
    private static Company__c company2;
    
    private static Product2  productObj1;
    private static Product2  productObj2;
    
    private static String uom;
    private static final String UOM_NEW = 'TEST UOM '+DateTime.now();
    private static final String UOM_ERROR = 'INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST';
    
    //<'{"items":'>
    private static final String ITEMS_JSON_NODE = EP_Common_Constant.LEFT_CURLY_BRACE + EP_Common_Constant.DOUBLE_QUOTES_STRING + EP_Common_Constant.ITEMS_NODE + EP_Common_Constant.DOUBLE_QUOTES_STRING + ( EP_Common_Constant.COLON_WITH_SPACE.trim() );
    
    
    /*
    Method to create data for tesing
    of object Company__c
    */
    static void setupCompanyData()
    {
        company1 = EP_TestDataUtility.createCompany( COMPANY_CODE1 );
        Database.insert(company1);
        system.AssertNotEquals( null, company1.Id );
        company2 = EP_TestDataUtility.createCompany( COMPANY_CODE2 );
        Database.insert(company2 );
        system.AssertNotEquals( null, company2.Id );
    }
    
    /*
    Method to create data for tesing
    of object Product2
    */
    static void setupProductData()
    {
        String uniqueKey1 = PRODUCTCODE1 + EP_Common_Constant.STRING_HYPHEN + COMPANY_CODE1;
        productObj1 = new product2(Name = PRODUCTNAME1, EP_NAV_Product_Company__c = uniqueKey1, EP_Company_Lookup__c = company1.Id, 
                                    CurrencyIsoCode = EP_Common_Constant.GBP, family = PRODUCTFAMILY1, ProductCode = PRODUCTCODE1, 
                                    EP_Unit_of_Measure__c = uom, EP_Product_Sold_As__c = ITEMSOLDAS1);
        Database.insert(productObj1);
        system.AssertNotEquals( null, productObj1.Id );
        String uniqueKey2 = PRODUCTCODE2 + EP_Common_Constant.STRING_HYPHEN + COMPANY_CODE2;
        productObj2 = new product2(Name = PRODUCTNAME2, EP_NAV_Product_Company__c = uniqueKey2, EP_Company_Lookup__c = company2.Id, 
                                    CurrencyIsoCode = EP_Common_Constant.GBP, family = PRODUCTFAMILY2, ProductCode = PRODUCTCODE2, 
                                    EP_Unit_of_Measure__c = uom, EP_Product_Sold_As__c = ITEMSOLDAS1 );
        Database.insert(productObj2);
        system.AssertNotEquals( null, productObj2.Id );
    }
    
    /*
    Method to cover postive senario of product creation from 
    REST RESOURCE class EP_ProductSyncNAVtoSFDC
    */
    static testMethod void createProductSuccessTest()  
    {
        Schema.DescribeFieldResult fieldResult = Product2.EP_Unit_of_Measure__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        if( !ple.isEmpty() )
        {
            uom = ple[0].getValue();
        }
        else
        {
            uom = EP_Common_Constant.UOM_STRING;
        }
        //insert company
        setupCompanyData();
        
        EP_ProductSyncHandler.WrapperIdentifier wrapperIdent = new EP_ProductSyncHandler.WrapperIdentifier();
        wrapperIdent.itemId = PRODUCTCODE1;
        wrapperIdent.clientId = COMPANY_CODE1;
        
        EP_ProductSyncHandler.WrapperProduct wrapperProd = new EP_ProductSyncHandler.WrapperProduct();
        wrapperProd.seqId = SEQID1;
        wrapperProd.identifier = wrapperIdent;
        wrapperProd.name = PRODUCTNAME1;
        wrapperProd.description = DESCRIPTION1;
        wrapperProd.uom = uom;
        wrapperProd.category = PRODUCTFAMILY1;
        wrapperProd.itemSoldAs = ITEMSOLDAS1;
        //L4 - 45514&45515 Code change start
        wrapperProd.block = 'No';
        wrapperProd.enterpriseId = '123456';
        wrapperProd.isClean = 'Yes';
        //L4 - 45514&45515 Code change end
        
        EP_ProductSyncHandler.WrapperProducts wrapperProds = new EP_ProductSyncHandler.WrapperProducts();
        wrapperProds.item = new list<EP_ProductSyncHandler.WrapperProduct>{wrapperProd};   
        
        String requestBody = JSON.serialize(wrapperProds);   
        requestBody = ITEMS_JSON_NODE + requestBody+ EP_Common_Constant.RIGHT_CURLY_BRACE ; //'{"items":'+requestBody+'}';
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        res.statusCode = Integer.valueOf( EP_Common_Constant.CODE_200 );
        
        req.requestURI = SFDCBASEURL + PRODUCTSYNCENDPOINT;
        req.httpMethod = EP_Common_Constant.POST;
        req.requestBody = Blob.valueOf( requestBody );
        
        req.addHeader( EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON );
        RestContext.request = req;
        RestContext.response = res;
        
        test.startTest();
        EP_ProductSyncNAVtoSFDC.productSync(); 
        test.stopTest();
        
        String uniqueKey = wrapperIdent.itemId + EP_Common_Constant.STRING_HYPHEN + wrapperIdent.clientId;
        //L4 - #45514 & 45515 code change start
        Product2 newProduct = [Select Id, Name, EP_Company__c, Family, ProductCode,IsActive, EP_Unit_of_Measure__c,EP_Enterprise_ID__c,EP_Dirty_Product__c, Description from Product2 WHERE EP_NAV_Product_Company__c =: uniqueKey LIMIT 1 ];
        system.AssertEquals( newProduct.Name, PRODUCTNAME1 );
        system.AssertEquals( newProduct.ProductCode, PRODUCTCODE1 );
        system.AssertEquals( newProduct.EP_Company__c, COMPANY_CODE1 );
        system.AssertEquals( newProduct.Family, PRODUCTFAMILY1 );
        system.AssertEquals( newProduct.EP_Unit_of_Measure__c, uom );
        system.AssertEquals( newProduct.Description, DESCRIPTION1 );
        system.AssertEquals( newProduct.IsActive, TRUE );
        system.AssertEquals( newProduct.EP_Dirty_Product__c, FALSE );
        system.AssertEquals( newProduct.EP_Enterprise_ID__c, 123456 );
        //L4 - #45514 & 45515 code change end
    }
    
    
    /*
    Method to cover postive senario of product update from 
    REST RESOURCE class EP_ProductSyncNAVtoSFDC
    */
    static testMethod void updateProductSuccessTest()  
    {
        Schema.DescribeFieldResult fieldResult = Product2.EP_Unit_of_Measure__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        if( !ple.isEmpty() )
        {
            uom = ple[0].getValue();
        }
        else
        {
            uom = EP_Common_Constant.UOM_STRING;
        }
        //insert company
        setupCompanyData();
        //insert product
        setupProductData();
        
        EP_ProductSyncHandler.WrapperIdentifier wrapperIdent = new EP_ProductSyncHandler.WrapperIdentifier();
        wrapperIdent.itemId = PRODUCTCODE1;
        wrapperIdent.clientId = COMPANY_CODE1;
        
        EP_ProductSyncHandler.WrapperProduct wrapperProd = new EP_ProductSyncHandler.WrapperProduct();
        wrapperProd.seqId = SEQID1;
        wrapperProd.identifier = wrapperIdent;
        wrapperProd.name = PRODUCTNAME_NEW;
        wrapperProd.description = DESCRIPTION_NEW;
        wrapperProd.uom = uom;
        wrapperProd.category = PRODUCTFAMILY_NEW;
        wrapperProd.itemSoldAs = ITEMSOLDAS1;
        //L4 - 45514&45515 Code change start
        wrapperProd.block = 'Yes';
        wrapperProd.enterpriseId = '123456';
        wrapperProd.isClean = 'No';
        //L4 - 45514&45515 Code change end
        
        EP_ProductSyncHandler.WrapperProducts wrapperProds = new EP_ProductSyncHandler.WrapperProducts();
        wrapperProds.item = new list<EP_ProductSyncHandler.WrapperProduct>{wrapperProd};   
        
        String requestBody = JSON.serialize(wrapperProds);   
        requestBody = ITEMS_JSON_NODE + requestBody+ EP_Common_Constant.RIGHT_CURLY_BRACE ; //'{"items":'+requestBody+'}';
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        res.statusCode = Integer.valueOf( EP_Common_Constant.CODE_200 );
        
        req.requestURI = SFDCBASEURL + PRODUCTSYNCENDPOINT;
        req.httpMethod = EP_Common_Constant.POST;
        req.requestBody = Blob.valueOf( requestBody );
        
        req.addHeader( EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON );
        RestContext.request = req;
        RestContext.response = res;
        
        test.startTest();
        EP_ProductSyncNAVtoSFDC.productSync(); 
        test.stopTest();
        
        String uniqueKey = wrapperIdent.itemId + EP_Common_Constant.STRING_HYPHEN + wrapperIdent.clientId;
        //L4 - #45514 & 45515 Code change start
        Product2 newProduct = [Select Id, Name, EP_Company__c, Family, ProductCode,IsActive,EP_Enterprise_ID__c, EP_Dirty_Product__c, EP_Unit_of_Measure__c, Description from Product2 WHERE EP_NAV_Product_Company__c =: uniqueKey LIMIT 1 ];
        
        system.AssertEquals( newProduct.Name, PRODUCTNAME_NEW );
        system.AssertEquals( newProduct.ProductCode, PRODUCTCODE1 );
        system.AssertEquals( newProduct.EP_Company__c, COMPANY_CODE1 );
        system.AssertEquals( newProduct.Family, PRODUCTFAMILY_NEW );
        system.AssertEquals( newProduct.EP_Unit_of_Measure__c, uom );
        system.AssertEquals( newProduct.IsActive, FALSE );
        system.AssertEquals( newProduct.EP_Dirty_Product__c, TRUE );
        system.AssertEquals( newProduct.EP_Enterprise_ID__c, 123456 );
        //L4 - #45514 & 45515 Code change end
    }
    
    
    /*
    Method to cover senario of product creation required fields missing from 
    REST RESOURCE class EP_ProductSyncNAVtoSFDC
    */
    static testMethod void createProductFail_RequiredFieldMissingTest()  
    {
        //insert company
        setupCompanyData();
        
        EP_ProductSyncHandler.WrapperIdentifier wrapperIdent = new EP_ProductSyncHandler.WrapperIdentifier();
        wrapperIdent.itemId = PRODUCTCODE1;
        wrapperIdent.clientId = COMPANY_CODE1;
        
        EP_ProductSyncHandler.WrapperProduct wrapperProd = new EP_ProductSyncHandler.WrapperProduct();
        wrapperProd.seqId = SEQID1;
        wrapperProd.identifier = wrapperIdent;
        //wrapperProd.name = PRODUCTNAME1;
        wrapperProd.description = DESCRIPTION1;
        //wrapperProd.uom = uom;
        //wrapperProd.category = PRODUCTFAMILY1;
        wrapperProd.itemSoldAs = ITEMSOLDAS1;
        
        
        EP_ProductSyncHandler.WrapperProducts wrapperProds = new EP_ProductSyncHandler.WrapperProducts();
        wrapperProds.item = new list<EP_ProductSyncHandler.WrapperProduct>{wrapperProd};   
        
        String requestBody = JSON.serialize(wrapperProds);   
        requestBody = ITEMS_JSON_NODE + requestBody+ EP_Common_Constant.RIGHT_CURLY_BRACE ; //'{"items":'+requestBody+'}';
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        res.statusCode = Integer.valueOf( EP_Common_Constant.CODE_200 );
        
        req.requestURI = SFDCBASEURL + PRODUCTSYNCENDPOINT;
        req.httpMethod = EP_Common_Constant.POST;
        req.requestBody = Blob.valueOf( requestBody );
        
        req.addHeader( EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON );
        RestContext.request = req;
        RestContext.response = res;
        
        test.startTest();
        EP_ProductSyncNAVtoSFDC.productSync(); 
        test.stopTest(); 
        
        String responseString = res.responseBody.toString();
        String uniqueKey = wrapperIdent.itemId + EP_Common_Constant.STRING_HYPHEN + wrapperIdent.clientId;
        list<Product2> newProductList = [Select Id, Name, EP_Company__c, Family, ProductCode, EP_Unit_of_Measure__c, Description from Product2 WHERE EP_NAV_Product_Company__c =: uniqueKey LIMIT 1 ];
        system.AssertEquals( newProductList.isEmpty(), TRUE );
        system.AssertEquals( responseString.containsIgnoreCase( EP_ProductSyncHandler.UOM_BLANK.removeEnd(EP_Common_Constant.COMMA_STRING.trim() )), TRUE );  
        system.AssertEquals( responseString.containsIgnoreCase( EP_ProductSyncHandler.NAME_BLANK.removeEnd(EP_Common_Constant.COMMA_STRING.trim() )), TRUE ); 
        system.AssertEquals( responseString.containsIgnoreCase( EP_ProductSyncHandler.CATEGORY_BLANK.removeEnd(EP_Common_Constant.COMMA_STRING.trim() )), TRUE );
    }
    
    /*
    Method to cover senario of product creation of company does not exist in SFDC missing from 
    REST RESOURCE class EP_ProductSyncNAVtoSFDC
    */
    static testMethod void createProductFail_CompanyDoesNotExistTest()  
    {
        Schema.DescribeFieldResult fieldResult = Product2.EP_Unit_of_Measure__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        if( !ple.isEmpty() )
        {
            uom = ple[0].getValue();
        }
        else
        {
            uom = EP_Common_Constant.UOM_STRING;
        }
        EP_ProductSyncHandler.WrapperIdentifier wrapperIdent = new EP_ProductSyncHandler.WrapperIdentifier();
        wrapperIdent.itemId = PRODUCTCODE1;
        wrapperIdent.clientId = COMPANY_CODE1;
        
        EP_ProductSyncHandler.WrapperProduct wrapperProd = new EP_ProductSyncHandler.WrapperProduct();
        wrapperProd.seqId = SEQID1;
        wrapperProd.identifier = wrapperIdent;
        wrapperProd.name = PRODUCTNAME1;
        wrapperProd.description = DESCRIPTION1;
        wrapperProd.uom = uom;
        wrapperProd.category = PRODUCTFAMILY1;
        wrapperProd.itemSoldAs = ITEMSOLDAS1;
                
        EP_ProductSyncHandler.WrapperProducts wrapperProds = new EP_ProductSyncHandler.WrapperProducts();
        wrapperProds.item = new list<EP_ProductSyncHandler.WrapperProduct>{wrapperProd};   
        
        String requestBody = JSON.serialize(wrapperProds);   
        requestBody = ITEMS_JSON_NODE + requestBody+ EP_Common_Constant.RIGHT_CURLY_BRACE ; //'{"items":'+requestBody+'}';
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        res.statusCode = Integer.valueOf( EP_Common_Constant.CODE_200 );
        
        req.requestURI = SFDCBASEURL + PRODUCTSYNCENDPOINT;
        req.httpMethod = EP_Common_Constant.POST;
        req.requestBody = Blob.valueOf( requestBody );
        
        req.addHeader( EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON );
        RestContext.request = req;
        RestContext.response = res;
        
        test.startTest();
        EP_ProductSyncNAVtoSFDC.productSync(); 
        test.stopTest(); 
        
        String responseString = res.responseBody.toString();
        String uniqueKey = wrapperIdent.itemId + EP_Common_Constant.STRING_HYPHEN + wrapperIdent.clientId;
        list<Product2> newProductList = [Select Id, Name, EP_Company__c, Family, ProductCode, EP_Unit_of_Measure__c, Description from Product2 WHERE EP_NAV_Product_Company__c =: uniqueKey LIMIT 1 ];
        system.AssertEquals( newProductList.isEmpty(), TRUE );
        system.AssertEquals( responseString.containsIgnoreCase( EP_ProductSyncHandler.COMPANY_NOT_EXISTS.removeEnd(EP_Common_Constant.COMMA_STRING.trim() )), TRUE );  
    }
    
    
    /*
    Method to cover senario of product creation invailid UOM value from 
    REST RESOURCE class EP_ProductSyncNAVtoSFDC
    */
    static testMethod void createProductFail_InvalidUOMTest()  
    {
        //insert company
        setupCompanyData();
        
        EP_ProductSyncHandler.WrapperIdentifier wrapperIdent = new EP_ProductSyncHandler.WrapperIdentifier();
        wrapperIdent.itemId = PRODUCTCODE1;
        wrapperIdent.clientId = COMPANY_CODE1;
        
        EP_ProductSyncHandler.WrapperProduct wrapperProd = new EP_ProductSyncHandler.WrapperProduct();
        wrapperProd.seqId = SEQID1;
        wrapperProd.identifier = wrapperIdent;
        wrapperProd.name = PRODUCTNAME1;
        wrapperProd.description = DESCRIPTION1;
        wrapperProd.uom = NULL;
        wrapperProd.category = PRODUCTFAMILY1;
        wrapperProd.itemSoldAs = ITEMSOLDAS1;
        
        EP_ProductSyncHandler.WrapperProducts wrapperProds = new EP_ProductSyncHandler.WrapperProducts();
        wrapperProds.item = new list<EP_ProductSyncHandler.WrapperProduct>{wrapperProd};   
        
        String requestBody = JSON.serialize(wrapperProds);   
        requestBody = ITEMS_JSON_NODE + requestBody+ EP_Common_Constant.RIGHT_CURLY_BRACE ; //'{"items":'+requestBody+'}';
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        res.statusCode = Integer.valueOf( EP_Common_Constant.CODE_200 );
        
        req.requestURI = SFDCBASEURL + PRODUCTSYNCENDPOINT;
        req.httpMethod = EP_Common_Constant.POST;
        req.requestBody = Blob.valueOf( requestBody );
        
        req.addHeader( EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON );
        RestContext.request = req;
        RestContext.response = res;
        
        test.startTest();
        EP_ProductSyncNAVtoSFDC.productSync(); 
        test.stopTest();
        
        String responseString = res.responseBody.toString();
        String uniqueKey = wrapperIdent.itemId + EP_Common_Constant.STRING_HYPHEN + wrapperIdent.clientId;
        list<Product2> newProductList = [Select Id, Name, EP_Company__c, Family, ProductCode, EP_Unit_of_Measure__c, Description from Product2 WHERE EP_NAV_Product_Company__c =: uniqueKey LIMIT 1 ];
        system.AssertEquals( newProductList.isEmpty(), TRUE );
        //system.AssertEquals( responseString.containsIgnoreCase( UOM_ERROR), TRUE );  
    }
    
     /*
    Method to cover senario of product creation invailid JSON from 
    REST RESOURCE class EP_ProductSyncNAVtoSFDC
    */
    static testMethod void createProductFail_InvailidJSONTest()  
    {
        //insert company
        setupCompanyData();
        //insert Error Custom Setting
        
        EP_Error_Handler_CS__c setting = new EP_Error_Handler_CS__c();
        setting.Name = EP_Common_Constant.THRESHOLD;
        setting.Severity__c = EP_Common_Constant.THRESHOLD;
        setting.Value__c = 3;
        
        EP_Error_Handler_CS__c setting1 = new EP_Error_Handler_CS__c();
        setting1.Name = EP_Common_constant.ERROR;
        setting1.severity__c = EP_Common_constant.ERROR;
        setting1.value__c = 5;
        Database.insert(new list<EP_Error_Handler_CS__c>{setting,setting1},false);
        
        EP_ProductSyncHandler.WrapperIdentifier wrapperIdent = new EP_ProductSyncHandler.WrapperIdentifier();
        wrapperIdent.itemId = PRODUCTCODE1;
        wrapperIdent.clientId = COMPANY_CODE1;
        
        EP_ProductSyncHandler.WrapperProduct wrapperProd = new EP_ProductSyncHandler.WrapperProduct();
        wrapperProd.seqId = SEQID1;
        wrapperProd.identifier = wrapperIdent;
        wrapperProd.name = PRODUCTNAME1;
        wrapperProd.description = DESCRIPTION1;
        wrapperProd.uom = UOM_NEW;
        wrapperProd.category = PRODUCTFAMILY1;
        wrapperProd.itemSoldAs = ITEMSOLDAS1;
                
        //EP_ProductSyncHandler.WrapperProducts wrapperProds = new EP_ProductSyncHandler.WrapperProducts();
        //wrapperProds.item = new list<EP_ProductSyncHandler.WrapperProduct>{wrapperProd};   
        
        String requestBody = JSON.serialize(wrapperProd); //Invalid JSON, Valid is "JSON.serialize(wps)"  should be <wps>,a list, not one instance <wp>
        requestBody = ITEMS_JSON_NODE + requestBody+ EP_Common_Constant.RIGHT_CURLY_BRACE ; //'{"items":'+requestBody+'}';
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        res.statusCode = Integer.valueOf( EP_Common_Constant.CODE_200 ); 
        
        req.requestURI = SFDCBASEURL + PRODUCTSYNCENDPOINT;
        req.httpMethod = EP_Common_Constant.POST;
        req.requestBody = Blob.valueOf( requestBody );
        
        req.addHeader( EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON );
        RestContext.request = req;
        RestContext.response = res;
        
        test.startTest();
        EP_ProductSyncNAVtoSFDC.productSync(); 
        test.stopTest(); 
        
        String responseString = res.responseBody.toString();
        String uniqueKey = wrapperIdent.itemId + EP_Common_Constant.STRING_HYPHEN + wrapperIdent.clientId;
        list<Product2> newProductList = [Select Id, Name, EP_Company__c, Family, ProductCode, EP_Unit_of_Measure__c, Description from Product2 WHERE EP_NAV_Product_Company__c =: uniqueKey LIMIT 1 ];
        system.AssertEquals( newProductList.isEmpty(), TRUE );
        system.AssertEquals( responseString.containsIgnoreCase( EP_Common_Constant.FAILURE ), TRUE );  
    }

    //Code changes for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
    /**
    * @author kalpesh.j.thakur@accenture.com
    * @date 06/09/2017
    * @description test to check the Integration records are created for Inbound request
    */
    static testMethod void createUpdateVendor_Idempotent_Test() {
        String requestBody = '{"MSG":{"HeaderCommon":{"MsgID":"170904113532446-{E3288D3D-7858-430F-A368-1C0B28ADDF52}","InterfaceType":"NAV","SourceGroupCompany":"","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-uat02.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-uat02.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtrafloco2k321.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"false","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"Item Staging SF","CommunicationType":"Async"},"Payload":{"any0":{"items":{"item":[{"seqId":"{BA2D79BA-DA6F-4F47-878D-932425018014}","identifier":{"itemId":"2000004","clientId":"AAF"},"name":"test Inbound","description":null,"uom":"LT","category":"AVI","itemSoldAs":"Bulk","dgClassification":""}]}}},"StatusPayload":"StatusPayload"}}';

        Test.startTest();
        string old_Response = EP_ProductSyncHandler.createUpdateProduct(requestBody); 
        Test.stopTest();
        List<EP_IntegrationRecord__c> lstIntegrationRec = [SELECT Id FROM EP_IntegrationRecord__c];
        System.assert(lstIntegrationRec.size()==1);
    }

    //Code changes for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
    /**
    * @author kalpesh.j.thakur@accenture.com
    * @date 06/09/2017
    * @description test to check same response from integration records is sent second time without processing the response
    */
    static testMethod void createUpdateVendor_IdempotentSameResponse_Test() {
        String requestBody = '{"MSG":{"HeaderCommon":{"MsgID":"170904113532446-{E3288D3D-7858-430F-A368-1C0B28ADDF52}","InterfaceType":"NAV","SourceGroupCompany":"","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-uat02.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-uat02.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtrafloco2k321.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"false","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"Item Staging SF","CommunicationType":"Async"},"Payload":{"any0":{"items":{"item":[{"seqId":"{BA2D79BA-DA6F-4F47-878D-932425018014}","identifier":{"itemId":"2000004","clientId":"AAF"},"name":"test Inbound","description":null,"uom":"LT","category":"AVI","itemSoldAs":"Bulk","dgClassification":""}]}}},"StatusPayload":"StatusPayload"}}';

        Test.startTest();
        string old_Response = EP_ProductSyncHandler.createUpdateProduct(requestBody); 
        // calling second time will return the logged last response
        string new_Response = EP_ProductSyncHandler.createUpdateProduct(requestBody); 
        Test.stopTest();
        System.assertEquals(old_Response,new_Response);
    }

    /**
    * @author kalpesh.j.thakur@accenture.com
    * @date 06/09/2017
    * @description test to check exception handling by using wrong request
    */
    static testMethod void createUpdateVendor_IdempotentException_Test() {
        String requestBody = '{"MSG":{"HeaderCommon":{"MsgID":"170904113532446-{E3288D3D-7858-430F-A368-1C0B28ADDF52}","InterfaceType":"NAV","SourceGroupCompany":"","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-uat02.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-uat02.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtrafloco2k321.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"false","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"Item Staging SF","CommunicationType":"Async"},"StatusPayload":"StatusPayload"}';
        Test.startTest();
        string old_Response = EP_ProductSyncHandler.createUpdateProduct(requestBody); 
        Test.stopTest();
        List<EP_Exception_Log__c> lstExLog = [SELECT Id FROM EP_Exception_Log__c];
        System.assertEquals(lstExLog.size(),1);
     }
     //Code changes End for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
     
     //L4 #45514, #45515 code start
    /**
    * @author saurav.kumar.pal@accenture.com
    * @date 18/12/2017
    * @description test to check if Product is for update
    */
    Static testmethod void isProductUpdate_trueTest(){
        setupCompanyData();
        setupProductData();
        Product2 Prdtest = [select ID, EP_NAV_Product_Company__c from product2 limit 1 ];
        Map<String, Product2> mapExistingProductCompanyKeyExistingProduct2s = new Map<String, Product2>();
        mapExistingProductCompanyKeyExistingProduct2s.put(Prdtest.EP_NAV_Product_Company__c, Prdtest);
        test.startTest();
        boolean isProductUpdate = EP_ProductSyncHandler.isProductUpdate(mapExistingProductCompanyKeyExistingProduct2s, Prdtest.EP_NAV_Product_Company__c);
        test.stopTest(); 
        System.assertEquals(isProductUpdate,true);
    }
    /**
    * @author saurav.kumar.pal@accenture.com
    * @date 18/12/2017
    * @description test to check if Product is for create
    */
    Static testmethod void isProductUpdate_falseTest(){
        setupCompanyData();
        setupProductData();
        Product2 Prdtest = [select ID, EP_NAV_Product_Company__c from product2 limit 1 ];
        Map<String, Product2> mapExistingProductCompanyKeyExistingProduct2s = new Map<String, Product2>();
        test.startTest();
        boolean isProductUpdate = EP_ProductSyncHandler.isProductUpdate(mapExistingProductCompanyKeyExistingProduct2s, Prdtest.EP_NAV_Product_Company__c);
        test.stopTest(); 
        System.assertEquals(isProductUpdate,false); 
    } 
    /**
    * @author saurav.kumar.pal@accenture.com
    * @date 18/12/2017
    * @description test to check whether it will Return the Instance of Product Object based on Product Attributes
    */
    Static testmethod void getProductForCreate_Test(){ 
        Map<String, Product2> mapExistingProductCompanyKeyExistingProduct2s = new Map<String, Product2>();
        setupCompanyData();
        Company__c companyTest = [select ID, EP_Company_Code__c from Company__c limit 1 ];
        string uniqueKey = 'Test123';
        Map<String, Company__c> mapExistingCompanyCodeExistingCompanies = new Map<String, Company__c>();
        mapExistingCompanyCodeExistingCompanies.put(companyTest.EP_Company_Code__c, companyTest);
        EP_ProductSyncHandler.WrapperIdentifier wrapperIdent = new EP_ProductSyncHandler.WrapperIdentifier();
        wrapperIdent.clientId = companyTest.EP_Company_Code__c;
        test.startTest();
        product2 product = EP_ProductSyncHandler.getProduct(mapExistingProductCompanyKeyExistingProduct2s, uniqueKey , mapExistingCompanyCodeExistingCompanies , wrapperIdent); 
        test.stopTest();
        System.assertEquals(product.ID,NULL); 
        System.assertEquals(product.EP_NAV_Product_Company__c,uniqueKey);
        System.assertNotEquals(product.EP_Company_Lookup__c,NULL);
    }
    /**
    * @author saurav.kumar.pal@accenture.com
    * @date 18/12/2017
    * @description test to check whether it will Return the Instance of Product Object based on Product Attributes
    */
    Static testmethod void getProductForUpdate_Test(){ 
        setupCompanyData();
        setupProductData();
        Product2 Prdtest = [select ID, EP_NAV_Product_Company__c from product2 limit 1 ];
        Map<String, Product2> mapExistingProductCompanyKeyExistingProduct2s = new Map<String, Product2>();
        mapExistingProductCompanyKeyExistingProduct2s.put(Prdtest.EP_NAV_Product_Company__c, Prdtest);
        Map<String, Company__c> mapExistingCompanyCodeExistingCompanies = new Map<String, Company__c>();
        EP_ProductSyncHandler.WrapperIdentifier wrapperIdent = new EP_ProductSyncHandler.WrapperIdentifier();
        test.startTest();
        product2 product = EP_ProductSyncHandler.getProduct(mapExistingProductCompanyKeyExistingProduct2s, Prdtest.EP_NAV_Product_Company__c , mapExistingCompanyCodeExistingCompanies , wrapperIdent); 
        test.stopTest();
        System.assertNotEquals(product.ID,NULL);
    }
    /**
    * @author saurav.kumar.pal@accenture.com
    * @date 18/12/2017
    * @description test to check will it be use to set the product attributes from the JSON String
    */
    Static testmethod void setProductAttributes_Test(){ 
        Product2 product = new Product2(); 
        EP_ProductSyncHandler.WrapperIdentifier wrapperIdent = new EP_ProductSyncHandler.WrapperIdentifier();
        wrapperIdent.itemId = '1234';
        EP_ProductSyncHandler.WrapperProduct wrapperProd = new EP_ProductSyncHandler.WrapperProduct();
        wrapperProd.seqId = SEQID1;
        wrapperProd.identifier = wrapperIdent;
        wrapperProd.name = PRODUCTNAME1;
        wrapperProd.description = DESCRIPTION1;
        Schema.DescribeFieldResult fieldResult = Product2.EP_Unit_of_Measure__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        if( !ple.isEmpty() )
        {
            uom = ple[0].getValue();
        }
        else
        {
            uom = EP_Common_Constant.UOM_STRING;
        }
        wrapperProd.uom = uom;
        wrapperProd.category = PRODUCTFAMILY1;
        wrapperProd.itemSoldAs = ITEMSOLDAS1;
        wrapperProd.block = 'No';
        wrapperProd.enterpriseId = '123456';
        wrapperProd.isClean = 'Yes';
        test.startTest();
        EP_ProductSyncHandler.setProductAttributes(wrapperProd, product, wrapperProd.identifier);
        test.stopTest();
        System.assertNotEquals(wrapperProd.name,NULL);
    }
    //L4 #45514, #45515 code end
}