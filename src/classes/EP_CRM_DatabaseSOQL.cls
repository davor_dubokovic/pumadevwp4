/* ================================================
 * @Class Name : EP_CRM_DatabaseSOQL 
 * @author : TCS
 * @Purpose: This class is used to store all SOQL queries related to opportunity volume report.
 * @created date: 15/11/2017
 ================================================*/
public with sharing class EP_CRM_DatabaseSOQL {
    /**
    * This method is used to return list of opportunities based on logged in users and filters provided
    */
    public static List < Opportunity > getOpportunities(EP_CRM_PipelineReportBean bean) {
        Integer year = bean.reportDateValue.year();
        // Date currentDate = bean.reportDateValue; //  AND EP_CRM_Contract_Expiry_Date__c >= :currentDate
        String query = 'SELECT Id, Name, OwnerId, Owner.Name, createdDate, closeDate, EP_CRM_Customer_Segment__c, EP_CRM_Geo1__c, EP_CRM_Geo2__c, EP_CRM_Geo3__c, EP_CRM_Geo4__c, ' +
            'EP_CRM_UoM__c, EP_CRM_Volume__c, Expected_Monthly_Volume__c, StageName, isWon, EP_CRM_Primary_Product_Interest__c, ' + 
            '(SELECT Id, Name, StartDate, EndDate, EP_CRM_Contract_Expiry_Date__c FROM Contracts__r WHERE StatusCode IN (\'Activated\')) ' + 
            'FROM Opportunity WHERE CALENDAR_YEAR(CloseDate) = :year AND StageName != \'Closed Lost\'';
        
        if(bean.getManagerUser() || bean.getExecutiveManagerUser()) {
            Map < Id, User > childUsers = EP_CRM_ReportUtility.getChildRoleUsers(bean.getRoleId(), bean.getExecutiveManagerUser()?true:false);
            Set < Id > userIds = new Set < Id > ();
            userIds.add(UserInfo.getUserId());
            if(childUsers != null) {
                userIds.addAll(childUsers.keySet());                
            }
            query = query + ' AND OwnerId IN :userIds';            
        }
            
        if(bean.opportunityObj != null) {
            if(String.isNotBlank(bean.opportunityObj.EP_CRM_Geo1__c)) {
                query = query + ' AND EP_CRM_Geo1__c = \'' + bean.opportunityObj.EP_CRM_Geo1__c + '\''; 
            }
            if(bean.getGlobalUser() == false) {
                if(String.isNotBlank(bean.opportunityObj.EP_CRM_Geo2__c)) {
                    query = query + ' AND EP_CRM_Geo2__c = \'' + bean.opportunityObj.EP_CRM_Geo2__c + '\''; 
                }   
                if(bean.getRegionalExecutiveUser() == false) {
                    if(String.isNotBlank(bean.opportunityObj.EP_CRM_Geo3__c)) {
                        query = query + ' AND EP_CRM_Geo3__c = \'' + bean.opportunityObj.EP_CRM_Geo3__c + '\''; 
                    }
                    if(bean.getCountryExecutiveUser() == false) {
                        if(String.isNotBlank(bean.opportunityObj.EP_CRM_Geo4__c)) {
                            query = query + ' AND EP_CRM_Geo4__c = \'' + bean.opportunityObj.EP_CRM_Geo4__c + '\''; 
                        }
                    }
                }
            }
            if(String.isNotBlank(bean.opportunityObj.EP_CRM_Customer_Segment__c)) {
                query = query + ' AND EP_CRM_Customer_Segment__c = \'' + bean.opportunityObj.EP_CRM_Customer_Segment__c + '\''; 
            }
            if(String.isNotBlank(bean.opportunityObj.EP_CRM_Product_Interest_Group__c)) {
                query = query + ' AND EP_CRM_Primary_Product_Interest__c IN (' + EP_CRM_ReportUtility.getMultipleSelectedValues(bean.opportunityObj.EP_CRM_Product_Interest_Group__c) + ')';
            }
        }
        query = query + ' ORDER BY EP_CRM_Geo1__c, EP_CRM_Geo2__c, EP_CRM_Geo3__c, EP_CRM_Geo4__c';
        System.debug('### query : ' + query);
        List < Opportunity > opportunityList = null;
        try {
            opportunityList = (List<Opportunity>) Database.query(query);
        } catch(Exception ex) {
            System.debug('### Exception raised in DatabaseSOQL.getOpportunies = ' + ex.getMessage());
        }
        return opportunityList;
    }
    
    /**
    * This method is used to get logged in user information 
    */
    public static User getUserDetails(){
        return [SELECT Id, Name, UserRoleId, UserRole.Name, EP_CRM_Geo1__c, EP_CRM_Geo2__c FROM User WHERE Id =: UserInfo.getUserId()]; 
    }     
}