/* ================================================
 * @Class Name : EP_CRM_PipelineReportWrapper 
 * @author : TCS
 * @Purpose: This wrapper class is used to report information for opportunity volume.
 * @created date: 15/11/2017
 ================================================*/
public with sharing class EP_CRM_PipelineReportWrapper {
    public String geo1 { get; set; }
    public String geo2 { get; set; }
    public String geo3 { get; set; }
    public String geo4 { get; set; }
    public String segment { get; set; }
    public String product { get; set; }
    public String childUserName { get; set; }
    public Opportunity opportunityObj { get; set; }
    
    // Returns open opportunities and its amount (Integer - Month, Decimal - Amount)
    public Map < Integer, Decimal > openOpportunityMap { get {
    	if(openOpportunityMap == null) {
    		openOpportunityMap = new Map < Integer, Decimal > (); 
    	}
    	return openOpportunityMap;
    } set; }
    
    // Returns closed won opportunities and its amount (Integer - Month, Decimal - Amount)
    public Map < Integer, Decimal > closedOpportunityMap { get {
    	if(closedOpportunityMap == null) {
    		closedOpportunityMap = new Map < Integer, Decimal > (); 
    	}
    	return closedOpportunityMap;
    } set; }
    
    // Returns list of opporutunities name
    public List < String > opportunityNameList { get {
    	if(opportunityNameList == null) {
    		opportunityNameList = new List < String > ();
    	}
    	return opportunityNameList;
    } set; }
    
    // Returns opportunity name
    public String getOpportunityName() {
    	return String.Join(opportunityNameList, '<br/>');
    }
    
    // Returns total open opportunities amount
    public Decimal remainingForecastAmount { get {
    	return remainingForecastAmount==null?0:remainingForecastAmount;
    } set; } 
    
    // Returns total closed won opportunities amount
    public Decimal closedWonAmount { get {
    	return closedWonAmount==null?0:closedWonAmount;
    } set; }
    
    // Returns total full year amount (closedWon + open) Opportunity
    public Decimal getFullYearAmount() {
    	return (remainingForecastAmount + closedWonAmount);
    }
    
    // Default Constructor
    public EP_CRM_PipelineReportWrapper() {
    	
    }
    
    // Parameterized Constructor
    public EP_CRM_PipelineReportWrapper(Map < Integer, Decimal > openOpportunityMap, Map < Integer, Decimal > closedOpportunityMap) {
    	this.openOpportunityMap = openOpportunityMap;
    	this.closedOpportunityMap = closedOpportunityMap;
    }
}