/*
   @Author 			CS
   @name 			EP_RetrospectiveOrdUpdateStub
   @CreateDate 		27/04/2018
   @Description		Retrospective Order Update stub
   @Version 		1.0
*/
public class  EP_RetrospectiveOrdUpdateStub {
    /* Class for MSG */
    public MSG msg;
    
    /* MSG stub class*/ 
    public class MSG {
        public HeaderCommon headerCommon;
        public Payload payload;
        public String statusPayload;
    }
    
    /* HeaderCommon stub class*/    
    public class HeaderCommon extends EP_MessageHeader {}
    
    /* Payload stub class*/
    public class Payload {
        public Any0 any0;
    }
    
    /* Any0 stub class*/
    public class Any0 {
        public SalesContracts salesContracts { get; set; }
    }
    
    /* SalesContracts stub class*/
    public class SalesContracts {
        
        public List<SalesContract> salesContract { get; set; }
    }
    
    /* SalesContract stub class*/
    public class SalesContract {
        
        // public string seqId { get; set; }
        public Identifier identifier { get; set; }
        // public string versionNr { get; set; }
        public Lines lines { get; set; }
    }

    /* Lines stub class*/
    public class Lines {
        
        public List<Line> line { get; set; }
    }
    
    /* Line stub class*/
    public class Line {
        
        // public string seqId { get; set; }
        // public Identifier identifier { get; set; }
        // public string itemId { get; set; }
        public Decimal consumedQty { get; set; }
    }

    /* Identifier stub class*/
    public class Identifier {
        
        //public string companyCode { get; set; }
        public string contractNr { get; set; }
        //public string entrprsId { get; set; }
        // public string lineId { get; set; }
    }
}