@isTest
public class EP_CountryMapper_UT{
private static String COUNTRY_NAME = 'Australia';
private static String COUNTRY_REGION = 'Australia';
private static String COUNTRY_CODE = 'AU';

static testMethod void getRecordsByIds_test() {
    EP_CountryMapper localObj = new EP_CountryMapper();
    EP_Country__c  country  = EP_TestDataUtility.createCountryRecord(COUNTRY_NAME ,COUNTRY_CODE ,COUNTRY_REGION);
    Database.insert(country);
    Set<id> countryIds = new Set<Id>();
    countryIds.add(country.id);
    Test.startTest();
    List<EP_Country__c> result = localObj.getRecordsByIds(countryIds);
    Test.stopTest();
    System.AssertEquals(true, result.size()>0);
}
static testMethod void getRecordsByNames_test(){
    EP_CountryMapper localObj = new EP_CountryMapper();
    EP_Country__c  country  = EP_TestDataUtility.createCountryRecord(COUNTRY_NAME ,COUNTRY_CODE ,COUNTRY_REGION);
    Database.insert(country);
    List<String> countryNameList = new List<String>();
    countryNameList.add(country.name);
    Test.startTest();
    list<EP_Country__c> result = localObj.getRecordsByNames(countryNameList);
    Test.stopTest();
    System.AssertEquals(true, result.size()>0);
}
static testMethod void getCountryById_test(){
    EP_CountryMapper localObj = new EP_CountryMapper();
    EP_Country__c  country  = EP_TestDataUtility.createCountryRecord(COUNTRY_NAME ,COUNTRY_CODE ,COUNTRY_REGION);
    Database.insert(country);
    Test.startTest();
    EP_Country__c result = localObj.getCountryById(country.id);
    Test.stopTest();
    System.AssertEquals(true, result != null);
    System.AssertEquals(true, country.name == result.name);
}
static testMethod void getCountryCodesMapByCodes_Test(){
    EP_CountryMapper localObj = new EP_CountryMapper();
    EP_Country__c  country  = EP_TestDataUtility.createCountryRecord(COUNTRY_NAME ,COUNTRY_CODE ,COUNTRY_REGION);
    Database.insert(country);
    Set<String> countryCodeSet = new Set<String>();
    countryCodeSet.add(country.EP_Country_Code__c);
    Test.startTest();
    Map<String,Id> result = localObj.getCountryCodesMapByCodes(countryCodeSet);
    Test.stopTest();
    System.AssertEquals(true, result.size() > 0);    
}
}