/**
  * @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
  * @name        : EP_Current
  * @CreateDate  : 04/02/2017
  * @Description : This is Class Contains logic for Current Orders 
  * @Version     : <1.0>
  * @reference   : N/A
  */
  public class EP_Current extends EP_OrderEpoch {
    
    public EP_Current() {
    }

    /** This method is returns NonVMI ShipTos for Current Order
     *  @date      15/02/2017
     *  @name      getShipTos
     *  @param     List<Account> lstAccount
     *  @return    List<Account>
     *  @throws    NA
     */
     public override List<Account> getShipTos(List<Account> lstAccount,EP_ProductSoldAs productSoldAsType) {
        EP_GeneralUtility.Log('Public','EP_Current','getShipTos');
        List<Account> lstShipTos = new List<Account>();
        for (Account objAccount : lstAccount) {
            if (objAccount.RecordType.DeveloperName == EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME) {
                lstShipTos.add(objAccount);
            }
        }
        return lstShipTos;
    }
    
    
    /** This method returns the Id of order record type
       *  @date      15/02/2017
       *  @name      findRecordType
       *  @param     EP_VendorManagement vmType,EP_ConsignmentType consignmentType
       *  @return    Id
       *  @throws    NA
       */
       public override Id findRecordType(EP_VendorManagement vmType,EP_ConsignmentType consignmentType) {
        EP_GeneralUtility.Log('Public','EP_Current','findRecordType');
        return consignmentType.findRecordType(vmType);
    }
    
    
    /** This method is used to set the Requested Date Time field on Order
     *  @date      15/02/2017
     *  @name      setRequestedDateTime
     *  @param     Order orderObj,String selectedDate,String availableSlots
     *  @return    Order 
     *  @throws    NA
     */
     public override csord__Order__c setRequestedDateTime(csord__Order__c orderObj,String selectedDate,String availableSlots) {
        EP_GeneralUtility.Log('Public','EP_Current','setRequestedDateTime');
        orderObj.EP_Requested_Delivery_Date__c = EP_GeneralUtility.convertSelectedDateToDateInstance(selectedDate);
        orderObj.EP_Requested_Pickup_Time__c = String.valueOf(EP_GeneralUtility.convertSelectedHourToTime(availableSlots)).subString(0,8);
        return orderObj;
    }
}