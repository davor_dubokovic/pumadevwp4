/* 
   @Author 			Accenture
   @name 			EP_TankDipExportHandler
   @CreateDate 		03/10/2017
   @Description		Stub class for tank dip inbound interface which is used to send data to external system
   @Version 		1.0
*/ 
public class EP_TankDipExportHandler extends EP_InboundHandler {
	/**
	* @author 			Accenture
	* @name				getTankDips
	* @date 			03/13/2017
	* @description 		Method used to get the tank dips in JSON format which are not exported and create integration records for same.
	* @param 			NA
	* @return 			string
	*/
    public override string processRequest(){
        EP_GeneralUtility.Log('Public','EP_TankDipExportHandler','processGetRequest');
        List<EP_Tank_Dip__c> tankDipRecords = new List<EP_Tank_Dip__c>();
        string messageId ='';
        string jsonResponse='';
        try {
        	EP_TankDipMapper tankDipMapper = new EP_TankDipMapper();
            tankDipRecords = tankDipMapper.getTankDipsToExport();
            if(!tankDipRecords.isEmpty()){
            	updateTankDipRecords(tankDipRecords);
            	EP_TankDipExportStub tankDipMessage = EP_TankDipExportHelper.setTankDipAttributes(tankDipRecords); 
	            messageId = EP_IntegrationUtil.getMessageId(EP_Common_Constant.strSource,EP_Common_Constant.strLocation,EP_Common_Constant.dip,System.Now(),EP_GeneralUtility.createMessageGeneratorRecord());
	            tankDipMessage.MSG.HeaderCommon.MsgID =messageId; 
	            tankDipMessage.MSG.HeaderCommon.SourceCompany =tankDipRecords[0].EP_Tank__r.EP_Ship_To__r.EP_Puma_Company_Code__c;
	            jsonResponse = JSON.serialize(tankDipMessage);
	            createIntegrationRecords(tankDipRecords,false,EP_Common_Constant.BLANK,messageId);
            }
        } catch(Exception ex){
            createIntegrationRecords(tankDipRecords,true,ex.getMessage(),messageId);
            jsonResponse =EP_Common_Constant.TANK_DIP_FAILURE_RESPONSE;
        }
        return jsonResponse;                                              
    }
    
    /**
	* @author 			Accenture
	* @name				updateTankDipRecords
	* @date 			03/13/2017
	* @description 		updates the tank dip records status to Sent
	* @param 			List<EP_Tank_Dip__c>
	* @return 			NA
	*/
	@TestVisible
    private static void updateTankDipRecords(List<EP_Tank_Dip__c> tankDipRecords){
    	for(EP_Tank_Dip__c tankDip : tankDipRecords){
            tankDip.EP_Tank_Dip_Exported__c = true;
            tankDip.EP_Integration_Status__c = EP_Common_Constant.SENT_STATUS;
        }
        database.update(tankDipRecords);
    }
    
    /**
	* @author 			Accenture
	* @name				createIntegrationRecords
	* @date 			03/13/2017
	* @description 		Creates the integration records for all the tank dip records which are being exported
	* @param 			List<EP_Tank_Dip__c>, boolean,string,Map<String,String>,string
	* @return 			NA
	*/
	@TestVisible
    private static void createIntegrationRecords(List<EP_Tank_Dip__c> tankDipRecords, boolean transactionFailed, string failureReason,string messageId){
        EP_GeneralUtility.Log('Public','EP_TankDipExportHandler','createIntegrationRecords');
        List<Id> listObjectIds = new list<Id>();
        Map<String,String> parentMap = new Map<String,String>();
        List<String> listObjectType = new List<String>();
        if(!tankDipRecords.isEmpty()){
        	Map<string,string> mapOfAPINameAndName = getObjectNameMap();
            for(EP_Tank_Dip__c tankDip : tankDipRecords){
            	listObjectIds.add(tankDip.Id);
            	listObjectType.add(mapOfAPINameAndName.get(tankDip.getSObjectType().getDescribe().getName().toUpperCase()));
            	parentMap.put(tankDip.id,tankDip.EP_Tank__c); 
            }
            if(!transactionFailed){
                EP_IntegrationUtil.sendBulkOk(listObjectIds,listObjectType,EP_Common_Constant.BLANK,messageId,EP_Common_Constant.WINDMS_SYNC,System.now(),EP_Common_Constant.WINDMS_SYNC,parentMap);
            } else{
                EP_IntegrationUtil.sendBulkError(listObjectIds,listObjectType,EP_Common_Constant.BLANK,messageId,EP_Common_Constant.WINDMS_SYNC,EP_Common_Constant.WINDMS_SYNC,failureReason,System.now(),EP_Common_Constant.BLANK,EP_Common_Constant.BLANK);
            }
        }
    }
    
    /**
	* @author 			Accenture
	* @name				getObjectNameMap
	* @date 			03/13/2017
	* @description 		gets the map of API name and name of objects
	* @param 			NA
	* @return 			Map<string,string>
	*/
	@TestVisible
    private static Map<string,string> getObjectNameMap(){
    	Map<string,string> mapOfAPINameAndName = new Map<String,String>();
    	for(EP_Integration_Status_Update__c integrationCS : EP_Integration_Status_Update__c.getAll().values()){
            mapOfAPINameAndName.put(integrationCS.EP_API_Name__c.toUpperCase(),integrationCS.Name);
    	}
    	return mapOfAPINameAndName;
    }
    
    //******************************************************** TANK DIP POST STARTS*******************************************************************************************
	
	/**
	* @author 			Accenture
	* @name				processRequest
	* @date 			03/13/2017
	* @description 		processes the request and updates the integration records
	* @param 			string
	* @return 			string
	*/
    public override string processRequest(string jsonRequest){
    	string response='';
    	try {
    		EP_AcknowledgementStub acknowledgement = (EP_AcknowledgementStub) System.JSON.deserialize(jsonRequest, EP_AcknowledgementStub.class); 
    		string correlationId = acknowledgement.MSG.HeaderCommon.CorrelationID;
			List<EP_AcknowledgementStub.dataset> datasets = acknowledgement.MSG.Payload.Any0.acknowledgement.dataSets.dataset;
			EP_AcknowledgementStub.dataset dataset = new EP_AcknowledgementStub.dataset();
			set<string> setTankDipIds = new set<string>();
			if(datasets!=null){
				dataset = datasets[0];
			}
			updateIntegrationRecords(correlationId,dataset,setTankDipIds);
			updateTankDipRecords(setTankDipIds,dataset);
			response = EP_Common_Constant.SUCCESS;
		}catch(Exception ex){
			response = EP_Common_Constant.EXCEPTION_Response;
		}
		return response;
	}
	@TestVisible
	private static void updateIntegrationRecords(string correlationId,EP_AcknowledgementStub.dataset dataset,set<string> setTankDipIds){
		List<EP_IntegrationRecord__c> integrationRecordsToUpdate = new List<EP_IntegrationRecord__c>();
		EP_IntegrationRecordMapper intMapper = new EP_IntegrationRecordMapper();
		integrationRecordsToUpdate = intMapper.getRecordsByMessageIdOrSeqId(correlationId,new set<string>());
		for(EP_IntegrationRecord__c intRecord : integrationRecordsToUpdate){
			setTankDipIds.add(intRecord.EP_Object_ID__c);
			if(string.isNotBlank(dataset.errorDescription)){
				intRecord.EP_Status__c = EP_Common_Constant.ERROR_SYNC_STATUS;
				intRecord.EP_Error_Description__c = dataset.errorDescription;
			} else {
				intRecord.EP_Status__c = EP_Common_constant.SYNC_STATUS;
			}
		}
		Database.update(integrationRecordsToUpdate);
	}
	@TestVisible
	private static void updateTankDipRecords(set<string> setTankDipIds,EP_AcknowledgementStub.dataset dataset){
		List<EP_Tank_Dip__c> tankDipsToUpdate = new List<EP_Tank_Dip__c>();
		EP_TankDipMapper tankDipMapper = new EP_TankDipMapper();
		tankDipsToUpdate = tankDipMapper.getTankDipsByIds(setTankDipIds);
		for(EP_Tank_Dip__c tankDip : tankDipsToUpdate){
			if(string.isNotBlank(dataset.errorDescription)){
				tankDip.EP_Integration_Status__c = EP_Common_Constant.ERROR_SYNC_STATUS;
				tankDip.EP_Tank_Dip_Exported__c = false;
			} else { 
				tankDip.EP_Integration_Status__c = EP_Common_constant.SYNC_STATUS;
			}
		}
		Database.update(tankDipsToUpdate);
	}
}