/* 
@Author      Accenture
@name        EP_CreditExceptionPayloadXML
@CreateDate  02/12/2017
@Description This class will be used to Generate Payload
@Version     1.0
*/
public with sharing class EP_CreditExceptionPayloadXML extends EP_GenerateOrderRequestXML{

    @testvisible EP_Credit_Exception_Request__c credExpObj;
    final String strDateFormat = 'yyyyMMdd';
    final String strTimeFormat = 'HHmmss.SSS';
    
    /**
    * @author           Accenture
    * @name             createXML
    * @date             04/18/2017
    * @description      This method is used to create XML for Order to Sync with NAV
    * @param            NA
    * @return           NA
    */
    public override string createXML(){
        EP_GeneralUtility.Log('Public','EP_CreditExceptionPayloadXML','createXML');
        return super.createXML();
    }

    /**
    * @author           Accenture
    * @name             createXML
    * @date             04/18/2017
    * @description      This method is used to create XML for Order to Sync with NAV
    * @param            NA
    * @return           NA
    */
    public override void init(){
        EP_GeneralUtility.Log('Public','EP_CreditExceptionPayloadXML','init');
        MSGNode = doc.createRootElement(EP_OrderConstant.MSG, null, null);
        //45435 Perform Credit Exception Review --Start  
        EP_CreditExceptionRequestMapper creditExcepMapper = new EP_CreditExceptionRequestMapper(); 
        credExpObj = creditExcepMapper.getRecord(recordId);
        //45435 Perform Credit Exception Review --End
        // Get relevant custom setting
        isEncryptionEnabled = false;
        EP_CS_OutboundMessageSetting__c outboundMsgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(messageType);
        if(outboundMsgSetting != null){
            isEncryptionEnabled = outboundMsgSetting.Payload_Encoded__c;
        }
    }
    
    /**
    * @author           Accenture
    * @name             createPayload
    * @date             04/18/2017
    * @description      This method is used to create Payload for Order to Sync with NAV
    * @param            NA
    * @return           NA
    */    
    public override void createPayload(){
        EP_GeneralUtility.Log('Public','EP_CreditExceptionPayloadXML','createXML');
        //45435 Perform Credit Exception Review --Start 
        String Status = credExpObj.EP_Status__c; 
        if (credExpObj.EP_Status__c == null) {
            Status = EP_Common_Constant.CREDIT_EXC_STATUS_NEW;
        }
        //45435 Perform Credit Exception Review --End
        
        DOM.Document doctemp = new DOM.Document();
        Dom.XMLNode creditExceptionRequestsNode = doctemp.createRootElement(EP_OrderConstant.creditExceptionRequests,null,null);
        Dom.XMLNode creditExceptionRequestNode = creditExceptionRequestsNode.addChildElement(EP_OrderConstant.creditExceptionRequest,null,null);
        
        //Repeat this for every Child node
        String seqid = EP_IntegrationUtil.reCreateSeqId(this.messageId, credExpObj.Id);
        creditExceptionRequestNode.addChildElement(EP_OrderConstant.seqId,null,null).addTextNode(getValueforNode(seqid)); //Value for seqId
        
        Dom.XMLNode identifierNode = creditExceptionRequestNode.addChildElement(EP_OrderConstant.identifier_windmsxml,null,null);
            identifierNode.addChildElement(EP_OrderConstant.exceptionNo,null,null).addTextNode(getValueforNode(credExpObj.Name)); 
            identifierNode.addChildElement(EP_OrderConstant.billTo,null,null).addTextNode(getValueforNode(credExpObj.EP_Bill_To_Account_No__c)); 
            identifierNode.addChildElement(EP_OrderConstant.clientId,null,null).addTextNode(getValueforNode(credExpObj.EP_Company_Code__c)); 
        
        //45435 Perform Credit Exception Review --Start    
        creditExceptionRequestNode.addChildElement(EP_OrderConstant.createdDate,null,null).addTextNode(formatDateAsString(credExpObj.CreatedDate));
        creditExceptionRequestNode.addChildElement(EP_OrderConstant.currencyId,null,null).addTextNode(getValueforNode(credExpObj.CurrencyIsoCode));
        creditExceptionRequestNode.addChildElement(EP_OrderConstant.orderId,null,null).addTextNode(getValueforNode(credExpObj.EP_CS_Order__r.OrderNumber__c));
        creditExceptionRequestNode.addChildElement(EP_OrderConstant.orderValue,null,null).addTextNode(getValueforNode(credExpObj.EP_CS_Order__r.TotalAmount__c));
        //L4-#45424,#45426,#45431 code changes Start
        creditExceptionRequestNode.addChildElement(EP_OrderConstant.entityType,null,null).addTextNode(getValueforNode(credExpObj.EP_Entity_Type__c));
        //L4-#45424,#45426,#45431 code changes End
        //Defect Fix 83665 
        creditExceptionRequestNode.addChildElement(EP_OrderConstant.orderPymntType,null,null).addTextNode(getValueforNode(credExpObj.EP_CS_Order__r.EP_Payment_Method_Name__c));
    //Defect Fix 83665
        creditExceptionRequestNode.addChildElement(EP_OrderConstant.orderEpoch,null,null).addTextNode(getValueforNode(credExpObj.EP_CS_Order__r.EP_Order_Epoch__c));
        creditExceptionRequestNode.addChildElement(EP_OrderConstant.comment,null,null).addTextNode(getValueforNode(credExpObj.EP_Comment__c));
        creditExceptionRequestNode.addChildElement(EP_OrderConstant.loadingDt,null,null).addTextNode(formatDateAsString(credExpObj.EP_CS_Order__r.EP_Loading_Date__c));
        creditExceptionRequestNode.addChildElement(EP_OrderConstant.deliveryDt,null,null).addTextNode(formatDateAsString(credExpObj.EP_CS_Order__r.EP_Expected_Delivery_Date__c));

        if(credExpObj.EP_CS_Order__r.RecordType.DeveloperName == 'Order'){
            creditExceptionRequestNode.addChildElement(EP_OrderConstant.recordType,null,null).addTextNode(getValueforNode(EP_OrderConstant.OrderTy));
        } else {
            creditExceptionRequestNode.addChildElement(EP_OrderConstant.recordType,null,null).addTextNode(getValueforNode(EP_OrderConstant.salesOrderTy));
        }
        creditExceptionRequestNode.addChildElement(EP_OrderConstant.senderId,null,null).addTextNode(getValueforNode(credExpObj.EP_SenderId__c));
        creditExceptionRequestNode.addChildElement(EP_OrderConstant.salesPrsnCd,null,null).addTextNode(getValueforNode(credExpObj.EP_Sales_Person_Code__c));
        creditExceptionRequestNode.addChildElement(EP_OrderConstant.requestedOn,null,null).addTextNode(formatDateAsString(credExpObj.EP_Request_Date__c));
        creditExceptionRequestNode.addChildElement(EP_OrderConstant.requestStatus,null,null).addTextNode(getValueforNode(Status));
        
        creditExceptionRequestNode.addChildElement(EP_OrderConstant.versionNr,null,null).addTextNode(formatVersionNr(credExpObj.CreatedDate));
        //45435 Perform Credit Exception Review --End

        Dom.XMLNode PayloadNode = MSGNode.addChildElement(EP_OrderConstant.Payload,null,null);
        dom.XmlNode anyNode = PayloadNode.addChildElement(EP_OrderConstant.any0,null, null);   
        // Encoding payload by calling encode XML method in superclass
        anyNode.addTextNode(encodeXML(doctemp));    
    }

    /**
    * @author           Accenture
    * @name             createStatusPayLoad
    * @date             04/18/2017
    * @description      This method is used to create Status XML tags
    * @param            NA
    * @return           NA
    */ 
    public override void createStatusPayLoad(){
        EP_GeneralUtility.Log('Public','EP_CreditExceptionPayloadXML','createStatusPayLoad');
        MSGNode.addChildElement(EP_OrderConstant.STATUS_PAYLOAD,null,null);
    }


    //45435 Perform Credit Exception Review --Start 
    /**
    * @author           Accenture
    * @name             formatVersionNr
    * @date             21/12/2017
    * @description      This method is used to get the VersionNr 
    * @param            NA
    * @return           NA
    */ 
    public String formatVersionNr(Datetime dt){
        
        EP_GeneralUtility.Log('Public','EP_CreditExceptionPayloadXML','formatVersionNr');

        String formatDate = dt.format(strDateFormat);
        String formatTime = dt.format(strTimeFormat);
        String str_T = 'T';
        return formatDate+str_T+formatTime;
    }
    //45435 Perform Credit Exception Review --End
}