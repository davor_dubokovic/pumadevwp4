global  with sharing class CustomLookupShipToAccount extends cscfga.ALookupSearch {

  public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit) {
	    String accountId = searchFields.get('AccountId');
	    String pType = searchFields.get('Product Type');
	    system.debug('map'+accountId);
	    system.debug('accountId'+searchFields);
	    system.debug('pType'+pType);
	    List <Account> accList = new List<Account>();
	    List<String> nonBulk = new List<String> {'Non-VMI Ship To','VMI Ship To'};
	    if(pType!=null) {
    	    if(pType.equalsIgnoreCase('Bulk')) {
    	        accList= [SELECT id,name,ShippingPostalCode,ParentId,EP_Puma_Company__c,AccountNumber,RouteAllocationCount__c,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c 
    	                  FROM Account 
    	                  WHERE parentid =: accountId and recordtype.name ='Non-VMI Ship To' and EP_Status__c = '05-Active'];
    	    }
            else {
    	        accList= [SELECT id,name,RouteAllocationCount__c,ParentId,EP_Puma_Company__c,AccountNumber,ShippingPostalCode,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c 
    	                  FROM Account 
    	                  WHERE parentid =: accountId and recordtype.name in:nonBulk and EP_Status__c = '05-Active' and EP_Ship_To_Type__c != 'Consignment'];
            }
	    }
        if(accList == null && !accList.isEmpty()){
            return null;
        }

        return accList;
    }  
    global override List<Object> doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionId) {
	    String accountId = searchFields.get('AccountId');
	    String pType = searchFields.get('Product Type');
	    system.debug('map'+accountId);
	    system.debug('accountId'+searchFields);
	    system.debug('pType'+pType);
	    List <Account> accList = new List<Account>();
	    List<String> nonBulk = new List<String> {'Non-VMI Ship To','VMI Ship To'};
	    if(pType!=null) {
    	    if(pType.equalsIgnoreCase('Bulk')) {
    	        accList= [SELECT id,name,ShippingPostalCode,ParentId,EP_Puma_Company__c,AccountNumber,RouteAllocationCount__c,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c 
    	                  FROM Account 
    	                  WHERE parentid =: accountId and recordtype.name ='Non-VMI Ship To' and EP_Status__c = '05-Active'];
    	    }
            else {
    	        accList= [SELECT id,name,RouteAllocationCount__c,ParentId,EP_Puma_Company__c,AccountNumber,ShippingPostalCode,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c 
    	                  FROM Account 
    	                  WHERE parentid =: accountId and recordtype.name in:nonBulk and EP_Status__c = '05-Active' and EP_Ship_To_Type__c != 'Consignment'];
            }
	    }
        if(accList == null && !accList.isEmpty()){
            return null;
        }
        return accList;
    }
	global override String getRequiredAttributes() {
    	return '["AccountId","Product Type"]';
  	}    
}