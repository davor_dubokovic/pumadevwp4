/* 
@Author <Jyotsna Tiwari>
@name <EP_FE_SubmitOrderEndpoint>
@CreateDate <25/04/2016>
@Description <This class updates the existing Order if the status of Order is "Draft"  >  
@Version <1.0>
*/

/*
@RestResource(urlMapping = '/FE/V1/order/submit/*')
*/
/*********************************************************************************************
     @Author <>
     @name <EP_FE_SubmitOrderEndpoint >
     @CreateDate <>
     @Description < >  
     @Version <1.0>
    *********************************************************************************************/
    
global with sharing class EP_FE_SubmitOrderEndpoint {
  /*  
    // global static EP_OrderPageContext ctx {get;set;}

    /*
    *  Default Constructor // Commenting the below constructor to remove the isssue from Nova Suite.
    */
    /*public EP_FE_SubmitOrderEndpoint() {

    }*/ 
/*
    @HttpPost
    */
/*********************************************************************************************
     @Author <>
     @name <updateOrder>
     @CreateDate <>
     @Description < >  
     @Version <1.0>
    *********************************************************************************************/
    /*
    global static EP_FE_OrderSubmissionResponse updateOrder(EP_FE_OrderSubmissionRequest request) {
        
        EP_FE_OrderSubmissionResponse response = new EP_FE_OrderSubmissionResponse();

        // Check any network issue / malformed request
        if (request == null || EP_FE_Utils.isUserProfileAccessEquals(EP_FE_Constants.PROFILE_BASIC_ACCESS)) {
            EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_OrderSubmissionResponse.CLASSNAME, 
                        EP_FE_OrderSubmissionResponse.METHOD, EP_FE_OrderSubmissionResponse.SEVERITY, 
                        EP_FE_OrderSubmissionResponse.TRANSACTION_ID, EP_FE_OrderSubmissionRequest.DESCRIPTION1), 
                        response,EP_FE_OrderSubmissionRequest.ERROR_MISSING_REQUEST);
        }
        else {
            // Check if the OrderId is defined
            if (request.draftOrderId == null) {
                EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_OrderSubmissionResponse.CLASSNAME, 
                        EP_FE_OrderSubmissionResponse.METHOD, EP_FE_OrderSubmissionResponse.SEVERITY, 
                        EP_FE_OrderSubmissionResponse.TRANSACTION_ID, EP_FE_OrderSubmissionRequest.DESCRIPTION2),
                        response,EP_FE_OrderSubmissionRequest.ERROR_MISSING_ORDERID);
            }
            else{
                // Order Id found
                csord__Order__c newOrder = [SELECT Id, Name, Status__c, csord__Account__r.RecordType.DeveloperName, AccountId__c, CurrencyIsoCode, OrderNumber__c, 
                                                //EP_Truck_Licence_Plate__c,
                                                EP_ShipTo__c, EP_Customer_Reference_Number__c, EP_Order_Comments__c, PriceBook2Id__c, EffectiveDate__c,
                                                CreatedById, EP_Order_Delivery_Type__c, EP_Requested_Delivery_Date__c, Type__c, EP_Sell_To__c,
                                                Stock_Holding_Location__r.Stock_Holding_Location__r.EP_Scheduling_Hours__c, EP_Is_Pre_Pay__c,
                                                Stock_Holding_Location__r.EP_Trip_Duration__c, EP_Is_Portal_Order_Active__c, EP_Bill_To__c
                                                FROM csord__Order__c
                                                WHERE Id = : request.draftOrderId
                                                LIMIT 1]; 
                                    //Because of security reasons API should validate that Order is Submitted by the same user,
                                    //who has created it    
                              

                if(newOrder != null){
                    //ctx = new EP_OrderPageContext(string.valueOf(newOrder.EP_Sell_To__c));
                    EP_OrderDomainObject orderDomainObj = new EP_OrderDomainObject(newOrder.Id);
                    EP_OrderService orderService = new EP_OrderService(orderDomainObj);
                    Boolean creditCheck = orderService.hasCreditIssue();
                    //response.pastDueInvoiceFlag = EP_FE_OrderUtils.getOverdueInvoice(newOrder.EP_Sell_To__c);
                    response.pastDueInvoiceFlag = orderDomainObj.isOrderSellToInvoiceOverdue();
                    
                    if(EP_Common_Constant.ORDER_DRAFT_STATUS.equalsIgnoreCase(newOrder.Status__c) || 
                       EP_FE_OrderUtils.evaluateCancellableEditableOrder(newOrder)){
                        try{
                                                       
                            //update the Order status from Draft to Submitted
                            /*if (EP_Common_Constant.ORDER_DRAFT_STATUS.equalsIgnoreCase(newOrder.Status)){
                                //Boolean creditCheck = EP_Orders.checkOrderCreditCheck(newOrder);
                                if(response.pastDueInvoiceFlag)
                                    newOrder.Status = EP_Common_Constant.ORDER_DRAFT_STATUS;
                                else if(creditCheck){
                                    newOrder.EP_Payment_Term__c = 'PrePayment';                                    
                                    newOrder.Status = EP_Common_Constant.ORDER_DRAFT_STATUS;
                                }
                                else
                                    newOrder.Status = EP_Common_Constant.SUBMITTED; 
                            }*/
                      /*      
                            if (request.truckPlate != null) { 
                                    //Truck plate doesn't cause price recalculation so should be updated during submission separately
                                    //newOrder.EP_Truck_Licence_Plate__c = EP_FE_Utils.getInputValue(request.truckPlate);
                                }
                            if (request.customerReferenceNumber != null) {
                                    newOrder.EP_Customer_Reference_Number__c = EP_FE_Utils.getInputValue(request.customerReferenceNumber);
                                }  
                            if (request.comments != null) {
                                    newOrder.EP_Order_Comments__c = EP_FE_Utils.getInputValue(request.comments);
                                }
                                
                            EP_OrderEvent orderEvent = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
                            orderService.setOrderStatus(orderEvent);
                            newOrder.Status__c = orderDomainObj.getStatus();
                            
                            /*if(newOrder.status == EP_Common_Constant.ORDER_DRAFT_STATUS) {
                                if(creditCheck || response.pastDueInvoiceFlag)                              
                                    newOrder.EP_Credit_Status__c = EP_OrderConstant.CREDIT_FAIL;
                            }
                            else if (ctx.isPrepaymentCustomer && !ctx.isConsignmentOrder){
                                newOrder.EP_Payment_Term__c = EP_Common_Constant.PREPAYMENT;
                            }*/
                       /*         
                            Database.update (newOrder);
                             
                            orderService.doPostStatusChangeActions();
                            
                            response.isSuccess = TRUE;
                            response.newOrderId = newOrder.Id;
                            response.newOrderNumber = newOrder.OrderNumber__c;
                            //EP_FE_Utils.raiseExceptionIfTestIsRunning();

                        }catch (Exception ex) {
                            throw ex;
                            EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_OrderSubmissionResponse.CLASSNAME, 
                                        EP_FE_OrderSubmissionResponse.METHOD, EP_FE_OrderSubmissionResponse.SEVERITY, 
                                        EP_FE_OrderSubmissionResponse.TRANSACTION_ID, EP_FE_OrderSubmissionResponse.DESCRIPTION1),
                                        response,EP_FE_OrderSubmissionResponse.ERROR_UPDATING_ORDER);
                        }
                    }
                    else{
                        EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_OrderSubmissionResponse.CLASSNAME, 
                                    EP_FE_OrderSubmissionResponse.METHOD, EP_FE_OrderSubmissionResponse.SEVERITY, 
                                    EP_FE_OrderSubmissionResponse.TRANSACTION_ID, EP_FE_OrderSubmissionResponse.DESCRIPTION2),
                                    response,EP_FE_OrderSubmissionResponse.ERROR_ORDER_STATUS_SHOULD_BE_DRAFT_FOR_UPDATING_ORDER);
                    }
                }
            }
        }
        return response;
    }
    */
}