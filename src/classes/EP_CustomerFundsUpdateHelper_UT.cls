@isTest
public class EP_CustomerFundsUpdateHelper_UT
{
    @testSetup static void init() {
        List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
    }
    static testMethod void setCustomerAttributes_test() {          
        /*CAM 2.7 Start*/
        EP_TestDataUtility.createInboundCustomerFundsMessage();   
        /*CAM 2.7 End*/      
        List< EP_CustomerFundsUpdateStub.customer> customerList = EP_TestDataUtility.createInboundListOfCustomerFunds();    
        Test.startTest();
        EP_CustomerFundsUpdateHelper.setCustomerAttributes(customerList);
        Test.stopTest();     
        System.assertEquals(true, customerList[0].SFAccount.EP_Source_Seq_Id__c == customerList[0].seqId);
        System.assertEquals(true, customerList[0].SFAccount.CurrencyIsoCode == customerList[0].currencyId);
    }

    static testMethod void setAccountFields_test() {    
        EP_CustomerFundsUpdateStub.Customer cust = EP_TestDataUtility.createInboundCustomerFunds(); 
        cust.SFAccount = new Account();
        Test.startTest();
        EP_CustomerFundsUpdateHelper.setAccountFields(cust);
        Test.stopTest();
        System.assertEquals(true, cust.SFAccount.EP_Source_Seq_Id__c == cust.seqId);
        System.assertEquals(true, cust.SFAccount.CurrencyIsoCode == cust.currencyId);  
    }

    static testMethod void getCustomerIdSet_test() {
        List< EP_CustomerFundsUpdateStub.customer> customerList = EP_TestDataUtility.createInboundListOfCustomerFunds(); 
        Test.startTest();
        Set<string> result = EP_CustomerFundsUpdateHelper.getCustomerIdSet(customerList);
        Test.stopTest(); 
        System.AssertEquals(true,result != null);
    }

    static testMethod void setAccount_NoMatch_test() { 
        EP_CustomerFundsUpdateStub.Customer cust = EP_TestDataUtility.createInboundCustomerFunds();   
        Account acc = EP_TestDataUtility.getSellToASBasicDataSetUp();
        cust.identifier.billTo = string.valueOf(acc.AccountNumber);
        cust.identifier.clientId = acc.EP_Account_Company_Name__c; 
        String compositeKey = acc.EP_Composite_Id__c ;
        Map<String,Account> mapCompositeIDCustomers = new Map<String,Account>();
        mapCompositeIDCustomers.put(acc.EP_Composite_Id__c ,acc );
        Test.startTest();
        EP_CustomerFundsUpdateHelper.setAccount(cust, mapCompositeIDCustomers);
        Test.stopTest();
        System.assertEquals(true, cust.SFAccount.id == null);
        System.assertEquals(true, cust.errorDescription != null);  
    }
    static testMethod void setAccount_Match_test() { 
        EP_CustomerFundsUpdateStub.Customer cust = EP_TestDataUtility.createInboundCustomerFunds();   
        Account acc = EP_TestDataUtility.getSellToASBasicDataSetUp();
        cust.identifier.billTo = string.valueOf(acc.AccountNumber);
        cust.identifier.clientId = acc.EP_Account_Company_Name__c; 
        Map<String,Account> mapCompositeIDCustomers = new Map<String,Account>();
        mapCompositeIDCustomers.put(acc.EP_Composite_Id__c.toUpperCase() ,acc );
        Test.startTest();
        EP_CustomerFundsUpdateHelper.setAccount(cust, mapCompositeIDCustomers);
        Test.stopTest();
        System.assertEquals(true, cust.SFAccount.id != null);
    }
    //L4# 67333 Code Changes Start
    static testMethod void setHoldingAccount_test() { 
        EP_CustomerFundsUpdateStub.Customer cust = EP_TestDataUtility.createInboundCustomerFunds();   
        Account acc = EP_TestDataUtility.getSellToWithHolding();
        Account holdingAccount = [Select EP_Composite_Id__c,AccountNumber from Account where Id=: acc.EP_Sell_To_Holding_Account__c limit 1];
        cust.creditHoldingInfo.creditHoldingId = string.valueOf(holdingAccount.AccountNumber);
        cust.identifier.clientId = acc.EP_Account_Company_Name__c; 
        Map<String,Account> mapCompositeIDCustomers = new Map<String,Account>();
        mapCompositeIDCustomers.put(holdingAccount.EP_Composite_Id__c.toUpperCase() ,holdingAccount );
        Test.startTest();
        EP_CustomerFundsUpdateHelper.setHoldingAccount(cust, mapCompositeIDCustomers);
        Test.stopTest();
        System.assertEquals(true, cust.holdingAccount.id != null);
    } 
    
    static testMethod void setHoldingAccountFields_test() {    
        EP_CustomerFundsUpdateStub.Customer cust = EP_TestDataUtility.createInboundCustomerFunds(); 
        Account accObj = new Account();
        Test.startTest();
        EP_CustomerFundsUpdateHelper.setHoldingAccountFields(cust,accObj);
        Test.stopTest();
        System.assertEquals(true, accObj.EP_Available_Funds_LCY__c == Decimal.valueOf(cust.creditHoldingInfo.availableAmt));
        System.assertEquals(true, accObj.EP_AvailableFunds__c == Decimal.valueOf(cust.creditHoldingInfo.availableAmtCustCurr));
        System.assertEquals(true, accObj.EP_Holding_Overdue_Balance_LCY__c == Decimal.valueOf(cust.creditHoldingInfo.overDueBalance));
        System.assertEquals(true, accObj.EP_Holding_Overdue_Balance_HCY__c == Decimal.valueOf(cust.creditHoldingInfo.overDueBalanceCustCurr));  
    }
    //L4# 67333 Code Changes End
}