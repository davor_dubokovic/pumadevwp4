@isTest
public class EP_OrderStateCreditReview_UT
{
    @testSetup 
    static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    } 
    static testMethod void getTextValue_test() {
        Test.startTest();
        String result = EP_OrderStateCreditReview.getTextValue();
        Test.stopTest();     
        System.AssertEquals(EP_OrderConstant.OrderState_Awaiting_Credit_Review,result);
    }
    static testMethod void doOnEntry_test() {
        EP_OrderStateCreditReview localObj = new EP_OrderStateCreditReview();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateCreditReviewDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.assert(True);
        //Dummy Assert, No operation performs on this method
    }
    static testMethod void doOnExit_test() {
        EP_OrderStateCreditReview localObj = new EP_OrderStateCreditReview();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateCreditReviewDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        System.assert(True);
        //Dummy Assert, No operation performs on this method        
    }
    
    static testMethod void setOrderContext_test() {
        EP_OrderStateCreditReview localObj = new EP_OrderStateCreditReview();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateCreditReviewDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);
        EP_OrderEvent currentEvent= new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        Test.startTest();
        localObj.setOrderContext(obj,currentEvent);
        Test.stopTest();
        System.AssertEquals(true,localObj.order != null);
    }
    static testMethod void setOrderDomainObject_test() {
        EP_OrderStateCreditReview localObj = new EP_OrderStateCreditReview();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateCreditReviewDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);    
        Test.startTest();
        localObj.setOrderDomainObject(obj);
        Test.stopTest();
        System.AssertEquals(obj.getOrder().Id, localObj.order.getOrder().Id);
    }
}