/**
 * @author <Pooja Dhiman>
 * @name <EP_InvoiceTriggerHandler>
 * @createDate <05/02/2016>
 * @description <This class handles requests from Invoice trigger> 
 * @version <1.0>
 */
public with sharing class EP_InvoiceTriggerHandler {
    /*
        This method handles before insert requests from Invoice trigger
    */
    public static void doBeforeInsert(List<EP_Invoice__c> lNewInvoice){
    	EP_InvoiceTriggerHelper.validateInvoice(lNewInvoice);
    }
}