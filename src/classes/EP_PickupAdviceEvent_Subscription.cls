/**
  * @author       Accenture                                       
  * @name         EP_PickupAdviceEvent_Subscription                           
  * @Created Date 02/21/2018                                      
  * @description  PickUp Advice Email Notification Class                   
*/
public with sharing class EP_PickupAdviceEvent_Subscription extends EP_Event_Subscription{

/****************************************************************
* @author       Accenture                                       *
* @name         EP_PickupAdviceEvent_Subscription               *
* @description  Constructor                                     *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public EP_PickupAdviceEvent_Subscription() {
        subscriptionEventObj.eventType = label.EP_PICKUPADVICE;
    }
}