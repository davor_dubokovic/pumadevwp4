/*
   @Author <Accenture>
   @name <EP_WrapperAccountStatementItem>
   @Description <This class handles requests from EP_CustomerAccountStatementUtility class>
   @Version <1.0>
*/
global with sharing class EP_WrapperAccountStatementItem {
    public Id     wCustAccStatementItemId {get;set;}
    public string wAccEntryDocNr {get;set;}
    public string wAccEntryType {get;set;}
    public string wAccEntryCurr {get;set;}
    public double wAccEntryAmount {get;set;}
    public string wAccEntryDueDate {get;set;}
    public string wAccEntryIssueDate {get;set;}
    public string wAccEntryDesc {get;set;}
    public string wAccEntryDescFull {get;set;}
    public double wClosingBalance{get;set;}
    // Newly added fields @Alper Genc.
    public double wAccRemainingBalance {get; set;}
    public List<csord__Order__c> wAccRelatedOrders {get; set;}
    public Account wAccBillTo {get; set;}
    public string wAccPDFLink {get; set;}
    
    /**
    *   This method creates the list to pass the Customer Account Statement Item table
    *   
    */
    public EP_WrapperAccountStatementItem() {
        wAccRelatedOrders = new List<csord__Order__c>();
    }
}