/* 
  @Author <Ram Rai>
   @name <EP_FE_CancelOrderInfoEndpoint>
   @CreateDate <20/05/2016>
   @Description <This class Cancel Order >  
   @Version <1.0>
*/
@RestResource(urlMapping = '/FE/V1/order/cancel/*')
global with sharing class EP_FE_CancelOrderInfoEndpoint {

    // Parameters keys
    global final static String PARAM_KEY_ORDER_ID = 'orderId';
    global final static String PARAM_KEY_REASON = 'reason';
    global final static String PARAM_KEY_REASONDETAIL = 'reasonDetail';

    global final static String OTHER = 'Other';
    global static final String CANCEL_ORDER= 'Order has been canceled at ';
    
    global static final String PARAM_KEY_ACTION = 'action';
    
    global static final String strCancel = 'c';
    global static final String strdelete = 'd';
    
   /** API Call */
    @HttpPost
    webservice static EP_FE_CancelOrderInfoResponse cancelOrder() {
        EP_FE_CancelOrderInfoResponse response = new EP_FE_CancelOrderInfoResponse();
/*
        // Get the parameters
         
        Map<String, Object> params = (Map<String, Object>) JSON.deserializeUntyped(RestContext.request.requestBody.toString());
           
        String orderIdStr = params.containsKey(PARAM_KEY_ORDER_ID) 
            ? String.valueOf(params.get(PARAM_KEY_ORDER_ID)) : null;
        String reasonStr = params.containsKey(PARAM_KEY_REASON) 
            ? String.valueOf(params.get(PARAM_KEY_REASON)) : null;
        String reasonDetailStr = params.containsKey(PARAM_KEY_REASONDETAIL) 
            ? String.valueOf(params.get(PARAM_KEY_REASONDETAIL)) : null;
        String actionStr = params.containsKey(PARAM_KEY_ACTION) 
            ? String.valueOf(params.get(PARAM_KEY_ACTION)) : null;

        // Check Parameter
        if(string.isNotBlank(orderIdStr) && (string.isNotBlank(reasonStr) || (reasonStr == OTHER && string.isNotBlank(reasonDetailStr))) && (string.isBlank(actionStr) || actionStr.equals(strCancel))) {
        
            if (reasonStr == OTHER && string.isBlank(reasonDetailStr)) {
                
                response.status = EP_FE_CancelOrderInfoResponse.ERROR_MISSING_REASONDETAIL;
                return response;
            }

            
            cancelOrder(response, Id.valueOf(orderIdStr), EP_FE_Utils.getInputValue(reasonStr), EP_FE_Utils.getInputValue(reasonDetailStr));

        } else if(string.isNotBlank(orderIdStr) && (string.isNotBlank(actionStr) && actionStr.equals(strdelete))){
            deleteOrder(response, Id.valueOf(orderIdStr)); 
        } else {
            response.status = EP_FE_CancelOrderInfoResponse.ERROR_MISSING_ORDERID; 
        }*/

        return response; 
    }    

}