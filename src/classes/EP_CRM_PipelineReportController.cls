/* ================================================
 * @Class Name : EP_CRM_PipelineReportController 
 * @author : TCS
 * @Purpose: This controller class is used to generate report for opportunity volume.
 * @created date: 15/11/2017
 ================================================*/
public with sharing class EP_CRM_PipelineReportController {
    // Model class reference used to provide values to UI
    public EP_CRM_PipelineReportBean bean { get; set; }

    // Show/Hide reports based on permission/roles
    public Boolean showForm { get; set; }

    // It stores logged in used information
    private User loggedInUserObj = EP_CRM_DatabaseSOQL.getUserDetails();

    // Stores all products with their default UoM values
    private Map < String, String > productsDefaultUoM = getProductsDefaultUoM();

    // Returns true if all selected products having same UoM...Default true 
    public Boolean isSelectedProductsWithSameUoM { get {
        if(isSelectedProductsWithSameUoM == null) {
            return true;
        } return isSelectedProductsWithSameUoM;
    } set; }
    
    // Default constructor to perform initialization
    public EP_CRM_PipelineReportController() {
        showForm = true;
        if(loggedInUserObj != null && loggedInUserObj.UserRole == null) {
            showForm = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, EP_CRM_Constants.NO_ROLE_ASSIGNED_ERROR));
            return;
        }
        bean = new EP_CRM_PipelineReportBean(loggedInUserObj);
        generatePipelineReport();
    }
    
    /**
    * This method is used to set default UoM value based on region and selected products.
    */
    public void prepareUoM() {
        isSelectedProductsWithSameUoM = true;
        if(String.isNotBlank(bean.opportunityObj.EP_CRM_Product_Interest_Group__c)) {
            Set < String > unitSet =  new Set < String > ();
            for(String str: (bean.opportunityObj.EP_CRM_Product_Interest_Group__c).split(';')) {
                if(bean.getAfricaUser()) {
                    unitSet.add(productsDefaultUoM.get(EP_CRM_Constants.AFRICA + ':' + str));
                } else if(bean.getAmericaUser()) {
                    unitSet.add(productsDefaultUoM.get(EP_CRM_Constants.US + ':' + str));
                } else if(bean.getAustraliaUser()) {
                    unitSet.add(productsDefaultUoM.get(EP_CRM_Constants.MEAP + ':' + str));
                }           
            } 
            List < String > unitList = new List < String > (unitSet);
            if(unitList.size() == 1) {
                bean.selectedUoM = unitList.get(0);
            } else {
                // Display Error | Information Message
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, EP_CRM_Constants.MULTIPLE_PRODUCTS_WITH_DIFFERENT_UOM_ERROR));
                isSelectedProductsWithSameUoM = false;
                bean.selectedUoM = EP_CRM_Constants.BLANK;
            }
        }
    }
    
    /**
    * This method is used to provide all default UoM value as per region and customer segment
    */
    private Map < String, String > getProductsDefaultUoM () {
        Map < String, String > productsDefaultUoM = new Map < String, String > ();
        productsDefaultUoM.put(EP_CRM_Constants.AFRICA + ':' + EP_CRM_Constants.FUEL_CARD, EP_CRM_Constants.LITRE);
        productsDefaultUoM.put(EP_CRM_Constants.AFRICA + ':' + EP_CRM_Constants.LUBRICANTS, EP_CRM_Constants.LITRE);
        productsDefaultUoM.put(EP_CRM_Constants.AFRICA + ':' + EP_CRM_Constants.FUELS, EP_CRM_Constants.LITRE);
        productsDefaultUoM.put(EP_CRM_Constants.AFRICA + ':' + EP_CRM_Constants.BITUMEN, EP_CRM_Constants.METRIC_TON);
        productsDefaultUoM.put(EP_CRM_Constants.AFRICA + ':' + EP_CRM_Constants.CHEMICAL_PRODUCTS, EP_CRM_Constants.METRIC_TON);
        productsDefaultUoM.put(EP_CRM_Constants.AFRICA + ':' + EP_CRM_Constants.LPG, EP_CRM_Constants.KILOGRAM);
        productsDefaultUoM.put(EP_CRM_Constants.AFRICA + ':' + EP_CRM_Constants.NON_FUELS, EP_CRM_Constants.UNIT);
        productsDefaultUoM.put(EP_CRM_Constants.US + ':' + EP_CRM_Constants.NON_FUELS, EP_CRM_Constants.UNIT);
        productsDefaultUoM.put(EP_CRM_Constants.US + ':' + EP_CRM_Constants.LUBRICANTS, EP_CRM_Constants.GALLON);
        productsDefaultUoM.put(EP_CRM_Constants.US + ':' + EP_CRM_Constants.FUEL_CARD, EP_CRM_Constants.GALLON);
        productsDefaultUoM.put(EP_CRM_Constants.US + ':' + EP_CRM_Constants.FUELS, EP_CRM_Constants.GALLON);
        productsDefaultUoM.put(EP_CRM_Constants.US + ':' + EP_CRM_Constants.LPG, EP_CRM_Constants.KILOGRAM);
        productsDefaultUoM.put(EP_CRM_Constants.US + ':' + EP_CRM_Constants.BITUMEN, EP_CRM_Constants.METRIC_TON);
        productsDefaultUoM.put(EP_CRM_Constants.US + ':' + EP_CRM_Constants.CHEMICAL_PRODUCTS, EP_CRM_Constants.METRIC_TON);
        productsDefaultUoM.put(EP_CRM_Constants.MEAP + ':' + EP_CRM_Constants.LUBRICANTS, EP_CRM_Constants.LITRE);
        productsDefaultUoM.put(EP_CRM_Constants.MEAP + ':' + EP_CRM_Constants.FUELS, EP_CRM_Constants.LITRE);
        productsDefaultUoM.put(EP_CRM_Constants.MEAP + ':' + EP_CRM_Constants.FUEL_CARD, EP_CRM_Constants.LITRE);
        productsDefaultUoM.put(EP_CRM_Constants.MEAP + ':' + EP_CRM_Constants.NON_FUELS, EP_CRM_Constants.UNIT);
        productsDefaultUoM.put(EP_CRM_Constants.MEAP + ':' + EP_CRM_Constants.LPG, EP_CRM_Constants.KILOGRAM);
        productsDefaultUoM.put(EP_CRM_Constants.MEAP + ':' + EP_CRM_Constants.BITUMEN, EP_CRM_Constants.METRIC_TON);
        productsDefaultUoM.put(EP_CRM_Constants.MEAP + ':' + EP_CRM_Constants.CHEMICAL_PRODUCTS, EP_CRM_Constants.METRIC_TON);
        return productsDefaultUoM;
    }
    
    /**
    * This method is used to generate pipeline report.
    */
    public void generatePipelineReport() {
        if(String.isBlank(bean.selectedUoM)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, EP_CRM_Constants.UOM_VALUE_MISSING_ERROR));
            return;
        }
        Map < String, EP_CRM_PipelineReportWrapper > filterWrapperMap = new Map < String, EP_CRM_PipelineReportWrapper > ();
        for(Opportunity opptyObj: EP_CRM_DatabaseSOQL.getOpportunities(bean)) {
            EP_CRM_PipelineReportWrapper wrapperObj = null;
            String key = generateUniqueKey(opptyObj);
            Date targetDate = EP_CRM_ReportUtility.getReportTargetDate(opptyObj);
            if(targetDate.year() == bean.reportDateValue.year()) {
                if(opptyObj.isWon) { // Change CreatedDate to CloseDate
                    if(targetDate <= bean.reportDateValue) {    
                        if(opptyObj.Expected_Monthly_Volume__c != null) {
                            wrapperObj = filterWrapperMap.get(key); 
                            if(wrapperObj == null) {
                                wrapperObj = prepareWrapperObject(opptyObj);                            
                            }
                            if(wrapperObj.closedOpportunityMap.get(targetDate.month()) != null) {
                                filterWrapperMap.put(key, wrapperObj);
                                wrapperObj.opportunityNameList.add(opptyObj.Name);
                                Decimal value = wrapperObj.closedOpportunityMap.get(targetDate.month());                        
                                value = value + (opptyObj.Expected_Monthly_Volume__c * EP_CRM_ReportUtility.getConvertionRate(opptyObj.EP_CRM_UoM__c, bean.selectedUoM));                       
                                // value = value + opptyObj.Expected_Monthly_Volume__c; 
                                wrapperObj.closedWonAmount = wrapperObj.closedWonAmount + (opptyObj.Expected_Monthly_Volume__c * EP_CRM_ReportUtility.getForcastMonthCount(opptyObj) * EP_CRM_ReportUtility.getConvertionRate(opptyObj.EP_CRM_UoM__c, bean.selectedUoM));                   
                                wrapperObj.closedOpportunityMap.put(targetDate.month(), value);
                            }                        
                        }                   
                    }
                } else {
                    if(targetDate >= bean.reportDateValue) {
                        if(opptyObj.Expected_Monthly_Volume__c != null) {
                            wrapperObj = filterWrapperMap.get(key); 
                            if(wrapperObj == null) {
                                wrapperObj = prepareWrapperObject(opptyObj);
                            }
                            if(wrapperObj.openOpportunityMap.get(targetDate.month()) != null) {  
                                filterWrapperMap.put(key, wrapperObj);                      
                                wrapperObj.opportunityNameList.add(opptyObj.Name);
                                Decimal value = wrapperObj.openOpportunityMap.get(targetDate.month());
                                value = value + (opptyObj.Expected_Monthly_Volume__c * EP_CRM_ReportUtility.getConvertionRate(opptyObj.EP_CRM_UoM__c, bean.selectedUoM));   
                                // value = value + opptyObj.Expected_Monthly_Volume__c;
                                wrapperObj.remainingForecastAmount = wrapperObj.remainingForecastAmount + (opptyObj.Expected_Monthly_Volume__c * EP_CRM_ReportUtility.getForcastMonthCount(opptyObj) * EP_CRM_ReportUtility.getConvertionRate(opptyObj.EP_CRM_UoM__c, bean.selectedUoM));
                                wrapperObj.openOpportunityMap.put(targetDate.month(), value);       
                            }
                        }
                    }
                }
            }
        }
        bean.wrapperList = filterWrapperMap.values(); 
    }
    
    /**
    * This method is used generate unique key which will be used to group data at each level based on filters.
    */
    private String generateUniqueKey(Opportunity opptyObj) {
        String key = '';
        if(opptyObj != null) {
            if(String.isNotBlank(opptyObj.EP_CRM_Geo1__c)) {
                key = key + ':' + opptyObj.EP_CRM_Geo1__c; 
            }
            if(bean.getGlobalUser() == false) {
                if(String.isNotBlank(opptyObj.EP_CRM_Geo2__c)) {
                    key = key + ':' + opptyObj.EP_CRM_Geo2__c; 
                } 
                if(bean.getRegionalExecutiveUser() == false) {
                    if(String.isNotBlank(opptyObj.EP_CRM_Geo3__c)) {
                        key = key + ':' + opptyObj.EP_CRM_Geo3__c; 
                    }
                    if(String.isNotBlank(opptyObj.EP_CRM_Geo4__c)) {
                        key = key + ':' + opptyObj.EP_CRM_Geo4__c; 
                    }                   
                }
            }
            if(String.isNotBlank(opptyObj.EP_CRM_Customer_Segment__c)) {
                key = key + ':' + opptyObj.EP_CRM_Customer_Segment__c; 
            }
            if(String.isNotBlank(opptyObj.EP_CRM_Primary_Product_Interest__c)) {
                key = key + ':' + opptyObj.EP_CRM_Primary_Product_Interest__c; 
            }
            if(bean.getManagerUser() && String.isNotBlank(opptyObj.OwnerId)) {
                key = key + ':' + opptyObj.OwnerId; 
            }
            if(bean.getSalesUser()) {
                key = key + ':' + opptyObj.Id;
            }
        }
        return key;
    }
    
    /**
    * This method is used to prepare wrapperobj used to generate volume report.
    */
    private EP_CRM_PipelineReportWrapper prepareWrapperObject(Opportunity opptyObj) {
        EP_CRM_PipelineReportWrapper wrapperObj = new EP_CRM_PipelineReportWrapper(bean.getOpenOpportunityMap(), bean.getClosedOpportunityMap());       
        wrapperObj.geo1 = opptyObj.EP_CRM_Geo1__c;
        wrapperObj.geo2 = opptyObj.EP_CRM_Geo2__c;
        wrapperObj.geo3 = opptyObj.EP_CRM_Geo3__c;
        wrapperObj.geo4 = opptyObj.EP_CRM_Geo4__c;
        wrapperObj.segment = opptyObj.EP_CRM_Customer_Segment__c;
        wrapperObj.product = opptyObj.EP_CRM_Primary_Product_Interest__c;
        wrapperObj.childUserName = opptyObj.Owner.Name;
        wrapperObj.opportunityObj = opptyObj;       
        return wrapperObj;
    }
}