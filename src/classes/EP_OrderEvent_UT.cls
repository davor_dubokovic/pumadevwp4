@isTest
public class EP_OrderEvent_UT
{

static testMethod void getEventName_test() {
	String eventname = EP_OrderConstant.USER_SUBMIT;
	EP_OrderEvent localObj = new EP_OrderEvent(eventname);
	Test.startTest();
	String result = localObj.getEventName();
	Test.stopTest();
	System.AssertEquals(EP_OrderConstant.USER_SUBMIT,result);
}
static testMethod void setEventName_test() {
	String eventname;
	EP_OrderEvent localObj = new EP_OrderEvent(eventname);
	String value = EP_OrderConstant.USER_SUBMIT;
	Test.startTest();
	localObj.setEventName(value);
	String result = localObj.getEventName();
	Test.stopTest();
	System.AssertEquals(EP_OrderConstant.USER_SUBMIT,result);
}
static testMethod void getEventMessage_test() {
	String eventname;
	EP_OrderEvent localObj = new EP_OrderEvent(eventname);
	String value = EP_OrderConstant.OVERDUE_MESSAGE;
	Test.startTest();
	localObj.setEventMessage(value);
	String result = localObj.getEventMessage();
	Test.stopTest();
	System.AssertEquals(EP_OrderConstant.OVERDUE_MESSAGE,result);
}
static testMethod void setEventMessage_test() {
	String eventname;
	EP_OrderEvent localObj = new EP_OrderEvent(eventname);
	String value = EP_OrderConstant.OVERDUE_MESSAGE;
	Test.startTest();
	localObj.setEventMessage(value);
	String result = localObj.getEventMessage();
	Test.stopTest();
	System.AssertEquals(EP_OrderConstant.OVERDUE_MESSAGE,result);
}
}