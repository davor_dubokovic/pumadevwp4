public with sharing class EP_AddressControllerContext {
    public String address {get;set;}
    public String recordTypeName;
    public Id recordId;
    public String ObjectName;
    public SObject record;
    public String countryCode;
    public Schema.SobjectType sObjectType;
    public Boolean hasRecordType;
    public Boolean isCountryReference = false;
    public Boolean isChangeRequest = false; 
    public String oldAddress;
    public string enteredCntryCode;
    public string addressFieldLbl;
    public string addressFieldApi;
    public AddressWrapper addressobj = new AddressWrapper();
    
    @testvisible
    map<String,String>mTypeField = new map<String,String>();
    private static final String CLASSNAME ='EP_AddressControllerContext';
    private static final String ADRESS_STR = 'address';
    private static final String STR_C = 'c';
    private static final String CR_STR = 'CR';
    private static final String EDIT_ACCESS_ERROR = 'You do not have edit access on this record.';
    private static final String IS_ADDRESS_VALID = 'IS ADDRESS VALID';
    private static final String CUSTOM_LAT= 'latitude__s';
    private static final String CUSTOM_LNG= 'longitude__s';
        private static final String LAT_STR = 'lat';
    private static final String LNG_STR = 'lng';
    private static final String ADD_CR_FIELD = 'Address CR Field';
        private static final String STREET_NUMBER_STR = 'street_number';
    private static final String ROUTE_STR = 'route';
    private static final String LOCALITY_STR = 'locality';
    private static final String ADMIN_AREA_STR = 'administrative_area_level_1';
    private static final String POSTAL_CODE_STR = 'postal_code';
        private static final String UPDATE_STR = 'UPDATE';
    public Boolean showError{public get; public set;}
    public String googleMapKey{get; set;}
    
    public EP_AddressControllerContext(Map<String, String> mapOfURLParameters){
        address = EP_Common_Constant.BLANK;
        showError = false;
        isCountryReference = false;
        recordId = (Id)mapOfURLParameters.get(EP_Common_Constant.ID);
        countryCode = mapOfURLParameters.get(EP_Common_Constant.CNTRY_CODE);
        sObjectType = recordId.getSObjectType();
        ObjectName = sObjectType.getDescribe().getName();
        hasRecordType = (sObjectType.getDescribe().getRecordTypeInfos().size()>1)? true:false;
        //isChangeRequest = (mapOfURLParameters.containsKey(CR_STR) && mapOfURLParameters.get(CR_STR).equalsIgnoreCase(EP_Common_Constant.YES))?true:false;
        oldAddress = mapOfURLParameters.get(ADRESS_STR);
        checkUserAccess();
    }
    
    /**
    * @author <Ashok Arora>
    * @date <02/05/2016>
    * @description <Method to check looged in user's access>
    * @param none
    * @return void
    */
	@testvisible
    private void checkUserAccess(){
        EP_GeneralUtility.Log('Private','EP_AddressControllerContext','checkUserAccess');        
        if(recordId != null){
            integer nRows = EP_Common_Util.getQueryLimit();
            List<UserRecordAccess> userAccessRec = new List<UserRecordAccess>([SELECT RecordId, HasEditAccess 
                FROM UserRecordAccess 
                WHERE UserId =: UserInfo.getUserId() 
                AND RecordId =:recordId Limit 1]);
            if(!userAccessRec.isEmpty() && !userAccessRec[0].HasEditAccess) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, EDIT_ACCESS_ERROR));
                showError = true;
            }                                                                                                                         
        }  
    }    
    
    public void setAddressFields(){   
        EP_GeneralUtility.Log('Private','EP_AddressControllerContext','setAddressFields'); 
        /*Bug Fix 85328*/
        removeNullString();
        /*Bug Fix 85328*/
        String street = addressObj.Street1+ EP_COMMON_CONSTANT.SPACE+ addressObj.Street2;
        record.put(mTypeField.get(EP_Common_Constant.STREET_STR),street);
        record.put(mTypeField.get(EP_Common_Constant.CITY_STR),addressObj.city);
        record.put(mTypeField.get(EP_Common_Constant.POST_CODE ),addressObj.zipCode);
        record.put(mTypeField.get(EP_COMMON_CONSTANT.Country.toUpperCase()),addressObj.country);
        record.put(mTypeField.get(EP_Common_Constant.STATE_STR),addressObj.state);
        if(mTypeField.containsKey(IS_ADDRESS_VALID )){
            record.put(mTypeField.get(IS_ADDRESS_VALID ),TRUE) ;
        }
        setLatitudeLongitude();
    }
    
	@TestVisible
    private void setLatitudeLongitude(){
        EP_GeneralUtility.Log('Private','EP_AddressControllerContext','setLatitudeLongitude'); 
        if(mTypeField.containsKey(EP_Common_Constant.LATLANG_STR)){
            setLATLANGString();
        }
        else {
            setLatitude(); 
            setLongitude();
        }   
    }
    
	@TestVisible
    private void setLATLANGString(){
        EP_GeneralUtility.Log('Private','EP_AddressControllerContext','setLATLANGString'); 
        record.put(mTypeField.get(EP_Common_Constant.LATLANG_STR).removeEnd(STR_C) + CUSTOM_LAT,addressObj.latitude);
        record.put(mTypeField.get(EP_Common_Constant.LATLANG_STR).removeEnd(STR_C) + CUSTOM_LNG,addressObj.longitude);
        //L4-45352
        record.put('EP_Position__latitude__s',addressObj.latitude);
        record.put('EP_Position__longitude__s',addressObj.longitude);
        //L4-45352
    }
    @TestVisible
    private void setLatitude(){
        EP_GeneralUtility.Log('Private','EP_AddressControllerContext','setLatitude'); 
        if(mTypeField.containsKey(EP_Common_Constant.LATITUDE_STR)){
            record.put(mTypeField.get(EP_Common_Constant.LATITUDE_STR),addressObj.latitude);
            //L4-45352
            record.put('EP_Position__latitude__s',addressObj.latitude);
            //L4-45352
        }   
    }
    
	@TestVisible
    private void setLongitude(){
        EP_GeneralUtility.Log('Private','EP_AddressControllerContext','setLongitude');
        if(mTypeField.containsKey(EP_Common_Constant.LONGITUDE_STR)){
            record.put(mTypeField.get(EP_Common_Constant.LONGITUDE_STR),addressObj.longitude);                
            //L4-45352
            record.put('EP_Position__longitude__s',addressObj.longitude);
            //L4-45352                
        }
    }
    
	@TestVisible
    private void removeNullString(){
        EP_GeneralUtility.Log('Private','EP_AddressControllerContext','removeNullString');
        addressObj.Street1 =  string.isNotBlank(addressObj.Street1)? addressObj.Street1 : EP_Common_Constant.BLANK;
        addressObj.Street2 = string.isNotBlank(addressObj.Street2)? addressObj.Street2 : EP_Common_Constant.BLANK;
        addressObj.city = string.isNotBlank(addressObj.city)? addressObj.city : EP_Common_Constant.BLANK;
        addressObj.zipCode = string.isNotBlank(addressObj.zipCode)? addressObj.zipCode : EP_Common_Constant.BLANK;
        addressObj.country = string.isNotBlank(addressObj.country)? addressObj.country : EP_Common_Constant.BLANK;
        addressObj.state = string.isNotBlank(addressObj.state)? addressObj.state : EP_Common_Constant.BLANK;
    }
    
	@TestVisible
    public AddressWrapper getAddressObject(){
        EP_GeneralUtility.Log('Private','EP_AddressControllerContext','getAddressObject');
        AddressWrapper addressObj = new AddressWrapper();
        List<String> lStr = new List<String>();
        addressObj.oldAddress = oldAddress;
        String value = EP_Common_Constant.BLANK;
        for(String addComp : address.removeEnd(EP_COMMON_CONSTANT.COMMA).split(EP_COMMON_CONSTANT.COMMA)){
            lStr = addComp.split(EP_COMMON_CONSTANT.TIME_SEPARATOR_SIGN);
            if(lStr.size() == EP_COMMON_CONSTANT.TWO && String.isNotBlank(lStr[1])){                
                value = lStr[1];
                if(STREET_NUMBER_STR .equalsIgnoreCase(lStr[0])){
                    addressObj.street1 = value;
                
                }else if(ROUTE_STR.equalsIgnoreCase(lStr[0])){
                    addressObj.street2 = value;
                }
                else if(LOCALITY_STR.equalsIgnoreCase(lStr[0])){
                    addressObj.city = lstr[1];
                }
                else if(ADMIN_AREA_STR.equalsIgnoreCase(lStr[0])){
                    addressObj.State = lstr[1];
                }
                else if(EP_COMMON_CONSTANT.COUNTRY.equalsIgnoreCase(lStr[0])){
                    addressObj.country= lstr[1];
                }
                else if(POSTAL_CODE_STR.equalsIgnoreCase(lStr[0])){
                    addressObj.zipCode = lstr[1];
                }
                else if(LAT_STR.equalsIgnoreCase(lStr[0])){
                    addressObj.latitude = Double.valueOf(lstr[1]);
                }
                else if(LNG_STR.equalsIgnoreCase(lStr[0])){
                    addressObj.longitude = Double.valueOf(lstr[1]);                    
                }
            }        
        }
        return addressObj;
    }

    @TestVisible
    private string getAddressAsString(){    
        EP_GeneralUtility.Log('Private','EP_AddressControllerContext','getAddressAsString');            
        string country = (addressObj.countryRecord != null ? addressObj.countryRecord.Name : EP_Common_Constant.BLANK);
        List<String> addressInfoList = new List<string>{addressObj.Street1,addressObj.Street2,addressObj.city,addressObj.state,country,addressObj.zipCode};
        string addressString =EP_Common_Constant.BLANK;
        for(string str: addressInfoList){
            addressString += (String.isNotBlank(str)?EP_Common_Constant.SPACE+str:EP_Common_Constant.BLANK);
        }
        return addressString;
    }
    
    public boolean isValidCountry(){
        EP_GeneralUtility.Log('Private','EP_AddressControllerContext','isValidCountry');
            boolean isValidcountry = false;
            EP_CountryMapper countryMapper = new EP_CountryMapper();        
            if(string.isNotBlank(addressObj.country)){
                List<EP_Country__c> countryRec = countryMapper.getRecordsByNames(new list<String>{addressObj.country});
                if(!countryRec.isEmpty()){
                    isValidcountry = true;         
                }           
            }
            return isValidcountry;
        }
        
        public boolean isAddressFromValidCountry(){
            EP_GeneralUtility.Log('Private','EP_AddressControllerContext','isAddressFromValidCountry');
            boolean isValidAddress = true;
            EP_CountryMapper countryMapper = new EP_CountryMapper();
            List<EP_Country__c> countryOnRec = countryMapper.getRecordsByCountryCode(new set<string> {countryCode});
            //if(!enteredCntryCode.equalsIgnoreCase(countryCode) && !isChangeRequest){
          	if(!enteredCntryCode.equalsIgnoreCase(countryCode)){
                isValidAddress = false;
            }
            return isValidAddress;
        }
        
        public pagereference commitAddressData(){
            EP_GeneralUtility.Log('Private','EP_AddressControllerContext','commitAddressData');
            pageReference pgref;    
            EP_Common_Util.isValidAddress = true;
            /*if(isChangeRequest){                      
                createChangeRequest();
            }
            else{
                DataBase.update(record);
            } */    
            DataBase.update(record);
            pgref = new PageReference(EP_COMMON_CONSTANT.SLASH + record.id);
            return pgref;
        }
    
        public void setAddressWrapperObject(){
            EP_GeneralUtility.Log('Private','EP_AddressControllerContext','setAddressWrapperObject');
            addressobj = getAddressObject();
            addressObj.addressFieldLabel = addressFieldLbl;
            addressObj.addressFieldAPIName = addressFieldApi;
            if(isCountryReference){
                addressObj.Country = null;
            }   
        }
        
        public void setContextVlaues(EP_Address_Fields__mdt addressField){
            EP_GeneralUtility.Log('Private','EP_AddressControllerContext','setContextVlaues');
            if(isCRAddressField(addressField.EP_Field_Type__c)){
                addressFieldLbl = addressField.EP_Field_Label__c;
                addressFieldApi = addressField.EP_Field_API_Name__c;
            }                    
            else if(isCountryReferenced(addressField)){
                isCountryReference = true;                        
            }
            else{
                mTypeField.put(addressField.EP_Field_Type__c.toUpperCase(),addressField.EP_Field_API_Name__c);
            }
        }
        
		@TestVisible
        private boolean isCRAddressField(string fieldType){
            EP_GeneralUtility.Log('Private','EP_AddressControllerContext','isCRAddressField');
            return (ADD_CR_FIELD.equalsIgnoreCase(fieldType));
        }
        
		@TestVisible
        private boolean isCountryReferenced(EP_Address_Fields__mdt addressField){
            EP_GeneralUtility.Log('Private','EP_AddressControllerContext','isCountryReferenced');
            return (EP_COMMON_CONSTANT.COUNTRY.equalsIgnoreCase(addressField.EP_Field_Type__c) && addressField.EP_IsReferenced__c);
        }
        
        public without sharing class AddressWrapper {
           public  String street1;
            public String Street2;
            public String City;
            public String zipCode;
            public String state;
            public String Country;
            public Double latitude;
            public Double longitude;
            public String cntryCode;
            public String oldAddress;
            public String addressFieldLabel;
            public String addressFieldAPIName;
            public EP_Country__c countryRecord;        
        }
}