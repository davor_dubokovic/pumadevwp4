/*
   @Author          CS
   @name            EP_RetrospectiveOrderRequestHandlerTest
   @CreateDate      23/04/2018
   @Description     Test for EP_RetrospectiveOrderRequestHandler Inbound handler (NAV to SFDC communication)
   @Version         1.0
*/
@IsTest
public class EP_RetrospectiveOrderRequestHandlerTest {

	// TMS Order XML data
	public static string jsonData(Account acc) {

		EP_NAVOrderNewHandler localObj = new EP_NAVOrderNewHandler();
		EP_NAVOrderNewStub stub = EP_TestDataUtility.createNAVOrderNewStub();

		stub.msg.payload.any0.order.sellToId = acc.AccountNumber;
		stub.msg.payload.any0.order.shipToId = acc.AccountNumber;

		return System.JSON.serialize(stub);
	}

	@testsetup
	private static void setupTestData() {

		EP_CS_InboundMessageSetting__c msgSetting = new EP_CS_InboundMessageSetting__c();

		msgSetting.Name = 'NAV_TO_SFDC_ORDER_SYNC';
		msgSetting.Source__c = 'NAV';
		msgSetting.Location__c = 'GBL';
		msgSetting.ProcessName__c = 'SCN';

		insert msgSetting;

		String COUNTRY_NAME = 'Australia';
		String COUNTRY_CODE = 'AU';
		String COUNTRY_REGION = 'Australia';
		String REGION_NAME = 'North-Australia';

		EP_Country__c country = EP_TestDataUtility.createCountryRecord(COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
		insert country;

		EP_Region__c region = EP_TestDataUtility.createCountryRegion(REGION_NAME, country.Id);
		insert region;

		Company__c comp = EP_TestDataUtility.createCompany('EPUMA');
		insert comp;

		Account storageLoc = EP_TestDataUtility.createStorageLocAccount(country.Id, null);
		Account sellToAccount = EP_TestDataUtility.createSellToAccountWithPickupContry(NULL, NULL, country.Id, region.Id);

		upsert sellToAccount;

		csord__Order__c csOrder = new csord__Order__c();
		csOrder.Name = 'Open Order Contract';
		csOrder.csord__Identification__c = '1236524584596622';
		csOrder.csord__Status__c = 'Active';
		csOrder.csord__Status2__c = 'Active';
		csOrder.csord__Account__c = sellToAccount.Id;

		insert csOrder;
	}

	@IsTest
	public static void processRequest() {

		Account acc = [SELECT Id, AccountNumber FROM Account LIMIT 1];

		EP_RetrospectiveOrderRequestHandler obj = new EP_RetrospectiveOrderRequestHandler();

		Test.startTest();

		obj.processRequest(EP_RetrospectiveOrderRequestHandlerTest.jsonData(acc));

		Test.stopTest();

		List<csord__Order__c> objs = [SELECT Id FROM csord__Order__c];

		System.assertEquals(2, objs.size(), 'Invalid Order count');
	}

	@IsTest
	public static void processRequestError() {

		EP_RetrospectiveOrderRequestHandler obj = new EP_RetrospectiveOrderRequestHandler();

		Test.startTest();

		string errorResponse = obj.processRequest('Invalid response');

		Test.stopTest();

		List<csord__Order__c> objs = [SELECT Id FROM csord__Order__c];

		System.assertEquals(1, objs.size(), 'Invalid Order count');
		System.assertNotEquals(null, errorResponse, 'Invalid error message');
	}
}