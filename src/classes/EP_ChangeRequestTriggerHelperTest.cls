/**
 * @author <Sandeep Kumar>
 * @name <EP_ChangeRequestTriggerHelperTest>
 * @createDate <03/11/2016>
 * @description
 * @version <1.0>
 */
@isTest
public class EP_ChangeRequestTriggerHelperTest {
     @isTest
     private static void init(){
        EP_TestDummyData.intializeChangeRqstTriggerHelperTest();
     }
    
    /*********************************************************************************************
    *@Description : This method valids mapDeliveryCountryAndRegion method of EP_ChangeRequestTriggerHelper class
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
    private static testMethod void testDelvryCountryAndRgnMapping(){
        init();
    }
    
    /*********************************************************************************************
    *@Description : This method valids commitCompletedRequest(Change request without record type and Operation type as Update)
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
    private static testMethod void testcommitCompletedRequest1(){
        init();
        Test.startTest();
        List<EP_ChangeRequest__c> changeRequestList = [Select Id, EP_Request_Status__c, EP_IS_Address_CR__c, EP_Record_Type__c, EP_Object_Type__c, 
                             EP_Product_List__c from EP_ChangeRequest__c where EP_Request_Status__c = 'Open' limit 1];
            
        Pricebook2 priceBook = new Pricebook2(Name = 'test Book');
        insert new List<Pricebook2>{priceBook};
        
        Product2 prdt = new product2(name = 'Test Product');
        insert new List<Product2>{prdt};
        
        List<PriceBookEntry> pEntryList = new List<PriceBookEntry>();
        pEntryList.add(new PriceBookEntry(Pricebook2Id = priceBook.Id, UnitPrice = 10, Product2Id = prdt.Id));
        insert pEntryList;
        
        List<EP_ChangeRequestLine__c> changeRequestLineList = new List<EP_ChangeRequestLine__c>();
        changeRequestLineList.add(new EP_ChangeRequestLine__c(EP_Change_Request__c = changeRequestList[0].Id,
                                                                               EP_Do_not_commit_changes__c = false,
                                                                               EP_Request_Status__c = EP_Common_Constant.CRLITEM_STATUS_OPEN ,
        
                                                                              EP_Product_Name__c = prdt.Id));
        insert changeRequestLineList;
        
        changeRequestList[0].EP_Request_Status__c = EP_Common_Constant.COMPLETED;
        changeRequestList[0].EP_IS_Address_CR__c = true;
        changeRequestList[0].EP_Record_Type__c = 'EP_VMI_Ship_To';
        changeRequestList[0].EP_Object_Type__c = 'account';     
        changeRequestList[0].EP_Product_List__c = priceBook.Id;  
         
        EP_ChangeRequestTriggerHandler.isExecuteAfterUpdate= false;
        update changeRequestList;
        
        Test.stopTest();
    }
    
    /*********************************************************************************************
    *@Description : This method valids commitCompletedRequest(Change request without record type and 
                    Operation type as NEW)
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
    private static testMethod void testcommitCompletedRequest2(){
        init();
        Test.startTest();
        List<EP_ChangeRequest__c> changeRequestList = [Select Id, EP_Request_Status__c, EP_IS_Address_CR__c, EP_Record_Type__c, EP_Object_Type__c, 
                             EP_Product_List__c from EP_ChangeRequest__c where EP_Request_Status__c = 'Open' limit 1];
            
        Pricebook2 priceBook = new Pricebook2(Name = 'test Book');
        insert new List<Pricebook2>{priceBook};
        
        Product2 prdt = new product2(name = 'Test Product');
        insert new List<Product2>{prdt};
        
        List<PriceBookEntry> pEntryList = new List<PriceBookEntry>();
        pEntryList.add(new PriceBookEntry(Pricebook2Id = priceBook.Id, UnitPrice = 10, Product2Id = prdt.Id));
        insert pEntryList;
        
        List<EP_ChangeRequestLine__c> changeRequestLineList = new List<EP_ChangeRequestLine__c>();
        changeRequestLineList.add(new EP_ChangeRequestLine__c(EP_Change_Request__c = changeRequestList[0].Id,
                                                                               EP_Do_not_commit_changes__c = false,
                                                                               EP_Request_Status__c = EP_Common_Constant.CRLITEM_STATUS_APPROVED,
                                                                               EP_Product_Name__c = prdt.Id,
                                                                               EP_Street__c = 'test Street'));
        insert changeRequestLineList;
        
        changeRequestList[0].EP_Request_Status__c = 'Completed';
        changeRequestList[0].EP_IS_Address_CR__c = true;
        changeRequestList[0].EP_Record_Type__c = 'EP_VMI_Ship_To';
        changeRequestList[0].EP_Object_Type__c = 'account';     
        changeRequestList[0].EP_Product_List__c = priceBook.Id; 
        changeRequestList[0].EP_Operation_Type__c = 'NEW';   
        EP_ChangeRequestTriggerHandler.isExecuteAfterUpdate= false;

        
        update changeRequestList;
        Test.stopTest();
    }
    
    /*********************************************************************************************
    *@Description : This method valids commitCompletedRequest(Change request without record type 
                    and Operation type as Update
                    and change request line has EP_Reference_Value__c 
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
    private static testMethod void testcommitCompletedRequest3(){
        init();
        Test.startTest();
        List<EP_ChangeRequest__c> changeRequestList = [Select Id, EP_Request_Status__c, EP_IS_Address_CR__c, EP_Record_Type__c, EP_Object_Type__c, 
                             EP_Product_List__c from EP_ChangeRequest__c where EP_Request_Status__c = 'Open' limit 1];
            
        Pricebook2 priceBook = new Pricebook2(Name = 'test Book');
        insert new List<Pricebook2>{priceBook};
        
        Product2 prdt = new product2(name = 'Test Product');
        insert new List<Product2>{prdt};
        
        List<PriceBookEntry> pEntryList = new List<PriceBookEntry>();
        pEntryList.add(new PriceBookEntry(Pricebook2Id = priceBook.Id, UnitPrice = 10, Product2Id = prdt.Id));
        insert pEntryList;
        
        List<EP_ChangeRequestLine__c> changeRequestLineList = new List<EP_ChangeRequestLine__c>();
        changeRequestLineList.add(new EP_ChangeRequestLine__c(EP_Change_Request__c = changeRequestList[0].Id,
                                                                               EP_Do_not_commit_changes__c = false,
                                                                               EP_Request_Status__c = EP_Common_Constant.CRLITEM_STATUS_APPROVED,
                                                                               EP_Reference_Value__c = 'testProduct',
                                                                               EP_Product_Name__c = prdt.Id));
        insert changeRequestLineList;
        
        changeRequestList[0].EP_Request_Status__c = 'Completed';
        changeRequestList[0].EP_IS_Address_CR__c = true;
        changeRequestList[0].EP_Record_Type__c = 'EP_VMI_Ship_To';
        changeRequestList[0].EP_Object_Type__c = 'account';     
        changeRequestList[0].EP_Product_List__c = priceBook.Id;    
        EP_ChangeRequestTriggerHandler.isExecuteAfterUpdate= false;
    
        update changeRequestList;
        Test.stopTest();
    }
    
    /*********************************************************************************************
    *@Description : This method valids commitCompletedRequest(Change request with record type)
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
    private static testMethod void testcommitCompletedRequest4(){
        init();
        Test.startTest();
        List<EP_ChangeRequest__c> changeRequestList = [Select Id, EP_Request_Status__c, EP_IS_Address_CR__c, EP_Record_Type__c, EP_Object_Type__c, 
                             EP_Product_List__c from EP_ChangeRequest__c where EP_Request_Status__c = 'Open' limit 1];
        
        Pricebook2 priceBook = new Pricebook2(Name = 'test Book');
        insert priceBook;
        
        Product2 prdt = new product2(name = 'Test Product');
        insert prdt;
        
        PriceBookEntry pEntry = new PriceBookEntry();
        pEntry.Pricebook2Id = priceBook.Id;
        pEntry.UnitPrice = 10;
        pEntry.Product2Id = prdt.Id;
        insert pEntry;
        
        
        List<EP_ChangeRequestLine__c> changeRequestLineList = new List<EP_ChangeRequestLine__c>();
        changeRequestLineList.add(new EP_ChangeRequestLine__c(EP_Change_Request__c = changeRequestList[0].Id,
                                                                               EP_Do_not_commit_changes__c = false,
                                                                               EP_Request_Status__c = EP_Common_Constant.CRLITEM_STATUS_APPROVED,
                                                                               EP_Product_Name__c = prdt.Id,
                                                                               EP_Street__c = 'test Street'));
        
        insert changeRequestLineList;
        
        
        
        RecordType recType = [Select id from recordtype where sObjectType = 'EP_ChangeRequest__c' and developerName = 'EP_Product_List_Change_Request'];
        
        changeRequestList[0].EP_Request_Status__c = 'Completed';
        changeRequestList[0].EP_IS_Address_CR__c = true;
        changeRequestList[0].EP_Record_Type__c = 'EP_VMI_Ship_To';
        changeRequestList[0].EP_Object_Type__c = 'account';     
        changeRequestList[0].EP_Product_List__c = priceBook.Id;
        changeRequestList[0].recordTypeId = recType.Id;    
        EP_ChangeRequestTriggerHandler.isExecuteAfterUpdate= false;
  
        update changeRequestList;
        
        Test.stopTest();
    }
    
    /*********************************************************************************************
    *@Description : This method valids commitCompletedRequest(Change request with record type)
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
   private static testMethod void testcommitCompletedRequest5(){
        init();
        Test.startTest();
        List<EP_ChangeRequest__c> changeRequestList = [Select Id, EP_Request_Status__c, EP_IS_Address_CR__c, EP_Record_Type__c, EP_Object_Type__c, 
                             EP_Product_List__c from EP_ChangeRequest__c where EP_Request_Status__c = 'Open' limit 1];
        
        Pricebook2 priceBook = new Pricebook2(Name = 'test Book');
        insert new List<Pricebook2>{priceBook};
        
        Product2 prdt = new product2(name = 'Test Product');
        Product2 prdt2 = new product2(name = 'Test Product2');
        insert new List<Product2>{prdt, prdt2};
        
        List<PriceBookEntry> pEntryList = new List<PriceBookEntry>();
        pEntryList.add(new PriceBookEntry(Pricebook2Id = priceBook.Id, UnitPrice = 10, Product2Id = prdt.Id));
        insert pEntryList;
        
        
        List<EP_ChangeRequestLine__c> changeRequestLineList = new List<EP_ChangeRequestLine__c>();
        changeRequestLineList.add(new EP_ChangeRequestLine__c(EP_Change_Request__c = changeRequestList[0].Id,
                                                                               EP_Do_not_commit_changes__c = false,
                                                                               EP_Request_Status__c = EP_Common_Constant.CRLITEM_STATUS_APPROVED,
                                                                               EP_Product_Name__c = prdt.Id,
                                                                               EP_Street__c = 'test Street'));
        changeRequestLineList.add(new EP_ChangeRequestLine__c(EP_Change_Request__c = changeRequestList[0].Id,
                                                                               EP_Do_not_commit_changes__c = false,
                                                                               EP_Request_Status__c = EP_Common_Constant.CRLITEM_STATUS_APPROVED,
                                                                               EP_Product_Name__c = prdt2.Id,
                                                                               EP_Street__c = 'test Street'));
        insert changeRequestLineList;
        
        
        
        RecordType recType = [Select id from recordtype where sObjectType = 'EP_ChangeRequest__c' and developerName = 'EP_Product_List_Change_Request'];
        
        changeRequestList[0].EP_Request_Status__c = 'Completed';
        changeRequestList[0].EP_IS_Address_CR__c = true;
        changeRequestList[0].EP_Record_Type__c = 'EP_VMI_Ship_To';
        changeRequestList[0].EP_Object_Type__c = 'account';     
        changeRequestList[0].EP_Product_List__c = priceBook.Id;
        changeRequestList[0].recordTypeId = recType.Id;     
        EP_ChangeRequestTriggerHandler.isExecuteAfterUpdate= false;
 
        update changeRequestList;
        
        Test.stopTest();
    }
}