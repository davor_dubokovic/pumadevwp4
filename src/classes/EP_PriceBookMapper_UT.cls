@isTest
public class EP_PriceBookMapper_UT
{
    static testMethod void getRecordsByIds_test() {
        EP_PriceBookMapper localObj = new EP_PriceBookMapper();
         // **** IMPLEMENT THIS SECTION ~@~ *****
        Pricebook2 PriceBook = EP_TestDataUtility.createPricebook2( True);      
        Set<id> idSet = new Set<Id>();
        idSet.add(PriceBook.id);   
         // **** TILL HERE ~@~ *****      
        Test.startTest();
        List<Pricebook2 > result = localObj.getRecordsByIds(idSet);
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
         System.AssertEquals(1,result.size());
        // **** TILL HERE ~@~ *****

    }
    
    static testMethod void getNumberOfAccountRelatedToPricebook_test() {
        EP_PriceBookMapper localObj = new EP_PriceBookMapper();
        // **** IMPLEMENT THIS SECTION ~@~ *****
        Pricebook2 PriceBook = EP_TestDataUtility.createPricebook2(True);        
        Set<id> idSet = new Set<Id>{PriceBook.id};         
       // Account acc =  EP_AccountTestDataUtility.createSellToAccountWithActiveStatus(1, true)[0];
       Account acc = EP_TestDataUtility.getSellTo();
        Set<id> AccIdSet = new Set<Id>{acc.id};    
        // **** TILL HERE ~@~ *****          
        Test.startTest();
        Integer result = localObj.getNumberOfAccountRelatedToPricebook(idSet,AccIdSet);
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        System.AssertEquals(0,result);
        // **** TILL HERE ~@~ *****
    }
}