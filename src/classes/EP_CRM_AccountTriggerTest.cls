/* ================================================
 * @Class Name : EP_CRM_AccountTriggerTest
 * @author : Kamendra Singh
 * @Purpose: This class is used to test EP_CRM_AccountTrigger apex trigger.
 * @created date: 28/07/2016
 ================================================*/
@isTest
private class EP_CRM_AccountTriggerTest{

    /* This method is used to create all test data.
    * @param : 
    * @return: 
    */  
    @testsetup
    public static  void testLoadData(){
        try{
            //Run as Sales User
            Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' LIMIT 1]; 
            User u=new User();
            u.firstName='Test First';
            u.lastname='Sales CRM';
            u.profileid=p.id;
            u.Alias = 'salesman';
            u.Email='standardusercrm@accenture.com'; 
            u.EmailEncodingKey='UTF-8';
            u.LanguageLocaleKey='en_US'; 
            u.LocaleSidKey='en_AU';
            u.TimeZoneSidKey='Australia/Brisbane';
            u.UserName='standardusercrm@accenture.com';
            u.CurrencyIsoCode='AUD';
            u.EP_CRM_Geo1__c = 'Africa';
            u.EP_CRM_Geo2__c = 'Ghana';
            
            Database.SaveResult saveUser = Database.insert(u);
            System.runAs(u){
            //Insert Payment Method....
            EP_Payment_Method__c objTempPayMeth = new EP_Payment_Method__c();
            objTempPayMeth.Name = 'Test payment method';
            objTempPayMeth.EP_Payment_Method_Code__c = '001ABC';
            
            Database.SaveResult savePaymentMeth = Database.insert(objTempPayMeth);
            
            //Insert Payment Term......
            EP_Payment_Term__c objTempPayTerm = new EP_Payment_Term__c();
            objTempPayTerm.Name = 'Test payment method';
            objTempPayTerm.EP_Payment_Term_Code__c = '001ABC';
            
            Database.SaveResult savePaymentTerm = Database.insert(objTempPayTerm);
            
            // Insert Account
            Account acc = new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';
            acc.EP_Payment_Method__c = objTempPayMeth.id;
            acc.EP_Payment_Term_Lookup__c = objTempPayTerm.id;
            acc.EP_Delivery_Type__c = 'Ex-Rack';
            acc.EP_Billing_Method__c = 'Per Order';
            acc.EP_Billing_Frequency__c = 'Daily';    
            acc.EP_CRM_Geo1__c = 'Africa';    
            acc.EP_CRM_Geo2__c = 'Ghana';    
            
            Database.SaveResult saveAcc = Database.insert(acc);
            ConnectionHelper.getConnectionOwnerId('Puma Energy');
            //Insert Opportunity
            Opportunity opp=new Opportunity();
            opp.name='Test';
            opp.Stagename='Prospecting';
            opp.Accountid =acc.id;
            //opp.EP_CRM_Credit_Approval_Received__c = true;
            opp.CloseDate=Date.Today() + 5;
            opp.Description = 'Test Description';
            opp.Probability = 5;
            opp.LeadSource = 'Phone';
            opp.EP_CRM_Region__c = 'AU';
            opp.EP_CRM_State__c = 'ACT';
            opp.EP_CRM_Geo1__c = 'Africa';    
            opp.EP_CRM_Geo2__c = 'Ghana';    
            
            Database.SaveResult saveOpp = Database.insert(opp);
            }
        }
        catch(exception ex){ex.getmessage();}
    }
     /* This method is used to create all test data and test after insert Account update it's all opportunity scenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testAccountInsertUpdateTrigger(){
        try{
            /*user tempUser = [select id from User where UserName='standardusercrm@accenture.com' limit 1];
             System.runAs(tempUser){*/
                Account tempAcc = [select id,EP_Payment_Method__c,EP_Billing_Frequency__c,EP_Payment_Term_Lookup__c,EP_Delivery_Type__c,EP_Billing_Method__c from Account where name='Test Accenture' limit 1];
            
                //update Account ..........
                tempAcc.EP_Delivery_Type__c = 'Delivery';
                tempAcc.EP_Billing_Frequency__c = 'Daily';                 
                tempAcc.EP_Billing_Method__c = 'Periodic';
                
                Database.SaveResult updateAcc = Database.update(tempAcc);
            // }
         }
        catch(exception ex){ex.getmessage();}
    }
      //  Modified by Hasmukh Jain on 19-Feb as Method does not exist afterInsertAccount so replaced with correct  method afterInsertConnectionAccount
      public static testMethod void testafterInsertAccount(){
        List<Account> acc;
        //EP_CRM_AccountTriggerHandler.afterInsertAccount(acc);
        EP_CRM_AccountTriggerHandler.afterInsertConnectionAccount(acc);
        
    }  
}