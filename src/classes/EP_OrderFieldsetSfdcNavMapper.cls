/**
   @Author          CR Team
   @name            EP_OrderFieldsetSfdcNavMapper
   @CreateDate      12/20/2016
   @Description     This class contains all SOQLs related to EP_OrderFieldsetSfdcNavMapper Object
   @Version         1.0
   @reference       NA
*/

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
*/
public with sharing class EP_OrderFieldsetSfdcNavMapper{
    List<EP_Order_Fieldset_Sfdc_Nav_Intg__c> orderCSList = null;    
    
    /**
    *  Constructor. 
    *  @name            EP_OrderFieldsetSfdcNavMapper
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */ 
    public EP_OrderFieldsetSfdcNavMapper() {
        
    } 
     
    /** This method returns OrderFieldsetSfdcNav Records.
    *  @name            getOrderFieldsetSfdcNavRec
    *  @param           Integer 
    *  @return          list<EP_Order_Fieldset_Sfdc_Nav_Intg__c>
    *  @throws          NA
    */
    public list<EP_Order_Fieldset_Sfdc_Nav_Intg__c> getOrderFieldsetSfdcNavRec(Integer nRows) {
        if(orderCSList == null) {
            orderCSList = new List<EP_Order_Fieldset_Sfdc_Nav_Intg__c> (); 
            for(list<EP_Order_Fieldset_Sfdc_Nav_Intg__c> orderCS : [Select e.Name,
                   e.Respective_Tag_Name__c,
                   e.Parent_Tag__c, e.Order_sequence__c,
                   e.Object_Name__c, 
                   e.Field_API_Name__c 
                   From EP_Order_Fieldset_Sfdc_Nav_Intg__c e 
                   ORDER BY Order_sequence__c ASC 
                   Limit : nRows
                   ]){
                    orderCSList .addAll(orderCS );
            }       
        }
        return orderCSList ;    
    }           
}