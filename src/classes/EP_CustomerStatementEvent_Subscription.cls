/**
  * @author       Accenture                                       
  * @name         EP_CustomerStatementEvent_Subscription                           
  * @Created Date 02/14/2018                                      
  * @description  Customer Statement Email Notification Class                   
*/
public with sharing class EP_CustomerStatementEvent_Subscription extends EP_Event_Subscription{
	public EP_CustomerStatementEvent_Subscription() {
		subscriptionEventObj.eventType = 'CUSTOMERSTATEMENT';
	}
}