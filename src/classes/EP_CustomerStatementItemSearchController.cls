/* 
   @Author <Vinay Jaiswal>
   @name <EP_CustomerStatementItemSearchController>
   @CreateDate <07/06/2016>
   @Description <This Class is controlled by EP_Customer_Statement_Item_Search_Page page>
   @Version <1.0>
 
*/
public with sharing class EP_CustomerStatementItemSearchController {
    public static final String OBJ_ID = 'id';
    private Boolean isError {get;set;}
    public string sBillToName {get;set;}
    public string sBillToId {get;set;}
    public Date dateFrom {get;set;}
    public Date dateTo {get;set;}
    public Boolean isSearch{get;set;}
    public Boolean isSearchStatement{get;set;}
    public Boolean isSearchStatementItem{get;set;}
    private Boolean isValidate{get;set;}
    private list<Account> lstAccount {get;set;} 
    public list<EP_CustomerAccountStatementUtility.WrapperCustomerAccountStatementItem> lstWrapperCustAccStatementItem {get;set;}
    public list<EP_CustomerAccountStatementUtility.WrapperCustomerAccountStatement> lstWrapperCustAccStatement {get;set;}
    public static final String SFIND_BLANK_STATEMENT = 'The system was not able to calculate the opening/closing balance of this customer as no '+
                                                                        'Customer Account Statements have been found in Salesforce. This could be because NAV has not '+ 
                                                                        'generated statements for this customer yet. If you do believe that there should be account statements '+ 
                                                                        'available for the selected customer, please contact your system administrator'; 
    public static final String SAFTER_SELECTED_DATE = 'The selected Date From cannot be after the selected Date To';
    public static final String SFUTURE_SELECTED_DATE = 'The selected date range cannot be in the future';
    public static final String SSELECTED_DATE_RANGE = 'The selected date range cannot be longer than two months in total';
    public static final String SSPECIFY_DATE_FORMAT = 'Must specify a date range before searching';
    public static final String SEXECUTION_ERROR = 'An Error occurred while executing this request.';
    private static final string SEARCH_CUSTOMER_STATEMENT = 'searchCustomerStatement';
    private static final string CLASS_NAME = 'EP_CustomerStatementItemSearchController';
    /*
    **Constructor
    */
    public EP_CustomerStatementItemSearchController(){
        isError = false;
        try{
            initialize();
        }
        catch (Exception e){
            isError = true;
            addPageMessages(ApexPages.Severity.Error,SEXECUTION_ERROR);
        }
    }
    
    /*
    **initialize method used by constructor
    */
    private void initialize(){
        isSearch = false;
        isSearchStatement = false; 
        isSearchStatementItem = false; 
        Date myDate = system.today();      
        sBillToName = EP_Common_Constant.Blank;
        String objId = Apexpages.currentpage().getparameters().get(OBJ_ID);
        if(!isSearchStatement){
            dateFrom = myDate.toStartOfMonth();
            dateTo = system.Today();
        }
        if (objId != null && string.isNotBlank(objId)){
            lstAccount = [select Id, Name from Account where ID =:objId limit: EP_Common_Constant.ONE ];
            if(!lstAccount.isEmpty()){
               sBillToName = lstAccount[0].Name;
               sBillToId = lstAccount[0].Id;
            }                                  
        }
    }
    
    /*
    **searchCustomerStatement controlled by apex page 'Search' Button
    */    
    public void searchCustomerStatement(){
        try{
        isSearchStatement = true;
        isSearchStatementItem = true;
        String objId = Apexpages.currentpage().getparameters().get(OBJ_ID); 
        Double dLastClosingBalance = 0.0;
        Double dCurrentOpeningBalance = 0.0;
        Double dCurrentClosingBalance = 0.0;
        Double dCurrentCreditsStatementItem = 0.0;
        Double dCurrentDebitsStatementItem =  0.0;
        Double dCurrentCreditsBeforeSelectedDate = 0.0;
        Double dCurrentDebitsBeforeSelectedDate =  0.0;
        lstWrapperCustAccStatement = new list<EP_CustomerAccountStatementUtility.WrapperCustomerAccountStatement>();
        lstWrapperCustAccStatementItem = new list<EP_CustomerAccountStatementUtility.WrapperCustomerAccountStatementItem>();
        Validate();
        if(objId <> null){
            dLastClosingBalance = EP_CustomerAccountStatementUtility.calculateClosingBalanceOfStatement(objId ,dateFrom);
            dCurrentCreditsBeforeSelectedDate = EP_CustomerAccountStatementUtility.totalCreditsBeforeSelectedDate(objId, dateFrom.toStartOfMonth(), dateFrom);          
            dCurrentDebitsBeforeSelectedDate = EP_CustomerAccountStatementUtility.totalDebitsBeforeSelectedDate(objId, dateFrom.toStartOfMonth(), dateFrom); 
            dCurrentOpeningBalance = EP_CustomerAccountStatementUtility.calculateOpeningBalanceBeforeSelectedDate();
            lstWrapperCustAccStatementItem = EP_CustomerAccountStatementUtility.getAccountStatementItemList(objId, dateFrom, dateTo); 
            dCurrentClosingBalance = EP_CustomerAccountStatementUtility.getCurrrentClosingBalance();            
            dCurrentCreditsStatementItem = EP_CustomerAccountStatementUtility.totalCreditsAccountStatementItem(objId, dateFrom, dateTo);          
            dCurrentDebitsStatementItem = EP_CustomerAccountStatementUtility.totalDebitsAccountStatementItem(objId, dateFrom, dateTo); 
              
            lstWrapperCustAccStatement = EP_CustomerAccountStatementUtility.getAccountStatementList(dCurrentOpeningBalance,dCurrentDebitsStatementItem ,dCurrentCreditsStatementItem, dCurrentClosingBalance);                       
            if(lstWrapperCustAccStatementItem.isEmpty() && lstWrapperCustAccStatement.isEmpty() && (isValidate == false)){
               isSearchStatementItem = false;
               isSearchStatement = false;
               addPageMessages(ApexPages.Severity.Error,SFIND_BLANK_STATEMENT);
            }
            else if(!lstWrapperCustAccStatementItem.isEmpty() ){
               isSearchStatementItem = true;
               isSearchStatement = true;
            }
            else if(!lstWrapperCustAccStatement.isEmpty()){
               isSearchStatementItem = false;
            }     
        }
        } catch(Exception e) {
            isSearchStatement = false;
            isSearchStatementItem = false;
            EP_LoggingService.logServiceException(e, UserInfo.getOrganizationId(), EP_Common_constant.EPUMA, SEARCH_CUSTOMER_STATEMENT, CLASS_NAME, EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF, EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);
            addPageMessages(ApexPages.Severity.Error, SEXECUTION_ERROR);
        }
    }

    /*
    **Validatation perform on date by Validate method
    */ 
    @TestVisible    
    private void Validate() {
        isValidate = false;
        try{
                 if(dateFrom > dateTo) {
                         isValidate = true;
                         isSearchStatement = false;
                         isSearchStatementItem = false;
                         addPageMessages(ApexPages.Severity.Error,SAFTER_SELECTED_DATE);
                 }
                 else if(dateFrom > system.today()){
                         isValidate = true;
                         isSearchStatement  = false;
                         isSearchStatementItem = false;
                     addPageMessages(ApexPages.Severity.Error,SFUTURE_SELECTED_DATE);
                 }
                 else if(dateTo > system.today()){
                          isValidate = true;
                          isSearchStatement  = false;
                          isSearchStatementItem = false;
                      addPageMessages(ApexPages.Severity.Error,SFUTURE_SELECTED_DATE);        
                 }
                 else if(dateFrom.daysBetween(dateTo) > 60){
                         isValidate = true;
                         isSearchStatement  = false;
                         isSearchStatementItem = false;
                         addPageMessages(ApexPages.Severity.Error,SSELECTED_DATE_RANGE);
                 }
                 else{
                        isSearchStatement  = true;
                        isSearchStatementItem = true;
                 }    
        }catch(Exception ex){
                isSearchStatement  = false;
                isSearchStatementItem = false;
                addPageMessages(ApexPages.Severity.Error,SSPECIFY_DATE_FORMAT);
        }     
    }

    /*
    **Error message 
    */         
    private void addPageMessages(ApexPages.Severity sevrty, string message){
        apexpages.Message msg = new Apexpages.Message(sevrty,message);
        Apexpages.addmessage(msg);
    }

}