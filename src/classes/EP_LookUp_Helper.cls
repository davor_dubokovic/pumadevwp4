/****************************************************************
* @author       Accenture                                       *
* @name         EP_LookUp_Helper                                *
* @Created Date 26/12/2017                                      *
* @description  Class to show LookUp Data                       *
****************************************************************/
public with sharing class EP_LookUp_Helper {
    private EP_LookUp_Context ctx;
    private static final string searchFilter = ' and name like \'%';
    private static final string limitFilter = ' Limit 1000';
    private  static final string LIKE_STRING = '%\'';

/****************************************************************
* @author       Accenture                                       *
* @name         EP_LookUp_Helper                                *
* @description  Constructor                                     *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public EP_LookUp_Helper(EP_LookUp_Context ctx) {
        this.ctx = ctx;
        lookUpRecords();
    }

/****************************************************************
* @author       Accenture                                       *
* @name         hideClearSearchButton                           *
* @description  method to hide clear search button              *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public void hideClearSearchButton(){
        EP_GeneralUtility.Log('public','EP_LookUp_Helper','hideClearSearchButton');
        ctx.clearSearchButton = false;
        ctx.searchText = null;
        lookUpRecords();
    }

/****************************************************************
* @author       Accenture                                       *
* @name         lookUpRecords                                   *
* @description  method to get data from system                  *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public void lookUpRecords(){
        EP_GeneralUtility.Log('public','EP_LookUp_Helper','lookUpRecords');
        ctx.SobjectList = new List<Sobject>();
        system.debug('ctx.objectVariable = '+ctx.objectVariable);
        ctx.queryData = EP_Look_Up__c.getValues(ctx.objectVariable);
        string queryString = ctx.queryData.EP_Fields__c + EP_Common_Constant.SPACE + ctx.queryData.EP_Where_Clause__c
                                        + EP_Common_Constant.QUERYQUOTEOPERATOR + ctx.IdVariable+ EP_Common_Constant.QUERYQUOTEOPERATOR;
        If(!String.isBlank(ctx.searchText)){
            ctx.clearSearchButton = true;
            queryString+= searchFilter +  String.escapeSingleQuotes(ctx.searchText) + LIKE_STRING;
        }
        queryString+= limitFilter;
        system.debug('queryString = '+queryString);
        ctx.SobjectList = Database.query(queryString);
        system.debug('ctx.SobjectList = '+ctx.SobjectList);
    }
}