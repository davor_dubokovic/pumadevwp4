public class bulkOrderQueryMapper{
/*
    set<id> queryIds;
    set<id> orderIdSet;
    set<id> accountIdSet;
    set<id> stockHoldingLocationIdSet;
    set<id> pricebookEntryIdSet;
    set<id> inventoryIdSet;
    set<id> tankIdSet;
    set<id> actionIdSet;
    set<id> productsIdSet;
    Integer limitVal = 25;
    
    public static map<id,Order> orderMap;
    public static map<id,OrderItem> orderitemMap;
    public static map<id,Account> accountMap;
    public static map<id,PricebookEntry> pricebookentryMap;
    public static map<id,EP_Stock_Holding_Location__c> stockHoldingMap;
    
    // Inventory details are available in orderitem
    public static map<id,String> inventoryMap;
    
    public static map<id,EP_Tank__c> tankMap;
    public static map<id,EP_Action__c> actionMap;    
    public static map<id,Product2> productMap;  
         
    
    public static Map<ID,Schema.RecordTypeInfo> orderRecordtypeMap = Order.sObjectType.getDescribe().getRecordTypeInfosById();
    public static Map<ID,Schema.RecordTypeInfo> accountRecordtypeMap = Account.sObjectType.getDescribe().getRecordTypeInfosById();
    
    
    public String orderqueryStr = 'SELECT AccountId, ActivatedById, ActivatedDate, BillToContactId, BillingAddress, ' +
                            'BillingCity, BillingCountry, BillingGeocodeAccuracy,BillingLatitude, BillingLongitude, ' +
                            'BillingPostalCode, BillingState, BillingStreet, CompanyAuthorizedById, ContractId, CreatedDate, ' +
                            'CurrencyIsoCode, EP_3rd_Party_Managed_Exported__c, EP_Action__c, EP_Action_for_Non_Delivery__c, ' +
                            'EP_Actual_Delivery_Date_Time__c, EP_Bill_To_Account__c, EP_Bill_To_Email__c, EP_Bill_To__c,'+
                            'EP_Billing_Basis__c, EP_Bulk_Order_Number__c, EP_Calculate_Price__c, EP_Confirmed_Pickup_Window__c, ' +
                            'EP_Credit_Status__c, EP_Customer_Local_Number__c, EP_Customer_PO_Number__c, EP_Customer_Reference_Number__c, '+
                            'EP_Delivery_Date__c, EP_Delivery_Type__c, EP_Destination__c, EP_Duty__c, EP_Email__c, EP_Error_Description__c, '+
                            'EP_Estimated_Delivery_Date_Time__c, EP_Estimated_Time_Range__c, EP_Ex_Rack_Order_Validity_Date__c, ' +
                            'EP_Exception_Number__c, EP_Expected_Delivery_Date__c, EP_Expected_Loading_Date__c, EP_Freight_Only__c, '+ 
                            'EP_Haulier__c, EP_Imported_Order_Number__c, EP_Integration_Action__c, EP_Integration_Status__c, '+
                            'EP_Is_Imported_Order__c, EP_Is_Order_Active__c, EP_Is_Portal_Order_Active__c, EP_Is_Pre_Pay__c, '+
                            'EP_Is_Retrospective__c, EP_Is_VMI_Suggestion_Order__c, EP_Last_Modified_By__c, EP_Line_Item_Count__c, '+
                            'EP_Loading_Date__c, EP_Modified_By__c, EP_NAV_Pricing_Stock_Holding_Location_ID__c, EP_Nav_Stock_Holding_Location_ID__c, '+
                            'EP_OnRunOrder__c, EP_OrderStatusCheckbox__c, EP_Order_Category__c, EP_Order_Comments__c, EP_Order_Credit_Status__c, ' +
                            'EP_Order_Customer_Status__c, EP_Order_Date__c, EP_Order_Delivery_Type__c, EP_Order_Epoch__c, EP_Order_Exported__c, ' +
                            'EP_Order_Load_Code__c, EP_Order_Price_Valid_Until__c, EP_Order_Product_Category__c, EP_Order_Push_From_WinDms__c, '+
                            'EP_Order_Record_Type__c, EP_Order_Status_Caption__c, EP_Order_Type__c, EP_Order_With_Alert_Manual__c, EP_Order_With_Alert__c, '+
                            'EP_Other_Reason__c, EP_Overdue_Amount__c, EP_Packaged_Order_Number__c, EP_Payment_Method_Code__c, EP_Payment_Method__c, '+
                            'EP_Payment_Term_Value_Code__c, EP_Payment_Term_Value__c, EP_Payment_Term__c, EP_Planned_Delivery_Date_Time__c, '+
                            'EP_Price_Consolidation_Basis__c, EP_Price_Type__c, EP_Pricing_Location_Id_NAV_XML__c, EP_Pricing_Request_Source__c, '+
                            'EP_Pricing_Status__c, EP_Puma_Company_Code__c, EP_RO_Delivery_Date__c, EP_Reason_For_Change__c, EP_Reason_For_Non_Delivery__c, '+
                            'EP_Region__c, EP_Related_Bulk_Order__c, EP_Requested_Delivery_Date_Windms__c,EP_Requested_Delivery_Date__c,'+
                            'EP_Requested_Delivery_End_Date__c,EP_Requested_Delivery_Pick_Up_Date_Time__c, EP_Requested_Pickup_Time__c, '+
                            'EP_Requested_Pickup_Window__c, EP_Route__c, EP_Run_Id__c, EP_Run__c, EP_Scheduler_Notes__c, EP_Sell_To_Id__c, '+
                            'EP_Sell_To__c, EP_SeqId__c, EP_ShipTo_Id_WinDMS_XML__c, EP_ShipTo__c, EP_Ship_To_ID__c, EP_Shipto_Storage_Location__c, '+
                            'EP_Slotting_Enabled__c, EP_Source_Interface__c, EP_Stock_Holding_Location_NAV_XML__c, EP_Stock_Holding_Location__c, '+
                            'EP_Supply_Location_Name__c, EP_Supply_Location_Pricing__c, EP_Sync_With_LS__c,EP_Sync_with_NAV__c, EP_Total_Order_Quantity__c, '+
                            'EP_Total_Price__c, EP_Total_Updated_Amount__c, EP_Track_Status_Planned__c, EP_Transport_Management__c, '+
                            'EP_Transportation_Code__c, EP_Transporter_Code__c, EP_Transporter_Pricing_No__c, EP_Transporter_Pricing__c, '+
                            'EP_Transporter__c, EP_Urgent_Order__c, EP_Use_Managed_Transport_Services__c, EP_WinDMS_Order_Number__c, '+
                            'EP_WinDMS_Order_Version_Number__c, EP_WinDMS_Status__c, EP_WinDMS_Trip_Reference_Number__c, EffectiveDate, '+
                            'EndDate, Id, IsReductionOrder, OrderNumber, OrderReferenceNumber, Pricebook2Id, RecordTypeId, '+
                            'Requested_Pickup_Time__c, ShipToContactId, ShippingAddress, ShippingCity, ShippingCountry, '+
                            'ShippingGeocodeAccuracy, ShippingLatitude, ShippingLongitude, ShippingPostalCode, ShippingState, ShippingStreet, '+
                            'Status, StatusCode, Stock_Holding_Location__c, SystemModstamp, TotalAmount, Type, EP_ShipTo__r.EP_Ship_To_Type__c, '+
                            'EP_ShipTo__r.RecordTypeId '+
                            ',(SELECT Id , orderId, EP_Pricing_Total_Amount__c , EP_Tax_Amount__c, EP_Tax_Percentage__c , EP_Is_Standard__c, '+
                            'PriceBookEntryId , Quantity, PriceBookEntry.Product2.Name , UnitPrice, PriceBookEntry.Product2.EP_Unit_Of_Measure__c ,'+
                            'EP_Unit_of_Measure__c, EP_Tank_Code__c , EP_Tank__r.EP_Tank_Alias__c, EP_Total_Price__c , EP_Invoice_Name__c, ' +
                            'EP_Prior_Quantity__c , EP_Quantity_UOM__c, EP_Stock_Holding_Location__c , EP_Tank__c, EP_Tank_Number__c , EP_Unit__c, '+
                            'EP_Is_Freight_Price__c, EP_Parent_Order_Line_Item__c , EP_Is_Taxes__c, ListPrice FROM OrderItems)'+ 
                            ' FROM Order';

    public bulkOrderQueryMapper(){
        // initialization of objects
        queryIds = new set<id>();
        orderIdSet = new set<id>();
        accountIdSet = new set<id>();
        stockHoldingLocationIdSet = new set<id>();
        pricebookEntryIdSet = new set<id>();
        inventoryIdSet = new set<id>();
        tankIdSet = new set<id>();
        actionIdSet = new set<id>();
        productsIdSet = new set<id>();
        
    }
    
    // take orders
    public boolean loadBulkMapperByOrder(set<id> OrderIds){
        EP_GeneralUtility.Log('Public','bulkOrderQueryMapper','loadBulkMapperByOrder');
        accountIdSet = new set<id>();
        
        try{
            setOrderObjectMapper(OrderIds);
            setOrderLineItemsMap(OrderIds);
            boolean isNull = false;
            
            for(Order ord: orderMap.values()){
                isNull = ord.EP_Sell_To__c != null ? AccountIdSet.add(ord.EP_Sell_To__c): true;
                isNull = ord.EP_Bill_To__c != null ? AccountIdSet.add(ord.EP_Bill_To__c): true;
                isNull = ord.EP_ShipTo__c != null ? AccountIdSet.add(ord.EP_ShipTo__c): true;
                stockHoldingLocationIdSet.add(ord.Stock_Holding_Location__c);
            }
            
            for(OrderItem ordItem: orderitemMap.values()){
                pricebookEntryIdSet.add(ordItem.PricebookEntryId);
                tankIdSet.add(ordItem.EP_Tank__c);
                productsIdSet.add(ordItem.Product2Id);
            }

            
            setAccountRecordsMap(AccountIdSet);
            setStockHoldingLocation(stockHoldingLocationIdSet);
            setpriceBookEntry(pricebookEntryIdSet);
            setProducts(productsIdSet);
            setTank(tankIdSet);
            
            return true;
        }catch(Exception e){
            system.debug('Error in initiating initiateBulkMapperByOrder of bulkOrderQueryMapper');
            system.debug(e);
            return false;
        }
        
    }
    
    
    // returns order by passing order id
public Order getOrderById(Id orderid){
EP_GeneralUtility.Log('Public','bulkOrderQueryMapper','getOrderById');
        return orderMap.get(orderid);
    }
    
    // returns orderitems by passing order
public List<OrderItem> getOrderItemsByOrder(Order ord){
EP_GeneralUtility.Log('Public','bulkOrderQueryMapper','getOrderItemsByOrder');
        List<OrderItem> ordlist = new list<OrderItem>();
        for(OrderItem oi: ord.orderitems){
            ordlist.add(orderitemMap.get(oi.id));
        }
        return ordlist;
    }
    
    // returns account by have account id
public Account getAccountById(Id accountid){
EP_GeneralUtility.Log('Public','bulkOrderQueryMapper','getAccountById');
        return accountMap.get(accountid);
    }
    
    // returns Ship to account of an order
public Account getShipToAccountById(Order ord){
EP_GeneralUtility.Log('Public','bulkOrderQueryMapper','getShipToAccountById');
        return accountMap.get(ord.EP_Ship_To_ID__c);
    }
    
    // returns sell to account of an order
public Account getSellToAccountById(Order ord){
EP_GeneralUtility.Log('Public','bulkOrderQueryMapper','getSellToAccountById');
        return accountMap.get(ord.EP_Sell_To_Id__c);
    }
    
    // returns bill to account of an order
public Account getBillToAccountById(Order ord){
EP_GeneralUtility.Log('Public','bulkOrderQueryMapper','getBillToAccountById');
        return accountMap.get(ord.EP_Bill_To__c);
    }
    
    // gets stock holding location by id
public EP_Stock_Holding_Location__c getStockHoldingById(Id stockHoldingid){
EP_GeneralUtility.Log('Public','bulkOrderQueryMapper','getStockHoldingById');
        return stockHoldingMap.get(stockHoldingid);
    }
    
    // return stock holding location by passing order
public EP_Stock_Holding_Location__c getStockHoldingforOrder(Order ord){
EP_GeneralUtility.Log('Public','bulkOrderQueryMapper','getStockHoldingforOrder');
        return stockHoldingMap.get(ord.Stock_Holding_Location__c);      
    }
    
public boolean isInventoryLowByOrder(Order ord){
EP_GeneralUtility.Log('Public','bulkOrderQueryMapper','isInventoryLowByOrder');
        boolean islow = false;
        List<OrderItem> orderitems = getOrderItemsByOrder(ord);
        for(OrderItem oItem: orderitems){
            if(!oItem.EP_Inventory_Availability__c.equalsIgnorecase('Good')){
                islow = true;
                break;
            }

        }
        return islow;
    } 
    

public List<PriceBookEntry> getPriceBookEntryForOrder(Order ord){
EP_GeneralUtility.Log('Public','bulkOrderQueryMapper','getPriceBookEntryForOrder');
        List<PriceBookEntry> priceBookList = new List<PriceBookEntry>();
        for(OrderItem oItem: getOrderItemsByOrder(ord)){
            priceBookList.add(pricebookentryMap.get(oItem.PricebookEntryId));
        }
        return priceBookList;
    }
    
public EP_Tank__c getTankById(Id tankid){
EP_GeneralUtility.Log('Public','bulkOrderQueryMapper','getTankById');
        return tankMap.get(tankid);
    }
    
public List<EP_Tank__c> getTankById(Order ord){
EP_GeneralUtility.Log('Public','bulkOrderQueryMapper','getTankById');
        List<EP_Tank__c> tankList = new List<EP_Tank__c>();
        for(OrderItem oItem: getOrderItemsByOrder(ord)){
            tankList.add(tankMap.get(oItem.EP_Tank__C));
        }
        return tankList;
    }
    
  /*  public static EP_Action__c getActionById(Id actionid){
        return actionMap.get(actionid);
    } 
    
public Product2 getProductById(Id productid){
EP_GeneralUtility.Log('Public','bulkOrderQueryMapper','getProductById');
        return productMap.get(productid);
    }
    
public List<Product2> getProductsByOrder(Order ord){
EP_GeneralUtility.Log('Public','bulkOrderQueryMapper','getProductsByOrder');
        List<Product2> productList = new List<Product2>();
        for(OrderItem oItem: getOrderItemsByOrder(ord)){
            productList.add(productMap.get(oItem.EP_Product__C));
        }
        return productList;
    }
    
    
    private void setOrderObjectMapper(set<id> orderIds){
        EP_GeneralUtility.Log('Private','bulkOrderQueryMapper','setOrderObjectMapper');
        if(!orderIds.isEmpty()){
            orderqueryStr = orderqueryStr + ' where id in :orderIds';
        }
        system.debug('*****orderqueryStr ' + orderqueryStr);
        orderMap = new map<id,Order>((List<Order>)Database.query(orderqueryStr));
    }
    
    
    private void setAccountRecordsMap(Set<id> acctIds){
        EP_GeneralUtility.Log('Private','bulkOrderQueryMapper','setAccountRecordsMap');
        /* TFS fix 45559,45560,45567,45568 start EP_NAV_ID__c deprecated, EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c and EP_Vendor_Type__c is replaced by EP_VendorType__c and EP_Vendor_Unique_Id__c replaced by EP_Enterprise_ID__c 

        accountMap = new map<id,Account>([SELECT Id, Name, Type, RecordTypeId, BillingStreet, BillingCity,
                                          BillingState, BillingPostalCode, BillingCountry, BillingLatitude, 
                                          BillingLongitude, BillingGeocodeAccuracy, BillingAddress, ShippingStreet, 
                                          ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, 
                                          ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy, 
                                          ShippingAddress, Phone, Fax, AccountNumber, AnnualRevenue, 
                                          NumberOfEmployees, TickerSymbol, Description, Rating, CurrencyIsoCode, 
                                          IsCustomerPortal, Jigsaw, JigsawCompanyId, AccountSource, SicDesc, 
                                          BSM_GM_Review_Required_On_Company__c, EP_Account_Company_Name__c, 
                                          EP_Account_Name_2__c, EP_Account_Name_3__c, EP_Account_Number__c, 
                                          EP_Account_Synced_With_winDms__c, EP_Account_Synced_with_NAV__c, 
                                          EP_Additional_Comments__c, EP_Allow_Only_Manual_Invoicing__c, 
                                           /*CAM 2.7 startEP_AvailableFunds__c /*CAM 2.7 end, EP_Available_Inventory__c, EP_Available_Slots__c, 
                                          EP_BSM_GM_Review_Completed__c, EP_BSM_GM_Review_Required__c, 
                                          EP_Bank_Name__c, EP_BillToCustomerNr__c, EP_Bill_To_Account__c, 
                                          EP_Billing_Basis__c, EP_Billing_Frequency__c, EP_Billing_Method__c, 
                                          EP_Block_Status__c, EP_Block_type__c, EP_Business_Hours__c, 
                                          EP_CSC_Review_Completed__c, EP_CSC_Review_Required__c, EP_Cntry_Region__c, 
                                          EP_Company_Is_Tax_Exempt__c, EP_Company_Tax_Number__c, EP_Country_Code__c, 
                                          EP_Country__c, EP_Credit_Limit_Approved__c, EP_Credit_Limit__c, 
                                          EP_Credit_Limit_updated_fom_NAV__c, EP_Credit_Status__c, 
                                          EP_Cstmr_Owner_Alias__c, EP_Currency__c, EP_Customer_Category__c, 
                                          EP_Default_Supply_Location__c, EP_Delivery_Pickup_Country__c, 
                                          EP_Delivery_Type__c, EP_Dispatch_Phone_Number__c, 
                                          EP_Driving_Instructions__c, EP_Eligible_for_Rebate__c, EP_Email__c, 
                                          EP_Freight_Matrix__c, EP_Fuel_Volume__c, EP_IBAN__c, 
                                          EP_Indicative_Total_Pur_Value_Per_Yr__c, EP_Integration_Status__c, 
                                          EP_Invoice_Consolidation_basis__c, EP_Is_Blocked_Required__c, 
                                          EP_Is_Customer_Reference_Mandatory__c, EP_Is_Dummy__c, EP_Is_Pre_Pay__c, 
                                          EP_Is_Ship_To_Open_Today__c, EP_Is_VMI_ShipTo__c, 
                                          EP_KYC_Review_Completed__c, EP_KYC_Review_Required__c, EP_Language_Code__c, 
                                          EP_Language__c, EP_Last_Modified_By_Name__c, EP_Logistics_Review_Completed__c, 
                                          EP_Lube_Volume__c, EP_ManualInvoicingAllowed_NAV__c, EP_Mobile_Phone__c, 
                                          EP_Nav_Stock_Location_Id__c, 
                                          EP_Number_of_Hoses__c, EP_Order_Frequency_Fuel__c, EP_Order_Frequency_Lube__c, 
                                          EP_Parent_Account_Email__c, EP_Parent_Customer_Nr__c, EP_Payment_Method_Code__c, 
                                          EP_Payment_Method__c, EP_Payment_Term_Lookup__c, EP_Payment_Term__c, 
                                          EP_Payment_Terms__c, EP_Payment_term_name__c, EP_Phone2__c, 
                                          EP_Position__Latitude__s, EP_Position__Longitude__s, EP_Position__c, 
                                          EP_PriceBook__c, EP_Products_Interested_In__c, EP_Puma_Company_Code__c, 
                                          EP_Puma_Company__c, EP_Pumps__c, EP_Reason_Blocked__c, EP_Rebate_Eligibity__c, 
                                          EP_Recommended_Credit_Limit__c, EP_RecordType_Code__c, EP_Record_Count_Number__c, 
                                          EP_Region__c, EP_Requested_Payment_Credit_Limit__c, EP_Requested_Payment_Method__c, 
                                          EP_Requested_Payment_Terms__c, EP_Scheduling_Hours__c, 
                                          EP_Send_Notification_Action_Completed__c, EP_Send_VMI_Compliance_Notification_1__c, 
                                          EP_Send_VMI_Compliance_Notification_2__c, EP_Send_VMI_Compliance_Notification_3__c, 
                                          EP_Send_VMI_Compliance_Notification_4__c, EP_Ship_To_Current_Date_Time__c, 
                                          EP_Ship_To_Number__c, EP_Ship_To_Opening_Days__c, EP_Ship_To_Type__c, 
                                          EP_Ship_To_UTC_Offset__c, EP_Ship_To_UTC_Timezone__c, EP_Site_Opening_Days__c, 
                                          EP_Site_Type__c, EP_Slotting_Enabled__c, EP_Source_Seq_Id__c, EP_Status__c, 
                                          EP_Stock_Holding_Location__c, EP_Storage_Location_ID__c, EP_Storage_Location__c, 
                                          EP_Tank_Dip_Entry_Mode__c, EP_Tank_Dips_Schedule_Time__c, EP_Third_Party_Vendor__c, 
                                          EP_Transportation_Management__c, EP_VAT_Exempted__c, EP_VendorType__c, 
                                          EP_Default_Transporter_at_Locations__c, EP_Number_of_Tanks__c, 
                                          EP_Tank_Dip_Missing_From_Days__c, EP_Total_Missing_Tanks__c, EP_Account_Id__c, 
                                          EP_Account_Statement_Periodicity__c, EP_Allow_Notifications__c, 
                                          EP_Allow_release_on_proof_of_payment__c, EP_Alternative_Payment_Method__c, 
                                          EP_BPAY_Biller_Reference__c, EP_Business_Channel_Code__c, EP_Business_Channel__c, 
                                          EP_Business_Segment_Code__c, EP_Business_Segment__c, EP_Combined_Invoicing__c, 
                                          EP_Composite_Id__c, EP_Customer_Activation_Date__c, EP_Customer_Of_Customer__c, 
                                          EP_Customer_PO_Number__c, EP_Customer_s_PO_number_required_by__c, EP_Defer_Invoice__c, 
                                          EP_Defer_Upper_Limit__c, EP_Display_Price__c, EP_Duty__c, 
                                          EP_Ex_Rack_Order_Validity_Period__c, EP_Excise_duty__c, 
                                          EP_Integration_Action__c, EP_Is_Customer_Reference_Visible__c, 
                                          EP_Is_Valid_Address__c, EP_KYC_Initiated__c, EP_Legacy_Id__c, 
                                          EP_NavSeqId__c, EP_Overdue_Amount__c, 
                                          EP_Package_Payment_Term__c, EP_Price_Consolidation_Basis__c, 
                                          EP_Provider_Managed_Transport_Services__c, EP_Requested_Packaged_Payment_Term__c, 
                                          EP_Show_Orders_in_Progress__c, EP_Sys_All_Items_Account_Statements__c, 
                                          EP_Sys_Open_Items_Account_Statements__c, EP_Type_of_Account_Statements__c, 
                                          EP_Use_Managed_Transport_Services__c, EP_VMI_Suggestion__c, EP_Valid_From_date__c, 
                                          EP_Valid_To_date__c ,EP_Enterprise_ID__c, EP_Number_of_Non_Decommissioned_Tanks__c, 
                                          EP_Allow_Same_Day_Pickup__c, EP_Order_By_Tank_Max_Capacity__c, 
                                          EP_Order_By_Tank_Enabled__c,EP_Source_Entity_ID__c, EP_CreditLimitRejected__c FROM Account where id in:acctIds]);
    }
    /* TFS fix 45559,45560,45567,45568 end
    
    private void setOrderLineItemsMap(set<id> orderidset){
        EP_GeneralUtility.Log('Private','bulkOrderQueryMapper','setOrderLineItemsMap');
        orderitemMap = new map<id,OrderItem>([SELECT OrderId, Id, Product2Id, PricebookEntryId, OriginalOrderItemId, 
                        AvailableQuantity, CurrencyIsoCode, Quantity, UnitPrice, ListPrice, EP_Accounting_Details__c, EP_ChargeType__c,
                        EP_Eligible_for_Rebate__c, EP_Integration_Status__c, EP_Inventory_Availability__c, EP_Invoice_Name__c, 
                        EP_Is_Freight_Price__c, EP_Is_Standard__c, EP_Is_Tank_Fill__c, EP_Is_Taxes__c, EP_Order_Line_Number__c, 
                        EP_Order_Line_Relationship_Index__c, EP_Order_Number__c, EP_Parent_Order_Line_item__c, 
                        EP_Pricing_Response_Unit_Price__c, EP_Pricing_Total_Amount__c, EP_Prior_Quantity__c, EP_Product_Code__c, 
                        EP_Product__c, EP_Quantity_UOM__c, EP_SeqId__c, EP_Stock_Holding_Location__c, EP_Stock_Location_Id__c, 
                        EP_Tank_Code__c, EP_Tank_Number__c, EP_Tank__c, EP_Tax_Amount__c, EP_Tax_Percentage__c, EP_Total_Price__c, 
                        EP_Unit__c, EP_Unit_of_Measure__c, EP_Updated_Amount__c, EP_WinDMS_Line_Item_Reference_Number__c, 
                        EP_3rd_Party_Stock_Supplier__c, EP_Ambient_Delivered_Quantity__c, EP_Ambient_Loaded_Quantity__c, EP_BOL_Number__c, 
                        EP_Contract_Nav_Id__c, EP_Contract__c, EP_Pricing_Error__c, EP_Standard_Delivered_Quantity__c, 
                        EP_Standard_Loaded_Quantity__c, EP_Supplier_Nav_Vendor_Id__c, EP_Stock_Location_Pricing__c, EP_Location_ID_NAV_XML__c, 
                        EP_Line_Id_NAV_XML__c, EP_Line_Id_WINDMS_XML__c, EP_Tax_Amount_XML__c, EP_WinDMS_Line_ItemId__c 
                        FROM OrderItem where OrderId in :orderidset]);
    }
    
    
    private void setPricebookEntryMap(set<id> pricebookentryid){
        pricebookentryMap = new map<id,PricebookEntry>([SELECT IsActive, UseStandardPrice, EP_Attached_to_Standard_Product_List__c, 
                            EP_Is_Sell_To_Assigned__c, UnitPrice, EP_Additional_Taxes__c, Id, EP_VAT_GST__c, CurrencyIsoCode, 
                            EP_Unit_Of_Measure__c, Pricebook2Id, Product2Id, Name, ProductCode FROM PricebookEntry]);
    }
    
private void setStockHoldingLocation(set<Id> stockholdinglocationid){
EP_GeneralUtility.Log('Private','bulkOrderQueryMapper','setStockHoldingLocation');
       stockHoldingMap = new Map<Id, EP_Stock_Holding_Location__c>([SELECT Id, Name, CurrencyIsoCode, 
                    RecordTypeId,EP_Client_Nr__c, EP_Default__c, EP_Distance__c, EP_Integration_Status__c, 
                    EP_Is_Pickup_Enabled__c, EP_Last_Modified_By_Name__c, EP_Location_Type__c, 
                    EP_Primany_Location__c, EP_Sell_To__c, EP_Ship_To_Id__c, EP_Ship_To__c, EP_Source_Seq_Id__c, 
                    EP_Status__c, EP_Stock_Holding_Location_Order__c, EP_Stock_Location_Id__c, EP_UOM__c, 
                    Stock_Holding_Location__c, EP_Default_Route__c, EP_Duty__c, EP_Purchase_Contract__c, 
                    EP_Supplier_Contract_Advice__c, EP_Supplier__c, EP_Transporter__c, EP_Trip_Duration__c, 
                    EP_Use_Managed_Transport_Services__c FROM EP_Stock_Holding_Location__c where id in :stockholdinglocationid and EP_Location_Type__c='Primary']);
    }
    
   /* private void setInventoryDetails(set<id> inventoryIdSet){
        inventoryMap = new Map<id, EP_Inventory__c>([SELECT Id, Name, CurrencyIsoCode, EP_Company__c,
                        EP_Inventory_Availability__c, EP_Product__c, EP_SKU_Unique_Key__c, 
                        EP_Source_Seq_Id__c, EP_Status__c, EP_Stock_Label__c, EP_Storage_Location__c, 
                        EP_Product_Code__c, EP_Supplier__c FROM EP_Inventory__c where id in :inventoryIdSet]);

    }
    
private void setpriceBookEntry(set<id> pricebookEntryIdSet){
EP_GeneralUtility.Log('Private','bulkOrderQueryMapper','setpriceBookEntry');
        priceBookEntryMap = new map<id,PriceBookEntry>([SELECT Id, Name, Pricebook2Id, Product2Id, CurrencyIsoCode, 
                            UnitPrice, IsActive, UseStandardPrice, ProductCode, IsDeleted, EP_Additional_Taxes__c, 
                            EP_Unit_Of_Measure__c, EP_VAT_GST__c, EP_Attached_to_Standard_Product_List__c, 
                            EP_Is_Sell_To_Assigned__c FROM PricebookEntry where id in :pricebookEntryIdSet]);
    }
    
private void setTank(set<id> tankIdSet){
EP_GeneralUtility.Log('Private','bulkOrderQueryMapper','setTank');
        tankMap = new map<id,EP_tank__C>([SELECT Id, Name, CurrencyIsoCode, RecordTypeId, EP_Ship_To__c, 
                            EP_Capacity__c, EP_Company_Name__c, EP_Deadstock__c, EP_Integration_Status__c, 
                            EP_Last_Dip_Reading__c, EP_Last_Dip_Ship_To_Date_Time__c, EP_Last_Modified_By_Name__c, 
                            EP_Last_Placeholder_Ship_To_Date_Time__c, EP_Last_Reading_Ship_To_Date_Time__c, 
                            EP_Left_Open__c, EP_Missing_Placeholder_Dip_For_Today__c, EP_Opening_Stock_Level__c, 
                            EP_Product_Code__c, EP_Product__c, EP_Reading_Date_Time__c, EP_Reason_Blocked__c, 
                            EP_Safe_Fill_Level__c, EP_Ship_To_Nr__c, EP_Stock_Owners__c, EP_Tank_Alias__c, 
                            EP_Tank_Code__c, EP_Tank_Dip_Entry_Mode__c, EP_Tank_Dip_Exported__c, EP_Tank_Lock_End_Date__c, 
                            EP_Tank_Missing_Dip_For_Today__c, EP_Tank_Soft_Book_Start_Date__c, EP_Tank_Status__c, 
                            /* #L4_45352_Start EP_UnitOfMeasure__c /* #L4_45352_END, EP_Last_Dip_Reading_Date__c, EP_Last_Place_Dip_Reading_Date_Time__c, 
                            EP_Missing_Tank_Dip_Nr__c, EP_Integration_Action__c, EP_Last_Tank_Dip__c, 
                            EP_Missing_Dips_From_Days__c FROM EP_Tank__c where id in :tankIdSet]);
    }
    
   /* private void setActions(set<id> actionIdSet){
        actionMap = new map<id,EP_Action__c>([SELECT Id, Name, CurrencyIsoCode, RecordTypeId, EP_Account__c, 
                            EP_Action_Name__c, EP_Bank_Account__c, EP_Comments__c, EP_Country__c, 
                            EP_Parent_Owner_Email__c, EP_Parent_Record_Owner__c, EP_Reason_No_Visit__c, 
                            EP_Reason__c, EP_Record_Type_Name__c, EP_Region__c, EP_Request_Id__c, 
                            EP_Site_Visit_Comments__c, EP_Site_Visit__c, EP_Status__c, EP_Tank__c, 
                            EP_System_Queue__c, EP_Supply_Option__c FROM EP_Action__c where id in :actionIdSet]);
    }
    
private void setProducts(set<id> productIdSet){
EP_GeneralUtility.Log('Private','bulkOrderQueryMapper','setProducts');
        productMap = new map<id,Product2>([SELECT Id, Name, ProductCode, Description, IsActive, Family, 
                            CurrencyIsoCode, ExternalDataSourceId, ExternalId, DisplayUrl, 
                            QuantityUnitOfMeasure, EP_Blended__c, EP_Company_Lookup__c, EP_Company__c, 
                            EP_Density__c, EP_Fuel_Conversion_Ratio__c, EP_Is_System_Product__c, 
                            EP_NAV_Product_Company__c, EP_Product_Capacity_Colour__c, EP_Product_Colour__c, 
                            EP_Short_Name__c, EP_Unit_of_Measure__c, Eligible_for_Rebate__c, 
                            EP_DG_Classification__c, EP_Product_Sold_As__c FROM Product2 where id in: productIdSet]);
    }
    */
}