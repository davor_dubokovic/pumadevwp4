/* 
@Author      Accenture
@name        EP_ShipTo
@CreateDate  02/27/2017
@Novasuite Fixes - Avoid unused method/trigger variables,Required Documentation
@Description This class executes common logic for VMI ship-To/Non VMI Ship TO Account
@Version     1.0
*/
public class EP_ShipTo extends EP_AccountType {

    EP_AccountMapper accountMapper = new EP_AccountMapper();
    EP_CountryMapper countryMapper = new EP_CountryMapper();
    EP_StockHoldingLocationMapper stockHoldingLocationMapper = new EP_StockHoldingLocationMapper();    
    EP_ActionMapper actionMapper = new EP_ActionMapper();
    
    /**
    * @author       Accenture
    * @name         setCurrency
    * @date         02/05/2017
    * @description  Method to set currency on Ship To from its parent Sell To
    * @param        Account
    * @return       NA
    */  
    @TestVisible
    private  void setCurrency(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','setCurrency');
        
        if(account.ParentId!= null){
            Account parentAccount = accountMapper.getAccountRecord(shipToAccount.ParentId);
            shipToAccount.CurrencyISOCode = parentAccount.CurrencyISOCode;
        } 
    }
	
    
    /**
    * @author       Accenture
    * @name         setCountry
    * @date         02/27/2017
    * @description  Method to update ship-To country based on EP_Country__c lookup Field
    * @param        Account
    * @return       NA
    */  
    public override void setCountry(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','setCountry');
        if(shipToAccount.EP_Country__c != null){
            EP_Country__c country = countryMapper.getCountryById(shipToAccount.EP_Country__c);
            If(country != null){
                shipToAccount.ShippingCountry = country.Name;
            }
        }          
    }
	
    
    /**
    * @author       Accenture
    * @name         setCompanyFromParent
    * @date         02/27/2017
    * @description  This method populates the Company on ship To from Parent Record. 
    * @param        Account
    * @return       NA
    */
    public override void setCompanyFromParent(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','setCompanyFromParent');
        if(shipToAccount.ParentId!=null){
            shipToAccount.EP_Puma_Company__c = getParentCompany(shipToAccount);
        }
    }
	
     /*****Defect Fix- 43911 START********/
    /**
    * @author       Accenture
    * @name         setOwnerfromParent
    * @date         04/20/2017
    * @description  This method populates the Owner on ship To from Parent Record. 
    * @param        Account
    * @return       NA
    */
    public override void setOwnerfromParent(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','setOwnerfromParent');
        if(shipToAccount.ParentId!=null){
            Account parentaccount = accountMapper.getAccountRecord(shipToAccount.ParentId);
            shipToAccount.OwnerId = parentaccount.OwnerId;
        }
    }
    /*****Defect Fix- 43911 END********/   
   
    /**
    * @author       Accenture
    * @name         getParentCompany
    * @date         02/27/2017
    * @description  This method is used to get company from Parent 
    * @param        Account
    * @return       Id
    */
    @TestVisible
    private Id getParentCompany(Account shipToAccount){
        EP_GeneralUtility.Log('Private','EP_ShipTo','getParentCompany');
        Account parentaccount = accountMapper.getAccountRecord(shipToAccount.ParentId);
        return parentaccount.EP_Puma_Company__c;
    }
    

    /**
    * @author       Accenture
    * @name         isPrimarySupplyOptionAttached
    * @date         02/27/2017
    * @description  Method to check if Ship to has primary supply location
    * @param        Account
    * @return       boolean
    */
    public override boolean isPrimarySupplyOptionAttached(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','isPrimarySupplyOptionAttached');
        boolean isSupplyOptionAttached = false;
        //Bulkification Implementation -- Start
        integer supplyOptionCount = accountDomainObj.stockHoldLocMapper.getCountOfPrimaryStockHoldingLocations(shipToAccount);
        //Bulkification Implementation -- End
        if(supplyOptionCount > 0){
            isSupplyOptionAttached = true;
        }
        return isSupplyOptionAttached;
    }
	 
     
    /**
    * @author       Accenture
    * @name         isAllReviewActionCompleted
    * @date         02/27/2017
    * @description  This method returns true if there is no incomplete action on ship to and its child Tank records 
    * @param        Account
    * @return       boolean
    */
    /*L4_45352_start
    public boolean isAllReviewActionCompleted(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','isAllReviewActionCompleted');
        boolean actionCompleted = true;           
        integer openActionCount = actionMapper.getCountOfCompletedReviewsOnShipTo(shipToAccount);
        if(openActionCount > 0){
            actionCompleted = false;
        }
        return actionCompleted;
    }
	L4_45352_End*/
    
    /**
    * @author       Accenture
    * @name         isShipToCSCReviewCompleted
    * @date         02/27/2017
    * @description  This method returns true if there is CSC Review On ship To is Completed
    * @param        Account
    * @return       boolean
    */
    /*L4_45352_Start
    public override boolean isShipToCSCReviewCompleted(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','isShipToCSCReviewCompleted');
        boolean reviewCompleted = true;
        if(shipToAccount.EP_CSC_Review_Required__c){
            integer cscReviewCount = actionMapper.getCountOfCSCReviewCompletedOnShipTo(shipToAccount);
            if(cscReviewCount > 0 ){
                reviewCompleted = false;
            }        
        }
        return reviewCompleted;
    }
	L4_45352_End*/
	
    
    /**
    * @author       Accenture
    * @name         isTankCSCReviewCompleted
    * @date         02/27/2017
    * @description  This method returns true if there is CSC Review On Tank is Completed
    * @param        Account
    * @return       boolean
    */
    /*L4_45352_Start
    public override boolean isTankCSCReviewCompleted(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','isTankCSCReviewCompleted');
        boolean reviewCompleted = true;
        integer tankCSCReviewCount = actionMapper.getCountOfCSCReviewCompletedOnTanks(shipToAccount);
        if(tankCSCReviewCount > 0){
            reviewCompleted = false;
        }
        return reviewCompleted;
    }
    L4_45352_End*/
	
    
   /**
    * @author       Accenture
    * @name         syncCustomerToPricingEngine
    * @date         02/27/2017
    * @description  This method sends ship-To along with Sell-To to pricing engince when user change the status to Basic Data setup from prospect
    * @param        Account
    * @return       NA
    */
    public override void syncCustomerToPricingEngine(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','syncCustomerToPricingEngine');
        List<Id> childIds = new List<Id>();
        EP_OutboundMessageService outservice = new EP_OutboundMessageService(shipToAccount.id,EP_AccountConstant.SFDC_TO_NAV_SHIPTO_PRICING_ENGINE,shipToAccount.EP_Puma_Company_Code__c);
        outservice.sendOutboundMessage(EP_AccountConstant.CUSTOMER_UPDATE,childIds);        
    }
	
      
    /**
    * @author       Accenture
    * @name         getSupplyLocationList
    * @date         02/27/2017
    * @description  This method returns supply location list for a ship to
    * @param        Id
    * @return       List<EP_Stock_Holding_Location__c>
    */
    public override List<EP_Stock_Holding_Location__c> getSupplyLocationList(Id accountId) {
        EP_GeneralUtility.Log('Public','EP_ShipTo','getSupplyLocationList');
        return stockHoldingLocationMapper.getStockHoldingLocationByIds(new set<Id> {accountId});
    }
	
    
    /**
    * @author       Accenture
    * @name         hasCompletedReviewActions
    * @date         02/27/2017
    * @description  This method check if all review actions on Ship to are completed or not
    * @param        Account
    * @return       Boolean
    */
    /*L4_45352_start
    public override boolean hasCompletedReviewActions(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','hasCompletedReviewActions');
        boolean isCompleted = false;
        system.debug('***shipToAccount**' +shipToAccount);
        system.debug('***accountDomainObj**' +accountDomainObj); 
        //Bulkification Implementation -- Start
        integer openActionCount = accountDomainObj.actionMapper.getCountOfIncompleteReviewActionsonShipTo(shipToAccount); 
        //Bulkification Implementation -- End
        if(openActionCount == 0){
            isCompleted = true;
        }
        return isCompleted;
    }
	L4_45352_End*/
    
    /**
    * @author       Accenture
    * @name         hasCompletedReviewActionsOnTanks
    * @date         02/27/2017
    * @description  This method check if all review actions on Tanks related to a ShipTo are completed
    * @param        Account
    * @return       Boolean
    */
    /*L4_45352_Start
    public override boolean hasCompletedReviewActionsOnTanks(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','hasCompletedReviewActionsOnTanks');
        boolean isCompleted = false;
        //Bulkification Implementation -- Start
        integer openActionCount = accountDomainObj.actionMapper.getCountOfIncompleteReviewActionsonTanks(shipToAccount); 
        //Bulkification Implementation -- End
        if(openActionCount == 0){
            isCompleted = true;
        }
        return isCompleted;
    }
	L4_45352_End*/
    
    /**
    * @author       Accenture
    * @name         sendCustomerCreateRequest
    * @date         02/27/2017
    * @description  This method send create Request to NAV and LS if parent is already active
    * @param        Account
    * @return       NA
    */
    /*public override void sendCustomerCreateRequest(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','sendCustomerCreateRequest');
        if(isParentActive(shipToAccount)){
            sendShipToSyncRequestToNAV(shipToAccount);
            sendShipToSyncRequestToLOMO(shipToAccount, true);
            setSentNAVWINDMFlag(shipToAccount);
        }
    }*/
	
	/**
    * @author       Accenture
    * @name         setSentNAVWINDMFlag 
    * @date         02/27/2017
    * @description  Method to set a flag on ShipTo records which are sent to NAV and WINDMS
    * @param        Account
    * @return       NA
    */
	/*public override void setSentNAVWINDMFlag(Account shipToAccount){
		EP_GeneralUtility.Log('Public','EP_ShipTo','setSentNAVWINDMFlag');
        
        Account accountObj = accountmapper.getAccountRecord(shipToAccount.Id);
        accountObj.EP_SENT_TO_NAV_WINDMS__c = true;
        update accountObj;
	}*/
    
    /**
    * @author       Accenture
    * @name         sendCustomerEditRequest
    * @date         02/27/2017
    * @description  This method send edit request to NAV and LS
    * @param        Account
    * @return       NA
    */
    public override void sendCustomerEditRequest(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','sendCustomerEditRequest');
        sendShipToSyncRequestToNAV(shipToAccount);
        sendShipToSyncRequestToLOMO(shipToAccount, false);
    }
	
    
    /**
    * @author       Accenture
    * @name         isParentActive
    * @date         02/27/2017
    * @description  This method returns true if ship To parent is in active status
    * @param        Account
    * @return       NA
    */
    @TestVisible
    private boolean isParentActive(Account shipToAccount){
        EP_GeneralUtility.Log('Private','EP_ShipTo','isParentActive');
        List<Account> acclist = accountMapper.getRecordsHavingActiveParent(new Set<Id>{shipToAccount.Id});
        return !acclist.isEmpty();
    }
	
// Changes made for CUSTOMER MODIFICATION L4 Start
    /**
    * @author       Accenture
    * @name         sendCustomerEditRequestToNAV
    * @date         30/11/2017
    * @description  This method send customer Edit request to NAV
    * @param        Account
    * @return       NA
    */
    public override void sendCustomerEditRequestToNAV(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','sendCustomerEditRequest');
        String transactionId = EP_IntegrationUtil.getTransactionID(EP_AccountConstant.NAV,EP_AccountConstant.CUSTOMER_EDIT);
        //Send Request To NAV
        sendShipToSyncRequestToNAV(shipToAccount);
    }

    /**
    * @author       Accenture
    * @name         sendCustomerEditRequestToWINDMS
    * @date         30/11/2017
    * @description  This method send customer Edit request to WINDMS
    * @param        Account
    * @return       NA
    */
    public override void sendCustomerEditRequestToWINDMS(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','sendCustomerEditRequest');
        //Send Request To LS
        sendShipToSyncRequestToLOMO(shipToAccount, false);
    }
    // Changes made for CUSTOMER MODIFICATION L4 End
    /**
    * @author       Accenture
    * @name         sendShipToSyncRequestToNAV
    * @date         02/27/2017
    * @description  This method send ship creation request to NAV
    * @param        Account
    * @return       NA
    */
    @TestVisible
    private void sendShipToSyncRequestToNAV(Account shipToAccount){
        EP_GeneralUtility.Log('Private','EP_ShipTo','sendShipToSyncRequestToNAV');
        List<id> childRecords = new List<id>();
        EP_OutboundMessageService outservice = new EP_OutboundMessageService(shipToAccount.ID,EP_AccountConstant.SFDC_TO_NAV_SHIPTO_EDIT,shipToAccount.EP_Puma_Company_Code__c);
        outservice.sendOutboundMessage(EP_AccountConstant.SHIP_TO_EDIT,childRecords);
        
    }
	
    
    /**
    * @author       Accenture
    * @name         sendShipToSyncRequestToLOMO
    * @date         02/27/2017
    * @description  This method send customer creation request to LOMOSOFT
    * @param        Account
    * @return       NA
    */
    @TestVisible
    private void sendShipToSyncRequestToLOMO(Account shipToAccount, boolean includeChildRecords){
        EP_GeneralUtility.Log('Private','EP_ShipTo','sendShipToSyncRequestToLOMO');
        List<id> childRecords = new List<id>();
        String companyName;
        List<account> shipToList = accountMapper.getShipToRecordsWithSupplyOptionAndTanks(new Set<Id>{shipToAccount.Id});
        if(!shipToList.isEmpty()){
            //Added condition for defect #44565 start
            if(includeChildRecords) {
                childRecords.addAll(getTankRecordIds(shipToList));
                childRecords.addAll(getStockHoldingLocationIds(shipToList));        
            }
            companyName = shipToList[0].EP_Puma_Company_Code__c;
            EP_OutboundMessageService outservice = new EP_OutboundMessageService(shipToAccount.Id,EP_AccountConstant.SFDC_TO_WINDMS_SHIPTO_EDIT,companyName);
            outservice.sendOutboundMessage(EP_AccountConstant.SHIP_TO_EDIT,childRecords);
        }
    }
    
    
    /**
    * @author       Accenture
    * @name         getTankRecordIds
    * @date         02/27/2017
    * @description  This method is used to get the record Ids of the tanks attached to a Ship To
    * @param        List<account>
    * @return       List<id>
    */
    @TestVisible
    private List<id> getTankRecordIds(List<account> shipToList){
        EP_GeneralUtility.Log('Private','EP_ShipTo','getTankRecordIds');
        List<id> tankRecordIds = new List<id>();
        for(EP_Tank__c tank :shipToList[0].Tank__r){
            tankRecordIds.add(tank.Id);
        }
        return tankRecordIds;
    }
	
    
    /**
    * @author       Accenture
    * @name         getStockHoldingLocationIds
    * @date         02/27/2017
    * @description  This method is used to the record Ids of the supply location attached to a Ship To
    * @param        List<account>
    * @return       List<id>
    */
    @TestVisible
    private List<id> getStockHoldingLocationIds(List<account> shipToList){
        EP_GeneralUtility.Log('Private','EP_ShipTo','getStockHoldingLocationIds');
        List<id> StockHoldingLocationIds = new List<id>();
        for(EP_Stock_Holding_Location__c supply :shipToList[0].Stock_Holding_Locations1__r){
            StockHoldingLocationIds.add(supply.Id);
        }
        return StockHoldingLocationIds; 
    }
	

   /**
    * @author       Accenture
    * @name         insertPlaceHolderTankDips
    * @date         02/27/2017
    * @description  This method is used to insert place holder tank dips for a Ship To
    * @param        Account
    * @return       NA
    */
    public override void insertPlaceHolderTankDips(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','insertPlaceHolderTankDips');
        EP_Account_VendorManagement vendorType  = getVendorType(shipToAccount);
        vendorType.insertPlaceHolderTankDips(shipToAccount);
    }
	
    
   /**
    * @author       Accenture
    * @name         deletePlaceHolderTankDips
    * @date         02/27/2017
    * @description  This method is used to delete place holder tank dips for a Ship To
    * @param        Account
    * @return       NA
    */
    public override void deletePlaceHolderTankDips(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','deletePlaceHolderTankDips');
        EP_Account_VendorManagement vendorType  = getVendorType(shipToAccount);
        vendorType.deletePlaceHolderTankDips(shipToAccount); 
    } 
	
    
    /**
    * @author       Accenture
    * @name         getVendorType
    * @date         02/27/2017
    * @description  This method is used to get the Vendor management strategy type for Ship To Account
    * @param        Account
    * @return       EP_Account_VendorManagement
    */
    @TestVisible
    private EP_Account_VendorManagement getVendorType(Account shipToAccount){
        EP_GeneralUtility.Log('Private','EP_ShipTo','getVendorType');
        EP_AccountDomainObject accountDomain = new EP_AccountDomainObject(shipToAccount);
        EP_AccountFactory accountFactory = new EP_AccountFactory(accountDomain);
        return accountFactory.getVendorType();
    }
	
    
    /**
    * @author       Accenture
    * @name         doBeforeInsertHandle
    * @date         02/27/2017
    * @description  This method handles logic which need to execute on trigger's before insert event of ship To 
    * @param        Account
    * @return       NA
    */
    public override void doBeforeInsertHandle(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','doBeforeInsertHandle');
        setCompanyFromParent(shipToAccount);
        setCurrency(shipToAccount);
        updateTankDipSubmissionTime(shipToAccount);
    }
	
    
    /**
    * @author       Accenture
    * @name         doBeforeUpdateHandle
    * @date         02/27/2017
    * @description  This method handles logic which need to execute on trigger's before update event of ship To 
    * @param        Account, Account
    * @return       NA
    */
    public override void doBeforeUpdateHandle(Account newShipToAccount, Account oldShipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','doBeforeUpdateHandle');
        setCompanyFromParent(newShipToAccount);       
    }
	
  
    /**
    * @author       Accenture
    * @name         doAfterInsertHandle
    * @date         02/27/2017
    * @description  This method handles logic which need to execute on trigger's after insert event of ship To 
    * @param        Account
    * @return       NA
    */
    public override void doAfterInsertHandle(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','doAfterInsertHandle');
        //doActionInsertManualSharingRecords(shipToAccount);
        EP_AccountShareUtility.insertManualSharingRecords(shipToAccount.Id);      
    }
	
    /**
    * @author       Accenture
    * @name         doAfterInsertHandle
    * @date         02/27/2017
    * @description  This method handles logic which need to execute on trigger's after insert event of ship To 
    * @param        Account
    * @return       NA
    */
    public override void doAfterUpdateHandle(Account newShipToAccount, Account oldShipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','doAfterUpdateHandle');
        if(isLocationIdChanged(newShipToAccount,oldShipToAccount)){
            updateConsignmentLocationReviewStatus(newShipToAccount);
        }
             
    }
    
    /**
    * @author       Accenture
    * @name         getBillTo
    * @date         02/27/2017
    * @description  This method returns bill to for Ship To 
    * @param        Account
    * @return       Account
    */
    public override Account getBillTo(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','getBillTo');
        return new EP_AccountMapper().fetchBillToAccountDetail(shipToAccount.parentId);
    }
	
    
   /**
    * @author       Accenture
    * @name         getSellTo
    * @date         02/27/2017
    * @description  This method returns Sell To for Ship To
    * @param        Account
    * @return       Account
    */
    public override Account getSellTo(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','getSellTo');
        return accountMapper.getAccountByAcountId(shipToAccount.parentId);
    }
	
    
   /**
    * @author       Accenture
    * @name         hasBillTo
    * @date         02/27/2017
    * @description  This method checks if there is a bill To attached to a customer
    * @param        Account
    * @return       Boolean
    */
    public override Boolean hasBillTo(Account shipToAccount) {
        EP_GeneralUtility.Log('Public','EP_ShipTo','hasBillTo');
        boolean hasBillTo = false;
        EP_AccountDomainObject accountDomainObj = new EP_AccountDomainObject(shipToAccount.parentId);
        EP_SellTo sellToStrategy = new EP_SellTo();
        hasBillTo = sellToStrategy.hasBillTo(accountDomainObj.localAccount);
        return hasBillTo;
    }


    /**
    * @author       Accenture
    * @name         getCustomerPONumber
    * @date         03/24/2017
    * @description  This method is used to get customer PO Number
    * @param        Account
    * @return       string
    */
    public override string getCustomerPONumber(Account shipToAccount){
        EP_GeneralUtility.Log('public','EP_ShipTo','getCustomerPONumber');
        string customerPONumber = null;
        if(String.isNotBlank(shipToAccount.EP_Customer_PO_Number__c) && shipToAccount.IsValidCustomerPONumber__c){
            customerPONumber = shipToAccount.EP_Customer_PO_Number__c;         
        }else{
            EP_AccountDomainObject accountdomain = new EP_AccountDomainObject(shipToAccount.ParentId);
            EP_SellTo sellToStrategy = new EP_SellTo();
            customerPONumber = sellToStrategy.getCustomerPONumber(accountdomain.localAccount); 
        }
        return customerPONumber;
    }
    
    /**
    * @author       Accenture
    * @name         getPricebookId
    * @date         03/27/2017
    * @description  This method is used to get the Pricebook Id from the bill to account
    * @param        Account
    * @return       Id
    */
    public override Id getPricebookId(Account shipToAccount){
        EP_GeneralUtility.Log('public','EP_ShipTo','getPricebookId');
        EP_AccountDomainObject accountDomain = new EP_AccountDomainObject(shipToAccount.ParentId);
        return accountDomain.getPriceBook();
    }  
    
    
	/**
    * @author       Accenture
    * @name         getPricebookEntries
    * @date         03/27/2017
    * @description  This method is used to get the product entries associcated to a customer
    * @param        Account
    * @return       List<PricebookEntry>
    */
    public override Map<String,PriceBookEntry> getPricebookEntry(Account shipToAccount,string key){
        EP_GeneralUtility.Log('public','EP_ShipTo','getPricebookEntries');
        map<String,PriceBookEntry> pricebookEntryMap = new map<String,PriceBookEntry>();
        if(String.isNotBlank(shipToAccount.ParentId)){
            EP_AccountDomainObject accountDomain = new EP_AccountDomainObject(shipToAccount.ParentId);
            EP_SellTo sellToStrategy = new EP_SellTo();
            pricebookEntryMap = sellToStrategy.getPricebookEntry(accountDomain.localAccount,key);
        }
        return pricebookEntryMap;
    }
    
	/**
    * @author       Accenture
    * @name         isShipToActive
    * @date         02/05/2017
    * @description  This method returns false , if there is any shipto for a customer with Error-Sync/Failure Integration Status
    * @param        account
    * @return       boolean 
    */
    public override boolean isShipToActive(Account sellToAccount){ 
        EP_GeneralUtility.Log('Public','EP_ShipTo','isShipToActive'); 
        boolean isActive = true;
        for(Account shipToAccount : accountMapper.getShipToRecordsSentToNAVandLS(sellToAccount.Id)){   
            if(!EP_Common_constant.SYNC_STATUS.equalsignorecase(shipToAccount.EP_Integration_Status__c)){
                isActive = false;
                break;
            }
        }
        return isActive;
    }
    
    // #routeSprintWork starts
     /**
    * @author       Accenture
    * @name         isRouteAllocationAvailable
    * @date         08/05/2017
    * @description  This method returns true if there is a junction record(Route Allocation) of ship to and selected default route 
    * @param        NA
    * @return       Boolean
    */
    public override boolean isRouteAllocationAvailable(Account acc){ // #routeSprintWork
       EP_GeneralUtility.Log('Private','EP_AccountService','isRouteAllocationAvailable');
       //Bulkification Implementation -- Start
       List<EP_Route_Allocation__c> routeAllocations = accountDomainObj.accountMapper.getRouteAllocations(acc); 
       //Bulkification Implementation -- End
       system.debug('routeAllocations = '+routeAllocations.size());
       if(routeAllocations.size() < 1){
           return false;
       }
        return true;
    }
    // #routeSprintWork ends

    //L4_45352_Start
    /**
    * @author       Accenture
    * @name         isSellToSynced
    * @date         11/20/2017
    * @description  This method used to check sell To is in Basic Data setup or active and is synced with all external systems
    * @param        Account
    * @return       boolen
    */
    public override boolean isSellToSynced(Account shipTo){
        EP_GeneralUtility.Log('Public','EP_ShipTo','isSellToSynced');
        EP_AccountDomainObject accountDomain = new EP_AccountDomainObject(shipTo.ParentId);
        EP_SellTo sellToStrategy = new EP_SellTo();
        return sellToStrategy.isSellToSynced(accountDomain.localAccount);
    }
    /**
    * @author       Accenture
    * @name         isSellToActive
    * @date         11/20/2017
    * @description  This method used to check sell To is in active state
    * @param        Account
    * @return       boolen
    */
    public override boolean isSellToActive(Account shipTo){
        EP_AccountDomainObject accountDomain = new EP_AccountDomainObject(shipTo.ParentId);
        EP_SellTo sellToStrategy = new EP_SellTo();
        return (sellToStrategy.isSellToActive(accountDomain.localAccount));
    }
    /**
    * @author       Accenture
    * @name         sendCustomerCreateRequestToNAV
    * @date         11/20/2017
    * @description  This method to send customer create request to NAV
    * @param        Account
    * @return       NA
    */
    public override void sendCustomerCreateRequestToNAV(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','sendCustomerCreateRequestToNAV');
        sendShipToSyncRequestToNAV(shipToAccount);
    }
    /**
    * @author       Accenture
    * @name         sendCustomerCreateRequestToWinDMS
    * @date         11/20/2017
    * @description  This method to send customer create request to WinDMs
    * @param        Account
    * @return       NA
    */    
    public override void sendCustomerCreateRequestToWinDMS(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','sendCustomerCreateRequestToWinDMS');
        sendShipToSyncRequestToLOMO(shipToAccount, true);
        //setSentNAVWINDMFlag(shipToAccount);
    }
    /**
    * @author       Accenture
    * @name         updateConsignmentLocationReviewStatus
    * @date         11/20/2017
    * @description  This method to update consignment location review to complete
    * @param        Account
    * @return       NA
    */
    public override void updateConsignmentLocationReviewStatus(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','updateConsignmentLocationReviewStatus');
        List<EP_Action__c> actionList = new List<EP_Action__c>();
        actionList = EP_ActionMapper.getConsignmentReview(shipToAccount.Id);
        for(EP_Action__c actObj : actionList){
            actObj.EP_Status__c = EP_AccountConstant.COMPLETE;
        }
        update actionList;        
    }
    /**
    * @author       Accenture
    * @name         isLocationIdChanged
    * @date         11/20/2017
    * @param        Account,Account
    * @return       Boolean
    */
    @TestVisible
  private boolean isLocationIdChanged(Account newShipToAccount, Account oldShipToAccount){
    return(newShipToAccount.EP_Location_Id__c!=null && newShipToAccount.EP_Location_Id__c!=oldShipToAccount.EP_Location_Id__c);
  }

//45463 Start
    /**
    * @author       Accenture
    * @name         updateTankDipSubmissionTime
    * @date         1/10/2018
    * @param        Account
    * @return       Void
    */
@TestVisible private void updateTankDipSubmissionTime(Account shipToAccount){
        EP_GeneralUtility.Log('private','EP_ShipTo','updateTankDipSubmissionTime');
        EP_CompanyMapper companyMapper = new EP_CompanyMapper();
        List<Company__c> companyList = companyMapper.getRecordsByIds(new set<Id>{shipToAccount.EP_Puma_Company__c});
        system.debug('companyList = '+companyList);
        if(!companyList.isEmpty() && companyList[0].EP_Tank_Dips_Submission_Time__c!=null 
                && companyList[0].EP_Reminder_Notification_Interval_in_hrs__c !=null){
            
            shipToAccount.EP_Tank_Dips_Submission_Time__c = companyList[0].EP_Tank_Dips_Submission_Time__c;
            shipToAccount.EP_Reminder_Notification_Interval_in_hrs__c = companyList[0].EP_Reminder_Notification_Interval_in_hrs__c;
        }
    }
//45463 End
}