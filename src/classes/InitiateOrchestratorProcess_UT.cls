/*
   @Author          CloudSense
   @Name            InitiateOrchestratorProcess_UT
   @CreateDate      08/05/2018
   @Description     This is the test class for InitiateOrchestratorProcess
   @Version         1.0
 
*/
@isTest
private class InitiateOrchestratorProcess_UT {
	@isTest 
	static void testModify() {
		//record type
		Id recordTypeId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Contract').getRecordTypeId();

		//order
		csord__Order__c order = EP_TestDataUtility.createCSOrder(null, recordTypeId, null);
		order.recordTypeId = recordTypeId;
		order.EP_Sync_with_NAV__c = true;
		order.Sync_with_WINDMS__c = true;
		order.Cancellation_Check_Done__c = false;
		INSERT order;

		List<Id> orderIdList = new List<Id>();
		orderIdList.add(order.Id);

		Test.startTest();
		InitiateOrchestratorProcess.execute(orderIdList);
		Test.stopTest();

		System.debug('Modify Order Id: ' + order.Id);

		Integer total = Database.countQuery('SELECT count() FROM CSPOFA__Orchestration_Process__c WHERE Order__c = \'' + order.Id + '\'');
		System.assertNotEquals(0, total, 'Orchestrator process not created');
	}

	@isTest 
	static void testCancel() {
		//record type
		Id recordTypeId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Contract').getRecordTypeId();

		//order
		csord__Order__c order = EP_TestDataUtility.createCSOrder(null, recordTypeId, null);
		order.recordTypeId = recordTypeId;
		order.EP_Sync_with_NAV__c = true;
		order.Sync_with_WINDMS__c = true;
		order.Cancellation_Check_Done__c = true;
		INSERT order;

		List<Id> orderIdList = new List<Id>();
		orderIdList.add(order.Id);

		Test.startTest();
		InitiateOrchestratorProcess.execute(orderIdList);
		Test.stopTest();

		System.debug('Cancel Order Id: ' + order.Id);

		Integer total = Database.countQuery('SELECT count() FROM CSPOFA__Orchestration_Process__c WHERE Order__c = \'' + order.Id + '\'');
		System.assertNotEquals(0, total, 'Orchestrator process not created');
	}

	@isTest 
	static void testCreateOrder() {
		//record type
		Id recordTypeId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Order').getRecordTypeId();

		//order
		csord__Order__c order = EP_TestDataUtility.createCSOrder(null, recordTypeId, null);
		order.recordTypeId = recordTypeId;
		order.EP_Sync_with_NAV__c = false;
		order.Sync_with_WINDMS__c = false;
		order.Cancellation_Check_Done__c = false;
		INSERT order;

		List<Id> orderIdList = new List<Id>();
		orderIdList.add(order.Id);

		Test.startTest();
		InitiateOrchestratorProcess.execute(orderIdList);
		Test.stopTest();

		System.debug('Create Order Order Id: ' + order.Id);

		Integer total = Database.countQuery('SELECT count() FROM CSPOFA__Orchestration_Process__c WHERE Order__c = \'' + order.Id + '\'');
		System.assertNotEquals(0, total, 'Orchestrator process not created');
	}

	@isTest 
	static void testCreateContract() {
		//record type
		Id recordTypeId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Contract').getRecordTypeId();

		//order
		csord__Order__c order = EP_TestDataUtility.createCSOrder(null, recordTypeId, null);
		order.recordTypeId = recordTypeId;
		order.EP_Sync_with_NAV__c = false;
		order.Sync_with_WINDMS__c = false;
		order.Cancellation_Check_Done__c = false;
		INSERT order;

		List<Id> orderIdList = new List<Id>();
		orderIdList.add(order.Id);

		Test.startTest();
		InitiateOrchestratorProcess.execute(orderIdList);
		Test.stopTest();

		System.debug('Create Contract Order Id: ' + order.Id);

		Integer total = Database.countQuery('SELECT count() FROM CSPOFA__Orchestration_Process__c WHERE Order__c = \'' + order.Id + '\'');
		System.assertNotEquals(0, total, 'Orchestrator process not created');
	}

	@testSetup
	static void createTestData(){
		//custom setting
		CS_ORDER_SETTINGS__c setting = EP_TestDataUtility.createCSOrderCustomSetting(null);
		setting.Orchestrator_Modify_Template__c = EP_TestDataUtility.createOrchestrationProccessTemplate('modify').Id;
		setting.Orchestrator_Cancel_Template__c = EP_TestDataUtility.createOrchestrationProccessTemplate('cancel').Id;
		setting.Contract_Orchestrator_Process_TemplateId__c = EP_TestDataUtility.createOrchestrationProccessTemplate('createContract').Id;
		setting.Orchestrator_Process_TemplateId__c = EP_TestDataUtility.createOrchestrationProccessTemplate('createOrder').Id;
		UPDATE setting;
	}
}