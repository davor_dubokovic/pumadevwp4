/* 
      @Author <Arpit Sethi>
      @name <EP_OrderConfigurationTrigger_Test>
      @CreateDate <22/01/2016>
      @Description <This is the test class for Order Configuration Trigger and covers Portal Order 002c and 002h Thread>
      @Version <1.0>
     
    */
   @isTest
    private class EP_OrderConfigurationTrigger_Test{
    
     /*
       Test Method to allow non mixing range records
    */
    static testMethod void testnonMixingRangeOrderConfig(){
        
    // insert the country   
    EP_Country__c  country = EP_TestDataUtility.createCountryRecord('Australia','AU','AU');
    country.EP_Mixing_Allowed__c = FALSE;  
    insert country;
    
   // insert the product 
    Product2 prod1 = EP_TestDataUtility.createTestRecordsForProduct();    
    
    // insert product
    Product2  productDieselObj = new product2(Name = 'Diesel', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true);                           
    Database.insert(productDieselObj);
    
    Product2  productPetrolObj = new product2(Name = 'petrol', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true);                           
    Database.insert(productPetrolObj);

    // insert the country order config record
    EP_Order_Configuration__c countryOrderConfig = EP_TestDataUtility.createOrderConfigRecord('countryOrderConfig1',EP_Common_Constant.COUNTRY_RANGE,1,1000,'LT',country.id,prod1.id);
    insert countryOrderConfig;
    
   // insert the non mixing order config record
    EP_Order_Configuration__c nonMixingOrderConfig = EP_TestDataUtility.createOrderConfigRecord('countryOrderConfig2',EP_Common_Constant.NON_MIXING_RANGE,1,1000,'LT',country.id,prod1.id);
    insert nonMixingOrderConfig;          
    system.assertEquals(nonMixingOrderConfig.EP_Min_Quantity__c >= countryOrderConfig.EP_Min_Quantity__c, TRUE); 
    system.assertEquals(nonMixingOrderConfig.EP_Min_Quantity__c <= countryOrderConfig.EP_Max_Quantity__c, TRUE); 
        
    nonMixingOrderConfig.EP_Min_Quantity__c = 2;
    try{
    update nonMixingOrderConfig;
    system.assertEquals(nonMixingOrderConfig.EP_Min_Quantity__c >= countryOrderConfig.EP_Min_Quantity__c, TRUE); 
    }
    catch(Exception e){      
    system.debug('============'+ApexPages.getMessages());
    System.assertEquals(true,ApexPages.getMessages().size()>0);
    }
  	countryOrderConfig.EP_Min_Quantity__c = 5;
     try{
    update countryOrderConfig;
    system.assertEquals(countryOrderConfig.EP_Min_Quantity__c == 5, TRUE); 
    }
    catch(Exception e){      
    system.debug('============'+ApexPages.getMessages());
    System.assertEquals(true,ApexPages.getMessages().size()>0);
    }
    }
    
     /**
     * @author Arpit Sethi
     * @date 01/02/2016
     * @description This Test Method is used to create mixing range order config record
     * @This method covers test case number 10462
     */
    static testMethod void testMixingRangeOrderConfig(){    
    // insert the country   
    EP_Country__c  country = EP_TestDataUtility.createCountryRecord('Australia','AU','AU');
    country.EP_Mixing_Allowed__c = TRUE;  
    insert country;
    
   // insert the product 
    Product2 prod1 = EP_TestDataUtility.createTestRecordsForProduct();    
       
    // insert the country order config record
    EP_Order_Configuration__c countryOrderConfig = EP_TestDataUtility.createOrderConfigRecord('countryOrderConfig1',EP_Common_Constant.COUNTRY_RANGE,200,20000,'LT',country.id,prod1.id);
    insert countryOrderConfig;
    
    // insert the non mixing order config record
    EP_Order_Configuration__c mixingOrderConfig = EP_TestDataUtility.createOrderConfigRecord('countryOrderConfig2',EP_Common_Constant.MIXING_RANGE,300,18000,'LT',country.id,prod1.id);
    insert mixingOrderConfig;
      
    system.assertEquals(mixingOrderConfig.EP_Min_Quantity__c >= countryOrderConfig.EP_Min_Quantity__c, TRUE); 
    system.assertEquals(mixingOrderConfig.EP_Max_Quantity__c <= countryOrderConfig.EP_Max_Quantity__c , TRUE); 
    }
    
    
     /**
     * @author Arpit Sethi
     * @date 01/02/2016
     * @description This Test Method is used to  disallow mixing range records more than max range of country record
     * @This method covers test case number 11110
     */
    static testMethod void testMaxDisallowMixingRangeOrderConfig(){     
    // insert the country   
    EP_Country__c  country = EP_TestDataUtility.createCountryRecord('Australia','AU','AU');
    country.EP_Mixing_Allowed__c = TRUE;  
    insert country;
    
   // insert the product 
    Product2 prod1 = EP_TestDataUtility.createTestRecordsForProduct();    
       
    // insert the country order config record
    EP_Order_Configuration__c countryOrderConfig = EP_TestDataUtility.createOrderConfigRecord('countryOrderConfig1',EP_Common_Constant.COUNTRY_RANGE,200,20000,'LT',country.id,prod1.id);
    insert countryOrderConfig;
    
    // insert the non mixing order config record
    try{
    EP_Order_Configuration__c mixingOrderConfig = EP_TestDataUtility.createOrderConfigRecord('countryOrderConfig2',EP_Common_Constant.MIXING_RANGE,200,20001,'LT',country.id,prod1.id);
    insert mixingOrderConfig;
    }
    catch(Exception e){      
    system.debug('============'+ApexPages.getMessages());
    System.assertEquals(true,ApexPages.getMessages().size()>0);
    }
    }
    

    /**
     * @author Arpit Sethi
     * @date 01/02/2016
     * @description This Test Method is used to disallow mixing range records less than min range of country record
     * @This method covers test case number 11108
     */
    static testMethod void testMinDisallowMixingRangeOrderConfig(){     
    // insert the country   
    EP_Country__c  country = EP_TestDataUtility.createCountryRecord('Australia','AU','AU');
    country.EP_Mixing_Allowed__c = TRUE;  
    insert country;
    
   // insert the product 
    Product2 prod1 = EP_TestDataUtility.createTestRecordsForProduct();    
       
    // insert the country order config record
    EP_Order_Configuration__c countryOrderConfig = EP_TestDataUtility.createOrderConfigRecord('countryOrderConfig1',EP_Common_Constant.COUNTRY_RANGE,200,20000,'LT',country.id,prod1.id);
    insert countryOrderConfig;
    
    // insert the non mixing order config record
    try{
    EP_Order_Configuration__c mixingOrderConfig = EP_TestDataUtility.createOrderConfigRecord('countryOrderConfig2',EP_Common_Constant.MIXING_RANGE,199,18000,'LT',country.id,prod1.id);
    insert mixingOrderConfig;
    }
    catch(Exception e){      
    system.debug('============'+ApexPages.getMessages());
    System.assertEquals(true,ApexPages.getMessages().size()>0);
    }
    }
    
    
     /*
       Test Method to disallow mixing range records
    */
    static testMethod void testDisAllowMixingNonMixingRangeOrderConfig(){       
    // insert the country   
    EP_Country__c  country = EP_TestDataUtility.createCountryRecord('Australia','AU','AU');
    country.EP_Mixing_Allowed__c = FALSE;  
    insert country;
    
   // insert the product 
    Product2 prod1 = EP_TestDataUtility.createTestRecordsForProduct();    
    
    // insert product
    Product2  productDieselObj = new product2(Name = 'Diesel', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true);                           
    Database.insert(productDieselObj);
    
    Product2  productPetrolObj = new product2(Name = 'petrol', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true);                           
    Database.insert(productPetrolObj);

    // insert the country order config record
   // EP_Order_Configuration__c countryOrderConfig = EP_TestDataUtility.createOrderConfigRecord('countryOrderConfig1',EP_Common_Constant.COUNTRY_RANGE,1,1000,'LT',country.id,prod1.id);
   //  insert countryOrderConfig;
    
    try{
   // insert the  mixing order config record
    EP_Order_Configuration__c mixingOrderConfig = EP_TestDataUtility.createOrderConfigRecord('countryOrderConfig2',EP_Common_Constant.MIXING_RANGE,1,1000,'LT',country.id,prod1.id);
    insert mixingOrderConfig;
    }
    catch(Exception ex){
            system.debug('============'+ApexPages.getMessages());
            system.assertEquals(true,ApexPages.getMessages().size()>0);
        }
        
        
    try{
   // insert the non mixing order config record
    EP_Order_Configuration__c nonMixingOrderConfig = EP_TestDataUtility.createOrderConfigRecord('countryOrderConfig2',EP_Common_Constant.NON_MIXING_RANGE,1,1000,'LT',country.id,prod1.id);
    insert nonMixingOrderConfig;
    }
    catch(Exception ex){
            system.debug('============'+ApexPages.getMessages());
            system.assertEquals(true,ApexPages.getMessages().size()>0);
        }

    }
    
    /**
     * @author Arpit Sethi
     * @date 01/02/2016
     * @description This Test Method is used to create country range records
     * @This method covers test case number 11107
     */
    static testMethod void testCountryRangeOrderConfig(){
        
    // insert the country   
    EP_Country__c  country = EP_TestDataUtility.createCountryRecord('Australia','AU','AU');
    country.EP_Mixing_Allowed__c = FALSE;  
    insert country;
    
   // insert the product 
    Product2 prod1 = EP_TestDataUtility.createTestRecordsForProduct();    
    
    // insert product
    Product2  productDieselObj = new product2(Name = 'Diesel', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true);                           
    Database.insert(productDieselObj);
    
    Product2  productPetrolObj = new product2(Name = 'petrol', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true);                           
    Database.insert(productPetrolObj);

    // insert the country order config record
    EP_Order_Configuration__c countryOrderConfig = EP_TestDataUtility.createOrderConfigRecord('countryOrderConfig1',EP_Common_Constant.COUNTRY_RANGE,200,2000,'LT',country.id,prod1.id);
    insert countryOrderConfig;
    
    system.assertEquals(countryOrderConfig.EP_Min_Quantity__c, 200); 
    system.assertEquals(countryOrderConfig.EP_Max_Quantity__c, 2000); 
  
    
    }
    
    }