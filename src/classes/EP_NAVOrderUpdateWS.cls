/* 
  @Author <Accenture>
   @name <EP_OrderStatusUpdateWS>
   @CreateDate <7/01/2016>
   @Description <This is the apex RESTful webService to Update status of the Orders synced from Nav> 
   @Version <1.0>
*/
@RestResource(urlMapping='/v1/OrderStatusUpdate/*')
global without sharing class EP_NAVOrderUpdateWS {
   /*
    This is a http post method which updates the order from external(NAV) system
   */
   @HttpPost
   global static void processRequest(){
        EP_GeneralUtility.Log('global','EP_NAVOrderUpdateWS','processRequest');
        RestRequest request = RestContext.request;
        String requestBody = request.requestBody.toString();
        RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
        EP_IntegrationService service = new EP_IntegrationService();
        string response = service.handleRequest(EP_Common_Constant.NAV_ORDER_UPDATE,requestBody); 
        RestContext.response.responseBody = Blob.valueOf(response);
   }
}