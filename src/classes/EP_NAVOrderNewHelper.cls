/* 
  @Author       Accenture
  @name :       EP_NAVOrderNewHelper
  @CreateDate   03/29/2017
  @Description  This is the Mapper/Data Transform class that Transform the JSON data into the Object fields to  create/update Orders and orderLine Items for Rest service from NAV
  @Version      1.0
*/
public class EP_NAVOrderNewHelper {
    @testVisible private EP_NAVOrderNewStub.orderWrapper localOrderWrapper;
    @testVisible private map<Id,Account> accountMap = new map<Id,Account>();
    @testVisible private set<string> accountCompositKeySet = new set<string>();
    @testVisible private set<string> orderNumberSet = new set<string>();
    @testVisible private map<String,Account> compositKeyAccountMap = new map<String,Account>();
    @testVisible private map<id,Account> accIdWithAccountMap = new map<id,Account>();
    @testVisible private map<string, csord__Order_Line_Item__c> orderItemMap = new map<string, csord__Order_Line_Item__c>();
    @testVisible private map<string, csord__Order_Line_Item__c> invoiceItemMap = new map<string, csord__Order_Line_Item__c>();
    @testVisible private map<String,PriceBookEntry> pricebookEntryWithProdCode = new map<String,PriceBookEntry>();
    @testVisible private map<String,PriceBookEntry> pricebookEntryWithName = new map<String,PriceBookEntry>();
    @testVisible private map<String, String> orderStatusMap = new map<String, String>();
    @testVisible private map<String, csord__Order__c> orderNumberWithOrderMap = new map<String, csord__Order__c>();
    
    /**
    * @Author       Accenture
    * @Name         createDataSets
    * @Date         03/29/2017
    * @Description  This Method is used  to create the sets to fetch the Bulk data
    * @Param        EP_NAVOrderNewStub.orderWrapper
    * @return       
    */
    @TestVisible
    private void createDataSets(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','createDataSets');
        setSellToCompositKey(orderWrapper);
        setShipToCompositKey(orderWrapper);
        setOrderNumber(orderWrapper);
        setOrderRefNumber(orderWrapper);
        this.localOrderWrapper = orderWrapper;

    }
    
    /**
    * @Author       Accenture
    * @Name         setOrderNumber
    * @Date         03/29/2017
    * @Description  This Method is used  to create set of OrderRef Numbers which are coming in inbound JSON.
    * @Param        EP_NAVOrderNewStub.orderWrapper
    * @return        
    */
    @TestVisible
    private void setOrderNumber(EP_NAVOrderNewStub.orderWrapper  orderWrapper) {
        if(string.isNotBlank(orderWrapper.Identifier.orderId)) {
            orderNumberSet.add(orderWrapper.Identifier.orderId);
        }
            
    }
    
    /**
    * @Author       Accenture
    * @Name         setOrderRefNumber
    * @Date         03/29/2017
    * @Description  This Method is used  to create set of OrderRef Numbers which are coming in inbound JSON.
    * @Param        EP_NAVOrderNewStub.orderWrapper
    * @return        
    */
    @TestVisible
    private void setOrderRefNumber(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        if(string.isNotBlank(orderWrapper.orderRefNr)) {
            orderNumberSet.add(orderWrapper.orderRefNr);
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         setSellToCompositKey
    * @Date         03/29/2017
    * @Description  This Method is used  to set CompositKey for SellTO.
    * @Param        EP_NAVOrderNewStub.orderWrapper
    * @return        
    */
    @TestVisible
    private void setSellToCompositKey(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setSellToCompositKey');
            system.debug('pep ' + orderWrapper);
        if(string.isNotBlank(orderWrapper.sellToId)) {
            system.debug('pep ' + orderWrapper);
            orderWrapper.sellToCompositKey = EP_GeneralUtility.getCompositeKey(orderWrapper.clientId,orderWrapper.sellToId);
            accountCompositKeySet.add(orderWrapper.sellToCompositKey);
        }
    }
    /**
    * @Author       Accenture
    * @Name         setShipToCompositKey
    * @Date         03/29/2017
    * @Description  This Method is used  to set CompositKey for shipTO.
    * @Param        EP_NAVOrderNewStub.orderWrapper
    * @return        
    */
    @TestVisible
    private void setShipToCompositKey(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setShipToCompositKey');
        if(string.isNotBlank(orderWrapper.shipToId)) {
            orderWrapper.shipToCompositKey = EP_GeneralUtility.getCompositeKey(orderWrapper.clientId,orderWrapper.sellToId, orderWrapper.shipToId);
            accountCompositKeySet.add(orderWrapper.shipToCompositKey);
        }
    }

    /**
    * @Author       Accenture
    * @Name         createDataMaps
    * @Date         03/29/2017
    * @Description  This Method is used to create Maps of SObjects from the Sets of data which are coming in inbound JSON.
    * @Param        EP_NAVOrderNewStub.orderWrapper
    * @return        
    */
    @TestVisible
    private void createDataMaps(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','createDataMaps');
        createDataSets(orderWrapper);
        orderStatusMap =  EP_CustomSettingsUtil.getOrderStatusFromCustomSetting();
        EP_OrderMapper orderMapper = new EP_OrderMapper();
        orderNumberWithOrderMap = orderMapper.getCsOrderMapByOrderNumber(orderNumberSet);
        setExistingOrderItems(orderWrapper.Identifier.orderId);
        setExistingInvoiceItems(orderWrapper.Identifier.orderId);
        EP_AccountMapper accountMapper = new EP_AccountMapper();
        system.debug('**accountCompositKeySet**'+accountCompositKeySet);
        compositKeyAccountMap = accountMapper.getAccountsMapByCompositeKey(accountCompositKeySet);
        system.debug('**compositKeyAccountMap**'+compositKeyAccountMap);
        setAccountIdWithAccount(compositKeyAccountMap);
        system.debug('**accIdWithAccountMap**'+accIdWithAccountMap);
    }
    /**
    * @Author       Accenture
    * @Name         getAccountIdWithAccount
    * @Date         03/24/2017
    * @Description  This Method is used to create Map of Account Sobject with Account Id as Key.
    * @Param        map<string, Account>
    * @return        
    */
    @TestVisible
    private void setAccountIdWithAccount(map<string, Account> compositKeyAccountMap) {
        EP_GeneralUtility.Log('private','EP_NAVOrderNewHelper','getAccountIdWithAccount');
        for(Account accountObj : compositKeyAccountMap.values()) {
            accIdWithAccountMap.put(accountObj.id, accountObj);
        }
    }
    /**
      * @Author    Accenture
      * @Name         setOrderAttributes
      * @Date         03/29/2017
      * @Description  This method will process the order which is coming in JSON string. 
                  It will check if a order is an existing order by orderNumber
                  It will call methods to update status, lookups and recordtype 
                  It will call method to clone the order based from orderRefNr in JSON string
      * @Param        EP_NAVOrderNewStub
      * @return       
    */
    public void setOrderAttributes(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        system.debug('pep ' + orderWrapper);
        EP_GeneralUtility.Log('Public','EP_NAVOrderNewHelper','setOrderAttributes');
        createDataMaps(orderWrapper);
        setOrderJSONAttributes(orderWrapper);
        setOrderStatus(orderWrapper);
        setSellTo(orderWrapper);
        setShipTo(orderWrapper);
        //setRecordType(orderWrapper);
        setStockHoldingLocation(orderWrapper);
        setPaymentMethod(orderWrapper);
        setPaymentTerm(orderWrapper);
        setPriceBook2Id(orderWrapper);
        orderWrapper.isCloned = false;
        //This should be call in the last to clone the order from given orderRefNr, 
        //this will overwrite all the objetct's values which are set above
        if(string.isNotBlank(orderWrapper.orderRefNr)) {
            cloneOrderFromOrderRefNumber(orderWrapper);
        }
    }
    /**
    * @Author       Accenture
    * @Name         setOrderJSONAttributes
    * @Date         03/24/2017
    * @Description  This Method is used to map Order fields with JSON String Attributes.
    * @Param        EP_NAVOrderNewStub.orderWrapper
    * @return        
    */
    @TestVisible
    private void setOrderJSONAttributes(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHelper','setOrderJSONAttributes');
        orderWrapper.sfOrder = new csord__Order__c();
        if(orderNumberWithOrderMap.containsKey(orderWrapper.Identifier.orderId)) {
            orderWrapper.sfOrder = orderNumberWithOrderMap.get(orderWrapper.Identifier.orderId);
            
        }
        orderWrapper.sfOrder.EP_SeqId__c = orderWrapper.seqId ;
        /** TR DateTime 59855 start **/
        if(string.isNotBlank(orderWrapper.reqDlvryDt)) {
            orderWrapper.sfOrder.EP_Requested_Delivery_Date__c = EP_DateTimeUtility.convertStringToDate(orderWrapper.reqDlvryDt);//Date.valueOf(orderWrapper.reqDlvryDt.subString(0,10));
        }
        orderWrapper.sfOrder.EP_Expected_Delivery_Date__c = EP_DateTimeUtility.convertStringToDate(orderWrapper.expctdDlvryDt); //EP_GeneralUtility.formatNAVDate(orderWrapper.expctdDlvryDt);
        orderWrapper.sfOrder.EffectiveDate__c = EP_DateTimeUtility.convertStringToDate(orderWrapper.orderStartDt);//EP_GeneralUtility.formatNAVDate(orderWrapper.orderStartDt);
        orderWrapper.sfOrder.EP_Loading_Date__c = EP_DateTimeUtility.convertStringToDate(orderWrapper.loadingDt);//EP_GeneralUtility.formatNAVDate(orderWrapper.loadingDt);
        /** TR DateTime 59855 End **/
        
        if(string.isNotBlank(orderWrapper.dlvryDt)) {
            string dateTimeStr = orderWrapper.dlvryDt.replace(EP_Common_Constant.STRING_HYPHEN,EP_Common_Constant.BLANK).replace(EP_Common_Constant.DATE_TIME_SEPARATOR,EP_Common_Constant.BLANK).replace(EP_Common_Constant.TIME_SEPARATOR_SIGN,EP_Common_Constant.BLANK);
            orderWrapper.sfOrder.EP_Actual_Delivery_Date_Time__c = decimal.valueof(dateTimeStr.left(12));
        }
        orderWrapper.sfOrder.EP_Order_Product_Category__c = EP_Common_Constant.PRODUCT_PACKAGED;
        orderWrapper.sfOrder.EP_Delivery_Type__c = orderWrapper.deliveryType;
        orderWrapper.sfOrder.CurrencyIsoCode = orderWrapper.currencyId;
        orderWrapper.sfOrder.EP_Order_Comments__c = orderWrapper.comment;
        orderWrapper.sfOrder.EP_Exception_Number__c = orderWrapper.exceptionNr ;
        orderWrapper.sfOrder.EP_Transporter_Pricing_No__c = orderWrapper.pricingTransporterCode ;
        orderWrapper.sfOrder.EP_Transporter_Code__c = orderWrapper.logisticTransporterCode;
        orderWrapper.sfOrder.EP_Order_Epoch__c = orderWrapper.orderEpoch;
        orderWrapper.sfOrder.EP_Source_Interface__c = orderWrapper.orderOrigination;
        orderWrapper.sfOrder.OrderReferenceNumber__c = orderWrapper.orderRefNr;
    }
    /**
      * @Author       Accenture
      * @Name         setOrderStatus
      * @Date         03/29/2017
      * @Description  This method will use to populate the Status based on the value in processingStatus in JSON by mapping this value with custom setting data
      * @Param        EP_NAVOrderNewStub.orderWrapper
      * @return       
    */
    @TestVisible
    private void setOrderStatus(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setOrderStatus');
        if(orderWrapper.sfOrder.Id == null) {
            orderWrapper.sfOrder.Status__c = orderStatusMap.get(orderWrapper.processingStatus);
        } else {
            //Status update only if status value is changed from NAV
            if(orderWrapper.sfOrder.Status__c != orderStatusMap.get(orderWrapper.processingStatus)) {
                EP_OrderDomainObject orderDomain = new EP_OrderDomainObject(orderWrapper.sfOrder.id);
                EP_OrderEvent orderEvent = new EP_OrderEvent(orderDomain.getStatus()+ EP_OrderConstant.OrderEvent_ToString + orderStatusMap.get(orderWrapper.processingStatus));
                EP_OrderService orderService = new EP_OrderService(orderDomain);
                orderService.setOrderStatus(orderEvent);
            }
        }
    }

    /**
      * @Author       Accenture
      * @Name         setShipTo
      * @Date         03/29/2017
      * @Description  This method is used to populate ShipTo Id on order Object
      * @Param        EP_NAVOrderNewStub.orderWrapper
      * @return       
    */
    @TestVisible
    private void setShipTo(EP_NAVOrderNewStub.orderWrapper orderWrapper){
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setShipTo');
        if(compositKeyAccountMap.containsKey(orderWrapper.shipToCompositKey)) {
            Account shipTo = compositKeyAccountMap.get(orderWrapper.shipToCompositKey);
            orderWrapper.sfOrder.EP_ShipTo__c  = shipTo.id;
            orderWrapper.sfOrder.Type__c = shipTo.EP_Ship_To_Type__c;
            orderWrapper.sfOrder.currencyIsoCode = shipTo.currencyIsoCode;
        }
    }
    /**
      * @Author       Accenture
      * @Name         setSellTo
      * @Date         03/29/2017
      * @Description  This method is used to populate the SellTo Id on order
      * @Param        EP_NAVOrderNewStub.orderWrapper
      * @return       
    */
    @TestVisible
    private void setSellTo(EP_NAVOrderNewStub.orderWrapper orderWrapper){
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setSellTo');
        if(compositKeyAccountMap.containsKey(orderWrapper.sellToCompositKey)) {
            Account sellTo = compositKeyAccountMap.get(orderWrapper.sellToCompositKey);
            orderWrapper.sfOrder.csord__Account__c  = sellTo.id;
        }
    }

    /**
      * @Author       Accenture
      * @Name         setRecordType
      * @Date         03/29/2017
      * @Description  This method will use to set the Record Type for order Object
      * @Param        EP_NAVOrderNewStub.orderWrapper
      * @return       
     */
    @TestVisible
    private void setRecordType(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setRecordType');
        orderWrapper.sfOrder.RecordTypeId = getOrderRecordType(orderWrapper.sfOrder);
    }

    /**
      * @Author       Accenture
      * @Name         getOrderRecordType
      * @Date         03/29/2017
      * @Description  This method will use to get the Record Type for order Object
      * @Param        SObject(Order)
      * @return       Id
     */
     @TestVisible
    private Id getOrderRecordType(csord__Order__c sfOrder) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','getOrderRecordType');
        Id orderRecordTypeId;
        Id accountId = sfOrder.EP_ShipTo__c == null ? sfOrder.csord__Account__c : sfOrder.EP_ShipTo__c;
        Account shipTobj = accIdWithAccountMap.get(accountId);
        if(isNonConsumptionOrder(sfOrder) && isVMIShipTo(shipTobj)) {
            orderRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER, EP_Common_Constant.VMI_ORDER_RECORD_TYPE_NAME);
        } else if((isNonConsumptionOrder(sfOrder) && isNonVMIShipTo(shipTobj)) || EP_Common_Constant.EX_RACK.equalsIgnoreCase(sfOrder.EP_Delivery_Type__c)) {
            orderRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER ,EP_Common_Constant.NONVMI_ORDER_RECORD_TYPE_NAME);
        } else {
            orderRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER ,EP_Common_Constant.CONSIGNMENT_ORDER_RECORD_TYPE_NAME);
        }
        system.debug('**orderRecordTypeId**' + orderRecordTypeId);
        return orderRecordTypeId;
    }
    /**
    * @Author       Accenture
    * @Name         setPriceBook2Id
    * @Date         03/29/2017
    * @Description  This Method is used to set PriceBook Id on Order Object.
    * @Param        EP_NAVOrderNewStub.orderWrapper
    * @return        
    */
    @TestVisible
    private void setPriceBook2Id(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setPriceBook2Id');
        //system.debug('**orderWrapper**'+orderWrapper);
        Id accountId = orderWrapper.sfOrder.EP_ShipTo__c == null ? orderWrapper.sfOrder.csord__Account__c : orderWrapper.sfOrder.EP_ShipTo__c;
        EP_AccountDomainObject accountDomain = new EP_AccountDomainObject(accountId);
        EP_AccountService accountService = new EP_AccountService(accountDomain);
        orderWrapper.sfOrder.PriceBook2Id__c = accountService.getPriceBookId();     
    }
    
    /**
      * @Author       Accenture
      * @Name         setStockHoldingLocation
      * @Date         03/29/2017
      * @Description :This method will use to populate stock Holding Location Id on order Object
      * @Param        EP_NAVOrderNewStub.orderWrapper
      * @return       
    */
    @TestVisible
    private void setStockHoldingLocation(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setStockHoldingLocation');
        if(orderWrapper.sfOrder.EP_ShipTo__c == null || string.isBlank(orderWrapper.pricingStockHldngLocId)) { 
            return;
        }
        EP_StockHoldingLocationMapper SHLMapper = new EP_StockHoldingLocationMapper();
        list<EP_Stock_Holding_Location__c> stockLocation = SHLMapper.getStockLocationByIdForShipTo(orderWrapper.pricingStockHldngLocId, orderWrapper.sfOrder.EP_ShipTo__c);
        if(!stockLocation.isEmpty()) {
            orderWrapper.sfOrder.Stock_Holding_Location__c = stockLocation[0].id;
        }
    }
   
   /**
    * @Author       Accenture
    * @Name         setPaymentMethod
    * @Date         03/29/2017
    * @Description  This method will use to populate payment Method Id on order Object
    * @Param        EP_NAVOrderNewStub.orderWrapper
    * @return       
    */
    @TestVisible
    private void setPaymentMethod(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setPaymentMethod');
        if(string.isBlank(orderWrapper.paymentMethod)) {
            return;
        }
        EP_PaymentMethodMapper paymentMethodMapper = new EP_PaymentMethodMapper();
        map<String,Id> paymentMethodMap = paymentMethodMapper.getPaymentMethodsMapByCodes(new set<string>{orderWrapper.paymentMethod});
        if(paymentMethodMap.containsKey(orderWrapper.paymentMethod)) {
            orderWrapper.sfOrder.EP_Payment_Method__c = paymentMethodMap.get(orderWrapper.paymentMethod);
        }
    }
    
   /**
  * @Author       Accenture
  * @Name         setPaymentTerm
  * @Date         03/29/2017
  * @Description  This method will use to populate payment Term Id on order Object
  * @Param        EP_NAVOrderNewStub.orderWrapper
  * @return       
  */
  @TestVisible
    private void setPaymentTerm(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setPaymentTerm');
        if(string.isBlank(orderWrapper.paymentTerm)) {
            return;
        }
        EP_PaymentTermMapper paymentTermMapper = new EP_PaymentTermMapper();
        map<String,Id> paymentTermsMap = paymentTermMapper.getPaymentTermsMapByCodes(new set<string>{orderWrapper.paymentTerm});
        if(paymentTermsMap.containsKey(orderWrapper.paymentTerm)) {
            orderWrapper.sfOrder.EP_Payment_Term_Value__c = paymentTermsMap.get(orderWrapper.paymentTerm);
        }
    }
    
    /**
  * @Author       Accenture
  * @Name         cloneOrderFromOrderRefNumber
  * @Date         03/29/2017
  * @Description  This method will be use to clone the Order from the given orderRefNr(OrderNumber) in JSON string
  * @Param        EP_NAVOrderNewStub.orderWrapper
  * @return       
  */
  @TestVisible
   private void cloneOrderFromOrderRefNumber(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','cloneOrderFromOrderRefNumber');
        if(orderNumberWithOrderMap.containsKey(orderWrapper.orderRefNr)) {
            orderWrapper.isCloned = true;
            orderWrapper.sfOrder = new csord__Order__c();
            orderWrapper.sfOrder = orderNumberWithOrderMap.get(orderWrapper.orderRefNr).Clone(false,true,false,false);
            orderWrapper.sfOrder.Status__c = EP_Common_Constant.ORDER_DRAFT_STATUS;
            orderWrapper.sfOrder.OrderReferenceNumber__c = orderWrapper.orderRefNr;
        }
    }

    /** 
    * @Author       Accenture
    * @Name         setPriceBookEntry
    * @Date         03/29/2017
    * @Description  This Method is used to set PriceBookEntry Id on Order Item Object.
    * @Param        EP_NAVOrderNewStub.orderWrapper
    * @return  
    */
    @TestVisible
    private void setPriceBookEntry(EP_NAVOrderNewStub.orderWrapper orderWrapper){
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setPriceBookEntry');
        Id accountId = orderWrapper.sfOrder.EP_ShipTo__c == null ? orderWrapper.sfOrder.csord__Account__c : orderWrapper.sfOrder.EP_ShipTo__c;
        //Account accountObj = accIdWithAccountMap.get(accountId);
        EP_AccountDomainObject accountDomain = new EP_AccountDomainObject(accountId);
        EP_AccountService accountService = new EP_AccountService(accountDomain);
        pricebookEntryWithProdCode = accountService.getPriceBookEntry(EP_Common_Constant.PRODUCTCODE);
        pricebookEntryWithName = accountService.getPriceBookEntry(EP_Common_Constant.PRODUCTName);
    }
    //-------------------------------Order Items------------------------------------------//
    
    
  /**
  * @Author       Accenture
  * @Name         setOrderItemAttributes
  * @Date         03/29/2017
  * @Description  This method is used to processes and set the fields values on Order Items.
  * @Param        EP_NAVOrderNewStub.orderWrapper
  * @return       
  */
    public void setOrderItemAttributes(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Public','EP_NAVOrderNewHelper','setOrderItemAttributes');
        setPriceBookEntry(orderWrapper);
        setExistingOrderItems(getOrderNumber(orderWrapper));
        for(EP_NAVOrderNewStub.OrderLine ordLine : orderWrapper.OrderLines.orderLine){
            setOrderItemJSONAttributes(ordLine);
            ordLine.orderLineItem.orderId__c = orderWrapper.sfOrder.id;
            setOrderItemId(ordLine, orderWrapper.isCloned);
            //Set The getPriceBookEntryId only for New OrderLine Items
            setPriceBookEntryIdByProdCode(ordLine);
            setAccountComponentXML(ordLine);
        }
    }
    /**
    * @Author       Accenture
    * @Name         setOrderItemJSONAttributes
    * @Date         03/24/2017
    * @Description  This method is used to set the JSON string values with Order Items object fields.
    * @Param        EP_NAVOrderNewStub.OrderLine
    * @return        
    */
    @TestVisible
    private void setOrderItemJSONAttributes(EP_NAVOrderNewStub.OrderLine ordLine) {
        EP_GeneralUtility.Log('private','EP_NAVOrderNewHelper','setOrderItemJSONAttributes');
        ordLine.orderLineItem = new csord__Order_Line_Item__c();
        ordLine.orderLineItem.EP_Is_Standard__c = true;
        ordLine.orderLineItem.UnitPrice__c = 0;
        ordLine.orderLineItem.EP_SeqId__c = ordLine.seqId;
        ordLine.orderLineItem.Quantity__c  = Decimal.valueOf(EP_GeneralUtility.formatNumber(ordLine.qty));
        ordLine.orderLineItem.EP_Quantity_UOM__c  = ordLine.uom;
        ordLine.orderLineItem.EP_Ambient_Loaded_Quantity__c  = Decimal.valueOf(ordLine.loadedAmbientQty);
        ordLine.orderLineItem.EP_Standard_Loaded_Quantity__c  = Decimal.valueOf(ordLine.loadedStandardQty);
        ordLine.orderLineItem.EP_Ambient_Delivered_Quantity__c  = Decimal.valueOf(ordLine.deliveredAmbientQty);
        ordLine.orderLineItem.EP_Standard_Delivered_Quantity__c  = Decimal.valueOf(ordLine.deliveredStandardQty);
        
    }
    /**
    * @Author       Accenture
    * @Name         getOrderNumber
    * @Date         03/29/2017
    * @Description  This method is used to get order Number from order Wrapper .
    * @Param        EP_NAVOrderNewStub.orderWrapper
    * @return        
    */
    @TestVisible
    private string getOrderNumber(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','getOrderNumber');
        string orderNumber = orderWrapper.Identifier.orderId;
        if(orderWrapper.isCloned) {
            orderNumber = orderWrapper.sfOrder.OrderReferenceNumber__c;
        }
        return orderNumber;
    }
    /**
      * @Author       Accenture
      * @Name         setOrderItemId
      * @Date         03/29/2017
      * @Description  This method is used to set SF id on order Item based on OrderNumer and itemId
      * @Param        EP_NAVOrderNewStub.OrderLine
      * @return       
    */
    @TestVisible
    private void setOrderItemId(EP_NAVOrderNewStub.OrderLine ordLine, boolean isOrderCloned) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setOrderItemId');
        Id orderLineItemId = null;
        if(orderItemMap.containsKey(ordLine.identifier.lineId) && !isOrderCloned) {
            ordLine.orderLineItem.Id = orderItemMap.get(ordLine.identifier.lineId).id;
        }
    }
    
    /**
      * @Author       Accenture
      * @Name         setPriceBookEntryIdByProdCode
      * @Date         03/29/2017
      * @Description  This method is used to set pricebook entry Id by Product Code
      * @Param        EP_NAVOrderNewStub.OrderLine
      * @return       
    */
    @TestVisible
    private void setPriceBookEntryIdByProdCode(EP_NAVOrderNewStub.OrderLine ordLine) {
         EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setPriceBookEntryIdByProdCode');
        if(ordLine.orderLineItem.Id == null) {
            ordLine.orderLineItem.PriceBookEntry__c  = getPriceBookEntryId(pricebookEntryWithProdCode, ordLine.itemId);
        }
    }
    /**
      * @Author       Accenture
      * @Name         setPriceBookEntryIdByProdName
      * @Date         03/29/2017
      * @Description  This method is used to set pricebook entry Id by Product Name
      * @Param        EP_NAVOrderNewStub.InvoiceComponent
      * @return       
    */
    @TestVisible
    private void setPriceBookEntryIdByProdName(EP_NAVOrderNewStub.InvoiceComponent ordLine) {
         EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setPriceBookEntryIdByProdName');
        if(ordLine.invoiceItem.Id == null) {
            ordLine.invoiceItem.PricebookEntryId__c = getPriceBookEntryId(pricebookEntryWithName, ordLine.type);
        }
    }
    /**
      * @Author       Accenture
      * @Name         setAccountComponentXML
      * @Date         03/29/2017
      * @Description  This method is used to set Account Component field on order Item
      * @Param        EP_NAVOrderNewStub.InvoiceComponent
      * @return       
    */
    @TestVisible
    private void setAccountComponentXML(EP_NAVOrderNewStub.OrderLine ordLine) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setAccountComponentXML');
        //Defect -58835--Start
        if(ordLine.lineComponents.acctComponents != null && ordLine.lineComponents.acctComponents.acctComponent != null) {
            ordLine.orderLineItem.EP_Accounting_Details__c = getAccountComponentXML(ordLine.lineComponents.acctComponents.acctComponent);
        }
        //Defect -58835--End
    }
    /**
      * @Author       Accenture
      * @Name         getPriceBookEntryId
      * @Date         03/29/2017
      * @Description  This method is used to get price book entry id from the Pricebook Map
      * @Param        Map<String,id>,string
      * @return       Id
    */
    @TestVisible
    private Id getPriceBookEntryId(Map<String,pricebookEntry> priceBookEntryMap,string itemId) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','getPriceBookEntryId');
        Id priceBookEntryId = null;
        string pricebookEntryKey = itemId + localOrderWrapper.sfOrder.PriceBook2Id__c + localOrderWrapper.sfOrder.currencyISOCode;
        if(priceBookEntryMap.containsKey(pricebookEntryKey)) {
            priceBookEntryId = priceBookEntryMap.get(pricebookEntryKey).id;
        }
        return priceBookEntryId;
    }
    /**
      * @Author       Accenture
      * @Name         getAccountComponentXML
      * @Date         03/29/2017
      * @Description  This method is used to create a XML string for Accounts components data coming in JSON string
      * @Param        list<EP_NAVOrderNewStub.AcctComponent>
      * @return       String
    */
    @TestVisible
    private String getAccountComponentXML(list<EP_NAVOrderNewStub.AcctComponent> acctComponentList) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','getAccountComponentXML');
        string accTagString = EP_Common_Constant.BLANK;
        for (EP_NAVOrderNewStub.AcctComponent accComp: acctComponentList) {
            accTagString += EP_Common_Constant.XML_ACC_COMPONENT_BEGIN;
            accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_COMPONENTCODE  + EP_Common_Constant.ANG_RIGHT + ((accComp.componentCode<> null)?accComp.componentCode:EP_Common_Constant.BLANK) + EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_COMPONENTCODE + EP_Common_Constant.ANG_RIGHT;
            accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_GLACCOUNT_NR + EP_Common_Constant.ANG_RIGHT + ((accComp.glAccountNr <> null)?accComp.glAccountNr:EP_Common_Constant.BLANK)  + EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_GLACCOUNT_NR + EP_Common_Constant.ANG_RIGHT;
            accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_BASEAMT + EP_Common_Constant.ANG_RIGHT + ((accComp.baseAmount <> null)?accComp.baseAmount :EP_Common_Constant.BLANK)+ EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_BASEAMT + EP_Common_Constant.ANG_RIGHT;
            accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_TAXRATE + EP_Common_Constant.ANG_RIGHT + ((accComp.taxRate <> null)?accComp.taxRate :EP_Common_Constant.BLANK)+ EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_TAXRATE + EP_Common_Constant.ANG_RIGHT;
            accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_TAXAMOUNT + EP_Common_Constant.ANG_RIGHT + ((accComp.taxAmount <> null)?accComp.taxAmount :EP_Common_Constant.BLANK)+ EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_TAXAMOUNT + EP_Common_Constant.ANG_RIGHT;
            accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.TOTALAMOUNT_TAG + EP_Common_Constant.ANG_RIGHT + ((accComp.totalAmount <> null)?accComp.totalAmount :EP_Common_Constant.BLANK)+ EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.TOTALAMOUNT_TAG + EP_Common_Constant.ANG_RIGHT;
            accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_IS_VAT + EP_Common_Constant.ANG_RIGHT + ((accComp.isVAT <> null)?accComp.isVAT :EP_Common_Constant.BLANK)+ EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_IS_VAT + EP_Common_Constant.ANG_RIGHT;
            accTagString += EP_Common_Constant.XML_ACC_COMPONENT_CLOSE;    
        }
        return accTagString;
    }
    
    /** Note : This Method should call after processOrderItems
      * @Author       Accenture
      * @Name         processInvoiceItems
      * @Date         03/29/2017
      * @Description  This Method will process Invoice Items and populate values for Invoice items(order Items)
      * @Param        EP_NAVOrderNewStub.orderWrapper
      * @return       
    */
    public void setInvoiceItemsAttributes(EP_NAVOrderNewStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Public','EP_NAVOrderNewHelper','setInvoiceItemsAttributes');
        if(orderWrapper.sfOrder.id == null) {
            return ;
        }
        for(EP_NAVOrderNewStub.OrderLine ordLine : orderWrapper.OrderLines.orderLine){
            //Defect -58835--Start
            if (ordLine.lineComponents.invoiceComponents != null) {
                for(EP_NAVOrderNewStub.InvoiceComponent ordItem: ordLine.lineComponents.invoiceComponents.invoiceComponent) {
                    //system.debug('**InvoiceComponent' + ordItem);
                    setInvoiceItemJSONAttributes(ordItem);
                    ordItem.invoiceItem.EP_Parent_Order_Line_Item__c = ordLine.orderLineItem.id;
                    ordItem.invoiceItem.Quantity__c = ordLine.orderLineItem.Quantity__c;
                    ordItem.invoiceItem.OrderId__c = ordLine.orderLineItem.OrderId__c;
                    ordItem.invoiceItem.Id = getInvoiceItemId(ordItem.invoiceItem, ordItem.type);
                    
                    setPriceBookEntryIdByProdName(ordItem);
                }
            }
            //Defect -58835--End
        }
        //system.debug('**InvoiceComponent-orderWrapper' + orderWrapper);
    }
    /**
    * @Author       Accenture
    * @Name         setOrderItemJSONAttributes
    * @Date         03/24/2017
    * @Description  This Method is used to map Invoice item field with data coming in JSON String
    * @Param        EP_NAVOrderNewStub.InvoiceComponent
    * @return        
    */
    @TestVisible
    private void setInvoiceItemJSONAttributes(EP_NAVOrderNewStub.InvoiceComponent ordItem) {
        EP_GeneralUtility.Log('private','EP_NAVOrderNewHelper','setInvoiceItemJSONAttributes');
        ordItem.invoiceItem = new csord__Order_Line_Item__c();
        ordItem.invoiceItem.EP_Invoice_Name__c = ordItem.Name;
        ordItem.invoiceItem.EP_Pricing_Total_Amount__c = Decimal.valueOf(EP_GeneralUtility.formatNumber(ordItem.amount));
        ordItem.invoiceItem.EP_Tax_Percentage__c = Decimal.valueOf(ordItem.taxPercentage);
        ordItem.invoiceItem.EP_Tax_Amount__c =  Decimal.valueOf(ordItem.taxAmount);
        ordItem.invoiceItem.UnitPrice__c = ordItem.invoiceItem.EP_Pricing_Total_Amount__c;
    }
    /**
      * @Author       Accenture
      * @Name         getInvoiceItemId
      * @Date         03/29/2017
      * @Description  This method will return existing Invoice Item Id based on priceBookEntryName and it's parent Id
      * @Param        orderItem, string
      * @return       Id
    */
    @TestVisible
    private Id getInvoiceItemId(csord__Order_Line_Item__c invoiceItem, string priceBookEntryName) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','getInvoiceItemId');
        Id invoiceId;
        string invoiceItemKey;
        invoiceItemKey = invoiceItem.EP_Parent_Order_Line_Item__c + priceBookEntryName;
        if(String.isNotBlank(invoiceItem.EP_Invoice_Name__c)){
            invoiceItemKey += invoiceItem.EP_Invoice_Name__c;
        }
        
        if(invoiceItemMap.containsKey(invoiceItemKey)) {
            invoiceId = invoiceItemMap.get(invoiceItemKey).id;
        }
        return invoiceId;
    }

    /**
      * @Author       Accenture
      * @Name         isNonConsumptionOrder
      * @Date         03/29/2017
      * @Description  Method to check if a order is NonConsumption type of order
      * @Param        Sobject(Order)
      * @return       boolean
     */
     @TestVisible
    private boolean isNonConsumptionOrder(csord__Order__c OrderObj) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','isNonConsumptionOrder');
        boolean isNonConsumptionOrder = false; 
        if(OrderObj.EP_Order_Category__c <> EP_Common_Constant.CONSUMPTION_ORDER) {
            isNonConsumptionOrder = true;
        }
        return isNonConsumptionOrder;
    }
    
    /**
      * @Author         Accenture
      * @Name           isVMIShipTo
      * @Date           03/29/2017
      * @Description    Method to check if a Account is a VMIShipTo
      * @Param          Sobject(Account)
      * @return         boolean
    */
    @TestVisible
    private boolean isVMIShipTo(Account accountObj) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','isVMIShipTo');
        boolean isVMIShipTo = false; 
        if(accountObj == null) {
            return isVMIShipTo;
        }
        if(EP_Common_Constant.VMI_SHIP_TO_DEV_NAME.equalsIgnoreCase(accountObj.RecordType.DeveloperName)) {
            isVMIShipTo = true;
        }
        return isVMIShipTo;
    }
    
    /**
      * @Author       Accenture
      * @Name         isNonVMIShipTo
      * @Date         03/29/2017
      * @Description  Method to check if a Account is a NonVMIShipTo
      * @Param        Sobject(Account)
      * @return       boolean
    */
    @TestVisible
    private boolean isNonVMIShipTo(Account accountObj) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','isNonVMIShipTo');
        boolean isNonVMIShipTo = false; 
        if(accountObj == null) {
            return isNonVMIShipTo;
        }

        if(EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME.equalsIgnoreCase(accountObj.RecordType.DeveloperName)) {
            isNonVMIShipTo = true;
        }
        return isNonVMIShipTo;
    }
    /**
      * @Author       Accenture
      * @Name         setExistingInvoiceItems
      * @Date         03/29/2017
      * @Description  This Method is used to set a map of the existing Invoice Items of Order
      * @Param        String
      * @return       
    */
    @TestVisible
    private void setExistingInvoiceItems(String orderNumber){
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setExistingInvoiceItems');
        map<string, csord__Order_Line_Item__c> invoiceItemsMap = new map<string, csord__Order_Line_Item__c>();
        string invoiceItemKey;
        EP_OrderItemMapper orderItemMapper = new EP_OrderItemMapper();
        for(csord__Order_Line_Item__c orderLine :orderItemMapper.getCSInvoiceItemsByOrderNumber(new set<string>{orderNumber})){
            invoiceItemKey = orderLine.EP_Parent_Order_Line_Item__c + orderLine.PriceBookEntryId__c;
            if(String.isNotBlank(orderLine.EP_Invoice_Name__c)){
                invoiceItemKey += orderLine.EP_Invoice_Name__c;
            }
            invoiceItemMap.put(invoiceItemKey,orderLine);
        }
    }
    /**
      * @Author       Accenture
      * @Name         setExistingOrderItems
      * @Date         03/29/2017
      * @Description  This method is used to set map of existing orderItems with Order Item Number
      * @Param        String
      * @return       
    */
    @TestVisible
    private void setExistingOrderItems(string orderNumber) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderNewHelper','setExistingOrderItems');
        EP_OrderItemMapper orderItemMapper = new EP_OrderItemMapper();
        for (csord__Order_Line_Item__c oItem: orderItemMapper.getCSOrderItemsByOrderNumber(orderNumber)) {
            orderItemMap.put(String.valueOf(oItem.EP_OrderItem_Number__c),oItem);
        }
    }
    
    /**
      * @Author       Accenture
      * @Name         sendOrderSyncRequest
      * @Date         03/29/2017
      * @Description  This method is used to send order Sync Request to NAV
      * @Param        String
      * @return       
    */
    public void sendOrderSyncRequest(Id orderid) {
        //Send Request only for Newly Created Order
        if(orderid != null && (string.isBlank(localOrderWrapper.Identifier.orderId) || localOrderWrapper.Identifier.orderId == 'null')){
            EP_OrderDomainObject orderDomain = new EP_OrderDomainObject(orderid);
            EP_OrderService orderService = new EP_OrderService(orderDomain);
            orderService.doSyncStatusWithNav();
        }
    }
}