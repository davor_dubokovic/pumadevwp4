@isTest
public class EP_NonVMIShipToSM_UT{

	static final string EVENT_NAME = '07-InactiveTo05-Active';
	@testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    
	static testMethod void getAccountState_test() {
		EP_NonVMIShipToSM localObj = new EP_NonVMIShipToSM();
		EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASInactiveDomainObject();
		EP_AccountEvent accountEvent = new EP_AccountEvent(EVENT_NAME);
		localObj.accountDomain = obj;
		localObj.accountEvent = accountEvent;
		Test.startTest();
		EP_AccountState result = localObj.getAccountState();
		Test.stopTest();
		Boolean instanceofresult = result instanceof EP_NonVMIShipToASInactive;
    	System.assertEquals(true, instanceofresult);
	}
}