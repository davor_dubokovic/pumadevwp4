/* ================================================
 * @Class Name : EP_CRM_Company_PayMeth_PayTermTest
 * @author : Hasmukh Jain
 * @Purpose: This class is used to test EP_CRM_CompanyTrigger, EP_CRM_PaymentMethodTrigger, EP_CRM_PaymentTermTriggerHandler apex trigger.
 * @created date: 22-Mar-2018
 ================================================*/
@isTest
private class EP_CRM_Company_PayMeth_PayTermTest{
      
    @testsetup
    public static  void testLoadData(){
        try{ 
            
            //Insert Company
            Company__c objcompy = new Company__c();
            objcompy.Name = 'Puma Energy';                        
            Database.SaveResult saveCompany = Database.insert(objcompy);
                   
            //Insert Payment Method
            EP_Payment_Method__c objPayMeth = new EP_Payment_Method__c();
            objPayMeth.Name = 'ACH';
            objPayMeth.EP_Payment_Method_Code__c = '001ABC'; 
            objPayMeth.EP_Company__c = objcompy.id;           
            Database.SaveResult savePaymentMeth = Database.insert(objPayMeth); 
            
            //Insert Payment Term
            EP_Payment_Term__c objPayTerm = new EP_Payment_Term__c();
            objPayTerm.Name = 'ACH';
            objPayTerm.EP_Payment_Term_Code__c = '001ABC'; 
            objPayTerm.EP_Company__c = objcompy.id;           
            Database.SaveResult savePaymentTerm = Database.insert(objPayTerm);
        }
        catch(exception ex){ex.getmessage();}
    }    
        
    public static testMethod void testafterInsertCompany(){
        List<Company__c> objcompy;       
        EP_CRM_CompanyTriggerHandler.afterInsertConnectionCompany(objcompy);        
    }  
    
    public static testMethod void testafterInsertPaymentMethod(){
        List<EP_Payment_Method__c> objPayMeth;       
        EP_CRM_PaymentMethodTriggerHandler.afterInsertConnectionPaymentMethod(objPayMeth);        
    } 
    
    public static testMethod void testafterInsertPaymentTerm(){
        List<EP_Payment_Term__c> objPayTerm;       
        EP_CRM_PaymentTermTriggerHandler.afterInsertConnectionPaymentTerm(objPayTerm);        
    }
}