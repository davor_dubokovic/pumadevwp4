/*
 *  @Author <Accenture>
 *  @Name <EP_SellToASBasicDataSetup>
 *  @CreateDate <20/02/2017>
 *  @Description <State Class for Sell To Basic Data Setup  Status> 
 *  @Version <1.0>
 */
 public with sharing class EP_SellToASBasicDataSetup extends EP_AccountState{
        /***NOvasuite fix constructor removed**/

    /**
    * @author       Accenture
    * @name         setAccountDomainObject
    * @param        NA
    * @return       NA
    */
    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount)
    {
        super.setAccountDomainObject(currentAccount);
    }
    /**
    * @author       Accenture
    * @name         doOnEntry
    * @param        NA
    */
    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_SellToASBasicDataSetup','doOnEntry');
        //L4_45352_start
        EP_AccountService service = new EP_AccountService(this.account); 
       //Chaining Fix Start       
       if(!EP_IntegrationUtil.ISERRORSYNC){
            //WP2-Pricing Engine Callout changes 
            if(!this.account.localaccount.EP_Synced_PE__c){
                system.debug('-Calling--PE--');
                service.doActionSyncCustomerToPricingEngine();
            }
           //WP2-Pricing Engine Callout changes 
           else if(!this.account.localaccount.EP_Synced_NAV__c){
                system.debug('-Calling--NAV--');
                service.doActionSendCreateRequestToNav();
            }
            else if(this.account.localaccount.EP_Synced_NAV__c && !this.account.localaccount.EP_Synced_WinDMS__C){
                system.debug('-Calling--WinDMS--');
                service.doActionSendCreateRequestToWinDMS();
            }
        }
        //Chaining Fix End
        //L4_45352_End
    }  
    /**
    * @author       Accenture
    * @name         doOnExit
    * @param        NA
    */
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_SellToASBasicDataSetup','doOnExit');
        
    }
    /**
    * @author       Accenture
    * @name         doTransition
    * @param        NA
    */
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_SellToASBasicDataSetup','doTransition');
        return super.doTransition();
    }
    /**
    * @author       Accenture
    * @name         isInboundTransitionPossible
    * @param        NA
    */
    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_SellToASBasicDataSetup','isInboundTransitionPossible');
        return super.isInboundTransitionPossible();
    }
}