/**
 * @author <Vinay Jaiswal>
 * @name <EP_OrderItemTriggerTest2>
 * @createDate <07/01/2016>
 * @description <Test class for testing automation tests for E2E 024_002e > 
 * @version <1.0>
 */
 
@isTest
private class EP_OrderItemTriggerTest2 {
    
    private static Account accObj, accObj1, shipTo;
    private static EP_Freight_Matrix__c freightMatrix;
    private static EP_Freight_Price__c freightPrice;
    private static OrderItem oOrderItems;
    private static Account vmiShipToAccount,sellToAccount; 
    private static Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.SELL_TO);
    private static Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];
    private static  User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
    private static Id shpToRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
    private static List<PricebookEntry> priceBookEntryToInsert;
    private static Id orderRecTypeIdVMI = Schema.SObjectType.Order.getRecordTypeInfosByName().get('VMI Orders').getRecordTypeId();
    private static  Id orderRecTypeIdNonVMI = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
    private static EP_Stock_Holding_Location__c stockLoc;
    private static void updateAccountStatus(){
    
        EP_ActionTriggerHandler.isExecuteAfterUpdate = true;
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = true;
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;      
    }
    
    /*
     Create Test Data
    */
    private static void createTestData() {
        // TO DO: implement unit test
     
        // Inserting a bill- to account
        accObj1= EP_TestDataUtility.createBillToAccount();
        accObj1.CurrencyIsoCode = 'USD';
        insert accObj1;
        updateAccountStatus();
        accObj1.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update accObj1;
        accObj1.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update accObj1;
    
    // Inserting a EP_Freight_Matrix__c record      
        freightMatrix = createFrightMatrix('Test');
        insert freightMatrix;
        
    //Create a sell-to account record
        EP_Payment_Term__c paymentTerm = EP_TestDataUtility.createPaymentTerm();
        paymentTerm.Name = EP_Common_Constant.CREDIT_PAYMENT_TERM;
        insert paymentTerm;
        accObj = EP_TestDataUtility.createSellToAccount(null,freightMatrix.id);
        accObj.EP_Payment_Term_Lookup__c = paymentTerm.Id;
        insert accObj;
        accObj.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update accObj;
        accObj.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update accObj;    
        shipTo = EP_TestDataUtility.createShipToAccount(accObj.Id,shpToRecordTypeId );
        insert shipTo;
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update shipTo;
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        shipTo.EP_VMI_Suggestion__c = false;
        update shipTo;
        
        Company__c company = EP_TestDataUtility.createCompany( 'AUN1');
        insert company;
      
        Account venderAccount = new Account();
        venderAccount.RecordTypeId = [select Id from recordType where sObjectType = 'Account' and developername = 'EP_Vendor' limit 1][0].Id;
        venderAccount.EP_Source_Entity_ID__c = 'TestVenderId';
        venderAccount.EP_Puma_Company__c = company.Id;
        venderAccount.BillingCity = 'testCity';
        venderAccount.Name = 'testVender';
        insert venderAccount;
        
        stockLoc = EP_TestDataUtility.createStockLocation(shipTo.id,true);
        stockLoc.EP_Distance__c = 50;
        stockLoc.EP_Transporter__c = venderAccount.Id;
        insert stockLoc;
      
        Order orderObj = createOrder(accObj.Id, orderRecTypeIdVMI, Test.getStandardPricebookId());
        //insert orderObj;
        orderObj.EP_Delivery_Type__c = 'Delivery';
        orderObj.EP_ShipTo__c = shipTo.Id;
        orderObj.Stock_Holding_Location__c = stockLoc.Id;
        orderObj.CurrencyIsoCode = 'USD';
        //update orderObj;
     
        Product2 productObj = createProduct('Test');
        Product2 taxProductObj = createProduct(EP_Common_Constant.TAX_PRODUCT_NAME);
        Product2 frieghtMatrixProductObj = createProduct(EP_Common_Constant.FREIGHT_PRICE_NAME);
        List<Product2> prodToInsert = new List<Product2>{productObj,taxProductObj,frieghtMatrixProductObj};
        insert prodToInsert;
        
        freightPrice = createFrightPrice(freightMatrix,prodToInsert[0].Id);
        insert freightPrice;
        //Test Class Fix Start
        PricebookEntry pricebookEntryObj = [select id ,CurrencyIsoCode, pricebook2id,product2id,product2.Name from PricebookEntry where product2id =: prodToInsert[0].id limit 1];
        PricebookEntry taxPricebookEntryObj = [select id ,pricebook2id,CurrencyIsoCode, product2id,product2.Name from PricebookEntry where product2id =: prodToInsert[1].id limit 1];
        PricebookEntry frieghtMatrixPricebookEntryObj = [select id ,pricebook2id,CurrencyIsoCode, product2id,product2.Name from PricebookEntry where product2id =: prodToInsert[2].id limit 1];
        priceBookEntryToInsert = new List<PricebookEntry>{pricebookEntryObj,taxPricebookEntryObj,frieghtMatrixPricebookEntryObj};
        orderObj.CurrencyIsoCode = pricebookEntryObj.CurrencyIsoCode;
        /*PricebookEntry pricebookEntryObj = createPricebookEntry(prodToInsert[0].Id, Test.getStandardPricebookId());
        PricebookEntry taxPricebookEntryObj = createPricebookEntry(prodToInsert[1].Id, Test.getStandardPricebookId());
        PricebookEntry frieghtMatrixPricebookEntryObj = createPricebookEntry(prodToInsert[2].Id, Test.getStandardPricebookId());
        priceBookEntryToInsert = new List<PricebookEntry>{pricebookEntryObj,taxPricebookEntryObj,frieghtMatrixPricebookEntryObj};
        insert priceBookEntryToInsert;*/
        //Test Class Fix End
        system.debug(')))))))'+priceBookEntryToInsert);
            
       
        
    }
    
    
     /**
     * @author Vinay Jaiswal
     * @date 07/01/2016
     * @description create test Order record
     * @params Id,Id,Id
     * @return Order
     */
    private static Order createOrder(Id acctId, Id recTypeId, Id pricebookId){
        Order obj = new Order();
        obj.AccountId = acctId;
        obj.RecordTypeId = recTypeId;
        obj.Status = 'Draft';
        obj.CurrencyIsoCode = 'USD';
        obj.PriceBook2Id = pricebookId;
        obj.EffectiveDate = system.today();
        
        return obj;
    }
    
    /**
     * @author Vinay Jaiswal
     * @date 07/01/2016
     * @description create test Product2 record
     * @params String
     * @return Product2
     */
    private static Product2 createProduct(String name){
        Product2 obj = new product2();
        obj.Name = name;
        obj.CurrencyIsoCode = 'USD';
        obj.IsActive = true;
        
        return obj;
    }
    
    /**
     * @author Vinay Jaiswal
     * @date 07/01/2016
     * @description create test PricebookEntry record
     * @params Id,Id
     * @return PricebookEntry
     */
    private static PricebookEntry createPricebookEntry(Id productId, Id pricebookId){
        PricebookEntry obj = new PricebookEntry();
        obj.Pricebook2Id = pricebookId;
        obj.Product2Id = productId;
        obj.UnitPrice = 1;
        obj.IsActive = true;
        obj.EP_Is_Sell_To_Assigned__c=True;
        obj.EP_VAT_GST__c = 14;
        obj.EP_Additional_Taxes__c = 5.0;
        obj.CurrencyIsoCode = 'USD';
        
        return obj;
    }
    
    /**
     * @author Vinay Jaiswal
     * @date 07/01/2016
     * @description create test OrderItem record
     * @params Id, Id
     * @return OrderItem
     */
    private static OrderItem createOrderItem(Id pricebookEntryId, Id orderId){
        OrderItem obj = new OrderItem();
        obj.PricebookEntryId = pricebookEntryId;
        obj.UnitPrice = 25.00;
        obj.Quantity = 1;
        obj.OrderId = orderId;
        return obj;
    }
    
    
    /**
     * @author Vinay Jaiswal
     * @date 07/01/2016
     * @description create test FrightMatrix record
     * @params Id, Id
     * @return OrderItem
     */
     
    private static EP_Freight_Matrix__c createFrightMatrix(String Name){
        EP_Freight_Matrix__c oFreightMatrix = new EP_Freight_Matrix__c();
        oFreightMatrix.Name = Name; 
        return oFreightMatrix;
    }
    
    /**
     * @author Vinay Jaiswal
     * @date 07/01/2016
     * @description create test FrightPrice record
     * @params Id, Id
     * @return OrderItem
     */
    
    private static EP_Freight_Price__c createFrightPrice(EP_Freight_Matrix__c oFm,Id prodId){
        EP_Freight_Price__c oFreightPrice = new EP_Freight_Price__c();
        //oFreightPrice.Name = 'Test';
        oFreightPrice.EP_Freight_Matrix__c = oFm.id;
        oFreightPrice.EP_Max_Distance__c = 100;
        oFreightPrice.EP_Min_Distance__c = 10;
        oFreightPrice.EP_Max_Volume__c = 1000;
        oFreightPrice.EP_Min_Volume__c = 1;
        oFreightPrice.EP_Distance_UOM__c = 'Km';
        oFreightPrice.EP_Volume_UOM__c = 'LT';
        oFreightPrice.EP_Freight_Price__c = 28.6;   
        oFreightPrice.EP_Product__c = prodId;
        oFreightPrice.CurrencyIsoCode = 'USD';
        return oFreightPrice;
    }
    
     /**
     * @author Vinay Jaiswal
     * @date 07/01/2016
     * @description create test method
     * @params Id, Id
     * @return OrderItem
     */
    
    private static testmethod void testInternal(){
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        Test.loadData(EP_Order_Fieldset_Sfdc_Nav_Intg__c.SobjectType,'EP_OrderStatusUpdate_FieldSet');
        Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_OrderStatusNAVMapping__c.SobjectType,'EP_OrderStatusUpdateNAV');
        createTestData();
        Test.startTest();
        shipTo = EP_TestDataUtility.createShipToAccount(accObj.Id,null);
        insert shipTo;
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        List<EP_Tank__c> tanksToBeInsertedList = new List<EP_Tank__c>();
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(shipTo.Id,prod.Id);
        tanksToBeInsertedList.add(tankObj);        
        insert tanksToBeInsertedList;
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update shipTo;
        
        //Updating the tank record to be 'Basic Data Setup'
        tankObj.EP_Tank_Status__c= EP_Common_Constant.BASIC_DATA_SETUP;
        update tankObj;
        
        //Updating the tank record to be 'Operational'
        tankObj.EP_Tank_Status__c = EP_Common_Constant.TANK_OPERATIONAL_STATUS;
        update tankObj;
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        shipTo.EP_Freight_Matrix__c = freightMatrix.Id;
        update shipTo;
        
        Company__c company = EP_TestDataUtility.createCompany( 'AUN3');
        insert company;
      
        Account venderAccount = new Account();
        venderAccount.RecordTypeId = [select Id from recordType where sObjectType = 'Account' and developername = 'EP_Vendor' limit 1][0].Id;
        /* TFS fix 45559,45560,45567,45568 start EP_NAV_ID__c deprecated, EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c and EP_Vendor_Type__c is replaced by EP_VendorType__c*/ 

        venderAccount.EP_Source_Entity_ID__c = 'TestVenderId';
        /* TFS fix 45559,45560,45567,45568 end*/
        venderAccount.EP_Puma_Company__c = company.Id;
        venderAccount.BillingCity = 'testCity';
        venderAccount.Name = 'testVender';
        insert venderAccount;
        
        stockLoc = EP_TestDataUtility.createStockLocation(shipTo.id,true);
        stockLoc.EP_Distance__c = 50;
        stockLoc.EP_Transporter__c = venderAccount.Id;
        insert stockLoc;
        Order orderObj = createOrder(accObj.Id, orderRecTypeIdNonVMI , Test.getStandardPricebookId());
        
        orderObj.EP_Delivery_Type__c = 'Delivery';
        orderObj.EP_ShipTo__c = shipTo.Id;
        orderObj.Stock_Holding_Location__c = stockLoc.Id;
         //Test Class Fix Start
        //orderObj.CurrencyIsoCode = 'USD';
        orderObj.CurrencyIsoCode = priceBookEntryToInsert[0].CurrencyIsoCode;
         //Test Class Fix End
        insert orderObj;
    oOrderItems = createOrderItem(priceBookEntryToInsert[0].Id, orderObj.Id );
        insert oOrderItems;
        Map<Id,OrderItem> oldOrderItemMap = new Map<Id,OrderItem>();
        oldOrderItemMap.put(oOrderItems.id,oOrderItems);
        oOrderItems.Quantity= 2;
        update oOrderItems;
        Map<Id,OrderItem> newOrderItemMap = new Map<Id,OrderItem>();
        newOrderItemMap.put(oOrderItems.id,oOrderItems);
       // EP_OrderItemTriggerHelper.updatechildItemQuantity(oldOrderItemMap,newOrderItemMap);
        Test.stopTest();
    List<OrderItem> orderItemsObj1 = [SELECT UnitPrice FROM ORDERITEM WHERE orderId=:orderObj.Id and pricebookentry.Name=:EP_Common_Constant.FREIGHT_PRICE_NAME];
    system.debug('======='+orderItemsObj1.size() );
        system.assertEquals(orderItemsObj1.size()==0, true);
        
    }
    
    
     /**
     * @author Vinay Jaiswal
     * @date 07/01/2016
     * @description create test method 1
     * @params Id, Id
     * @return OrderItem
     */
    
    private static testmethod void testInternal1(){
        
        createTestData();
        Test.startTest();               

        shipTo = EP_TestDataUtility.createShipToAccount(accObj.Id,null);
        insert shipTo;
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        List<EP_Tank__c> tanksToBeInsertedList = new List<EP_Tank__c>();
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(shipTo.Id,prod.Id);
        tanksToBeInsertedList.add(tankObj);
        
        insert tanksToBeInsertedList;
        freightMatrix = createFrightMatrix('Test');
        insert freightMatrix;
        
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update shipTo;
        tanksToBeInsertedList = new List<EP_Tank__c>();
        tankObj.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
        update tankObj;
        
        tankObj.EP_Tank_Status__c = EP_Common_Constant.TANK_OPERATIONAL_STATUS;
        update tankObj;
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        EP_Freight_Matrix__c freightMatrix1 = createFrightMatrix('Test');
        insert freightMatrix1 ;
        shipTo.EP_Freight_Matrix__c = freightMatrix1.Id;
        update shipTo;
        
        Company__c company = EP_TestDataUtility.createCompany( 'AUN2');
        insert company;
      
        Account venderAccount = new Account();
        venderAccount.RecordTypeId = [select Id from recordType where sObjectType = 'Account' and developername = 'EP_Vendor' limit 1][0].Id;
        venderAccount.EP_Source_Entity_ID__c= 'TestVenderId';
        venderAccount.EP_Puma_Company__c = company.Id;
        venderAccount.BillingCity = 'testCity';
        venderAccount.Name = 'testVender';
        insert venderAccount;
                
        stockLoc = EP_TestDataUtility.createStockLocation(shipTo.id,true);
        stockLoc.EP_Distance__c = 50;
        stockLoc.EP_Transporter__c = venderAccount.Id;
        system.debug( 'stockLoc==='+stockLoc);
        insert stockLoc;
         Order orderObj = createOrder(accObj.Id, orderRecTypeIdNonVMI , Test.getStandardPricebookId());
        
        orderObj.EP_Delivery_Type__c = 'Delivery';
        orderObj.EP_ShipTo__c = shipTo.Id;
        orderObj.Stock_Holding_Location__c = stockLoc.Id;
         //Test Class Fix Start
        //orderObj.CurrencyIsoCode = 'USD';
        orderObj.CurrencyIsoCode = priceBookEntryToInsert[0].CurrencyIsoCode;
         //Test Class Fix End
        insert orderObj;
    oOrderItems = createOrderItem(priceBookEntryToInsert[0].Id, orderObj.Id );
        insert oOrderItems;
        
        Test.stopTest();
    List<OrderItem> orderItemsObj1 = [SELECT UnitPrice FROM ORDERITEM WHERE orderId=:orderObj.Id and pricebookentry.Name=:EP_Common_Constant.FREIGHT_PRICE_NAME];
    system.debug('======='+orderItemsObj1.size() );
        system.assertEquals(orderItemsObj1.size()==0, true);
        
    }
    
}