/* 
   @Author          Accenture
   @name            EP_CustomerOtherAdjustmentHandler_UT
   @CreateDate      02/08/2017
   @Description     Test class for handler class to handle the inbound customer adjustment interface
   @Version         1.0
*/
@isTest
public class EP_CustomerOtherAdjustmentHandler_UT {
    
    @testSetup static void init() {
      List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
    }
    
    /**
    * @author           Accenture
    * @name             processRequest_positive1
    * @date             02/08/2017
    * @description      Test method used to process the inbound customer adjustment request and sends back the response
    * @param            NA
    * @return           NA
    */
    static testMethod void processRequest_positive1() {
        EP_CustomerOtherAdjustmentHandler localObj = new EP_CustomerOtherAdjustmentHandler();
        
        Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
        EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
        
        EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
        List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
        EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        string jsonRequest = JSON.serialize(stub);
        Test.startTest();
        String result = localObj.processRequest(jsonRequest);
        Test.stopTest();
        System.assertEquals(true, result != null);
    }
    
    /**
    * @author           Accenture
    * @name             processRequest_positive2
    * @date             02/08/2017
    * @description      Test method used to process the inbound customer adjustment request and sends back the response
    * @param            NA
    * @return           NA
    */
    static testMethod void processRequest_positive2() {
        EP_CustomerOtherAdjustmentHandler localObj = new EP_CustomerOtherAdjustmentHandler();
        
        Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
        EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
        
        EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
        List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
        EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        document.docState = EP_Common_Constant.APPLICATION;
        string jsonRequest = JSON.serialize(stub);
        Test.startTest();
        String result = localObj.processRequest(jsonRequest);
        Test.stopTest();
        System.assertEquals(true, result != null);
    }
    
    /**
    * @author           Accenture
    * @name             processDocuments_test
    * @date             02/08/2017
    * @description      Test method used to process the inbound customer adjustment request and sends back the response
    * @param            NA
    * @return           NA
    */
    static testMethod void processDocuments_test() {
        EP_CustomerOtherAdjustmentHandler localObj = new EP_CustomerOtherAdjustmentHandler();
        
        Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
        EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
        
        EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
        List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
        EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        document.docState = EP_Common_Constant.APPLICATION;
        Test.startTest();
        List<EP_AcknowledgementStub.dataset> result = EP_CustomerOtherAdjustmentHandler.processDocuments(documents);
        Test.stopTest();
        System.assertEquals(true, !result.isEmpty());
    }
    
    /**
    * @author           Accenture
    * @name             processRequest_negative
    * @date             02/08/2017
    * @description      Test method used to process the inbound customer adjustment request and sends back the response
    * @param            NA
    * @return           NA
    */
    static testMethod void processRequest_negative() {
        EP_CustomerOtherAdjustmentHandler localObj = new EP_CustomerOtherAdjustmentHandler();
        
        Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
        EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
        
        EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
        List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
        EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        string jsonRequest = JSON.serialize(stub);
        jsonRequest= jsonRequest.substring(0,jsonRequest.length()-2);
        Test.startTest();
        String result = localObj.processRequest(jsonRequest);
        Test.stopTest();
        System.assertEquals(true, result != null);
    }
    
    /**
    * @author           Accenture
    * @name             parse_test
    * @date             02/08/2017
    * @description      Test method used to parse the inbound customer adjustment request 
    * @param            NA
    * @return           NA
    */
    static testMethod void parse_test() {
        EP_CustomerOtherAdjustmentHandler localObj = new EP_CustomerOtherAdjustmentHandler();
        EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
        String jsonString = JSON.serialize(stub);
        Test.startTest();
        EP_CustomerOtherAdjustmentStub result = EP_CustomerOtherAdjustmentHandler.parse(jsonString);
        Test.stopTest();
        System.assertEquals(true, result != null);
    }
    
    /**
    * @author           Accenture
    * @name             createAcknowledgementDatasets_test1
    * @date             02/08/2017
    * @description      Test method used to create acknowledgement datasets for inbound customer adjustment request 
    * @param            NA
    * @return           NA
    */
    static testMethod void createAcknowledgementDatasets_test1() {
        EP_CustomerOtherAdjustmentHandler localObj = new EP_CustomerOtherAdjustmentHandler();
        
        Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
        EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
        
        EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
        List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
        EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        EP_CustomerOtherAdjustmentHelper.setCustomerAdjustmentAttributes(documents);
        Test.startTest();
        List<EP_AcknowledgementStub.dataset> result = EP_CustomerOtherAdjustmentHandler.createAcknowledgementDatasets(documents);
        Test.stopTest();
        System.assertEquals(true, result.size()>0);
    }
    
    /**
    * @author           Accenture
    * @name             createAcknowledgementDatasets_test2
    * @date             02/08/2017
    * @description      Test method used to create acknowledgement datasets for inbound customer adjustment request 
    * @param            NA
    * @return           NA
    */
    static testMethod void createAcknowledgementDatasets_test2() {
        EP_CustomerOtherAdjustmentHandler localObj = new EP_CustomerOtherAdjustmentHandler();
        
        Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
        EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
        
        EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
        List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
        EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        document.errorDescription='Error';
        Test.startTest();
        List<EP_AcknowledgementStub.dataset> result = EP_CustomerOtherAdjustmentHandler.createAcknowledgementDatasets(documents);
        Test.stopTest();
        System.assertEquals(true, result.size()>0);
    }
    
    /**
    * @author           Accenture
    * @name             doDML_test1
    * @date             02/08/2017
    * @description      Test method used to perform the DML for inbound customer adjustment request 
    * @param            NA
    * @return           NA
    */
    static testMethod void doDML_test1() {
        EP_CustomerOtherAdjustmentHandler localObj = new EP_CustomerOtherAdjustmentHandler();
        
        Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
        EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
        
        List<EP_Customer_Other_Adjustment__c> customerAdjustmentsToUpdate = new List<EP_Customer_Other_Adjustment__c>();
        List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
        EP_Customer_Other_Adjustment__c custAdjust = EP_TestDataUtility.createCustomerOtherAdjustment(sellTo.Id,paymentTerm.Id,sellTo.EP_Puma_Company_Code__c);
        custAdjust.EP_Amount_Paid__c=2500;
        customerAdjustmentsToUpdate.add(custAdjust);
        Test.startTest();
        boolean result = EP_CustomerOtherAdjustmentHandler.doDML(customerAdjustmentsToUpdate,ackDatasets);
        Test.stopTest();
        System.assertEquals(true, !result);
    }
    
    /**
    * @author           Accenture
    * @name             doDML_test2
    * @date             02/08/2017
    * @description      Test method used to perform the DML for inbound customer adjustment request 
    * @param            NA
    * @return           NA
    */
    static testMethod void doDML_test2() {
        EP_CustomerOtherAdjustmentHandler localObj = new EP_CustomerOtherAdjustmentHandler();
        
        Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
        EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
        
        List<EP_Customer_Other_Adjustment__c> customerAdjustmentsToUpdate = new List<EP_Customer_Other_Adjustment__c>();
        List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
        EP_Customer_Other_Adjustment__c custAdjust = EP_TestDataUtility.createCustomerOtherAdjustment(sellTo.Id,paymentTerm.Id,sellTo.EP_Puma_Company_Code__c);
        custAdjust.EP_Client_ID__c =null;
        custAdjust.EP_Amount_Paid__c=2500;
        customerAdjustmentsToUpdate.add(custAdjust);
        Test.startTest();
        boolean result = EP_CustomerOtherAdjustmentHandler.doDML(customerAdjustmentsToUpdate,ackDatasets);
        Test.stopTest();
        System.assertEquals(true, result);
    }
    
    
}