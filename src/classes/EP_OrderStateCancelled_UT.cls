@isTest
public class EP_OrderStateCancelled_UT
{
    @testSetup static void init() {
            List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
            List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
            List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
            List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }

    static testMethod void getTextValue_test() {
        Test.startTest(); 
        String result = EP_OrderStateCancelled.getTextValue();
        Test.stopTest();
        System.AssertEquals(EP_OrderConstant.OrderState_Cancelled, result);
         
    }
    static testMethod void doOnEntry_test() { 
        EP_OrderStateCancelled localObj = new EP_OrderStateCancelled();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateSubmittedDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent( EP_OrderConstant.USER_CANCEL);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry(); 
        Test.stopTest();
    }    
    static testMethod void doOnExit_test() {
        EP_OrderStateCancelled localObj = new EP_OrderStateCancelled();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateSubmittedDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();        
    }
    
    
    static testMethod void setOrderDomainObject_test() {
        EP_OrderStateCancelled localObj = new EP_OrderStateCancelled();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateSubmittedDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        localObj.setOrderContext(obj,oe);    
        Test.startTest();
        localObj.setOrderDomainObject(obj);
        Test.stopTest();
        System.assertEquals(obj, localObj.order);
    }
}