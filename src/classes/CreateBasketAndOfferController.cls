/* 
@Author      CloudSense
@name        CreateBasketAndOfferController
@CreateDate  23/07/2017
@Description Class for creating the basket in the background and create MLE URL for the iframe
@Version     1.0
*/

public with sharing class CreateBasketAndOfferController
{
    private static final String CHANNEL_PARAM = system.label.ORDER_CHANNEL_CSC;
    private static final String COMMUNITY_CHANNEL = system.label.ORDER_CHANNEL_CUSTOMER_COMMUNITY;

    // Initialise
    public Account account;
    public String configurationId;
    public String basketId;
    // fetch Custom setting
    public CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();
    // Populate offerId from custom setting
    public String offerId = csOrderSetting.OfferTemplateId__c;
    public String orderContractOfferId = csOrderSetting.OrderContractOfferTemplateId__c;
    public String OpenOrderContractScreenFlow = csOrderSetting.OpenOrderContractScreenFlow__c;
    public cscfga__Product_Basket__c basket;
    public cscfga__Product_Configuration__c parentConfiguration;
    public cscfga__Product_Configuration__c childConfiguration;
    public cscfga__Attribute__c attribute1;
    public cscfga__Attribute__c attribute2;
    public cscfga__Attribute__c attribute3;
    public cscfga__Attribute__c attribute4;
    public cscfga__Attribute__c attribute5;
    public cscfga__Attribute__c attribute6;
    public cscfga__Attribute__c attribute7;
    public List<cscfga__Attribute__c> attributes = new List<cscfga__Attribute__c>();
    // Set the channel accordingly
    public String channel {
        get {
            if (this.channel == null) {
                this.channel = ApexPages.currentPage().getParameters().get(CHANNEL_PARAM);
            }
            return this.channel;
        }
        private set;
    }

    /**
    * @author       CloudSense
    * @name         CreateBasketAndOfferController
    * @date         23/07/2017
    * @description  Constructor for the class
    * @param        StandardController
    * @return       NA
    */  
    public CreateBasketAndOfferController(ApexPages.StandardController controller)
    {
        EP_GeneralUtility.Log('Public','CreateBasketAndOfferController','CreateBasketAndOfferController');
        account = (Account) controller.getRecord();  
    }
    
    /**
    * @author       CloudSense
    * @name         CreateBasketAndOfferController
    * @date         23/07/2017
    * @description  Create the basket, add the offer and launch the product configuration screen
    * @param        NA
    * @return       NA
    */  
    public PageReference createBasket()
    {
        EP_GeneralUtility.Log('Public','CreateBasketAndOfferController','createBasket');
        // Create the basket add the offer and update the basket name
        basketId = ProductConfigurationCloner.createBasketAndCloneOfferToBasket(account.Id, offerId);
        basket = [select id, name from cscfga__product_basket__c where id = :basketId];
        basket.Name = 'Order for ' + account.Name;
        update basket;

        // Set attributes that connect the child product with the parent product
        connectConfigurations();
        System.debug('CreateBasketAndOfferController - createBasket - basketId is ' + basketId);

        PageReference configPage = Page.customConfiguration;
        Map<String, String> configPageParams = configPage.getParameters();
        configPageParams.put('configId', parentConfiguration.id);
        configPageParams.put('linkedId', basketId);
        configPage.setRedirect(true);
        return configPage;
    }

    /**
    * @author       CloudSense
    * @name         CreateBasketAndOfferController
    * @date         23/04/2018
    * @description  Create the basket, add the offer and launch the product configuration screen
    * @param        NA
    * @return       NA
    */  
    public PageReference createBasketForOpenOrderContract()
    {
        EP_GeneralUtility.Log('Public','CreateBasketAndOfferController','createBasketForOpenOrderContract');
        // Create the basket add the offer and update the basket name
        basketId = ProductConfigurationCloner.createBasketAndCloneOfferToBasket(account.Id, orderContractOfferId);
        basket = [select id, name from cscfga__product_basket__c where id = :basketId];
        basket.Name = 'Contract for ' + account.Name;
        update basket;

        // Set attributes that connect the child product with the parent product
        connectConfigurations();
        System.debug('CreateBasketAndOfferController - createBasketForOpenOrderContract - basketId is ' + basketId);

        PageReference configPage = Page.OpenOrderContract;
        Map<String, String> configPageParams = configPage.getParameters();
        configPageParams.put('configId', parentConfiguration.id);
        configPageParams.put('linkedId', basketId);
        configPageParams.put('type', 'Contract');
        configPageParams.put('ScreenFlowName', OpenOrderContractScreenFlow);
        configPage.setRedirect(true);
        return configPage;
    }
    
     public PageReference editBasket()
    {
        EP_GeneralUtility.Log('Public','CreateBasketAndOfferController','editBasket');
        // Create the basket add the offer and update the basket name
        try{
            basketId = ProductConfigurationCloner.createBasketAndCloneOfferToBasket(account.Id, offerId);
            basket = [select id, name from cscfga__product_basket__c where id = :basketId];
            basket.Name = 'Order for ' + account.Name;
            update basket;
        }Catch(Exception exp){
            
        }

        // Set attributes that connect the child product with the parent product
        connectConfigurations();
        System.debug('CreateBasketAndOfferController - createBasket - basketId is ' + basketId);

        PageReference configPage = Page.customConfiguration;
        Map<String, String> configPageParams = configPage.getParameters();
        configPageParams.put('configId', parentConfiguration.id);
        configPageParams.put('linkedId', basketId);
        configPage.setRedirect(true);
        return configPage;
    }

    /**
    * @author       CloudSense
    * @name         CreateBasketAndOfferController
    * @date         23/07/2017
    * @description  Set attributes that connect the child product with the parent product
    * @param        NA
    * @return       NA
    */  
        //
    public void connectConfigurations()
    {
        EP_GeneralUtility.Log('Public','CreateBasketAndOfferController','createBasket');
        
        system.debug('connectConfigurations mategr c1');
        // Get the parent and child product configurations
        parentConfiguration = [select id, name from cscfga__product_configuration__c where name = : csOrderSetting.Parent_Order_Name__c and cscfga__Product_Basket__c = :basketId];
        // childConfiguration = [select id, name from cscfga__product_configuration__c where name = 'Puma Fuel' and cscfga__Product_Basket__c = :basketId];
        system.debug('connectConfigurations mategr c2 parentConfiguration ' + parentConfiguration);
        // Get the parent product basket id attribute and set the value
        //attribute1 = [select id, name, cscfga__value__c from cscfga__attribute__c where name =: system.label.MLE_Product_Basket_Id and cscfga__product_configuration__c = :parentConfiguration.Id];
        //system.debug('connectConfigurations mategr c3 attribute1 ' + attribute1);
        //attribute1.cscfga__Value__c = basketId;
        //system.debug('connectConfigurations mategr c4 attribute1 ' + attribute1);
        //system.debug('connectConfigurations mategr c5 attribute1.cscfga__Value__c ' + attribute1.cscfga__Value__c);
        
        // Get the parent product order lines attribute and set the value
        //attribute7 = [select id, name, cscfga__value__c from cscfga__attribute__c where name = : system.label.Order_Line and cscfga__product_configuration__c = :parentConfiguration.Id];
        //system.debug('connectConfigurations mategr c6 attribute7 ' + attribute7);
        
        String prefixUrl;
        
        if(String.isNotBlank(Network.getNetworkId())){
            prefixUrl=csOrderSetting.Community_MLE_URL__c;
        }else{
            prefixUrl = csOrderSetting.MLE_Org_URL__c;
        }
        
       // attribute7.cscfga__Value__c = '<iframe id="mle-iframe" src="'+prefixUrl+'?batchSize='+csOrderSetting.MLE_BatchSize__c+'&id=' + basketId + '&productDefinitionId=' +csOrderSetting.OrderLineItemProductDefinitionLineId__c +'&showHeader='+csOrderSetting.MLE_ShowHeader__c+'&sideBar='+csOrderSetting.MLE_SideBar_Selected__c+'&embedded='+csOrderSetting.MLE_Embedded__c+'&cssoverride='+csOrderSetting.CSS_Resource_Path__c+'&scriptplugin='+csOrderSetting.Javascript_Resource_Path__c + '&accid=' + account.id + '&screenFlowName=' + csOrderSetting.CurrentMleScreenFlow__c
        //+'" width="'+csOrderSetting.MLE_IFRAME_WIDTH__c+'" height="'+csOrderSetting.MLE_HEIGHT__c+'" frameBorder="'+csOrderSetting.MLE_FrameBorder__c+'"></iframe>';
       
       
       
       //https://gdevap01-pumaenergyapac.cs57.force.com/apex/csmle__Editor
        //attribute7.cscfga__Value__c = '<script src=\"https://ajax.googleapis.com/ajax/libs/angularjs/1.2.9/angular.min.js\"><div ng-app=\"MyApp\" ng-controller=\"MainController\"><a href=\"#\" class=\"button\" ng-click=\"addRow()\">Add Row </a><a href=\"#\" class=\"button\" ng-click=\"removeRow()\">Remove Row </a><table><thead><th width=\"100\" ng-repeat=\"header in headerList\">{{header}}</th></tr></thead><tbody><tr ng-repeat=\"item in items\" ng-if=\"item.show\"><td ><input type=\"checkbox\" ng-model=\"item.selected\" name=\"rovvalue\" id=\"{{$index}}\" value=\"{{$index}}\">row_cb_{{$index}}/></input></td><td><span ng-hide=\"item.editing\" ng-dblclick=\"editItem(item)\">{{item.name}}</span><input ng-show=\"item.editing\" ng-model=\"item.name\" ng-blur=\"doneEditing(item)\" ></input></td><td><select><option ng-repeat=\"p in item.products\" id=\"{{p}}\">{{p}}</option></select></td></tr></tbody></table></script></div>';

        //attribute7.cscfga__Value__c = '<iframe id="mle-iframe" src="https://pumaenergyapac--gdevap01--c.cs57.visual.force.com/apex/CustomMLE' + '?basketId=' + basketId + '"></iframe>';

       //system.debug('connectConfigurations mategr c7 attribute7 ' + attribute7);
       //system.debug('connectConfigurations mategr c8 attribute7.cscfga__Value__c ' + attribute7.cscfga__Value__c);
       //attribute7.cscfga__Value__c = '<p id="addLineItemRow">Add new row</p><br/><table id="lineitemtable"><thead><tr><th>Saved</th><th>Status</th><th>Product</th><th>Min</th><th>Max</th><th>Quantity</th><th>List Price</th><th>Price</th><th>Inventory</th><th>Vat</th><th>Freight</th></tr></thead></table>';
        // Load the attributes into a list and commit changes

        //attribute7.cscfga__Value__c = '<button id="addLineItemRow">Add new row</button><br/><table id="lineitemtable"><thead><tr><th>Saved</th><th>Status</th><th>Product</th><th>Min</th><th>Max</th><th>Quantity</th><th>List Price</th><th>Price</th><th>Inventory</th><th>Vat</th><th>Freight</th></tr></thead></table><script type="text/javascript">var t = jQuery("#lineitemtable").DataTable();var counter = 1;jQuery("#addLineItemRow").on( "click", function () {t.row.add( [counter +".1",counter +".2",counter +".3",counter +".4",counter +".5",counter +".6",counter +".7",counter +".8",counter +".9",counter +".10",counter +".11"] ).draw( false );counter++;} );</script>';
        //attributes.add(attribute1);
        system.debug('connectConfigurations mategr c9');
        attributes.add(attribute7);
        //system.debug('connectConfigurations mategr c10');
        //update attributes;
        system.debug('connectConfigurations mategr c11');
    }
    
   
}