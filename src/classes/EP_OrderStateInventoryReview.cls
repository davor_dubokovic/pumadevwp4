/*   
     @Author Aravindhan Ramalingam
     @name <EP_OrderStateInventoryReview.cls>     
     @Description <Order State for Inventory Review. common for all order type
                   Override if there is any change>   
     @Version <1.1> 
     */

     public class EP_OrderStateInventoryReview extends EP_OrderState {

        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            super.setOrderDomainObject(currentOrder);
        }

        
        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_OrderStateInventoryReview','doOnEntry');
        }
        
        public override void doOnExit(){
            EP_GeneralUtility.Log('Public','EP_OrderStateInventoryReview','doOnExit');
            EP_OrderService orderService = new EP_OrderService(this.order);
            orderService.submitForInventoryApproval();
        }
        

        public static String getTextValue()
        {
            return EP_OrderConstant.OrderState_Awaiting_Inventory_Review;
        }
    }