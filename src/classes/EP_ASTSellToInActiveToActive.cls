public class EP_ASTSellToInActiveToActive extends EP_AccountStateTransition {
    
    public EP_ASTSellToInActiveToActive() {
        finalstate = EP_AccountConstant.ACTIVE;
    }
    
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToInActiveToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToInActiveToActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
    
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToInActiveToActive','isGuardCondition');
        //No guard conditions for Inactive to Active
        return true;
    }
}