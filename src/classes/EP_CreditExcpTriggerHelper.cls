/* 
  @Author <Accenture>
   @name <EP_CreditExcpTriggerHelper>
   @CreateDate <08-06-2016>
   @Description <This class handles request from EP_Credit_Exception_Request__c trigger> 
   @Version <1.0>
   */
   public with sharing class EP_CreditExcpTriggerHelper {
     private static final String CLASS_NAME = 'EP_CreditExcpTriggerHelper';
     private static final String EXECUTE_SFDC_TO_NAV = 'executeSFDCTONAV';
     private static final String UPDATE_ORDER_STATUS = 'updateOrderStatus';
     private static final String UPDATE_CRED_EXCP_STATUS = 'updateCredExcpStatus';
     private static final String isSyncRequired_Method = 'sendSyncRequestToNAV';
     private static final String creditExceptionToOrderStatusUpdate_Method = 'creditExceptionToOrderStatusUpdate';
     private static final string NAV = 'NAV'; // Defect 28272
     private static final string CREDEXCEPTION_SYNC = 'CREDEXCEPTION_SYNC'; // Defect 28272

    //45435 Perform Credit Exception Review --Start
    /**
    * @Author       Accenture
    * @Name         sendSyncRequestToNAV
    * @Date         19/12/2017
    * @Description  This method Sends the credit exception request to NAV
    * @Param        Set<Id> , String
    * @return       NA
    */
    public static void sendSyncRequestToNAV(map<Id,EP_Credit_Exception_Request__c> mapNewCreditException,
                                              map<Id,EP_Credit_Exception_Request__c> mapOldCreditException, string companyCode) {

      EP_GeneralUtility.Log('Public','EP_CreditExcpTriggerHelper','sendSyncRequestToNAV');
      try{
          list<id> childIdList = new list<id>(); 
          EP_OutboundMessageService outboundService;
          for(EP_Credit_Exception_Request__c objNewCreditExcep  : mapNewCreditException.values()) {
            if (isSyncRequired(objNewCreditExcep,mapOldCreditException))  {
              outboundService = new EP_OutboundMessageService(objNewCreditExcep.Id, EP_Common_Constant.SEND_CREDIT_EXCEPTION_REQUEST, companyCode);
              outboundService.sendOutboundMessage(CREDEXCEPTION_SYNC,childIdList);
            }
          }              
      }
      catch(Exception excep){
        EP_LoggingService.logHandledException (excep, EP_Common_Constant.EPUMA, isSyncRequired_Method, CLASS_NAME, ApexPages.Severity.ERROR);
      }
    }

    /**
    * @Author       Accenture
    * @Name         isSyncRequired
    * @Date         19/12/2017
    * @Description  This method checks if the credit exception request needs to send to NAV
    * @Param        EP_Credit_Exception_Request__c , map<Id,EP_Credit_Exception_Request__c>
    * @return       NA
    */  
    @TestVisible
    private static boolean isSyncRequired(EP_Credit_Exception_Request__c objNewCreditExcep,
                                                            map<Id,EP_Credit_Exception_Request__c> mapOldCreditException ) {
      EP_GeneralUtility.Log('Public','EP_CreditExcpTriggerHelper','isSyncRequired');
      return ((objNewCreditExcep.EP_Integration_Status__c == null && objNewCreditExcep.EP_Status__c == null) 
              || (mapOldCreditException != null && mapOldCreditException.get(objNewCreditExcep.Id).EP_Status__c != objNewCreditExcep.EP_Status__c 
                                                && objNewCreditExcep.EP_Status__c == EP_OrderConstant.CANCELLED_STATUS));
    }
    //45435 Perform Credit Exception Review --End
    
    /**
    * @Author       CloudSense
    * @Name         creditExceptionToOrderStatusUpdate
    * @Date         28/03/2018
    * @Description  This method update Order Status
    * @Param        Set<Id> , String
    * @return       NA
    */
    public static void creditExceptionToOrderStatusUpdate(map<Id,EP_Credit_Exception_Request__c> mapNewCreditException) {

      EP_GeneralUtility.Log('Public','EP_CreditExcpTriggerHelper','creditExceptionToOrderStatusUpdate');
      List<csord__Order__c> orders = new List<csord__Order__c>();
      try{          
          for(EP_Credit_Exception_Request__c objNewCreditExcep  : mapNewCreditException.values()) {  
            csord__Order__c objOrder = new csord__Order__c(id=objNewCreditExcep.EP_CS_Order__c);        
            if (objNewCreditExcep.EP_Status__c == EP_Common_Constant.CREDIT_EXC_STATUS_APPROVED) {
                objOrder.CE_Approved_Flag__c = true;
            } else  if (objNewCreditExcep.EP_Status__c == EP_Common_Constant.CREDIT_EXC_STATUS_REJECTED)  {
                objOrder.csord__Status2__c = EP_Common_Constant.ORDER_DRAFT_STATUS ;
            }else if (objNewCreditExcep.EP_Status__c == EP_Common_Constant.CANCELLED_STATUS) {
                objOrder.csord__Status2__c = EP_Common_Constant.CANCELLED_STATUS;
            }
            orders.add(objOrder);
          }
          if (orders != null && !orders.isEmpty()) {
              update orders;
          }       
      }
      catch(Exception excep){
        EP_LoggingService.logHandledException (excep, EP_Common_Constant.EPUMA, creditExceptionToOrderStatusUpdate_Method, CLASS_NAME, ApexPages.Severity.ERROR);
      }
    }
}