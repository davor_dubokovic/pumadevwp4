/*
@Author      CloudSense
@Name        CreateBasketAndOfferFromOrderController
@CreateDate  08/05/2018
@Description Test class for the controller: CreateBasketAndOfferFromOrderController
@Version     1.0
*/
@isTest
private class CreateBasketAndOfferFromOrderControllerT {
	
	@isTest
	static void connectConfigurations_test() {
		//1) Insert test Account:
		Account acc = new Account(Name = 'testAccount', billingCountry = 'US');
		INSERT acc;
		
		//2) Use method from EP_TestDataUtility, which inserts records of Order and Order Line Item:
		// It also inserts a record of the Basket object
		List<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
		//Select an Order from the Wrapper:
		csord__Order__c order = orderWrapperList[0].SFOrder;
		//Link the Order to the inserted Account:
		order.csord__Account__c = acc.Id;
		UPDATE order;
		
		//2.1) Get Basket inserted by the previous method
		cscfga__Product_Basket__c basket = [SELECT Id, Name, csbb__Account__c FROM cscfga__Product_Basket__c ];
		
		//3) Use method from EP_TestDataUtility, to insert a record of the Offer object:
		cscfga__Configuration_Offer__c offer = EP_TestDataUtility.createOffer();
		
		//4) Insert the Custom Setting that stores the Name of the Parent Order:
		CS_ORDER_SETTINGS__c csOrderSetting = EP_TestDataUtility.createCSOrderCustomSetting(null);
		csOrderSetting.Parent_Order_Name__c = order.Id;
		csOrderSetting.OfferTemplateId__c = offer.Id;
		UPDATE csOrderSetting;
		
		//5) Insert record of Product Configuration with method EP_TestDataUtility:
		cscfga__product_configuration__c parentConfiguration = EP_TestDataUtility.createProductConfiguration(csOrderSetting.Parent_Order_Name__c, offer.Id);
		parentConfiguration.cscfga__Product_Basket__c = basket.Id;
		UPDATE parentConfiguration;
		
		//6) Instantiate a new Apex Page Standard Controller:
		ApexPages.StandardController sc = new ApexPages.StandardController(order);
		
		Test.startTest();
			//7) Assign the tested Class as the controller of the new Apex Page:
			CreateBasketAndOfferFromOrderController controller = new CreateBasketAndOfferFromOrderController(sc);
			
			//7.1) Set the public variable basketId with the Id of the previously inserted Basket:
			controller.basketId = basket.Id;
			
			//8) Call the method (being tested) that returns the Page Reference:
			controller.connectConfigurations();
		Test.stopTest();
		System.assertEquals(basket.Id, controller.basketId);
		System.assertEquals(parentConfiguration.Id, controller.parentConfiguration.Id);
		
	}
	

	@isTest
	static void createBasketForOpenDrawdownOrder_test() {
		//1) Insert test Account:
		Account acc = new Account(Name = 'testAccount', billingCountry = 'US');
		INSERT acc;
		
		//2) Use method from EP_TestDataUtility, which inserts records of Order and Order Line Item:
		// It also inserts a record of the Basket object (which we don't want because a method in the controller already inserts a record of Basket)
		List<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
		//Select an Order from the Wrapper:
		csord__Order__c order = orderWrapperList[0].SFOrder;
		//Link the Order to the inserted Account:
		order.csord__Account__c = acc.Id;
		UPDATE order;
		
		//2.1) Delete Basket inserted by the previous method
		// (because a method in the controller will already create a Basket when it is invoked up next):
		cscfga__Product_Basket__c basket = [SELECT Id, Name, csbb__Account__c FROM cscfga__Product_Basket__c ];
		DELETE basket;
		
		//3) Use method from EP_TestDataUtility, to insert a record of the Offer object:
		cscfga__Configuration_Offer__c offer = EP_TestDataUtility.createOffer();
		
		//4) Insert the Custom Setting that stores the Name of the Parent Order:
		CS_ORDER_SETTINGS__c csOrderSetting = EP_TestDataUtility.createCSOrderCustomSetting(null);
		csOrderSetting.Parent_Order_Name__c = order.Id;
		csOrderSetting.OfferTemplateId__c = offer.Id;
		UPDATE csOrderSetting;
		
		//5) Insert record of Product Configuration with method EP_TestDataUtility:
		cscfga__product_configuration__c parentConfiguration = EP_TestDataUtility.createProductConfiguration(csOrderSetting.Parent_Order_Name__c, offer.Id);
		
		//6) Instantiate a new Apex Page Standard Controller:
		ApexPages.StandardController sc = new ApexPages.StandardController(order);
		
		Test.startTest();
			//7) Instantiate the tested Class assign it as the controller of the new Apex Page:
			CreateBasketAndOfferFromOrderController controller = new CreateBasketAndOfferFromOrderController(sc);
	
			//8) Call the method (being tested) that returns the Page Reference:
			// >>> (This method will also insert a record of the Basket object) <<<
			PageReference configPage = controller.createBasketForOpenDrawdownOrder();
		Test.stopTest();
		
		String basketName = 'Order for ' + acc.Name;
		//Query Basket object and check if the field: csbb__Account__c has the Id of the test Account:
		List<cscfga__Product_Basket__c> basketList = [SELECT Id, Name, csbb__Account__c FROM cscfga__Product_Basket__c ];
		
		System.assertEquals(basketName, basketList[0].Name);
		System.assertEquals(acc.Id,  basketList[0].csbb__Account__c);
		
		//Query Product Configuration object and check if its Id is in the Page parameters:
		List<cscfga__product_configuration__c> parentConfigurationList = [SELECT id, name, cscfga__Product_Basket__c FROM cscfga__product_configuration__c
																		  WHERE name = : csOrderSetting.Parent_Order_Name__c
																		  AND cscfga__Product_Basket__c = :controller.basketId];
		
		System.assertEquals(parentConfigurationList[0].Id,  configPage.getParameters().get('configId'));
		System.assertEquals(controller.basketId,  configPage.getParameters().get('linkedId'));
		System.assertEquals(order.Id,  configPage.getParameters().get('parentOrderId'));

	}
	
	
}