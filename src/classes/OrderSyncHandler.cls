/*
   @Author          CloudSense
   @Name            OrderSyncHandler
   @CreateDate      16/10/2017
   @Description     This class is responsible for initiating Order Sync Callout
   @Version         1.1
 
*/

global class OrderSyncHandler implements CSPOFA.ExecutionHandler{
    
    /**
    * @Author       CloudSense
    * @Name         execute
    * @Date         02/09/2017
    * @Description  Method to process the step and and call Order Sync Outbound service
    * @Param        list<SObject>
    * @return       NA
    */  
    public List<sObject> process(List<SObject> data)
    {
        List<sObject> result = new List<sObject>();
        List<CSPOFA__Orchestration_Step__c> stepList= (List<CSPOFA__Orchestration_Step__c>)data;
        
        system.debug('StepList is :' +stepList);
        List<CSPOFA__Orchestration_Step__c> extendedList = [Select
                                                                id,CSPOFA__Orchestration_Process__r.Order__c,CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c,CSPOFA__Status__c,CSPOFA__Completed_Date__c,CSPOFA__Message__c
                                                            from 
                                                                CSPOFA__Orchestration_Step__c 
                                                            where 
                                                            id in :stepList];

        system.debug('extended list is :' +extendedList);
        // send order_sync with order ID                                     
        for(CSPOFA__Orchestration_Step__c step:extendedList){
            list<id> childIdList = new list<id>(); 
            system.debug('Order Id is :' +step.CSPOFA__Orchestration_Process__r.Order__c) ;
            
            try{
                    EP_OutboundMessageService outboundService = new EP_OutboundMessageService(step.CSPOFA__Orchestration_Process__r.Order__c, 'SFDC_TO_NAV_ORDER_SYNC', 'AAF');
                    outboundService.sendOutboundMessage('SFDCTONAVORDER_SYNC',childIdList);
            }Catch(Exception e){
                
             system.debug('exception is :' + e);
             EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, 'process', 'CreateCreditExceptionHandler',apexPages.severity.ERROR);
            
          }
          
           step.CSPOFA__Status__c ='Complete';
           step.CSPOFA__Completed_Date__c=Date.today();
           step.CSPOFA__Message__c = 'Custom step succeeded';
           result.add(step);
            
        }
        system.debug(' final result is :' +result);
     return result;        
        
        
    }
        
      
}