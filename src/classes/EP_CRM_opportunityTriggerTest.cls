/* ================================================
 * @Class Name : EP_CRM_opportunityTriggerTest
 * @author : Kamendra Singh
 * @Purpose: This class is used to test EP_CRM_opportunityTrigger apex trigger.
 * @created date: 29/07/2016
 ================================================*/
@isTest
private class EP_CRM_opportunityTriggerTest{
     /* This method is used to create all test data and test after insert Account update it's all opportunity scenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testbeforeInsertAndUpdateOpportunity(){
        try{
            //Run as Sales User
            Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' LIMIT 1]; 
            User u = EP_TestDataUtility.createUser(p.Id);            
            Database.SaveResult saveUser = Database.insert(u);            
            System.runAs(u){
            //Insert Payment Method....
            EP_Payment_Method__c objTempPayMeth = new EP_Payment_Method__c();
            objTempPayMeth.Name = 'Test payment method';
            objTempPayMeth.EP_Payment_Method_Code__c = '001ABC';
            
            Database.SaveResult savePaymentMeth = Database.insert(objTempPayMeth);
            
            //Insert Payment Term......
            EP_Payment_Term__c objTempPayTerm = new EP_Payment_Term__c();
            objTempPayTerm.Name = 'Test payment method';
            objTempPayTerm.EP_Payment_Term_Code__c = '001ABC';
            
            Database.SaveResult savePaymentTerm = Database.insert(objTempPayTerm);
            
            // Insert Account
            Account acc = new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';            
            //acc.EP_Payment_Method__c = objTempPayMeth.id;
            //acc.EP_Payment_Term_Lookup__c = objTempPayTerm.id;
            //acc.EP_Delivery_Type__c = 'Ex-Rack';
            //acc.EP_Billing_Method__c = 'Per Order';

            Database.SaveResult saveAcc = Database.insert(acc);
            
            //Insert Opportunity
            Opportunity objopp=new Opportunity();
            objopp.name='Test';
            objopp.Stagename='Prospecting';
            objopp.Accountid =acc.id;
            objopp.EP_CRM_Credit_Approval_Received__c = true;
            objopp.CloseDate=Date.Today() + 5;
            objopp.Description = 'Test Description';
            objopp.Probability = 5;
            objopp.LeadSource = 'Phone';
            objopp.EP_CRM_Region__c = 'AU';
            objopp.EP_CRM_State__c = 'ACT';               
            Database.SaveResult saveObjOpp = Database.insert(objopp);
            
            Opportunity opp=new Opportunity();
            opp.name='Test';
            opp.Stagename='Prospecting';
            opp.Accountid =acc.id;
            opp.EP_CRM_Credit_Approval_Received__c = true;
            opp.CloseDate=Date.Today() + 5;
            opp.Description = 'Test Description';
            opp.Probability = 5;
            opp.LeadSource = 'Phone';
            opp.EP_CRM_Region__c = 'AU';
            opp.EP_CRM_State__c = 'ACT';
            opp.EP_CRM_Payment_Method__c = objTempPayMeth.id;
            opp.Payment_Term__c = objTempPayTerm.id;
            opp.Delivery_Type__c = 'Ex-Rack';
            opp.Billing_Method__c = 'Per Order';
            
            Database.SaveResult saveOpp = Database.insert(opp);
            opportunity tempOpp = [select id from opportunity where id =:opp.id];
            system.Assert(tempOpp.id != null);
            
            //update Opportunity ..........
            opp.Delivery_Type__c = 'Delivery';
            opp.Billing_Method__c = 'Periodic';
            
            Database.SaveResult updateAcc = Database.update(opp);

            Test.startTest();
            //Check System assert---------
            Account tempAcc1 = [select id,EP_Payment_Method__c,EP_Payment_Term_Lookup__c,EP_Delivery_Type__c,EP_Billing_Method__c from Account where Id= : acc.Id limit 1];
            //validate through system Assert  
            System.Assert(tempAcc1.EP_Payment_Method__c != null);  
            System.Assert(tempAcc1.EP_Payment_Term_Lookup__c != null);  
            System.Assert(tempAcc1.EP_Delivery_Type__c != null);
            System.Assert(tempAcc1.EP_Billing_Method__c != null);   
            test.stopTest();
            }
        }
        catch(exception ex){ex.getmessage();}
    }
    
    public static testMethod void test1afterInsertOpportunity(){
        List<opportunity> opp = new List<Opportunity>();
        //  Modified by Hasmukh Jain on 19-Feb as Method does not exist afterInsertOpportunity so replaced with correct  method afterInsertConnectionOpportunity
        //EP_CRM_opportunityTriggerHandler.afterInsertOpportunity(opp);
        EP_CRM_opportunityTriggerHandler.afterInsertConnectionOpportunity(opp);
    }
    public static testMethod void test2afterInsertOpportunity(){
        List<opportunity> opp;
        //  Modified by Hasmukh Jain on 19-Feb as Method does not exist afterInsertOpportunity so replaced with correct  method afterInsertConnectionOpportunity
        //EP_CRM_opportunityTriggerHandler.afterInsertOpportunity(opp);
        EP_CRM_opportunityTriggerHandler.afterInsertConnectionOpportunity(opp);
    }
    public static testMethod void test1afterUpdateOpportunity(){
        List<opportunity> opp = new List<Opportunity>();
        //EP_CRM_opportunityTriggerHandler.afterUpdateOpportunity(opp);        
    }
    public static testMethod void test2afterUpdateOpportunity(){
        List<opportunity> opp;
        //EP_CRM_opportunityTriggerHandler.afterUpdateOpportunity(opp);        
    }
    public static testMethod void test1beforeInsertAndUpdateOpportunity(){
        List<opportunity> opp = new List<Opportunity>();
        //EP_CRM_opportunityTriggerHandler.beforeInsertAndUpdateOpportunity(opp);
    }   
    public static testMethod void test2testbeforeInsertAndUpdateOpportunity(){
        
        //Run as Sales User
            Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' LIMIT 1]; 
            User u = EP_TestDataUtility.createUser(p.Id);      
            
            Database.SaveResult saveUser = Database.insert(u);
            System.runAs(u){
        
        //Insert Payment Method....
            EP_Payment_Method__c objTempPayMeth = new EP_Payment_Method__c();
            objTempPayMeth.Name = 'Test payment method';
            objTempPayMeth.EP_Payment_Method_Code__c = '001ABC';
            
            Database.SaveResult savePaymentMeth = Database.insert(objTempPayMeth);
            
            //Insert Payment Term......
            EP_Payment_Term__c objTempPayTerm = new EP_Payment_Term__c();
            objTempPayTerm.Name = 'Test payment method';
            objTempPayTerm.EP_Payment_Term_Code__c = '001ABC';
            
            Database.SaveResult savePaymentTerm = Database.insert(objTempPayTerm);
            // Insert Account
            Account acc = new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';            
            acc.EP_CRM_Geo1__c = 'Africa';
            acc.EP_CRM_Geo2__c = 'Ghana';

            Database.SaveResult saveAcc = Database.insert(acc);
            
            //Insert Opportunity
            Opportunity objopp=new Opportunity();
            objopp.name='Test';
            objopp.Stagename='Prospecting';
            objopp.Accountid =acc.id;
            objopp.EP_CRM_Credit_Approval_Received__c = false;
            objopp.CloseDate=Date.Today() + 5;
            objopp.Description = 'Test Description';
            objopp.Probability = 5;
            objopp.LeadSource = 'Phone';
            objopp.EP_CRM_Region__c = 'AU';
            objopp.EP_CRM_State__c = 'ACT'; 
            objopp.EP_CRM_Geo1__c = 'Africa';
            objopp.EP_CRM_Geo2__c = 'Ghana';
            Database.SaveResult saveObjOpp = Database.insert(objopp);
            
            Opportunity opp=new Opportunity();
            opp.name='Test';
            opp.Stagename='Prospecting';
            opp.Accountid =acc.id;
            opp.EP_CRM_Credit_Approval_Received__c = false;
            opp.CloseDate=Date.Today() + 5;
            opp.Description = 'Test Description';
            opp.Probability = 5;
            opp.LeadSource = 'Phone';
            opp.EP_CRM_Region__c = 'AU';
            opp.EP_CRM_State__c = 'ACT';
            opp.EP_CRM_Payment_Method__c = objTempPayMeth.id;
            opp.Payment_Term__c = objTempPayTerm.id;
            opp.Delivery_Type__c = 'Ex-Rack';
            opp.Billing_Method__c = 'Per Order';
            opp.EP_CRM_Geo1__c = 'Africa';
            opp.EP_CRM_Geo2__c = 'Ghana'; 
            
            Database.SaveResult saveOpp = Database.insert(opp);
            opportunity tempOpp = [select id from opportunity where id =:opp.id];
            system.Assert(tempOpp.id != null);
            
            //update Opportunity ..........
            opp.Delivery_Type__c = 'Delivery';
            opp.Billing_Method__c = 'Periodic';
            
            Database.SaveResult updateAcc = Database.update(opp);       
            }
    }
}