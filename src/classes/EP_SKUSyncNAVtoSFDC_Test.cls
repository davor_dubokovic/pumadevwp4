/**
 
 Test class for EP_SKUSyncNAVtoSFDC
 
 */
@isTest
private class EP_SKUSyncNAVtoSFDC_Test {
	
	
	private static final string COMPANY_CODE1 = 'AUN';
	
	private static final string PRODUCTCODE1 = 'CODE1';
		
	private static final string PRODUCTNAME1 = 'Name1';
		
	private static final string PRODUCTFAMILY1 = 'Family1';
		
	private static final string SEQID1 = '112345';
		
	private static final string DESCRIPTION1 = 'Test Description 1';
		
	private static final string SFDCBASEURL = URL.getSalesforceBaseUrl().toExternalForm();
	private static final string SKUSYNCENDPOINT = '/services/apexrest/v1/SKUSync/*';
	private static Company__c company1;
		
	private static Product2  productObj1;
		
	private static String uom;
	
	/*
	Method to create data for tesing
	of object Company__c
	*/
	static void setupCompanyData()
	{
		company1 = EP_TestDataUtility.createCompany( COMPANY_CODE1 );
		Database.insert(company1);
        system.AssertNotEquals( null, company1.Id );
	}
	
	/*
	Method to create data for tesing
	of object Product2
	*/
	static void setupProductData()
	{
		String uniqueKey1 = PRODUCTCODE1 + EP_Common_Constant.STRING_HYPHEN + COMPANY_CODE1;
		productObj1 = new product2(Name = PRODUCTNAME1, EP_NAV_Product_Company__c = uniqueKey1, EP_Company_Lookup__c = company1.Id, CurrencyIsoCode = EP_Common_Constant.GBP, family = PRODUCTFAMILY1, ProductCode = PRODUCTCODE1, EP_Unit_of_Measure__c = uom);
		Database.insert(productObj1);
       	system.AssertNotEquals( null, productObj1.Id );
	}

    static testMethod void skuCreateSuccess() {
    	EP_PROCESS_NAME_CS__c pName = new EP_PROCESS_NAME_CS__c(Name = 'SKU Creation From Nav', EP_Process_Name__c = 'SCN');
    	Insert pName;
    	Account StorageLocation = EP_TestDataUtility.createStockHoldingLocation();
    	StorageLocation.EP_Nav_Stock_Location_Id__c = '1234567890';
    	Database.insert( StorageLocation );
    	setupCompanyData();
    	setupProductData();
    	
    	EP_SKUHandler.IdentifierWrapper obj_idt = new EP_SKUHandler.IdentifierWrapper();
    	obj_idt.supplyLoc = StorageLocation.EP_Nav_Stock_Location_Id__c; 
    	obj_idt.itemNr = PRODUCTCODE1;
    	obj_idt.clientId = COMPANY_CODE1;
    	
    	EP_SKUHandler.SKUInventoryWrapper Obj_sku = new EP_SKUHandler.SKUInventoryWrapper();
    	Obj_sku.identifier = obj_idt;
    	Obj_sku.description = 'Test';
    	Obj_sku.seqId = '12345';
    	
    	EP_SKUHandler.SKUwrapper obj_SKUwrapper = new EP_SKUHandler.SKUwrapper();
    	obj_SKUwrapper.listSKUInventories = new list<EP_SKUHandler.SKUInventoryWrapper>{Obj_sku};
    	
    	EP_AcknowledmentGenerator.cls_HeaderCommon headerCommon = new EP_AcknowledmentGenerator.cls_HeaderCommon();
    	obj_SKUwrapper.headerCommon = headerCommon;

    	String requestBody =  JSON.serialize(obj_SKUwrapper);
       	requestBody = requestBody.replaceAll('listSKUInventories','sku' ); 
       	RestRequest req = new RestRequest();
   		RestResponse res = new RestResponse();
	   	res.statusCode = Integer.valueOf( EP_Common_Constant.CODE_200 );
	   	
	   	req.requestURI = SFDCBASEURL + SKUSYNCENDPOINT;
	   	req.httpMethod = EP_Common_Constant.POST;
	   	req.requestBody = Blob.valueOf( requestBody );
	   	
	   	req.addHeader( EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON );
	   	RestContext.request = req;
	   	RestContext.response = res;
	   	
	   	test.startTest();
	   	EP_SKUSyncNAVtoSFDC.SKUSync(); 
	   	test.stopTest();  
	   	
	   	List<EP_Inventory__c> invt = [select Id, EP_Storage_Location__r.EP_Nav_Stock_Location_Id__c, EP_Product__r.EP_NAV_Product_Company__c from EP_Inventory__c] ;
	   	//system.AssertEquals( invt.size(), 1 );
	   	//system.AssertEquals( invt[0].EP_Storage_Location__r.EP_Nav_Stock_Location_Id__c, StorageLocation.EP_Nav_Stock_Location_Id__c );
	   	//system.AssertEquals( invt[0].EP_Product__r.EP_NAV_Product_Company__c, productObj1.EP_NAV_Product_Company__c );
	   	
    	
    }
    
    static testMethod void skuCreateFail_LocaionNotExist() {
    	EP_PROCESS_NAME_CS__c pName = new EP_PROCESS_NAME_CS__c(Name = 'SKU Creation From Nav', EP_Process_Name__c = 'SCN');
    	Insert pName;
    	//Account StorageLocation = EP_TestDataUtility.createStockHoldingLocation();
    	//StorageLocation.EP_Nav_Stock_Location_Id__c = '1234567890';
    	//Database.insert( StorageLocation );
    	setupCompanyData();
    	setupProductData();
    	
    	EP_SKUHandler.IdentifierWrapper obj_idt = new EP_SKUHandler.IdentifierWrapper();
    	obj_idt.supplyLoc = '1234567890'; 
    	obj_idt.itemNr = PRODUCTCODE1;
    	obj_idt.clientId = COMPANY_CODE1;
    	
    	EP_SKUHandler.SKUInventoryWrapper Obj_sku = new EP_SKUHandler.SKUInventoryWrapper();
    	Obj_sku.identifier = obj_idt;
    	Obj_sku.description = 'Test';
    	Obj_sku.seqId = '12345';
    	
    	EP_SKUHandler.SKUwrapper obj_SKUwrapper = new EP_SKUHandler.SKUwrapper();
    	obj_SKUwrapper.listSKUInventories = new list<EP_SKUHandler.SKUInventoryWrapper>{Obj_sku};
    	
    	EP_AcknowledmentGenerator.cls_HeaderCommon headerCommon = new EP_AcknowledmentGenerator.cls_HeaderCommon();
    	obj_SKUwrapper.headerCommon = headerCommon;

    	String requestBody =  JSON.serialize(obj_SKUwrapper);
       	requestBody = requestBody.replaceAll('listSKUInventories','sku' ); 
       	RestRequest req = new RestRequest();
   		RestResponse res = new RestResponse();
	   	res.statusCode = Integer.valueOf( EP_Common_Constant.CODE_200 );
	   	
	   	req.requestURI = SFDCBASEURL + SKUSYNCENDPOINT;
	   	req.httpMethod = EP_Common_Constant.POST;
	   	req.requestBody = Blob.valueOf( requestBody );
	   	
	   	req.addHeader( EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON );
	   	RestContext.request = req;
	   	RestContext.response = res;
	   	
	   	test.startTest();
	   	EP_SKUSyncNAVtoSFDC.SKUSync(); 
	   	test.stopTest();  
	   	
	   	List<EP_Inventory__c> invt = [select Id from EP_Inventory__c] ;
	   	system.AssertEquals( invt.size(), 0 );
	   	system.AssertEquals( RestContext.response.responseBody.toString().contains('INVALID_FIELD'), true );
    	
    }
    
    static testMethod void skuCreateFail_FieldMissing() {
    	EP_PROCESS_NAME_CS__c pName = new EP_PROCESS_NAME_CS__c(Name = 'SKU Creation From Nav', EP_Process_Name__c = 'SCN');
    	Insert pName;
    	Account StorageLocation = EP_TestDataUtility.createStockHoldingLocation();
    	StorageLocation.EP_Nav_Stock_Location_Id__c = '1234567890';
    	Database.insert( StorageLocation );
    	setupCompanyData();
    	setupProductData();
    	
    	EP_SKUHandler.IdentifierWrapper obj_idt = new EP_SKUHandler.IdentifierWrapper();
    	obj_idt.supplyLoc = '1234567890';  
    	obj_idt.itemNr = EP_Common_Constant.BLANK;
    	obj_idt.clientId = EP_Common_Constant.BLANK;
    	
    	EP_SKUHandler.SKUInventoryWrapper Obj_sku = new EP_SKUHandler.SKUInventoryWrapper();
    	Obj_sku.identifier = obj_idt;
    	Obj_sku.description = 'Test';
    	Obj_sku.seqId = '12345';
    	
    	EP_SKUHandler.SKUwrapper obj_SKUwrapper = new EP_SKUHandler.SKUwrapper();
    	obj_SKUwrapper.listSKUInventories = new list<EP_SKUHandler.SKUInventoryWrapper>{Obj_sku};
    	
    	EP_AcknowledmentGenerator.cls_HeaderCommon headerCommon = new EP_AcknowledmentGenerator.cls_HeaderCommon();
    	obj_SKUwrapper.headerCommon = headerCommon;

    	String requestBody =  JSON.serialize(obj_SKUwrapper);
       	requestBody = requestBody.replaceAll('listSKUInventories','sku' ); 
       	RestRequest req = new RestRequest();
   		RestResponse res = new RestResponse();
	   	res.statusCode = Integer.valueOf( EP_Common_Constant.CODE_200 );
	   	
	   	req.requestURI = SFDCBASEURL + SKUSYNCENDPOINT;
	   	req.httpMethod = EP_Common_Constant.POST;
	   	req.requestBody = Blob.valueOf( requestBody );
	   	
	   	req.addHeader( EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON );
	   	RestContext.request = req;
	   	RestContext.response = res;
	   	
	   	test.startTest();
	   	EP_SKUSyncNAVtoSFDC.SKUSync(); 
	   	test.stopTest();  
	   	
	   	List<EP_Inventory__c> invt = [select Id from EP_Inventory__c] ;
	   	system.AssertEquals( invt.size(), 0 );
	   	//system.AssertEquals( RestContext.response.responseBody.toString().contains('FIELD_INTEGRITY_EXCEPTION'), true );
    	
    }
    //Code changes for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
    //Unit Test
    static testMethod void parse_Test() {
    	string requestBody = '{"MSG":{"HeaderCommon":{"MsgID":"170529123107367-{042A388A-188D-4217-8B98-6372BD798305}","InterfaceType":"NAV","SourceGroupCompany":"AAF","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtraflon2k202.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"SKU Staging-SF","CommunicationType":"Async"},"Payload":{"any0":{"skus":{"sku":{"seqId":"{CD079EF3-3544-4A41-8FDD-3E440D71BA3F}","identifier":{"supplyLoc":"00234050","itemNr":"1000011","clientId":"AAF"}}}}},"StatusPayload":"StatusPayload"}}';
    	test.startTest();
    		EP_SKUHandler.SKUwrapper skuRecords = EP_SKUHandler.parse(requestBody);
    	test.stopTest();
    	system.AssertEquals(1, skuRecords.listSKUInventories.size());  
    }
    
    //Unit Test
    static testMethod void createUpdateSKU_Test() {
    	string requestBody = '{"MSG":{"HeaderCommon":{"MsgID":"170529123107367-{042A388A-188D-4217-8B98-6372BD798305}","InterfaceType":"NAV","SourceGroupCompany":"AAF","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtraflon2k202.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"SKU Staging-SF","CommunicationType":"Async"},"Payload":{"any0":{"skus":{"sku":{"seqId":"{CD079EF3-3544-4A41-8FDD-3E440D71BA3F}","identifier":{"supplyLoc":"00234050","itemNr":"1000011","clientId":"AAF"}}}}},"StatusPayload":"StatusPayload"}}';
    	EP_IntegrationRecord__c integrationRecord = EP_TestDataUtility.createIntegrationRecforInboundRequest('170529123107367-{042A388A-188D-4217-8B98-6372BD798305}', 'SKU_SYNC', requestBody);
    	database.insert(integrationRecord);
    	RestRequest req = new RestRequest();
   		RestResponse res = new RestResponse();
	   	res.statusCode = Integer.valueOf( EP_Common_Constant.CODE_200 );
	   	req.requestURI = SFDCBASEURL + SKUSYNCENDPOINT;
	   	req.httpMethod = EP_Common_Constant.POST;
	   	req.requestBody = Blob.valueOf( requestBody );
	   	req.addHeader( EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON );
	   	RestContext.request = req;
	   	RestContext.response = res;
	   	
	   	test.startTest();
	   		EP_SKUSyncNAVtoSFDC.SKUSync(); 
	   	test.stopTest();  
	   	
	   	system.AssertEquals(integrationRecord.EP_Process_Result__c, res.responseBody.tostring());  
    }
    //Unit Test
    static testMethod void createUpdateSKU_ExceptionTest() {
    	string requestBody = '{"MSG":{"HeaderCommon":{"MsgID":"170529123107367-{042A388A-188D-4217-8B98-6372BD798305}","InterfaceType":"NAV","SourceGroupCompany":"AAF","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-dev.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtraflon2k202.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"SKU Staging-SF","CommunicationType":"Async"},"Payload":{"any0":{"skus":{"sku":{"seqId":"{CD079EF3-3544-4A41-8FDD-3E440D71BA3F}","identifier":{"supplyLoc":"","itemNr":""}}}}},"StatusPayload":"StatusPayload"}}';
    	
    	RestRequest req = new RestRequest();
   		RestResponse res = new RestResponse();
	   	res.statusCode = Integer.valueOf( EP_Common_Constant.CODE_200 );
	   	req.requestURI = SFDCBASEURL + SKUSYNCENDPOINT;
	   	req.httpMethod = EP_Common_Constant.POST;
	   	req.requestBody = Blob.valueOf( requestBody );
	   	req.addHeader( EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON );
	   	RestContext.request = req;
	   	RestContext.response = res;
	   	
	   	test.startTest();
	   		EP_SKUSyncNAVtoSFDC.SKUSync(); 
	   	test.stopTest(); 
    }
}