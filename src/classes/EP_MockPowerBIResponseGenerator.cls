@isTest
global  class EP_MockPowerBIResponseGenerator implements HttpCalloutMock{

    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
       
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
       
         res.setBody('{"tokenId":"90c27e39-888d-48fb-a2ed-f97ea2bf227b","token":"H4sIAAAAAAAEAB2WRQ7sCBJE7_K3NZKZRuqFmdkuw87MzG7N3ae69yFl6ikzIv7-YyVPPyX5n__-gQcV8t9dWx_6JkvBsvPHINWV_oqFEyK0nrzyaolE91Z-V2HsNlvblvM07RTtk43RAZPz011qDZwME-Y2ow4ZlWll_G2wWeEVI2QOclN7fchQYU1QnaaVZJlrS7AuyRdA05x8GGxtZRY2y_behZwcj_aWKDcIFlhxdmC_jVAPnznqWdqiS3kHa5wGTIzcvD6r8aLv2LUuNSGj2uWF9JgG1jVYVts9vhgb4ag-37I4uZok-APDdowW4ybVFGa77kT-kQXMZ5cKajcSc7IDddZUQ1EmAxKUmPXvB59HlI2VegBBO4f12p2ieMcgPkTc9HrTaXzvnWjAjrO9YBRq8iQ1b7ennaoQOl0bddJAjQsH-duiW1ZEuZKOb3d7oz6fh5EhK1xlEFDNxZnShFrZFlOL5tALDnYAWTPnOCVabUudnp7KqN40mwjWak9W8JAqtN7n0p2mPR47wDvPgANPUR8joMWj0uJ5fPdtOxBzhyX0BTgRVYdVfS9GP2OSIkOyYddmNEF1sbdcSScODTvcQ-IUaPxOb2iThf11oBp4JcOujnD1raY52djaNMas7HUF_E1JCTuslS2Y6evO0iAmAMUquyjBP7qdGQGxyhZt6RoQVtZU749k8QcaGBfci5xfwEPTpnxLGdFa9m8JTU4itzuubfLi9MJaDueldcLQyvUbUnKUZ4UcznUCftxMfEijLpOLPYeljWVghytjcV4KaqUQ2tEEF6nGPBCd3p9uQNPTa-yaZYHcHJtGS1_G7aqA-UQFs9A0IxvUuGePT9","REPORTID":"2f88b05a-fac1-4b55-a23c-77227a5ef97f","PowerBIURL":"https://api.powerbi.com/v1.0/myorg/groups/d126a7df-d739-4fd0-893f-d7de2045e612/reports/2f88b05a-fac1-4b55-a23c-77227a5ef97f","GROUPID":"d126a7df-d739-4fd0-893f-d7de2045e612","expiration":"2018-01-29T12:20:07Z","context":"null","access_token":"##$34343434fsdkfj#@#@$@$@$","refresh_token":"894309584385#$%#$%","expires_on":"1515669924"}');
        res.setStatusCode(200);
        return res;
    }

}