@isTest
public class TrialCustomerPortalHomePController_Test {
  public static testMethod void SortTest(){
        //Run as Sales User
        Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' LIMIT 1]; 
        User u = EP_TestDataUtility.createUser(p.Id);            
        Database.SaveResult saveUser = Database.insert(u);            
        System.runAs(u){
        
            EP_Country__c country = new EP_Country__c(EP_Country_Code__c = '101', EP_Region__c = 'India');
            insert country;
            Account acc = new Account(name='TESTREC', EP_Country__c = country.Id, BillingStreet='street', BillingCity='city', BillingState='state', BillingPostalCode='123',
            BillingCountry='in',
            ShippingStreet='street', ShippingCity='city', ShippingState='state', ShippingPostalCode='123',
            ShippingCountry='in',EP_CRM_Geo1__c = 'Africa',EP_CRM_Geo2__c = 'Ghana');
            insert acc; 
            
            Asset newAsset = new Asset(
              CurrencyIsoCode = 'GBP',
                Name = 'Test Asset',
                AccountId = acc.Id
            );
            insert newAsset;
            
            Case newCase = new Case(
              Status = 'New Application',
                Origin = 'Email',
                CurrencyIsoCode = 'GBP'
            );
            insert newCase;
            
            Solution newSolution = new Solution(
              CurrencyIsoCode = 'GBP',
                IsPublished = false,
                SolutionNote = 'Test Solution',
                SolutionName = 'Test Solution',
                Status = 'Draft'
            );
            insert newSolution;
            list<Community> communityList = [Select c.Name, c.IsActive, c.Id From Community c Limit 1];
            
            if( !communityList.isEmpty() )
            {
                Idea newIdea = new Idea(
                    Body = 'Test Idea',
                    CurrencyIsoCode = 'GBP',
                    Status = 'Test',
                    Title = 'Test Idea',
                    CommunityId = communityList[0].Id
                );
                insert newIdea;
            }
            
            TrialCustomerPortalHomePageController TCPHPC = new TrialCustomerPortalHomePageController();
            TCPHPC.sortField1 = 'Name';
            TCPHPC.PrevioussortField1 = 'Name';
            TCPHPC.SortProducts();
            TCPHPC.sortField2 = 'CaseNumber';
            TCPHPC.PrevioussortField2 = 'CaseNumber';
            TCPHPC.SortCases();
            TCPHPC.sortField3 = 'SolutionName';
            TCPHPC.PrevioussortField3 = 'SolutionName';
            TCPHPC.SortSolutions();
            
            if( !communityList.isEmpty() )
            {
              TCPHPC.sortField4 = 'Title';
              TCPHPC.PrevioussortField4 = 'Title';
              TCPHPC.SortIdeas();
            }  
            
            TrialCustomerPortalHomePageController TCPHPC2 = new TrialCustomerPortalHomePageController();  
            TCPHPC2.sortField1 = 'Name';
            TCPHPC2.PrevioussortField1 = 'TestName'; 
            TCPHPC2.SortProducts(); 
            TCPHPC2.sortField2 = 'CaseNumber';
            TCPHPC2.PrevioussortField2 = 'TestCaseNumber';
            TCPHPC2.SortCases();
            TCPHPC2.sortField3 = 'SolutionName';
            TCPHPC2.PrevioussortField3 = 'TestSolutionName';
            TCPHPC2.SortSolutions();
            
            if( !communityList.isEmpty() )
            {
              TCPHPC2.sortField4 = 'Title';
              TCPHPC2.PrevioussortField4 = 'TestTitle';
              TCPHPC2.SortIdeas();
            } 
        }
    }
}