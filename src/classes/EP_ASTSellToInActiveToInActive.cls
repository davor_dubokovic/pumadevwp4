public class EP_ASTSellToInActiveToInActive extends EP_AccountStateTransition{
    
    public EP_ASTSellToInActiveToInActive() {
        finalstate = EP_AccountConstant.INACTIVE;
    }
    
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToInActiveToInActive','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToInActiveToInActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToInActiveToInActive','isGuardCondition');
        //No guard conditions for Active to Blocked
        return true;
    }

}