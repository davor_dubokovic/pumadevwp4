/**
 * @author <Vinay Jaiswal>
 * @name EP_CustomerPaymentRefundHandler 
 * @description <This class used to create 'Customer Payment' and 'Customer Account Statement Ttem' records For Payment Refund> 
 * @version <1.0>
 */
 public with sharing class EP_CustomerPaymentRefundHandler {

 	private static EP_GenericFinance_Json.EP_AcknowlegmentWrapper acknowledgementWrapper = NULL;
 	private static final String METHOD_NAME = 'syncCustomerPaymentRefund';
 	private static final String METHOD_NAME1 = 'createCustomerAccountStatementItem';
    /*Novasuite Fix Start*/
    Public static final String KEY = 'PR';
    /*Novasuite Fix end*/
	/**
	 * @author <Vinay Jaiswal>
	 * @description <This method is used to sync "Customer Refund" custom object recods in SFDC from NAV>
	 * @date <06/12/2016>
	 * @param List<EP_GenericFinance_Json.Document>
	 * @return EP_GenericFinance_Json.EP_AcknowlegmentWrapper
	 */
	 public static EP_GenericFinance_Json.EP_AcknowlegmentWrapper syncCustomerPaymentRefund(List<EP_GenericFinance_Json.Document> listOfDocument){
	 	EP_GeneralUtility.Log('Public','EP_CustomerPaymentRefundHandler','syncCustomerPaymentRefund');
	 	acknowledgementWrapper = new EP_GenericFinance_Json.EP_AcknowlegmentWrapper();
		//list of documents to be be inserted in SFDC
		List<EP_GenericFinance_Json.Document> listOfDocumentsToBeCreated = new List<EP_GenericFinance_Json.Document>();
		//list of documents to be be updated in SFDC
		List<EP_GenericFinance_Json.Document> listOfDocumentsToBeUpdated  = new List<EP_GenericFinance_Json.Document>();
		try{
			for(EP_GenericFinance_Json.Document document : listOfDocument){
				if(EP_Common_Constant.CREATE.equals(document.docState)){
					listOfDocumentsToBeCreated.add(document);
				}	
				else{ 
					if(EP_Common_Constant.APPLICATION.equals(document.docState)){
						listOfDocumentsToBeUpdated.add(document);
					}
				}
			}
			if(!listOfDocumentsToBeCreated.isEmpty()){
				upsertCustomerPaymentRefund(listOfDocumentsToBeCreated);
			}
			if(!listOfDocumentsToBeUpdated.isEmpty()){
				upsertCustomerPaymentRefund(listOfDocumentsToBeUpdated);
			}
		}
		catch(Exception ex){
			Database.rollback(EP_GenericFinance_Json.sp);
			Map<String, String> mapSeqIdErrorDescription = new Map<String, String>();
			for(EP_GenericFinance_Json.Document paymentRefundDocument : listOfDocument) {
				mapSeqIdErrorDescription.put(paymentRefundDocument.seqId, ex.getMessage());
			}
			EP_LoggingService.logHandledException (ex, EP_Common_Constant.EPUMA, EP_CustomerPaymentRefundHandler.class.getName(), METHOD_NAME, ApexPages.Severity.ERROR);
			acknowledgementWrapper = EP_GenericFinance_Json.fillSendAcknowledgementWrapper(ex, NULL, mapSeqIdErrorDescription);
		}
		return acknowledgementWrapper;
	}
	
	/**
	* @author <Vinay Jaiswal>
	* @description <This method is used to sync customer payment Refund>
	* @date <12/06/2016>
	* @param List<EP_GenericFinance_Json.Document>
	* @return void
	*/
	private static void upsertCustomerPaymentRefund(List<EP_GenericFinance_Json.Document> listOfDocument){
		EP_GeneralUtility.Log('Private','EP_CustomerPaymentRefundHandler','upsertCustomerPaymentRefund');
		Integer count;
		String compositkey ;
		List<Database.UpsertResult> listOfUpsertResult;
		Set<String> setPaymentRefundKeys = new Set<String>();
		Map<String, ID> mapExistingPaymentKeyId = new Map<String, ID>();
		Map<String,String> mapNavSeqIdErrorDescription = new Map<String,String>();
		Map<String,String> mapSeqIdCustomerPaymentId = new Map<String,String>();
		Set<String> setAccountNumbers = new Set<String>();
		Id paymentRefundRecordTypeId  = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMERPAYMENT_OBJECT,EP_Common_Constant.PAYMENT_REFUND_RECORD_TYPE_NAME);
		Integer noOfRows = EP_Common_Util.getQueryLimit();
		if(!listOfDocument.isEmpty()){
			for(EP_GenericFinance_Json.Document paymentRefundDocument : listOfDocument){
				//L4 #45304 Changes Start
				setPaymentRefundKeys.add(EP_DocumentUtil.generateUniqueKey(new Set<String>{paymentRefundDocument.identifier.billTo, paymentRefundDocument.identifier.docId, paymentRefundDocument.identifier.clientId, paymentRefundDocument.identifier.entryNr}));
				//L4 #45304 Changes End
				compositkey = paymentRefundDocument.identifier.clientId + EP_Common_Constant.HYPHEN +paymentRefundDocument.identifier.billTo;
				setAccountNumbers.add(compositkey);
			}
			if(!setPaymentRefundKeys.isEmpty()){
				for(EP_Customer_Payment__c customerPayment : [Select Id, EP_Payment_Key__c from EP_Customer_Payment__c where EP_Payment_Key__c IN : setPaymentRefundKeys limit: noOfRows ]){
					mapExistingPaymentKeyId.put(customerPayment.EP_Payment_Key__c , customerPayment.Id);
				}
			}
			//map containing containing 'AccountNumber' as key and 'Id' as value corresponding to a Set of specific RecordType 'DeveloperName' 
			Map<String, Id> acctNumIdMap = getAccountNumberIdMap(setAccountNumbers, new Set<String>{EP_Common_Constant.RT_BILL_TO_DEV_NAME, EP_Common_Constant.SELL_TO_DEV_NAME});
			
			EP_Customer_Payment__c customerPaymentObject;
			List<EP_Customer_Payment__c> listOfCustomerPaymentObjectsToBeUpserted = new List<EP_Customer_Payment__c>();
			for(EP_GenericFinance_Json.Document paymentRefundDocument : listOfDocument){
				if(paymentRefundDocument.amount.contains(EP_Common_Constant.COMMA)){
					paymentRefundDocument.amount = paymentRefundDocument.amount.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK);
				}
				if(paymentRefundDocument.remainingAmount.contains(EP_Common_Constant.COMMA)){
					paymentRefundDocument.remainingAmount = paymentRefundDocument.remainingAmount.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK);
				}
				if(!mapExistingPaymentKeyId.isEmpty()){
					//L4 #45304 Changes Start
					if(mapExistingPaymentKeyId.containsKey(EP_DocumentUtil.generateUniqueKey(new Set<String>{paymentRefundDocument.identifier.billTo, 
						paymentRefundDocument.identifier.docId, paymentRefundDocument.identifier.clientId, paymentRefundDocument.identifier.entryNr}))){
						Id recordId = (Id)mapExistingPaymentKeyId.get(EP_DocumentUtil.generateUniqueKey(new Set<String>{paymentRefundDocument.identifier.billTo, 
							paymentRefundDocument.identifier.docId, paymentRefundDocument.identifier.clientId, paymentRefundDocument.identifier.entryNr}));
						//L4 #45304 Changes End
						customerPaymentObject = generateCustomerPaymentRefundRecord(paymentRefundDocument, recordId, paymentRefundRecordTypeId, acctNumIdMap);
					}
					} else{
						customerPaymentObject = generateCustomerPaymentRefundRecord(paymentRefundDocument, NULL, paymentRefundRecordTypeId, acctNumIdMap);
					}
					listOfCustomerPaymentObjectsToBeUpserted.add(customerPaymentObject);
				}
				listOfUpsertResult = Database.Upsert(listOfCustomerPaymentObjectsToBeUpserted, EP_Customer_Payment__c.fields.EP_Payment_Key__c, true);
				for(count = 0 ; count < listOfUpsertResult.size(); count++){
					if(listOfUpsertResult[count].isSuccess()){
						mapSeqIdCustomerPaymentId.put(listOfCustomerPaymentObjectsToBeUpserted[count].EP_NavSeqId__c,listOfCustomerPaymentObjectsToBeUpserted[count].Id);
					}
					else{
						for(Database.Error err : listOfUpsertResult[count].getErrors()){
							mapNavSeqIdErrorDescription.put(listOfCustomerPaymentObjectsToBeUpserted[count].EP_NavSeqId__c, err.getMessage());
						}
					}
				}
			}
			acknowledgementWrapper = EP_GenericFinance_Json.fillSendAcknowledgementWrapper(NULL, mapSeqIdCustomerPaymentId, mapNavSeqIdErrorDescription);
		}

	/**
	* @author <Kamal Garg>
	* @description <This method is used to create "EP_Customer_Payment__c" custom object record>
	* @date <01/11/2017>
	* @param EP_GenericFinance_Json.Document, Id, Id, Map<String, Id>
	* @return EP_Customer_Payment__c
	*/
	private static EP_Customer_Payment__c generateCustomerPaymentRefundRecord(EP_GenericFinance_Json.Document paymentRefundDocument, Id recordId, Id paymentRefundRecordTypeId, Map<String, Id> acctNumIdMap) {
		EP_GeneralUtility.Log('Private','EP_CustomerPaymentRefundHandler','generateCustomerPaymentRefundRecord');
		EP_Customer_Payment__c customerPaymentObject = new EP_Customer_Payment__c();
		
		if(recordId != NULL){
			customerPaymentObject.Id = recordId;
			customerPaymentObject.EP_NavSeqId__c = paymentRefundDocument.seqId;
			customerPaymentObject.EP_Payment_Remaining_Balance__c = Decimal.ValueOf(paymentRefundDocument.remainingAmount);
			/** TR Datetime 59846 Start **/
			customerPaymentObject.EP_Payment_Date__c = EP_DateTimeUtility.convertStringToDate(paymentRefundDocument.submissionDate);//Date.ValueOf(paymentRefundDocument.submissionDate);
			/** TR Datetime 59846 End **/
			customerPaymentObject.EP_Amount_Paid__c = Decimal.ValueOf(paymentRefundDocument.amount);
			} else{
				customerPaymentObject.EP_NavSeqId__c = paymentRefundDocument.seqId;
				customerPaymentObject.EP_Payment_Remaining_Balance__c = Decimal.ValueOf(paymentRefundDocument.remainingAmount);
				/** TR Datetime 59846 Start **/
				customerPaymentObject.EP_Payment_Date__c = EP_DateTimeUtility.convertStringToDate(paymentRefundDocument.submissionDate);//Date.ValueOf(paymentRefundDocument.submissionDate);
				/** TR Datetime 59846 End **/
				customerPaymentObject.EP_Amount_Paid__c = Decimal.ValueOf(paymentRefundDocument.amount);
				customerPaymentObject.Name = paymentRefundDocument.identifier.docId;
				customerPaymentObject.EP_Payment_Method__c = paymentRefundDocument.paymentMethod;
				customerPaymentObject.RecordTypeId = paymentRefundRecordTypeId;
				customerPaymentObject.CurrencyIsoCode = paymentRefundDocument.currencyId;
				String compositKey = paymentRefundDocument.identifier.clientId + EP_Common_Constant.HYPHEN + paymentRefundDocument.identifier.billTo;
				if(acctNumIdMap.containsKey(compositKey)){
					customerPaymentObject.EP_Bill_To__c = acctNumIdMap.get(compositKey);
				}
				customerPaymentObject.EP_Payment_Reference_Number__c = paymentRefundDocument.referenceNr;
			}
			//L4 #45304 Changes Start
			customerPaymentObject.EP_Payment_Key__c = EP_DocumentUtil.generateUniqueKey(new Set<String>{paymentRefundDocument.identifier.billTo, paymentRefundDocument.identifier.docId, paymentRefundDocument.identifier.clientId, paymentRefundDocument.identifier.entryNr});
			customerPaymentObject.EP_Is_Reversal__c = paymentRefundDocument.isReversal != null && paymentRefundDocument.isReversal.equalsIgnoreCase(EP_COMMON_CONSTANT.STRING_TRUE) ? true : false ;
			customerPaymentObject.EP_Entry_Number__c = paymentRefundDocument.identifier.entryNr;
			//L4 #45304 Changes End
			
			return customerPaymentObject;
		}

	/**
	* @author <Vinay Jaiswal>
	* @description <This method is used to get Map containing 'AccountNumber' as key and 'Id' as value corresponding to a Set of specific RecordType 'DeveloperName'>
	* @date <06/21/2016>
	* @param Set<String>, Set<String>
	* @return Map<String, Id>
	*/
	private static Map<String, Id> getAccountNumberIdMap(Set<String> setAccountNumbers, Set<String> setRecordTypeDevloperNames){
		EP_GeneralUtility.Log('Private','EP_CustomerPaymentRefundHandler','getAccountNumberIdMap');
		Map<String, Id> acctNumIdMap = new Map<String, Id>();		
		for(Account acct :[SELECT Id, AccountNumber, EP_Composite_Id__c FROM Account WHERE EP_Composite_Id__c IN:setAccountNumbers 
			AND RecordType.DeveloperName IN:setRecordTypeDevloperNames LIMIT 1]){
			acctNumIdMap.put(acct.EP_Composite_Id__c.toUpperCase(), acct.Id);
		}
		return acctNumIdMap;
	}
	
	/**
	 * @author <Vinay Jaiswal>
	 * @description <This method is used to perform insert operation for 'Customer Account Statement Item' custom object records>
	 * @date <06/21/2016>
	 * @param List<EP_Customer_Payment__c>
	 * @return void
	 */
	 public static void createCustomerAccountStatementItem(List<EP_Customer_Payment__c> listOfCustomerPayment){
	 	EP_GeneralUtility.Log('Public','EP_CustomerPaymentRefundHandler','createCustomerAccountStatementItem');
        /*Novasuite Fix string placed at the top of the class*/
	 	EP_Customer_Account_Statement_Item__c customerAccountStatementItemObject;
	 	try{
	 		Id paymentOutRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMERACCOUNTSTATEMENTITEM_OBJECT
	 			,EP_Common_Constant.PAYMENTOUT_RECORD_TYPE_NAME);
	 		List<EP_Customer_Account_Statement_Item__c> listOfCustomerAccountStatementItems = new List<EP_Customer_Account_Statement_Item__c>();
			/*L4 45308 Changes Start*/
	 		map<string,Id> mapStatementItemKeyToId = new map<string,Id>();
	 		mapStatementItemKeyToId = EP_AccountStatementItemMapper.getStatemrntItemId(listOfCustomerPayment);
	 		/*L4 45308 Changes End*/
	 		for(EP_Customer_Payment__c customerPayment : listOfCustomerPayment){
	 			customerAccountStatementItemObject = new EP_Customer_Account_Statement_Item__c();
	 			customerAccountStatementItemObject.RecordTypeId = paymentOutRecordTypeId;
	 			customerAccountStatementItemObject.EP_NAV_Statement_Item_ID__c = customerPayment.Name;
	 			customerAccountStatementItemObject.EP_Bill_To__c = customerPayment.EP_Bill_To__c;
	 			customerAccountStatementItemObject.EP_Statement_Item_Issue_Date__c = customerPayment.EP_Payment_Date__c;
	 			customerAccountStatementItemObject.EP_Debit__c = customerPayment.EP_Amount_Paid__c;
	 			customerAccountStatementItemObject.CurrencyIsoCode = customerPayment.CurrencyIsoCode;
	 			customerAccountStatementItemObject.EP_Customer_Payment__c = customerPayment.Id;
	 			customerAccountStatementItemObject.EP_SeqId__c = customerPayment.EP_NavSeqId__c;
	 			customerAccountStatementItemObject.EP_Customer_Account_Statement_Item_Key__c = KEY + EP_Common_Constant.UNDERSCORE_STRING + customerPayment.EP_Bill_To__c + EP_Common_Constant.UNDERSCORE_STRING +customerPayment.Name+ EP_Common_Constant.UNDERSCORE_STRING +customerPayment.EP_Payment_Date__c;
				/*L4 45308 Changes Start*/
				customerAccountStatementItemObject.EP_Entry_Number__c = customerPayment.EP_Entry_Number__c;
				customerAccountStatementItemObject.EP_Document_Type__c = EP_Common_Constant.DOCUMENTTYPE_PAYMENT_REFUND;
				customerAccountStatementItemObject.EP_Is_Reversal__c = customerPayment.EP_Is_Reversal__c;
				EP_CustomerAccountStatementUtility.setStatemrntItemId(mapStatementItemKeyToId,customerAccountStatementItemObject);
				/*L4 45308 Changes End*/
	 			listOfCustomerAccountStatementItems.add(customerAccountStatementItemObject);
	 		}
	 		if(!listOfCustomerAccountStatementItems.isEmpty()){
	 			/*L4 45308 Changes Start*/
	 			Database.upsert(listOfCustomerAccountStatementItems);
	 			/*L4 45308 Changes End*/
	 		}
	 		}catch(Exception ex){
	 			EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, EP_CustomerPaymentRefundHandler.class.getName(), METHOD_NAME1, ApexPages.Severity.ERROR);
	 		}
	 	}
	 }