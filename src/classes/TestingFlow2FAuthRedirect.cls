public with sharing class TestingFlow2FAuthRedirect {
	

	@InvocableMethod(label='Redirect 2FA' description='Redirect 2FA')
	public static List<String> redirect2FA() {
		List<String> output = new List<String>();
//'https://gdev-gdev-pumaenergy.cs81.force.com' + 
		String url = Auth.SessionManagement.generateVerificationUrl(Auth.VerificationPolicy.HIGH_ASSURANCE, 'testing nic', '/home');
		System.debug('redirect2FA called with ' + url);
		//return new PageReference('http://www.google.com');

		output.add(url);

		return output;
	}

}