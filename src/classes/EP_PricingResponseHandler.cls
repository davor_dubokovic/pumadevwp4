/* 
   @Author <Accenture>
   @name <EP_PricingResponseHandler>
   @CreateDate <03/03/2017>
   @Description <These are the stub classes to map JSON string for Pricing Response Rest Service Request from NAV> 
   @Version <1.1>
   Update - Changes for #60147 - To Handle Pricing Response in Synchronous way 
   */
   public class EP_PricingResponseHandler  extends EP_InboundHandler {
    @testVisible private static List<EP_AcknowledgementStub.dataSet> ackResponseList = new list <EP_AcknowledgementStub.dataSet> ();
    private static boolean isProcessingFailed = false;
    private static boolean isPricingSuccess = false;
    private static EP_PricingResponseHelper pricingResponseHelper = new EP_PricingResponseHelper();
   	
   /**
    * @Author       Accenture
    * @Name         processRequest
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    public override string processRequest(string jsonBody, string orderNumber) {
    	EP_GeneralUtility.Log('public','EP_PricingResponseHandler','processRequest');
        string response = EP_Common_Constant.BLANK;
        EP_MessageHeader headerCommon = new EP_MessageHeader();
        isProcessingFailed = false;
        boolean isParsingFailed = false; 
        list<EP_PricingResponseStub.LineItem> priceLineItems = new list<EP_PricingResponseStub.LineItem>();
		//Changes for #60147
		//Handle Byte order mark("\ufeff") which is coming at the start in JSON response
		if(jsonBody.startsWith(EP_Common_Constant.BOM_STR)) {
			jsonBody = jsonBody.replaceFirst(EP_Common_Constant.BOM_STR, EP_Common_Constant.BLANK);
		}
		
        try {
            EP_PricingResponseStub stub = (EP_PricingResponseStub) System.JSON.deserialize(jsonBody, EP_PricingResponseStub.class);
            EP_PricingResponseStub.priceDetails priceDetails = stub.Payload.Any0.pricingResponse.priceDetails;
            processPricingResponse(priceDetails, priceDetails.priceLineItems.orderId);

        } catch (exception exp) {
        	isParsingFailed = true;
            logExceptions(exp, null);
            createResponse(null, exp.getTypeName(),  exp.getMessage()); 
        }
        
        //Handle this if Parsing failed then Update the Order with failure status
        if(isParsingFailed) {
        	updatePricingOnOrder(orderNumber, '','Pricing Error from NAV');
        }
        return sendProcessResponse();
    }
    
    /**
    * @Description: This Method will send the Process Response
    * @Date :06/28/2017
    * @Name :sendProcessResponse
    * @param:  
    * @return: String
    */
    @TestVisible
    private string sendProcessResponse() {
    	string response = EP_Common_Constant.SUCCESS + EP_Common_Constant.COLON_WITH_SPACE;
    	if(isProcessingFailed) {
    		response = EP_Common_Constant.FAILURE + EP_Common_Constant.COLON_WITH_SPACE;
    	}
    	return response+JSON.serialize(ackResponseList);
    }
    
    /**
    * @Description: This method will process the Pricing Response inbound request
    * @Date :03/03/2017
    * @Name :processPricingResponse
    * @param:  Instance of EP_PricingResponseMsg Class
    * @return:
    */
    @TestVisible
    private static void processPricingResponse(EP_PricingResponseStub.priceDetails priceDetails, string orderNumber) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHandler','processPricingResponse');
        // Error is coming in Pricing Response the Update the order and oder items with error details
        //Otherwise process to update and insert orderItems
        if(string.isNotEmpty(priceDetails.Error.ErrorDescription)) {
            updatePricingResponse(orderNumber, priceDetails.Error.ErrorDescription, priceDetails.seqId);
        } else {
            pricingResponseHelper.setOrderItemAttributes(priceDetails.priceLineItems.lineItem, orderNumber);
            processOrderItems(priceDetails, orderNumber);
        }
    }
    
    /**
    * @Description:  This method will process the Order Items which are coming in Pricing Response inbound request
    * @Date :03/03/2017
    * @Name :processOrderItems
    * @param: Instance of EP_PricingResponseMsgTransform class
    * @return:
    */
    @TestVisible
    private static void processOrderItems(EP_PricingResponseStub.priceDetails priceDetails, string orderNumber) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHandler','processOrderItems');
        Savepoint sp = Database.setSavepoint();
        string errorDetails = EP_Common_Constant.BLANK; 
        string errorCode = EP_Common_Constant.BLANK; 
        try {
            //deletetExistingInvoiceItems(orderNumber);
            upsertOrderLineItems(priceDetails.priceLineItems.lineItem, priceDetails.seqId);
            isPricingSuccess = true;
        } catch(Exception exp) {
            database.rollback(sp);
            logExceptions(exp, priceDetails.seqId);
            errorDetails = exp.getMessage();
            errorCode = 'PRICING_EXCEPTION';
        }
        createResponse(priceDetails.seqId, errorCode, errorDetails);
        updatePricingResponse(orderNumber, errorDetails, priceDetails.seqId);
    }
    
    public static void upsertOrderLineItems(list<EP_PricingResponseStub.LineItem> priceLineItems , string seqId) {
        list<orderItem> updateOrderItemList = EP_PricingResponseHelper.getOrderItemList(priceLineItems);
        if(!updateOrderItemList.isEmpty()) {
            database.upsert(updateOrderItemList, true);
        }
    }
    
    /**
    * @Description: This method will process and log the exceptions
    * @Date :03/03/2017
    * @Name :logExceptions
    * @param: exp - Exception
    * @param: seqId - Order seqId from JSON string
    * @return:
    */
    @TestVisible
    private static void logExceptions(Exception exp, string seqId) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHandler','logExceptions');
        isProcessingFailed = true;
        EP_LoggingService.logServiceException(exp, UserInfo.getOrganizationId(), EP_Common_constant.EPUMA, 'logExceptions', 'EP_PricingResponseHandler', EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF,  EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);
    }

    /**
    * @Author       Accenture
    * @Name         createResponse
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void createResponse(string seqId, string errorCode, string errorDescription) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHandler','createResponse');
        if(string.isNotBlank(errorDescription)) isProcessingFailed = true;
        ackResponseList.add(EP_AcknowledgementUtil.createDataSet(EP_Common_constant.PRICING_ENGINE, seqId, errorCode, errorDescription));
    }
    
    /**
    * @Description: This method will delete all existing invoice line items of Order
    * @Date :03/03/2017
    * @Name :deletetExistingInvoiceItems
    * @param: orderNumber - orderNumber of order Object
    * @return:
    
    @TestVisible
    private static void deletetExistingInvoiceItems(string orderNumber) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHandler','deletetExistingInvoiceItems');
        EP_OrderItemMapper orderItemMapper = new EP_OrderItemMapper();
        list<orderItem> invoiceItemsDelList = orderItemMapper.getInvoiceItemsByOrderNumber(new set<string>{orderNumber});
        if(!invoiceItemsDelList.isEmpty()){
            database.delete(invoiceItemsDelList);
        }
    } */
    /**
    * @Description: This method will process the message errors coming in JSON string
    * @Date :03/03/2017
    * @Name :updatePricingResponse
    * @param: orderNumber - orderNumber of order Object
    * @param: errorDes - Error Description from JSON string
    * @param: seqId -  Order item seqId from JSON string
    * @return: 
    */
    @TestVisible
    private static void updatePricingResponse(string orderNumber, string errorDes, string seqId) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHandler','updatePricingResponse');
        updatePricingOnOrderItems(orderNumber,errorDes);
        //Defect Fix - Pass Error Message to update on order
        updatePricingOnOrder(orderNumber,seqId, errorDes);
    }
    /**
    * @Description: This method will update Pricing Error On OrderItems
    * @Date :03/03/2017
    * @Name :updatePricingErrorOnOrderItems
    * @param: orderNumber - orderNumber of order Object
    * @param: errorDes - Error Description from JSON string
    * @return:
    */
    @TestVisible
    private static void updatePricingOnOrderItems(string orderNumber, string errorDes) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHandler','updatePricingOnOrderItems');
        list<orderItem> updateOrderItemList = new list<orderItem>();
        EP_OrderItemMapper orderItemMapper = new EP_OrderItemMapper();
        /*for(orderItem ordItemObj : orderItemMapper.getOrderItemsByOrderNumber(orderNumber)) {
            if(errorDes.length() > 240){
                errorDes = errorDes.subString(0,240);
            }
            ordItemObj.EP_Pricing_Error__c = errorDes;
            updateOrderItemList.add(ordItemObj);
        }*/
        if(!updateOrderItemList.isEmpty()){ 
            database.update(updateOrderItemList); 
        }
    }
    
    /**
    * @Description:  This method will update Pricing Status On Order Object
    * @Date :03/03/2017
    * @Name :updatePricingOnOrder
    * @param: orderNumber - orderNumber of order Object
    * @param: pricingStatus - pricingStatus
    * @return: NA
    */
    @TestVisible
    private static void updatePricingOnOrder(string orderNumber, string seqId,  string errorDes) {
   		EP_GeneralUtility.Log('Private','EP_PricingResponseHandler','updatePricingOnOrder');
    	try {   
            EP_OrderMapper orderMapper = new EP_OrderMapper();
            map<string, csord__Order__c> orderNumberOrderMap = orderMapper.getCSOrderMapByOrderNumber(new set<string>{orderNumber});
            csord__Order__c orderObject = orderNumberOrderMap.get(orderNumber);
            orderObject.EP_Pricing_Status__c = isPricingSuccess ? EP_Common_constant.PRICING_STATUS_PRICED : EP_Common_constant.PRICING_STATUS_FAILURE;
            //Defect Fix - update Order with Error Message
            if(errorDes.length() > 250){
                errorDes = errorDes.subString(0,250);
            }
            orderObject.EP_Error_Description__c = errorDes;
            //Defect Fix END
            pricingResponseHelper.setOrderStatus(orderObject);
            database.update(orderObject);
            pricingResponseHelper.doPostUpdateActions();
        } catch(exception exp){
            logExceptions(exp,seqId);
        }
    }
}