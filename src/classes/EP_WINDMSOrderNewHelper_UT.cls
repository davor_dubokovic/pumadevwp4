@isTest
 public class EP_WINDMSOrderNewHelper_UT
 {
    public static final String ERRORDESCRIPTION = 'UPDATE OF ORDER NOT HAPPENED DUE TO MISMATCH PRODUCT SOLD AS VALUE IN ORDERITEMS';
    static EP_OrderMapper ordMapper = new EP_OrderMapper();
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        EP_INTEGRATION_CUSTOM_SETTING__c integration_Setting = new EP_INTEGRATION_CUSTOM_SETTING__c(Name = 'Request TimeOut',EP_Value__c = '120000');
        insert integration_Setting; 
	    List<EP_OrderStatusNAVMapping__c> OrderStatusNAVMappingSetting = Test.loadData(EP_OrderStatusNAVMapping__c.sObjectType, 'EP_OrderStatusNAVMapping_TestData'); 
	    List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    }
    
    public static EP_WINDMSOrderNewStub.orderWrapper getOrderWrapper(){
        EP_WINDMSOrderNewStub localobj = new EP_WINDMSOrderNewStub();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapperObj = EP_TestDataUtility.createWINDMSOrderWrapper();
        Account custObj = [SELECT Id ,AccountNumber,EP_Composite_Id__c FROM Account WHERE Id = : orderWrapperObj.sfOrder.EP_Sell_To__c];
        Account shipToObj = [SELECT Id ,AccountNumber,EP_Composite_Id__c FROM Account WHERE Id = :orderWrapperObj.sfOrder.EP_ShipTo__c];
        orderWrapperObj.custId = custObj.AccountNumber;
        orderWrapperObj.shiptoId = shipToObj.AccountNumber;
        orderWrapperObj.clientId = orderWrapperObj.clientId.toUppercase();
        orderWrapperObj.sellToCompositKey = orderWrapperObj.clientId +'-'+orderWrapperObj.custId ;
        orderWrapperObj.shipToCompositKey = orderWrapperObj.sellToCompositKey +'-'+orderWrapperObj.shiptoId;
        Account accObj = [SELECT EP_PriceBook__c FROM Account WHERE AccountNumber =: orderWrapperObj.custId LIMIT 1];
        PriceBookEntry priceBookEntryObj = [SELECT Id , ProductCode FROM PriceBookEntry WHERE PriceBook2Id =: accObj.EP_PriceBook__c LIMIT 1];
        for(EP_WINDMSOrderNewStub.OrderLine ordLine : orderWrapperObj.OrderLines.OrderLine) {
            ordLine.ItemId = priceBookEntryObj.ProductCode;
        }
        return orderWrapperObj;
    }
    
    static testMethod void createDataSets_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapperObj = getOrderWrapper();
        
        Test.startTest();
        	localObj.createDataSets(new List<EP_WINDMSOrderNewStub.orderWrapper>{orderWrapperObj});
        Test.stopTest();
        
        System.AssertEquals(true,localObj.orderNumberSet != Null);
        System.AssertEquals(true,localObj.compositKeySet != Null);
        System.AssertEquals(true,localObj.transporterCodeSet != Null);
        System.AssertEquals(true,localObj.lineItemIdSet != Null);
    }
    static testMethod void createOrderNumberSet_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        orderWrapper.orderIdSF = ordMapper.getRecordById(orderWrapper.sforder.Id).OrderNumber;
        
        Test.startTest();
        	localObj.createOrderNumberSet(orderWrapper);
        Test.stopTest();
        
        System.AssertNotEquals(0,localObj.orderNumberSet.size());
    }
    static testMethod void createSellToSet_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        Test.startTest();
        localObj.createSellToSet(getOrderWrapper());
        Test.stopTest();
        System.AssertEquals(true,localObj.compositKeySet.size() > 0);
    }
    static testMethod void createShipToSet_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        
        Test.startTest();
        	localObj.createShipToSet(getOrderWrapper());
        Test.stopTest();
        
        System.AssertEquals(true,localObj.compositKeySet.size() > 0);
    }
    static testMethod void createTransporterCodeSet_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        
        Test.startTest();
        	localObj.createTransporterCodeSet(getOrderWrapper());
        Test.stopTest();
        
        System.AssertEquals(true,localObj.transporterCodeSet.size() > 0);
    }
    static testMethod void createOrderItemDataSets_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.OrderLine orderLineObj = EP_TestDataUtility.createWINDMSOrderLine();
        
        Test.startTest();
        	localObj.createOrderItemDataSets(new List<EP_WINDMSOrderNewStub.OrderLine>{orderLineObj});
        Test.stopTest();
        
        System.AssertEquals(true, localObj.lineItemIdSet.size() > 0);
    }
    static testMethod void createDataMaps_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        LIST<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new LIST<EP_WINDMSOrderNewStub.orderWrapper>{getOrderWrapper()};
        
        Test.startTest();
        	localObj.createDataMaps(orderWrapperList);
        Test.stopTest();
        
        System.AssertNotEquals(Null,localObj.accCompKeyWithAccountMap);
        System.AssertNotEquals(Null,localObj.transporterMap);
        System.AssertNotEquals(Null,localObj.productObjMap);
    }
    static testMethod void setAccountIdWithAccount_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        csord__Order__c orderObj = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        List<Account> listAccounts = [SELECT Id FROM Account];
        Map<string,Account> accCompKeyWithAccountMap = new Map<string,Account>();
        for(Account accObj : listAccounts){
            accCompKeyWithAccountMap.put(accObj.id,accObj);
        }
        
        Test.startTest();
        	localObj.setAccountIdWithAccount(accCompKeyWithAccountMap);
        Test.stopTest();
        
        System.AssertNotEquals(Null,localObj.accIdWithAccountMap);
    }
    static testMethod void setOrderAttributes_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        List<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new List<EP_WINDMSOrderNewStub.orderWrapper>{getOrderWrapper()};
        
        Account shipTo = [select id,AccountNumber,Parent.AccountNumber from Account where ParentId=:orderWrapperList[0].SFOrder.AccountId__c];
        orderWrapperList[0].shiptoId = shipTo.AccountNumber;
        orderWrapperList[0].custId = shipTo.Parent.AccountNumber;
        orderWrapperList[0].clientId=orderWrapperList[0].clientId.toUppercase();
        
        Test.startTest(); 
        	localObj.setOrderAttributes(orderWrapperList);
        Test.stopTest();
        
        System.AssertEquals(Date.today(),orderWrapperList[0].sfOrder.EffectiveDate__c);
    }
    static testMethod void setOrderDefaultAttributes_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        csord__Order__c OrderObject = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);
        localObj.orderNumberOrderMap.put(OrderObject.OrderNumber__c, OrderObject);
        
        Test.startTest();
        	localObj.setOrderDefaultAttributes(orderWrapper);
        Test.stopTest();
        
        System.AssertEquals(EP_Common_Constant.ORDER_PLANNED_STATUS,orderWrapper.sfOrder.Status__c);
        System.AssertEquals(Date.today(), orderWrapper.sfOrder.EffectiveDate__c);
        System.AssertEquals(EP_Common_Constant.ORDER_PLANNED_STATUS, orderWrapper.sfOrder.Status__c);
        System.AssertEquals(true, orderWrapper.sfOrder.EP_Sync_With_LS__c);
        System.AssertEquals(true, orderWrapper.sfOrder.EP_Order_Push_From_WinDms__c);
        System.AssertEquals(EP_Common_Constant.EPOC_CURRENT, orderWrapper.sfOrder.EP_Order_Epoch__c );
        System.AssertEquals(EP_Common_Constant.DELIVERY, orderWrapper.sfOrder.EP_Delivery_Type__c);
    }
    static testMethod void setOrderJSONAttributes_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        
        Test.startTest();
        	localObj.setOrderJSONAttributes(orderWrapper);
        Test.stopTest();
        
        System.AssertEquals(orderWrapper.transporterCode,orderWrapper.sfOrder.EP_Transporter_Code__c );
    }
    static testMethod void setCompanyCode_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        String companyCode = EP_Common_Constant.AAF_COMPANY;
        
        Test.startTest();
        	localObj.setCompanyCode(companyCode);
        Test.stopTest();
        
        System.AssertEquals(companyCode,localObj.companyCode );
    }
    static testMethod void setShipToId_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
       	EP_WINDMSOrderNewStub.OrderWrapper orderWrapper = getOrderWrapper();
       
       	EP_AccountDomainObject accDomain = new EP_AccountDomainObject(orderWrapper.sfOrder.EP_ShipTo__c);
        EP_AccountDomainObject accDomainSellTo = new EP_AccountDomainObject(orderWrapper.sfOrder.AccountId__c);
        localObj.accCompKeyWithAccountMap.put(orderWrapper.shipToCompositKey,accDomain.getAccount());
		localObj.accCompKeyWithAccountMap.put(orderWrapper.sellToCompositKey,accDomainSellTo.getAccount());
		orderWrapper.sfOrder.EP_ShipTo__c = null;
        
        Test.startTest();
        	localObj.setShipToId(orderWrapper);
        Test.stopTest();
        
        System.AssertNotEquals(null,orderWrapper.sfOrder.EP_ShipTo__c);
    }
    static testMethod void setProductCategory_NegativeTest() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.OrderWrapper orderWrapper = getOrderWrapper();
        for(EP_WINDMSOrderNewStub.OrderLine ordLine : orderWrapper.OrderLines.OrderLine) {
            ordLine.ItemId = '';
        }
        
        Test.startTest();
        	localObj.setProductCategory(orderWrapper);
        Test.stopTest();
        
        System.AssertEquals(ERRORDESCRIPTION,orderWrapper.errorDescription );
    }
    static testMethod void setProductCategory_PositiveTest() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        LIST<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new LIST<EP_WINDMSOrderNewStub.orderWrapper>{getOrderWrapper()};
        
        Test.startTest();
        	localObj.setProductCategory(orderWrapperList[0]);
        Test.stopTest();
        
        System.AssertNotEquals(Null,orderWrapperList[0].sfOrder.EP_Order_Product_Category__c );
    }
    static testMethod void setRecordTypeId_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        Id orderRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER,EP_Common_Constant.VMI_ORDER_RECORD_TYPE_NAME);
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        orderWrapper.SFOrder.Id= null;
        LIST<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new LIST<EP_WINDMSOrderNewStub.orderWrapper>{orderWrapper};
        
        Test.startTest();
        	localObj.setRecordTypeId(orderWrapper);
        Test.stopTest();
        
        System.AssertEquals(orderRecordTypeId,orderWrapper.sfOrder.RecordTypeId);
    }
    static testMethod void setSellToId_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        LIST<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new LIST<EP_WINDMSOrderNewStub.orderWrapper>{orderWrapper};
        csord__Order__c ord = orderWrapper.SFOrder;
        Account shipTo = [select id,CurrencyISOCode,EP_Status__c,EP_Account_Type__c,EP_Customer_PO_Number__c,ParentId from Account where id=:orderWrapper.SFOrder.EP_ShipTo__c];
        localObj.accIdWithAccountMap.put(shipTo.Id,shipTo);
        Id sellToId = ord.EP_Sell_To__c;
        
        Test.startTest();
        	localObj.setSellToId(orderWrapper);
        Test.stopTest();
        
        System.AssertEquals(sellToId,orderWrapper.sfOrder.AccountId__c);
        System.AssertEquals(sellToId,orderWrapper.sfOrder.EP_Sell_To__c);
    }
    static testMethod void setOrderType_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        Account shipTo = [select id,CurrencyISOCode,EP_Status__c,EP_Account_Type__c,EP_Customer_PO_Number__c,ParentId,EP_Ship_To_Type__c from Account where id=:orderWrapper.SFOrder.EP_ShipTo__c];
        localObj.accIdWithAccountMap.put(shipTo.Id,shipTo);
        
        LIST<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new LIST<EP_WINDMSOrderNewStub.orderWrapper>{orderWrapper};
        
        Test.startTest();
        	localObj.setOrderType(orderWrapper);
        Test.stopTest();
        
        System.AssertEquals(shipTo.EP_Ship_To_Type__c,orderWrapper.sfOrder.Type__c);
    }
    static testMethod void setPriceBook2Id_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        Account acc = [select id,CurrencyISOCode,EP_Status__c,EP_Account_Type__c,EP_Customer_PO_Number__c,ParentId from Account where id=:orderWrapper.SFOrder.EP_ShipTo__c];
        localObj.accIdWithAccountMap.put(acc.Id,acc);
        
        Test.startTest();
        	localObj.setPriceBook2Id(orderWrapper);
        Test.stopTest();
        
        System.AssertNotEquals(Null,localObj.priceBookIdSet);
    }
    static testMethod void setCurrencyISOCode_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        Account acc = [select id,CurrencyISOCode from Account where id=:orderWrapper.SFOrder.EP_ShipTo__c];
        localObj.accIdWithAccountMap.put(acc.Id,acc);
        
        Test.startTest();
        	localObj.setCurrencyISOCode(orderWrapper);
        Test.stopTest();
        
        System.AssertEquals(true,localObj.currencyCodeSet.size() > 0 );
    }
    static testMethod void setTransporterId_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        LIST<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new LIST<EP_WINDMSOrderNewStub.orderWrapper>{orderWrapper};
        csord__Order__c OrderObject = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);
        /* TFS fix 45559,45560,45567,45568 start EP_NAV_ID__c deprecated, EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c and EP_Vendor_Type__c is replaced by EP_VendorType__c*/
        list<EP_Stock_Holding_Location__c> supplyOpt =  [select Id,EP_Transporter__c, EP_Transporter__r.EP_Source_Entity_ID__c from EP_Stock_Holding_Location__c where EP_Ship_To__c =:OrderObject.EP_ShipTo__c AND EP_Transporter__r.EP_Source_Entity_ID__c != null];
        orderWrapper.transporterCode = supplyOpt.get(0).EP_Transporter__r.EP_Source_Entity_ID__c;
        orderWrapper.SFOrder.EP_Transporter_Code__c = supplyOpt.get(0).EP_Transporter__r.EP_Source_Entity_ID__c;
        Account transporterAcc = [SELECT Id,EP_Source_Entity_ID__c FROM Account WHERE id =:supplyOpt.get(0).EP_Transporter__c];
        localObj.transporterMap.put(orderWrapper.transporterCode,transporterAcc);
        /* TFS fix 45559,45560,45567,45568 end*/

        Test.startTest();
        	localObj.setTransporterId(orderWrapper);
        Test.stopTest();
        
        System.AssertEquals(transporterAcc.id, orderWrapper.sfOrder.EP_Transporter__c);
    }
    static testMethod void setCustomerPONumber_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        LIST<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new LIST<EP_WINDMSOrderNewStub.orderWrapper>{orderWrapper};
        Account acc = [select id,CurrencyISOCode,EP_Status__c,EP_Account_Type__c,EP_Customer_PO_Number__c,ParentId from Account where id=:orderWrapper.SFOrder.EP_ShipTo__c];
        localObj.accIdWithAccountMap.put(acc.Id,acc);
        
        Test.startTest();
        	localObj.setCustomerPONumber(orderWrapper);
        Test.stopTest();
        
        System.AssertNotEquals(Null,orderWrapper.sfOrder.EP_Customer_PO_Number__c);
    }
    static testMethod void getOrderProductCategory_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        LIST<EP_WINDMSOrderNewStub.OrderLine> ordLines = new List<EP_WINDMSOrderNewStub.OrderLine>();
        set<string> lineItemIdSet = new set<string>();
        EP_ProductMapper productMapper = new EP_ProductMapper();
        for(EP_WINDMSOrderNewStub.OrderLine ordLine : orderWrapper.OrderLines.OrderLine) {
            ordLines.add(ordLine);
            lineItemIdSet.add(ordLine.ItemId);
        }
        localObj.productObjMap = productMapper.getProductSoldAsInfo(lineItemIdSet);
        
        Test.startTest();
        	String result = localObj.getOrderProductCategory(ordLines);
        Test.stopTest();
        
        System.AssertEquals(localObj.productObjMap.get(ordLines.get(0).ItemId),result);
    }
    static testMethod void setOrderItemAttributes_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        LIST<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new LIST<EP_WINDMSOrderNewStub.orderWrapper>{orderWrapper};
        Account shipTo = [select id,AccountNumber,Parent.AccountNumber,EP_Status__c,EP_Account_Type__c from Account where ParentId=:orderWrapperList[0].SFOrder.AccountId__c];
        orderWrapperList[0].sfOrder.EP_ShipTo__c = shipTo.Id;
        orderWrapperList[0].shiptoId = shipTo.AccountNumber;
        orderWrapperList[0].custId = shipTo.Parent.AccountNumber;
        orderWrapperList[0].clientId=orderWrapperList[0].clientId.toUppercase();
        localObj.accIdWithAccountMap.put(shipTo.Id, shipTo);
        
        Test.startTest();
        	localObj.setOrderItemAttributes(orderWrapperList);
        Test.stopTest();
        
        System.AssertEquals(Label.EP_Required_field_are_missing_stock_location_Id,orderWrapper.OrderLines.OrderLine[0].errorDescription);
    }
    static testMethod void setOrderItemJSONAttributes_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        LIST<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new LIST<EP_WINDMSOrderNewStub.orderWrapper>{orderWrapper};
        EP_WINDMSOrderNewStub.OrderLine ordLine = new EP_WINDMSOrderNewStub.OrderLine();
        ordLine.Identifier = new EP_WINDMSOrderNewStub.Identifier_Z();
        ordLine.Identifier.lineNr='123112';
        ordLine.qty='3000';
        
        Test.startTest();
        	localObj.setOrderItemJSONAttributes(ordLine);
        Test.stopTest();
        System.AssertEquals(ordLine.orderLineItem.EP_WinDMS_Line_Item_Reference_Number__c,ordLine.Identifier.lineNr ); 
        System.AssertEquals(string.valueOf(ordLine.orderLineItem.Quantity__c),ordLine.qty);
        System.AssertEquals(ordLine.orderLineItem.EP_WinDMS_Line_ItemId__c,ordLine.itemId );
    }
    static testMethod void setOrderItems_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        
        EP_WINDMSOrderNewStub.OrderLine orderLineObj = EP_TestDataUtility.createWINDMSOrderLine();
        string pricebookEntryKey = orderLineObj.orderLineItem.EP_WinDMS_Line_ItemId__c + orderWrapper.SFOrder.priceBook2Id__c + orderWrapper.SFOrder.currencyISOCode;
        Account accObj = [select EP_PriceBook__c from Account where AccountNumber =: orderWrapper.custId LIMIT 1];
        PriceBookEntry priceBookEntryObj = [select Id , ProductCode from PriceBookEntry where PriceBook2Id =: accObj.EP_PriceBook__c LIMIT 1];
        
        Account shipTo = [select id,CurrencyISOCode,EP_Status__c,EP_Account_Type__c,AccountNumber,EP_Customer_PO_Number__c,ParentId,(Select Id,EP_Tank_Code__c from Tank__r) from Account where id=:orderWrapper.SFOrder.EP_ShipTo__c];
        localObj.accIdWithAccountMap.put(shipTo.Id,shipTo);
        
        Map<String,PricebookEntry> priceBookEntryMap = new Map<String,PricebookEntry>();
        priceBookEntryMap.put(pricebookEntryKey,priceBookEntryObj);
        
        Test.startTest();
        	localObj.setOrderItems(orderWrapper,priceBookEntryMap);
        Test.stopTest();
        
        System.AssertEquals(orderWrapper.sfOrder.id,orderWrapper.OrderLines.orderLine[0].orderLineItem.OrderId__c);
    }
    static testMethod void setStockHoldingLocation_PositiveTest() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        LIST<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new LIST<EP_WINDMSOrderNewStub.orderWrapper>{orderWrapper};
        csord__Order__c OrderObject = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);
        /* TFS fix 45559,45560,45567,45568 start EP_NAV_ID__c deprecated, EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c and EP_Vendor_Type__c is replaced by EP_VendorType__c*/
        list<EP_Stock_Holding_Location__c> supplyOpt =  [select Id,EP_Transporter__c, EP_Transporter__r.EP_Source_Entity_ID__c, Stock_Holding_Location__r.EP_Nav_Stock_Location_Id__c from EP_Stock_Holding_Location__c where EP_Ship_To__c =:OrderObject.EP_ShipTo__c];
        LIST<EP_WINDMSOrderNewStub.OrderLine> ordLines = new List<EP_WINDMSOrderNewStub.OrderLine>();
        localObj.storageStockLocationMap.put(supplyOpt.get(0).Stock_Holding_Location__r.EP_Nav_Stock_Location_Id__c, supplyOpt.get(0));
        for(EP_WINDMSOrderNewStub.OrderLine ordLine : orderWrapper.OrderLines.OrderLine) {
            ordLine.orderLineItem = new csord__Order_Line_Item__c();
            ordLine.orderLineItem.EP_WinDMS_StockHldngLocId__c = supplyOpt.get(0).Stock_Holding_Location__r.EP_Nav_Stock_Location_Id__c;
            ordLines.add(ordLine);
        }
        
        Test.startTest();
        	localObj.setStockHoldingLocation(ordLines.get(0));
        Test.stopTest();
        
        System.AssertEquals(supplyOpt.get(0).id,  ordLines.get(0).orderLineItem.EP_Stock_Holding_Location__c);
    }
    
    static testMethod void setStockHoldingLocation_NegativeTest() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        LIST<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = new LIST<EP_WINDMSOrderNewStub.orderWrapper>{orderWrapper};
        csord__Order__c OrderObject = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);
        list<EP_Stock_Holding_Location__c> supplyOpt =  [select Id,EP_Transporter__c, EP_Transporter__r.EP_Source_Entity_ID__c, Stock_Holding_Location__r.EP_Nav_Stock_Location_Id__c from EP_Stock_Holding_Location__c where EP_Ship_To__c =:OrderObject.EP_ShipTo__c];
        LIST<EP_WINDMSOrderNewStub.OrderLine> ordLines = new List<EP_WINDMSOrderNewStub.OrderLine>();
        for(EP_WINDMSOrderNewStub.OrderLine ordLine : orderWrapper.OrderLines.OrderLine) {
            ordLine.orderLineItem = new csord__Order_Line_Item__c();
            ordLine.orderLineItem.EP_WinDMS_StockHldngLocId__c = supplyOpt.get(0).Stock_Holding_Location__r.EP_Nav_Stock_Location_Id__c;
            ordLines.add(ordLine);
        }
        /* TFS fix 45559,45560,45567,45568 end*/
        Test.startTest();
        	localObj.setStockHoldingLocation(ordLines.get(0));
        Test.stopTest();
        
        System.AssertEquals(ordLines.get(0).errorDescription,  Label.EP_Required_field_are_missing_stock_location_Id);
    }
    
    static testMethod void setPriceBookEntryId_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        EP_WINDMSOrderNewStub.OrderLine ordLine = EP_TestDataUtility.createWINDMSOrderLine();
        string pricebookEntryKey = ordLine.orderLineItem.EP_WinDMS_Line_ItemId__c + orderWrapper.SFOrder.priceBook2Id__c + orderWrapper.SFOrder.currencyISOCode;
        Account accObj = [select EP_PriceBook__c from Account where AccountNumber =: orderWrapper.custId LIMIT 1];
        PriceBookEntry priceBookEntryObj = [select Id,ProductCode,UnitPrice,Product2Id from PriceBookEntry where PriceBook2Id =: accObj.EP_PriceBook__c LIMIT 1];
        Map<String,PricebookEntry> priceBookEntryMap = new Map<String,PricebookEntry>();
        priceBookEntryMap.put(pricebookEntryKey,priceBookEntryObj);
        
        Test.startTest();
        localObj.setPriceBookEntryId(ordLine,priceBookEntryMap,orderWrapper.SFOrder);
        Test.stopTest();
        System.AssertEquals(pricebookEntryMap.get(pricebookEntryKey).id,ordLine.orderLineItem.PriceBookEntryId__c);        
    }
    static testMethod void setTank_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.OrderLine orderLineObj = EP_TestDataUtility.createWINDMSOrderLine();
        csord__Order__c orderObj = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        Id shipToId = orderObj.EP_ShipTo__c;
        EP_Tank__c tank = [select id,EP_Tank_Code__c from EP_Tank__c LIMIT 1];
        
        orderLineObj.orderLineItem.EP_Tank_Number__c = tank.EP_Tank_Code__c;
        for(Account accObj : [SELECT Id,EP_Composite_Id__c,EP_Puma_Company_Code__c,(Select id,EP_Tank_Code__c from Tank__r) FROM Account]){
            localObj.accCompKeyWithAccountMap.put(accObj.EP_Composite_Id__c,accObj);
            localObj.accIdWithAccountMap.put(accObj.Id,accObj);
        }
        
        Test.startTest();
        	localObj.setTank(orderLineObj,shipToId);
        	csord__Order_Line_Item__c orderItemObj = orderLineObj.orderLineItem;
        Test.stopTest();
        
        System.AssertNotEquals(Null,orderItemObj.EP_Tank__c);
    }
    static testMethod void setOrderLineItemId_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_WINDMSOrderNewStub.orderWrapper orderWrapper = getOrderWrapper();
        
        csord__Order__c OrderObject = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);
        OrderObject.EP_WinDMS_Order_Number__c = 'WIN-123';
        update OrderObject;
        
        List<Account>  listAccounts = [select id,CurrencyISOCode,EP_Status__c,EP_Account_Type__c,EP_Customer_PO_Number__c,ParentId,AccountNumber from Account];
        for(Account accObj : listAccounts){
            localObj.accIdWithAccountMap.put(accObj.Id,accObj);
        }
        list<csord__Order_Line_Item__c> orderItems = [select Id, EP_Order_Item_Key__c, 
                                                        //priceBookEntry.ProductCode, 
                                                        EP_OrderItem_Number__c, Quantity__c, EP_Pricing_Response_Unit_Price__c, orderId__c, UnitPrice__c, EP_WinDMS_Line_ItemId__c, csord__Order__r.Status__c, csord__Order__r.EP_Pricing_Status__c, PriceBookEntryId__c 
                                                        //, PricebookEntry.Product2.Name 
                                                        from csord__Order_Line_Item__c where OrderId__c = : OrderWrapper.sfOrder.id];
        
        for(EP_WINDMSOrderNewStub.OrderLine ordLine : orderWrapper.OrderLines.OrderLine) {
            ordLine.orderLineItem = new csord__Order_Line_Item__c();
            ordLine.orderLineItem.PriceBookEntryId__c = orderItems.get(0).PriceBookEntryId__c;
            ordLine.orderLineItem.EP_WinDMS_Line_ItemId__c = ordLine.itemId;
            ordLine.orderLineItem.EP_WinDMS_StockHldngLocId__c = '';
        }
        
        Test.startTest();
        	localObj.setExistingOrderLineItems(OrderItems);
        	localObj.setOrderLineItemId(OrderObject,orderWrapper.OrderLines.OrderLine.get(0));
        Test.stopTest();
        
        System.AssertNotEquals(Null,orderWrapper.OrderLines.OrderLine.get(0).orderLineItem.id);
    }
    static testMethod void setOrdersByOrderNumber_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        Set<string> orderNumberSet = new Set<string>{EP_TestDataUtility.getNonConsignmentOrderPositiveScenario().orderNumber__c};
        Test.startTest();
        localObj.setOrdersByOrderNumber(orderNumberSet);
        Test.stopTest();
        System.AssertEquals(true,localObj.orderNumberOrderMap.keySet().size() > 0);
    }
    static testMethod void setExistingOrderLineItems_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        csord__Order__c orderObj = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        LIST<csord__Order_Line_Item__c> OrderItems = [SELECT Id, OrderId__c, EP_Order_Item_Key__c FROM csord__Order_Line_Item__c WHERE OrderId__c =:orderObj.id ];
        
        Test.startTest();
        	localObj.setExistingOrderLineItems(OrderItems);
        Test.stopTest();
        
        System.AssertNotEquals(Null,localObj.existingOrderLineItems);
    }
    static testMethod void getPriceBookEntry_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        csord__Order__c obj =  EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        Account shipTo = [select id,CurrencyISOCode,EP_Status__c,EP_Account_Type__c,EP_Customer_PO_Number__c,ParentId from Account where id=:obj.EP_ShipTo__c];
        localObj.accIdWithAccountMap.put(shipTo.Id,shipTo);        
        
        Test.startTest();
        	Map<String,PriceBookEntry> result = localObj.getPriceBookEntry(shipTo);
        Test.stopTest();
        
        System.AssertEquals(true,result.size() > 0);
    }
    static testMethod void validateAndUpdateOrderStatus_test() {
       Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_OrderDomainObject OrderDomainObject = EP_TestDataUtility.getVmiConsignmentOrderStatePlannedDomainObject();
        csord__Order__c OrderObject = ordMapper.getCSRecordById(OrderDomainObject.getOrder().Id);
        OrderObject.Status__c = 'Planned';
        OrderObject.EP_Order_Epoch__c = EP_Common_Constant.EPOC_CURRENT; 
        update OrderObject;
        
        Test.startTest();
        	List<csord__Order__c> result = localObj.validateAndSetOrderStatus(new Set<id>{OrderObject.id});
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);        
    }
    static testMethod void doPostStatusUpdateActions_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_OrderDomainObject OrderDomainObject = EP_TestDataUtility.getVmiConsignmentOrderStatePlannedDomainObject();
        csord__Order__c OrderObject = ordMapper.getCSRecordById(OrderDomainObject.getOrder().Id);
        OrderObject.Status__c = 'Planned';
        OrderObject.EP_Order_Epoch__c=EP_Common_Constant.EPOC_CURRENT; 
        update OrderObject;
		
        EP_OrderDomainObject orderDomain = new EP_OrderDomainObject(OrderObject.id);
        EP_OrderService orderService = new EP_OrderService(orderDomain);
        string orderEventStr = EP_OrderConstant.VMINEWORDER;
        EP_OrderEvent orderEvent = new EP_OrderEvent(orderEventStr);   
        // sets order status by calling state machine
        if(orderService.setOrderStatus(orderEvent)){
            localObj.orderServiceMap.put(OrderObject.id,orderService);
        }
        
        Test.startTest();
        	localObj.doPostStatusUpdateActions(new Set<id>{OrderObject.id});
        Test.stopTest();
        //Not Need to put Asserts as it is just delegating to other method
        system.assert(true);
    }
    static testMethod void setOrderStatus_test() {
        EP_WINDMSOrderNewHelper localObj = new EP_WINDMSOrderNewHelper();
        EP_OrderDomainObject OrderDomainObject = EP_TestDataUtility.getVmiConsignmentOrderStatePlannedDomainObject();
       
        csord__Order__c OrderObject = ordMapper.getCSRecordById(OrderDomainObject.getOrder().Id);
        Account VMIShipTo = EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();
        system.debug('**EP_ShipTo_Type__c**' + VMIShipTo.EP_ShipTo_Type__c);
        OrderObject.EP_ShipTo__c = VMIShipTo.Id;
        OrderObject.Status__c = 'Planned';
        OrderObject.EP_Order_Epoch__c = EP_Common_Constant.EPOC_CURRENT; 
        update OrderObject;
        
        Test.startTest();
        	csord__Order__c result = localObj.setOrderStatus(OrderObject.Id);
        Test.stopTest();
        //System.AssertEquals(null,VMIShipTo.EP_ShipTo_Type__c);
        System.AssertEquals(OrderObject.Status__c,result.Status__c);
    }
}