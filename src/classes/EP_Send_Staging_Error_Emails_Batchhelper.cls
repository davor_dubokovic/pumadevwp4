/****************************************************************
* @author       Accenture                                       *
* @name         EP_Send_Staging_Error_Emails_Batchhelper        *
* @Created Date 16/04/2018                                      *
* @description  Helper Class to send notifications              *
****************************************************************/
public class EP_Send_Staging_Error_Emails_Batchhelper{

    EP_RO_Import_Staging__c staging = new EP_RO_Import_Staging__c();
    static String emailAddress;
    static string htmlbody;
    
    public static string start()
    {
        string querystring = 'select Name, EP_CheckSum_Key__c, (select EP_LineNr__c, EP_SupplyLocation__r.EP_Email__c, EP_Remark_Reason__c, EP_Product_Code__c from RO_Import_Stagings__r where EP_SupplyLocation__r.EP_Email__c != Null and EP_Status__c = ' + '\''+'Error' + '\'' 
        + ') from EP_File__c where EP_StagingErrorCount__c > 0 and EP_EmailSent__c = False';
        return querystring;

    }
    public static void execute(list<EP_File__c> mylist)
    {
        Messaging.SingleEmailMessage emailObj;
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();

        for(EP_File__c fileObj: mylist){
        system.debug('=====fileObj' + fileObj);
        system.debug('====size' + fileObj.RO_Import_Stagings__r.Size());
            if(!fileObj.RO_Import_Stagings__r.isEmpty()){
                emailObj = new Messaging.SingleEmailMessage();
                setbody(fileObj);
                setstagingbody( fileObj.RO_Import_Stagings__r); 
                
                emailObj.toAddresses = new List<String>{emailAddress};
                emailObj.setSubject('Retail Import processing error -'+ fileObj.Name);
                emailObj.setHtmlBody(htmlbody);
                emailList.add(emailObj);  
                }
          }   
        system.debug('======list======' + emailList);  
        if(!emailList.isEmpty())
              Messaging.SendEmail(emailList);
        
    }
/****************************************************************
* @author       Accenture                                       *
* @name         setbody                                         *
* @description  method to set html body of the email            *
* @param        sObject                                         *
* @return       NA                                              *
****************************************************************/
    private static void setbody(EP_File__c fileObj)
    { 
        htmlbody = '<html><body>';
        htmlbody = 'Error occured while processing the retail file ' + fileObj.Name +'. The details are as below -<br/>';
        htmlbody += 'Filename:  ' +  fileObj.Name+'<br/>';
        htmlbody += 'Transaction No:  ' + fileObj.EP_CheckSum_Key__c + '<br/><br/>';
        htmlbody += '<table border="1" style="border-collapse: collapse"><tr><th>LineNr</th><th>Product</th><th>Error Description</th></tr>';

    }
/****************************************************************
* @author       Accenture                                       *
* @name         setstagingbody                                  *
* @description  method to set staging body                      *
* @param        List<sObject>                                   *
* @return       NA                                              *
****************************************************************/    
    private static void setstagingbody(list<EP_RO_Import_Staging__c> stageList)
    {    
        for(EP_RO_Import_Staging__c stageObj: stageList){
            htmlbody += '<tr><td>' + stageObj.EP_LineNr__c + '</td><td>' + stageObj.EP_Product_Code__c + '</td><td>' + stageObj.EP_Remark_Reason__c + '</td></tr>';
            emailAddress = stageObj.EP_SupplyLocation__r.EP_Email__c;
          }
        htmlbody += '</table><br/><br/>Regards,<br/>The Puma Team</body></html>';
    }
    
}