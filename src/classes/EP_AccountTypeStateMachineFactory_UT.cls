@isTest
public class EP_AccountTypeStateMachineFactory_UT
{
	static testMethod void getAccountStateMachine_test() {
		Test.startTest();
		EP_AccountTypeStateMachineFactory.accountDomain = EP_TestDataUtility.getSellToASAccountSetupDomainObject();
		EP_AccountStateMachine result = EP_AccountTypeStateMachineFactory.getAccountStateMachine();
		Test.stopTest();
		System.AssertNotEquals(null,result);
	}
	static testMethod void getAccountStateMachine1_test() {
		EP_AccountDomainObject currentAccount = EP_TestDataUtility.getSellToASAccountSetupDomainObject();
		Test.startTest();
		EP_AccountStateMachine result = EP_AccountTypeStateMachineFactory.getAccountStateMachine(currentAccount);
		Test.stopTest();
		System.AssertNotEquals(null,result);
	}
	static testMethod void getAccountStateMachineInstance_test() {
		EP_AccountDomainObject currentAccount = EP_TestDataUtility.getSellToASAccountSetupDomainObject();
		Test.startTest();
		EP_AccountStateMachine result = EP_AccountTypeStateMachineFactory.getAccountStateMachineInstance(currentAccount);
		Test.stopTest();
		System.AssertNotEquals(null,result);
	}
	static testMethod void getStateMachineType_test() {
		EP_AccountTypeStateMachineFactory.accountDomain = EP_TestDataUtility.getSellToASAccountSetupDomainObject();
		Test.startTest();
		String result = EP_AccountTypeStateMachineFactory.getStateMachineType();
		Test.stopTest();
		System.AssertNotEquals(null,result);
	}
}