/*
  @Author <CloudSense>
   @name <TrafiguraEditConfigurationController>
   @CreateDate <10/12/2017>
   @Description Test Class for the Controller: TrafiguraEditConfigurationController
   @Version <1.0>
 */

@isTest
public class TrafiguraEditConfigurationController_UT {
    
    @isTest
    static void openEditOrderPage_test(){
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
        insert fm;
        
        Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
        insert newAccount;
        
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, null, null);
        insert newOrder;
        
        PageReference pageRef = Page.EditOrder;
        pageRef.getParameters().put('id', newOrder.Id);
        Test.setCurrentPage(pageRef);

        Test.startTest();
            TrafiguraEditConfigurationController controller = new TrafiguraEditConfigurationController();
            PageReference pr = controller.openEditOrderPage();
        Test.stopTest();
        
        System.assertEquals('System.PageReference[/apex/customconfiguration?configId=null&linkedId='+String.valueOf(newOrder.csord__Identification__c)+']', 
                            String.valueOf(pr), 'Wrong string value returned.');
    }
    
    @isTest
    static void openEditOrderPage_cancelCheckDoneTrue_test(){
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
        insert fm;
        
        Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
        insert newAccount;
        
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, null, null);
        newOrder.Cancellation_Check_Done__c = true;
        insert newOrder;
        
        PageReference pageRef = Page.EditOrder;
        pageRef.getParameters().put('id', newOrder.Id);
        Test.setCurrentPage(pageRef);

        Test.startTest();
            TrafiguraEditConfigurationController controller = new TrafiguraEditConfigurationController();
            PageReference pr = controller.openEditOrderPage();
        Test.stopTest();
        
        System.assertEquals('System.PageReference[/apex/customconfiguration?configId=null&linkedId='+String.valueOf(newOrder.csord__Identification__c)+']', 
                            String.valueOf(pr), 'Wrong string value returned.');
    }
    
    @isTest
    static void openEditOrderPageWithId_test(){
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
        insert fm;
        
        Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
        insert newAccount;
        
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, null, null);
        insert newOrder;
        
        Test.startTest();
            String pr = TrafiguraEditConfigurationController.openEditOrderPageWithId(newOrder.Id);
        Test.stopTest();
        
        System.assertEquals('/apex/customconfiguration?configId=null&linkedId='+String.valueOf(newOrder.csord__Identification__c), pr, 'Wrong string value returned.');
    }
	
	@isTest
	static void openEditContractOrderPageWithId_test() {
		EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;
		
		Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
		insert newAccount;
		
		csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, null, null);
		insert newOrder;
		
		CS_ORDER_SETTINGS__c csOrderSetting = EP_TestDataUtility.createCSOrderCustomSetting(null);
		csOrderSetting.OpenOrderContractScreenFlow__c = 'Open Contract';
		update csOrderSetting;
		
		Test.startTest();
		String pr = TrafiguraEditConfigurationController.openEditContractOrderPageWithId(newOrder.Id);
		Test.stopTest();
		
		System.assertEquals('/apex/OpenOrderContract?configId=' + String.valueOf(newOrder.csordtelcoa__Product_Configuration__c) +
							'&linkedId=' + String.valueOf(newOrder.csord__Identification__c) +
							'&ScreenFlowName=' + String.valueOf(csOrderSetting.OpenOrderContractScreenFlow__c), pr, 'Wrong string value returned.');
	}
}