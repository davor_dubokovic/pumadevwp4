/* 
   @Author          Accenture
   @name            EP_StagingRetailOrderHelper 
   @CreateDate      13/04/2018
   @Description     Transformation class used to populate lookups and other calculated fields in the wrapper instance
   @Version         1.0
*/
public class EP_StagingRetailOrderHelper { 
    
    public static void ProcessRequest(String json){  
        EP_GeneralUtility.Log('private','EP_StagingRetailOrderHandler','parse');
        EP_StagingRetailOrderStub objStub =parse(json);
        boolean isError =ValidateItemsAndInsert(objStub);
    }    
    
    /**
    * @author           Accenture
    * @name             parse
    * @date             13/04/2018
    * @description      Method to parse i.e. deserialize the json string into EP_StagingRetailOrderStub stub class
    * @param            String
    * @return           EP_StagingRetailOrderStub
    */
    @TestVisible
    private static EP_StagingRetailOrderStub parse(String json){  
        EP_GeneralUtility.Log('private','EP_StagingRetailOrderHandler','parse');
        return (EP_StagingRetailOrderStub) System.JSON.deserialize(json, EP_StagingRetailOrderStub.class);

    }    
    
    public static boolean ValidateItemsAndInsert(EP_StagingRetailOrderStub stagingStub){ 
        EP_GeneralUtility.Log('private','EP_StagingRetailOrderHandler','ValidateItemsAndCreateAcknowledgement');
        Savepoint sp = Database.setSavepoint();
        boolean isError = false;
        if(EP_StagingRetailOrderHelper.ValidateFile(stagingStub.MSG.payload.any0.salesData.header))
            return true;  
        
        id Fileid =CreateFileRecord(stagingStub.MSG.payload.any0.salesData.header);
        list<EP_RO_Import_Staging__c> lstStaging =createStagingObject(stagingStub.MSG.payload.any0.salesData,Fileid,isError );
        if(InsertandCreateAcnowledgement(lstStaging,stagingStub.MSG.payload.any0.salesData.lines.line) || isError){
            Database.rollback( sp );
        }
        return isError;

    }    
    
    /**
    * @author           Accenture
    * @name             InsertandCreateAcnowledgement
    * @date             17/04/2018
    * @description      This method is used for insert stating record and create Line item Acknowledgement
    * @param            list<EP_RO_Import_Staging__c> list<EP_ROStagingAcknowledgementStub.Line>
    * @return           boolean
    */
    @TestVisible
    private static boolean InsertandCreateAcnowledgement(list<EP_RO_Import_Staging__c> lstStaging,list<EP_StagingRetailOrderStub.line> lstLine){ 
        EP_GeneralUtility.Log('private','EP_StagingRetailOrderHandler','InsertandCreateAcnowledgement');
        boolean isError = false;
        if(lstStaging.size()>0){    
            List<Database.SaveResult> results =  DataBase.insert(lstStaging,false); 
            for(Integer count = 0; count < results.size(); count++){ 
                if(!results[count].isSuccess()){
                    isError= true;
                    BindLineStatus(lstLine[count],EP_Common_Constant.ERROR,EP_GeneralUtility.getDMLErrorMessage(results[count]));
                }
                else{
                    BindLineStatus(lstLine[count],EP_Common_Constant.SUCCESS,EP_Common_Constant.Blank);
                }
            } 
        }
        return isError;
    } 
    
    
    
    /**
    * @author           Accenture
    * @name             createStagingOnject
    * @date             14/04/2018
    * @description      this method bind attribute in the Staging object
    * @param            EP_StagingRetailOrderStub ,id
    * @return           list<EP_RO_Import_Staging__c>
    */
    public static list<EP_RO_Import_Staging__c> createStagingObject(EP_StagingRetailOrderStub.salesData objSalesDta,id FileId, boolean isError){
        EP_GeneralUtility.Log('public','EP_StagingRetailOrderHelper','createStagingObject'); 
        list<EP_RO_Import_Staging__c> lstStaging = new list<EP_RO_Import_Staging__c>();
        
        for(EP_StagingRetailOrderStub.Line objLine :objSalesDta.lines.line){ 
            
            string msg =setStagingAttributes(objLine,objSalesDta.header,FileId,lstStaging); 
            if(!string.isBlank(msg)){
                BindLineStatus(objLine,EP_Common_Constant.ERROR,msg);
                isError =true;
            }            
        }
        return lstStaging;
    } 
    
    /**
    * @author           Accenture
    * @name             setStagingAttributes
    * @date             14/04/2018
    * @description      This method is for bind the staging object's attributes
    * @param            EP_StagingRetailOrderStub , id,list<EP_RO_Import_Staging__c>
    * @return           string
    */
    public static string setStagingAttributes(EP_StagingRetailOrderStub.line objLine,EP_StagingRetailOrderStub.header objHeader, id FileId, list<EP_RO_Import_Staging__c> lstStaging){
        EP_GeneralUtility.Log('public','EP_StagingRetailOrderHelper','setStagingAttributes'); 
        string msg ='';
        boolean isInvalidDate =false;
        try{
            EP_RO_Import_Staging__c objStaging = SetCommanStagingAttribute(objHeader,FileId ,isInvalidDate);       
            objStaging.EP_Ordered_quantity__c               = Decimal.valueof(objLine.qty);
            objStaging.EP_Ambient_Loaded_Quantity__c        = Decimal.valueof(objLine.qty);
            objStaging.EP_Standard_Loaded_Quantity__c       = Decimal.valueof(objLine.qty);
            objStaging.EP_Ambient_Delivered_Quantity__c     = Decimal.valueof(objLine.qty);
            objStaging.EP_Standard_Delivered_Quantity__c    = Decimal.valueof(objLine.qty);
            objStaging.EP_LineNr__c                         = objLine.lineNr;
            objStaging.EP_Product_Code__c                   = objLine.itemId;
            objStaging.EP_Product_Description__c            = objLine.descreption ;
            objStaging.EP_Unit_Price__c                     = Decimal.valueof(objLine.unitPrice);
            objStaging.recordTypeId                         = new EP_ROImportStagingMapper().getRecordTypeIdByName(EP_Common_Constant.RETAIL_ORDER_RT_NAME);
            lstStaging.add(objStaging);
        }   
        catch(exception ex){
            if(isInvalidDate)
            {
                msg= 'Invalid date ';
            }
            else
            {
                msg =ex.getmessage();
            }
            
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'setStagingAttributes',EP_StockLocationCreateUpdateHandler.class.getName(), ApexPages.Severity.ERROR);
        }       

        return msg;
    } 
        
    /**
    * @author           Accenture
    * @name             SetCommanStagingAttribute
    * @date             14/04/2018
    * @description      This method is for bind the staging object's attributes
    * @param            EP_StagingRetailOrderStub,id
    * @return           EP_RO_Import_Staging__c
    */
    private static  EP_RO_Import_Staging__c SetCommanStagingAttribute(EP_StagingRetailOrderStub.Header objHeader, id FileId , boolean isInvalidDate){
        EP_GeneralUtility.Log('public','EP_StagingRetailOrderHelper','SetCommanStagingAttribute'); 
        EP_RO_Import_Staging__c objStaging      = new EP_RO_Import_Staging__c();
        objStaging.EP_File_Name__c              = FileId;
        objStaging.EP_Transaction_No__c         = objHeader.transactionNr;
        objStaging.EP_CompanyCode__c            = objHeader.companyCode;
        objStaging.EP_Supply_Location_Code__c   = objHeader.posLocationId;
        objStaging.EP_Sell_To_Number__c         = objHeader.sellToId;
        objStaging.EP_Ship_To_Number__c         = objHeader.locationId;
        objStaging.EP_Product_Code__c           = objHeader.posLocationId;
        objStaging.EP_Loading_Date__c           = date.valueOf(dateValidatior(objHeader.transactionDt,isInvalidDate));
        objStaging.EP_CreatedDt__c              = date.valueOf(dateValidatior(objHeader.createdDt,isInvalidDate));
        objStaging.EP_CreatedBy__c              = objHeader.createdBy;
        objStaging.EP_SenderCode__c             = objHeader.senderCode;
        return objStaging;
    }
    
    
    
    /**
    * @author           Accenture
    * @name             dateValidatior
    * @date             15/12/2017
    * @description      Method for format date
    * @param            string,boolean
    * @return           string
    */
    public static string dateValidatior(string instStrDate , boolean isInvalidDate)
    {
        string dateRegExMain = '^((19|20)\\d\\d|(0[1-9]|[12][0-9]|3[01]))([-])(0[1-9]|1[012])([-])((0[1-9]|[12][0-9]|3[01])|(19|20)\\d\\d)$';
        string dateRegExPartial = '^(19|20)\\d\\d([-])(0[1-9]|1[012])([-])(0[1-9]|[12][0-9]|3[01])$';
        Pattern dateValidPattern = Pattern.compile(dateRegExMain);
        Matcher patternMatcher = dateValidPattern.matcher(instStrDate);
        isInvalidDate= false;
        if(Pattern.matches(dateRegExMain, instStrDate))
        {
            if(Pattern.matches(dateRegExPartial, instStrDate))
            {
                string[] strParts           = instStrDate.split('-');
                string strFormattedDate     = strParts[2] +'-'+strParts[1]+'-'+strParts[0];
                return strFormattedDate;
            }
            
        }
        else
            isInvalidDate= true;
        
        return instStrDate;
        
    }
    
    /**
    * @author           Accenture
    * @name             CreateFileRecord
    * @date             15/12/2017
    * @description      Method for insert file record
    * @param            EP_StagingRetailOrderStub.Header
    * @return           id
    */
    public static  id CreateFileRecord(EP_StagingRetailOrderStub.Header objHeader){
        EP_GeneralUtility.Log('public','EP_StagingRetailOrderHelper','CreateFileRecord');
        EP_File__c objFile = new EP_File__c();
        objFile.EP_CheckSum_Key__c = objHeader.transactionNr;
        insert objFile; 
        return objFile.id;
    }
    
    /**
    * @author           Accenture
    * @name             CreateLineAcknowledgement
    * @date             15/12/2017
    * @description      This method used to create line item Acknowledgement
    * @param            string,string,string
    * @return           EP_ROStagingAcknowledgementStub
    */
    public static  EP_StagingRetailOrderStub.Line BindLineStatus(EP_StagingRetailOrderStub.Line objLine, String status, String descreption){
        EP_GeneralUtility.Log('Public','EP_StagingRetailOrderHelper','CreateLineAcknowledgement');
        ObjLine.processingStatus = status;
        ObjLine.preocessingDesc = descreption;            
        return ObjLine;
    }
    
    /**
    * @author           Accenture
    * @name             ValidateFile
    * @date             15/12/2017
    * @description      
    * @param            EP_StagingRetailOrderStub.Header
    * @return           boolean
    */
    public static  boolean ValidateFile(EP_StagingRetailOrderStub.Header objHeader){
        EP_GeneralUtility.Log('Public','EP_StagingRetailOrderHelper','IsDuplicateFile');
        list<EP_File__c> lstFile = EP_ROImportFileMapper.getValidRecordbyTransactionNr(objHeader.transactionNr);
        boolean isDuplicate = false;
        if(!lstFile.isEmpty()){         
            if(lstFile[0].EP_StagingErrorCount__c==0){
                isDuplicate = true;
                objHeader.preocessingDesc = 'Duplicate File.';
                objHeader.processingStatus = 'Error';
            }
            else if(lstFile[0].EP_StagingErrorCount__c>0){ 
                Delete lstFile[0];
            }               
        }
        return isDuplicate;
    }
    
    /**
    * @author           Accenture
    * @name             DeleteExistingStagingRecord
    * @date             15/12/2017
    * @description      
    * @param            id
    * @return           NA
    */
    public static string createAcknowledgement(){
        EP_GeneralUtility.Log('Public','EP_StagingRetailOrderHelper','IsDuplicateFile');
        return '';
    }
}