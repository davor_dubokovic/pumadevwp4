/**
   @author          Shakti Mehtani
   @name            EP_ProfileMapper
   @CreateDate      12/27/2016
   @Description     This class contains all SOQLs related to Order Object
   @Version         1.0
   @reference       NA
*/

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
*/
public with sharing class EP_ProfileMapper {
 
    /**
    *  Constructor.
    *  @author          Shakti Mehtani 
    *  @name            EP_ProfileMapper
    *  @param           NA
    *  @return          NA 
    *  @throws          NA
    */   
    public EP_ProfileMapper() {} 
    
    
    /** This method is used to get profile records by Ids
    *  @author          Rahul Jain
    *  @name            getRecordsByIds
    *  @param           set<id>
    *  @return          list<Profile>
    *  @throws          NA
    */
    public list<Profile> getRecordsByIds(set<id> idSet) {
        list<Profile> profileList = new list<Profile>();
        
        for(list<Profile> profileObjList: [   SELECT id
                                                , Name 
                                                FROM Profile 
                                                WHERE Id IN :idSet
                                            ]){
            profileList.addAll(profileObjList);    
        }
        
        return profileList;
    }
    
    
    /** This method is used to get profile records by Name
    *  @author          Rahul Jain
    *  @name            getProfilesByName
    *  @param           List<string> list of profile names
    *  @return          list<Profile>
    *  @throws          NA
    */
    public List<Profile> getProfilesByName(List<String>listOfProfileNames){
        list<Profile> profileList = new list<Profile>();
		/*QUERY PROFILE BASED ON PROFILE NAME*/
		for(list<Profile> profileObjList: [Select id,Name From Profile Where Name =:listOfProfileNames]){
			profileList.addAll(profileObjList);
		}
         return profileList;
    }


     /** This method is used to get profile for Current User
    *  @author          Rahul Jain
    *  @name            getCurrentUserProfile
    *  @param           NA
    *  @return          Profile
    *  @throws          NA
    */
    public Profile getCurrentUserProfile() {

        Profile currentUserProfile = [SELECT Id, Name FROM Profile WHERE Id =: userinfo.getProfileId()];

        return currentUserProfile;
    }
}