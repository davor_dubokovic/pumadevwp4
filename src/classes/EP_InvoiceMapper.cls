/**
   @Author          Shakti Mehtani
   @name            EP_InvoiceMapper
   @CreateDate      01/01/2017
   @Description     This class contains all SOQLs related to Invoice Object
   @Version         1.0
   @reference       NA
*/

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    *************************************************************************************************************
*/
public with sharing class EP_InvoiceMapper {
    /**
    *  Constructor. 
    *  @name            EP_InvoiceMapper
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */
    public EP_InvoiceMapper() {}    
    

    /**  This method is used to get the list of Overdue Invoices 
    *  @author          Shakti Mehtani
    *  @date            01/01/2017
    *  @name            getOverdueInvoices
    *  @param           set<id>
    *  @return          list<EP_Invoice__c>
    *  @throws          NA
    */
    public list<EP_Invoice__c> getOverdueInvoices(set<id> idSet) {
        list<EP_Invoice__c> invoiceObjList = new list<EP_Invoice__c>();
        
        for (List<EP_Invoice__c> invoiceList:   [   SELECT 
                                                    id
                                                    , Name
                                                    , EP_Outstanding_Amount__c
                                                    , EP_Invoice_Due_Date__c 
                                                FROM EP_Invoice__c 
                                                WHERE EP_Bill_To__c IN :idSet 
                                                AND EP_Overdue__c = true 
                                                ORDER BY EP_Invoice_Due_Date__c DESC
                                            ]) {
            invoiceObjList.addAll(invoiceList);
        }       
        
        return invoiceObjList; 
    }
}