@isTest
public class EP_ASTSellToBasicDataSetupToRejected_UT
{
    static final string EVENT_NAME = '02-BasicDataSetupTo08-Rejected';
    static final string INVALID_EVENT_NAME = '08-ProspectTo04-Account Set-up';
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }

    static testMethod void isGuardCondition_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getDummySellToAsBasicDataSetup();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTSellToBasicDataSetupToRejected ast = new EP_ASTSellToBasicDataSetupToRejected();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void isTransitionPossible_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getDummySellToAsBasicDataSetup();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTSellToBasicDataSetupToRejected ast = new EP_ASTSellToBasicDataSetupToRejected();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void isRegisteredForEvent_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getDummySellToAsBasicDataSetup();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTSellToBasicDataSetupToRejected ast = new EP_ASTSellToBasicDataSetupToRejected();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    // method always returns true.
    /*static testMethod void isGuardCondition_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTSellToBasicDataToActive ast = new EP_ASTSellToBasicDataToActive();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }*/
    
    static testMethod void doOnExit_test(){
        EP_AccountDomainObject obj = EP_TestDataUtility.getDummySellToAsBasicDataSetup();
        EP_ASTSellToBasicDataSetupToRejected ast = new EP_ASTSellToBasicDataSetupToRejected();
        Test.startTest();
        ast.doOnExit();
        Test.stopTest();
                
    }
    
}