/*
 *  @Author <Accenture>
 *  @Name <EP_NonVMIShipToASActive>
 *  @CreateDate <>
 *  @Description <VMI Ship To Account State for 05-Active Status>
 *  @NovaSuite Fix -- comments added
 *  @Version <1.0>
 */
 public with sharing class EP_NonVMIShipToASActive extends EP_AccountState{
      /***NOvasuite fix constructor removed**/
    /*
     *  @Author <Accenture>
     *  @Name setAccountDomainObject
     *  @param EP_AccountDomainObject
     */
    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASActive','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }
    /*
     *  @Author <Accenture>
     *  @Name doOnEntry
     */
    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASActive','doOnEntry');
        // Changes made for CUSTOMER MODIFICATION L4 Start
        if(!this.account.isStatuschanged() && !account.isUpdatedFromNAV() &&!EP_IntegrationUtil.ISERRORSYNC){
            EP_AccountService service = new EP_AccountService(this.account);
            service.doActionSendRequest();
        // Changes made for CUSTOMER MODIFICATION L4 End
        } else if(this.account.isUnblocked()){ //Code Changes for #45362 Start
            EP_AccountService accountService = new EP_AccountService(this.account);
            accountService.doActionSendUpdateRequestToNavAndLomo();
        }
		//Code Changes for #45362 End
    }  
    /*
     *  @Author <Accenture>
     *  @Name doOnExit
     */
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASActive','doOnExit');
        
    }
    /*
     *  @Author <Accenture>
     *  @Name doTransition
     *  @return boolean
     */
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASActive','doTransition');
        return super.doTransition();
    }
    /*
     *  @Author <Accenture>
     *  @Name isInboundTransitionPossible
     *  @return boolean
     */
    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASActive','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    }

}