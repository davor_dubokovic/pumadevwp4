global class TrafiguraGetDefinedTolerance implements csoe.IRemoteAction {

	global Map<String, Object> execute(Map<String, Object> inputMap) {
		Map<String, Object> returnMap = new Map<String, Object>();
		if (inputMap.get('AccountId') != null) {
			String accId = (String) inputMap.get('AccountId');

            Account acc = [Select EP_Puma_Company__c from Account where id=:accId];
            if(acc!= null && acc.EP_Puma_Company__c!=null) {
                Company__c company = [Select EP_Tol_Stand_Amb__c,EP_Tol_Amb_Del_Qty_Inc__c,EP_Tol_Ord_Load_Inc__c,
                                            EP_Tol_Ord_Load_Dec__c, EP_Tol_Amb_Del_Qty_Dec__c
                                      from Company__c 
                                      where id = :acc.EP_Puma_Company__c];
                if(company!=null) {
                    returnMap.put('st_vs_am_q', company.EP_Tol_Stand_Amb__c);
                    returnMap.put('ord_vs_load__q_inc', company.EP_Tol_Ord_Load_Inc__c);
                    returnMap.put('ord_vs_load__q_dec', company.EP_Tol_Ord_Load_Dec__c);
                    returnMap.put('ord_vs_del__q_inc', company.EP_Tol_Amb_Del_Qty_Inc__c);
                    returnMap.put('ord_vs_del__q_dec', company.EP_Tol_Amb_Del_Qty_Dec__c);
                }
            }
		}
		return returnMap;
	}
	
}