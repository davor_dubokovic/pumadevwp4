/****************************************************************
* @author       Accenture                                       *
* @name         EP_Event_Subscription                           *
* @Created Date 2/1/2018                                        *
* @description                                                  *
****************************************************************/
@isTest
private class EP_Event_Subscription_UT {
	static Account acc;
	static EP_Event_Subscription subscribeEventObj;
	static EP_Notification_Account_Settings__c notObj;
	static Contact con;
	static Attachment att;

	@TestSetup
	static void setupData(){
		EP_AccountNotificationTestDataUtility.insertEmailTemplate(EP_AccountNotificationTestDataUtility.getFolder(system.label.EP_Folder_Name).id);
	}

	static void data(){
		EmailTemplate templateObj = [select id from EmailTemplate where developerName = 'UserTemplate'];
		acc = EP_TestDataUtility.getSellTo();
		con = EP_TestDataUtility.createTestRecordsForContact(acc);
		att = EP_AccountNotificationTestDataUtility.createAttachment(acc.id);
		EP_Notification_Type__c typeObj = EP_AccountNotificationTestDataUtility.createNotificationType(templateObj.id);
		typeObj.EP_Notification_Code__c = 'DIP REMINDERS';
		update typeObj;
		notObj = EP_AccountNotificationTestDataUtility.createAccountNotification(acc.id,templateObj.id,'',con.id,typeObj.id);
		subscribeEventObj = new EP_Event_Subscription();
		
	}
	
	@isTest static void subscribeEvent_Positive(){
		data();
		subscribeEventObj.subscriptionEventObj.eventType = 'DIP REMINDERS';
		subscribeEventObj.subscriptionEventObj.attachment = att;
		subscribeEventObj.subscriptionEventObj.sellToId = acc.id;
		subscribeEventObj.subscriptionEventObj.whatId = con.id;
		Test.startTest();
		subscribeEventObj.subscribeEvent();
		Test.stopTest();
		system.assertEquals(true,subscribeEventObj.saveResultobj.isSuccess());
	}

	@isTest static void subscribeEvent_Negative(){
		data();
		Test.startTest();
		subscribeEventObj.subscribeEvent();
		Test.stopTest();
		system.assert(subscribeEventObj.saveResultobj==null);
	}

	@isTest static void publishEvent_Positive(){
		data();
		subscribeEventObj.subscriptionEventObj.eventType = 'DIP REMINDERS';
		subscribeEventObj.subscriptionEventObj.attachment = att;
		subscribeEventObj.subscriptionEventObj.sellToId = acc.id;
		subscribeEventObj.subscriptionEventObj.whatId = con.id;
		notObj = [select id,EP_Notification_Type__r.name,EP_Recipient_Contact__c,EP_Recipient_User__c 
							from EP_Notification_Account_Settings__c where id =:notObj.id  limit 1];
		Test.startTest();
		subscribeEventObj.publishEvent(notObj);
		Test.stopTest();
		system.assertEquals(true,subscribeEventObj.saveResultobj.isSuccess());
	}

	@isTest static void publishEvent_Negative(){
		EP_Notification_Account_Settings__c notifyId = new EP_Notification_Account_Settings__c();
		Test.startTest();
		subscribeEventObj = new EP_Event_Subscription();
		subscribeEventObj.publishEvent(notifyId);
		Test.stopTest();
		system.assertEquals(false,subscribeEventObj.saveResultobj.isSuccess());
	}

	@isTest static void logNotification_Positive(){
		data();
		subscribeEventObj.subscriptionEventObj.eventType = 'DIP REMINDERS';
		subscribeEventObj.subscriptionEventObj.attachment = att;
		subscribeEventObj.subscriptionEventObj.sellToId = acc.id;
		subscribeEventObj.subscriptionEventObj.whatId = con.id;
		Test.startTest();
		subscribeEventObj.subscribeEvent();
		Test.stopTest();
		system.assertEquals(true,[select id from EP_Notification_Log__c].size()>0);
	}

	@isTest static void logNotification_Negative(){
		data();
		Test.startTest();
		subscribeEventObj.subscribeEvent();
		Test.stopTest();
		system.assertEquals(false,[select id from EP_Notification_Log__c].size()>0);
	}
}