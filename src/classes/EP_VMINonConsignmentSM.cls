/*   
     @Author Aravindhan Ramalingam
     @name <EP_VMINonConsignmentSM.cls>     
     @Description <VMI Non Consignment Statemachine >   
     @Version <1.1> 
     */

     public class EP_VMINonConsignmentSM extends EP_OrderStateMachine {
     	
     	public EP_VMINonConsignmentSM(){

     	}
     	
     	public override EP_OrderState getOrderState(EP_OrderEvent currentEvent){
     		EP_GeneralUtility.Log('Public','EP_VMINonConsignmentSM','getOrderState');
     		return super.getOrderState(currentEvent);
     	}
     }