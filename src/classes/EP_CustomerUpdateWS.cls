/* 
   @author <Ashok Arora>
   @name <EP_CustomerUpdateWS>
   @createDate <17/08/2016>
   @description <Rest Resource to update customer from NAV to SFDC> 
   @version <1.0>
*/
@RestResource(UrlMapping='/v1/cstmrUpdate')
global without sharing class EP_CustomerUpdateWS {
    
    /**
    * @author <Ashok Arora>
    * @name <updateCustomer>
    * @date <17/08/2016>
    * @description <THIS IS A HTTPPOST METHOD WHICH UPDATES CUSTOMER>
    * @version <1.0>
    * @param none 
    * @return void
    */
    @HttpPost
    global static void updateCustomer(){
        RestRequest req = RestContext.request;
        String requestBody = req.requestBody.toString();
        RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
        //EP_IntegrationService service = new EP_IntegrationService();
        //string response = service.handleCustomerUpdate(requestBody);
        EP_IntegrationService service = new EP_IntegrationService();
        string response =service.handleRequest(EP_Common_Constant.NAV_TO_SFDC_CUSTOMER_UPDATE,requestBody);
        RestContext.response.responseBody = blob.valueOf(response);
    }
}