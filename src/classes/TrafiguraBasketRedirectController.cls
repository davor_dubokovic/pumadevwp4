public class TrafiguraBasketRedirectController {

    public cscfga__Product_Basket__c basket { get; private set; }

    public TrafiguraBasketRedirectController(ApexPages.StandardController stdController) {
        this.basket = (cscfga__Product_Basket__c) stdController.getRecord();
    }

    public PageReference redirect() {
        String retUrl = ApexPages.currentPage().getParameters().get('retURL');
        Id trafiguraRootConfigId = this.findTrafiguraConfigurationId();
        if (trafiguraRootConfigId != null) {
            PageReference redirectToConfig = Page.customConfiguration;
            redirectToConfig.getParameters().put('configId', trafiguraRootConfigId);
            redirectToConfig.getParameters().put('linkedId', this.basket.id);
            redirectToConfig.getParameters().put('retURL', retUrl);
            return redirectToConfig;
        }

        PageReference defaultRedirect = Page.csbb__CSBasketRedirect;
        defaultRedirect.getParameters().put('id', this.basket.id);
        defaultRedirect.getParameters().put('retURL', retUrl);
        return defaultRedirect;
    }

    private Id findTrafiguraConfigurationId() {
        if (this.basket == null || this.basket.id == null) {
            return null;
        }

        List<cscfga__Product_Configuration__c> trafiguraOrderConfigs = [
            select
                id
            from cscfga__Product_Configuration__c
            where cscfga__product_basket__c = :this.basket.id and name = 'Puma Energy Order'
            order by createdDate
        ];

        if (!trafiguraOrderConfigs.isEmpty()) {
            return trafiguraOrderConfigs[0].id;
        }
        return null;
    }
}