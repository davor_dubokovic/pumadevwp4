@isTest
public class EP_OutboundMessageHeader_UT
{
    static testMethod void outboundMessageHeader_test() {
        Test.startTest();
        EP_OutboundMessageHeader localObj = new EP_OutboundMessageHeader();
        String MsgID =localObj.MsgID;
    	String InterfaceType =localObj.InterfaceType;
        String SourceGroupCompany=localObj.SourceGroupCompany;
    	String DestinationGroupCompany=localObj.DestinationGroupCompany;
    	String SourceCompany=localObj.SourceCompany;
    	String DestinationCompany=localObj.DestinationCompany;
    	String CorrelationID=localObj.CorrelationID;
    	String DestinationAddress=localObj.DestinationAddress;
    	String SourceResponseAddress=localObj.SourceResponseAddress;
    	String SourceUpdateStatusAddress=localObj.SourceUpdateStatusAddress;
    	String DestinationUpdateStatusAddress =localObj.DestinationUpdateStatusAddress;
    	String MiddlewareUrlForPush =localObj.MiddlewareUrlForPush;
    	String EmailNotification =localObj.EmailNotification;
    	String ErrorCode =localObj.ErrorCode;
    	String ErrorDescription =localObj.ErrorDescription;
    	String ProcessingErrorDescription =localObj.ProcessingErrorDescription;
    	Boolean ContinueOnError =localObj.ContinueOnError;
    	Boolean ComprehensiveLogging =localObj.ComprehensiveLogging;
    	String TransportStatus =localObj.TransportStatus;
    	String ProcessStatus =localObj.ProcessStatus;
    	Boolean UpdateSourceOnReceive =localObj.UpdateSourceOnReceive;
    	Boolean UpdateSourceOnDelivery =localObj.UpdateSourceOnDelivery;
    	Boolean UpdateSourceAfterProcessing =localObj.UpdateSourceAfterProcessing;
    	Boolean UpdateDestinationOnDelivery =localObj.UpdateDestinationOnDelivery;
    	Boolean CallDestinationForProcessing =localObj.CallDestinationForProcessing;
    	String ObjectType =localObj.ObjectType;
    	String ObjectName =localObj.ObjectName;
    	String CommunicationType =localObj.CommunicationType;
        Test.stopTest();
        System.AssertEquals(EP_Common_Constant.TABLE_STRING,localObj.ObjectType );
        System.AssertEquals(EP_Common_Constant.ASYNC,localObj.CommunicationType );
    }
}