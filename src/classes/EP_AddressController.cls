/* 
@Author <Ashok Arora (Accenture)>
@name <EP_AddressController>
@CreateDate <02/05/2016>
@Description <Class to get address value>
@Version <1.0>
*/
public with sharing class EP_AddressController { 

    public EP_AddressControllerContext addressContext {get; set;}
    
    private static final String QUERY_FROM = 'Select Id,RecordType.DeveloperName from ';
    private static final String QUERY_WHERE = ' Where Id =\'';
    private static final String END_STRING = '\' ';
    private static final String STREET_NUMBER_STR = 'street_number';
    private static final String ROUTE_STR = 'route';
    private static final String LOCALITY_STR = 'locality';
    private static final String ADMIN_AREA_STR = 'administrative_area_level_1';
    private static final String POSTAL_CODE_STR = 'postal_code';
    private static final String FETCHKEY='fetchKey';
    private static final String CLASSNAME ='EP_AddressController';
    private static final String LAT_STR = 'lat';
    private static final String LNG_STR = 'lng';
    private static final String ADD_CR_FIELD = 'Address CR Field';
    private static final String UPDATE_STR = 'UPDATE';
    private static final String CR_STR = 'CR';
    private static final String ADRESS_STR = 'address';
    private static final String CUSTOM_LAT= 'latitude__s';
    private static final String CUSTOM_LNG= 'longitude__s'; 
    private static final String STR_C = 'c';
    private static final String IS_ADDRESS_VALID = 'IS ADDRESS VALID';
    private static final String EDIT_ACCESS_ERROR = 'You do not have edit access on this record.';
    EP_CustomSettingMapper customSettingMapper = new EP_CustomSettingMapper();
    
    /**
    * @author <Ashok Arora>
    * @date <02/05/2016>
    * @description <Constructor>
    * @param none
    * @return none
    */
    public EP_AddressController(){
        addressContext = new EP_AddressControllerContext(ApexPages.currentPage().getParameters());
    } 
    
    /**
    * @author <Ashok Arora>
    * @date <02/05/2016>
    * @description <To fetch key for address>
    * @param none
    * @return void
    */
    public void fetchKey(){
        EP_GeneralUtility.Log('Public','EP_AddressController','fetchKey');     
        try{
            List<EP_Address_Fields__mdt> addressField = customSettingMapper.getAddressDataByName(EP_COMMON_CONSTANT.GGL_API_KEY);
            addressContext.googleMapKey = addressField[0].EP_Field_Label__c;
        }catch(Exception handledException){
            EP_LoggingService.logHandledException( handledException, EP_Common_Constant.EPUMA, FETCHKEY, CLASSNAME, ApexPages.Severity.ERROR );
        }
    } 
        
    /**
    * @author <Amit Singh>
    * @date <02/03/2017>
    * @description <this method returns blank string in passed values is null , if not null returns same string.>
    * @param String
    * @return string
    */
    @TestVisible
    private string removeNullString(string value){
        EP_GeneralUtility.Log('Private','EP_AddressController','removeNullString');
        return string.isNotBlank(value)? value : EP_Common_Constant.BLANK;
    }
    
    /**
    * @author <Amit Singh>
    * @date   <02/03/2017>
    * @description <This method populates the address fields on sobject>
    * @param  AddressWrapper, Map<String, String>
    * @return NA
    */
    @TestVisible
    private void populateAddressFieldOnRecord(){
        addressContext.setAddressFields();   
    }
    
    /**
    * @author <Amit Singh>
    * @date   <02/03/2017>
    * @description <This method creates the address wrapper from address string>
    * @param  String, String
    * @return AddressWrapper
    */
    /*private AddressWrapper createAddressObject(String address, String oldaddrs){
        EP_GeneralUtility.Log('Private','EP_AddressController','createAddressObject');
        AddressWrapper addressObj = addressContext.getAddressObject(address,oldaddrs);
        return addressObj;
    } */ 
    
    /**
    * @author <Amit Singh>
    * @date   <02/03/2017>
    * @description <This method returns creates address string to save on request line>
    * @param  AddressWrapper
    * @return String
    */
    /*private String createAddressString(){
        EP_GeneralUtility.Log('Private','EP_AddressController','createAddressString');
        return  addressContext.getAddressAsString();
    }*/
    
    /**
    * @author <Amit Singh>
    * @date   <02/03/2017>
    * @description <this method process the addresswrapper object and update tht address on sobject . >
    * @param  AddressWrapper, Map<String, String>
    * @return void
    */
    @TestVisible
    private pageReference processAddress(){     
        pageReference pgref;   
        EP_CountryMapper countryMapper = new EP_CountryMapper();
        List<EP_Country__c> countryOnRec = countryMapper.getRecordsByCountryCode(new set<string> {addressContext.countryCode});     
        if(addressContext.isValidCountry()){            
            List<EP_Country__c> countryRec = countryMapper.getRecordsByNames(new list<String>{addressContext.addressObj.country});
            addressContext.enteredCntryCode = countryRec[0].EP_Country_Code__c;
            addressContext.addressObj.country = addressContext.isCountryReference ? string.valueOF(countryRec[0].id) : countryRec[0].Name;
            addressContext.addressObj.countryRecord = countryRec[0];
            addressContext.addressObj.cntryCode = countryRec[0].EP_Country_Code__c;
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.EP_Invalid_Country));
        }    
        if(!addressContext.isAddressFromValidCountry()){
            addressContext.showError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.EP_Address_SelectMsg + EP_Common_Constant.SPACE + countryOnRec[0].Name));
        }        
        if(!addressContext.showError){
            populateAddressFieldOnRecord();
            pgref= addressContext.commitAddressData();           
        }
        return pgref;
    }
    
    /**
    * @author <Amit Singh>
    * @date   <02/03/2017>
    * @description <this method query the record dynamically from ID>
    * @param  NA
    * @return sObject
    */
    @TestVisible
    private sObject queryRecord(){
        EP_GeneralUtility.Log('Private','EP_AddressController','queryRecord');
        sObject recordToProcess ;
        if(addressContext.hasRecordType){
            String query = QUERY_FROM + addressContext.objectName + QUERY_WHERE + addressContext.recordId + END_STRING ;
            recordToProcess = DataBase.query(query);
            addressContext.recordTypeName = (String)recordToProcess.getSObject(EP_Common_Constant.RECORD_TYPE ).get(EP_Common_Constant.DEVELOPER_NAME );
        }
        else{
            recordToProcess = addressContext.sObjectType.newSObject(addressContext.recordId);
        }
        return recordToProcess;
    }
    
    /**
    * @author <Ashok Arora>
    * @date <02/05/2016>
    * @description <To update address using Page>
    * @param none
    * @return void
    */
    public PageReference updateAddress(){
        EP_GeneralUtility.Log('Public','EP_AddressController','updateAddress');
        PageReference pgref;
        map<String,String>mTypeField = new map<String,String>();
        string country;       
        try{            
            addressContext.showError = false;
            addressContext.record = queryRecord();
            EP_CustomSettingMapper cstMapper = new EP_CustomSettingMapper ();
            for(EP_Address_Fields__mdt addressField : cstMapper.queryAddressFieldCustomMetaDataTypes(addressContext.objectName)){
                if(isValidRecordType(addressContext.hasRecordType, addressField)){
                    addressContext.setContextVlaues(addressField);
                }
            }
            if(String.isNotBlank(addressContext.address)){
                addressContext.setAddressWrapperObject();
                pgref = processAddress();
            }
            else{
                addressContext.showError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,LABEL.EP_No_address_selected));
            }
        }
        catch(Exception handledException){
            addressContext.showError = true;
            ApexPages.addMessages(handledException);
        }
        return pgref;
        
    }
    
    @TestVisible
    private boolean isValidRecordType(Boolean recTypeAvailabe, EP_Address_Fields__mdt addressField){
        EP_GeneralUtility.Log('Private','EP_AddressController','isValidRecordType');
        return ((recTypeAvailabe && (addressField.EP_Record_Type__c.containsIgnoreCase(addressContext.recordTypeName) || addressField.EP_Record_Type__c.equalsIgnoreCase(EP_Common_Constant.ANYString )))
            || !recTypeAvailabe);
    }
    
    /**
    * @author <Ashok Arora>
    * @date <02/05/2016>
    * @description <To cancel the operation using cancel button>
    * @param none
    * @return PageReference
    */
    public PageReference cancel(){
        EP_GeneralUtility.Log('Public','EP_AddressController','cancel');
        String recId;
        PageReference page ;
        try{ 
            //recId= (Id)ApexPages.currentPage().getParameters().get(EP_Common_Constant.ID);
        page = new pageReference(EP_COMMON_CONSTANT.SLASH + addressContext.recordId);
        }catch(Exception handledException){
            System.debug(handledException);
            addressContext.showError = true;
            ApexPages.addMessages(handledException);
        }
        return page;
    }
}