/* 
   @Author <Ashok Arora>
   @name <EP_StorageLocationHelper>
   @CreateDate <03/11/2015>
   @Description <This class handles requests from EP_StorageLocationHandler> 
   @Version <1.0>
*/
public without sharing class EP_StorageLocationTriggerHelper {
    
    private static final String PRIMARY = 'Primary';
    private static final String SECONDARY = 'Secondary';
    private static final String RESTRICT_ADD_REMOVE_LOCATION = 'restrictAddRemoveLocation';
    private static final String EP_STORAGE_LOCATION_TRIGGER_HELPER = 'EP_StorageLocationTriggerHelper';
    private static final String RESTRICT_ONE_SECONDARY_LOCATION = 'restrictOneSecondaryLocation';
    private static final String RESTRICT_ONE_PRIMARY_LOCATION =  'restrictOnePrimaryLocation';
    private static final String CAN_NOT_CHANGE_STATUS_INACTIVE = 'cannotChangeStatusToInactive';
    private static final String CHECK_SHIPTO_ROUTE_ALLOCATION = 'checkShipToRouteAllocation';
    private static final String SET_SUPPLY_LOCATION_STATUS = 'setSupplyLocationStatus';
    private static final String SUPP_OPT_DOES_NOT_MATCH = 'Suply Option Ship-To Combination does not match.';
    private static final String CANT_ADD_DEF_ROUTE = 'Cannot add the default route as there is no Route Allocation Record associated with the given Route record.';
    
    /**
	 * @author <Ashok Arora>
	 * @date  <03/11/2015>
	 * @description < This method checks for duplicate entry of stock holding location in same ship-to/sellto>
	 * @param Set<id>,List<EP_Stock_Holding_Location__c>
	 * @return None
	 */      
    private static void checkForDulplicateLocaton(Set<id> parentIds, List<EP_Stock_Holding_Location__c>listOfNewSupplyOption){
        
        Map<Id, Map<Id, EP_Stock_Holding_Location__c>> stockHoldLocEmptyNestedMap = new Map<Id, Map<Id, EP_Stock_Holding_Location__c>>();
        Map<Id, EP_Stock_Holding_Location__c> stockHoldLocEmptyMap = new Map<Id, EP_Stock_Holding_Location__c>();
        If(parentIds != null && !parentIds.isEmpty()){
            Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
            Map<Id, Map<Id, Map<Id, EP_Stock_Holding_Location__c>>> stockHoldingMap = new Map<Id, Map<Id, Map<Id, EP_Stock_Holding_Location__c>>>();
            Id parentId ;
            for(EP_Stock_Holding_Location__c shl : [Select EP_Ship_To__c,Stock_Holding_Location__c, EP_Sell_To__c
                                                      from EP_Stock_Holding_Location__c 
                                                     where (EP_Ship_To__c in : parentIds
                                                            OR EP_Sell_To__c in : parentIds)
                                                     Limit : nRows]){
                
                parentId = getParentIdOfSupplyOption(shl);
                
                if(parentId != null){
                
                    if(!stockHoldingMap.containsKey(parentId)){
                        stockHoldingMap.put(parentId, stockHoldLocEmptyNestedMap ); // new Map<Id, Map<Id, EP_Stock_Holding_Location__c>>());
                    }
                    if(!stockHoldingMap.get(parentId).containskey(shl.Stock_Holding_Location__c)){
                        stockHoldingMap.get(parentId).put(shl.Stock_Holding_Location__c, stockHoldLocEmptyMap ); // new Map<Id, EP_Stock_Holding_Location__c>());
                    }
                    stockHoldingMap.get(parentId).get(shl.Stock_Holding_Location__c).put(shl.id, shl);
                }   
            }
            
            for(EP_Stock_Holding_Location__c sh : listOfNewSupplyOption){
                
                parentId = getParentIdOfSupplyOption(sh);
                
                if((stockHoldingMap.containsKey(parentId) && stockHoldingMap.get(parentId).containsKey(sh.Stock_Holding_Location__c)) && 
                    (sh.id == null ||(sh.id != null && !stockHoldingMap.get(parentId).get(sh.Stock_Holding_Location__c).containsKey(sh.id))) ){
                        sh.Stock_Holding_Location__c.addError(Label.EP_Duplicate_Stock_Holding_Location);
                        
                }else{
                    if(!stockHoldingMap.containsKey(parentId)){
                        stockHoldingMap.put(parentId, stockHoldLocEmptyNestedMap ); //new Map<Id, Map<Id, EP_Stock_Holding_Location__c>>());
                    }
                    if(!stockHoldingMap.get(parentId).containskey(sh.Stock_Holding_Location__c)){
                        stockHoldingMap.get(parentId).put(sh.Stock_Holding_Location__c, stockHoldLocEmptyMap ); //new Map<Id, EP_Stock_Holding_Location__c>());
                    }
                    if(sh.id != null){
                        stockHoldingMap.get(parentId).get(sh.Stock_Holding_Location__c).put(sh.id, sh);
                    }
                }
            }
        }   
    }
    
    private static string getParentIdOfSupplyOption(EP_Stock_Holding_Location__c supplyOptionObject){
    	return supplyOptionObject != null ? 
    		   (supplyOptionObject.EP_Sell_To__c != null ? supplyOptionObject.EP_Sell_To__c : supplyOptionObject.EP_Ship_To__c)
    		   : null;
    
    }
    
    
    /**
	 * @author <Amit SIngh>
	 * @date  <01/25/2017>
	 * @description <This method validates that selected storage location for delivery type supply option should be available on parent sell To>
	 * @param Set<Id>
	 * @return None
	 */  
    private static void validateStorageLocationForDeliverySupplyOption(Map<Id, Account> mapOfIdShipTos, 
                                                                        Set<Id> setOfShipToIds,
                                                                        List<EP_Stock_Holding_Location__c> listOfNewSupplyOption){
    	if(setOfShipToIds != null && !setOfShipToIds.isEmpty()){
            Map<Id, Set<Id>> mapOfIdStorageLocationIdSet = new  Map<Id, Set<Id>>(); 
            Map<Id, Id> mapOfShipToIdSellToId = new Map<Id, Id>();
            Set<Id> sellToEmptySet = new Set<Id>();
            Integer nRows = EP_Common_Util.getQueryLimit();
            if(mapOfIdShipTos != null){
	            for(Account sellTo : mapOfIdShipTos.values()){
	                if(sellTo.parentId != null){
	                    mapOfShipToIdSellToId.put(sellTo.id, sellTo.parentId);
	                }   
	            }
            }
            if(!mapOfShipToIdSellToId.isempty()){
                nRows = EP_Common_Util.getQueryLimit();
                for(EP_Stock_Holding_Location__c shl: [Select id, EP_Sell_To__c,Stock_Holding_Location__c from EP_Stock_Holding_Location__c 
                                                        where EP_Sell_To__c in : mapOfShipToIdSellToId.values()  AND Stock_Holding_Location__c != null LIMIT :nRows]){
                    
                    if(!mapOfIdStorageLocationIdSet.containsKey(shl.EP_Sell_To__c)) {
                        mapOfIdStorageLocationIdSet.put(shl.EP_Sell_To__c, sellToEmptySet); // new Set<Id>());
                    }
                    mapOfIdStorageLocationIdSet.get(shl.EP_Sell_To__c).add(shl.Stock_Holding_Location__c);
                }
            }

            for(EP_Stock_Holding_Location__c storageLocation : listOfNewSupplyOption){
                Boolean isNotValidLocation = isStorageLocationNotAssignedOnSellTo(storageLocation, mapOfShipToIdSellToId, mapOfIdStorageLocationIdSet);
	            if(isNotValidLocation){
                    storageLocation.addError(system.Label.EP_Supply_Location_on_Ship_To_Not_Assigned_on_Sell_To_Error);             
                }
            }
    	}  
    }
    
    /**
	 * @author <Amit SIngh>
	 * @date  <01/25/2017>
	 * @description <This method returns true if storage location selected on delivery supply opion is not available on sellTo, other wise returns false>
	 * @param EP_Stock_Holding_Location__c, Map<Id, Id>, Map<Id, Set<Id>>
	 * @return Boolean
	 */  
    private static Boolean isStorageLocationNotAssignedOnSellTo(EP_Stock_Holding_Location__c storageLocation, 
    															Map<Id, Id> mapOfShipToIdSellToId, 
    															Map<Id, Set<Id>> mapOfIdStorageLocationIdSet){
    
    	return storageLocation.EP_Ship_To__c != null && storageLocation.Stock_Holding_Location__c != null &&
               ((mapOfShipToIdSellToId.containsKey(storageLocation.EP_Ship_To__c) && 
                        ((mapOfIdStorageLocationIdSet.containsKey(mapOfShipToIdSellToId.get(storageLocation.EP_Ship_To__c)) && 
                            !mapOfIdStorageLocationIdSet.get(mapOfShipToIdSellToId.get(storageLocation.EP_Ship_To__c)).contains(storageLocation.Stock_Holding_Location__c))
                          || 
                          !mapOfIdStorageLocationIdSet.containsKey(mapOfShipToIdSellToId.get(storageLocation.EP_Ship_To__c))
                         )
                )
                || 
                !mapOfShipToIdSellToId.containsKey(storageLocation.EP_Ship_To__c));
    
    }
    
    /**
     * @author <Amit SIngh>
     * @date  <01/25/2017>
     * @description <This method validate data for supply options. This method is getting called from EP_StorageLocationTriggerHandler class.>
     * @param List<EP_Stock_Holding_Location__c>
     * @return none
     */  
    public static void validateSupplyOption(List<EP_Stock_Holding_Location__c> listOfNewSupplyOption){
		
		set<Id> setOfAccountIds  = new set<Id>();
		set<Id> setOfShipToIds = new Set<Id>();
		for(EP_Stock_Holding_Location__c storageLocation : listOfNewSupplyOption){
            if(storageLocation.EP_Ship_To__c != null){
                /*if(storageLocation.EP_Location_Type__c==PRIMARY){
                    sShipTo.add(storageLocation.EP_Ship_To__c);
                }*/
                setOfShipToIds.add(storageLocation.EP_Ship_To__c);
            }
            if(storageLocation.EP_Sell_To__c != null){
                setOfAccountIds.add(storageLocation.EP_Sell_To__c);
            }
        }
        
        // validate ship to storage location 
        if(!setOfShipToIds.isEmpty()){
            validateDeliverySupplyOption(setOfShipToIds, listOfNewSupplyOption) ;
            setOfAccountIds.addAll(setOfShipToIds);
        }
        
        if(!setOfAccountIds.isEmpty()){
           checkForDulplicateLocaton(setOfAccountIds, listOfNewSupplyOption);
        }
        
    }
    
    /**
     * @author <Amit Singh>
     * @date  <01/25/2017>
     * @description <This method validate data for delivery supply option>
     * @param Set<Id>, List<EP_Stock_Holding_Location__c>
     * @return None
     */
    private static void validateDeliverySupplyOption(Set<Id> setOfShipToIds, 
    											List<EP_Stock_Holding_Location__c> listOfNewSupplyOption){
    	
    	if(!setOfShipToIds.isEmpty()){
    		Integer nRows = EP_Common_Util.getQueryLimit();
    		Map<Id, Account> mapOfIdShipTos =  new Map<Id, Account>([Select id, Name, ParentId 
									                                   From Account 
									                                   Where id in : setOfShipToIds LIMIT :nRows]);
    		// user can select only those storage location which are available on related sell-to
    		validateStorageLocationForDeliverySupplyOption(mapOfIdShipTos, setOfShipToIds, listOfNewSupplyOption);
    		restrictUserToSelectMultiplePrimaryLocation(mapOfIdShipTos, listOfNewSupplyOption);
    		restrictUserToSelectMultipleSecondaryLocation(mapOfIdShipTos, listOfNewSupplyOption);
        } 
    }
    
    /**
     * @author <Amit Singh>
     * @date  <01/25/2017>
     * @description <This method restricts user to select multiple primary locations for one ship-To>
     * @param Map<Id, Account>, List<EP_Stock_Holding_Location__c>
     * @return None
     */
    private static void restrictUserToSelectMultiplePrimaryLocation(Map<Id, Account> mapOfIdShipTos,  List<EP_Stock_Holding_Location__c>listOfNewSupplyOption){
		Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
		Map<Id, EP_Stock_Holding_Location__c> mapOfIdPrimarySupplyOption = new Map<Id, EP_Stock_Holding_Location__c>();
        if(mapOfIdShipTos != null){
	        for(EP_Stock_Holding_Location__c storageLocation : [Select id, EP_Location_Type__c, Stock_Holding_Location__c, EP_Ship_To__c
	                                                              From EP_Stock_Holding_Location__c
	                                                             Where EP_Location_Type__c=:PRIMARY
	                                                               And EP_Ship_To__c IN:mapOfIdShipTos.keySet()
	                                                             limit :nRows]){
	            mapOfIdPrimarySupplyOption.put(storageLocation.EP_Ship_To__c,storageLocation);
	        }
	        //IF THE RELATED SHIP-TO ALREADY HAS PRIMARY STOCK HOLDING LOCATION THEN THROW ERROR
            for(EP_Stock_Holding_Location__c storageLocation : listOfNewSupplyOption){
                 if(storageLocation.EP_Location_Type__c==PRIMARY && mapOfIdPrimarySupplyOption.containsKey(storageLocation.EP_Ship_To__c)
                    &&(String.isBlank(storageLocation.id)
                        ||storageLocation.id != mapOfIdPrimarySupplyOption.get(storageLocation.EP_Ship_To__c).id)){
                    storageLocation.addError(System.Label.EP_No_More_Than_One_Primary_Location);
                }
                else{
                    if(storageLocation.EP_Location_Type__c==PRIMARY
                        && !mapOfIdPrimarySupplyOption.containsKey(storageLocation.EP_Ship_To__c)){
                        mapOfIdPrimarySupplyOption.put(storageLocation.EP_Ship_To__c,storageLocation);
                    }
                }
            }
        }
    }
    
    /**
     * @author <Amit Singh>
     * @date  <01/25/2017>
     * @description <This method restricts user to select multiple secondary locations for one ship-To>
     * @param Map<Id, Account>, List<EP_Stock_Holding_Location__c>
     * @return None
     */
    private static void restrictUserToSelectMultipleSecondaryLocation(Map<Id, Account> mapOfIdShipTos, List<EP_Stock_Holding_Location__c> listOfNewSupplyOption){
        Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
        Map<Id, EP_Stock_Holding_Location__c> mapOfIdSecondarySupplyOption = new Map<Id, EP_Stock_Holding_Location__c>();
        
        if(mapOfIdShipTos != null){
            for(EP_Stock_Holding_Location__c storageLocation : [Select id, EP_Location_Type__c, Stock_Holding_Location__c, EP_Ship_To__c
                                                                  From EP_Stock_Holding_Location__c
                                                                 Where EP_Location_Type__c=:SECONDARY
                                                                   And EP_Ship_To__c IN:mapOfIdShipTos.keySet()     
                                                                 limit :nRows]){
                mapOfIdSecondarySupplyOption.put(storageLocation.EP_Ship_To__c,storageLocation);
            }
            //IF THE RELATED SHIP-TO ALREADY HAS PRIMARY STOCK HOLDING LOCATION THEN THROW ERROR
            for(EP_Stock_Holding_Location__c storageLocation : listOfNewSupplyOption){
                 if(storageLocation.EP_Location_Type__c == SECONDARY && mapOfIdSecondarySupplyOption.containsKey(storageLocation.EP_Ship_To__c)
                    &&(String.isBlank(storageLocation.id)
                        ||storageLocation.id != mapOfIdSecondarySupplyOption.get(storageLocation.EP_Ship_To__c).id
                    )){
                    storageLocation.addError(System.Label.EP_No_More_Than_One_Secondary_Location);
                }
                else{
                    if(storageLocation.EP_Location_Type__c == SECONDARY
                        && !mapOfIdSecondarySupplyOption.containsKey(storageLocation.EP_Ship_To__c)){
                        
                        mapOfIdSecondarySupplyOption.put(storageLocation.EP_Ship_To__c, storageLocation);
                    }
                }
            }    
         }
    }

    /**DEFECT 43913 FIX END**/
    
     /**L4_45352_START**/
     /**
     * @author <Accenture>
     * @description <Prevent user from deleting storage location of sell to its associated ship to is having primary storage location>
     * @param List<EP_Stock_Holding_Location__c>
     * @return None
     */
    Public static void isSellToSupplyOptionPrimaryonShipTo(List<EP_Stock_Holding_Location__c> supplyoptionList)
    {
        EP_StockHoldingLocationMapper shlMapper =  new EP_StockHoldingLocationMapper();     
        EP_AccountMapper accMapper = new EP_AccountMapper();
        set<id> sellToIds = new set<id>();
        map<id,Boolean> mapSellToAssociated = new map<id,Boolean>();
        list<Account> accountShipToList = new List<Account>();
        for(EP_Stock_Holding_Location__c suppplyOption : supplyoptionList){
            if(suppplyOption.EP_Sell_To__c!=null)
                sellToIds.add(suppplyOption.EP_Sell_To__c);
        }
        if(sellToIds.size()>0){
            for(Account acc : accMapper.getShipToRecords(sellToIds)){
               if(acc.ChildAccounts.size()>0){
                   accountShipToList.addall(acc.ChildAccounts);
               }
            }
        }       
        for(EP_Stock_Holding_Location__c Location : shlMapper.getPrimaryStockLoactionsForShipTos(accountShipToList)) {
                mapSellToAssociated.put(Location.EP_Ship_To__r.ParentId,true);
        }
        if(mapSellToAssociated.size()>0){
            for(EP_Stock_Holding_Location__c suppplyOption : supplyoptionList){
                if(mapSellToAssociated.containsKey(suppplyOption.EP_Sell_To__c) && mapSellToAssociated.get(suppplyOption.EP_Sell_To__c)){
                    suppplyOption.addError(Label.EP_On_Supply_Location_Deletion);
                }
            }
        }
    }
    /**L4_45352_end**/ 
}