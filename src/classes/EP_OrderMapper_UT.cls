@isTest
public class EP_OrderMapper_UT
{
    /*static testMethod void getRecordsByIds_test() {
        EP_OrderMapper localObj = new EP_OrderMapper();
        Set<id> idSet = new Set<id>{EP_TestDataUtility.getSalesOrderPositiveScenario().id};
        Test.startTest();
        LIST<Order> result = localObj.getRecordsByIds(idSet);
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
    
    static testMethod void getOpenOrderOfTransporters_test() {
        EP_OrderMapper localObj = new EP_OrderMapper();
        Order orderObject = [Select id,EP_Transporter__c from Order where Id=:EP_TestDataUtility.getSalesOrderPositiveScenario().Id];
        Set<id> idSet = new Set<id>{orderObject.EP_Transporter__c};
        Set<string> stringSet = new Set<String>{EP_OrderConstant.OrderState_Draft};
        Test.startTest();
        LIST<Order> result = localObj.getOpenOrderOfTransporters(idSet,stringSet);
        Test.stopTest();
        System.Assert(true,result.size()==1);
    }
    static testMethod void getRecordsByOrderNumber_test() {
        EP_OrderMapper localObj = new EP_OrderMapper();
        Order orderObj = [Select ordernumber from Order where Id=:EP_TestDataUtility.getSalesOrderPositiveScenario().Id];
        Set<String> orderNumberSet = new Set<String>{orderObj.ordernumber};
        Test.startTest();
        LIST<Order> result = localObj.getRecordsByOrderNumber(orderNumberSet);
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
    static testMethod void getOrderListWithStandardOrderItemsDetails_test() {
        EP_OrderMapper localObj = new EP_OrderMapper();
        Set<Id> mOrder = new Set<id>{EP_TestDataUtility.getSalesOrderPositiveScenario().id};
        Test.startTest();
        LIST<Order> result = localObj.getOrderListWithStandardOrderItemsDetails(mOrder);
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    static testMethod void getRecordById_test() {
        EP_OrderMapper localObj = new EP_OrderMapper();
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        Test.startTest();
        Order result = localObj.getRecordById(orderId);
        Test.stopTest();
        System.Assert(result <> null);
    }
    static testMethod void getOrderPricing_test() {
        EP_OrderMapper localObj = new EP_OrderMapper();
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        Test.startTest();
        Order result = localObj.getOrderPricing(orderId);
        Test.stopTest();
        System.Assert(result <> null);
    }
    /*static testMethod void getBulkOrdersForAccount_test() {
        EP_OrderMapper localObj = new EP_OrderMapper();
        Id strAccountId= EP_TestDataUtility.getSalesOrderPositiveScenario().AccountId__c;
        Test.startTest();
        List<Order> result = localObj.getBulkOrdersForAccount(strAccountId);
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    static testMethod void getOrderByAccountAndProductCategory_test() {
        EP_OrderMapper localObj = new EP_OrderMapper();
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        Order OrderObj = [SELECT Id,AccountId,EP_Order_Product_Category__c ,Status FROM Order WHERE ID =: orderId ];
        Test.startTest();
        List<Order> result = localObj.getOrderByAccountAndProductCategory(OrderObj.AccountId,OrderObj.EP_Order_Product_Category__c ,new Set<String>{OrderObj.Status});
        Test.stopTest();
        System.Assert(result.size() == 0);
    }
    static testMethod void getOrderMapByOrderNumber_test() {
        EP_OrderMapper localObj = new EP_OrderMapper();
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        Order OrderObj = [SELECT Id,OrderNumber FROM Order WHERE ID =: orderId ];
        Test.startTest();
        map<string, Order> result = localObj.getOrderMapByOrderNumber(new set<string>{OrderObj.OrderNumber});
        Test.stopTest();
        System.Assert(result.size() == 1);
    }
    static testMethod void getOrderSumAmountbyBillTo_test() {
        EP_OrderMapper localObj = new EP_OrderMapper();
        csord__Order__c OrderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
        Test.startTest();
        List<AggregateResult> result = localObj.getCSOrderSumAmountbyBillTo(OrderObj);
        Test.stopTest();
        System.Assert(result.size() == 0);
    }
    static testMethod void getOrderWithStandardItems_test() {
        EP_OrderMapper localObj = new EP_OrderMapper();
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        csord__Order__c OrderObj = [SELECT Id,OrderNumber__c FROM csord__Order__c WHERE ID =: orderId ];
        Test.startTest();
        map<string, csord__Order__c> result = localObj.getCSOrderWithStandardItems(new set<string>{OrderObj.OrderNumber__c});
        Test.stopTest();
        System.Assert(result.size() == 1);
    }*/
    
}