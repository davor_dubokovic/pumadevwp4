/****************************************************************
* @author       Accenture                                       *
* @name         EP_Account_Notification_Settings_Hlpr           *
* @Created Date 21/12/2017                                      *
* @description  Class to show Notifications Data                *
****************************************************************/
public with sharing class EP_Account_Notification_Settings_Hlpr {
    private EP_Account_Notification_Settings_Context ctx;
    
/****************************************************************
* @author       Accenture                                       *
* @name         EP_Account_Notification_Settings_Hlpr           *
* @description  Constructor                                     *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public EP_Account_Notification_Settings_Hlpr(EP_Account_Notification_Settings_Context ctx) {
        this.ctx = ctx;
        getDefaultAccountNotificationSetting();
        folderId();
    }

/****************************************************************
* @author       Accenture                                       *
* @name         getDefaultAccountNotificationSetting            *
* @description  method to get all account notification data     *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    @testVisible private void getDefaultAccountNotificationSetting(){
        EP_GeneralUtility.Log('private','EP_Account_Notification_Settings_Hlpr','getDefaultAccountNotificationSetting');
        setIds();
        setMapData();
        for(EP_Notification_Account_Settings__c notifyObj : ctx.accNotifyList){
            ctx.wrapperObj = new EP_Account_Notification_Settings_Context.notificationAccountWrapper(notifyObj,ctx.mapIdName);
            ctx.notificationAccountList.add(ctx.wrapperObj);
        }
    }

/****************************************************************
* @author       Accenture                                       *
* @name         folderId                                        *
* @description  method to get folder id                         *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    private void folderId(){
        EP_GeneralUtility.Log('private','EP_Account_Notification_Settings_Hlpr','folderId');
        ctx.folderId = EP_Account_Notification_SettingsMapper.getFolderId(system.label.EP_Folder_Name);
    }

/****************************************************************
* @author       Accenture                                       *
* @name         setIds                                          *
* @description  method to put ids in set                        *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    @testVisible private void setIds(){
        EP_GeneralUtility.Log('private','EP_Account_Notification_Settings_Hlpr','setIds');
        ctx.accNotifyList = EP_Account_Notification_SettingsMapper.queryAccountNotificationSettings(ctx.account.id);
        for(EP_Notification_Account_Settings__c notifyObj : ctx.accNotifyList){
            ctx.templateContactIdSet.add(notifyObj.EP_Notification_Template_Contact__c);
            ctx.templateContactIdSet.add(notifyObj.EP_Notification_Template_User__c);
            ctx.templateContactIdSet.add(notifyObj.EP_Recipient_Contact__c);
            ctx.templateContactIdSet.add(notifyObj.EP_Recipient_User__c);
        }
    }

/****************************************************************
* @author       Accenture                                       *
* @name         setMapData                                      *
* @description  method to put data in map                       *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    @testVisible private void setMapData(){
        EP_GeneralUtility.Log('private','EP_Account_Notification_Settings_Hlpr','setMapData');
        EP_Account_Notification_SettingsMapper.getNameFromId(ctx.templateContactIdSet,system.label.EP_EmailTemplate,ctx.mapIdName);
        EP_Account_Notification_SettingsMapper.getNameFromId(ctx.templateContactIdSet,system.label.EP_Contact,ctx.mapIdName);
        EP_Account_Notification_SettingsMapper.getNameFromId(ctx.templateContactIdSet,system.label.EP_User,ctx.mapIdName);
    }

/****************************************************************
* @author       Accenture                                       *
* @name         Save                                            *
* @description  method to update account notification data      *
* @param        NA                                              *
* @return       pagereference                                   *
****************************************************************/
    public pageReference save(){
        EP_GeneralUtility.Log('public','EP_Account_Notification_Settings_Hlpr','save');
        pageReference redirectPage = null;
        ctx.accNotifyList = new List<EP_Notification_Account_Settings__c>();
        for(EP_Account_Notification_Settings_Context.notificationAccountWrapper wrapperObj : ctx.notificationAccountList){
            system.debug('wrapperObj.notifyAcc.EP_Recipient_User__c = '+wrapperObj.notifyAcc.EP_Recipient_User__c);
            system.debug('wrapperObj.inputHiddenUser = '+wrapperObj.inputHiddenUser);
            wrapperObj.notifyAcc.EP_Notification_Template_Contact__c = wrapperObj.inputHiddenContactTemplate;
            wrapperObj.notifyAcc.EP_Notification_Template_User__c = wrapperObj.inputHiddenUserTemplate;
            if(String.isBlank(wrapperObj.inputHiddenContact))
                wrapperObj.inputHiddenContact = null;
                wrapperObj.notifyAcc.EP_Recipient_Contact__c = wrapperObj.inputHiddenContact;
            if(String.isBlank(wrapperObj.inputHiddenUser))
                wrapperObj.inputHiddenUser = null;
                wrapperObj.notifyAcc.EP_Recipient_User__c = wrapperObj.inputHiddenUser;
            ctx.accNotifyList.add(wrapperObj.notifyAcc);
        }
        update ctx.accNotifyList;
        redirectPage = new PageReference(EP_Common_Constant.SLASH+ctx.account.id);
        redirectPage.setRedirect(true);
        return redirectPage;
    }
}