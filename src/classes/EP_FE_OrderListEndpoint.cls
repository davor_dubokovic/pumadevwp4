/* 
  @Author <Ram Rai>
   @name <EP_FE_OrderListEndpoint>
   @CreateDate <06/04/2016>
   @Description <This class >  
   @Version <1.0>
*/
@RestResource(urlMapping='/FE/V1/order/list/*')                                                            
global with sharing class EP_FE_OrderListEndpoint {

    // Parameters keys
    global final static String PARAM_KEY_OFFSET = 'offset';
    global final static String PARAM_KEY_LIMIT = 'limit';
    global final static String PARAM_KEY_TYPE = 'type';
     global final static String PARAM_KEY_SELLTO = 'selectedSellToId';

    // Feasible Types
    global final static String TYPE_ACTIVE = 'active';
    global final static String TYPE_PAST = 'past';
    global final static String TYPE_ALERT = 'alert';
    global final static String TYPE_BLANKET = 'blanket';

    global final static String TYPE_ORDER = 'ORDERTYPE';
    global final static String TYPE_STATUS = 'STATUS';
    global final static String QUERYLIMITS = 'QUERYLIMIT';    
    global final static String ORDER_ENDPOINT = 'EP_FE_OrderListEndpoint';
    global final static String ORDER_ENDPOINT_RESPONSE = 'EP_FE_OrderListResponse';
    global final static String API_195 = 'PE-195';
    global final static String API_ERROR = 'ERROR';
    public final static String CLASS_NAME= 'EP_FE_OrderListEndpoint';
    public final static String METHD_NAME = 'getOrderList';
    public final static String API_NAME = 'Order List API';    
    public final static String MSSG= 'SOQL Query to fetch the list for pricing';
    
   /*********************************************************************************************
    *@Description : This is the method to return Order List.                  
    *@Params      : void                   
    *@Return      : EP_FE_OrderListResponse                                                                              
    *********************************************************************************************/     
    @HttpGet
    webservice static EP_FE_OrderListResponse getOrderList() {
        // Prepare the output
        EP_FE_OrderListResponse response = new EP_FE_OrderListResponse();

        // Get all the parameters
        Map<String, Object> params = RestContext.request.params;
    system.debug('==params='+ params);
        // Parse the parameters
        Integer queryOffset = params.containsKey(PARAM_KEY_OFFSET) ? Integer.valueOf(params.get(PARAM_KEY_OFFSET)) : 0;
        Integer queryLimit = params.containsKey(PARAM_KEY_LIMIT) ? Integer.valueOf(params.get(PARAM_KEY_LIMIT)) : 50000;
        String orderType = params.containsKey(PARAM_KEY_TYPE) 
            ? (String.valueOf(params.get(PARAM_KEY_TYPE))).toLowerCase() : null;
       
        string sellToid=EP_FE_DataService.getAssociatedAccIDFromUserId();
        
        
        //Get SellTo Id From Portal
        string selectedSellToId  = params.containsKey(PARAM_KEY_SELLTO) 
            ? (String.valueOf(params.get(PARAM_KEY_SELLTO))) : sellToid;

        Set<ID> sellToIds=new Set<ID>();
       //sellToIds.add('0010k00000OhssjAAB');
        sellToIds.add(selectedSellToId);
      
        //Validate the parameters
        if(queryOffset < 0) {
            response.status = EP_FE_OrderListResponse.ERROR_NOT_VALID_OFFSET;
            EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(ORDER_ENDPOINT,ORDER_ENDPOINT_RESPONSE,API_ERROR,
                API_195,TYPE_STATUS));
        } else if(queryLimit < 0 || queryLimit > 50000) {
            response.status = EP_FE_OrderListResponse.ERROR_NOT_VALID_LIMIT;
            EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(ORDER_ENDPOINT,ORDER_ENDPOINT_RESPONSE,API_ERROR,
                API_195,QUERYLIMITS));
        } else if(orderType == null || (!TYPE_ACTIVE.equalsIgnoreCase(orderType) && !TYPE_BLANKET.equalsIgnoreCase(orderType)
                && !TYPE_PAST.equalsIgnoreCase(orderType) && !TYPE_ALERT.equalsIgnoreCase(orderType))) {
            response.status = EP_FE_OrderListResponse.ERROR_NOT_VALID_TYPE;
            EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(ORDER_ENDPOINT,ORDER_ENDPOINT_RESPONSE,API_ERROR,
                API_195,TYPE_ORDER));
        }

        // All the validations have been passed
        else {
            // Query Contracts and the associated blanket orders
            if(TYPE_BLANKET.equalsIgnoreCase(orderType)) {
                // Set the map of contracts
                Map<Id, Contract> blanketContractsMap = fetchBlanketOrders(queryOffset, queryLimit);
                response.blanketContracts = blanketContractsMap.values();
                System.debug('EP_FE_OrderListEndpoint.blanket contracts: ' + blanketContractsMap);

                response.contractBlanketOrderItems = fetchContractBlanketOrderItems(blanketContractsMap.keySet());
                System.debug('EP_FE_OrderListEndpoint.blanket order items: ' + response.contractBlanketOrderItems);
            } 
            // Query he orders
            else {
            
              System.debug('=======response==='+response);
              System.debug('=======orderType==='+orderType);
              System.debug('========queryOffset=='+queryOffset);
              System.debug('========queryLimit=='+queryLimit);
              System.debug('========sellToIds=='+sellToIds);
              
                // Get all the orders with and without pricing information
                Map<Id, csord__Order__c> orderMapWithoutPrice = new Map<Id, csord__Order__c>(fetchOrders(response, orderType, queryOffset, queryLimit, false,sellToIds));
                
                    
                List<csord__Order__c> orderListWithPrice = fetchOrders(response, orderType, queryOffset, queryLimit, true,sellToIds);
              
                // Check which orders are editable / cancellable
                Map<Id, Boolean> editableCancellableOrdersMap = EP_FE_OrderUtils.evaluateCancellableEditableOrders(orderListWithPrice);
                
                // Check which orders can be re-ordered
                Map<Id, Boolean> reOrderMap = EP_FE_OrderUtils.evaluatereorderOrders(orderListWithPrice);

                // Check price visibility per orders
                Map<Id,Boolean> priceVisibilityOrderMap = EP_FE_OrderUtils.evaluatePriceVisibility(orderListWithPrice);

                Map<Id, Account> billToMap = EP_FE_OrderUtils.fetchOrderBillTos(orderListWithPrice);
                
               Map<Id, List<EP_FE_OrderListResponse.EP_FE_Attachment>> mapOrderAttachment=EP_FE_OrderUtils.getMapOrderAttachment(orderListWithPrice);
                
                // Loop on the list with price info and then delegate 
                response.orderList = new List<EP_FE_OrderListResponse.EP_FE_OrderWrapper>();
                for(csord__Order__c order : orderListWithPrice) {
                    response.orderList.add(
                        new EP_FE_OrderListResponse.EP_FE_OrderWrapper(
                            orderMapWithoutPrice.get(order.Id),
                            editableCancellableOrdersMap.get(order.Id), 
                            priceVisibilityOrderMap.get(order.Id),
                            reOrderMap.get(order.Id),
                            EP_FE_OrderUtils.getOrderProductData(order, priceVisibilityOrderMap.get(order.Id)),
                            order.EP_Bill_To__c != null ? billToMap.get(order.EP_Bill_To__c) : null
                          ,mapOrderAttachment.get(order.Id)
                        )                           
                    );
                   
                }
               // response.orderList.
                
            }
        }
       
        return response;
    }
    
   /*********************************************************************************************
    *@Description : This is the method to return Active query.                  
    *@Params      : Integer,Integer,Boolean                   
    *@Return      : String                                                                             
    *********************************************************************************************/ 
    public static String getActiveQuery(Integer queryOffset, Integer queryLimit, Boolean isActive, Boolean canSeePrices,Set<ID> sellToIds) {
    system.debug('#### ' + isActive + ' ' + canSeePrices);
        return getQueryFields(canSeePrices)
            + getActiveQueryBody(isActive,sellToIds) 
            + getQueryOrder()
            + getQueryPage(queryOffset, queryLimit);
    }
   
    
   /*********************************************************************************************
    *@Description : This is the method to return Alert query.                  
    *@Params      : Integer,Integer
    *@Return      : String                                                                             
    *********************************************************************************************/ 
    public static String getAlertQuery(Integer queryOffset, Integer queryLimit, Boolean canSeePrices,Set<ID> sellToIds) {
        return getQueryFields(canSeePrices)
            + getAlertQueryBody(sellToIds) 
            + getQueryOrder()
            + getQueryPage(queryOffset, queryLimit);
    }

    /*********************************************************************************************
    *@Description : This is the method to return Alert query.                  
    *@Params      : Integer,Integer
    *@Return      : String                                                                             
    *********************************************************************************************/ 
    public static String getAccountSummaryQuery(Integer queryOffset, Integer queryLimit, Boolean canSeePrices,Set<ID> sellToIds) {
        return getQueryFields(canSeePrices)
            + getAccountSummaryQueryBody(sellToIds) 
            + getAccountSummaryQueryOrder()
            + getQueryPage(queryOffset, queryLimit);
    }

   /*********************************************************************************************
    *@Description : This is the method to return query.                  
    *@Params      : void
    *@Return      : String                                                                             
    *********************************************************************************************/ 
    public static String getQueryFields(Boolean canSeePrices) {
        system.debug('>>>> CAN SEE PRICES :' + canSeePrices);
        return canSeePrices
            ? 'SELECT EP_Expected_Delivery_Date__c,csord__Account__r.BillingStreet,csord__Account__r.billingcity,csord__Account__r.billingcountry,csord__Account__r.BillingState,csord__Account__r.billingPostalcode, EP_Requested_Delivery_Date__c, EP_Requested_Delivery_End_Date__c, EP_Bill_To_Account__c, '
            + 'EP_Order_Status_Caption__c, EP_ShipTo__r.ShippingAddress,EP_ShipTo__r.EP_Ship_To_Type__c, Type__c, EP_Bill_To__c, '
            + 'EP_ShipTo__r.Name, OrderNumber__c, EffectiveDate__c, csord__Status2__c, EP_ShipTo__r.BillingAddress,EP_Requested_Pickup_Window__c, '
            + 'EP_Delivery_Type__c, EP_Payment_Term_Value__r.Name, EP_Planned_Delivery_Date_Time__c, EP_Actual_Delivery_Date_Time__c, EP_Is_Pre_Pay__c, '
            + 'TotalAmount__c,EP_Customer_Reference_Number__c, Stock_Holding_Location__r.EP_Ship_To__r.Name, Stock_Holding_Location__r.EP_Trip_Duration__c, '
            + 'EP_Estimated_Delivery_Date_Time__c, EP_Order_Customer_Status__c, EP_Order_With_Alert__c, EP_Reason_For_Change__c, EP_Other_Reason__c,  '
            + 'AccountId__c, csord__Account__c,EP_Sell_To__c, EP_Payment_Term_Value_Code__c,EP_Purchase_Contract_Info__r.ContractNumber,CurrencyIsoCode,EP_Is_Order_Active__c,EP_Is_Portal_Order_Active__c, '
            + 'Stock_Holding_Location__r.Stock_Holding_Location__r.EP_Scheduling_Hours__c, EP_Order_Delivery_Type__c, EP_Order_Load_Code__c, EP_Order_Comments__c,  '
            + 'Stock_Holding_Location__r.Stock_Holding_Location__r.Name, Stock_Holding_Location__r.Stock_Holding_Location__r.EP_Slotting_Enabled__c, '
            +'(SELECT Id,Name FROM Attachments), '
                + '( '
                + 'SELECT EP_Product__r.EP_Unit_of_Measure__c, EP_Total_Price__c, OrderItemNumber__c,ListPrice__c, CurrencyIsoCode, EP_Unit__c, Quantity__c, EP_ChargeType__c, '
                + 'EP_Quantity_UOM__c, Product__r.Name,EP_Product__r.Name,UnitPrice__c, EP_Is_Freight_Price__c, Product__r.EP_Product_Sold_As__c, '
                + 'Product__r.EP_Unit_of_Measure__c,  EP_Is_Taxes__c, EP_Parent_Order_Line_Item__c, EP_Tank__c, LastModifiedDate, '
                + 'EP_Ambient_Delivered_Quantity__c, EP_Ambient_Loaded_Quantity__c, EP_Standard_Delivered_Quantity__c, EP_Standard_Loaded_Quantity__c '
                + 'FROM csord__Order_Line_Items__r '
                + ') '
            : 'SELECT EP_Expected_Delivery_Date__c,csord__Account__r.BillingStreet,csord__Account__r.billingcity,csord__Account__r.billingcountry,csord__Account__r.BillingState,csord__Account__r.billingPostalcode,BillingCity__c,BillingCountry__c,csord__Identification__c, EP_Requested_Delivery_Date__c, EP_Requested_Delivery_End_Date__c, EP_Bill_To_Account__c,EP_Order_Product_Category__c, '
            + 'EP_Order_Status_Caption__c, EP_ShipTo__r.ShippingAddress,EP_ShipTo__r.EP_Ship_To_Type__c, Type__c, EP_Bill_To__c, '
            + 'EP_ShipTo__r.Name, OrderNumber__c, EffectiveDate__c, csord__Status2__c, EP_ShipTo__r.BillingAddress,EP_Requested_Pickup_Window__c, '
            + 'EP_Delivery_Type__c, EP_Planned_Delivery_Date_Time__c, EP_Actual_Delivery_Date_Time__c, EP_Is_Pre_Pay__c, '
            + 'TotalAmount__c,EP_Customer_Reference_Number__c, Stock_Holding_Location__r.EP_Ship_To__r.Name, Stock_Holding_Location__r.EP_Trip_Duration__c, '
            + 'EP_Estimated_Delivery_Date_Time__c, EP_Order_Customer_Status__c, EP_Order_With_Alert__c, EP_Reason_For_Change__c, EP_Other_Reason__c,  '
            + 'AccountId__c,csord__Account__c, EP_Sell_To__c,EP_Purchase_Contract_Info__r.ContractNumber,CurrencyIsoCode,EP_Is_Order_Active__c,EP_Is_Portal_Order_Active__c, '
            + 'Stock_Holding_Location__r.Stock_Holding_Location__r.EP_Scheduling_Hours__c, EP_Order_Delivery_Type__c,EP_Order_Load_Code__c, EP_Order_Comments__c, '
            + 'Stock_Holding_Location__r.Stock_Holding_Location__r.Name, Stock_Holding_Location__r.Stock_Holding_Location__r.EP_Slotting_Enabled__c, '
            +'(SELECT Id,Name FROM Attachments), '
                + '( '
                + 'SELECT OrderItemNumber__c,EP_Product__r.EP_Unit_of_Measure__c, CurrencyIsoCode, EP_Product__r.Name, EP_Unit__c,ListPrice__c, Quantity__c, EP_ChargeType__c, '
                + 'EP_Quantity_UOM__c, Product__r.Name, EP_Is_Freight_Price__c, Product__r.EP_Product_Sold_As__c, '
                + 'Product__r.EP_Unit_of_Measure__c,  EP_Is_Taxes__c, EP_Parent_Order_Line_Item__c, EP_Tank__c, LastModifiedDate, '
                + 'EP_Ambient_Delivered_Quantity__c, EP_Ambient_Loaded_Quantity__c, EP_Standard_Delivered_Quantity__c, EP_Standard_Loaded_Quantity__c '
                + 'FROM csord__Order_Line_Items__r '
                + ') ';
    }

   /*********************************************************************************************
    *@Description : This is the method to return Active query.                  
    *@Params      : Boolean
    *@Return      : String                                                                             
    **********************************************************************************************/
    public static String getActiveQueryBody(Boolean isActive,Set<ID> sellToIds) {
        
        Set<ID> accntIDs=new  Set<ID>();    
        
        string strAccountIds=IDsToStringConverter(sellToIds);
        
        
        string portalSellToId=(new list<ID>(sellToIds) )[0] ;
        Set<ID> ShipToaccntIDs=EP_FE_DataService.getAccountIds('SHIPTO',portalSellToId, 'OrderList');
        string strShipToIds=IDsToStringConverter(ShipToaccntIDs);
       
        
        
        String condition =  '';
        if(isActive){
            //in order to show the draft orders on active orders page
            condition = ' EP_Purchase_Contract_Info__c = NULL AND ( EP_Is_Portal_Order_Active__c = ' + isActive + ') ';
                        //+ ' OR (csord__Status2__c = \'' + EP_Common_Constant.ORDER_DRAFT_STATUS + '\' AND  EP_Order_Customer_Status__c = \'Ordered\' ) )';
        } else {
            //this condition is for past orders
            //condition = ' EP_Is_Portal_Order_Active__c = ' + isActive + ' AND Status__c != \'' + EP_Common_Constant.ORDER_DRAFT_STATUS + '\'';
            condition = ' EP_Is_Portal_Order_Active__c = ' + isActive + ' AND csord__Status2__c NOT IN (\'' + EP_Common_Constant.ORDER_DRAFT_STATUS + '\', \'' + EP_FE_Constants.ORDER_DELETED_STATUS + '\')' ;
        }
        return ' FROM csord__Order__c WHERE '
            + condition
            + ' AND EP_Delivery_Type__c IN (\'' + EP_Common_Constant.EX_RACK + '\', \'' + EP_Common_Constant.DELIVERY + '\') '
            + ' AND RecordType.DeveloperName != \'' + EP_FE_Constants.ORDER_RECORD_TYPE_CONSUMPTION + '\' '
           // + ' AND EP_Sell_To__c IN (' + strAccountIds + ')  ';
           + ' AND ((csord__Account__c   IN (' + strAccountIds + ') AND EP_ShipTo__c=NULL) OR (csord__Account__c IN (' + strAccountIds + ') AND EP_ShipTo__c IN(' + strShipToIds + ')) ) ';
           
    }
    
    /*********************************************************************************************
    *@Description : This is the method to return string of comma seperated Set of IDs.                  
    *@Params      : Set<Id>
    *@Return      : String                                                                             
    **********************************************************************************************/
    public static string IDsToStringConverter(Set<Id> sId){
        string straccIDs='';
        for(ID item: sId) {
            
            straccIDs= straccIDs +'\''+String.valueOf(item)+ '\'' + ',';
        }
        straccIDs=straccIDs.subString(0,straccIDs.length()-1);
        system.debug('==straccIDs='+ straccIDs);       
        
        return straccIDs;
    }
   

    /*********************************************************************************************
    *@Description : This is the method to return Active query.                  
    *@Params      : Boolean
    *@Return      : String                                                                             
    **********************************************************************************************/
    public static String getAccountSummaryQueryBody(Set<ID> sellToIds) {
          Set<ID> accntIDs=new  Set<ID>();    
        
        string strAccountIds=IDsToStringConverter(sellToIds);
        
        string portalSellToId=(new list<ID>(sellToIds) )[0] ;
        Set<ID> ShipToaccntIDs=EP_FE_DataService.getAccountIds('SHIPTO',portalSellToId, 'OrderList');
        string strShipToIds=IDsToStringConverter(ShipToaccntIDs);
       

        return ' FROM csord__Order__c '
            + ' WHERE EP_Purchase_Contract_Info__c = NULL '
            /*+ ' AND Status__c NOT IN (\'' + EP_Common_Constant.ORDER_CANCELLED_STATUS + '\', \''
            + EP_Common_Constant.ORDER_DRAFT_STATUS +'\', \''+ EP_FE_Constants.ORDER_QUOTE_STATUS+'\') '*/
            + 'AND EP_Delivery_Type__c IN (\'' + EP_Common_Constant.EX_RACK + '\', \'' + EP_Common_Constant.DELIVERY + '\') '
            + ' AND RecordType.DeveloperName != \'' + EP_FE_Constants.ORDER_RECORD_TYPE_CONSUMPTION + '\' '  
         //+ ' AND EP_Sell_To__c IN (' + strAccountIds + ') ';
       + ' AND ((csord__Account__c  IN (' + strAccountIds + ') AND EP_ShipTo__c=NULL) OR (csord__Account__c IN (' + strAccountIds + ') AND EP_ShipTo__c IN(' + strShipToIds + ')) ) ';
    }

   /*********************************************************************************************
    *@Description : This is the method to return Alert query.                  
    *@Params      : void
    *@Return      : String                                                                             
    *********************************************************************************************/ 
    public static String getAlertQueryBody(Set<ID> sellToIds) {
         Set<ID> accntIDs=new  Set<ID>();    
       
       string strAccountIds=IDsToStringConverter(sellToIds);
        
        string portalSellToId=(new list<ID>(sellToIds) )[0] ;
        Set<ID> ShipToaccntIDs=EP_FE_DataService.getAccountIds('SHIPTO',portalSellToId, 'OrderList');
        string strShipToIds=IDsToStringConverter(ShipToaccntIDs);
       
        
        
        return ' FROM csord__Order__c '
            + ' WHERE ( EP_Is_Portal_Order_Active__c = true AND EP_Order_With_Alert__c = true ) '
            + ' AND EP_Delivery_Type__c IN (\'' + EP_Common_Constant.EX_RACK + '\', \'' + EP_Common_Constant.DELIVERY + '\') '
            + ' AND RecordType.DeveloperName != \'' + EP_FE_Constants.ORDER_RECORD_TYPE_CONSUMPTION + '\' '
        // + ' AND EP_Sell_To__c IN (' + strAccountIds + ') ';
        + ' AND ((csord__Account__c   IN (' + strAccountIds + ') AND EP_ShipTo__c=NULL) OR (csord__Account__c IN (' + strAccountIds + ') AND EP_ShipTo__c IN(' + strShipToIds + ')) ) ';
    }

    

   /*********************************************************************************************
    *@Description : This is the method to return query.                  
    *@Params      : void
    *@Return      : String                                                                             
    *********************************************************************************************/ 
    public static String getAccountSummaryQueryOrder() {
        return ' ORDER BY LastModifiedDate DESC ';
    }

    /*********************************************************************************************
    *@Description : This is the method to return query.                  
    *@Params      : void
    *@Return      : String                                                                             
    *********************************************************************************************/ 
    public static String getQueryOrder() {
        return ' ORDER BY EP_Requested_Delivery_Date__c , OrderNumber__c';
    }

   /*********************************************************************************************
    *@Description : This is the method to return query.                  
    *@Params      : Integer,Integer
    *@Return      : String                                                                             
    *********************************************************************************************/ 
    public static String getQueryPage(Integer queryOffset, Integer queryLimit) {
      
        return ' LIMIT ' + queryLimit
            + ' OFFSET ' + queryOffset;
    }

    /*********************************************************************************************
    *@Description : This is the method to return contracts.                  
    *@Return      : List<Contract>                          
    *********************************************************************************************/ 
    private static Map<Id, Contract> fetchBlanketOrders(Integer queryOffset, Integer queryLimit) {
        DateTime nowDT = DateTime.now();

        List<Contract> contractList = Database.query(
              'SELECT Id,AccountId, ActivatedDate,BillingAddress,BillingCity,BillingCountry,BillingPostalCode,BillingState, '
            + 'CompanySignedDate,ContractNumber,CustomerSignedDate,CustomerSignedTitle,EndDate,BillingStreet, ' 
            + 'LastReferencedDate,ShippingAddress,ShippingCity,ShippingCountry,ShippingPostalCode,ShippingState,ShippingStreet, '
            + 'StartDate,Status,StatusCode, '
            + '('
                + 'SELECT EP_Contract_Product_Quantity__c, EP_Product__r.Name, '
                + 'EP_Product__r.EP_Short_Name__c, EP_Unit_of_Measure__c '
                + 'FROM EP_Contract_Items__r '
            + ')'
            + getBlanketOrderdContractBody()
            + ' ORDER BY LastReferencedDate DESC '
            + ' LIMIT ' + queryLimit
            + ' OFFSET ' + queryOffset
        );
        
        Map<Id, Contract> contractMap = new Map<Id, Contract>(contractList);
        return contractMap;
    }

    /*********************************************************************************************
    *@Description : This is the method to return the body of the query to extract contracts.                  
    *@Return      : List<Contract>                          
    *********************************************************************************************/ 
    public static String getBlanketOrderdContractBody() {
        return 'FROM Contract '
            + 'WHERE IsDeleted = false '
            + 'AND StartDate <= TODAY AND EndDate > TODAY AND ActivatedDate <= :nowDT '
            + 'AND Status = \'' + EP_Common_Constant.EP_CONTRACT_STATUS_ACTIVE + '\' ';
    }

    /*********************************************************************************************
    * @Description : This is the method to return the body of the query to extract contracts.                  
    * @Return      : Map<Id, OrderItem>                          
    *********************************************************************************************/ 
    private static Map<String, List<csord__Order_Line_Item__c>> fetchContractBlanketOrderItems(Set<Id> blanketContractIds) {
        List<csord__Order_Line_Item__c> orderItems = [SELECT Id, EP_Unit_of_Measure__c, EP_Unit__c, EP_Quantity_UOM__c, EP_Product_Code__c, 
            OrderItemNumber__c, Quantity__c, csord__Line_Number__c, EP_Contract__r.Id, csord__Order__r.EP_Actual_Delivery_Date_Time__c,
            csord__Order__r.OrderNumber__c, csord__Order__r.EP_Is_Pre_Pay__c, Product__r.Name, Product__r.EP_Unit_of_Measure__c
            FROM csord__Order_Line_Item__c
            WHERE EP_Contract__r.Id IN :blanketContractIds AND csord__Order__r.csord__Status2__c != :EP_Common_Constant.STATUS_CANCELLED
            AND EP_Is_Taxes__c = false AND EP_Is_Freight_Price__c = false
            ORDER BY csord__Order__r.EP_Actual_Delivery_Date_Time__c DESC, Product__r.Name ASC
            LIMIT 1000];

        Map<String, List<csord__Order_Line_Item__c>> contractBlanketOrderItemsMap = new Map<String, List<csord__Order_Line_Item__c>>();
        for(Id blanketContractId : blanketContractIds) {
            contractBlanketOrderItemsMap.put(String.valueOf(blanketContractId), new List<csord__Order_Line_Item__c>());
        }

        for(csord__Order_Line_Item__c orderItem : orderItems) {
            contractBlanketOrderItemsMap.get(String.valueOf(orderItem.EP_Contract__r.Id)).add(orderItem);
        }

        return contractBlanketOrderItemsMap;
    }

    /*********************************************************************************************
    *@Description : Return the list of Orders
    *@Return      : List<Order>
    *********************************************************************************************/ 
    public static List<csord__Order__c> fetchOrders(EP_FE_OrderListResponse response, String orderType, Integer queryOffset, Integer queryLimit,Set<ID> sellToIds) {
        return fetchOrders(response, orderType, queryOffset, queryLimit, 
            currentUserCanSeePrices(response, orderType, orderType.equalsIgnoreCase(TYPE_ACTIVE), queryOffset, queryLimit,sellToIds),sellToIds);
    }
 /*********************************************************************************************
    *@Description : Return the list of Orders without pricing information
    *@Return      : List<Order>
    *********************************************************************************************/ 
    public static List<csord__Order__c> fetchOrders(EP_FE_OrderListResponse response, String orderType, Integer queryOffset, Integer queryLimit, Boolean canSeePrices,Set<ID> sellToIds) {
       
       
       System.debug('======response====>'+response);
       System.debug('======orderType====>'+orderType);
       System.debug('======queryOffset====>'+queryOffset);
       System.debug('======queryLimit====>'+queryLimit);
       System.debug('======canSeePrices====>'+canSeePrices);
        List<csord__Order__c> orderList;
        String query = null;
        Boolean isActive = orderType.equalsIgnoreCase(TYPE_ACTIVE);
        try {
            query = orderType.equals(TYPE_ALERT) 
                    ? getAlertQuery(queryOffset, queryLimit, canSeePrices,sellToIds) 
                    : getActiveQuery(queryOffset, queryLimit, isActive, canSeePrices,sellToIds);
            System.debug('EP_FE_OrderListEndpoint.fetchOrdersNoPrice: Query ' + query);
            orderList = Database.query(query);            
            
            System.debug('=======UserLanguage==>' + UserInfo.getLanguage()) ;
            Map<String,String> mEngstrVsSpnstr = new Map<String,String>();
            
            for(EP_FE_Order_Status_Lang_Translator__mdt OrderStatus : [SELECT EP_FE_Default__c,EP_FE_Converted__c,EP_FE_Language_Status_key__c FROM EP_FE_Order_Status_Lang_Translator__mdt]){
                 mEngstrVsSpnstr.put(OrderStatus.EP_FE_Language_Status_key__c,OrderStatus.EP_FE_Converted__c);            
            }
            
            for(csord__Order__c ordItem : orderList){
                String UsrLangVsOrderStatusCaptionStr;
                
                If(ordItem.EP_Order_Status_Caption__c != NUll){
                    UsrLangVsOrderStatusCaptionStr = UserInfo.getLanguage()+'-'+ordItem.EP_Order_Status_Caption__c;
                }
                else
                {            
                    UsrLangVsOrderStatusCaptionStr = UserInfo.getLanguage()+'-'+'Draft';               
                }
                If(mEngstrVsSpnstr.Containskey(UsrLangVsOrderStatusCaptionStr)){                 
                    ordItem.EP_Order_Status_Caption__c = mEngstrVsSpnstr.get(UsrLangVsOrderStatusCaptionStr);                                
                }        
                
                String UsrLangVsOrderCustStatusStr;    
                
                If(ordItem.EP_Order_Customer_Status__c != NUll){
                    UsrLangVsOrderCustStatusStr = UserInfo.getLanguage()+'-'+ordItem.EP_Order_Customer_Status__c;
                }
                else
                {            
                    UsrLangVsOrderCustStatusStr = UserInfo.getLanguage()+'-'+'Draft';               
                }
                
                If(mEngstrVsSpnstr.Containskey(UsrLangVsOrderCustStatusStr)){                 
                    ordItem.EP_Order_Customer_Status__c = mEngstrVsSpnstr.get(UsrLangVsOrderCustStatusStr); 
                    System.debug('=========ordItem.EP_Order_Customer_Status__c===1===='+ordItem.EP_Order_Customer_Status__c);                                
                }                      
            }
            
             System.debug('======orderList====>'+orderList);
            // PR3 Defect : 28939  start
            for(csord__Order__c ordItem : orderList){
                if(ordItem.EP_Order_Status_Caption__c != null && ordItem.EP_Order_Status_Caption__c.contains('[')){
                    String captionDate = ordItem.EP_Order_Status_Caption__c.substringBetween('[',']');
                    String datelocaleFormat = EP_FE_Utils.getUserDateTimeFormat();
                    DateTime dt = DateTime.newInstance(Integer.valueof(captionDate.substring(0,4)),Integer.valueof(captionDate.substring(4,6)),Integer.valueof(captionDate.substring(6,8)),Integer.valueof(captionDate.substring(8,10)),Integer.valueof(captionDate.substring(10,12)),00);
                    String newCaptionDate = dt.format(datelocaleFormat);
                    String oldCaptiondate = '['+captionDate+']';
                    String statusCaption = ordItem.EP_Order_Status_Caption__c.replace(oldCaptiondate,newCaptionDate);
                    ordItem.EP_Order_Status_Caption__c = statusCaption;

                }
            }
            // PR3 Defect : 28939  End

           
           
            
            EP_FE_Utils.raiseExceptionIfTestIsRunning();
        } catch(Exception e) {
            EP_FE_Utils.logError(CLASS_NAME, METHD_NAME , API_NAME, MSSG, 
                query, e, EP_FE_OrderListResponse.ERROR_FETCHING_ORDER_LIST, response);
        }

   System.debug('======orderList====>'+orderList);
        return orderList;
    }


    /*********************************************************************************************
    *@Description : Return a Boolean to say if the user can see prices
    *@Return      : Boolean
    *********************************************************************************************/ 
    public static Boolean currentUserCanSeePrices(EP_FE_Response response, String orderType, Boolean isActive, 
            Integer queryOffset, Integer queryLimit,Set<ID> sellToIds) {
             System.debug('=====In====');
        // Check price visibility: 
        // assuming all orders are associated to the single Sell-To the user can see, 
        // there will be a single Bill-To, shared across all orders
        String priceQuery = null;
        List<csord__Order__c> orderList = null;
        try {
            priceQuery = TYPE_ALERT.equalsIgnoreCase(orderType) 
                    ? getAlertQuery(queryOffset, 1, false,sellToIds) 
                    : getActiveQuery(queryOffset, 1, isActive, false,sellToIds);
                    System.debug('=====priceQuery===='+priceQuery);
            orderList = Database.query(priceQuery);
            EP_FE_Utils.raiseExceptionIfTestIsRunning();
        } catch(Exception e) {
            EP_FE_Utils.logError(CLASS_NAME, METHD_NAME , API_NAME, MSSG, 
                priceQuery, e, EP_FE_OrderListResponse.ERROR_FETCHING_ORDER_LIST, response);
        }
        Boolean canSeePrices = false;
        System.debug('currentUserCanSeePrices: orderList ' + orderList);
                
        if(orderList != null && orderList.size() == 1) {
            csord__Order__c orderForPricing = orderList.get(0);
            system.debug('order ' + orderForPricing.Id);
            canSeePrices = EP_FE_OrderUtils.evaluatePriceVisibility(orderForPricing);
        }

        return canSeePrices;
    }

 }