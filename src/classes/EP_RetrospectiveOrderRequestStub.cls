/*
   @Author 			CS
   @name 			EP_RetrospectiveOrderRequestStub
   @CreateDate 		27/04/2018
   @Description		Retrospective Order Request stub
   @Version 		1.0
*/
public class EP_RetrospectiveOrderRequestStub {

    /* Class for MSG */
    public MSG msg;
    
    /* MSG stub class*/ 
    public class MSG {

        public HeaderCommon headerCommon { get; set; }
        public Payload payload { get; set; }
        public string statusPayload { get; set; }
    }
    
    /* HeaderCommon stub class*/    
    public class HeaderCommon extends EP_MessageHeader {}

    public class Payload {

        public Any0 any0 { get; set; }
    }

    public class Any0 {

        public Order order { get; set; }
    }

    public class Order {

        public string seqId { get; set; }
        public Identifier identifier { get; set; }
        public string sellToId { get; set; }
        public string shipToId { get; set; }
        public DateTime loadingDt { get; set; }
        public DateTime dlvryDt { get; set; }
        public string deliveryType { get; set; }
        public string orderType { get; set; }
        public string orderOrigination { get; set; }
        public string orderEpoch { get; set; }
        public string totalOrderQty { get; set; }
        public string logisticStockHldngLocId { get; set; }
        public string comment { get; set; }
        public string logisticTransporterCode { get; set; }
        public string tripId { get; set; }
        public string salesContractNr { get; set; }
        public string vehicleId { get; set; }
        public string driverId { get; set; }
        public string driverName { get; set; }
        public string versionNr { get; set; }
        public OrderLines orderLines { get; set; }
        public string clientId { get; set; }
    }

    public class Identifier {

        public string orderId { get; set; }
        public string entrprsId { get; set; }
        public string lineId { get; set; }
    }

    public class OrderLines {

        public List<OrderLine> orderLine { get; set; }
    }

    public class OrderLine {

        public string seqId { get; set; }
        public Identifier identifier { get; set; }
        public string itemId { get; set; }
        public Decimal qty { get; set; }
        public string uom { get; set; }
        public Decimal unitPrice { get; set; }
        public Decimal loadedAmbientQty { get; set; }
        public Decimal loadedStandardQty { get; set; }
        public Decimal deliveredAmbientQty { get; set; }
        public Decimal deliveredStandardQty { get; set; }
        public Bols bols { get; set; }
    }

    public class Bols {

        public List<Bol> bol { get; set; }
    }

    public class Bol {

        public string bolId { get; set; }
        public string qty { get; set; }
        public string contractId { get; set; }
        public string supplierId { get; set; }
    }
}