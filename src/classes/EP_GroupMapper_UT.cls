@isTest
public class EP_GroupMapper_UT
{
    static final String groupName = 'Test Group';
    static testMethod void getGroupMembrRecordsAsPerGrpNames_test() {
        EP_GroupMapper localObj = new EP_GroupMapper();
        Set<String> setGroupName = new Set<String>();
        Group grp = EP_TestDataUtility.createGroup();
        
        Group groupObj = [SELECT Id,Name FROM Group WHERE name =: grp.Name LIMIT 1];
        setGroupName.add(groupObj.Name);
        Test.startTest();
        Group result = localObj.getGroupMembrRecordsAsPerGrpNames(setGroupName);
        Test.stopTest();
        System.AssertNotEquals(null,result);
    }
    
    static testMethod void getGroupByUserRoles_test(){
        EP_GroupMapper localObj = new EP_GroupMapper();
        List<UserRole> setUserRoleId = new List<UserRole>();
        System.debug('test##'+UserInfo.getUserRoleId());
        UserRole r = [SELECT Id FROM UserRole LIMIT 1];
        System.debug('role###'+r);
        User u = [SELECT Id,UserRoleId FROM User WHERE Id =: UserInfo.getUserId()];
        u.UserRoleId = r.Id;
        update u;
        Group grp = EP_TestDataUtility.createGroup();
        System.debug('user id ###'+UserInfo.getUserRoleId());
        setUserRoleId.add(r);
        Test.startTest();
        List<Group> result = localObj.getGroupByUserRoles(setUserRoleId);
        Test.stopTest();
        System.AssertNotEquals(null,result);
    }
}