@isTest
public class EP_VendorASBlocked_UT
{

    static final string EVENT_NAME = '06-BlockedTo06-Blocked';
    static final string INVALID_EVENT_NAME = '08-ProspectTo04-Account Set-up';
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    static testMethod void setAccountDomainObject_test() {
        EP_VendorASBlocked localObj = new EP_VendorASBlocked();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVendorASBlockedDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        EP_AccountDomainObject currentAccount;
        Test.startTest();
        localObj.setAccountDomainObject(currentAccount);
        Test.stopTest();
        System.assert(True);
        //Dummy Assert, this method is calling super class method
    }
    static testMethod void doOnEntry_test() {
        EP_VendorASBlocked localObj = new EP_VendorASBlocked();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVendorASBlockedDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.assert(True);
        //Dummy Assert, No operation performs on this method
    }
    static testMethod void doOnExit_test() {
        EP_VendorASBlocked localObj = new EP_VendorASBlocked();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVendorASBlockedDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        System.assert(True);
        //Dummy Assert, No operation performs on this method
    }
    static testMethod void doTransition_PositiveScenariotest() {
        EP_VendorASBlocked localObj = new EP_VendorASBlocked();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVendorASBlockedDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_VendorASBlocked localObj = new EP_VendorASBlocked();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVendorASBlockedDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        List<Exception> excList = new List<Exception>();
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
         }
        catch(Exception e){
            excList.add(e);
        }
        Test.stopTest();
        System.Assert(excList.size() > 0);
    }
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_VendorASBlocked localObj = new EP_VendorASBlocked();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVendorASBlockedDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
}