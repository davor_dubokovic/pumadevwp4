/*   
     @Author Aravindhan Ramalingam
     @name <EP_OrderStateSubmitted.cls>     
     @Description <Order State for Submitted status, applicable for all order type>   
     @Version <1.1> 
     */

     public class EP_OrderStateSubmitted extends EP_OrderState {

        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder){
            super.setOrderDomainObject(currentOrder);
        }

        
        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_OrderStateSubmitted','doOnEntry');
        }
        
        public override void doOnExit(){
            EP_GeneralUtility.Log('Public','EP_OrderStateSubmitted','doOnExit');
            //Defect Start #57261 - In order modification, this condition is not allowing us to do outbound messages , adding User Edit Condition
            if(!EP_OrderStateSubmitted.getTextValue().equalsIgnoreCase(this.order.previousStatus) ||
            	EP_OrderConstant.USER_EDIT.equalsIgnoreCase(orderEvent.getEventName())){
                this.orderService.doSyncStatusWithNav();    
            }
            //Defect End #57261
        }

        public static String getTextValue()
        {
            return EP_OrderConstant.OrderState_Submitted;
        }
    }