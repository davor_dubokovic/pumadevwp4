@isTest
private class EP_CRM_SystemPermissionUtilityTest {
/*	
    @isTest
    static void testUserHasPermission_Valid_Profile_Missing_PermissionSet_Success() {

        // Get a basic user.
        User u = EP_CRM_SystemPermissionUtilityTest.createUserByProfileName('Standard User');
        
        // Create PermissionSet and assign to test user
        EP_CRM_SystemPermissionUtilityTest.createPermissionSetAndAssignUser(u);

        System.runAs(u) {
            System.assertEquals(false, EP_CRM_SystemPermissionUtility.userHasPermission('PermissionsConvertLeads'));
        }
    }
*/
    @isTest
    static void testUserHasPermission_Valid_Profile_Missing_PermissionSet_Missing() {

        // Get a basic user.
        User u = EP_CRM_SystemPermissionUtilityTest.createUserByProfileName('Standard User');

        System.runAs(u) {
            System.assertEquals(true, EP_CRM_SystemPermissionUtility.userHasPermission('PermissionsConvertLeads'));
        }

    }
/*
    @isTest
    static void testUserHasPermission_Valid_Profile_Success_PermissionSet_Success() {

        // Get a sys admin user.
        User u = EP_CRM_SystemPermissionUtilityTest.createUserByProfileName('System Administrator');
        
        // Create PermissionSet and assign to test user
        EP_CRM_SystemPermissionUtilityTest.createPermissionSetAndAssignUser(u);

        System.runAs(u) {
            System.assertEquals(true, EP_CRM_SystemPermissionUtility.userHasPermission('PermissionsConvertLeads'));
        }
    }
*/
    @isTest
    static void testUserHasPermission_Valid_Profile_Success_PermissionSet_Missing() {

        // Get a sys admin user.
        User u = EP_CRM_SystemPermissionUtilityTest.createUserByProfileName('System Administrator');

        System.runAs(u) {
            System.assertEquals(true, EP_CRM_SystemPermissionUtility.userHasPermission('PermissionsConvertLeads'));
        }
    }

    static void createPermissionSetAndAssignUser(User u) {
        PermissionSet ps = new PermissionSet();
        ps.Name = 'EP_CRM_SystemPermissionUtilityTest';
        ps.Label = 'EP_CRM_SystemPermissionUtilityTest';
        ps.PermissionsAssignTopics = true;
        ps.PermissionsConnectOrgToEnvironmentHub = true;
        ps.PermissionsConvertLeads = true;
        ps.PermissionsCreateCustomizeFilters = true;
        ps.PermissionsCreateTopics = true;
        ps.PermissionsDeleteTopics = true;
        ps.PermissionsEditEvent = true;
        ps.PermissionsEditPublicFilters = true;
        ps.PermissionsEditPublicTemplates = true;
        ps.PermissionsEditTask = true;
        ps.PermissionsEditTopics = true;
        ps.PermissionsImportLeads = true;
        ps.PermissionsManageCategories = true;
        ps.PermissionsManageNetworks = true;
        ps.PermissionsRunReports = true;
        ps.PermissionsSolutionImport = true;
        ps.PermissionsTransferAnyEntity = true;
        ps.PermissionsTransferAnyLead = true;
        ps.PermissionsUseTeamReassignWizards = true;
        ps.PermissionsViewEventLogFiles = true;
        ps.PermissionsViewSetup = true;
        insert ps;

        PermissionSetAssignment psa = new PermissionSetAssignment();
        psa.AssigneeId = u.Id;
        psa.PermissionSetId = ps.Id;
        insert psa;
    }
    
    public static User createUserByProfileName(String profileName){
        Id profileId = [SELECT Id, Name FROM Profile WHERE Name = : profileName limit 1].Id;
        User userObj = new User(alias = 'standt'
            , email='user@testorg.com'
            , emailencodingkey='UTF-8'
            , lastname='Testing'
            , languagelocalekey='en_US'
            , localesidkey='en_US'
            , profileId = profileId
            , timezonesidkey='Europe/London'
            ,EP_CRM_Geo1__c = 'Africa'
			,EP_CRM_Geo2__c = 'Ghana'                   
            , username='user@testorg.com'            
                + EncodingUtil.convertToHex(
                    Crypto.generateDigest('MD5', Blob.valueOf('1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'))))
                  ) + Integer.valueOf(math.rint(math.random()*1000000))
            );
        insert userObj;
        return userObj;        
    }
}