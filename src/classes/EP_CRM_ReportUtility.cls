/* ================================================
 * @Class Name : EP_CRM_ReportUtility 
 * @author : TCS
 * @Purpose: This utility class is used to store all reusable methods for opportunity volume report.
 * @created date: 15/11/2017
 ================================================*/
public with sharing class EP_CRM_ReportUtility {
	/**
	* This method is used to get users in child roles based on parent role 
	*/
    public static Map < Id, User > getChildRoleUsers(Id roleId, Boolean isAllChildUsers) {
        // get requested user's role -- Passed role Id directly to avoid extra query 
        // Id roleId = [select UserRoleId from User where Id =: userId].UserRoleId;
        
        // get all of the roles underneath the user
        Set < Id > allSubRoleIds = getChildRoleIds(new Set < ID > { roleId }, isAllChildUsers);
        
        // get all of the ids for the users in those roles
        return (new Map < Id, User > ([Select Id, Name From User where UserRoleId IN: allSubRoleIds]));
    }

	/**
	* This method is used to get child roles based on parent role
	*/
    private static Set < ID > getChildRoleIds(Set < ID > roleIds, Boolean isAllChildRoles) {
        Set < ID > currentRoleIds = new Set < ID > ();

        // get all of the roles underneath the passed roles
        for (UserRole userRole: [select Id from UserRole where ParentRoleId
                IN: roleIds AND ParentRoleID != null
            ])
        currentRoleIds.add(userRole.Id);
        
        // go fetch some more rolls! if isAllChildRoles is set to true only
        if (currentRoleIds.size() > 0 && isAllChildRoles)
            currentRoleIds.addAll(getChildRoleIds(currentRoleIds, true));
        
        return currentRoleIds;
    }
    
    /**
    * This method is used to get all picklist values of provided object & field name
    */
    public static List < SelectOption > getPickValues(Sobject object_name, String field_name, String first_val) {
	    List < SelectOption > options = new List < SelectOption > ();
	    // If there is a first value being provided
	    if (first_val != null) {
	        // Add the first option	
	        options.add(new SelectOption(first_val, first_val));
	    }
	    // Grab the sobject that was passed
	    Schema.sObjectType sobject_type = object_name.getSObjectType();
	    // Describe the sobject
	    Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
	    // Get a map of fields for the passed sobject
	    Map < String, Schema.SObjectField > field_map = sobject_describe.fields.getMap();
	    // Grab the list of picklist values for the passed field on the sobject
	    List < Schema.PicklistEntry > pick_list_values = field_map.get(field_name).getDescribe().getPickListValues();
	    // Iterate all values in the picklist list 
	    for (Schema.PicklistEntry p: pick_list_values) {
	    	if(p.isActive()) {
	    		// Add the value and label to our final list        
	        	options.add(new SelectOption(p.getValue(), p.getLabel()));	
	    	}
	    }
	    return options;
	}   
	
	/**
	* This method is used to return calculated supplyDate (if Contract StartDate > Opportunity CloseDate then Contract StartDate else Opportunity CloseDate)
	*/
	public static Date getReportTargetDate(Opportunity opportunityObj) {
		Date supplyDate = opportunityObj.closeDate;
		if(opportunityObj.contracts__r != null && opportunityObj.contracts__r.size() > 0) {
			if(opportunityObj.contracts__r[0].StartDate > supplyDate) {
				supplyDate = opportunityObj.contracts__r[0].StartDate;
			}
		}
		return supplyDate;
	}
	
	/**
	* This method is used to get months count to get forcast values
	*/
	public static Integer getForcastMonthCount(Opportunity opportunityObj) {
		Date supplyDate = getReportTargetDate(opportunityObj);
		Date endDate = null;
		if(opportunityObj.contracts__r != null && opportunityObj.contracts__r.size() > 0) {
			endDate = opportunityObj.contracts__r[0].EP_CRM_Contract_Expiry_Date__c;
		}
		if(endDate == null || ((supplyDate.month() + supplyDate.monthsBetween(endDate)) > 12)) {
			return (12 - supplyDate.month() + 1);
		}
		return (endDate.month() - supplyDate.month() + 1);
	}
    
    /**
    * This method is used to get single quoted values in string like IN operator values
    */
    public static String getMultipleSelectedValues(String multiSelectValues) {
    	String selectedValues = '';
    	if(String.isNotBlank(multiSelectValues)) {
    		for(String str: multiSelectValues.split(';')) {
    			selectedValues += ',\'' + str + '\'';
    		}
    		//remove the first comma
			selectedValues = selectedValues.subString(1, selectedValues.length());
    	}
    	return selectedValues;
    }
    
    /**
    * This method is used to get conversion rate value (fromUoM -> toUoM)
    */
    public static Decimal getConvertionRate(String fromUoM, String toUoM) {
    	if(String.isNotBlank(fromUoM)) {
    		if(fromUoM.equalsIgnoreCase(EP_CRM_Constants.LITRE)) {
    			return getLitreConvertionRate(toUoM);
    		} else if(fromUoM.equalsIgnoreCase(EP_CRM_Constants.KILOGRAM)) {
    			return getKilogramConvertionRate(toUoM);
    		} else if(fromUoM.equalsIgnoreCase(EP_CRM_Constants.GALLON)) {
    			return getGallonConvertionRate(toUoM);
    		} else if(fromUoM.equalsIgnoreCase(EP_CRM_Constants.METRIC_TON)) {
    			return getMetricTonConvertionRate(toUoM);
    		} else if(fromUoM.equalsIgnoreCase(EP_CRM_Constants.CUBIC_MTR)) {
    			return getCubesConvertionRate(toUoM);
    		} else {
    			// Converation Not Available
    		}    		
    	}
    	return EP_CRM_Constants.ONE;
    }
    
    /**
    * This method is used to get conversion rate value (LITRE -> toUoM)
    */
    public static Decimal getLitreConvertionRate(String toUoM) {
    	if(String.isNotBlank(toUoM)) {
    		if(toUoM.equalsIgnoreCase(EP_CRM_Constants.KILOGRAM)) {
    			return 1.0;	
    		} else if(toUoM.equalsIgnoreCase(EP_CRM_Constants.GALLON)) {
    			return 0.26417205;
    		} else if(toUoM.equalsIgnoreCase(EP_CRM_Constants.CUBIC_MTR)) {
    			return 0.001;
    		} else {
    			// Converation Not Available
    		}
    	}
    	return EP_CRM_Constants.ONE;
    }
    
    /**
    * This method is used to get conversion rate value (KILOGRAM -> toUoM)
    */
    public static Decimal getKilogramConvertionRate(String toUoM) {
    	if(String.isNotBlank(toUoM)) {
    		if(toUoM.equalsIgnoreCase(EP_CRM_Constants.LITRE)) {
    			return 1.0;	
    		} else if(toUoM.equalsIgnoreCase(EP_CRM_Constants.METRIC_TON)) {
    			return 0.001;
    		} else {
    			// Converation Not Available
    		}
    	}
    	return EP_CRM_Constants.ONE;
    }
    
    /**
    * This method is used to get conversion rate value (METRIC_TON -> toUoM)
    */
    public static Decimal getMetricTonConvertionRate(String toUoM) {
    	if(String.isNotBlank(toUoM)) {
    		if(toUoM.equalsIgnoreCase(EP_CRM_Constants.KILOGRAM)) {
    			return 1000;	
    		} else {
    			// Converation Not Available
    		}
    	}
    	return EP_CRM_Constants.ONE;
    }
    
    /**
    * This method is used to get conversion rate value (GALLON -> toUoM)
    */
    public static Decimal getGallonConvertionRate(String toUoM) {
    	if(String.isNotBlank(toUoM)) {
    		if(toUoM.equalsIgnoreCase(EP_CRM_Constants.LITRE)) {
    			return 3.7854;	 	
    		} else if(toUoM.equalsIgnoreCase(EP_CRM_Constants.KILOGRAM)) {
    			return 3.7854;	 	
    		} else if(toUoM.equalsIgnoreCase(EP_CRM_Constants.METRIC_TON)) {
    			return 0.0038;
    		} else if(toUoM.equalsIgnoreCase(EP_CRM_Constants.CUBIC_MTR)) {
    			return 0.0038;
    		} else {
    			// Converation Not Available
    		}
    	}
    	return EP_CRM_Constants.ONE;
    }
    
    /**
    * This method is used to get conversion rate value (CUBIC_MTR -> toUoM)
    */
    public static Decimal getCubesConvertionRate(String toUoM) {
    	if(String.isNotBlank(toUoM)) {
    		if(toUoM.equalsIgnoreCase(EP_CRM_Constants.LITRE)) {
    			return 1000;	 	
    		} else if(toUoM.equalsIgnoreCase(EP_CRM_Constants.GALLON)) {
    			return 264.172052;
    		} else {
    			// Converation Not Available
    		}
    	}
    	return EP_CRM_Constants.ONE;
    }
}