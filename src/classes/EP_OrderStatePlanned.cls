/*   
     @Author Aravindhan Ramalingam
     @name <EP_OrderStatePlanned.cls>     
     @Description <Order State for Planned status>   
     @Version <1.1> 
     */

     public class EP_OrderStatePlanned extends EP_OrderState {
        
        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            super.setOrderDomainObject(currentOrder);
        }

        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_OrderStatePlanned','doOnEntry');
            
        }
        public static String getTextValue()
        {
            return EP_OrderConstant.OrderState_Planned;
        }
    }