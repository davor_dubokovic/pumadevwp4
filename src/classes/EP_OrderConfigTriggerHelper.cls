/**
 * @author <Accenture>
 * @name <EP_OrderConfigTriggerHelper>
 * @createDate <21/01/2016>
 * @description <This class handles requests from  EP_OrderConfigTriggerHandler> 
 * @version <1.0>
 */
 public with sharing class EP_OrderConfigTriggerHelper{
    
    private static final string VALIDATE_ORDER_RANGE = 'validateOrderRange';
    private static final string EP_ORDER_CONFIG_TRIGGER_HELPER = 'EP_OrderConfigTriggerHelper';
    private static final string VALIDATE_ORDER_RANGE_BEFORE_UPDATE = 'validateOrderRangeBeforeUpdate';
 
   /**
       This method perform quantity validations for Order Configuration Records
   */
   public static void validateOrderRange(List<EP_Order_Configuration__c> listNewOrderConfig){
       Map<String,EP_Order_Configuration__c> countryRangeRec = new Map<String,EP_Order_Configuration__c>();
       Map<String,EP_Order_Configuration__c> prodCountryRangeMap = new Map<String,EP_Order_Configuration__c>();
       Set<String> productSet = new Set<String>();
       Set<String> countrySet = new Set<String>();
       Integer nRows;
       
       try{
        for(EP_Order_Configuration__c orderConfig : listNewOrderConfig )
        {    
            productSet.add(orderConfig.EP_Product__c);
            countrySet.add(orderConfig.EP_Country__c);
        }
        
        nRows = EP_Common_Util.getQueryLimit();
        
        for(EP_Order_Configuration__c countryRangeRecord :[SELECT EP_Country__c,EP_Product__c,EP_Min_Quantity__c,
                                                            EP_Max_Quantity__c,RecordType.DeveloperName FROM EP_Order_Configuration__c 
                                                             WHERE EP_Country__c IN : countrySet
                                                                 AND EP_Product__c IN :productSet
                                                                     AND RecordType.DeveloperName =: EP_Common_Constant.RT_COUNTRY_RANGE
                                                                         limit : nRows])
        {
            if(!(prodCountryRangeMap.containsKey(countryRangeRecord.EP_Country__c+
                                                EP_Common_Constant.BLANK+countryRangeRecord.EP_Product__c)))
            {
                prodCountryRangeMap.put(countryRangeRecord.EP_Country__c+
                                        EP_Common_Constant.BLANK+countryRangeRecord.EP_Product__c,countryRangeRecord);
            }
        }
        
        for(EP_Order_Configuration__c orderConfig : listNewOrderConfig)
        {
            String prodCountryKey = orderConfig.EP_Country__c+EP_Common_Constant.BLANK+orderConfig.EP_Product__c;
            String recordTypeName  = EP_Common_Util.fetchRecordTypeName(EP_Common_Constant.ORDER_CONFIG_OBJ,orderConfig.RecordTypeId);
            EP_Order_Configuration__c countryRange = prodCountryRangeMap.get(prodCountryKey);
            // Validate Mixing and Non Mixing Record Types
            if(recordTypeName != EP_Common_Constant.COUNTRY_RANGE)
            {           
                // Check if Order configuration setup is done earlier for country range record type else throw error
                if(prodCountryRangeMap.containsKey(prodCountryKey))
                {
                    
                    // Validate only minimum quantity in case of Non Mixing Record Type
                    if(recordTypeName.equals(EP_Common_Constant.NON_MIXING_RANGE))
                    {
                        if(!(orderConfig.EP_Min_Quantity__c >= countryRange.EP_Min_Quantity__c 
                                &&  orderConfig.EP_Min_Quantity__c <= countryRange.EP_Max_Quantity__c ))                   
                           {
                            orderConfig.addError(Label.EP_OrderConfig_Quantity_Error);
                           }
                    }
                    
                    // Validate Min and Max quantity in case of Mixing Record Type
                    else
                    {
                        if(!((orderConfig.EP_Min_Quantity__c >= countryRange.EP_Min_Quantity__c
                             &&  orderConfig.EP_Min_Quantity__c <= countryRange.EP_Max_Quantity__c ) 
                             && (orderConfig.EP_Max_Quantity__c >= countryRange.EP_Min_Quantity__c 
                             && orderConfig.EP_Max_Quantity__c <= countryRange.EP_Max_Quantity__c )))
                           {
                            orderConfig.addError(Label.EP_OrderConfig_Quantity_Error);
                           }
                    }
                    
                }
                
                else{
                          orderConfig.addError(Label.EP_OrderConfig_Setup_Error);
                }
            }
            
            else{
                if(prodCountryRangeMap.containsKey(prodCountryKey)){
                orderConfig.addError(Label.EP_OrderConfig_DuplicateError);
                }
            }
        }
       }
         catch(Exception handledException){
                EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, 
                                                     VALIDATE_ORDER_RANGE,EP_ORDER_CONFIG_TRIGGER_HELPER, 
                                                     ApexPages.Severity.ERROR );
                
            }
    
    }

  }