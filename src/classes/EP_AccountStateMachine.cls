/*
 *  @Author <Accenture>
 *  @Name <EP_AccountStateMachine >
 *  @CreateDate <03/02/2017>
 *  @Description <Account StateMachine provides account state based on Account record type>
 *  @Version <1.0>
 */

 public virtual class EP_AccountStateMachine {
    public EP_AccountDomainObject accountDomain;
    public EP_AccountEvent accountEvent;
    
    public EP_AccountStateMachine(){

    }
    
    public virtual EP_AccountState getAccountState(){
        EP_GeneralUtility.Log('Public','EP_AccountStateMachine','getAccountState');
        return getAccountStateInstance(this.accountDomain,this.accountEvent);
    }

    public EP_AccountState getAccountState(EP_AccountEvent currentEvent){
        EP_GeneralUtility.Log('Public','EP_AccountStateMachine','getAccountState');
        return getAccountStateInstance(accountDomain,currentEvent);
    } 
    //a Static method to return the state in some invocation scenarios
    public static EP_AccountState getAccountState(EP_AccountDomainObject currentAccount,EP_AccountEvent currentEvent){
        EP_GeneralUtility.Log('Public','EP_AccountStateMachine','getAccountState');
        return getAccountStateInstance(currentAccount,currentEvent);
    }
    @TestVisible
    private static EP_AccountState getAccountStateInstance(EP_AccountDomainObject localaccountDomain, EP_AccountEvent currentEvent){
        EP_GeneralUtility.Log('Private','EP_AccountStateMachine','getAccountStateInstance');
        string classname = getAccountStateClassName(localaccountDomain);
        EP_AccountState accountState;
        try{
            Type typ = Type.forName(classname);
            accountState = (EP_AccountState)typ.newInstance();
            accountState.setAccountContext(localaccountDomain, currentEvent);
            }catch(Exception e){
                system.debug(e);
                throw new AccountStateMachineException('Account State Machine not available for ' + classname);
            }
            return accountState;
        }

        public void setAccountDomainObject(EP_AccountDomainObject accountDomainObject){
            EP_GeneralUtility.Log('Public','EP_AccountStateMachine','setAccountDomainObject');
            accountDomain = accountDomainObject;
        }
        @TestVisible
        private static string getAccountStateClassName(EP_AccountDomainObject accountDomain){
        string accountType = accountDomain.localAccount.EP_Entity_Type__c;
        string accountStateClassName;
        for(EP_Account_State_Mapping__c stateMapping  : EP_Account_State_Mapping__c.getall().values()){
            if(statemapping.Account_Type__c == accountType && statemapping.Account_Status__c == accountDomain.localAccount.EP_Status__c ){
                accountStateClassName = stateMapping.Classname__c;
                break;
            }
        }
        return accountStateClassName;
    }
    @TestVisible
    private static string getAccountType(EP_AccountDomainObject localAccountDomain){
        EP_GeneralUtility.Log('Private','EP_AccountStateMachine','getAccountType');
        String stateMachineType;
        string accountType = localAccountDomain.localAccount.EP_Account_Type__c;
        if(accountType.equalsIgnoreCase('ShipTo')){        
            accountType  = localAccountDomain.localAccount.EP_ShipTo_Type__c+accountType;
        }
        return accountType;
    }
}