/**
 * This class contains unit tests for validating the behavior of InvoiceTrigger
 * and triggers.
 *
 */
@isTest
private class EP_InvoiceTrigger_Test {
	
    static testMethod void validateInvoice_Test() {
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
			Account sellToAccount = EP_AccountTestDataUtility.createSellToAccountWithActiveStatus(1, true)[0];
			test.startTest();
			 	EP_Invoice__c invoice = EP_TestDataUtility.createInvoice(sellToAccount);
		        invoice.EP_Invoice_Due_Date__c = system.today().addDays(1);
		        invoice.EP_Remaining_Balance__c = 1.0;
		        invoice.EP_Invoice_Issue_Date__c = system.today().addDays(-15);
		        invoice.EP_Type__c = EP_Common_Constant.PREPAYMENT_INVOICE;
		        Database.insert(invoice);
			test.stopTest();
			  
			EP_Invoice__c invoice1 = [select EP_Is_Proforma_Invoice__c,EP_Invoice_Type__c from EP_Invoice__c limit 1];
			System.assertEquals(true, invoice1.EP_Is_Proforma_Invoice__c);
			System.assertEquals(EP_Common_Constant.Proforma, invoice1.EP_Invoice_Type__c);
		}
    }
}