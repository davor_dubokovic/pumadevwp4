/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationDeactiveToDeactive >
*  @CreateDate <20/12/2017>
*  @Description <Handles Storage Ship To Account status change from 06-Deactivate to 06-Deactivate>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationDeactiveToDeactive extends EP_AccountStateTransition {
	
	/**
    * @author           Accenture
    * @name             EP_ASTStorageLocationDeactiveToDeactive
    * @date             20/12/2017
    * @description      Constructor
    * @param            NA
    * @return           NA 
    */
    public EP_ASTStorageLocationDeactiveToDeactive () {
        finalState = EP_AccountConstant.DEACTIVATE;
    }
	
	/**
    * @author           Accenture
    * @name             isTransitionPossible
    * @date             20/12/2017
    * @description      Check wether transitionposoble or not
    * @param            NA
    * @return           boolean 
    */
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationDeactiveToDeactive ','isTransitionPossible');
        return super.isTransitionPossible();
    }

	/**
    * @author           Accenture
    * @name             isRegisteredForEvent
    * @date             20/12/2017
    * @description      Check wether Registered For Event or not
    * @param            NA
    * @return           boolean 
    */
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationDeactiveToDeactive ', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
	
	/**
    * @author           Accenture
    * @name             isGuardCondition
    * @date             20/12/2017
    * @description      Check all validation
    * @param            NA
    * @return           boolean 
    */
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationDeactiveToDeactive ',' isGuardCondition');        
        return true;
    }
}