@isTest
public class EP_RetrospectiveOrderBatch_UT{
    
    @isTest
    static void startExecuteTest(){
        EP_File__c tempFile =  new EP_File__c(Name = 'TestFile',EP_CheckSum_Key__c = '12swerdERT345sdErrr');
        database.insert(tempFile);
    	EP_RO_Import_Staging__c staging = EP_TestDataUtility.CreateThirdPartyROStagingRecord(tempFile.Id,null,false);
        staging.EP_Status__c = 'Pending';
        Id recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'EP_Third_Party_Order' LIMIT 1].Id;
        staging.RecordTypeId = recordTypeId;
        INSERT staging;
        
        Test.startTest();
            EP_RetrospectiveOrderBatch batch = new EP_RetrospectiveOrderBatch();
            DataBase.executeBatch(batch);
        Test.stopTest();
    }
    
    @isTest
    static void startExecuteTest2(){
        csord__Order__c testOrder = EP_TestDataUtility.getCSOrder();
        csord__Order__c testOrderQuery = [SELECT Id, OrderNumber__c FROM csord__Order__c WHERE Id =: testOrder.Id LIMIT 1];
        Id recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'EP_Third_Party_Order' LIMIT 1].Id;
        
        EP_File__c tempFile =  new EP_File__c(Name = 'TestFile',EP_CheckSum_Key__c = '12swerdERT345sdErrr');
        database.insert(tempFile);
    	EP_RO_Import_Staging__c staging = EP_TestDataUtility.CreateThirdPartyROStagingRecord(tempFile.Id,null,false);
        staging.EP_Status__c = 'Pending';
        staging.RecordTypeId = recordTypeId;
        staging.EP_Order_Number__c = testOrderQuery.OrderNumber__c;
        INSERT staging;
        
        Test.startTest();
            EP_RetrospectiveOrderBatch batch = new EP_RetrospectiveOrderBatch();
            DataBase.executeBatch(batch);
        Test.stopTest();
    }
    
    /*@isTest
    static void insertOrderTest(){
        EP_File__c tempFile =  new EP_File__c(Name = 'TestFile',EP_CheckSum_Key__c = '12swerdERT345sdErrr');
        database.insert(tempFile);
    	EP_RO_Import_Staging__c staging = EP_TestDataUtility.CreateThirdPartyROStagingRecord(tempFile.Id,null,false);
        INSERT staging;
        
        EP_RO_Import_Staging__c testStaging = [SELECT Id, EP_SupplyLocation__c, EP_Sell_To__c, EP_BOL_Number__c, EP_Ship_To__c,
                                                    EP_Transporter__c, EP_Loading_Date__c, EP_Delivered_Date__c, EP_Customer_PO_Number__c,
                                                    EP_Delivery_Reference_No__c, EP_Staging_Order_Identifier__c,
                                                    EP_Ordered_quantity__c, EP_Ambient_Loaded_Quantity__c, EP_Standard_Loaded_Quantity__c,
                                                    EP_Ambient_Delivered_Quantity__c, EP_Standard_Delivered_Quantity__c, EP_Product_Code__c,
                                                    EP_Order_Number__c
                                                FROM EP_RO_Import_Staging__c 
                                                WHERE Id =: staging.Id LIMIT 1];
        
        Test.startTest();
            EP_RetrospectiveOrderBatch.insertOrder(new List<EP_RO_Import_Staging__c>{testStaging});
        Test.stopTest();
        
        csord__Order__c orderResult = [SELECT Id FROM csord__Order__c LIMIT 1];
        csord__Order_Line_Item__c lineItemResult = [SELECT Id FROM csord__Order_Line_Item__c LIMIT 1];
        
        System.assertNotEquals(orderResult, null, 'Result was not as expected.');
        System.assertNotEquals(lineItemResult, null, 'Result was not as expected.');
    }
    
    @isTest
    static void updateOrderTest(){
        csord__Order__c testOrder = EP_TestDataUtility.getCSOrder();
        csord__Order__c testOrderQuery = [SELECT Id, OrderNumber__c FROM csord__Order__c WHERE Id =: testOrder.Id LIMIT 1];
        system.debug('=====>testOrder: '+testOrder);
        system.debug('=====>testOrderQuery: '+testOrderQuery);
        
        EP_File__c tempFile =  new EP_File__c(Name = 'TestFile',EP_CheckSum_Key__c = '12swerdERT345sdErrr');
        database.insert(tempFile);
    	EP_RO_Import_Staging__c testStaging = EP_TestDataUtility.CreateThirdPartyROStagingRecord(tempFile.Id,null,false);
        testStaging.EP_Order_Number__c = testOrder.OrderNumber__c;
        INSERT testStaging;
        
        EP_RO_Import_Staging__c testStagingQuery = [SELECT Id, EP_SupplyLocation__c, EP_Sell_To__c, EP_BOL_Number__c, EP_Ship_To__c,
                                                    EP_Transporter__c, EP_Loading_Date__c, EP_Delivered_Date__c, EP_Customer_PO_Number__c,
                                                    EP_Delivery_Reference_No__c, EP_Staging_Order_Identifier__c,
                                                    EP_Ordered_quantity__c, EP_Ambient_Loaded_Quantity__c, EP_Standard_Loaded_Quantity__c,
                                                    EP_Ambient_Delivered_Quantity__c, EP_Standard_Delivered_Quantity__c, EP_Product_Code__c,
                                                    EP_Order_Number__c
                                                FROM EP_RO_Import_Staging__c 
                                                WHERE Id =: testStaging.Id LIMIT 1];
        
        Test.startTest();
            EP_RetrospectiveOrderBatch.updateOrder(new List<EP_RO_Import_Staging__c>{testStagingQuery});
        Test.stopTest();
        
        System.assertEquals(result.keySet().contains(testStaging.EP_Staging_Order_Identifier__c), true, 'Result was not as expected.');
        System.assertEquals(result.get(testStaging.EP_Staging_Order_Identifier__c)[0], testStaging, 'Result was not as expected.');
    }
    
    @isTest
    static void groupImportStagingTest(){
        EP_File__c tempFile =  new EP_File__c(Name = 'TestFile',EP_CheckSum_Key__c = '12swerdERT345sdErrr');
        database.insert(tempFile);
    	EP_RO_Import_Staging__c testStaging = EP_TestDataUtility.CreateThirdPartyROStagingRecord(tempFile.Id,null,false);
        INSERT testStaging;
        
        Test.startTest();
            Map<String, List<EP_RO_Import_Staging__c>> result = EP_RetrospectiveOrderBatch.groupImportStaging(new List<EP_RO_Import_Staging__c>{testStaging});
        Test.stopTest();
        
        System.assertNotEquals(result.size(), 0, 'Result was not as expected.');
        System.assertEquals(result.keySet().contains(testStaging.EP_Staging_Order_Identifier__c), true, 'Result was not as expected.');
        System.assertEquals(result.get(testStaging.EP_Staging_Order_Identifier__c)[0], testStaging, 'Result was not as expected.');
    }
    
    @isTest
    static void orderSObjectTest(){
        EP_File__c tempFile =  new EP_File__c(Name = 'TestFile',EP_CheckSum_Key__c = '12swerdERT345sdErrr');
        database.insert(tempFile);
    	EP_RO_Import_Staging__c testStaging = EP_TestDataUtility.CreateThirdPartyROStagingRecord(tempFile.Id,null,false);
        INSERT testStaging;
        
        Contract contract = new Contract();
        INSERT contract;
        
        Test.startTest();
            csord__Order__c result = EP_RetrospectiveOrderBatch.orderSObject(testStaging, String.valueOf(contract.Id));
        Test.stopTest();
        
        System.assertNotEquals(result, null, 'Result was not as expected.');
        System.assertEquals(result.EP_BOL_Number__c, testStaging.EP_BOL_Number__c, 'Result was not as expected.');
        System.assertEquals(result.EP_Customer_PO_Number__c, testStaging.EP_Customer_PO_Number__c, 'Result was not as expected.');
    }
    
    @isTest
    static void orderLineItemSObjectTest(){
        EP_File__c tempFile =  new EP_File__c(Name = 'TestFile',EP_CheckSum_Key__c = '12swerdERT345sdErrr');
        database.insert(tempFile);
    	EP_RO_Import_Staging__c testStaging = EP_TestDataUtility.CreateThirdPartyROStagingRecord(tempFile.Id,null,false);
        INSERT testStaging;
        
        csord__Order__c testOrder = EP_TestDataUtility.getCSOrder();
        
        Test.startTest();
            csord__Order_Line_Item__c result = EP_RetrospectiveOrderBatch.orderLineItemSObject(testStaging, testOrder);
        Test.stopTest();
        
        System.assertNotEquals(result, null, 'Result was not as expected.');
        System.assertEquals(result.Quantity__c, testStaging.EP_Ordered_quantity__c, 'Result was not as expected.');
        System.assertEquals(result.csord__Order__c, testOrder.Id, 'Result was not as expected.');
    }*/
}