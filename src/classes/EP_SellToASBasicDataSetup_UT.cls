@isTest
public class EP_SellToASBasicDataSetup_UT{
	
	static final string EVENT_NAME = '02-BasicDataSetupTo02-BasicDataSetup';
	static final string EVENT_ACCOUNT_SET= '04-AccountSet-upTo05-Active';
	static final string INVALID_EVENT_NAME = '08-RejectedTo02-BasicDataSetup';
	
	@testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    }

	static testMethod void setAccountDomainObject_test() {
	    EP_SellToASBasicDataSetup localObj = new EP_SellToASBasicDataSetup();
	    EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
	    EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
	    localObj.setAccountContext(obj,oe);
	    Test.startTest();
	    localObj.setAccountDomainObject(obj);
	    Test.stopTest();
	    System.AssertEquals(obj,localObj.account);	    
	}
	
	static testMethod void doOnEntry_test() {
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
	    EP_SellToASBasicDataSetup localObj = new EP_SellToASBasicDataSetup();
	    EP_AccountDomainObject objdomain = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
	    Account oldAccount = objdomain.localaccount.clone();
        oldAccount.EP_Status__c = EP_AccountConstant.PROSPECT;
        EP_AccountDomainObject obj = new EP_AccountDomainObject(objdomain.localaccount, oldAccount);
	    EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
	    localObj.setAccountContext(obj,oe);
	    Test.startTest();
	    localObj.doOnEntry();
	    Test.stopTest();
	    //No Assert required in this case
	    system.assert(true);
	}

	static testMethod void doOnExit_test() {
	    EP_SellToASBasicDataSetup localObj = new EP_SellToASBasicDataSetup();
	    EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
	    EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
	    localObj.setAccountContext(obj,oe);
	    Test.startTest();
	    localObj.doOnExit();
	    Test.stopTest();
		//No Assert required in this case
		system.assert(true);
	}
	static testMethod void doTransition_PositiveScenariotest() {
	    EP_SellToASBasicDataSetup localObj = new EP_SellToASBasicDataSetup();
	    EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
	    EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
	    localObj.setAccountContext(obj,oe);
	    Test.startTest();
	    Boolean result = localObj.doTransition();
	    Test.stopTest();
	    System.AssertEquals(true,result);
	}
	static testMethod void doTransition_NegativeScenariotest() {
	    EP_SellToASBasicDataSetup localObj = new EP_SellToASBasicDataSetup();
	    EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectNegativeScenario();
	    EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
	    localObj.setAccountContext(obj,oe);
	    String message;
		Test.startTest();
		try{
			Boolean result = localObj.doTransition();
		}catch(Exception e){
            message = e.getMessage();
        }
        Test.stopTest();
        System.assert(string.isNotBlank(message));
	}
	static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
	    EP_SellToASBasicDataSetup localObj = new EP_SellToASBasicDataSetup();
	    EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
	    EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
	    localObj.setAccountContext(obj,oe);
	    Test.startTest();
	    Boolean result = localObj.isInboundTransitionPossible();
	    Test.stopTest();
	    System.AssertEquals(true,result);
	}
    /** 
        This isInboundTransitionPossible() method is returning only true for all cases.
        So only positive scenario applicable. 
    **/
}