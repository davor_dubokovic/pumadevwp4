/* 
   @Author          Accenture
   @name            EP_StagingRetailOrderWS
   @CreateDate      13/04/2018
   @Description     Web service class to create File and staging reocord for retail orders
   @Version         1.0
*/
@RestResource(urlMapping='/v1/StagingRetailOrderSync/*')
global without sharing class EP_StagingRetailOrderWS{
    /**
    * @author       Accenture
    * @name         UpsertStockLocation
    * @date         13/04/2018
    * @description  Processess the Retail order request and sends back the acknowledgement
    * @param        NA 
    * @return       NA
    */
    @HttpPost
    global static void ProcessRetailOrderRequest(){
      RestRequest req = RestContext.request;
        String requestBody = req.requestBody.toString();
        RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
        EP_IntegrationService service = new EP_IntegrationService();
        string response =service.handleRequest(EP_Common_Constant.NAV_TO_SFDC_STOCK_LOCATION_SYNC,requestBody);
        RestContext.response.responseBody = blob.valueOf(response);
    } 
}