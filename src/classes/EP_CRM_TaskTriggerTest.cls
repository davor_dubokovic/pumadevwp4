/* ================================================
 * @Class Name : EP_CRM_TaskTriggerTest
 * @author : Kamendra Singh
 * @Purpose: This class is used to test EP_CRM_TaskTriggertrigger.
 * @created date: 17/04/2016
 ================================================*/
@isTest
private class EP_CRM_TaskTriggerTest{
     /* This method is used to create all test data and test before insert tasks schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testTaskInsertUpdateTrigger(){
        try{
            //Run as Sales User
            Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' LIMIT 1]; 
            User u=new User();
            u.firstName='Test First';
            u.lastname='Sales CRM';
            u.profileid=p.id;
            u.Alias = 'salesman';
            u.Email='standardusercrm@accenture.com'; 
            u.EmailEncodingKey='UTF-8';
            u.LanguageLocaleKey='en_US'; 
            u.LocaleSidKey='en_AU';
            u.TimeZoneSidKey='Australia/Brisbane';
            u.UserName='standardusercrm@accenture.com';
            u.CurrencyIsoCode='AUD';
            u.EP_CRM_Geo1__c='Africa';
            u.EP_CRM_Geo2__c='Ghana';
           
            Database.SaveResult saveUer = Database.insert(u);
            System.runAs(u){
   
            // Insert Account
            Account acc=new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';
            acc.EP_CRM_Geo1__c='Africa';
            acc.EP_CRM_Geo2__c='Ghana';
            
            Database.SaveResult saveacc = Database.insert(acc);
            //Insert Opportunity
            Opportunity opp=new Opportunity();
            opp.name='Test';
            opp.Stagename='Prospecting';
            opp.AccountId=acc.Id;
            //opp.EP_CRM_Credit_Approval_Received__c = true;
            opp.CloseDate=Date.Today() + 5;
            opp.EP_CRM_Primary_Product_Interest__c = 'Aviation';
            opp.EP_CRM_Current_Supplier__c = 'Allied';
            opp.EP_CRM_Industry__c = 'Agriculture';
            //opp.EP_CRM_KYC_Done__c = true;
            opp.EP_CRM_Margin_unit_of_measure__c = 1;
            opp.EP_CRM_Price_Type__c = 'Formula Price';
            //opp.EP_CRM_Pricing_Approval__c = true;
            opp.EP_CRM_Product_Interest_Group__c = 'Aviation';
            opp.EP_CRM_Volume__c = 1;
            opp.EP_CRM_Geo1__c='Africa';
            opp.EP_CRM_Geo2__c='Ghana';
            opp.LeadSource='Campaign';
            Database.SaveResult saveopp = Database.insert(opp);
            
            Task tempTask = new Task();
            tempTask.OwnerId = UserInfo.getUserId();
            tempTask.Subject='Donni';
            tempTask.whatId = opp.Id;
			tempTask.Type='Call';  
            Database.SaveResult savetempTask = Database.insert(tempTask);
           
            //Check System assert---------
            Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id limit 1];
            
 
            
            //update Event........
            tempTask.Subject = 'test subject';
            
            Database.SaveResult updatetempTask = Database.update(tempTask);
            Test.startTest();
            
             //Check System assert---------
            Opportunity tempOpp1 = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id limit 1];
            

                
            test.stopTest();
            
            //validate through system Assert 
            System.Assert(tempOpp1.EP_CRM_Last_Activity_Date__c != null);
            System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c != null);
            }
        }
        catch(Exception ex){
            ex.getmessage();
        }
    }
     /* This method is used to create all test data and test before delete event schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testBulkTasktDeleteTrigger(){
        try{
            //Run as Sales User
            Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' LIMIT 1]; 
            User u=new User();
            u.firstName='Test First';
            u.lastname='Sales CRM';
            u.profileid=p.id;
            u.Alias = 'salesman';
            u.Email='standardusercrm@accenture.com'; 
            u.EmailEncodingKey='UTF-8';
            u.LanguageLocaleKey='en_US'; 
            u.LocaleSidKey='en_AU';
            u.TimeZoneSidKey='Australia/Brisbane';
            u.UserName='standardusercrm@accenture.com';
            u.CurrencyIsoCode='AUD';
            u.EP_CRM_Geo1__c='Africa';
            u.EP_CRM_Geo2__c='Ghana';
            Database.SaveResult saveUser = Database.insert(u);
            
            System.runAs(u){
            
             // Insert Account
            Account acc=new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';
            acc.EP_CRM_Geo1__c='Africa';
            acc.EP_CRM_Geo2__c='Ghana';
            Database.SaveResult saveacc = Database.insert(acc);
            
            //Insert Opportunity
            Opportunity opp=new Opportunity();
            opp.name='Test';
            opp.Stagename='Prospecting';
            opp.AccountId=acc.Id;
            //opp.EP_CRM_Credit_Approval_Received__c = true;
            opp.CloseDate=Date.Today() + 5;
            opp.EP_CRM_Primary_Product_Interest__c = 'Aviation';
            opp.EP_CRM_Current_Supplier__c = 'Allied';
            opp.EP_CRM_Industry__c = 'Agriculture';
            //opp.EP_CRM_KYC_Done__c = true;
            opp.EP_CRM_Margin_unit_of_measure__c = 1;
            opp.EP_CRM_Price_Type__c = 'Formula Price';
            //opp.EP_CRM_Pricing_Approval__c = true;
            opp.EP_CRM_Product_Interest_Group__c = 'Aviation';
            opp.EP_CRM_Volume__c = 1;
            opp.EP_CRM_Geo1__c='Africa';
            opp.EP_CRM_Geo2__c='Ghana';
            opp.LeadSource='Campaign';
            Database.SaveResult saveopp = Database.insert(opp);
            
            //insert Event..............
            list<Task> lstTask = new list<Task>();
            Task tempTask = new Task();
            tempTask.OwnerId = UserInfo.getUserId();
            tempTask.Subject='Donni';
            tempTask.whatId = opp.Id;
            tempTask.Type='Call';  
                
            Task tempTask1 = new Task();
            tempTask1.OwnerId = UserInfo.getUserId();
            tempTask1.Subject='Donni';
            tempTask1.whatId = opp.Id;
            tempTask1.Type='Call';  
            
            lstTask.add(tempTask );
            lstTask.add(tempTask1);
            
            Database.SaveResult[] savelstTask = Database.insert(lstTask);
                
            //delete Event........
            Database.deleteResult deletetempTask = Database.delete(tempTask);
                if(deletetempTask.isSuccess())
            system.debug('after deleting'+tempTask);
            Test.startTest();
            //Check System assert---------
            Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id limit 1];
            

            test.stopTest();
            
            //validate through system Assert 
            System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c != null);
            }
        }
        catch(Exception ex){
            ex.getmessage();
        }
    }
    
     /* This method is used to create all test data and test before delete event schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testSingleTaskDeleteTrigger(){
        try{
            //Run as Sales User
            Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' LIMIT 1]; 
            User u=new User();
            u.firstName='Test First';
            u.lastname='Sales CRM';
            u.profileid=p.id;
            u.Alias = 'salesman';
            u.Email='standardusercrm@accenture.com'; 
            u.EmailEncodingKey='UTF-8';
            u.LanguageLocaleKey='en_US'; 
            u.LocaleSidKey='en_AU';
            u.TimeZoneSidKey='Australia/Brisbane';
            u.UserName='standardusercrm@accenture.com';
            u.CurrencyIsoCode='AUD';
            u.EP_CRM_Geo1__c='Africa';
            u.EP_CRM_Geo2__c='Ghana';
            Database.SaveResult saveUser = Database.insert(u);
            
            System.runAs(u){
                
             // Insert Account
            Account acc=new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';
            acc.EP_CRM_Geo1__c='Africa';
            acc.EP_CRM_Geo2__c='Ghana';
             
            Database.SaveResult saveacc = Database.insert(acc);
            
            //Insert Opportunity
            Opportunity opp=new Opportunity();
            opp.name='Test';
            opp.Stagename='Prospecting';
            opp.AccountId=acc.Id;
            //opp.EP_CRM_Credit_Approval_Received__c = true;
            opp.CloseDate=Date.Today() + 5;
            opp.EP_CRM_Primary_Product_Interest__c = 'Aviation';
            opp.EP_CRM_Current_Supplier__c = 'Allied';
            opp.EP_CRM_Industry__c = 'Agriculture';
            //opp.EP_CRM_KYC_Done__c = true;
            opp.EP_CRM_Margin_unit_of_measure__c = 1;
            opp.EP_CRM_Price_Type__c = 'Formula Price';
           // opp.EP_CRM_Pricing_Approval__c = true;
            opp.EP_CRM_Product_Interest_Group__c = 'Aviation';
            opp.EP_CRM_Volume__c = 1;
            opp.EP_CRM_Geo1__c='Africa';
            opp.EP_CRM_Geo2__c='Ghana';
            opp.LeadSource='Campaign';
            
            Database.SaveResult saveaopp = Database.insert(opp);
            
            //insert Event..............
           
            Task tempTask = new Task();
            tempTask.OwnerId = UserInfo.getUserId();
            tempTask.Subject='Donni';
            tempTask.whatId = opp.Id;
            tempTask.Type='Call';  
            
            Database.SaveResult savetempTask= Database.insert(tempTask);
            
            //delete Event........
            delete tempTask ;
            Database.saveResult deletetempTask = Database.insert(tempTask);
            
             Test.startTest();
            //Check System assert---------
            Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id limit 1];
            
            test.stopTest();
            //validate through system Assert 
            System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c == null);
            }
        }
        catch(Exception ex){
            ex.getmessage();
        }
    }
}