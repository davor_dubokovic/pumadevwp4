@isTest
private class EP_ROImportStagingMapper_UT {
	private static final string THIRDPARTYRTNAME =  'Third Party Order';
	private static final string INVALID_THIRDPARTYRTNAME =  'EP_Third Party Order';
	private static final string SOBJTYPE  = 'EP_RO_Import_Staging__c';
    static testMethod void getRecordTypeIdByNameTest() {
        EP_ROImportStagingMapper localObj = new EP_ROImportStagingMapper();
        Test.startTest();
        String result = localObj.getRecordTypeIdByName(THIRDPARTYRTNAME);
        Test.stopTest();
        Id recTypId = [select id FROM RecordType Where Name =: THIRDPARTYRTNAME AND sObjectType =: SOBJTYPE].Id;
        system.assertEquals(recTypId,result);
    }
    static testMethod void getRecordTypeIdByNameNegativeTest() {
        EP_ROImportStagingMapper localObj = new EP_ROImportStagingMapper();
        try{
        	Test.startTest();
        	String result = localObj.getRecordTypeIdByName(INVALID_THIRDPARTYRTNAME);
        	Test.stopTest();
    	}catch(Exception ex){
        	system.assertNotEquals(null,ex);
    	}        
    }
    static testMethod void getRecordByFileIdTest() {
    	EP_File__c tempFile =  new EP_File__c(Name = 'TestFile',EP_CheckSum_Key__c = '12swerdERT345sdErrr');
        database.insert(tempFile);
    	List<EP_RO_Import_Staging__c> stagingRecords = Test.loadData(EP_RO_Import_Staging__c.sObjectType, 'EP_RO_Import_Staging_Test_Data');
    	for(EP_RO_Import_Staging__c obj : stagingRecords){
    		obj.EP_File_Name__c = tempFile.Id;
    	}
    	database.update(stagingRecords);
        EP_ROImportStagingMapper localObj = new EP_ROImportStagingMapper();
    	Test.startTest();
    	List<EP_RO_Import_Staging__c> result = localObj.getRecordByFileId(tempFile.Id);
    	Test.stopTest(); 
    	system.assertNotEquals(null,result);
    	system.assertEquals(stagingRecords.size(),result.size());     
    }
}