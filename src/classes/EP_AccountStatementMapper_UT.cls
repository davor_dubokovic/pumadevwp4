/*
   @Author <Accenture>
   @name <EP_AccountStatementMapper_UT>
   @Description <This is the test class for the class of EP_AccountStatementMapper>
   @Version <1.0>
*/
@isTest
private class EP_AccountStatementMapper_UT {
	/**
	* @author Accenture
	* @date 13/03/2018
	* @description create test data for all required object in test methods
	*/
	 @TestSetup
	 static void initData() {
        // Create Bill TO Account
        Account TempAccount = EP_TestDataUtility.createBillToAccount();
        insert TempAccount;
        //Create Customer Account Statement
        EP_Customer_Account_Statement__c tempCAS = EP_TestDataUtility.createCustomerAccountStatement(TempAccount.id);
        tempCAS.EP_Customer_Account_Statement_Key__c = '123123123sdsdeasdasasdsads';
        insert tempCAS;
	}
	
	/**
    * @author Accenture
    * @date 13/03/2018
    * @description PositiveTest to check getCustAccstmtNrIdMap method.
    */
    static testMethod void getCustAccstmtNrIdMap_PositiveTest(){
    	set<Id> setBillToId = new set<Id>();
    	Map<String, EP_Customer_Account_Statement__c> CustAccstmtNrIdMap = new Map<String, EP_Customer_Account_Statement__c>();
    	Account AccountTemp = [select id from Account limit 1];
    	setBillToId.add(AccountTemp.id);
    	
    	Test.startTest();
            CustAccstmtNrIdMap = EP_AccountStatementMapper.getCustAccstmtNrIdMap(setBillToId);
        Test.stopTest();
        
        //Assert
        system.assertEquals(CustAccstmtNrIdMap.isEmpty() , false);
    }
    
    /**
    * @author Accenture
    * @date 13/03/2018
    * @description NegativeTest to check getCustAccstmtNrIdMap method.
    */
    static testMethod void getCustAccstmtNrIdMap_NegativeTest(){
    	set<Id> setBillToId = new set<Id>();
    	Map<String, EP_Customer_Account_Statement__c> CustAccstmtNrIdMap = new Map<String, EP_Customer_Account_Statement__c>();

    	Test.startTest();
            CustAccstmtNrIdMap = EP_AccountStatementMapper.getCustAccstmtNrIdMap(setBillToId);
        Test.stopTest();
        
        //Assert
        system.assertEquals(CustAccstmtNrIdMap.isEmpty() , true);
    }
}