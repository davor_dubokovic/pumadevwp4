/**
  * @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
  * @name        : EP_PackagedProducts
  * @CreateDate  : 16/02/2017
  * @Description : This is Class Contains logic Packaged Order
  * @Version     : <1.0>
  * @reference   : N/A
  */
  public class EP_BulkProducts extends EP_ProductSoldAs {

    public EP_BulkProducts() {
    }
    

    /** This method is returns Accounts for Packaged Order
     *  @date      16/02/2017
     *  @name      getShipTos
     *  @param     List<Account> lstAccount
     *  @return    List<Account>
     *  @throws    NA
     */
     public override List<Account> getShipTos(List<Account> lstAccount) {
      EP_GeneralUtility.Log('Public','EP_BulkProducts','getShipTos');

      List<Account> lstShipTos = new List<Account>();

      for (Account objAccount : lstAccount) {

        if (objAccount.RecordType.DeveloperName == EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME 
          || objAccount.RecordType.DeveloperName == EP_Common_Constant.VMI_SHIP_TO_DEV_NAME) {

          lstShipTos.add(objAccount);
        }
      }    

      return lstShipTos;
    }
    
    
    /** This method is used to get Tanks for the Bulk Products in pricebook
      *  @date      05/02/2017
      *  @name      getTanksForBulkProduct
      *  @param     Map<Id,EP_Tank__c> mapOperationalTanks
      *  @return    Map<Id,EP_Tank__c> 
      *  @throws    NA
      */
      public override Map<Id,EP_Tank__c> getTanksForBulkProduct(Map<Id,EP_Tank__c> mapOperationalTanks){
        EP_GeneralUtility.Log('Public','EP_BulkProducts','getTanksForBulkProduct');
        Map<Id,EP_Tank__c> mapBulkProductTanks = new Map<Id,EP_Tank__c>();
        for( EP_Tank__c tank : mapOperationalTanks.values() ){
          if( EP_Common_Constant.BULK_ORDER_PRODUCT_CAT.equalsIgnoreCase( tank.EP_Product__r.EP_Product_Sold_As__c )){
            mapBulkProductTanks.put( tank.Id,tank );
          }
        }
        return mapBulkProductTanks;
      }
    }