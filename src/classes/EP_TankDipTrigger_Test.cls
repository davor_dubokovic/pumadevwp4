@isTest
public with sharing class EP_TankDipTrigger_Test {
	@testSetup static void createTestData() {
		// create test record for Account with RecordType='VMI Ship To'
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
        Account obj1 = EP_TestDataUtility.getSellTo();
        Account testShipToAccount = EP_TestDataUtility.createShipToAccount(obj1.Id,strRecordTypeId);
        testShipToAccount.EP_Tank_Dips_Schedule_Time__c = '10:00';
        testShipToAccount.EP_Ship_To_UTC_Timezone__c='UTC+00:00';
        insert testShipToAccount;   
        
        testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_PROSPECT;
     	update testShipToAccount;
        testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        testShipToAccount.EP_Is_Valid_Address__c = true;
        update testShipToAccount;
        testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
       	update testShipToAccount;
       	
       	Product2 prod = EP_TestDataUtility.createTestRecordsForProduct();
       	EP_Tank__c tankObj = new EP_Tank__c(EP_Ship_To__c = testShipToAccount.id, EP_Product__c = prod.id,EP_Safe_Fill_Level__c = 2500); 
   		tankObj.EP_Tank_Status__c = 'New';
        insert tankObj;
        tankObj.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
        update tankObj;
        tankObj.EP_Tank_Status__c = EP_Common_Constant.OPERATIONAL_TANK_STATUS;
        tankObj.EP_Tank_Dip_Entry_Mode__c='Automatic Dip Entry';
        update tankObj;
	}
	private static testMethod void addTankStatus_test(){
		System.runAs(EP_TestDataUtility.createRunAsUser()) {
			EP_Tank__c tankObj1 = [select Id,EP_Tank_Status__c from EP_Tank__c limit 1];
			
			test.startTest();
			 	// create test records for EP_Tank_Dip__c of RecordType='Actual' with EP_Reading_Date_Time__c='system.now()-5'
	    		EP_TestDataUtility.createTestTankDipsRecords(new list<EP_Tank__c>{tankObj1}, system.now()-5, true);
			  test.stopTest();
			  
			EP_Tank_Dip__c dip = [select EP_Tank_Status__c from EP_Tank_Dip__c limit 1];
			System.assertEquals(tankObj1.EP_Tank_Status__c, dip.EP_Tank_Status__c);
		}
	}
	private static testMethod void validateTankDips_test(){ 
		System.runAs(EP_TestDataUtility.createRunAsUser()) {
			EP_Tank__c tankObj1 = [select Id,EP_Tank_Status__c from EP_Tank__c limit 1];
			EP_TestDataUtility.createTestTankDipsRecords(new list<EP_Tank__c>{tankObj1}, system.now()-1, false);
			EP_TestDataUtility.createTestTankDipsRecords(new list<EP_Tank__c>{tankObj1}, system.now()+1, false);
			
			test.startTest();
				EP_TestDataUtility.createTestTankDipsRecords(new list<EP_Tank__c>{tankObj1}, system.now(), false);
			test.stopTest();
			
			EP_Tank_Dip__c dip = [select EP_Tank_Dip_Reading_Date_Time_Error__c from EP_Tank_Dip__c limit 1];
			System.assertEquals(false, dip.EP_Tank_Dip_Reading_Date_Time_Error__c);
		}
	}
	private static testMethod void validateTankDips_test1(){ 
		System.runAs(EP_TestDataUtility.createRunAsUser()) {
			EP_Tank__c tankObj1 = [select Id,EP_Tank_Status__c from EP_Tank__c limit 1];
			EP_TestDataUtility.createTestTankDipsRecords(new list<EP_Tank__c>{tankObj1}, system.now()-1, false);
			EP_TestDataUtility.createTestTankDipsRecords(new list<EP_Tank__c>{tankObj1}, system.now()+1, false);
			list<EP_Tank_Dip__c> tankDipsToBeInserted = new list<EP_Tank_Dip__c>();
			ID RECORDTYPEID_ACTUAL = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get('Actual').getRecordTypeId();
			
			test.startTest();
	                EP_Tank_Dip__c tankDip = new EP_Tank_Dip__c();
	                tankDip.EP_Ambient_Quantity__c = 100;
	                tankDip.EP_Unit_Of_Measure__c = 'LT';
	                tankDip.EP_Tank__c = tankObj1.id;
	                tankDip.EP_Reading_Date_Time__c = system.now()+1;
	                tankDip.RecordTypeId = RECORDTYPEID_ACTUAL;
	                tankDipsToBeInserted.add(tankDip);
	                
	                EP_Tank_Dip__c tankDip1 = new EP_Tank_Dip__c();
	                tankDip1.EP_Ambient_Quantity__c = 100;
	                tankDip1.EP_Unit_Of_Measure__c = 'LT';
	                tankDip1.EP_Tank__c = tankObj1.id;
	                tankDip1.EP_Reading_Date_Time__c = system.now()-1;
	                tankDip1.RecordTypeId = RECORDTYPEID_ACTUAL;
	                tankDipsToBeInserted.add(tankDip1);
	                
	                EP_Tank_Dip__c tankDip2 = new EP_Tank_Dip__c();
	                tankDip2.EP_Ambient_Quantity__c = 100;
	                tankDip2.EP_Unit_Of_Measure__c = 'LT';
	                tankDip2.EP_Tank__c = tankObj1.id;
	                tankDip2.EP_Reading_Date_Time__c = system.now()+2;
	                tankDip2.RecordTypeId = RECORDTYPEID_ACTUAL;
	                tankDipsToBeInserted.add(tankDip2);
	            
	            	Database.insert(tankDipsToBeInserted, false);
			test.stopTest();
			
			EP_Tank_Dip__c dip = [select EP_Tank_Dip_Reading_Date_Time_Error__c from EP_Tank_Dip__c limit 1];
			System.assertEquals(false, dip.EP_Tank_Dip_Reading_Date_Time_Error__c);
		}
	}
	private static testMethod void removePlaceholderTankDips_test(){ 
		System.runAs(EP_TestDataUtility.createRunAsUser()) {
			EP_Tank__c tankObj1 = [select Id from EP_Tank__c limit 1];
			EP_TestDataUtility.createTestTankDipsRecords(new list<EP_Tank__c>{tankObj1}, system.now()-3, true);
			
			test.startTest();
				EP_TestDataUtility.createTestTankDipsRecords(new list<EP_Tank__c>{tankObj1}, system.now()-3, false);
			test.stopTest();
			
			list<EP_Tank_Dip__c> diplist = [select Id from EP_Tank_Dip__c limit 2];
			System.assertEquals(1, diplist.size());
		}
	}
	private static testMethod void updateUserUTCOffsetOnReadingTimeChange_test(){ 
		System.runAs(EP_TestDataUtility.createRunAsUser()) {
			EP_Tank__c tankObj1 = [select Id from EP_Tank__c limit 1];
			Map<ID, EP_Tank_Dip__c> tankDipsMap = EP_TestDataUtility.createTestTankDipsRecords(new list<EP_Tank__c>{tankObj1}, system.now()-3, false);
			list<EP_Tank_Dip__c> tankdipsList = new list<EP_Tank_Dip__c>();
			for(EP_Tank_Dip__c tankdip : tankDipsMap.values()){
				tankdip.EP_Reading_Date_Time__c = system.now();
				tankdipsList.add(tankdip);
			}
			
			test.startTest();
				database.update(tankdipsList);
			test.stopTest();
			
			EP_Tank_Dip__c dip = [select EP_LastModified_User_offset__c from EP_Tank_Dip__c limit 1];
			System.assertNotEquals(null, dip.EP_LastModified_User_offset__c);
		}
	}
	private static testMethod void updateIsExported_test(){ 
		System.runAs(EP_TestDataUtility.createRunAsUser()) {
			EP_Tank__c tankObj1 = [select Id from EP_Tank__c limit 1];
			Map<ID, EP_Tank_Dip__c> tankDipsMap = EP_TestDataUtility.createTestTankDipsRecords(new list<EP_Tank__c>{tankObj1}, system.now()-3, false);
			list<EP_Tank_Dip__c> tankdipsList = new list<EP_Tank_Dip__c>();
			for(EP_Tank_Dip__c tankdip : tankDipsMap.values()){
				tankdip.EP_Integration_Status__c = EP_Common_Constant.SYNC_STATUS;
				tankdip.EP_Tank_Dip_Exported__c = false;
				tankdipsList.add(tankdip);
			}
			database.update(tankdipsList);
			
			test.startTest();
				EP_TankDipTriggerHelper_R1.updateIsExported();
			test.stopTest();
			
			EP_Tank_Dip__c dip = [select EP_Tank_Dip_Exported__c from EP_Tank_Dip__c limit 1];
			System.assertEquals(false, dip.EP_Tank_Dip_Exported__c);
		}
	}
}