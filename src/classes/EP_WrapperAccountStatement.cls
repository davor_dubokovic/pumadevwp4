/*
   @Author <Accenture>
   @name <EP_WrapperAccountStatement>
   @Description <This class handles requests from EP_CustomerAccountStatementUtility class>
   @Version <1.0>
*/
global with sharing class EP_WrapperAccountStatement {
    public double wOpeningBalance {get;set;}
    public double wTotalDebits {get;set;}
    public double wTotalCredits {get;set;}
    public double wClosingBalance{get;set;}
    // Newly added fields @Alper Genc.
    public List<EP_WrapperAccountStatementItem> wAccountStatementItems {get; set;}
    
    /**
    *   This method creates the list to pass the Customer Account Statement 
    *   
    */
    public EP_WrapperAccountStatement() {
        wAccountStatementItems = new List<EP_WrapperAccountStatementItem>();
    }
}