/*   
     @Author <Jai Singh (Accenuture)>
     @name <EP_GenericRetryOI.cls>   
     @CreateDate <25/02/2016>   
     @Description: This class will contain method to perform retry to ERROR-SEND Integration Records
     @Version <1.1>
*/ 
public with sharing class EP_GenericRetryOI implements Queueable, Database.AllowsCallouts{
    
    //messageid to be processed 
    private String messageId;
    private static final String GENERIC_RETRY  = 'EP_Generic_Retry_OI';
    private static final String EXECUTE_METHOD  = 'execute';
    private static final String CLASSNAME = 'EP_GenericRetryOI';   
    /**
        Author: Jai Singh(Accenture)
        Description: This  is constructor to set messageid
        Method Name: EP_GenericRetryOI
        Created Date: 25/02/2016
        Params: String 
        Return Type: String
    */
    public EP_GenericRetryOI( String messageId ){
        this.messageId = messageId;
    }
    
    /**
        Author: Jai Singh(Accenture)
        Description: This method makes callout to for the sent MessageId integration record
        Method Name: execute
        Created Date: 25/02/2016
        Params: QueueableContext 
        Return Type: NA
    */ 
    public void execute(QueueableContext context){
    	
        try{
        	//Start Changes for #59187 
            list<EP_IntegrationRecord__c> listOfIntegrationRecord = [Select id,EP_Object_Type__c, EP_Message_Type__c,EP_Object_ID__c,EP_XML_Message__c,EP_Endpoint_URL__c,CreatedDate,EP_Message_ID__c,EP_Error_Code__c,EP_Queuing_Retry__c from EP_IntegrationRecord__c where EP_Message_ID__c =: this.messageId AND EP_IsLatest__c = True AND EP_ParentNode__c = null limit 1];
            if( !listOfIntegrationRecord.isEmpty() ){
                /*
                //get Endpoint and EML from record
                String endpoint = listOfIntegrationRecord[0].EP_Endpoint_URL__c;
                String requestXML = listOfIntegrationRecord[0].EP_XML_Message__c;
                Boolean isManualreTry = TRUE;
                
                //Make callout to webservice
                String success_IPASS = EP_IntegrationUtil.callSfdcToIPASS(endpoint, EP_COMMON_CONSTANT.POST, requestXML, GENERIC_RETRY);
                
                //if there is success from Integration Call then reSendOk API else reSendError API will be invoked to update Integration record
                if( success_IPASS == EP_Common_Constant.CODE_200 ){
                    EP_IntegrationUtil.reSendOk( this.messageId );
                }
                else{
                    EP_IntegrationUtil.reSendError( this.messageId, success_IPASS );
                }*/
                EP_AutomaticTechnicalRetryBatch.sendOutboundMessage(listOfIntegrationRecord[0]);
                
                //End Changes for #59187              
            }
        }
        catch(Exception handledException ){
            EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA, EXECUTE_METHOD , CLASSNAME, ApexPages.Severity.ERROR);            
        }
    }
}