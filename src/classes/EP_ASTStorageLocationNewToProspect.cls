/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationNewToProspect>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 01-Prospect to 01-Prospect>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationNewToProspect extends EP_AccountStateTransition {

    public EP_ASTStorageLocationNewToProspect () {
        finalState = EP_AccountConstant.PROSPECT;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationNewToProspect','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationNewToProspect', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationNewToProspect',' isGuardCondition');        
        return true;
    }
}