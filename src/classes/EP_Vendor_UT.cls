@isTest
 public class EP_Vendor_UT{

    static testMethod void isTransportOrThirdPartyVender_PositiveScenariotest() {
        EP_Vendor localObj = new EP_Vendor();
        Account acct = EP_TestDataUtility.getVendorPositiveScenario();
        acct.EP_VendorType__c = EP_AccountConstant.TRANSPORTER_VENDOR_TYPE;//L4# 78534 - Test Class Fix
        Test.startTest();
        Boolean result = localObj.isTransportOrThirdPartyVender(acct);
        Test.stopTest();
        System.AssertEquals(true,result);
    }

    static testMethod void isTransportOrThirdPartyVender_NegativeScenariotest() {
        EP_Vendor localObj = new EP_Vendor();
        Account acct = EP_TestDataUtility.getVendorNegativeScenario();
        /* TFS fix 45559,45560,45567,45568 start EP_NAV_ID__c deprecated, EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c and EP_Vendor_Type__c is replaced by EP_VendorType__c*/
        acct.EP_VendorType__c = 'None';
        /* TFS fix 45559,45560,45567,45568 end*/
        Test.startTest();
        Boolean result = localObj.isTransportOrThirdPartyVender(acct);
        Test.stopTest();
        System.AssertEquals(false,result);
    }

    static testMethod void setUniqueVendorId_test() {
        EP_Vendor localObj = new EP_Vendor();
        Account acct = EP_TestDataUtility.getVendor();
        Test.startTest();
        localObj.setUniqueVendorId(acct);
        Test.stopTest();
        /* TFS fix 45559,45560,45567,45568 start commented below code */
        //System.AssertEquals(true,acct.EP_Vendor_Unique_Id__c!=null);
        /* TFS fix 45559,45560,45567,45568 end*/
    }
    
    static testMethod void doBeforeInsertHandle_test() {
        EP_Vendor localObj = new EP_Vendor();
        Account acct = EP_TestDataUtility.getVendor();
        Test.startTest();
        localObj.doBeforeInsertHandle(acct);
        Test.stopTest();
        /* TFS fix 45559,45560,45567,45568 start commented below code */
        //System.AssertEquals(true,acct.EP_Vendor_Unique_Id__c!=null);
        /* TFS fix 45559,45560,45567,45568 end*/
    }

    static testMethod void doBeforeUpdateHandle_test() {
        EP_Vendor localObj = new EP_Vendor();
        Account vendorAccount = EP_TestDataUtility.getVendor();    
        Test.startTest();
        localObj.doBeforeUpdateHandle(vendorAccount,null);
        Test.stopTest();
        /* TFS fix 45559,45560,45567,45568 start commented below code */
        //System.AssertEquals(true,vendorAccount.EP_Vendor_Unique_Id__c!=null);
        /* TFS fix 45559,45560,45567,45568 End*/
    }
}