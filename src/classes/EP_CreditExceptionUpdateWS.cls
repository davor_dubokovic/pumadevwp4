/* 
    @Author <Accenture>
    @name <EP_CreditExceptionSyncNAVtoSFDC>
    @CreateDate <14/06/2016>
    @Description <This is apex RESTful WebService for Credit Exception Status Update from NAV to SFDC> 
    @Version <1.0>
*/
@RestResource(urlMapping='/v1/CREDEXCEPTIONSync/*')
global without sharing class EP_CreditExceptionUpdateWS{

    /**
    * @author <Accenture>
    * @name <creditExceptionSync>
    * @date <14/06/2016>
    * @description <This is the method that handles HttpPost request for Credit Exception>
    * @version <1.0>
    * @param none
    * @return void
    */
    @HttpPost
    global static void processCreditExceptionUpdate(){
        EP_GeneralUtility.Log('public','EP_CreditExceptionUpdateWS','processCreditExceptionUpdate');
        RestRequest request = RestContext.request;
        String requestBody = request.requestBody.toString();
        RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
        if(String.isNotBlank(requestBody)){
            EP_IntegrationService service = new EP_IntegrationService();
            string response = service.handleRequest(EP_Common_Constant.CREDIT_EXCEPTION_UPDATE,requestBody); 
            RestContext.response.responseBody = Blob.valueOf(response);
        }
    }
}