global with sharing class TrafiguraOrderProductsLookupImpl extends cscfga.ALookupSearch {

	private static final String ACCOUNT_ID_FIELD = 'Account ID';

	//global override List<Object> doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionId) {
	public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit) {
		String accid = searchFields.get(ACCOUNT_ID_FIELD);
		//accid = '0010k00000OhssjAAB';

		system.debug('TrafiguraOrderProductsLookupImpl searchFields = ' + searchFields);
		system.debug('TrafiguraOrderProductsLookupImpl accid = ' + accid);

		List<EP_Product_Option__c> productOptions = [select Price_List__c from EP_Product_Option__c where Sell_To__c = :accid];

		system.debug('TrafiguraOrderProductsLookupImpl productOptions = ' + productOptions);

		List<Id> pricebooks = new List<Id>(); //[SELECT id from Pricebook2 where ];

		for (EP_Product_Option__c prodOp : productOptions) {
			pricebooks.add(prodOp.Price_List__c);
		}

		system.debug('TrafiguraOrderProductsLookupImpl pricebooks = ' + pricebooks);

		List<PricebookEntry> products = [SELECT name, ProductCode, Product2.id, Product2.name, Product2.EP_Company__c from PricebookEntry where Pricebook2.Id IN :pricebooks];

		system.debug('TrafiguraOrderProductsLookupImpl products = ' + products);
		
		return products;
	}

	public override String getRequiredAttributes() {
    	return '["' + ACCOUNT_ID_FIELD + '"]';
    	//return '["Account ID","Product Basket ID","Customer"]';
  	}
}