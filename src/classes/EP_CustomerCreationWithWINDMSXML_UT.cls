@isTest
private class EP_CustomerCreationWithWINDMSXML_UT {

    @testSetup
    public static void init(){
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
    }
    /*static testMethod void init_testIf() {
        EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        account acc = new account(id=objAccDomain.localAccount.id);
        EP_TestDataUtility.createTestEP_Tank(acc);
        system.debug('KK--->'+objAccDomain.localAccount);
        EP_CustomerCreationWithWINDMSXML objLocal = new EP_CustomerCreationWithWINDMSXML();
        objLocal.recordId =objAccDomain.localAccount.ParentId;
        Test.startTest();
        objLocal.init();
        Test.stopTest();
        
        System.assertEquals( true,objLocal.lstAccount.size()>0); 
         
    }*/
    
    static testMethod void init_testElse() {
        EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        account acc = new account(id=objAccDomain.localAccount.id);
        EP_TestDataUtility.createTestEP_Tank(acc);
        system.debug('KK--->'+objAccDomain.localAccount);
        EP_CustomerCreationWithWINDMSXML objLocal = new EP_CustomerCreationWithWINDMSXML();
        objLocal.recordId =objAccDomain.localAccount.ParentId;
        Test.startTest();
        objLocal.init();
        Test.stopTest();
        
        System.assertEquals( true,objLocal.lstAccount.size()>0); 
         
    }
    
    static testMethod void createPayload_testPositive() {
        EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        account acc = new account(id=objAccDomain.localAccount.id);
        EP_TestDataUtility.createTestEP_Tank(acc);
        system.debug('KK--->'+objAccDomain.localAccount);
        EP_CustomerCreationWithWINDMSXML objLocal = new EP_CustomerCreationWithWINDMSXML();
        objLocal.recordId =objAccDomain.localAccount.ParentId;
        Test.startTest();
        objLocal.isEncryptionEnabled =true;
        objLocal.init();
        objLocal.createPayload();
        Test.stopTest(); 
        
        System.assertEquals( true,objLocal.lstAccount.size()>0); 
         
    }
    
    static testMethod void createPayload_testNegative() {
        EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        account acc = new account(id=objAccDomain.localAccount.id);
        EP_TestDataUtility.createTestEP_Tank(acc);
        system.debug('KK--->'+objAccDomain.localAccount);
        EP_CustomerCreationWithWINDMSXML objLocal = new EP_CustomerCreationWithWINDMSXML();
        objLocal.recordId =objAccDomain.localAccount.id;
        Test.startTest();
        objLocal.isEncryptionEnabled =true;
        objLocal.init();
        objLocal.createPayload();
        Test.stopTest(); 
        
        System.assertEquals( 'MSG',objLocal.MSGNode.getname()); 
         
    }
    
    static testMethod void bindAccountRecordsInXML_testPositive() {
        EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        account acc = new account(id=objAccDomain.localAccount.id);
        EP_TestDataUtility.createTestEP_Tank(acc);
        system.debug('KK--->'+objAccDomain.localAccount);
        EP_CustomerCreationWithWINDMSXML objLocal = new EP_CustomerCreationWithWINDMSXML();
        objLocal.recordId =objAccDomain.localAccount.ParentId;
        Test.startTest();
        objLocal.init();
        DOM.Document tempDoc = new DOM.Document(); 
        Dom.XMLNode customersNode = tempDoc.createRootElement(EP_AccountConstant.CUSTOMERS,null, null);
        objLocal.bindAccountRecordsInXML(customersNode); 
        Test.stopTest(); 
        
        System.assertEquals( true,customersNode.getChildren().size()>=0);
        
    }
    static testMethod void bindAccountRecordsInXML_testNegative() {
        //EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        //account acc = new account(id=objAccDomain.localAccount.id);
        account acc = EP_TestDataUtility.getShipTo();
        acc.EP_Is_Dummy__c = true;
        update acc;
        EP_Tank__c objTank = EP_TestDataUtility.createTestEP_Tank(acc);
        //system.debug('KK--->'+objAccDomain.localAccount);
        EP_CustomerCreationWithWINDMSXML objLocal = new EP_CustomerCreationWithWINDMSXML();
        objLocal.recordId =acc.id;
        DOM.Document tempDoc = new DOM.Document();
        Dom.XMLNode customersNode = tempDoc.createRootElement(EP_AccountConstant.CUSTOMERS,null, null);
        Test.startTest();
        objLocal.init();
        
        
        objLocal.bindAccountRecordsInXML(customersNode); 
        Test.stopTest(); 
        
        System.assertEquals( true,customersNode.getChildren().size()<=0);
        
    }
    
    static testMethod void bindTankRecordsInXML_testPositive() {
        EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        account acc = new account(id=objAccDomain.localAccount.id);
        EP_Tank__c objtank = EP_TestDataUtility.createTestEP_Tank(acc);
        list<EP_Tank__c> lstTank = new list<EP_Tank__c>();
        lstTank.add(objtank);
        system.debug('KK--->'+objAccDomain.localAccount); 
        EP_CustomerCreationWithWINDMSXML objLocal = new EP_CustomerCreationWithWINDMSXML();
        objLocal.recordId =objAccDomain.localAccount.ParentId;
        Test.startTest();

        DOM.Document tempDoc = new DOM.Document(); 
        Dom.XMLNode customersNode = tempDoc.createRootElement(EP_AccountConstant.CUSTOMERS,null, null);
        objLocal.bindTankRecordsInXML(customersNode,lstTank); 
        Test.stopTest(); 
        
        System.assertEquals( true,customersNode.getChildren().size()>=0);
        
    }
    static testMethod void bindTankRecordsInXML_testNegative() {
        //EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        //account acc = EP_TestDataUtility.getShipTo(); //new account(id=objAccDomain.localAccount.id);
        //EP_Tank__c objtank = EP_TestDataUtility.createTestEP_Tank(acc);
        list<EP_Tank__c> lstTank = new list<EP_Tank__c>();
        //system.debug('KK--->'+objAccDomain.localAccount);
        EP_CustomerCreationWithWINDMSXML objLocal = new EP_CustomerCreationWithWINDMSXML();
        //objLocal.recordId =acc.Id;
        Test.startTest();

        DOM.Document tempDoc = new DOM.Document(); 
        Dom.XMLNode customersNode = tempDoc.createRootElement(EP_AccountConstant.CUSTOMERS,null, null);
        objLocal.bindTankRecordsInXML(customersNode,lstTank); 
        Test.stopTest(); 
        
        System.assertEquals( true,customersNode.getChildren().size()<=1);
        
    }
    
   /* static testMethod void bindSupplyLocationRecordsInXML_testPositive() {
        EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        account acc = new account(id=objAccDomain.localAccount.id);
        EP_Tank__c objtank = EP_TestDataUtility.createTestEP_Tank(acc);
        list<EP_Tank__c> lstTank = new list<EP_Tank__c>();
        lstTank.add(objtank);
        system.debug('KK--->'+objAccDomain.localAccount); 
        EP_CustomerCreationWithWINDMSXML objLocal = new EP_CustomerCreationWithWINDMSXML();
        objLocal.recordId =objAccDomain.localAccount.ParentId;
        Test.startTest();

        DOM.Document tempDoc = new DOM.Document(); 
        Dom.XMLNode customersNode = tempDoc.createRootElement(EP_AccountConstant.CUSTOMERS,null, null);
        objLocal.bindSupplyLocationRecordsInXML(customersNode,lstTank); 
        Test.stopTest(); 
        
        System.assertEquals( true,customersNode.getChildren().size()>=0);
        
    }
    static testMethod void bindSupplyLocationRecordsInXML_testNegative() {
        EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        account acc = new account(id=objAccDomain.localAccount.id);
        Stock_Holding_Locations1__r objStockHolding= EP_TestDataUtility.createTestEP_Tank(acc);
        list<EP_Tank__c> lstTank = new list<EP_Tank__c>();
        system.debug('KK--->'+objAccDomain.localAccount);
        EP_CustomerCreationWithWINDMSXML objLocal = new EP_CustomerCreationWithWINDMSXML();
        objLocal.recordId =objAccDomain.localAccount.ParentId;
        Test.startTest();

        DOM.Document tempDoc = new DOM.Document(); 
        Dom.XMLNode customersNode = tempDoc.createRootElement(EP_AccountConstant.CUSTOMERS,null, null);
        objLocal.bindSupplyLocationRecordsInXML(customersNode,lstTank); 
        Test.stopTest(); 
        
        System.assertEquals( true,customersNode.getChildren().size()<=0);
        
    }*/
}