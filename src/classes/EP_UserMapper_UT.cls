@isTest
public class EP_UserMapper_UT{

     static testMethod void getRecordsByIds_test() {
        EP_UserMapper localObj = new EP_UserMapper();
        User userObj = [SELECT Id FROM User LIMIT 1];
        Set<id> setUser = new Set<Id>{userObj.id };
        Test.startTest();
        list<User> result = localObj.getRecordsByIds(setUser);
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }  
    
    static testMethod void getUserProfileId_testPositive() {
        EP_UserMapper localObj = new EP_UserMapper();
        User userObj = [SELECT Id,profileid FROM User LIMIT 1];
        Test.startTest();
        string result = localObj.getUserProfileName(userObj.id);
        Test.stopTest();
        profile objProfile = [select name from profile where id=:userObj.profileid limit 1];
        System.AssertEquals(objProfile.name,result);
    }  
    
    static testMethod void getUserProfileId_testNegative() {
        EP_UserMapper localObj = new EP_UserMapper();
        User userObj = [SELECT Id,profileid FROM User LIMIT 1];
        Test.startTest();
		try{
        	string result = localObj.getUserProfileName(userObj.profileid);
		}catch(exception ex){
			system.debug('ll-->'+ex.getmessage());
    		System.AssertEquals(true,ex.getmessage().contains('List index out of bounds'));	
    	}
        Test.stopTest();

    } 
    // Code changes for #45436
    static testMethod void getUser_TestPositive() {
        EP_UserMapper localObj = new EP_UserMapper();
        map<Id,user> usersProfileMap = new map<Id,user>();
        User userObj = [SELECT Id,profileid, Profile.Name FROM User LIMIT 1];
        Test.startTest();
			usersProfileMap = localObj.getUsers(new set<id>{userObj.id});
        Test.stopTest();
		System.AssertEquals(usersProfileMap.get(userObj.id).Profile.Name,userObj.Profile.Name);
    } 
}