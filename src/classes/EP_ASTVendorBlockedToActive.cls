public class EP_ASTVendorBlockedToActive extends EP_AccountStateTransition {
    public EP_ASTVendorBlockedToActive() {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorBlockedToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorBlockedToActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
		EP_GeneralUtility.Log('Public','EP_ASTVendorBlockedToActive','isGuardCondition');
       	return true;
    }

}