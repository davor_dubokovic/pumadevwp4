/* 
   @Author          Accenture
   @name            EP_CustomerUpdateStub
   @CreateDate      03/10/2017
   @Description     Stub class for customer update inbound class
   @Version         1.0
*/
public class EP_CustomerUpdateStub {
    public MSG MSG;
    public EP_CustomerUpdateStub(){
        MSG = new MSG();
    }
    
    public class MSG {
        public EP_MessageHeader HeaderCommon;
        public payload payload;
        public String StatusPayload;    //StatusPayload
        public MSG(){
            HeaderCommon = new EP_MessageHeader();
            payload = new payload();
        }
    }
    public class payload {
        public any0 any0;
        public payload(){
            any0 = new any0();
        }
    }
    public class any0{
        public customers customers;
        public custBanks custBanks;
        public shipToAddresses shipToAddresses;//L4 #45361 Code changes
        public any0(){
            customers = new customers();
            custBanks = new custBanks();
            shipToAddresses = new shipToAddresses();//L4 #45361 Code changes
        }
    }
    public class customers {
        public List<customer> customer;
        public customers(){
            customer = new List<customer>();
        }
    }
    
    public class customer {
        public Account SFAccount;
        /**L4_45352_start**/
        Public String financeSettingReview;
        Public String creditSettingReview;
        /**L4_45352_end**/
        public string errorDescription;
        public String seqId;
        public cust_identifier identifier; 
        //L4 #45361 Code changes Start
        public String billToCustId; 
        //L4 #45361 Code changes End
        public String creditHoldingId;//L4 #67333
        public String pmtMthdId;    //LCL_TRF
        public String pmtCrdLmt;
        public String pmtTerm;   //PP L4# 67333
        public String pckgdPymntTerm;   //PP
        public String billMthd;
        public String billFrqncy;
        public String blocked;
        public String reasonForblocked;
        public String reasonCode;   //not mapped and not in use
        public String manualInvcingAllowed;
        public String isVatExempted;
        public String deferInvoice;
        public String deferUpperLmt;
        public String allowRlsOnPymntProof;
        public String activeStatus; //Added New field - L4 #45361 Code changes
        public acntStmntFrqncy acntStmntFrqncy;
        public String combinedInvoicing;
        public String billConsolBasis;
        public String stmntTypAll;
        public String stmntTypOpen;
        public String bpayRefCode;
        public String creditLimitApprovalStatus;
        public String rejectedCreditLimtAmount; //10,000.00
        public custBanks custBanks; //
        public String clientId;
        //L4 #45313 Changes Start
        public String aprvdCreditLimtAmtCusCur;
        //L4 #45313 Changes End
        //L4 #45361 Code changes Start
        public shipToAddresses shipToAddresses;
        public String vatRegNr;//EP_Company_Tax_Number__c
		public String incomeTaxNr;//EP_Income_Tax_Number__c
		public String prfrdBankAcc;//EP_Preferred_Bank_Account__c lookup BankAccount
		public String reprcngOnAvgPrdcPrcng;//EP_Repricing_Based_On__c
		public String cnsldtdInvcPrntOptn;//EP_Consolidated_Invoice_Print_Options__c
		public String reasonForStatusChng;
		public String billBasis;//EP_Billing_Basis__c
		public String priceConsolBasis;//EP_Price_Consolidation_Basis__c
		public String exciseDuty;//EP_Excise_duty__c
		public String invcDueDtBsdOn;//EP_Invoice_Due_Date_Based_on__c
		
        public customer(){
            custBanks = new custBanks();
            identifier = new cust_identifier(); 
            acntStmntFrqncy = new acntStmntFrqncy();
            shipToAddresses = new shipToAddresses();//L4 #45361 Code changes
        }
        //L4 #45361 Code changes End
    }
    public class banks_identifier {
        public String custId;   //custId
        public String bankAccId;    //bankAccId
        //BUG-73892- Adding code tag in identifier as per the updated XML
        public String code;
        //BUG-73892
    }
    public class cust_identifier {
        public String custId;   //custId
        public String entrprsId;//L4 #45361 Code changes
    }
    public class acntStmntFrqncy {
        public String daily;    //YES
        public String weekly;   //YES
        public String fortnightly;  //YES
        public String monthly;  //YES
    }
    public class custBanks {
        public List<custBank> custBank;
        public custBanks(){
            custBank = new List<custBank>();
        }
    }
    public class custBank {
        //L4 #58828 & #58829 Code Changes Start
        public EP_Bank_Account__c SFBankAccount;
        public String clientId; //clientId
        public banks_identifier identifier;
        public String isActiveStatus;//EP_Bank_Account_Status__c
         
        public String seqId;  //EP_Source_Seq_Id__c
        public String name;//EP_Bank_Name__c
        public String name2;//EP_Bank_Name_2__c
        public String address;
        public String postCode;
        public String city;
        public String phone;
        public String fax;
        public String email;
        public String bankBranchId;//EP_Bank_Branch_No__c
        public String bankAccountNo;//EP_Bank_Account_No__c
        public String transitNo;
        public String currencyId;
        public String cntryCode;
        public String county;
        public String website;
        public String iBAN;
        public String swiftCode;
        public String bankClearCode;
        public String bankClearingStd;
        public String address2;
        public String contactName;
        public String telexNo;
        public String telexAnsBck;
        public String giroAccNo;
        public String langCode;
        public String corrBankName;
        public String corrBankCntryCode;
        public String corrBankSwiftCode;
        public String corrBankClearCode;
        //public String code;// Code tag moved in identifier as per the updated XML for BUG-73892
        public String textKey;
        public String routeNo;
        public String versionNr;
        //L4 #58828 & #58829 Code Changes End
        public custBank(){
            identifier = new banks_identifier();
        }
    }
    //L4 #45361 Code changes Start
    public class shipToAddresses {
        public List<shipToAddress> shipToAddress;
        public shipToAddresses(){
            shipToAddress = new List<shipToAddress>();
        }
    }
    public class shipToAddress {
        public Account SFShipToObject;
        public String seqId;    //seqId
        public shipTo_identifier identifier;
        public String duty; //Excise Payable
        public String locationId;   //locationId
        public shipToAddress(){
            identifier = new shipTo_identifier();
        }
    }
    public class shipTo_identifier {
        public String shipToId; //shipToId
        public String entrprsId;    //entrprsId
    }
    //L4 #45361 Code changes End
}