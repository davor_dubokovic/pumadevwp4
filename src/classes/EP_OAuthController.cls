/* 
   @Author <Accenture>
   @name <EP_OAuthController>
   @CreateDate <02/01/2018>
   @Description <This class is used to get access token for OAuth authentication process>
   @Version <1.0>
*/
public virtual class EP_OAuthController {
    @TestVisible public String access_token;
    @TestVisible public String refresh_token;
    @TestVisible public String expires_on;
    @TestVisible private Boolean isCallback;

    public String validateResult;

    public String application_name;
   
    public String PBIaccess_token { 
        get {
            Cookie pbi_AccessToken = ApexPages.currentPage().getCookies().get(EP_Common_Constant.ACCESSTOKEN);
            if(pbi_AccessToken == null)
                this.access_token = EP_Common_Constant.BLANK;
            else
                this.access_token = pbi_AccessToken.getValue();
            
            return this.access_token;
        } 
        set;
        }
        
        
    public String PBIrefresh_token { 
        get {
            Cookie pbi_RefreshToken = ApexPages.currentPage().getCookies().get(EP_Common_Constant.REFRESHTOKEN);
            if(pbi_RefreshToken == null)
                this.refresh_token= EP_Common_Constant.BLANK;
            else
                this.refresh_token= pbi_RefreshToken.getValue();
            
            return this.refresh_token;
        } 
        set;
        }
        
    public String PBIexpires_on { 
        get {
            Cookie pbi_ExpiresOn= ApexPages.currentPage().getCookies().get(EP_Common_Constant.EXPIRES_ON);
            if(pbi_ExpiresOn== null)
                this.expires_on= EP_Common_Constant.BLANK;
            else
                this.expires_on= pbi_ExpiresOn.getValue();
            
            return this.expires_on;
        } 
        set;
        }
        
        
    public String reportId{ 
        get {
            return EP_OAuthApp_pbi__c.getValues(this.application_name).EP_Report_Id__c;
        } 
        set;
        }  
        
        public String groupId{ 
        get {
            return EP_OAuthApp_pbi__c.getValues(this.application_name).EP_Group_Id__c;
        } 
        set;
        }        

    
    /**
    * @author       Accenture
    * @name         getHasToken
    * @date         02/01/2018
    * @description  This method checks if access token is retrieved
    * @param        NA
    * @return       Boolean
    */  
    public Boolean getHasToken() {
          Boolean check;
         if(PBIaccess_token== null)
              check = false;
           else 
              check = PBIaccess_token.length()>0;
          return check;
    }


    
    /**
    * @author       Accenture
    * @name         OAuthResult
    * @date         02/01/2018
    * @description  Inner class for successful OAUTH Json Result
    * @param        NA
    * @return       NA
    */ 
    public class OAuthResult {
        /** The access token */
        public String access_token {get; set;}

        /** The refresh token */
        public String refresh_token {get; set;}
        
        /** The token expiry date*/
        public String expires_on {get;set;}
    }
    
    /**
    * @author       Accenture
    * @name         validateCode
    * @date         02/01/2018
    * @description  This method validates the oauth code
    * @param        code to validate,URL to redirect after successful validation
    * @return       OAuthResult
    */ 
    public OAuthResult validateCode(String code, String redirect_uri) {
        EP_GeneralUtility.Log('public','EP_OAuthController','validateCode');
        String client_id = EP_OAuthApp_pbi__c.getValues(this.application_name).EP_Client_Id__c;
        String client_secret = EP_OAuthApp_pbi__c.getValues(this.application_name).EP_Client_Secret__c;
        List<String> urlParams = new List<String> {
            EP_Common_Constant.GRANT_TYPE+EP_Common_Constant.EQUAL+EP_Common_Constant.AUTHORIZATION_CODE,
            EP_Common_Constant.CODE+EP_Common_Constant.EQUAL+ EncodingUtil.urlEncode(code, EP_Common_Constant.UTF),
            EP_Common_Constant.PBI_CLIENTID+EP_Common_Constant.EQUAL+ EncodingUtil.urlEncode(client_id, EP_Common_Constant.UTF), //6a366c74-5101-4368-b523-6f5b241c8407
            EP_Common_Constant.CLIENTSECRET+EP_Common_Constant.EQUAL + EncodingUtil.urlEncode(client_secret, EP_Common_Constant.UTF),//K99lwvGO/qwUuNKTqhd7Kh2BttsasY59x3czuvixbgw=
            EP_Common_Constant.REDIRECTURI+ EP_Common_Constant.EQUAL + EncodingUtil.urlEncode(redirect_uri, EP_Common_Constant.UTF)
        };

        
        return makeCallout(urlParams);
    }
    
    
      
    /**
    * @author       Accenture
    * @name         refreshAccessToken
    * @date         02/01/2018
    * @description  This method Gets new access token through refresh token
    * @param        pagereference
    * @return       PageReference
    */ 
    public PageReference refreshAccessToken(PageReference location){
        EP_GeneralUtility.Log('public','EP_OAuthController','refreshAccessToken');
        Cookie accessTokenCookie = new Cookie(EP_Common_Constant.ACCESSTOKEN, EP_Common_Constant.BLANK,null,0,false);
        Cookie expiresOn = new Cookie(EP_Common_Constant.EXPIRES_ON ,EP_Common_Constant.BLANK,null,0,false);
            
        ApexPages.currentPage().setCookies(new Cookie[]{accessTokenCookie,expiresOn}); 
            
        
        String client_id = EP_OAuthApp_pbi__c.getValues(this.application_name).EP_Client_Id__c;
        String client_secret = EP_OAuthApp_pbi__c.getValues(this.application_name).EP_Client_Secret__c;
        String resource_URI = EP_OAuthApp_pbi__c.getValues(this.application_name).EP_Resource_URI__c;
        String refreshToken = this.PBIrefresh_token;
        List<String> urlParams = new List<String> {
            EP_Common_Constant.GRANT_TYPE+EP_Common_Constant.EQUAL+EP_Common_Constant.REFRESH_TOKEN,
            EP_Common_Constant.REFRESH_TOKEN+EP_Common_Constant.EQUAL + EncodingUtil.urlEncode(refreshToken, EP_Common_Constant.UTF),
            EP_Common_Constant.PBI_CLIENTID+EP_Common_Constant.EQUAL + EncodingUtil.urlEncode(client_id, EP_Common_Constant.UTF),
            EP_Common_Constant.CLIENTSECRET+EP_Common_Constant.EQUAL + EncodingUtil.urlEncode(client_secret, EP_Common_Constant.UTF),
            EP_Common_Constant.RESOURCE+EP_Common_Constant.EQUAL + EncodingUtil.urlEncode(resource_URI, EP_Common_Constant.UTF)
        };
        
       
        OAuthResult result = makeCallout(urlParams);
        Cookie refreshTokenCookie = new Cookie(EP_Common_Constant.REFRESHTOKEN, result.refresh_token,null,-1,false);        
        accessTokenCookie = new Cookie(EP_Common_Constant.ACCESSTOKEN, result.access_token,null,-1,false);
        expiresOn = new Cookie(EP_Common_Constant.EXPIRES_ON,result.expires_on,null,-1,false);
        
        ApexPages.currentPage().setCookies(new Cookie[]{accessTokenCookie,refreshTokenCookie,expiresOn});   
            
        return location;
    }
    
       
    /**
    * @author       Accenture
    * @name         makeCallout
    * @date         16/01/2018
    * @description  This method makes the https callout to the endpoint
    * @param        param list
    * @return       OAuthResult
    */ 
    private OAuthResult makeCallout(List<String> urlParams){
       
        EP_GeneralUtility.Log('private','EP_OAuthController','makeCallout');
        String access_token_url = EP_OAuthApp_pbi__c.getValues(this.application_name).EP_Access_Token_URL__c;
        Http h = new Http();

        HttpRequest req = new HttpRequest();
        req.setEndpoint(access_token_url);
        req.setMethod(EP_Common_Constant.POST);
        req.setHeader(EP_Common_Constant.CONTENTTYPE, EP_Common_Constant.APPLICATIONENCODED);
        req.setHeader(EP_Common_Constant.ACCEPT, EP_Common_Constant.APPLICATIONJSON );
        String body = String.join(urlParams,EP_Common_Constant.AMPERSAND);
        req.setBody(body);

        HttpResponse res = h.send(req);

        OAuthResult result = (OAuthResult)(JSON.deserialize(res.getBody(), OAuthResult.class));
        return result;
    }

    /**
    * @author       Accenture
    * @name         EP_OAuthController
    * @date         02/01/2018
    * @description  Constructor
    * @param        NA
    * @return       NA
    */ 
    public EP_OAuthController() {
    EP_GeneralUtility.Log('public','EP_OAuthController','EP_OAuthController');
    this.isCallback = ApexPages.currentPage().getParameters().containsKey(EP_Common_Constant.CODE);              

        if (EP_OAuthApp_pbi__c.getValues(this.application_name) != null) {
            Cookie pbi_AccessToken = ApexPages.currentPage().getCookies().get(EP_Common_Constant.ACCESSTOKEN);
            Cookie pbi_RefreshToken = ApexPages.currentPage().getCookies().get(EP_Common_Constant.REFRESHTOKEN);
            Cookie pbi_ExpiresOn = ApexPages.currentPage().getCookies().get(EP_Common_Constant.EXPIRES_ON);
             access_token = pbi_AccessToken==null?EP_Common_Constant.BLANK:pbi_AccessToken.getValue();
             refresh_token = pbi_RefreshToken==null?EP_Common_Constant.BLANK:pbi_RefreshToken.getValue();
             expires_on = pbi_ExpiresOn==null?EP_Common_Constant.BLANK:pbi_ExpiresOn.getValue();
        }
    }

    /**
    * @author       Accenture
    * @name         getAuthUrl
    * @date         02/01/2018
    * @description  It gets the authorrization URL
    * @param        NA
    * @return       Auth Url
    */ 
    
    public String getAuthUrl() {
        EP_GeneralUtility.Log('public','EP_OAuthController','getAuthUrl');
        Map<String, String> urlParams = new Map<String, String> {
            EP_Common_Constant.PBI_CLIENTID => EP_OAuthApp_pbi__c.getValues(this.application_name).EP_Client_Id__c,
            EP_Common_Constant.REDIRECTURI => getPageUrl(),
            EP_Common_Constant.RESOURCE => EP_OAuthApp_pbi__c.getValues(this.application_name).EP_Resource_URI__c,
            EP_Common_Constant.RESPONSETYPE => EP_Common_Constant.CODE
        };
        String auth_url = EP_OAuthApp_pbi__c.getValues(this.application_name).EP_Authorization_URL__c;

        PageReference ref = new PageReference(auth_url);
        ref.getParameters().putAll(urlParams);
        system.debug('AUTH URL ==='+ref.getUrl());
        return ref.getUrl();
    }
    
    /**
    * @author       Accenture
    * @name         getPageUrl
    * @date         02/01/2018
    * @description  It gets the page URL
    * @param        NA
    * @return       current Page Url
    */ 
    @testVisible
    private String getPageUrl() {
        EP_GeneralUtility.Log('private','EP_OAuthController','getPageUrl');
        String host = ApexPages.currentPage().getHeaders().get(EP_Common_Constant.HOST);
        String path = ApexPages.currentPage().getUrl().split(EP_Common_Constant.SPLITSTRING).get(0);

        return EP_Common_Constant.HTTPS + host + path;
    }

    /**
    * @author       Accenture
    * @name         redirectOnCallback
    * @date         02/01/2018
    * @description  This method validates the callback code and generates the access and refresh tokens
    * @param        Rediect URl after success
    * @return       PageReference
    */
       public PageReference redirectOnCallback(PageReference location) {
       EP_GeneralUtility.Log('public','EP_OAuthController','redirectOnCallback');
        if (this.isCallback)  {
            String code = ApexPages.currentPage().getParameters().get(EP_Common_Constant.CODE);
            OAuthResult result = validateCode(code, this.getPageUrl());
                                
            //Store accesstoken in cookie
            Cookie accessTokenCookie = new Cookie(EP_Common_Constant.ACCESSTOKEN, result.access_token,null,-1,false);
            Cookie refreshToken = new Cookie(EP_Common_Constant.REFRESHTOKEN, result.refresh_token,null,-1,false);
            Cookie expiresOn = new Cookie(EP_Common_Constant.EXPIRES_ON,result.expires_on,null,-1,false);

            ApexPages.currentPage().setCookies(new Cookie[]{accessTokenCookie,refreshToken,expiresOn}); 
            
            return location;
        }
        return null;
    }
}