@isTest
private class CarryOutPricingCallTest {
	class Order_Submission_Mock implements HttpCalloutMock {    

		public Order_Submission_Mock(Boolean pass) {
			this.PassTest = pass;
		}

		public Boolean PassTest {get; private set;}    

		public HTTPResponse respond(HTTPRequest req) {
			HTTPResponse retVal = new HTTPResponse();
			retVal.setStatusCode(this.PassTest ? 202 : 404);
			retVal.setBody(JSON.serialize(new List<String> { 'A', 'B' } ));
			return retVal;
		}

	}

	@isTest static void test_method_one() {
		Test.setMock(HttpCalloutMock.class, new Order_Submission_Mock(true));

		Opportunity opp = TQTestUtils.buildOpportunity();
        
        
        csord__Order_Request__c morc = new csord__Order_Request__c();
        morc.Name = 'Test MORC';
        morc.csord__Module_Name__c = 'Test Module';
        morc.csord__Module_Version__c = 'Test Version';
        morc.csord__Process_Status__c = 'Requested';
        insert morc;

        csord__Order__c moc = new csord__Order__c();
        moc.Name = 'Test MOC';
        moc.csord__Identification__c = 'testOrder0';
        moc.csord__Order_Request__c = morc.Id;
        moc.csord__Status__c = 'Active';
        moc.csord__Status2__c = 'Active';
        moc.csordtelcoa__Opportunity__c	= opp.Id;
        insert moc;
        
        csord__Subscription__c msc = new csord__Subscription__c();
        msc.Name = 'Test MSC';
        msc.csord__Identification__c = 'testSubscription0';
        msc.csord__Order_Request__c = morc.Id;
        msc.csord__Order__c = moc.Id;
        insert msc;
  
		csord__Order_Line_Item__c oli = new csord__Order_Line_Item__c();
		oli.Quantity__c = 10;
		oli.EP_Company_Code__c = 'CA';
		oli.EP_Product_Code__c = 'CB';
		oli.csord__Order__c = moc.Id;
		oli.csord__Identification__c = 'testOLI0';
  
        insert new List<csord__Order_Line_Item__c> { oli };
        
        CSPOFA__Orchestration_Process_Template__c moptc = new CSPOFA__Orchestration_Process_Template__c();
        moptc.Name = 'Test MOPTC';
        insert moptc;
        
        CSPOFA__Orchestration_Process__c mopc = new CSPOFA__Orchestration_Process__c();
        mopc.Name = 'Test MOPC';
        mopc.CSPOFA__Orchestration_Process_Template__c = moptc.Id;
        mopc.Order__c = moc.Id;
        insert mopc;
        
        CSPOFA__Orchestration_Step__c mosc = new CSPOFA__Orchestration_Step__c();
        mosc.Name = 'Test MOSC';
        mosc.CSPOFA__Orchestration_Process__c = mopc.Id;
        insert mosc;
        
        list<CSPOFA__Orchestration_Step__c> moscList = new list<CSPOFA__Orchestration_Step__c>();
        moscList.add(mosc);     
        //Run test
        test.startTest();
        
       	CarryOutPricingCall plic = new CarryOutPricingCall();

       	plic.process(moscList);
        
        test.stopTest();
	}
}