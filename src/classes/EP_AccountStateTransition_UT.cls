@isTest
public class EP_AccountStateTransition_UT
{   
    static final string EVENT_NAME = '04-AccountSet-upTo04-AccountSet-up';
    static final string INVALID_EVENT_NAME = '06-BlockedToNew';
    
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    
    static testMethod void isTransitionPossible_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASAccountSetupDomainObject();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_AccountStateTransition ast = new EP_AccountStateTransition ();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(false, result);
    }
    
    static testMethod void isRegisteredForEvent_positive_test() {
          EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASAccountSetupDomainObject();
          EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
          EP_CustomSettingMapper customSettingMapper = new EP_CustomSettingMapper();
          EP_AccountStateTransition ast = new EP_AccountStateTransition ();
          ast.setAccountContext(obj,ae);
          Test.startTest();
          boolean result = ast.isRegisteredForEvent();
          Test.stopTest();
          System.AssertEquals(true,result);
    }
    static testMethod void isRegisteredForEvent_negative_test() {
          EP_AccountDomainObject obj = EP_TestDataUtility.getAccountNegativeTestScenario();
          EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
          EP_AccountStateTransition ast = new EP_AccountStateTransition ();
          ast.setAccountContext(obj,ae);
          Test.startTest();
          
          boolean result = ast.isRegisteredForEvent();
          Test.stopTest();
          System.AssertEquals(false,result);
    }
    static testMethod void isGuardCondition_test() {
          EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASAccountSetupDomainObject();
          EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
          EP_AccountStateTransition ast = new EP_AccountStateTransition ();
          ast.setAccountContext(obj,ae);
          Test.startTest();
          boolean result = ast.isGuardCondition();
          Test.stopTest();
          System.AssertEquals(false,result);
    }
    
    
    static testMethod void setAccountContext_test() {
          EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASAccountSetupDomainObject();
          EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
          EP_AccountStateTransition ast = new EP_AccountStateTransition ();
          Test.startTest();
          ast.setAccountContext(obj,ae);
          Test.stopTest();
          System.assert(ast.account != null);
          System.assert(ast.accountEvent != null);
    }
    
    static testMethod void setPriority_test() {
          EP_AccountStateTransition ast = new EP_AccountStateTransition ();
          Integer priorityInt = 1;
          Test.startTest();
          ast.setPriority(priorityInt );
          Test.stopTest();
          System.AssertEquals(priorityInt ,ast.getPriority());
    }
    
    static testMethod void getPriority_test() {
          EP_AccountStateTransition ast = new EP_AccountStateTransition ();
          Integer priorityInt = 1;
          ast.setPriority(priorityInt );
          Test.startTest();
          Integer result = ast.getPriority();
          Test.stopTest();
          System.AssertEquals(priorityInt ,result);
    }
    
    static testMethod void doOnExit_test() {
          EP_AccountStateTransition ast = new EP_AccountStateTransition ();
          Test.startTest();
          ast.doOnExit();
          Test.stopTest();
          //assert not needed
          system.assert(true);
    }
    
}