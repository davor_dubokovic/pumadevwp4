/**
 * @Author <Accenture>
 * @name <EP_AttachmentNotificationService_UT>
 * @CreateDate <01/18/2018>
 * @Description <This class contains unit tests for validating the behavior of EP_AttachmentNotificationService Apex class >
 * @Version <1.0>
 */
@isTest
public class EP_AttachmentNotificationService_UT {
	@TestSetup
	static void setupData(){
		Folder foldr = EP_AccountNotificationTestDataUtility.getFolder(system.label.EP_Folder_Name);
		EP_AccountNotificationTestDataUtility.insertEmailTemplate(foldr.id);
	}
	
	private static list<EP_AttachmentNotificationService.AttachmentWrapper> createBasicData() {
    	EmailTemplate emailObj = [select id from EmailTemplate where developerName = 'UserTemplate'];		
  		csord__Order__c ordObj = EP_TestDataUtility.getCSOrder();
  		Attachment attach = EP_AccountNotificationTestDataUtility.createAttachment(ordObj.id);
        
        EP_Notification_Type__c typeObj = EP_AccountNotificationTestDataUtility.createNotificationType(emailObj.Id);
        typeObj.EP_Notification_Code__c = 'QUOTE';
		update typeObj;
		
		EP_Notification_Account_Settings__c notObj = EP_AccountNotificationTestDataUtility.createAccountNotification(ordObj.EP_Sell_To__c, emailObj.Id, null, null,typeObj.Id);
        
        list<EP_AttachmentNotificationService.attachmentWrapper> attachmentWrapperList = new list <EP_AttachmentNotificationService.attachmentWrapper>();
        attachmentWrapperList.add(new EP_AttachmentNotificationService.attachmentWrapper(attach, ordObj.EP_Sell_To__c, EP_Common_Constant.SALESQUOTES_TITLE));
		return attachmentWrapperList;
    }
    
    private static testMethod void sendAttachmentNotification_PositiveTest() {
    	list<EP_AttachmentNotificationService.AttachmentWrapper> attWrp = createBasicData();
    	
    	Test.startTest();
			EP_AttachmentNotificationService.sendAttachmentNotification(attWrp);
		Test.stopTest();
    	
    	List<EP_Notification_Account_Settings__c> notList = [select id from EP_Notification_Account_Settings__c limit 1];
		System.AssertEquals(true,notList.size() > 0);
    }
    
    private static testMethod void sendAttachmentNotification_NegativeTest() {
		list<EP_AttachmentNotificationService.AttachmentWrapper> attWrpList = createBasicData();
		EP_AttachmentNotificationService.AttachmentWrapper attWrp = attWrpList.get(0);
		attWrp.notificationClass = null;
		attWrpList = new list<EP_AttachmentNotificationService.AttachmentWrapper>();
		attWrpList.add(attWrp);
    	
    	Test.startTest();
			EP_AttachmentNotificationService.sendAttachmentNotification(attWrpList);
		Test.stopTest();
    	
    	List<EventBusSubscriber > notList = [select id from EventBusSubscriber limit 1];
		System.AssertEquals(0,notList.size());
    }
    
    private static testMethod void sendNotification_PositiveTest() {
    	list<EP_AttachmentNotificationService.AttachmentWrapper> attWrp = createBasicData();
    	string className = EP_Common_Constant.CLASS_NAME_PREFIX+attWrp.get(0).notificationClass+EP_Common_Constant.EVENT_CLASS_NAME_SUFFIX;
    	
    	Test.startTest();
			EP_Event_Subscription subscribeEventObj = EP_AttachmentNotificationService.sendNotification(attWrp.get(0).attachmentObject,attWrp.get(0).sellToId, className);
		Test.stopTest();
    	
		system.assertEquals(true,subscribeEventObj.saveResultobj.isSuccess());
    }
    
    private static testMethod void sendNotification_NegativeTest() {
    	list<EP_AttachmentNotificationService.AttachmentWrapper> attWrp = createBasicData();
    	string className = EP_Common_Constant.CLASS_NAME_PREFIX+attWrp.get(0).notificationClass+EP_Common_Constant.EVENT_CLASS_NAME_SUFFIX;
    	Attachment attch = attWrp.get(0).attachmentObject;
    	
    	Test.startTest();
			EP_Event_Subscription subscribeEventObj = EP_AttachmentNotificationService.sendNotification(attch,null, className);
		Test.stopTest();
    	
		system.assertEquals(null,subscribeEventObj.saveResultobj);
    }
}