@isTest
public class EP_CustomerUpdateHelper_UT
{
    static String PREVIOUS_VALUE = 'previousValue';
    static String FREQUENCY = EP_Common_Constant.YES;
    static String VALUE_TO_UPDATE = 'valueToUpdate';
    static String INVALID_CREDIT_STATUS = 'Blocked';

    @testSetup static void init() {
      List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
    }
    
    static testMethod void setCustomerAttributes_test() {
        List<EP_CustomerUpdateStub.customer> customers = new List<EP_CustomerUpdateStub.customer>();
        EP_CustomerUpdateStub.customer cust = EP_TestDataUtility.createInboundCustomer();
        cust.SFAccount = new Account(); 
        customers.add(cust);
        Test.startTest();
        EP_CustomerUpdateHelper.setCustomerAttributes(customers);
        Test.stopTest();
       // System.assertEquals(true,cust.SFAccount.BillingCity!=null);
        //System.assertEquals(true,cust.SFAccount.Phone!=null);
    }
    
    static testMethod void setBankAttributes_test() {
        EP_CustomerUpdateStub.customer cust = EP_TestDataUtility.createInboundCustomer();
        cust.SFAccount = new Account(); 
        List<EP_CustomerUpdateStub.custBank> banks = new List<EP_CustomerUpdateStub.custBank>();
        EP_CustomerUpdateStub.custBank custBank = cust.custBanks.custBank[0];
        banks.add(custBank);
        Test.startTest();
        EP_CustomerUpdateHelper.setBankAttributes(banks);
        Test.stopTest();
        System.assertEquals(true,custBank.SFBankAccount.EP_Source_Seq_Id__c== custBank.seqId);
    }
    
    static testMethod void setAccountIdOfBankAccount_test() {
        Map<String,Account> mapCompositeIDAccounts = new Map<String, Account>();
        Map<String,EP_Bank_Account__c> mapBankAccounts = new Map<String, EP_Bank_Account__c>();
        Account account = EP_TestDataUtility.getSellToPositiveScenario();
        mapCompositeIDAccounts.put(account.EP_Composite_Id__c.toUpperCase(), account);
        Account accObject = [select Id,EP_Account_Company_Name__c,AccountNumber from Account where Id =: account.Id];
        EP_Bank_Account__c bankAccount = [select id,EP_NAV_Bank_Account_Code__c,Name,EP_Account__r.EP_Puma_Company_Code__c from EP_Bank_Account__c where EP_Account__c =:account.Id];
        mapBankAccounts.put(bankAccount.EP_NAV_Bank_Account_Code__c.toUpperCase(),bankAccount);
        List<EP_CustomerUpdateStub.customer> customers = EP_TestDataUtility.createInboundCustomers();
        EP_CustomerUpdateStub.customer customer = customers[0];
        customer.SFAccount = new Account(); 
        customer.clientId = string.valueOf(accObject.EP_Account_Company_Name__c).toUpperCase();
        customer.identifier.custId = string.valueOf(accObject.AccountNumber);    
        List<EP_CustomerUpdateStub.custBank> custBanks = customer.custBanks.custBank;
        EP_CustomerUpdateStub.custBank custBank = custBanks[0];
        custBank.identifier.custId = string.valueOf(accObject.AccountNumber);
        custBank.identifier.bankAccId = bankAccount.Name;
        custBank.clientId= bankAccount.EP_Account__r.EP_Puma_Company_Code__c.toUpperCase();
        Test.startTest();
        EP_CustomerUpdateHelper.setAccountIdOfBankAccount(custBank,mapBankAccounts,mapCompositeIDAccounts);
        Test.stopTest();
        System.assertEquals(true,bankAccount.id==custBank.SFBankAccount.Id);
    }
    
    static testMethod void getAccountIdFromBankAccounts_test() {
        EP_CustomerUpdateStub.custBank bank = EP_TestDataUtility.createInboundCustomerBank();
        List<EP_CustomerUpdateStub.custBank> banks = new List<EP_CustomerUpdateStub.custBank>();
        banks.add(bank);
        Test.startTest();
        set<string> result = EP_CustomerUpdateHelper.getAccountIdFromBankAccounts(banks);
        Test.stopTest();
        System.assertEquals(true,result !=null);
    }
    
    static testMethod void setAccountFields_test() {
        EP_CustomerUpdateStub.customer cust = EP_TestDataUtility.createInboundCustomer();
        cust.SFAccount = new Account(); 
        Test.startTest();
        EP_CustomerUpdateHelper.setAccountFields(cust); 
        Test.stopTest();
        //System.assertEquals(true,cust.SFAccount.Phone!=null);
    }
    static testMethod void setBankAccountFields_test() {
        //L4 #58828 & #58829 Code Changes Start
        EP_CustomerUpdateStub.customer cust = EP_TestDataUtility.createInboundCustomer();
        cust.custBanks.custBank[0].SFBankAccount = new EP_Bank_Account__c();
        Account acc = EP_TestDataUtility.getSellTo();
        Account accObject = [select id,EP_Country_Code__c,EP_Country__r.EP_Country_Code__c from Account where id=:acc.Id];
        Map<String,Id> mapCountryCodeCountryIDs = new Map<String,Id>();
        mapCountryCodeCountryIDs.put(accObject.EP_Country_Code__c,accObject.EP_Country__c);
        Test.startTest();   
        EP_CustomerUpdateHelper.setBankAccountFields(cust, mapCountryCodeCountryIDs);
        Test.stopTest();
        System.assertEquals(true,cust.custBanks.custBank[0].SFBankAccount.EP_Source_Seq_Id__c== cust.custBanks.custBank[0].seqId);
        //L4 #58828 & #58829 Code Changes End
    }
    
    static testMethod void setBankFields_test() {
        //L4 #58828 & #58829 Code Changes Start
        EP_CustomerUpdateStub.customer cust = EP_TestDataUtility.createInboundCustomer();
        Account acc = EP_TestDataUtility.getSellTo();
        Account accObject = [select id,EP_Country_Code__c,EP_Country__r.EP_Country_Code__c from Account where id=:acc.Id];
        Map<String,Id> mapCountryCodeCountryIDs = new Map<String,Id>();
        mapCountryCodeCountryIDs.put(accObject.EP_Country_Code__c,accObject.EP_Country__c);
        EP_CustomerUpdateStub.custBank bank= cust.custBanks.custBank[0];
        bank.SFBankAccount = new EP_Bank_Account__c(); 
        Test.startTest();   
        EP_CustomerUpdateHelper.setBankFields(bank, mapCountryCodeCountryIDs);
        Test.stopTest();
        System.assertEquals(true,bank.SFBankAccount.EP_Source_Seq_Id__c== bank.seqId);
        //L4 #58828 & #58829 Code Changes End
    }
    
    static testMethod void getAccountStatementType_test() {
        String stmntTypAll = EP_Common_Constant.YES;
        String stmntTypOpen = EP_Common_Constant.YES;
        Test.startTest();
        String result = EP_CustomerUpdateHelper.getAccountStatementType(stmntTypAll,stmntTypOpen);
        Test.stopTest();
        System.assertEquals(true,result!=null);
        System.assertEquals(true, EP_COMMON_Constant.ALL_ITEMS_ACC_STATMNTS+EP_COMMON_Constant.SEMICOLON+EP_COMMON_Constant.OPEN_ITEM_ACC_STMNTS== result);
    }
    
    /*static testMethod void getCreditLimitApproved_PositiveScenariotest() {
        String creditLimitApprovalStatus = EP_Common_Constant.ACCOUNT_APPROVED;
        Test.startTest();
        Boolean result = EP_CustomerUpdateHelper.getCreditLimitApproved(creditLimitApprovalStatus);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void getCreditLimitApproved_PositiveScenariotest1() {
        String creditLimitApprovalStatus = EP_Common_Constant.ACCOUNT_REJECTED;
        Test.startTest();
        Boolean result = EP_CustomerUpdateHelper.getCreditLimitApproved(creditLimitApprovalStatus);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void getCreditLimitApproved_NegativeScenariotest() {
        String creditLimitApprovalStatus = INVALID_CREDIT_STATUS;
        Test.startTest();
        Boolean result = EP_CustomerUpdateHelper.getCreditLimitApproved(creditLimitApprovalStatus);
        Test.stopTest();
        System.AssertEquals(false,result);
    }*/
    
    /*static testMethod void getBlockedType_test() {
        String blocked = '';
        String blockedForNewOrder = EP_Common_Constant.YES;
        Test.startTest();
        String result = EP_CustomerUpdateHelper.getBlockedType(blocked,blockedForNewOrder);
        Test.stopTest();
        System.assertEquals(true,EP_COMMON_CONSTANT.BLOCKED_FOR_NEW_ORDERS== result);
    }
    static testMethod void getBlockedType1_test() {
        String blocked = EP_Common_Constant.YES;
        String blockedForNewOrder = '';
        Test.startTest();
        String result = EP_CustomerUpdateHelper.getBlockedType(blocked,blockedForNewOrder);
        Test.stopTest();
        System.assertEquals(true, EP_COMMON_CONSTANT.ALL==result);
    }
    static testMethod void getBlockedType2_test() {
        String blocked = '';
        String blockedForNewOrder = '';
        Test.startTest();
        String result = EP_CustomerUpdateHelper.getBlockedType(blocked,blockedForNewOrder);
        Test.stopTest();
        System.assertEquals(EP_COMMON_CONSTANT.BLANK, result);
    }*/
    static testMethod void getAccountPeriodicity_test() {
        Test.startTest();
        String result = EP_CustomerUpdateHelper.getAccountPeriodicity(PREVIOUS_VALUE,EP_Common_Constant.YES,VALUE_TO_UPDATE);
        Test.stopTest();
        System.assertEquals(true, PREVIOUS_VALUE+';'+VALUE_TO_UPDATE==result);
    }
    static testMethod void getAccountStatementPeriodicity_test() {
        EP_CustomerUpdateStub.acntStmntFrqncy acntStmntFrqncy = new EP_CustomerUpdateStub.acntStmntFrqncy();
        acntStmntFrqncy.daily=EP_Common_Constant.YES;
        acntStmntFrqncy.weekly=EP_Common_Constant.YES;
        acntStmntFrqncy.fortnightly=EP_Common_Constant.YES;
        acntStmntFrqncy.monthly=EP_Common_Constant.YES;
        Test.startTest();
        String result = EP_CustomerUpdateHelper.getAccountStatementPeriodicity(acntStmntFrqncy);
        Test.stopTest();
        System.assertEquals(true,result!=null);
    }
    static testMethod void getAllSets_test() {
        LIST<EP_CustomerUpdateStub.customer> customers = new List<EP_CustomerUpdateStub.customer>{EP_TestDataUtility.createInboundCustomer()};
        Test.startTest();
        Map<string, Set<string>> result = EP_CustomerUpdateHelper.getAllSets(customers);
        Test.stopTest();
        System.assertEquals(true, result != null);
    }
    static testMethod void getBankAccountSet_test() {
        LIST<EP_CustomerUpdateStub.custBank> custBank =  new List<EP_CustomerUpdateStub.custBank>{EP_TestDataUtility.createInboundCustomerBank()};
        Test.startTest();
        Set<String> result = EP_CustomerUpdateHelper.getBankAccountSet(custBank, null);
        Test.stopTest();
        System.assertEquals(true, result != null);
    }
    static testMethod void setBankAccounts_test1() {
        Map<String,Account> mapCompositeIDAccounts = new Map<String, Account>();
        Map<String,EP_Bank_Account__c> mapBankAccounts = new Map<String, EP_Bank_Account__c>();
        Account account = EP_TestDataUtility.getSellToPositiveScenario();
        mapCompositeIDAccounts.put(account.EP_Composite_Id__c, account);
        Account accObject = [select Id,EP_Account_Company_Name__c,AccountNumber from Account where Id =: account.Id];
        EP_Bank_Account__c bankAccount = [select id,EP_NAV_Bank_Account_Code__c,Name,EP_Account__r.EP_Puma_Company_Code__c from EP_Bank_Account__c where EP_Account__c =:account.Id];
        mapBankAccounts.put(bankAccount.EP_NAV_Bank_Account_Code__c.toUpperCase(),bankAccount);
        List<EP_CustomerUpdateStub.customer> customers = EP_TestDataUtility.createInboundCustomers();
        EP_CustomerUpdateStub.customer customer = customers[0];
        customer.SFAccount = new Account(); 
        customer.clientId = string.valueOf(accObject.EP_Account_Company_Name__c).toUpperCase();
        customer.identifier.custId = string.valueOf(accObject.AccountNumber);    
        List<EP_CustomerUpdateStub.custBank> custBanks = customer.custBanks.custBank;
        EP_CustomerUpdateStub.custBank custBank = custBanks[0];
        custBank.identifier.custId = string.valueOf(accObject.AccountNumber);
        custBank.identifier.bankAccId = bankAccount.Name;
        custBank.clientId= bankAccount.EP_Account__r.EP_Puma_Company_Code__c.toUpperCase();
        Test.startTest();
        EP_CustomerUpdateHelper.setBankAccounts(customers[0],mapBankAccounts,mapCompositeIDAccounts);
        Test.stopTest();
        System.assertEquals(true,account.id==customers[0].custBanks.custBank[0].SFBankAccount.EP_Account__c);
    }
    
    static testMethod void setBankAccounts_test2() {
        Map<String,Account> mapCompositeIDAccounts = new Map<String, Account>();
        Map<String,EP_Bank_Account__c> mapBankAccounts = new Map<String, EP_Bank_Account__c>();
        Account account = EP_TestDataUtility.getSellToPositiveScenario();
        mapCompositeIDAccounts.put(account.EP_Composite_Id__c, account);
        Account accObject = [select Id,EP_Account_Company_Name__c,AccountNumber from Account where Id =: account.Id];
        List<EP_CustomerUpdateStub.customer> customers = EP_TestDataUtility.createInboundCustomers();
        EP_CustomerUpdateStub.customer customer = customers[0];
        customer.SFAccount = new Account(); 
        customer.clientId = string.valueOf(accObject.EP_Account_Company_Name__c);
        customer.identifier.custId = string.valueOf(accObject.AccountNumber);    
        List<EP_CustomerUpdateStub.custBank> custBanks = customer.custBanks.custBank;
        EP_CustomerUpdateStub.custBank custBank = custBanks[0];
        custBank.identifier.custId = string.valueOf(accObject.AccountNumber);
        Test.startTest();
        EP_CustomerUpdateHelper.setBankAccounts(customers[0],mapBankAccounts,mapCompositeIDAccounts);
        Test.stopTest();
        EP_Bank_Account__c bankAccount = [select id,EP_NAV_Bank_Account_Code__c,EP_Account__c from EP_Bank_Account__c where EP_Account__c =:account.Id limit 1];
        System.assertEquals(true,account.id==bankAccount.EP_Account__c);
    }
    
    static testMethod void setBillToAccount_test() {
        EP_CustomerUpdateStub.customer cust = EP_TestDataUtility.createInboundCustomer();
        cust.SFAccount = new Account();     
        Account acc = EP_TestDataUtility.getSellTo();
        Account billToAccount =[select Id,EP_Composite_Id__c from Account where Id=:acc.EP_Bill_To_Account__c];
        cust.billToCustId = acc.AccountNumber;
        cust.clientId = acc.EP_Account_Company_Name__c.toUpperCase();
        Map<String,Account> mapCompositeIDAccounts = new Map<String,Account>();
        mapCompositeIDAccounts.put(billToAccount.EP_Composite_Id__c.toUpperCase(),billToAccount);
        Test.startTest();
        EP_CustomerUpdateHelper.setBillToAccount(cust,mapCompositeIDAccounts);
        Test.stopTest();
        System.assertNotEquals(true,billToAccount.id==cust.SFAccount.EP_Bill_To_Account__c);
    }
    //L4 #45357 Code changes Start
    static testMethod void setBillToAccountNegative_test() {
        EP_CustomerUpdateStub.customer cust = EP_TestDataUtility.createInboundCustomerWithoutBillTo();
        cust.SFAccount = new Account();   
        Account accountObj = EP_TestDataUtility.getSellTo();
        cust.clientId = accountObj.EP_Account_Company_Name__c.toUpperCase();
        Map<String,Account> mapCompositeIDAccounts = new Map<String,Account>();
        mapCompositeIDAccounts.put(accountObj.EP_Composite_Id__c.toUpperCase(),accountObj);
        Test.startTest();
        EP_CustomerUpdateHelper.setBillToAccount(cust,mapCompositeIDAccounts);
        Test.stopTest();
        System.assertEquals(true,cust.SFAccount.EP_Bill_To_Account__c==null);
    }
    //L4 #45357 Code changes End
    /*static testMethod void setCountryCodes_test() {
        EP_CustomerUpdateStub.customer customer = EP_TestDataUtility.createInboundCustomer();
        customer.SFAccount = new Account();
        Account acc = EP_TestDataUtility.getSellTo();   
        Account accObject = [select id,EP_Country_Code__c,EP_Country__r.EP_Country_Code__c from Account where id=:acc.Id];
        Map<String,Id> mapCountryCodeCountryIDs = new Map<String,Id>();
        mapCountryCodeCountryIDs.put(accObject.EP_Country_Code__c,accObject.EP_Country__c);
        customer.clientId = string.valueOf(acc.EP_Account_Company_Name__c);
        customer.identifier.custId = string.valueOf(acc.AccountNumber);     
        List<EP_CustomerUpdateStub.custBank> custBanks = customer.custBanks.custBank;
        Test.startTest();
        EP_CustomerUpdateHelper.setCountryCodes(customer,mapCountryCodeCountryIDs);
        Test.stopTest();
       // System.assertEquals(true,customer.SFAccount.EP_Country_Code__c!= null);
    }*/
    static testMethod void setPackagedPaymentTerm_test() {
        EP_CustomerUpdateStub.customer customer = EP_TestDataUtility.createInboundCustomer();
        customer.SFAccount = new Account(); 
        Account acc = EP_TestDataUtility.getSellTo();
        Map<String,ID> mapPaymentTermPaymentIDs = new Map<String,ID>();
        mapPaymentTermPaymentIDs.put(acc.EP_Payment_Term_Lookup__r.EP_Payment_Term_Code__c,acc.EP_Payment_Term_Lookup__r.id);
        customer.pckgdPymntTerm = acc.EP_Payment_Term_Lookup__r.EP_Payment_Term_Code__c;
        Test.startTest();
        EP_CustomerUpdateHelper.setPackagedPaymentTerm(customer,mapPaymentTermPaymentIDs);
        Test.stopTest();
        System.assertEquals(true, customer.SFAccount.EP_Package_Payment_Term__c==customer.pckgdPymntTerm);
    }
    static testMethod void setPaymentTerm_test() {
        EP_CustomerUpdateStub.customer cust = EP_TestDataUtility.createInboundCustomer();
        cust.SFAccount = new Account(); 
        Account acc = EP_TestDataUtility.getSellTo();
        Map<String,ID> mapPaymentTermPaymentIDs = new Map<String,ID>();
        mapPaymentTermPaymentIDs.put(acc.EP_Payment_Term_Lookup__r.EP_Payment_Term_Code__c,acc.EP_Payment_Term_Lookup__r.id);
        cust.pmtTerm = acc.EP_Payment_Term_Lookup__r.EP_Payment_Term_Code__c;
        cust.pckgdPymntTerm = acc.EP_Payment_Term_Lookup__r.EP_Payment_Term_Code__c;    
        Test.startTest();
        EP_CustomerUpdateHelper.setPaymentTerm(cust,mapPaymentTermPaymentIDs);
        Test.stopTest();
        System.assertEquals(true, cust.pmtTerm==cust.SFAccount.EP_Payment_Term_Lookup__c);
    }
    static testMethod void setPaymentMethod_test() {
        EP_CustomerUpdateStub.customer cust = EP_TestDataUtility.createInboundCustomer();
        cust.SFAccount = new Account();
        Account acc = EP_TestDataUtility.getSellTo();
        Account accObject =[select id,EP_Payment_Method__r.EP_Payment_Method_Code__c,EP_Payment_Method__c from Account where id=:acc.Id];
        Map<String,ID> mapPaymentMtdPaymentIDs = new Map<String,ID>();
        mapPaymentMtdPaymentIDs.put(accObject.EP_Payment_Method__r.EP_Payment_Method_Code__c, accObject.EP_Payment_Method__c);
        cust.pmtMthdId = acc.EP_Payment_Method__c;  
        Test.startTest();
        EP_CustomerUpdateHelper.setPaymentMethod(cust,mapPaymentMtdPaymentIDs);
        Test.stopTest();
        System.assertEquals(true, cust.pmtMthdId== cust.SFAccount.EP_Payment_Method__c);
    }
    static testMethod void setAccountStatus_test() {
        EP_CustomerUpdateStub.customer cust = EP_TestDataUtility.createInboundCustomer();
        cust.SFAccount = new Account(); 
        Account acc = EP_TestDataUtility.getSellTo();
        cust.SFAccount = acc;
        Map<String,Account> mapCompositeIDAccounts = new Map<String,Account>();
        mapCompositeIDAccounts.put(acc.EP_Composite_Id__c.toUpperCase(),acc);
        cust.blocked='';
        cust.clientId = acc.EP_Account_Company_Name__c.toUpperCase();
        cust.identifier.custId = string.valueOf(acc.AccountNumber);
        EP_CustomerUpdateHelper.setAccountFields(cust);
        Test.startTest();
        EP_CustomerUpdateHelper.setAccountStatus(cust,mapCompositeIDAccounts);
        Test.stopTest();
        System.assertEquals(true,cust.SFAccount.EP_Status__c!=null);
    }
    static testMethod void setAccount_test() {
        EP_CustomerUpdateStub.customer cust = EP_TestDataUtility.createInboundCustomer();
        cust.SFAccount = new Account(); 
        Account acc = EP_TestDataUtility.getSellTo();
        Map<String,Account> mapCompositeIDAccounts = new Map<String,Account>();
        mapCompositeIDAccounts.put(acc.EP_Composite_Id__c.toUpperCase(),acc);
        cust.clientId = acc.EP_Account_Company_Name__c.toUpperCase();
        cust.identifier.custId = string.valueOf(acc.AccountNumber);
        Test.startTest();
        EP_CustomerUpdateHelper.setAccount(cust,mapCompositeIDAccounts);
        Test.stopTest();
        System.assertEquals(true, acc.Id==cust.SFAccount.Id);
    }
    static testMethod void setNewBankAccountAttributes_test() {
        EP_CustomerUpdateStub.custBank bank= EP_TestDataUtility.createInboundCustomerBank();
        Account acc = EP_TestDataUtility.getSellTo();
        Map<String,Account> mapCompositeIDAccounts = new Map<String,Account>();
        mapCompositeIDAccounts.put(acc.EP_Composite_Id__c,acc);
        bank.clientId = acc.EP_Account_Company_Name__c;
        bank.identifier.custId = string.valueOf(acc.AccountNumber);
        String custCompositeKey = acc.EP_Composite_Id__c;
        String bankCompositeKey = bank.identifier.custId;   
        bank.SFBankAccount = new EP_Bank_Account__c();
        Test.startTest();
        EP_CustomerUpdateHelper.setNewBankAccountAttributes(bank,mapCompositeIDAccounts,custCompositeKey,bankCompositeKey);
        Test.stopTest();
        System.assertEquals(true,acc.id==bank.SFBankAccount.EP_Account__c);
    }
    
    //L4 #58828 & #58829 Code Changes Start
    static testMethod void getBankAccountCountryCodeSet_test() {
        EP_CustomerUpdateStub.custBank bank = EP_TestDataUtility.createInboundCustomerBank();
        List<EP_CustomerUpdateStub.custBank> banks = new List<EP_CustomerUpdateStub.custBank>();
        banks.add(bank);
        
        Test.startTest();
            set<string> result = EP_CustomerUpdateHelper.getBankAccountCountryCodeSet(banks);
        Test.stopTest();
        
        System.assertEquals(true,result.size() > 0);
    }
    
    static testMethod void getBankAccountCountryCodeSet_NegativeTest() {
        EP_CustomerUpdateStub.custBank bank = EP_TestDataUtility.createInboundCustomerBank();
        bank.cntryCode = null;
        List<EP_CustomerUpdateStub.custBank> banks = new List<EP_CustomerUpdateStub.custBank>();
        banks.add(bank);
        
        Test.startTest();
            set<string> result = EP_CustomerUpdateHelper.getBankAccountCountryCodeSet(banks);
        Test.stopTest();
        
        System.assertEquals(true,result.size() == 0);
    }
    
    static testMethod void setBankAccountCountryCodes_test() {
        EP_CustomerUpdateStub.customer customer = EP_TestDataUtility.createInboundCustomer();
        Account acc = EP_TestDataUtility.getSellTo();   
        Account accObject = [select id,EP_Country_Code__c,EP_Country__r.EP_Country_Code__c from Account where id=:acc.Id];
        Map<String,Id> mapCountryCodeCountryIDs = new Map<String,Id>();
        
        EP_CustomerUpdateStub.custBank bank = EP_TestDataUtility.createInboundCustomerBank();
        mapCountryCodeCountryIDs.put(accObject.EP_Country_Code__c.toUpperCase(),accObject.EP_Country__c);
        bank.cntryCode = accObject.EP_Country_Code__c.toUpperCase();
        bank.SFBankAccount = new EP_Bank_Account__c();
        Test.startTest();
            EP_CustomerUpdateHelper.setBankAccountCountryCodes(bank, mapCountryCodeCountryIDs);
        Test.stopTest();
        
        System.assertEquals(true,bank.SFBankAccount.EP_Country__c !=null);
    }
    
    static testMethod void setBankAccountCountryCodes_Negativetest() {
        EP_CustomerUpdateStub.customer customer = EP_TestDataUtility.createInboundCustomer();
        Account acc = EP_TestDataUtility.getSellTo();   
        Account accObject = [select id,EP_Country_Code__c,EP_Country__r.EP_Country_Code__c from Account where id=:acc.Id];
        Map<String,Id> mapCountryCodeCountryIDs = new Map<String,Id>();
        mapCountryCodeCountryIDs.put(accObject.EP_Country_Code__c,accObject.EP_Country__c);
        EP_CustomerUpdateStub.custBank bank = EP_TestDataUtility.createInboundCustomerBank();
        bank.cntryCode = null;
        Test.startTest();
            EP_CustomerUpdateHelper.setBankAccountCountryCodes(bank, mapCountryCodeCountryIDs);
        Test.stopTest();
        
        System.assertEquals(true,bank.SFBankAccount.EP_Country__c == null);
    }
    //L4 #58828 & #58829 Code Changes End
    //L4 #45313 Changes Start
    static testMethod void setAccountCreditLimitFields_test() {
    EP_CustomerUpdateStub.customer cust = EP_TestDataUtility.createInboundCustomer();
    cust.SFAccount = new Account(); 
    Test.startTest();
    EP_CustomerUpdateHelper.setAccountFields(cust); 
    Test.stopTest();
    System.assertEquals(true,cust.SFAccount.EP_Credit_Limit__c==Decimal.valueOf(cust.pmtCrdLmt) );
      System.assertEquals(true,cust.SFAccount.EP_Credit_Limit_in_Customer_Currency__c== Decimal.valueOf(cust.aprvdCreditLimtAmtCusCur) );
  }
  //L4 #45313 Changes End

  // L4 #45361 Code changes start

    static testMethod void setShipTo_test(){

        EP_CustomerUpdateStub.customer customers = EP_TestDataUtility.createInboundCustomers()[0];
        Account acc = EP_TestDataUtility.getSellTo();
        List<Account> accList = new List<Account>{acc};
        EP_CustomerUpdateStub.shipTo_identifier shipIdentifier = new EP_CustomerUpdateStub.shipTo_identifier();
        shipIdentifier.shipToId = EP_Common_Constant.CAS_CLIENTID;
        List<EP_CustomerUpdateStub.shipToAddress> listshipToAddress = new List<EP_CustomerUpdateStub.shipToAddress>();
        EP_CustomerUpdateStub.shipToAddress shipToAddressObj = new EP_CustomerUpdateStub.shipToAddress();
        shipToAddressObj.SFShipToObject = acc;
        shipToAddressObj.identifier =shipIdentifier;
        listshipToAddress.add(shipToAddressObj);
        EP_CustomerUpdateStub.shipToAddresses shipToAddressesObj = new EP_CustomerUpdateStub.shipToAddresses();
        shipToAddressesObj.shipToAddress = listshipToAddress;
        
        EP_CustomerUpdateStub.cust_identifier identfyObj= new EP_CustomerUpdateStub.cust_identifier();
        identfyObj.custId =EP_Common_Constant.CAS_CLIENTID;
        identfyObj.entrprsId =EP_Common_Constant.CAS_CLIENTID;
        customers.Identifier=identfyObj;
        customers.shipToAddresses = shipToAddressesObj;
        string keyStr =EP_GeneralUtility.getCompositeKey(customers.clientId,EP_Common_Constant.CAS_CLIENTID,EP_Common_Constant.CAS_CLIENTID);
        Map<String,Account>  mapStrAcc = new  Map<String,Account> ();
        mapStrAcc.put(keyStr,acc);
        Test.startTest();
        EP_CustomerUpdateHelper.setShipTo(customers,mapStrAcc);
        Test.stopTest(); 
        System.assertEquals(true,customers.shipToAddresses<> null );
    }
    
    static testMethod void setShipToAttributes_test(){

        Account acc = EP_TestDataUtility.getSellTo();

        EP_CustomerUpdateStub.shipToAddress shipToAddressObj = new EP_CustomerUpdateStub.shipToAddress();
        shipToAddressObj.SFShipToObject = acc;
        shipToAddressObj.locationId =EP_Common_Constant.BLANK;
        Test.startTest();
        EP_CustomerUpdateHelper.setShipToAttributes(acc,shipToAddressObj);
        Test.stopTest();
        System.assertEquals(true,string.isBlank(shipToAddressObj.locationId) );
    }
    
    static testMethod void getShipToIdSet_test(){
        account acc = EP_TestDataUtility.getSellTo();
        EP_CustomerUpdateStub.shipTo_identifier shipIdentifier = new EP_CustomerUpdateStub.shipTo_identifier();
        shipIdentifier.shipToId = EP_Common_Constant.CAS_CLIENTID;
        List<EP_CustomerUpdateStub.shipToAddress> listshipToAddress = new List<EP_CustomerUpdateStub.shipToAddress>();
        EP_CustomerUpdateStub.shipToAddress shipToAddressObj = new EP_CustomerUpdateStub.shipToAddress();
        shipToAddressObj.SFShipToObject = acc;
        shipToAddressObj.identifier =shipIdentifier;
        listshipToAddress.add(shipToAddressObj);
        Test.startTest();
        Set<String> result =EP_CustomerUpdateHelper.getShipToIdSet(listshipToAddress,EP_Common_Constant.CAS_CLIENTID,EP_Common_Constant.CAS_CLIENTID);
        Test.stopTest(); 
        System.assertEquals(true,result.size()> 0 );
    }
    
    static testMethod void setPreferredBankAccount_test(){
        list<EP_CustomerUpdateStub.customer> customers = EP_TestDataUtility.createInboundCustomers();
        account acc = EP_TestDataUtility.getSellTo();
        
        EP_CustomerUpdateStub.cust_identifier identfyObj= new EP_CustomerUpdateStub.cust_identifier();
        identfyObj.custId =EP_Common_Constant.CAS_CLIENTID;
        identfyObj.entrprsId =EP_Common_Constant.CAS_CLIENTID;
        customers[0].Identifier=identfyObj;
        customers[0].prfrdBankAcc =EP_Common_Constant.CAS_CLIENTID;
        customers[0].SFAccount = new account();
        
        EP_Bank_Account__c objbankAcc = EP_TestDataUtility.createBankAccount(acc.id); 
        objbankAcc.EP_NAV_Bank_Account_Code__c =EP_GeneralUtility.getCompositeKey(customers[0].clientId,customers[0].identifier.custId,customers[0].prfrdBankAcc).toUpperCase();
        insert objbankAcc;

        Test.startTest();
        EP_CustomerUpdateHelper.setPreferredBankAccount(customers);
        Test.stopTest(); 
        System.assertEquals(true,objbankAcc.id<> null ); 
    }
    
    static testMethod void getPreferredBankAccountSet_test(){
        list<EP_CustomerUpdateStub.customer> customers = EP_TestDataUtility.createInboundCustomers();
        account acc = EP_TestDataUtility.getSellTo();
        
        EP_CustomerUpdateStub.cust_identifier identfyObj= new EP_CustomerUpdateStub.cust_identifier();
        identfyObj.custId =EP_Common_Constant.CAS_CLIENTID;
        identfyObj.entrprsId =EP_Common_Constant.CAS_CLIENTID;
        customers[0].Identifier=identfyObj;
        customers[0].prfrdBankAcc =EP_Common_Constant.CAS_CLIENTID;
        customers[0].SFAccount = new account();
        
        EP_Bank_Account__c objbankAcc = EP_TestDataUtility.createBankAccount(acc.id); 
        objbankAcc.EP_NAV_Bank_Account_Code__c =EP_GeneralUtility.getCompositeKey(customers[0].clientId,customers[0].identifier.custId,customers[0].prfrdBankAcc).toUpperCase();
        insert objbankAcc;

        Test.startTest();
        EP_CustomerUpdateHelper.getPreferredBankAccountSet(customers);
        Test.stopTest(); 
        System.assertEquals(true,objbankAcc.id<> null ); 
    }
    
    
    static testMethod void setAccountDeactivated_test(){
        EP_CustomerUpdateStub.customer customers = EP_TestDataUtility.createInboundCustomers()[0];
        account acc = EP_TestDataUtility.getSellTo();
        customers.prfrdBankAcc =EP_Common_Constant.CAS_CLIENTID;
        customers.SFAccount = new account();
        customers.activeStatus = EP_COMMON_Constant.NO;

        Test.startTest();
        EP_CustomerUpdateHelper.setAccountDeactivated(customers);
        Test.stopTest(); 
        System.assertEquals(false,customers.SFAccount.IsCustomerPortal); 
    }
    
    static testMethod void setAccountBlocked_test(){
        EP_CustomerUpdateStub.customer customers = EP_TestDataUtility.createInboundCustomers()[0];
        account acc = EP_TestDataUtility.getSellTo();
        customers.blocked =EP_Common_Constant.CAS_CLIENTID;
        customers.SFAccount = new account();

        Test.startTest();
        EP_CustomerUpdateHelper.setAccountBlocked(customers);
        Test.stopTest(); 
        System.assertEquals(true,String.isNotBlank(customers.blocked)); 
    }
    
    static testMethod void setBlockStatus_test(){
        account acc = EP_TestDataUtility.getSellTo();

        Test.startTest();
        EP_CustomerUpdateHelper.setBlockStatus(acc,EP_Common_Constant.REASON_STR,EP_Common_Constant.REASON_STR);
        Test.stopTest(); 
        System.assertEquals(EP_AccountConstant.BLOCKED ,acc.EP_Status__c); 
    }
    
    static testMethod void setAccountUnblocked_test(){
        EP_CustomerUpdateStub.customer customers = EP_TestDataUtility.createInboundCustomers()[0];
        account acc = EP_TestDataUtility.getSellTo();
        customers.blocked =EP_Common_Constant.BLANK;
        customers.SFAccount = new account();
        customers.SFAccount.EP_Status__c = EP_AccountConstant.BLOCKED;
        customers.activeStatus = EP_COMMON_Constant.YES;

        Test.startTest();
        EP_CustomerUpdateHelper.setAccountUnblocked(customers);
        Test.stopTest(); 
        System.assertEquals(EP_AccountConstant.ACTIVE,customers.SFAccount.EP_Status__c); 
    }
    
    static testMethod void clearBlockTypeAndReason_test(){
        account acc = EP_TestDataUtility.getSellTo();

        Test.startTest();
        EP_CustomerUpdateHelper.clearBlockTypeAndReason(acc);
        Test.stopTest(); 
        System.assertEquals(null,acc.EP_Block_type__c); 
    }
// L4 #45361 Code changes END
  //L4# 67333 Code Changes Start
  static testMethod void setSellToHoldingAccount_test() {
        EP_CustomerUpdateStub.customer cust = EP_TestDataUtility.createInboundCustomer();
        cust.SFAccount = new Account();     
        Account acc = EP_TestDataUtility.getSellToWithHolding();
        Account holdingAccount =[select Id,AccountNumber,EP_Composite_Id__c from Account where Id=:acc.EP_Sell_To_Holding_Account__c];
        cust.creditHoldingId = holdingAccount.AccountNumber;
        cust.clientId = acc.EP_Account_Company_Name__c.toUpperCase();
        Map<String,Account> mapCompositeIDAccounts = new Map<String,Account>();
        mapCompositeIDAccounts.put(holdingAccount.EP_Composite_Id__c.toUpperCase(),holdingAccount);
        Test.startTest();
        EP_CustomerUpdateHelper.setSellToHoldingAccount(cust,mapCompositeIDAccounts);
        Test.stopTest();
        System.assertEquals(true,holdingAccount.id==cust.SFAccount.EP_Sell_To_Holding_Account__c);
    }
    //L4# 67333 Code Changes End
}