@isTest
public class EP_SellToASRejected_UT
{

    static final string EVENT_NAME = '08-RejectedTo08-Rejected';
    static final string INVALID_EVENT_NAME = '08-ProspectTo04-Account Set-up';
    /*  
       @description: method to intialise data
    */
    //L4_45352_start
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    }
	//L4_45352_End
static testMethod void setAccountDomainObject_test() {
    EP_SellToASRejected localObj = new EP_SellToASRejected();
    EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASRejectedDomainObject();
    EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
    localObj.setAccountContext(obj,oe);
    Test.startTest();
    localObj.setAccountDomainObject(obj);
    Test.stopTest();
    System.AssertEquals(obj,localObj.account);
}
static testMethod void doOnEntry_test() {
    EP_SellToASRejected localObj = new EP_SellToASRejected();
    EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASRejectedDomainObject();
    EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
    localObj.setAccountContext(obj,oe);
    Test.startTest();
    localObj.doOnEntry();
    Test.stopTest();
    //assert not needed
    system.assert(true);
}
static testMethod void doOnExit_test() {
    EP_SellToASRejected localObj = new EP_SellToASRejected();
    EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASRejectedDomainObject();
    EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
    localObj.setAccountContext(obj,oe);
    Test.startTest();
    localObj.doOnExit();
    Test.stopTest();
    //assert not needed
    system.assert(true);
}
static testMethod void doTransition_PositiveScenariotest() {
    EP_SellToASRejected localObj = new EP_SellToASRejected();
    EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASRejectedDomainObjectPositiveScenario();
    EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
    localObj.setAccountContext(obj,oe);
    Test.startTest();
    Boolean result = localObj.doTransition();
    Test.stopTest(); 
    System.AssertEquals(true,result);
}
static testMethod void doTransition_NegativeScenariotest() {
    EP_SellToASRejected localObj = new EP_SellToASRejected();
    EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASRejectedDomainObjectNegativeScenario();
    EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
    localObj.setAccountContext(obj,oe);
    List<Exception> excList = new List<Exception>();
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
            }
        catch(Exception e){
            excList.add(e);
        }
        Test.stopTest();
        System.Assert(excList.size() > 0);
}

 /* isInboundTransitionPossible does not have negative scenerio because it does not have implementation. It always returns true */
static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
    EP_SellToASRejected localObj = new EP_SellToASRejected();
    EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASRejectedDomainObjectPositiveScenario();
    EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
    localObj.setAccountContext(obj,oe);
    Test.startTest();
    Boolean result = localObj.isInboundTransitionPossible();
    Test.stopTest();
    System.AssertEquals(true,result);
}
    //L4_45352_Start
        static testMethod void doOnEntry_test1() {
        EP_SellToASRejected localObj = new EP_SellToASRejected();
        Account acc = EP_TestDataUtility.getSellToASRejectedDomainObject().localaccount;
        acc.EP_Synced_PE__c = false;
        acc.EP_Synced_NAV__c = false;
        acc.EP_Synced_WinDMS__C = false;
        EP_AccountDomainObject obj1 = new EP_AccountDomainObject(acc);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj1,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //No Assertion as the method is empty ,hence adding a dummy assert.
        system.Assert(true);
    }
	static testMethod void doOnEntry_test2() {
        EP_SellToASRejected localObj = new EP_SellToASRejected();
        Account acc = EP_TestDataUtility.getSellToASRejectedDomainObject().localaccount;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_NAV__c = false;
        acc.EP_Synced_WinDMS__C = false;
        EP_AccountDomainObject obj1 = new EP_AccountDomainObject(acc);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj1,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //No Assertion as the method is empty ,hence adding a dummy assert.
        system.Assert(true);
    }
	static testMethod void doOnEntry_test3() {
        EP_SellToASRejected localObj = new EP_SellToASRejected();
        Account acc = EP_TestDataUtility.getSellToASRejectedDomainObject().localaccount;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_WinDMS__C = false;
        EP_AccountDomainObject obj1 = new EP_AccountDomainObject(acc);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj1,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //No Assertion as the method is empty ,hence adding a dummy assert.
        system.Assert(true);
    }
    //L4_45352_End

}