@isTest
public class EP_AccountFactory_UT
{

static testMethod void getAccountType_test() {
    Account acc = EP_TestDataUtility.getSellTo();
    EP_AccountDomainObject ado = new EP_AccountDomainObject(acc);
    EP_AccountFactory localObj = new EP_AccountFactory(ado);
    Test.startTest();
    EP_AccountType result = localObj.getAccountType();
    Test.stopTest();
    System.AssertNotEquals(null, result);
    
}
static testMethod void getVendorType_test() {
	Account acc = EP_TestDataUtility.getShipTo();
    EP_AccountDomainObject ado = new EP_AccountDomainObject(acc);
    EP_AccountFactory localObj = new EP_AccountFactory(ado);
    Test.startTest();
    EP_Account_VendorManagement result = localObj.getVendorType();
    Test.stopTest();
    System.AssertEquals(true, result != null);
    
}
}