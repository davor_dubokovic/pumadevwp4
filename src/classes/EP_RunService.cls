/* 
@Author      Accenture
@name        EP_RunService  
@CreateDate  05/05/2017
@Description Service class for Run Object
@Version     1.0
*/
public with sharing class EP_RunService {
    
    EP_RunDomainObject runDomainObject; 
    List <EP_Run__c> newRunList ;     
    Map<Id,EP_Run__c> mOldRun;
    
    /**
    * @author       Accenture
    * @name         EP_RunService
    * @date         05/05/2017
    * @description  Constructor for setting runDomain
    * @param        EP_RunDomainObject 
    * @return       NA
    */  
    public EP_RunService(EP_RunDomainObject runDomainObject){
        this.runDomainObject = runDomainObject;
        this.newRunList = runDomainObject.getNewRun();           
        this.mOldRun =  runDomainObject.getOldRunMap();
    }
      
   /**
    * @author       Accenture
    * @name         doBeforeInsertHandle
    * @date         05/05/2017
    * @description  Method to update the runCounter on run
    * @param        NA
    * @return       NA
    */  
    public void doBeforeInsertHandle(){
        EP_GeneralUtility.Log('Public','EP_RunService','doAfterInsertHandle');          
        setRunInstanceNumber();
    }
    
    /**
    * @author       Accenture
    * @name         EP_RunService
    * @date         05/05/2017
    * @description  Constructor for setting runDomain
    * @param        EP_RunDomainObject 
    * @return       NA
    */      
    public void doBeforeDeleteHandle(){
        EP_GeneralUtility.Log('Public','EP_RunService','doBeforeDeleteHandle');          
        Map<Id, Boolean> mapOfRunIdAndBoolean = isRunDeletable();
        for(Id runId : mOldRun.keySet()){
            //If map value is false then it is not deletable 
            If(!mapOfRunIdAndBoolean.get(runId)){
                mOldRun.get(runId).addError(system.Label.EP_Run_Deletion_Error_Msg_If_Order_Associated);
            }
        }
    }
    
    /**
    * @author       Accenture
    * @name         EP_RunService
    * @date         05/05/2017
    * @description  Constructor for setting runDomain
    * @param        EP_RunDomainObject 
    * @return       NA
    */    
    @TestVisible
    private void setRunInstanceNumber(){     
        
        EP_GeneralUtility.Log('Private','EP_RunService','setRunName');
        Map<Id, EP_Route__c> routeMap = new EP_RouteMapper().getRouteRecordsById(runDomainObject.getRouteIds());
        Map<Id, Integer> instanceNumberMap = new Map<Id, Integer>();        
            for(EP_Run__c run: newRunList){           
                If(!instanceNumberMap.containsKey(run.EP_Route__c)){
                    Integer instanceNum = routeMap.get(run.EP_Route__c).EP_Max_Instance_Number__c != null ? 
                                            Integer.valueOf(routeMap.get(run.EP_Route__c).EP_Max_Instance_Number__c) :0;
                    instanceNumberMap.put(run.EP_Route__c, instanceNum);
                        
                }
                run.EP_Instance_Number__c = instanceNumberMap.get(run.EP_Route__c) +1; 
                instanceNumberMap.put(run.EP_Route__c, instanceNumberMap.get(run.EP_Route__c) +1); 
            }        
   }
    
    /**
    * @author       Accenture
    * @name         validateRunDeletionWithOrders
    * @date         05/05/2017
    * @description  This method validates run record before deletion.If run is associated with order
    *               User can not delete run record
    * @param        NA
    * @return       Map<Id, Boolean>
    */    
    public Map<Id, Boolean> isRunDeletable(){       
        EP_RunMapper runMapperObj = new EP_RunMapper();
        Map<Id, Boolean> mapOfRunIdAndvalidFlag = new Map<Id, Boolean>();
        runDomainObject.invalidRunIds = runMapperObj.getAssociatedOrdersWithRun(mOldRun.values());
        for(Id runID: mOldRun.keySet()){
            mapOfRunIdAndvalidFlag.put(runID, runDomainObject.isRunDeletable(runID)) ;            
        }
        return mapOfRunIdAndvalidFlag;
    }
    
}