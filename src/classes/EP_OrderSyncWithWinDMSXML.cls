/*
    @Author          Accenture
    @Name            EP_OrderSyncWithWinDMSXML
    @CreateDate      04/18/2017
    @Description     This class  is used to generate outbound XML's of Order to Sync with WINDMS
    @Version         1.0
    @Reference       NA
*/
public class EP_OrderSyncWithWinDMSXML extends EP_GenerateOrderRequestXML{ 
    /**
    * @author           Accenture
    * @name             createXML
    * @date             04/18/2017
    * @description      This method is used to create XML for Order to Sync with NAV
    * @param            NA
    * @return           NA
    */
    public override string createXML(){
        EP_GeneralUtility.Log('Public','EP_OrderCreditCheckXML','createXML');
        return super.createXML();
    }
    
    /**
    * @author           Accenture
    * @name             createPayload
    * @date             04/18/2017
    * @description      This method is used to create Payload for Order to Sync with NAV
    * @param            NA
    * @return           NA
    */    
    public override void createPayload(){
        EP_GeneralUtility.Log('Public','EP_OrderCreditCheckXML','createXML');
        DOM.Document doctemp = new DOM.Document();
        Dom.XMLNode ordersNode = doctemp.createRootElement(EP_OrderConstant.orders_windmsxml,null,null);
        Dom.XMLNode orderNode = ordersNode.addChildElement(EP_OrderConstant.order_windmsxml,null,null);

        Datetime dt = Datetime.now();
        String versionNr = dt.format('yyyyMMdd\'T\'hhmmss') + '.' + dt.millisecondGmt();
        
        // Defect -80324 Ex-Rack changes
        String locationId;
        if(OrderObj.EP_Delivery_Type__c == Label.Delivery_Type || OrderObj.EP_Delivery_Type__c == Label.Consumption_Type){
            locationId = getValueforNode(OrderObj.EP_Supply_Location_Number__c);
        }else{
            locationId = getValueforNode(OrderObj.EP_Pickup_Location_ID__c);
      
        }
        
        //Ship to changes for Ex-Rack.82022
        
        String shiptovalue = EP_Common_Constant.BLANK;
        if(OrderObj.EP_Delivery_Type__c == Label.Delivery_Type || OrderObj.EP_Delivery_Type__c == Label.Consumption_Type){
            shiptovalue = getValueforNode(OrderObj.EP_Ship_To_ID__c);
        }else{
            shiptovalue = getValueforNode(OrderObj.EP_Sell_To_Id__c);            
        }
        //Repeat this for every Child node
        String seqid = EP_IntegrationUtil.reCreateSeqId(this.messageId, OrderObj.Id);
        orderNode.addChildElement(EP_OrderConstant.seqId,null,null).addTextNode(getValueforNode(seqid)); //Value for seqId
        orderNode.addChildElement(EP_OrderConstant.sellToId,null,null).addTextNode(getValueforNode(OrderObj.EP_Sell_To_Id__c)); //Value for sellToId
        orderNode.addChildElement(EP_OrderConstant.shipToId,null,null).addTextNode(shiptovalue); //Value for shipToId
        Dom.XMLNode identifierNode = orderNode.addChildElement(EP_OrderConstant.identifier_windmsxml,null,null);
        Dom.XMLNode orderIdNode = identifierNode.addChildElement(EP_OrderConstant.orderId,null,null).addTextNode(getValueforNode(OrderObj.OrderNumber__c)); //Value for orderId
        Dom.XMLNode entrprsIdNode = IdentifierNode.addChildElement(EP_OrderConstant.entrprsId,null,null).addTextNode(getValueforNode(OrderObj.Enterprise_Id__c));
        orderNode.addChildElement(EP_OrderConstant.purchaseOrder,null,null).addTextNode(getValueforNode(OrderObj.EP_Customer_PO_Number__c));
        orderNode.addChildElement(EP_OrderConstant.transporterCode,null,null).addTextNode(getValueforNode(OrderObj.EP_Transportation_Code__c));
        orderNode.addChildElement(EP_OrderConstant.stockHldngLocId,null,null).addTextNode(locationId); //Value for stockHldngLocId
        orderNode.addChildElement(EP_OrderConstant.shiftDate,null,null);
        orderNode.addChildElement(EP_OrderConstant.shiftId,null,null);
        orderNode.addChildElement(EP_OrderConstant.orderFromDt,null,null).addTextNode(EP_DateTimeUtility.formatDateAsString(OrderObj.Delivery_From_Date__c)); //Value for orderFromDt
        //PS:21/03/2018 Added to avoid requested delivery date blank
        if (OrderObj.EP_Requested_Delivery_Date__c == null){
        orderNode.addChildElement(EP_OrderConstant.reqDlvryDt,null,null).addTextNode(EP_DateTimeUtility.formatDateAsString(OrderObj.Delivery_From_Date__c)); //Value for reqDlvryDt
        } else {
        orderNode.addChildElement(EP_OrderConstant.reqDlvryDt,null,null).addTextNode(EP_DateTimeUtility.formatDateAsString(OrderObj.EP_Requested_Delivery_Date__c)); //Value for reqDlvryDt
        }
        orderNode.addChildElement(EP_OrderConstant.deliveryType,null,null).addTextNode(getValueforNode(OrderObj.EP_Delivery_Type__c)); //Value for deliveryType
        orderNode.addChildElement(EP_OrderConstant.urgentOrder,null,null).addTextNode(EP_OrderConstant.NO_TAG);
        orderNode.addChildElement(EP_OrderConstant.remark1,null,null).addTextNode(getValueforNode(OrderObj.EP_Order_Comments__c));
        orderNode.addChildElement(EP_OrderConstant.remark2,null,null);
        orderNode.addChildElement(EP_OrderConstant.creditStatus,null,null).addTextNode(EP_OrderConstant.OK_TAG); //Value for creditStatus // EMPTY ?
        orderNode.addChildElement(EP_OrderConstant.orderDlvrNr,null,null);
        orderNode.addChildElement(EP_OrderConstant.modifyOn,null,null).addTextNode(EP_DateTimeUtility.formatDateAsString(OrderObj.LastModifiedDate)); //Value for modifyOn
        orderNode.addChildElement(EP_OrderConstant.modifyBy,null,null).addTextNode(OrderObj.EP_Modified_By__c); //Value for modifyBy
        orderNode.addChildElement(EP_OrderConstant.creditIndctr,null,null).addTextNode(getCreditIndictorValue()); //Value for creditIndctr
        orderNode.addChildElement(EP_OrderConstant.pctQntChange,null,null);
        orderNode.addChildElement(EP_OrderConstant.action,null,null).addTextNode(getValueforNode(OrderObj.EP_Action__c));
        orderNode.addChildElement(EP_OrderConstant.clientId,null,null).addTextNode(getValueforNode(OrderObj.EP_Puma_Company_Code__c)); //Value for clientId
        //MC 9/4/2018 - RO CR if RO then send Retrospective instead of Run Id
        if(OrderObj.EP_Order_Epoch__c == EP_OrderConstant.Order_Epoch_Retro){
            orderNode.addChildElement(EP_OrderConstant.runId,null,null).addTextNode(getValueforNode(OrderObj.EP_Order_Epoch__c));
        } else {
            orderNode.addChildElement(EP_OrderConstant.runId,null,null).addTextNode(getValueforNode(OrderObj.EP_Run_Id_2__c));
        }
        orderNode.addChildElement(EP_OrderConstant.transportManagement,null,null).addTextNode(getValueforNode(OrderObj.EP_Transport_Management_2__c));
        orderNode.addChildElement(EP_OrderConstant.productType,null,null).addTextNode(getValueforNode(OrderObj.EP_Order_Product_Category__c));
        //MC 11/04/2018 Implemeting Ex-Rack with Scheduling CR
        if(OrderObj.EP_Delivery_Type__c == EP_OrderConstant.deliveryExRackWithSch){
            orderNode.addChildElement(EP_OrderConstant.route,null,null).addTextNode(getValueforNode(OrderObj.EP_Delivery_Type__c)); 
        }else{
            orderNode.addChildElement(EP_OrderConstant.route,null,null).addTextNode(getValueforNode(OrderObj.EP_Route_Number__c)); 
        }
        orderNode.addChildElement(EP_OrderConstant.route,null,null).addTextNode(getValueforNode(OrderObj.EP_Route_Number__c));
        OrderNode.addChildElement(EP_OrderConstant.versionNr,null,null).addTextNode(getValueforNode(versionNr));
        OrderNode.addChildElement(EP_OrderConstant.loadCode,null,null);
        
        Dom.XMLNode orderLinesNode = orderNode.addChildElement(EP_OrderConstant.OrderLines_windmsxml,null,null);
        for(orderLineItemWrapper orderLineWrp :orderLineItemsWrapper){
            
           if (orderLineWrp.orderLineItem.EP_Product_Sold_As__c == system.label.EP_OrderLineItemBulkProduct) {//PS:15/03/2018 Added if check
            Dom.XMLNode orderLineNode = orderLinesNode.addChildElement(EP_OrderConstant.OrderLine_windmsxml,null,null);
            
           
            //Repeat this for every Child node
            orderLineNode.addChildElement(EP_OrderConstant.seqId,null,null).addTextNode(getValueforNode(seqid)); //orderLineWrp.orderLineSeqId); //Value for seqId
            Dom.XMLNode identifierNode2 = orderLineNode.addChildElement(EP_OrderConstant.identifier_windmsxml,null,null);
            identifierNode2.addChildElement(EP_OrderConstant.lineId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.Order_Line_Number__c)); //EP_Line_Id_WINDMS_XML__c)); //Value for lineId
            orderLineNode.addChildElement(EP_OrderConstant.itemId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Product_Code__c)); //Value for itemId
            orderLineNode.addChildElement(EP_OrderConstant.description,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.Description__c)); //Value for description
            orderLineNode.addChildElement(EP_OrderConstant.tankId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Tank_Code__c)); //Value for tankId   // EMPTY ?
            orderLineNode.addChildElement(EP_OrderConstant.isTankFill,null,null).addTextNode(EP_OrderConstant.NO_TAG); //Value for isTankFill   // EMPTY ?
            orderLineNode.addChildElement(EP_OrderConstant.qty,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.Quantity__c)); //Value for qty
            orderLineNode.addChildElement(EP_OrderConstant.UOM_SMALL,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Quantity_UOM__c)); //Value for uom
        }
        }

        System.debug('EP_OrderSyncWithWinDMSXML orderNode ' + orderNode);
        System.debug('EP_OrderSyncWithWinDMSXML orderLinesNode ' + orderLinesNode);
        Dom.XMLNode PayloadNode = MSGNode.addChildElement(EP_OrderConstant.Payload,null,null);
        dom.XmlNode anyNode = PayloadNode.addChildElement(EP_OrderConstant.any0,null, null);   
        // Encoding payload by calling encode XML method in superclass
        system.debug('pep ' + doctemp.toXmlString());
        anyNode.addTextNode(encodeXML(doctemp));    
        
    }
    /**
    * @author           Accenture
    * @name             getOrderUrgency
    * @date             04/18/2017
    * @description      This method is used to get Order Urgency value for XML tag
    * @param            NA
    * @return           NA
    */
    @TestVisible
    private String getOrderUrgency(){
        EP_GeneralUtility.Log('Private','EP_OrderCreditCheckXML','createXML');
        if(string.isblank(orderObj.EP_Urgent_Order__c)){
            return EP_Common_Constant.STRING_NO_LS;
        }
        return orderobj.EP_Urgent_Order__c;
    }
    
    /**
    * @author           Accenture
    * @name             getCreditIndictorValue
    * @date             04/18/2017
    * @description      This method is used to get Credit Indictor Value for XML tag
    * @param            NA
    * @return           NA
    */
    @TestVisible
    private string getCreditIndictorValue() {
        EP_GeneralUtility.Log('Private','EP_OrderCreditCheckXML','getCreditIndictorValue');
        string creditIndictorStr = EP_Common_Constant.BLANK;
        if(String.isNotBlank(orderObj.EP_Exception_Number__c)) {
            creditIndictorStr =   EP_Common_Constant.CEC ;
        } else if ( EP_Common_Constant.PREPAYMENT.equalsIgnoreCase(orderObj.EP_Payment_Term__c)) {
            creditIndictorStr =   EP_Common_Constant.PRE ;
        } else {
            creditIndictorStr =  EP_Common_Constant.CHK;
        }
        return creditIndictorStr;
    }
    
}