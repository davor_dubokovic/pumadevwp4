/*   
     @Author Aravindhan Ramalingam
     @name <EP_RetroVMINonConsignmentSM.cls>     
     @Description <Retro VMI Non Consignment Statemachine >   
     @Version <1.1> 
     */

     public class EP_RetroVMINonConsignmentSM extends EP_OrderStateMachine {
     	
     	public EP_RetroVMINonConsignmentSM(){

     	}
     	
     	public override EP_OrderState getOrderState(EP_OrderEvent currentEvent){
     		EP_GeneralUtility.Log('Public','EP_RetroVMINonConsignmentSM','getOrderState');
     		return super.getOrderState(currentEvent);
     	}
     }