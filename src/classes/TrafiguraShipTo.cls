global with sharing class TrafiguraShipTo extends cscfga.ALookupSearch {

  private static final String BASKET_ID_FIELD = 'Product Basket ID';
  private static final String REFRESH_INDICATIOR_FIELD = 'Refresh Order Value Indicator';
  private cscfga__Product_Basket__c basketSObject;

  global override List<Object> doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionId) {
    String basketId = searchFields.get(BASKET_ID_FIELD);
    basketSObject = (cscfga__Product_Basket__c) SObjectUtils.loadSObject(basketId);
    List<Account> accountList = new List<Account>();
    
    List<cscfga__Product_Configuration__c> pc =[SELECT 
                                                    ID,cscfga__Product_Basket__c 
                                                FROM
                                                   cscfga__Product_Configuration__c
                                                WHERE
                                                    cscfga__Product_Basket__c =:basketId];
    system.debug('pc is :' +pc);
                                                    
    if(pc != null && pc.size () ==1){
         List <cscfga__Attribute__c> attr =[SELECT 
                                                cscfga__Display_Value__c,cscfga__Product_Configuration__c,cscfga__Value__c,Id,Name 
                                            FROM cscfga__Attribute__c 
                                            where name ='Order Type' and cscfga__Product_Configuration__c=: pc.get(0).id];
        system.debug('attr is :' +attr);
        
        if(attr != null && attr.size() ==1){
            
            if(attr.get(0).cscfga__Value__c == 'Delivery')
            {
                accountList =[SELECT
                                    Id,parentId,name,recordtypeid,EP_Ship_To_Type__c from account
                                    where parentId='0010k00000DBGqa'
                                ];
                
            }
        }
    }
  
    return null; 
  }
   
  
}