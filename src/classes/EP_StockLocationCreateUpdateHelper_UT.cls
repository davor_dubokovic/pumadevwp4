@isTest
public class EP_StockLocationCreateUpdateHelper_UT{
	
	static testMethod void setStockLocationAttributes_PositiveTest() {
		list<EP_StockLocationStub.InventoryLoc> lstStockLoc = new list<EP_StockLocationStub.InventoryLoc>();
		EP_StockLocationStub.InventoryLoc invLoc = EP_TestDataUtility.createInboundStorageLocation();
		lstStockLoc.add(invLoc); 
		Company__c  cmpny = EP_TestDataUtility.createCompany('AUN');
        insert cmpny;
	    Test.startTest();
	    list<Account> lstAccount =EP_StockLocationCreateUpdateHelper.setStockLocationAttributes(lstStockLoc);
	    Test.stopTest();     
	    System.assertEquals(true, !lstAccount.isEmpty());
	}
	
	static testMethod void setStockLocationAttributes_NegativeTest() {
		list<EP_StockLocationStub.InventoryLoc> lstStockLoc = new list<EP_StockLocationStub.InventoryLoc>();
	    Test.startTest();
	    list<Account> lstAccount =EP_StockLocationCreateUpdateHelper.setStockLocationAttributes(lstStockLoc);
	    Test.stopTest();     
	    System.assertEquals(true, lstAccount.isEmpty());
	}
	
	static testMethod void getRecordTypeId_Test() {
	    Test.startTest();
	    string result =EP_StockLocationCreateUpdateHelper.getRecordTypeId();
	    Test.stopTest();     
	    System.assertEquals(true, string.isnotblank(result));
	}
	
	static testMethod void getCompanyMap_PositiveTest() {
		list<EP_StockLocationStub.InventoryLoc> lstStockLoc = new list<EP_StockLocationStub.InventoryLoc>();
		EP_StockLocationStub.InventoryLoc invLoc = EP_TestDataUtility.createInboundStorageLocation();
		lstStockLoc.add(invLoc); 
		Company__c  cmpny = EP_TestDataUtility.createCompany('AUN');
        insert cmpny;
	    Test.startTest();
	    map<string,Company__c> result =EP_StockLocationCreateUpdateHelper.getCompanyMap(lstStockLoc);
	    Test.stopTest();     
	    System.assertEquals(true, !result.isEmpty());
	}
	static testMethod void getCompanyMap_NegativeTest() {
		list<EP_StockLocationStub.InventoryLoc> lstStockLoc = new list<EP_StockLocationStub.InventoryLoc>();
		EP_StockLocationStub.InventoryLoc invLoc = EP_TestDataUtility.createInboundStorageLocation();
		lstStockLoc.add(invLoc); 
		Company__c  cmpny = EP_TestDataUtility.createCompany('ABC');
        insert cmpny;
	    Test.startTest();
	    map<string,Company__c> result =EP_StockLocationCreateUpdateHelper.getCompanyMap(lstStockLoc);
	    Test.stopTest();     
	    System.assertEquals(true, result.isEmpty());
	}
	
	static testMethod void getCompany_PositiveTest() {
		list<EP_StockLocationStub.InventoryLoc> lstStockLoc = new list<EP_StockLocationStub.InventoryLoc>();
		EP_StockLocationStub.InventoryLoc invLoc = EP_TestDataUtility.createInboundStorageLocation();
		lstStockLoc.add(invLoc); 
		Company__c  cmpny = EP_TestDataUtility.createCompany('AUN');
        insert cmpny;	    
	    map<string,Company__c> mapcompany =EP_StockLocationCreateUpdateHelper.getCompanyMap(lstStockLoc);
	    Test.startTest();
	    string result =EP_StockLocationCreateUpdateHelper.getCompany(mapcompany,'AUN');
	    Test.stopTest();     
	    System.assertEquals(true, string.isNotBlank(result));
	}
	
	static testMethod void getStatus_PositiveTest() {
	    Test.startTest();
	    string result =EP_StockLocationCreateUpdateHelper.getStatus('YES');
	    Test.stopTest();     
	    System.assertEquals(true, result.equalsignorecase(EP_Common_Constant.STATUS_DEACTIVETE));
	}
	static testMethod void getStatus_NegativeTest() {
	    Test.startTest();
	    string result =EP_StockLocationCreateUpdateHelper.getStatus('NO');
	    Test.stopTest();     
	    System.assertEquals(true, result.equalsignorecase(EP_Common_Constant.STATUS_ACTIVE));
	} 
	
	static testMethod void generateExternalIdForStockLocation_Test() {
		EP_StockLocationStub.InventoryLoc invLoc = EP_TestDataUtility.createInboundStorageLocation();
	    Test.startTest();
	    string result =EP_StockLocationCreateUpdateHelper.generateExternalIdForStockLocation(invLoc);
	    Test.stopTest();     
	    System.assertEquals(true, string.isNotBlank(result));
	}
}