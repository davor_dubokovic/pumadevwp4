/* 
   @Author <Cihan Sunar>
   @name <EP_FE_TankDipLevelResponse>
   @CreateDate <03/05/2016>
   @Description <Wrapper for the tank dip submission API>  
   @Version <1.0>
*/
global with sharing class EP_FE_TankDipSubmissionRequest {
/* 
   @Author <Cihan Sunar>
   @name <EP_FE_TankDipLevelResponse>
   @CreateDate <03/05/2016>
   @Description <Wrapper for the tank dip submission API>  
   @Version <1.0>
*/
    global with sharing class EP_FE_TankDipSubmissionItem {
        public String tankId {get; set;}
        public String unitOfMeasurement {get; set;}
        public Integer dipLevel {get; set;}
        public Datetime measurementTime {get; set;}
    }
    
    public List<EP_FE_TankDipSubmissionItem> tankDips {get; set;}
    
}