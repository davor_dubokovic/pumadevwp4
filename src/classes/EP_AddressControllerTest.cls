@isTest
private class EP_AddressControllerTest {

    private static final String ID_STR = 'id';
    private static final String ADD_STR = 'address';
    private static final String CNTRY_CODE = 'CntryCode';
    private static final String STREET_NUMBER_STR = 'street_number';
    private static final String ROUTE_STR = 'route';
    private static final String LOCALITY_STR = 'locality';
    private static final String ADMIN_AREA_STR = 'administrative_area_level_1';
    private static final String POSTAL_CODE_STR = 'postal_code';
    private static final String LAT_STR = 'lat';
    private static final String LNG_STR = 'lng';
    static testMethod void validateAddressTest() {
        
        Company__c company = EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA);
        Database.insert(company); 
        EP_Country__c country = EP_TestDataUtility.createCountryRecord('Australia','AU','Asia-Pacific');
        DataBase.insert(country);
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix);
        Profile cscAgent = [Select id from Profile
                                Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                                limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);          
        DataBase.SaveResult svr;
        Test.startTest();
        System.runAs(cscUser){  
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
        Account customer = EP_TestDataUtility.createSellToAccount(null, freightMatrix.id);
        customer.EP_Country__c = country.id;
        customer.EP_Is_Valid_Address__c = false;
        database.insert(customer);
        
        customer.EP_Status__C = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        customer.recordtypeid=EP_TestDataUtility.NON_VMI_SHIP_RT;
        svr = DataBase.Update(customer,false);
        
        
        //Test Class Fix Start
        PageReference pageRef = Page.EP_AddressValidation;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put(ID_STR,customer.id);
        ApexPages.currentPage().getParameters().put(ADD_STR,customer.billingStreet+EP_Common_Constant.Space+customer.billingCity+EP_Common_Constant.Space+customer.BillingState+EP_Common_Constant.Space+customer.BillingCountry+EP_Common_Constant.Space+customer.BillingPostalCode );
        ApexPages.currentPage().getParameters().put(CNTRY_CODE,country.EP_Country_Code__c); 
        EP_AddressController addCtlr = new EP_AddressController();
        //Test Class Fix End
        addCtlr.addressContext.address = STREET_NUMBER_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingStreet + EP_Common_Constant.COMMA
                        +ROUTE_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingStreet + EP_Common_Constant.COMMA
                        +LOCALITY_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingStreet + EP_Common_Constant.COMMA
                        +ADMIN_AREA_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingState + EP_Common_Constant.COMMA
                        +EP_COMMON_CONSTANT.COUNTRY + EP_Common_Constant.TIME_SEPARATOR_SIGN + country.Name + EP_Common_Constant.COMMA
                        +POSTAL_CODE_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingPostalCode + EP_Common_Constant.COMMA
                        +LNG_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + '-33.868820' + EP_Common_Constant.COMMA
                        +LAT_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + '151.209296' + EP_Common_Constant.COMMA;
        addCtlr.updateAddress();
        addCtlr.cancel();
        
        }
        Test.stopTest();
        System.assert(!svr.isSuccess());
        System.assert(svr.getErrors()[0].getMessage().containsIgnoreCase('Please validate address through \'Validate Address\' Button.'));
        System.assert(EP_Common_Util.isValidAddress);
        
    }
    
    static testMethod void cntryInvalidTest() {
        
        Company__c company = EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA);
        Database.insert(company); 
        list<EP_Country__c> country = new list<EP_Country__c>();
        list<ApexPages.Message> pagemsgs = new list<ApexPages.Message>(); 
        country.add(EP_TestDataUtility.createCountryRecord('Australia','AU','Asia-Pacific'));
        country.add(EP_TestDataUtility.createCountryRecord('America','Us','Pacific'));
        DataBase.insert(country);
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix);
        Profile cscAgent = [Select id from Profile
                                Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                                limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);          
        DataBase.SaveResult svr;
        Test.startTest();
        System.runAs(cscUser){  
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
        Account customer = EP_TestDataUtility.createSellToAccount(null,freightMatrix.id);
        customer.EP_Country__c = country[0].id;
        customer.EP_Is_Valid_Address__c = false;
        database.insert(customer);
        
        customer.EP_Status__C = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        customer.recordtypeid=EP_TestDataUtility.NON_VMI_SHIP_RT;
        svr = DataBase.Update(customer,false);
        
        
        //Test Class Fix Start
        PageReference pageRef = Page.EP_AddressValidation;
        Test.setCurrentPage(pageRef);
       
        ApexPages.currentPage().getParameters().put(ID_STR,customer.id);
        ApexPages.currentPage().getParameters().put(ADD_STR,customer.billingStreet+EP_Common_Constant.Space+customer.billingCity+EP_Common_Constant.Space+customer.BillingState+EP_Common_Constant.Space+customer.BillingCountry+EP_Common_Constant.Space+customer.BillingPostalCode );
        ApexPages.currentPage().getParameters().put(CNTRY_CODE,country[0].EP_Country_Code__c);  
        EP_AddressController addCtlr = new EP_AddressController();
         //Test Class Fix End
        addCtlr.addressContext.address = STREET_NUMBER_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingStreet + EP_Common_Constant.COMMA
                        +ROUTE_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingStreet + EP_Common_Constant.COMMA
                        +LOCALITY_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingStreet + EP_Common_Constant.COMMA
                        +ADMIN_AREA_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingState + EP_Common_Constant.COMMA
                        +EP_COMMON_CONSTANT.COUNTRY + EP_Common_Constant.TIME_SEPARATOR_SIGN + country[1].Name + EP_Common_Constant.COMMA
                        +POSTAL_CODE_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingPostalCode + EP_Common_Constant.COMMA
                        +LNG_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + '-33.868820' + EP_Common_Constant.COMMA
                        +LAT_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + '151.209296' + EP_Common_Constant.COMMA;
        addCtlr.updateAddress();
        pagemsgs = ApexPages.getMessages();
        }
        Test.stopTest();
        System.assert(!svr.isSuccess());
        System.assert(svr.getErrors()[0].getMessage().containsIgnoreCase('Please validate address through \'Validate Address\' Button.'));
        System.assert(!EP_Common_Util.isValidAddress);
        System.assert(pagemsgs[0].getDetail().containsIgnoreCase(Label.EP_Address_SelectMsg + EP_Common_Constant.SPACE + country[0].Name));
        
    }
    
    static testMethod void noAddressSelectedTest() {
        
        Company__c company = EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA);
        Database.insert(company); 
        list<EP_Country__c> country = new list<EP_Country__c>();
        list<ApexPages.Message> pagemsgs = new list<ApexPages.Message>(); 
        country.add(EP_TestDataUtility.createCountryRecord('Australia','AU','Asia-Pacific'));
        country.add(EP_TestDataUtility.createCountryRecord('America','Us','Pacific'));
        DataBase.insert(country);
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix);
        Profile cscAgent = [Select id from Profile
                                Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                                limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);          
        DataBase.SaveResult svr;
        Test.startTest();
        System.runAs(cscUser){  
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
        Account customer = EP_TestDataUtility.createSellToAccount(null, freightMatrix.id);
        customer.EP_Country__c = country[0].id;
        customer.EP_Is_Valid_Address__c = false;
        database.insert(customer);
        
        customer.EP_Status__C = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        customer.recordtypeid=EP_TestDataUtility.NON_VMI_SHIP_RT;
        svr = DataBase.Update(customer,false);
        
        
        //Test Class Fix Start
        PageReference pageRef = Page.EP_AddressValidation;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put(ID_STR,customer.id);
        ApexPages.currentPage().getParameters().put(ADD_STR,customer.billingStreet+EP_Common_Constant.Space+customer.billingCity+EP_Common_Constant.Space+customer.BillingState+EP_Common_Constant.Space+customer.BillingCountry+EP_Common_Constant.Space+customer.BillingPostalCode );
        ApexPages.currentPage().getParameters().put(CNTRY_CODE,country[0].EP_Country_Code__c);  
        EP_AddressController addCtlr = new EP_AddressController();
         //Test Class Fix End
        addCtlr.updateAddress();
        pagemsgs = ApexPages.getMessages();
        }
        Test.stopTest();
        System.assert(!svr.isSuccess());
        System.assert(svr.getErrors()[0].getMessage().containsIgnoreCase('Please validate address through \'Validate Address\' Button.'));
        System.assert(!EP_Common_Util.isValidAddress);
        System.assert(pagemsgs[0].getDetail().containsIgnoreCase(Label.EP_No_address_selected));
        
    }
    
    static testMethod void addressSelectedTest() {
        
        Company__c company = EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA);
        Database.insert(company); 
        list<EP_Country__c> country = new list<EP_Country__c>();
        list<ApexPages.Message> pagemsgs = new list<ApexPages.Message>(); 
        country.add(EP_TestDataUtility.createCountryRecord('Australia','AU','Asia-Pacific'));
        country.add(EP_TestDataUtility.createCountryRecord('America','Us','Pacific'));
        DataBase.insert(country);
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix);
        Profile cscAgent = [Select id from Profile
                                Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                                limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);          
        DataBase.SaveResult svr;
        Test.startTest();
        System.runAs(cscUser){  
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
        Account customer = EP_TestDataUtility.createSellToAccount(null, freightMatrix.id);
        customer.EP_Country__c = country[0].id;
        customer.EP_Is_Valid_Address__c = true;
        EP_Common_Util.isAddressCR = true;
        database.insert(customer);
        
        customer.EP_Status__C = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        svr = DataBase.Update(customer,false);
        
        
        //Test Class Fix Start
        PageReference pageRef = Page.EP_AddressValidation;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put(ID_STR,customer.id);
        ApexPages.currentPage().getParameters().put(ADD_STR,customer.billingStreet+EP_Common_Constant.Space+customer.billingCity+EP_Common_Constant.Space+customer.BillingState+EP_Common_Constant.Space+customer.BillingCountry+EP_Common_Constant.Space+customer.BillingPostalCode );
        ApexPages.currentPage().getParameters().put(CNTRY_CODE,country[0].EP_Country_Code__c);  
        EP_AddressController addCtlr = new EP_AddressController();
         //Test Class Fix End
        addCtlr.updateAddress();
        pagemsgs = ApexPages.getMessages();
        }
        Test.stopTest();
        System.assert(svr.isSuccess());
        
    }
    
    static testMethod void validateAddressCRErrTest() {
        
        Company__c company = EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA);
        Database.insert(company); 
        EP_Country__c country = EP_TestDataUtility.createCountryRecord('Australia','AU','Asia-Pacific');
        DataBase.insert(country);
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix);
        Profile cscAgent = [Select id from Profile
                                Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                                limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);          
        DataBase.SaveResult svr;
        Test.startTest();
        System.runAs(cscUser){  
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
        Account customer = EP_TestDataUtility.createSellToAccount(null, freightMatrix.id);
        customer.EP_Country__c = country.id;
        customer.EP_Is_Valid_Address__c = false;
        database.insert(customer);
        
        customer.EP_Status__C = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        customer.recordtypeid=EP_TestDataUtility.NON_VMI_SHIP_RT;
        svr = DataBase.Update(customer,false);
        
        
        //Test Class Fix Start
        PageReference pageRef = Page.EP_AddressValidation;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put(ID_STR,customer.id);
        ApexPages.currentPage().getParameters().put(ADD_STR,customer.billingStreet+EP_Common_Constant.Space+customer.billingCity+EP_Common_Constant.Space+customer.BillingState+EP_Common_Constant.Space+customer.BillingCountry+EP_Common_Constant.Space+customer.BillingPostalCode );
        ApexPages.currentPage().getParameters().put(CNTRY_CODE,country.EP_Country_Code__c); 
        ApexPages.currentPage().getParameters().put('CR','Yes'); 
        EP_AddressController addCtlr = new EP_AddressController();
        //Test Class Fix End
        addCtlr.addressContext.address = STREET_NUMBER_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingStreet + EP_Common_Constant.COMMA
                        +ROUTE_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingStreet + EP_Common_Constant.COMMA
                        +LOCALITY_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingStreet + EP_Common_Constant.COMMA
                        +ADMIN_AREA_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingState + EP_Common_Constant.COMMA
                        +EP_COMMON_CONSTANT.COUNTRY + EP_Common_Constant.TIME_SEPARATOR_SIGN + country.Name + EP_Common_Constant.COMMA
                        +POSTAL_CODE_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingPostalCode + EP_Common_Constant.COMMA
                        +LNG_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + '-33.868820' + EP_Common_Constant.COMMA
                        +LAT_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + '151.209296' + EP_Common_Constant.COMMA;
        addCtlr.updateAddress();
        addCtlr.cancel();
        
        }
        Test.stopTest();
        System.assert(!svr.isSuccess());
        System.assert(svr.getErrors()[0].getMessage().containsIgnoreCase('Please validate address through \'Validate Address\' Button.'));
        System.assert(EP_Common_Util.isValidAddress);
        
    }
    
    static testMethod void validateAddressCRTest() {
        
        
        Company__c company = EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA);
        Database.insert(company); 
        EP_Country__c country = EP_TestDataUtility.createCountryRecord('Australia','AU','Asia-Pacific');
        DataBase.insert(country);
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix);
        Profile cscAgent = [Select id from Profile
                                Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                                limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);          
        DataBase.SaveResult svr;
        Test.startTest();
        System.runAs(cscUser){  
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
        Account customer = EP_TestDataUtility.createSellToAccount(null, freightMatrix.id);
        customer.EP_Country__c = country.id;
        customer.EP_Is_Valid_Address__c = false;
        database.insert(customer);
        
        customer.EP_Status__C = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        customer.EP_Is_Valid_Address__c = true;    
        svr = DataBase.Update(customer,false);
        
        
        //Test Class Fix Start
        PageReference pageRef = Page.EP_AddressValidation;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put(ID_STR,customer.id);
        ApexPages.currentPage().getParameters().put(ADD_STR,customer.billingStreet+EP_Common_Constant.Space+customer.billingCity+EP_Common_Constant.Space+customer.BillingState+EP_Common_Constant.Space+customer.BillingCountry+EP_Common_Constant.Space+customer.BillingPostalCode );
        ApexPages.currentPage().getParameters().put(CNTRY_CODE,country.EP_Country_Code__c); 
        ApexPages.currentPage().getParameters().put('CR','Yes'); 
        EP_AddressController addCtlr = new EP_AddressController();
        //Test Class Fix End
        addCtlr.addressContext.address = STREET_NUMBER_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingStreet + EP_Common_Constant.COMMA
                        +ROUTE_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingStreet + EP_Common_Constant.COMMA
                        +LOCALITY_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingStreet + EP_Common_Constant.COMMA
                        +ADMIN_AREA_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingState + EP_Common_Constant.COMMA
                        +EP_COMMON_CONSTANT.COUNTRY + EP_Common_Constant.TIME_SEPARATOR_SIGN + country.Name + EP_Common_Constant.COMMA
                        +POSTAL_CODE_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + customer.billingPostalCode + EP_Common_Constant.COMMA
                        +LNG_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + '-33.868820' + EP_Common_Constant.COMMA
                        +LAT_STR + EP_Common_Constant.TIME_SEPARATOR_SIGN + '50.209296' + EP_Common_Constant.COMMA;
        addCtlr.updateAddress();
        addCtlr.cancel();
        }
        Test.stopTest();
        System.assert(svr.isSuccess());
        System.assert(EP_Common_Util.isValidAddress);
        
    }
    
    
    static testMethod void fetchKeyTest() {
        //Test Class Fix Start
        Company__c company = EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA);
        Database.insert(company); 
        EP_Country__c country = EP_TestDataUtility.createCountryRecord('Australia','AU','Asia-Pacific');
        DataBase.insert(country);
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix);
        Account customer = EP_TestDataUtility.createSellToAccount(null, freightMatrix.id);
        customer.EP_Country__c = country.id;
        customer.EP_Is_Valid_Address__c = false;
        database.insert(customer);
        
        customer.EP_Status__C = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        customer.EP_Is_Valid_Address__c = true;    
        DataBase.Update(customer,false);
       
        Profile cscAgent = [Select id from Profile
                                Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                                limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);          
        PageReference pageRef = Page.EP_AddressValidation;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(ID_STR,customer.id);
        EP_AddressController addCtlr = new EP_AddressController();
        //Test Class Fix End
        Test.startTest();
        System.runAs(cscUser){  
            addCtlr.fetchKey();
        }
        Test.stopTest();
        System.assertNotEquals(null,addCtlr.addressContext.googleMapKey);
    }
}