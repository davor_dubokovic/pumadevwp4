/* ================================================
 * @Class Name : EP_CRM_TestTaskTrigger
 * @author : Kamendra Singh
 * @Purpose: This class is used to test EP_CRM_TaskTriggertrigger.
 * @created date: 17/04/2016
 ================================================*/
@isTest(seealldata=false)
private class EP_CRM_TestTaskTrigger{
     /* This method is used to create all test data and test before insert tasks schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testTaskInsertUpdateTrigger(){
         Test.startTest();
         // Insert Account
            Account acc=new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';
            insert acc;
            
            //Insert Opportunity
            Opportunity opp=new Opportunity();
            opp.name='Test';
            opp.Stagename='Closed Won';
            opp.Account=acc;
            opp.EP_CRM_Credit_Approval_Received__c = true;
            opp.CloseDate=Date.Today() + 5;
            insert opp;
            
            Task tempTask = new Task();
            tempTask.OwnerId = UserInfo.getUserId();
            tempTask.Subject='Donni';
            tempTask.whatId = opp.Id;

            insert tempTask ;
            
        //Check System assert---------
        Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id];
        
        //validate through system Assert 
        System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c != null);
        
        //update Event........
        tempTask .Subject = 'test subject';
        
        update tempTask ;
        
         //Check System assert---------
        Opportunity tempOpp1 = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id];
        
        //validate through system Assert 
        System.Assert(tempOpp1.EP_CRM_Last_Activity_Date__c != null);
            
        test.stopTest();
    }
     /* This method is used to create all test data and test before delete event schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testBulkTasktDeleteTrigger(){
         Test.startTest();
         // Insert Account
            Account acc=new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';
            insert acc;
            
            //Insert Opportunity
            Opportunity opp=new Opportunity();
            opp.name='Test';
            opp.Stagename='Closed Won';
            opp.Account=acc;
            opp.EP_CRM_Credit_Approval_Received__c = true;
            opp.CloseDate=Date.Today() + 5;
            insert opp;
            
            //insert Event..............
            list<Task> lstTask = new list<Task>();
            Task tempTask = new Task();
            tempTask.OwnerId = UserInfo.getUserId();
            tempTask.Subject='Donni';
            tempTask.whatId = opp.Id;
            
            Task tempTask1 = new Task();
            tempTask1.OwnerId = UserInfo.getUserId();
            tempTask1.Subject='Donni';
            tempTask1.whatId = opp.Id;
            
            lstTask.add(tempTask );
            lstTask.add(tempTask1);
            
            insert lstTask;
            
            //delete Event........
            delete tempTask ;
            
        //Check System assert---------
        Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id];
        
        //validate through system Assert 
        System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c != null);
        test.stopTest();
    }
    
     /* This method is used to create all test data and test before delete event schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testSingleTaskDeleteTrigger(){
         Test.startTest();
         // Insert Account
            Account acc=new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';
            insert acc;
            
            //Insert Opportunity
            Opportunity opp=new Opportunity();
            opp.name='Test';
            opp.Stagename='Closed Won';
            opp.Account=acc;
            opp.EP_CRM_Credit_Approval_Received__c = true;
            opp.CloseDate=Date.Today() + 5;
            insert opp;
            
            //insert Event..............
           
            Task tempTask = new Task();
            tempTask.OwnerId = UserInfo.getUserId();
            tempTask.Subject='Donni';
            tempTask.whatId = opp.Id;
            
            insert tempTask ;
            
            //delete Event........
            delete tempTask ;
            
        //Check System assert---------
        Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id];
        
        //validate through system Assert 
        System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c == null);
        test.stopTest();
    }
}