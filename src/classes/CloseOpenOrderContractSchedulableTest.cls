/*
@Author      CloudSense
@name        EP_CloseOpenOrderContractSchedulable
@CreateDate  27/04/2018
@Description Test class for the Schedulable Job: CloseOpenOrderContractSchedulable
@Version     1.0
*/
@isTest
private class CloseOpenOrderContractSchedulableTest {

    @isTest
    static void testBatch_OrderwithInvoicedDrawDownOrders(){
		//Get the Record Type ID of the Order 'Contract':
		String contractId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Contract').getRecordTypeId();

		//Insert Custom Setting (CS ORDER SETTINGS) used to store the Id of the Orchestration Process Template:
		CloseOpenOrderContractSchedulableTest.insertCustomSetting();

		//Use method from EP_TestDataUtility, which inserts records of Order and Order Line Item:
		List<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
		//Select an Order from the Wrapper:
		csord__Order__c order = orderWrapperList[0].SFOrder;

		//Select an Order Line Item linked to the previous Order:
		List<csord__Order_Line_Item__c> orderItems = [SELECT Id FROM csord__Order_Line_Item__c WHERE OrderId__c = :order.Id];
		csord__Order_Line_Item__c orderLineItem = orderItems[0];
		//Update an Order Line Item Status to make the Status different from other Order Line Items:
		// This update serves to bypass the Workflow Rule: 'Update Order Status to Invoiced'
		orderLineItem.Status__c = '';
		UPDATE orderLineItem;

		//Update the Order selected from the Wrapper to give the fields (Status AND Contract End Date) values to run the Batch:
		order.RecordTypeId = contractId;
		order.csord__Status2__c = 'Accepted';
		order.Contract_End_Date__c = Date.Today() - 1;
		UPDATE order;

		//Insert records of DrawDown Order (Record Type: 'Order'; Status: 'Invoiced'):
		CloseOpenOrderContractSchedulableTest.insertDrawDownOrders(order.Id,5,  'Invoiced');

		//Insert records of Actions (Record Type: 'Open DrawDown', Status: '03-Completed'):
		CloseOpenOrderContractSchedulableTest.insertActions(order.Id,5,  '03-Completed');

        Test.startTest();
        	Database.executeBatch(new CloseOpenOrderContractSchedulable());
        Test.stopTest();

        List<csord__Order__c> contractList = [SELECT csord__Status2__c, RecordTypeId  FROM csord__Order__c WHERE RecordTypeId =: contractId];
        System.assertEquals('Closed', contractList[0].csord__Status2__c, 'The Order should have been closed by the Batch ');

    }

	@isTest
	static void testBatch_OrderWithOpenDrawDownOrders(){
		//Get the Record Type ID of the Order 'Contract':
		String contractId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Contract').getRecordTypeId();

		//Insert Custom Setting (CS ORDER SETTINGS) used to store the Id of the Orchestration Process Template:
		CloseOpenOrderContractSchedulableTest.insertCustomSetting();

		//Use method from EP_TestDataUtility, which inserts records of Order and Order Line Item:
		List<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
		//Select an Order from the Wrapper:
		csord__Order__c order = orderWrapperList[0].SFOrder;

		//Select an Order Line Item linked to the previous Order:
		List<csord__Order_Line_Item__c> orderItems = [SELECT Id FROM csord__Order_Line_Item__c WHERE OrderId__c = :order.Id];
		csord__Order_Line_Item__c orderLineItem = orderItems[0];
		//Update an Order Line Item Status to make the Status different from other Order Line Items:
		// This update serves to bypass the Workflow Rule: 'Update Order Status to Invoiced'
		orderLineItem.Status__c = '';
		UPDATE orderLineItem;

		//Update the Order selected from the Wrapper to give the fields (Status AND Contract End Date) values to run the Batch:
		order.RecordTypeId = contractId;
		order.csord__Status2__c = 'Accepted';
		order.Contract_End_Date__c = Date.Today() - 1;
		UPDATE order;
		
		//Insert records of DrawDown Order (Record Type: 'Order',  Status != 'Invoiced'):
		CloseOpenOrderContractSchedulableTest.insertDrawDownOrders(order.Id,5,  'Open');

		//Insert records of Actions (Record Type: 'Open DrawDown',  Status: '03-Completed'):
		CloseOpenOrderContractSchedulableTest.insertActions(order.Id,5,  '03-Completed');

		Test.startTest();
		Database.executeBatch(new CloseOpenOrderContractSchedulable());
		Test.stopTest();

		List<csord__Order__c> contractList = [SELECT csord__Status2__c, RecordTypeId  FROM csord__Order__c WHERE RecordTypeId =: contractId];
		System.assertEquals('Accepted', contractList[0].csord__Status2__c, 'The Order has open DrawDown Orders so it could not be closed by bacth' +
				' and its Status should remain the same as in the beginning: Status = \'Accepted\' ');
	}

	@isTest
	static void testBatch_OrderWithOpenDrawDownOrders_AndOpenActions(){
		//Get the Record Type ID of the Order 'Contract':
		String contractId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Contract').getRecordTypeId();

		//Insert Custom Setting (CS ORDER SETTINGS) used to store the Id of the Orchestration Process Template:
		CloseOpenOrderContractSchedulableTest.insertCustomSetting();

		//Use method from EP_TestDataUtility, which inserts records of Order and Order Line Item:
		List<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
		//Select an Order from the Wrapper:
		csord__Order__c order = orderWrapperList[0].SFOrder;

		//Select an Order Line Item linked to the previous Order:
		List<csord__Order_Line_Item__c> orderItems = [SELECT Id FROM csord__Order_Line_Item__c WHERE OrderId__c = :order.Id];
		csord__Order_Line_Item__c orderLineItem = orderItems[0];
		//Update an Order Line Item Status to make the Status different from other Order Line Items:
		// This update serves to bypass the Workflow Rule: 'Update Order Status to Invoiced'
		orderLineItem.Status__c = '';
		UPDATE orderLineItem;

		//Update the Order selected from the Wrapper to give the fields (Status AND Contract End Date) values to run the Batch:
		order.RecordTypeId = contractId;
		order.csord__Status2__c = 'Accepted';
		order.Contract_End_Date__c = Date.Today() - 1;
		UPDATE order;

		//Insert records of DrawDown Order (Record Type: 'Order',  Status != 'Invoiced'):
		CloseOpenOrderContractSchedulableTest.insertDrawDownOrders(order.Id,5,  'Open');
		
		//Insert records of Actions (Record Type: 'Open DrawDown',  Status != '03-Completed'):
		CloseOpenOrderContractSchedulableTest.insertActions(order.Id,5,  '01-New');

		Test.startTest();
		Database.executeBatch(new CloseOpenOrderContractSchedulable());
		Test.stopTest();

		List<csord__Order__c> contractList = [SELECT csord__Status2__c, RecordTypeId  FROM csord__Order__c WHERE Id =: order.Id AND RecordTypeId =: contractId];
		System.assertEquals('Accepted', contractList[0].csord__Status2__c, 'The Order has open DrawDown Orders so it could not be closed by bacth' +
							' and its Status should remain the same as in the beginning: Status = \'Accepted\' ');
	}

	@isTest
	static void testSchedulableBatch() {
		//Get the Record Type ID of the Order 'Contract':
		String contractId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Contract').getRecordTypeId();

		//Insert Custom Setting (CS ORDER SETTINGS) used to store the Id of the Orchestration Process Template:
		CloseOpenOrderContractSchedulableTest.insertCustomSetting();

		//Use method from EP_TestDataUtility, which inserts records of Order and Order Line Item:
		List<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
		//Select an Order from the Wrapper:
		csord__Order__c order = orderWrapperList[0].SFOrder;

		//Select an Order Line Item linked to the previous Order:
		List<csord__Order_Line_Item__c> orderItems = [SELECT Id FROM csord__Order_Line_Item__c WHERE OrderId__c = :order.Id];
		csord__Order_Line_Item__c orderLineItem = orderItems[0];
		//Update an Order Line Item Status to make the Status different from other Order Line Items:
		// This update serves to bypass the Workflow Rule: 'Update Order Status to Invoiced'
		orderLineItem.Status__c = '';
		UPDATE orderLineItem;

		//Update the Order selected from the Wrapper to give the fields (Status AND Contract End Date) values to run the Batch:
		order.RecordTypeId = contractId;
		order.csord__Status2__c = 'Accepted';
		order.Contract_End_Date__c = Date.Today() - 1;
		UPDATE order;

		Datetime tomorrow = Datetime.now().addDays(1);
		Integer year = tomorrow.year();
		Integer month = tomorrow.month();
		Integer day = tomorrow.day();
		Integer hour = tomorrow.hour();
		Integer minute = tomorrow.minute();
		Integer second = tomorrow.second();

		Test.startTest();
		String jobId = System.schedule('CS Close Open Orders',
										second+' '+minute+' '+hour+' '+day+' '+month+' ? '+year,
										new CloseOpenOrderContractSchedulable());
        Test.stopTest();

		// Get the information from the CronTrigger API object
		CronTrigger ct = [SELECT Id,CronExpression,TimesTriggered,NextFireTime FROM CronTrigger WHERE id = :jobId];
		// Verify the expressions are the same
		System.assertEquals(second+' '+minute+' '+hour+' '+day+' '+month+' ? '+year,ct.CronExpression);
		// Verify the job has not run
		System.assertEquals(0, ct.TimesTriggered);

	}
	
	/*
	@Author      CloudSense
	@name        insertOrders
	@CreateDate  27/07/2017
	@Description Auxiliary method to insert DrawDown Orders (Orders with Record Type: 'Order'):
	@Version     1.0
	*/
	private static void insertDrawDownOrders(Id primaryOrder, Integer numberOfDrawDownOrders, String Status){
		//1) List of DrawDown Orders (Record Type: 'Order') to be inserted:
		List<csord__Order__c> drawDownOrderList = new List<csord__Order__c>();

		//2) Setup values for the fields for the DrawDown Order:  RecordTypeId, Order Name and Identification:
		String drawDownOrderId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Order').getRecordTypeId();

		String orderName = 'DrawDown Order ';
		String identification = 'spokeServiceAmsterdam';

		//3) For the Order ('Contract') given, add a number of DrawDown Orders linked to that Contract:
		for(Integer i = 0; i < numberOfDrawDownOrders; i++) {
			csord__Order__c drawDownOrder = new csord__Order__c();
			drawDownOrder.RecordTypeId = drawDownOrderId;
			drawDownOrder.Name = orderName + i;
			drawDownOrder.csord__Identification__c = identification;
			drawDownOrder.csord__Primary_Order__c = primaryOrder;
			drawDownOrder.csord__Status2__c = Status;
			drawDownOrderList.add(drawDownOrder);
		}
		INSERT drawDownOrderList;
		System.assertEquals(numberOfDrawDownOrders, [SELECT COUNT() FROM csord__Order__c WHERE RecordTypeId =:drawDownOrderId],
						numberOfDrawDownOrders +' records of DrawDown Order should have been inserted');

		//Update the Status of the previous DrawDown Orders because an existing Workflow Rule ('Update Order Status to Invoiced')  changed it to 'Invoiced':
		List<csord__Order__c> updateList = [SELECT csord__Status2__c, csord__Primary_Order__c FROM csord__Order__c WHERE csord__Primary_Order__c =: primaryOrder];
		for(csord__Order__c drawDownOrder : updateList) {
			drawDownOrder.csord__Status2__c = Status;
		}
		UPDATE updateList;
	}

	/*
	@Author      CloudSense
	@name        insertActions
	@CreateDate  27/07/2017
	@Description Auxiliary method to insert Actions (Record Type: 'Open DrawDown'):
	@Version     1.0
	*/
	private static void insertActions(Id primaryOrder, Integer numberOfActions, String Status) {
		//1) List of Actions (Record Type: 'Open Drawdown') to be inserted:
		List<EP_Action__c> actionList = new List<EP_Action__c>();

		//2) Setup values for the fields for the Action:  RecordTypeId
		String actionRecordTypeId = Schema.SObjectType.EP_Action__c.getRecordTypeInfosByName().get('Open DrawDown').getRecordTypeId();

		//3) For the Order ('Contract') given, add a number of Actions linked to that Contract:
		for(Integer i = 0; i < numberOfActions; i++) {
			EP_Action__c action = new EP_Action__c();
			action.RecordTypeId = actionRecordTypeId;
			action.EP_CSOrder__c = primaryOrder;
			action.EP_Status__c = Status;
			actionList.add(action);
		}
		INSERT actionList;
		System.assertEquals(numberOfActions, [SELECT COUNT() FROM EP_Action__c],
						numberOfActions +' records of DrawDown Order should have been inserted');
	}
	/*
	@Author      CloudSense
	@name        insertCustomSetting
	@CreateDate  27/07/2017
	@Description Auxiliary method to insert the Custom Setting (CS ORDER SETTINGS) used to store the Id of the Orchestration Process Template:
	@Version     1.0
	*/
	private static void insertCustomSetting(){
		//Call the method to Insert a record of Orchestration Process Template (which will be used to Close Orders):
		CloseOpenOrderContractSchedulableTest.insertOrchestrationProcessTemplate();

		//Create Custom Setting with the Id of the previously inserted Orchestration Process Template:
		CS_ORDER_SETTINGS__c customSetting = new CS_ORDER_SETTINGS__c();
		customSetting.Contract_Close_Orchestrator_Process_Id__c = [SELECT Id FROM CSPOFA__Orchestration_Process_Template__c].Id;

		INSERT customSetting;
		System.assertEquals(1, [SELECT COUNT() FROM CS_ORDER_SETTINGS__c], '1 Custom Setting should have been inserted');
	}

	/*
	@Author      CloudSense
	@name        insertCustomSetting
	@CreateDate  27/07/2017
	@Description Auxiliary method to insert a record of Orchestration Process Template (which will be used to Close Orders):
	@Version     1.0
	*/
	private static void insertOrchestrationProcessTemplate(){
		//Create a new record of Orchestration Process Template:
		CSPOFA__Orchestration_Process_Template__c template = new CSPOFA__Orchestration_Process_Template__c();
		template.Name = 'CSC Close Contract Template';

		INSERT template;
		System.assertEquals(1, [SELECT COUNT() FROM CSPOFA__Orchestration_Process_Template__c],'1 Orchestration Process Template should have been inserted');
	}

}