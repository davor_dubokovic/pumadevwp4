global class TrafiguraGetProductOptions implements csoe.IRemoteAction {

	global Map<String, Object> execute(Map<String, Object> inputMap) {
		Map<String, Object> returnMap = new Map<String, Object>();
		if (inputMap.get('AccountId') != null) {
			String accId = (String) inputMap.get('AccountId');
			String productType = (String) inputMap.get('ProductType');
			String orderType = (String) inputMap.get('OrderType');
			String supplyLocationId = (String) inputMap.get('LocationId');
			List<EP_Product_Option__c> productOptions = [
				select Price_List__c 
				from EP_Product_Option__c 
				where Sell_To__c = :accid
				and Price_List__r.IsActive = true
			];
			List<Id> pricebooks = new List<Id>(); 

			for (EP_Product_Option__c prodOp : productOptions) {
				pricebooks.add(prodOp.Price_List__c);
			}
			String productQuery = 'SELECT name, ProductCode, Product2.EP_Product_Sold_As__c , Product2.id, Product2.name, Product2.EP_Dirty_Product__c, Product2.EP_Company__c, Product2.Family, Product2.EP_Unit_of_Measure__c, Product2.EP_Min_Order_Qty__c from PricebookEntry where Pricebook2.Id IN :pricebooks and IsActive = true';
			String productTypeQuery = '';

			if (productType != null && orderType != null) {
				if (productType.equals(EP_OrderConstant.productTypeBulk)) {
					if (orderType.equalsIgnoreCase(EP_OrderConstant.salesOrderType)) {
						productTypeQuery = ' AND (Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeBulk + '\' OR Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeService + '\')';
					} else {
						productTypeQuery = ' AND (Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeBulk + '\')';
					}
				} else if(productType.equals(EP_OrderConstant.productTypeService)) {
					productTypeQuery = ' AND Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeService + '\'';
				} else if (productType.equals(EP_OrderConstant.productTypePackaged)) {
					if (orderType.equalsIgnoreCase(EP_OrderConstant.salesOrderType)) {
						productTypeQuery = ' AND (Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypePackaged + '\' OR Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeService + '\')';
					} else {
						productTypeQuery = ' AND (Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypePackaged + '\')';
					}
				}
			}
			List<PricebookEntry> products = Database.query(productQuery + productTypeQuery);
			Set<Id> product2Ids = new Set<Id>();
			for (PricebookEntry pbe : products) {
				product2Ids.add(pbe.Product2.id);
			}
			List<EP_Inventory__c> inventories = [
				select id, EP_Product__c, EP_Inventory_Availability__c, EP_Storage_Location__c
				from EP_Inventory__c
				where EP_Product__c in :product2Ids
			];
			List<Account> accs = [
				select id, name, EP_Puma_Company__c
				from Account
				where id = :accId
			];
			if (!accs.isEmpty()) {
				List<EP_Total_Order_Quantity_Constraint__c> quantities = [
					select Total_Order_Min_Quantity__c, EP_Total_Order_Max_Quantity__c, EP_Product__c, EP_Item_Category__c, EP_Delivery_Type__c, 
						EP_Company__c
					from EP_Total_Order_Quantity_Constraint__c
					where EP_Product__c in :product2Ids
				];
				returnMap.put('quantities', quantities);
			}
			List<EP_Stock_Holding_Location__c> stockLocation = [
				select id, name, Stock_Holding_Location__c
				from EP_Stock_Holding_Location__c
				where id = :supplyLocationId
			];
			returnMap.put('products', products);
			returnMap.put('inventories', inventories);
			returnMap.put('stockLocation', stockLocation);
		}
		return returnMap;
	}
	
}