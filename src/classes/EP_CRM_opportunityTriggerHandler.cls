/* ================================================
 * @Class Name : EP_CRM_opportunityTriggerHandler 
 * @author : Kamendra Singh
 * @Purpose: 
 * @created date: 08/07/2016
 * @Modified By: Shailla Gokhale, for s2s functionality
 ================================================*/
public with sharing class EP_CRM_opportunityTriggerHandler{
    private static set<id> accIds = new set<id>();
    public static Boolean isAfterInsertRecursiveTrigger = False; 
    public static Boolean isBeforeInsertRecursiveTrigger = False; 
    public static final String CLOSED_WON = 'Closed Won';
    public static final String CLOSED_LOST = 'Closed Lost' ;
  

    /* This method is execute after opportunity record updation.   
     * @description : This method will create Opportunity records on the secondary connection org    
     * @param : Opportunity record list    
     * @return: NA    
    */    
    public static void afterInsertConnectionOpportunity(list<Opportunity> TriggerNew){        
        try{    
            // Define connection id     
            Id networkId = ConnectionHelper.getConnectionId(System.Label.Connection_Name);          
            Set<Id> localOpportunitySet = new Set<Id>();     
            List<Opportunity> localContacts = new List<Opportunity>();     
            Set<Id> sharedAccountSet = new Set<Id>();           
            // only share records created in this org, do not add contacts received from another org.     
            for (Opportunity newOpportunity : TriggerNew) {         
                if (newOpportunity.ConnectionReceivedId == null && newOpportunity.AccountId != null ) {             
                    localOpportunitySet.add(newOpportunity.AccountId);             
                    localContacts.add(newOpportunity);         
                }            
            }         
            if (localOpportunitySet.size() > 0) {         
                // Get the Opportunity account's partner network record connections         
                for (PartnerNetworkRecordConnection accountSharingRecord : [SELECT p.Status, p.LocalRecordId, p.ConnectionId  FROM PartnerNetworkRecordConnection p WHERE p.LocalRecordId IN :localOpportunitySet]) { 
                    // for each partner connection record for Opportunity's account, check if it is active             
                    if ((accountSharingRecord.status.equalsignorecase('Sent') || accountSharingRecord.status.equalsignorecase('Received')) && (accountSharingRecord.ConnectionId == networkId)) {                 
                        sharedAccountSet.add(accountSharingRecord.LocalRecordId); 
                    } 
                } 
                
                if (sharedAccountSet.size() > 0) {             
                    List<PartnerNetworkRecordConnection> opportunityConnections =  new  List<PartnerNetworkRecordConnection>(); 
                    for (Opportunity newOpportunity : localContacts) { 
                        if (sharedAccountSet.contains(newOpportunity.AccountId)) {
                            PartnerNetworkRecordConnection newConnection = new PartnerNetworkRecordConnection( 
                            ConnectionId = networkId,                           
                            LocalRecordId = newOpportunity.Id,                           
                            SendClosedTasks = false,                          
                            SendOpenTasks = false,                           
                            SendEmails = false,                           
                            ParentRecordId = newOpportunity.AccountId);                                                
                            opportunityConnections.add(newConnection);                                 
                        }             
                    }               
                    if (opportunityConnections.size() > 0 ) {                    
                        database.insert(opportunityConnections);             
                    }         
                }      
            }
        }   
        catch(exception ex){
            ex.getmessage(); 
        }   
    }  
    
    public static void beforeInsertOpportunity(list<Opportunity> TriggerNew){ 
        
        map<id,account> accounts = new map<id,account>();
        for(opportunity o:TriggerNew) {
          accounts.put(o.accountid,null);
        }
        accounts.remove(null);
        accounts.putAll([select id,name,EP_Puma_Company__c from account where id in :accounts.keyset()]);
        for(opportunity o : TriggerNew) {
          if(accounts.containskey(o.accountid)) {    
              o.EP_CRM_Puma_Company__c = accounts.get(o.accountid).EP_Puma_Company__c ;
              system.debug('New Oppty is>>>>>'+o); 
              system.debug('New Oppty EP_CRM_Puma_Company__c is>>>>>'+o.EP_CRM_Puma_Company__c);   
          }
        }   
    }          
}