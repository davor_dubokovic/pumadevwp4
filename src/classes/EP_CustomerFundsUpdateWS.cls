/* 
   @Author 			Accenture
   @name 			EP_CustomerFundsUpdateHandler
   @CreateDate 		03/10/2017
   @Description		Web service class to update available funds of customer i.e. inbound interface to update customers funds
   @Version 		1.0
*/
@RestResource(urlMapping='/v1/AccountAvailableFundSync/*')
global without sharing class EP_CustomerFundsUpdateWS {
	/**
	* @author 			Accenture
	* @name				updateCustomerFunds
	* @date 			03/13/2017
	* @description 		Processess the account available fund request and sends back the acknowledgement
	* @param 			NA 
	* @return 			NA
	*/
    @HttpPost
    global static void updateCustomerFunds(){
    	RestRequest req = RestContext.request;
        String requestBody = req.requestBody.toString();
        RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
        //EP_IntegrationService service = new EP_IntegrationService();
        //string response = service.handleCustomerFundsUpdate(requestBody);
        EP_IntegrationService service = new EP_IntegrationService();
        string response =service.handleRequest(EP_Common_Constant.NAV_TO_SFDC_CUSTOMER_FUNDS_UPDATE,requestBody);
        RestContext.response.responseBody = blob.valueOf(response);
    } 
}