/*   
     @Author Aravindhan Ramalingam
     @name <EP_OrderStateAccepted.cls>     
     @Description < Order State for Accepted status common for all OrderState>   
     @Version <1.1> 
     */

     public class EP_OrderStateAccepted extends EP_OrderState {
        
        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            super.setOrderDomainObject(currentOrder);
        }
        
        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_OrderStateAccepted','doOnEntry');
            
        }
        
        public override void doOnExit(){
        	//Defect Fix Start #57588 - Do not send Outbound SYNC Request to WINDMS if EP_Use_Managed_Transport_Services__c is true 
            EP_GeneralUtility.Log('Public','EP_OrderStateAccepted','doOnExit');
            if(!this.order.isRetrospective() && !EP_OrderStateAccepted.getTextValue().equalsIgnoreCase(this.order.previousStatus)
            	&& !EP_Common_Constant.STRING_TRUE.equalsIgnoreCase(this.order.getOrder().EP_Use_Managed_Transport_Services__c) ){
                this.orderService.doSyncStatusWithWindms();
            }
            //Defect Fix End #57588
        }
        
        
        public static String getTextValue()
        {
            return EP_OrderConstant.OrderState_Accepted;
        }
}