/**
 * Helper class for easy-to-create SOQLs.
 */
global virtual class SOQLBuilder { 

  @TestVisible
  private static final String COLUMN_JOIN_DELIMITER = ', ';

  /**
   * Object over which you wish to make SOQL statement.
   */
  global Schema.SObjectType objectType {
    get;
    protected set {
      if (value == null) {
        // 'Object type must be defined for SOQLBuilder to work properly.'
        throw new ObjectNotQueryableException(
          Label.soqlbuildercls_object_type_must_be_defined_for_SOQLBuilder
        );
      }

      DescribeSObjectResult describer = value.getDescribe();
      if (!describer.isQueryable()) {
        // String.valueOf(objectType) + ' is not queryable therefore SOQL builder cannot be used over it.'
        throw new ObjectNotQueryableException(
          String.format(
            Label.soqlbuildercls_is_not_queryable,
            new String[] { String.valueOf(value) }
          )
        );
      }

      this.objectType = value;

      this.resetAll();
    }
  }

  /**
   * Select columns (order not important). If it's empty all of fields from type will be selected
   */
  @TestVisible
  private Set<String> selectColumns = new Set<String>();

  /**
   * Order by columns.
   * @see OrderByExpression
   */
  @TestVisible
  private List<OrderByExpression> orderByColumns = new List<OrderByExpression>();

  /**
   * Where clause build elements.<br>
   * Map keys are {@link WhereClauseExpression} identifies, while values where clause descriptions.
   *
   * For example, if you wish to build "EXPR1 OR EXPR2" where clause, this map would contain
   * two keys "EXPR1" and "EXPR2".
   *
   * @see WhereClauseExpression
   */
  @TestVisible
  private Map<String, WhereClauseExpression> whereClauseExpressions = new Map<String, WhereClauseExpression>();

  /**
   * Collection for expressions with no identifer.<br>
   * Values from this set will be appended with AND after regular where clause
   */
  @TestVisible
  private Set<WhereClauseExpression> whereClausesWithoutIdentifier = new Set<WhereClauseExpression>();

  /**
   * Offset param of SOQL.<br>
   * Must be greater or equal to zero.
   */
  global Integer offset {
    get;
    set {
      if (value < 0) { // Null safe check. False if value = null
        // Offset must be zero or greater.
        throw new QueryBuildingException(Label.soqlbuildercls_Offset_cannot_be_less_than_0);
      }

      this.offset = value;
      this.ensureQueryRebuild();
    }
  }

  /**
   * Limit param of SOQL.<br>
   * Must be greater or equal to zero.
   */
  global Integer fetchLimit {
    get;
    set {
      if (value <= 0) { // Null safe check. False if value = null
        // Limit must be greater or equal to zero.
        throw new QueryBuildingException(Label.soqlbuildercls_Limit_cannot_be_less_than_0);
      }

      this.fetchLimit = value;
      this.ensureQueryRebuild();
    }
  }

  /**
   * Built SOQL query string. Null indicates that query has to be built or built again.
   */
  @TestVisible
  private String query;

  /**
   * Where clause logic.<br>
   * Use identifier from whereClauseParams to set logic for WHERE clause building.<br><br>
   * <b>If not defined</b> all where clause expressions are concatenated with AND between.
   * Use appropriate identifiers in where clause logic and provide where clause expressions with same identifiers.<br><br>
   *
   * For example:
   * <code>whereClauseLogic = 'A AND (B OR C)';</code>
   * <code>Map<String, String> whereClauseExpressions = {
   *       'A' => '1 = 1',
   *       'B' => 'columnX__c == \'test\'',
   *       'C' => 'columnY__c >= 0'
   *     };</code>
   * <br>
   * Will produce where clause <i>1 = 1 AND (columnX__c == 'test' OR columnY__c >= 0)</i>
   *
   * If you for example change where clause logic to null, wherclause will then be
   * <i>1 = 1 AND columnX__c == 'test' AND columnY__c >= 0</i>
   */
  global String whereClauseLogic {
    get;
    set {
      this.whereClauseLogic = value;
      this.ensureQueryRebuild();
    }
  }

  /*
   * Whether to include all parent fields or not when building select * from statement.
   *<br><br>
   * Defaults to <code>false</code>
   */
  global Boolean includeParentFieldsOnStarSelect {
    get {
      if (this.includeParentFieldsOnStarSelect == null) {
        this.includeParentFieldsOnStarSelect = false;
      }
      return this.includeParentFieldsOnStarSelect;
    }
    set;
  }

  /**
   * SOQL builder for a given SObjectType. You cannot change object type in future, be cautious.
   * @param forObject Object over which you wish to make SOQL statement.
   */
  global SOQLBuilder(Schema.SObjectType objectType) {
    this.objectType = objectType;
  }

  /**
   * Empty constructor to ensure that extending class can set type later on in process.
   */
  global SOQLBuilder() {
  }

  /**
   * Resets all clauses
   */
  global virtual void resetAll() {
    this.clearWhereClause();
    this.whereClauseLogic = null;
    this.offset = null;
    this.fetchLimit = null;
    this.clearSelectClause();
    this.clearOrderByColumns();

    this.ensureQueryRebuild();
  }

  /**
   * Adds where clause element or criterion.
   * If <code>whereClauseLogic</code> is used please support right identifers (identifiers from <code>whereClauseLogic</code>).<br>
   * If identifer is an empty string or you get empty string by trimming it, expression will be appended at the end of where clause separated with AND.
   * <br>
   *
   * Example code:
   * <code>
   *     whereClauseLogic = 'A AND (B OR C)';
   *     addWhereClauseExpression('A', '1 = 1');
   *     addWhereClauseExpression('B', 'columnX__c == \'test\'');
   *     addWhereClauseExpression('C', 'columnY__c >= 0');
   *     System.assertEquals(buildWhereClause(), '1 = 1 AND (columnX__c == 'test' OR columnY__c >= 0)');
   *
   *     addWhereClauseExpression(null, '2 = 1');
   *     System.assertEquals(buildWhereClause(), '(1 = 1 AND (columnX__c == 'test' OR columnY__c >= 0)) AND 2 = 1');
   * </code>
   * @param identifier Expression identifier for whereClauseLogic usage (if null or trimmed empty, expression will be appended after transformed whereClauseLogic)
   * @param expression Expression to be injected to SOQL where clause
   * @return Newly created WhereClauseExpression with provided identifier and where clause expression
   * @see whereClauseLogic
   * @see WhereClauseExpression
   */
  global virtual WhereClauseExpression addWhereClauseExpression(String identifier, String expression) {
    if (expression == null) {
      // 'Where clause expression most not be null.'
      throw new QueryBuildingException(Label.soqlbuildercls_Where_clause_expression_most_not_be_null);
    }

    WhereClauseExpression entity = new WhereClauseExpression(identifier, expression);
    this.addWhereClauseExpression(entity);

    return entity;
  }

  /**
   * Adds where clause element or criterion.
   * @param entity Prepared entity. Be cautious about identifiers
   * @see addWhereClauseExpression(String identifier, String whereClauseExpression)
   * @see whereClauseLogic
   * @see WhereClauseExpression
   */
  global virtual void addWhereClauseExpression(WhereClauseExpression entity) {
    // if identifer is empty string or consists only of space elements (tabs etc), identifer is not valid
    if (entity.identifier == null || entity.identifier.trim() == '') {
      this.whereClausesWithoutIdentifier.add(entity);
    } else {
      this.whereClauseExpressions.put(entity.identifier, entity);
    }
    this.ensureQueryRebuild();
  }

  /**
   * Adds where clause entity which will be appended after whereClauseLogic cluster with AND logic used.<br>
   * <i>{transformed whereClauseLogic} AND <code>whereClauseExpression</code></i>
   * @param expression Expression to be injected to SOQL where clause
   * @return Newly created WhereClauseExpression with empty identifier and provided where clause expression
   * @see WhereClauseExpression
   */
  global virtual WhereClauseExpression addWhereClauseExpression(String expression) {
    return addWhereClauseExpression(null, expression);
  }

  /**
   * Removes where clause condition expression from where clause.
   * @param entity An where clause condition expression you wish to remove
   */
  global virtual void removeWhereClauseExpression(WhereClauseExpression expression) {
    if (expression == null) {
      return;
    }

    Boolean whereClauseHasExpr = this.whereClauseExpressions.containsKey(expression.identifier);
    Boolean whereClauseWOIndentifierHasExpr = this.whereClausesWithoutIdentifier.contains(expression);
    Boolean whereClauseExpresionIsExpression = this.whereClauseExpressions.get(expression.identifier) != expression;
    Boolean isNonIndetifiableNotInBuilder = !whereClauseHasExpr && !whereClauseWOIndentifierHasExpr;
    Boolean hasKeyWithDifferentReference = whereClauseHasExpr && whereClauseExpresionIsExpression;
    // null entites as parameter are ignored
    if (isNonIndetifiableNotInBuilder || hasKeyWithDifferentReference) {
      return;
    }

    this.whereClausesWithoutIdentifier.remove(expression);
    this.whereClauseExpressions.remove(expression.identifier);
    this.ensureQueryRebuild();
  }

  /**
   * Removes all where clause entities from SOQL builder.
   */
  global virtual void clearWhereClause() {
    this.whereClauseExpressions.clear();
    this.whereClausesWithoutIdentifier.clear();
    this.ensureQueryRebuild();
  }

  /**
   * Validation for columnApiName method.
   */
  global virtual void validateColumnApiName(String columnApiName) {
    if (columnApiName == null) {
      // 'Select column cannot be null in SOQL.'
      throw new SOQLBuilderValidationException(Label.soqlbuildercls_Select_column_cannot_be_null_in_SOQL);
    }

    Schema.SObjectField describeField = this.objectType.getDescribe().fields.getMap().get(columnApiName);

    if (describeField == null) {
      // columnApiName + ' does not exist as an attribute in "' + String.valueOf(this.objectType) + '" object.'
      throw new SOQLBuilderValidationException(
        String.format(
          Label.soqlbuildercls_Failed_to_validate_expression,
          new String[] { columnApiName, String.valueOf(this.objectType) }
        )
      );
    }
  }

  /**
   * Adds column to select clause.
   * @param columnApiName Column you wish to add to select statement.
   */
  global virtual void selectColumn(String columnApiName) {
    validateColumnApiName(columnApiName);
    this.selectColumns.add(columnApiName.toLowerCase());
    this.ensureQueryRebuild();
  }

  /**
   * Adds all columns to select clause.
   * @param columnApiNames Columns you wish to add to select statement.
   */
  global virtual void selectColumns(String[] columnApiNames) {
    if (columnApiNames == null) {
      return;
    }
    // we have to validate each column api name recieved
    for (String columnApiName : columnApiNames) {
      validateColumnApiName(columnApiName);
      this.selectColumns.add(columnApiName.toLowerCase());
    }

    this.ensureQueryRebuild();
  }

  /**
   * Removes column from select clause.
   * @param columnApiName Column you wish to remove from select statement.
   */
  global virtual void removeSelectColumn(String columnApiName) {
    if (columnApiName != null) {
      columnApiName = columnApiName.toLowerCase();
    }
    this.selectColumns.remove(columnApiName);
    this.ensureQueryRebuild();
  }

  /**
   * Removes columns from select clause.
   * @param columnApiNames Columsn you wish to remove from select statement.
   */
  global virtual void removeSelectColumns(String[] columnApiNames) {
    for (String columnApiName : columnApiNames) {
      this.removeSelectColumn(columnApiName);
    }
  }

  /**
   * Clears select clause. Be aware that SOQL creation is behaving like "select * from table"
   */
  global virtual void clearSelectClause() {
    this.selectColumns.clear();
  }

  /**
   * Adds soqlExpression to sort columns.
   * @param soqlExpression soqlExpression you wish to add
   * @return newly created and added OrderByExpression
   *
   * @see OrderByExpression
   */
  global virtual OrderByExpression addSortExpression(String soqlExpression) {
    return addSortExpression(soqlExpression, SortOrder.SORT_DEFAULT, NullSortOrder.NULLS_DEFAULT);
  }

  /**
   * Adds soqlExpression to sort colums sorting it by defined sort order.
   * @param soqlExpression soqlExpression you wish to add
   * @param sortDirection Sort direction
   * @return newly created and added OrderByExpression
   *
   * @see OrderByExpression
   * @see SortOrder
   */
  global virtual OrderByExpression addSortExpression(String soqlExpression, SortOrder sortDirection) {
    return addSortExpression(soqlExpression, sortDirection, NullSortOrder.NULLS_DEFAULT);
  }

  /**
   * Adds soqlExpression to sort colums sorting it by defined sort order.
   * @param soqlExpression soqlExpression you wish to add
   * @param sortDirection Sort direction
   * @param nullSortDirection Null sort direction
   * @return newly created and added OrderByExpression
   *
   * @see OrderByExpression
   * @see SortOrder
   * @see NullSortOrder
   */
  global virtual OrderByExpression addSortExpression(String soqlExpression, SortOrder sortDirection, NullSortOrder nullSortDirection) {
    OrderByExpression anEntity = new OrderByExpression(soqlExpression, sortDirection, nullSortDirection);
    this.addToOrderBy(anEntity);

    return anEntity;
  }

  /**
   * Adds order by entity to order by clause
   * @see OrderByExpression
   */
  global virtual void addToOrderBy(OrderByExpression descriptor) {
    this.orderByColumns.add(descriptor);
    this.ensureQueryRebuild();
  }

  /**
   * Removes order by entity from order by clause
   * @see OrderByExpression
   
  global virtual void removeFromOrderBy(OrderByExpression descriptor) {
    if (Utils.removeFromListByReference(this.orderByColumns, descriptor)) {
      this.ensureQueryRebuild();
    }
  }*/

  /**
   * Removes all order by entitis from order by clause
   * @see OrderByExpression
   */
  global virtual void clearOrderByColumns() {
    this.orderByColumns.clear();
    this.ensureQueryRebuild();
  }

  /**
   * Request a query rebuild
   */
  global virtual void ensureQueryRebuild() {
    this.query = null;
  }

  /**
   * Builds query from scratch on each invocation.<br>
   * Query is built using given select, where and order by parameters.<br><br>
   * Use this method only if you want to have newly created query built regardless to fact that
   * query for current state of builder may already have been built.
   */
  global virtual String buildQuery() {
    this.query = new StringBuilder()
      // select statement
      .append(this.createSelectStatement())
      // from clause
      .append(this.createFromStatement())
      // where clause
      .append(this.createWhereStatement())
      // order by clause
      .append(this.createOrderByStatement())
      // limit clause
      .append(this.createLimitStatement())
      // offset clause
      .append(this.createOffsetStatement())
      // generate result
      .toString();

    return this.query;
  }

  /**
   * Builds query form strach if query building model was changed. For example if you added new select column, remove it or change
   * any other clause parameters. Otherwise, returns lastly built query.<br>
   * Using this method you ensure optimized usage of resources unlike with usage of buildQuery method.
   */
  global virtual String getSOQL() {
    if (this.query == null) {
      return buildQuery();
    }
    return this.query;
  }

  /*
   * Executes built query and returns results.
   */
  global virtual List<SObject> executeQuery() {
    return Database.query(this.getSOQL());
  }

  /**
   * Creates from statement as ' from ' + objectType
   */
  @TestVisible
  private String createFromStatement() {
    return ' from ' + String.valueOf(this.objectType);
  }

  /**
   * Creates limit statement.
   * @return <code>' limit ' + fetchLimit</code>, empty string if fetchLimit not defined
   */
  @TestVisible
  private String createLimitStatement() {
    if (this.fetchLimit != null) {
      return ' limit ' + this.fetchLimit;
    }
    return '';
  }

  /**
   * Creates offset statement.
   * @return <code>' offset ' + offset</code> or empty string if offset not defined
   */
  @TestVisible
  private String createOffsetStatement() {
    if (this.offset != null) {
      return ' offset ' + this.offset;
    }
    return '';
  }

  /**
   * Calcualtes 'select ' + list of select columns separated with ',' string
   * @see buildSelectClause
   */
  @TestVisible
  private String createSelectStatement() {
    String builtSelectClause = this.buildSelectClause();
    if (builtSelectClause == null || builtSelectClause.trim().length() <= 0) {
      // 'Select clause at SOQL builder not built correctly.'
      throw new QueryBuildingException(Label.soqlbuildercls_Select_clause_at_SOQL_builder_not_built_correctly);
    }
    return 'select ' + builtSelectClause;
  }

  /**
   * Builds select clause.<br>
   * If <b>no column</b> defined for select clause, select columns are read from objectType.<br><br>
   * If overriding, be sure not to add 'select' keyword
   * @return Select clause for givven sql. Never returns <code>null</code>
   */
  @TestVisible
  protected virtual String buildSelectClause() {
    // New list is introduced to not have duplicate code for select clause build.
    // Idea is to fill it with all columns if no select column defined
    List<String> columnsToAppend = new List<String>();
    if (this.selectColumns == null || this.selectColumns.isEmpty()) {
      Schema.DescribeSObjectResult targetType = this.objectType.getDescribe();

      for (Schema.SObjectField field : targetType.fields.getMap().values()) {
        Schema.DescribeFieldResult describedField = field.getDescribe();
        if (isFieldSelectable(describedField)) {
          columnsToAppend.add(String.valueOf(field));

          if (this.includeParentFieldsOnStarSelect) {
            List <Schema.SObjectType> referencesObjects = describedField.getReferenceTo();
            if (!referencesObjects.isEmpty()) {
              String relationshipName = describedField.getRelationshipName();
              if (describedField.isNamePointing()) {
                columnsToAppend.add(relationshipName + '.Name');
              } else {
                Schema.DescribeSObjectResult parentDescribe = referencesObjects[0].getDescribe();

                for (Schema.SObjectField parentField : parentDescribe.fields.getMap().values()) {
                  if (isFieldSelectable(parentField.getDescribe())) {
                    columnsToAppend.add(relationshipName + '.' + String.valueOf(parentField));
                  }
                }
              }
            }
          }
        }
      }

    } else {
      columnsToAppend.addAll(this.selectColumns);
    }
    return String.join(columnsToAppend, COLUMN_JOIN_DELIMITER);
  }

  /**
   * Calcualtes ' order by ' + list of order columns separated with ',' string
   * @see buildOrderByClause
   */
  @TestVisible
  private String createOrderByStatement() {
    String builtOrderByClause = this.buildOrderByClause();
    if (builtOrderByClause != null) {
      return ' order by ' + builtOrderByClause;
    }
    return '';
  }

  /**
   * Builds order by clause.<br>
   * If overriding, be sure not to add 'order by' keywords
   * @return Order by clause. If <b>no column</b> defined for order by clause, <code>null is returned</code>.<br><br>
   */
  @TestVisible
  protected virtual String buildOrderByClause() {
    if (this.orderByColumns == null || this.orderByColumns.isEmpty()) {
      return null;
    }

    List<String> orderByClauses = new List<String>();
    for (OrderByExpression orderByEntity : this.orderByColumns) {
      orderByClauses.add(orderByEntity.createSOQL());
    }

    return String.join(orderByClauses, COLUMN_JOIN_DELIMITER);
  }

  /**
   * Calcualtes ' where ' + created where statement if any where clause expression is provided.
   * @see buildWhereClause
   */
  @TestVisible
  private String createWhereStatement() {
    String whereClause = buildWhereClause();
    if (whereClause != null) {
      return ' where ' + whereClause;
    }
    return '';
  }

  /**
   * Builds where clause.<br>
   * If overriding, be sure not to add 'where' keyword.<br><br>
   * Where clause building transforces whereClauseLogic replacing all identifiable provided expressions with identifiers
   * from logic after which it appends unidentifiable expressions with AND logic.
   * Also, if no logic provided all expressions will be injected into one big AND expression
   * @return Where clause. If <b>no expression</b> defined for where clause, <code>null is returned</code>.<br><br>
   */
  @TestVisible
  protected virtual String buildWhereClause() {

    // If no expression return null
    if (this.whereClauseExpressions.isEmpty()
      && this.whereClausesWithoutIdentifier.isEmpty()) {
      return null;
    }

    // Expressions which will be separated with AND and will be on first level of expression tree
    // Set is used to have multiple exactly the same expressions presented as single one (it is AND logic after all)
    Set<String> andPartExpressions = new Set<String>();
    String whereClause = '';
    Boolean whereLogicExists = this.whereClauseLogic != null && !String.isEmpty(this.whereClauseLogic.trim());
    if (whereLogicExists) {
      whereClause = this.whereClauseLogic;
    }

    for (String identifier : this.whereClauseExpressions.keySet()) {
      WhereClauseExpression whereExpression = this.whereClauseExpressions.get(identifier);

      String whereSOQL = whereExpression.getSOQL();
      if (whereExpression == null || whereSOQL == null
        || String.isEmpty(whereSOQL.trim())) {
        // 'A where clause entity with identifier ' + identifier + ' is empty and cannot be injected to SOQL'
        throw new QueryBuildingException(
          String.format(
            Label.soqlbuildercls_where_clause_entity_is_empty,
            new String[] { identifier }
          )
        );
      }

      if (!whereLogicExists) {
        andPartExpressions.add(surroundWithBrackets(whereSOQL));
      } else {
        // Not best pracitce. For example, in previously created expressions, one can accidentally use next identifier and crash query
        // Also, someone cannot build logic in a sense {id1}AND{id2} because it will not be replaced.
        // This approach was previously used, so it's ok to sktick with it.
        // TODO in later versions of lookup refactoring, do invent SOQL expressions and build SOQLs out of it
        whereClause = whereClause.replaceAll('\\b' + identifier + '\\b', surroundWithBrackets(whereSOQL.trim()));
      }
    }

    for (WhereClauseExpression anEntity : this.whereClausesWithoutIdentifier) {
      andPartExpressions.add(surroundWithBrackets(anEntity.getSOQL()));
    }

    if (whereLogicExists && !andPartExpressions.isEmpty()) {
      // Surrounds where clause with brackets to be able to append and statements to it
      // Also, appends first and if there are more expressions with and included
      whereClause = '(' + whereClause + ') and ';
    }

    // Now append all and expressions after logic expressions
    whereClause += String.join(new List<String>(andPartExpressions), ' and ');

    return whereClause;
  }

  private String surroundWithBrackets(String s) {
    return '(' + s + ')';
  }

  /**
   * Checks if field is selectable.<br>
   * Current foruma is <code>isAccessible() && !isDeprecatedAndHidden()</code>
   */
  @TestVisible
  protected virtual Boolean isFieldSelectable(Schema.DescribeFieldResult fieldDescriber) {
    return fieldDescriber.isAccessible() && !fieldDescriber.isDeprecatedAndHidden();
  }

  /**
   * Where clause entity information holder.
   * It's whereClauseExpression will be injected into SOQL builder class if its appended ti SOQLBuilder.
   * If identifier not provided, it will be injected with AND logic to where clause
   */
  global class WhereClauseExpression {
    /**
     * Where clause entity identifer. Used to be recognizable in combination with whereClauseLogic property of SOQL builder.<br>
     * Please do use unique identifiers here
     */
    global String identifier { get; set; }

    /**
     * Where clause expression which will be injected in SOQL by SOQLBuilder class.<br>
     * <b>It is expected that its syntax is correct.</b>
     */
    global String expression { get; set; }

    /**
     * Default where clause entity constructor.
     * @param identifier Where clause expression identifier which will be used in combination with whereClauseLogic property of SOQLBuilder.
     *       If null, <code>whereClauseExpression</code> will be appended with AND logic
     * @param whereClauseExpression SOQL expression which will be injected into SOQL
     */
    global WhereClauseExpression(String identifier, String expression) {
      this.identifier = identifier;
      this.expression = expression;
    }

    /**
     * Generates its SOQL.<br>
     * For now it is equals to whereClauseExpression
     */
    global virtual String getSOQL() {
      return this.expression;
    }
  }

  /**
   * Sort expression (column for sort) with its description data.
   * @see  SortOrder
   * @see  SortNullOrder
   */
  global class OrderByExpression{
    /**
     * Defined sort type (asc, desc or default)
     * @see  SortOrder
     */
    global SortOrder sortDirection { get; set; }

    /**
     * SOQL syntax correct order expression
     */
    global String orderExpression { get; set; }

    /**
     * Given sort type for sort (asc, desc or default)
     * @see SortNullOrder
     */
    global NullSortOrder nullSortDirection { get; set; }

    /**
     * Default order by for given SOQL expression
     * @param orderExpression SOQL syntax correct order expression
     */
    global OrderByExpression(String orderExpression) {
      this.orderExpression = orderExpression;
      this.sortDirection = SortOrder.SORT_DEFAULT;
      this.nullSortDirection = NullSortOrder.NULLS_DEFAULT;
    }

    /**
     * Defines order by for given SOQL expression
     * @param orderExpression SOQL syntax correct order expression
     * @param sortDirection Given sort type for sort (asc, desc or default)
     */
    global OrderByExpression(String orderExpression, SortOrder sortDirection) {
      this(orderExpression);
      this.sortDirection = sortDirection;
    }

    /**
     * Defines order by for given SOQL expression
     * @param orderExpression SOQL syntax correct order expression
     * @param sortDirection Given sort type for sort (asc, desc or default)
     * @param nullSortDirection Given null sort type for sort SOQL creation (nulls first, nulls last or default)
     */
    global OrderByExpression(String orderExpression, SortOrder sortDirection, NullSortOrder nullSortDirection) {
      this(orderExpression, sortDirection);
      this.nullSortDirection = nullSortDirection;
    }

    /**
     * Creates SOQL element for sort type in format:<br>
     * {orderExpression} {sortDirection}? {nullsortDirection}?
     */
    global virtual String createSOQL() {
      if (this.orderExpression == null) {
        return null;
      }

      String sortSOQL = this.orderExpression;
      sortSOQL += (this.sortDirection == SortOrder.SORT_ASC ? ' asc' : (this.sortDirection == SortOrder.SORT_DESC ? ' desc' : ''));
      sortSOQL += (this.nullSortDirection == NullSortOrder.NULLS_FIRST ? ' nulls first' : (this.nullSortDirection == NullSortOrder.NULLS_LAST ? ' nulls last' : ''));

      return sortSOQL;
    }
  }

  /**
   * Enumerator for order by type.
   */
  global enum SortOrder {
    /**
     * ASC sort
     */
    SORT_ASC,
    /**
     * DESC sort
     */
    SORT_DESC,
    /**
     * Salesforce defined default sort (SOQL empty)
     */
    SORT_DEFAULT
  }

  /**
   * Nulls first, nulls last or default null order.
   */
  global enum NullSortOrder {
    /**
     * NULLS FIRST sort
     */
    NULLS_FIRST,
    /**
     * NULLS LAST sort
     */
    NULLS_LAST,
    /**
     * Salesforce defined default null sort (SOQL empty)
     */
    NULLS_DEFAULT
  }

  /**
   * Exception during soql building process.
   */
  global virtual class SOQLBuilderException extends Exception {}

  /**
   * Exception thrown when object is not quearyable
   */
  global class ObjectNotQueryableException extends SOQLBuilderException {}

  /**
   * Exception thrown when object is not quearyable
   */
  global class QueryBuildingException extends SOQLBuilderException {}

  /**
   * Exception thrown when object is not quearyable
   */
  global class SOQLBuilderValidationException extends SOQLBuilderException {}
}