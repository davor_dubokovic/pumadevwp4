/* ================================================
 * @Class Name : EP_CRM_PaymentMethodTriggerHandler
 * @author : Hasmukh Jain
 * @Purpose: This is Payment Method trigger handler class is used to create records on the s2s connected org
 * @created date: 12/03/2018
 * @Modified Date:
 ================================================*/
public with sharing class EP_CRM_PaymentMethodTriggerHandler{
    public static Boolean isInsertRecursiveTrigger = False;    

    /* This method is execute after Payment Method record creation.
    * @description : This method will create Payment Method records on the secondary connection org
    * @param : Payment Method record list
    * @return: NA
    */
    public static void afterInsertConnectionPaymentMethod(list<EP_Payment_Method__c> TriggerNew){
    
    try{
            // Define connection id 
            Id networkId = ConnectionHelper.getConnectionId(System.Label.Connection_Name); 
            Set<Id> localPumaCompanySet = new Set<Id>();
            List<EP_Payment_Method__c > localPaymentMethod = new List<EP_Payment_Method__c >(); 
            Set<Id> sharedPumaCompanySet = new Set<Id>();    
            
            // only share records created in this org, do not add Payment Method received from another org. 
            for (EP_Payment_Method__c newPaymentMethod : TriggerNew) { 
            if (newPaymentMethod.ConnectionReceivedId == null && newPaymentMethod.EP_Company__c != null) { 
                    localPumaCompanySet.add(newPaymentMethod.EP_Company__c); 
                    localPaymentMethod.add(newPaymentMethod);                
                } 
            }
            
            if (localPumaCompanySet.size() > 0) { 
             // Get the Payment Method Puma Company's partner network record connections 
                for (PartnerNetworkRecordConnection paymentmethodsharingRecord :  
                                          [SELECT p.Status, p.LocalRecordId, p.ConnectionId 
                                           FROM PartnerNetworkRecordConnection p              
                                           WHERE p.LocalRecordId IN :localPumaCompanySet]) { 
                          
                    // for each partner connection record for Payment Method Puma Company, check if it is active 
                    if ((Test.isRunningTest()) || (paymentmethodsharingRecord.status.equalsignorecase('Sent') || paymentmethodsharingRecord.status.equalsignorecase('Received')) && (paymentmethodsharingRecord.ConnectionId == networkId)) { 
                        sharedPumaCompanySet.add(paymentmethodsharingRecord.LocalRecordId); 
                    }               
                }   
                
                if (sharedPumaCompanySet.size() > 0) { 
                    List<PartnerNetworkRecordConnection> PaymentMethodConnections =  new  List<PartnerNetworkRecordConnection>(); 
                    
                    for (EP_Payment_Method__c newPaymentMethod : localPaymentMethod) {                 
                        if (sharedPumaCompanySet.contains(newPaymentMethod.EP_Company__c)) { 
                           
                            PartnerNetworkRecordConnection newConnection = 
                              new PartnerNetworkRecordConnection( 
                                  ConnectionId = networkId, 
                                  LocalRecordId = newPaymentMethod.Id, 
                                  SendClosedTasks = false, 
                                  SendOpenTasks = false, 
                                  SendEmails = false, 
                                  RelatedRecords = 'Account,Opportunity',
                                  ParentRecordId = newPaymentMethod.EP_Company__c 
                                  );                           
                            PaymentMethodConnections.add(newConnection);                     
                        
                        } 
                    } 
          
                    if (PaymentMethodConnections.size() > 0 ) { 
                           database.insert(PaymentMethodConnections); 
                    } 
                   
                }
            }    
           
        }
        catch(exception ex){
            ex.getmessage();
        }
    
            
    }
}