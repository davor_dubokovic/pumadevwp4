/****************************************************************
* @author       Accenture                                       *
* @name         EP_Send_Staging_Error_Emails_Batch              *
* @Created Date 16/04/2018                                      *
* @description  To send email notification of staging errors    *
****************************************************************/

global class EP_Send_Staging_Error_Emails_Batch implements Database.batchable<sObject>, Database.Stateful {

/****************************************************************
* @author       Accenture                                       *
* @name         setbody                                         *
* @description  method to query the required field              *
* @param        NA                                              *
* @return       string                                          *
****************************************************************/

    global Database.QueryLocator start(Database.BatchableContext BC)
     {
        return Database.getQueryLocator(EP_Send_Staging_Error_Emails_Batchhelper.start());
        
     }
     

/****************************************************************
* @author       Accenture                                       *
* @name         setbody                                         *
* @description  mMethod to build an email template              *
* @param        list>String>                                    *
* @return       NA                                              *
****************************************************************/     

    global void execute(Database.BatchableContext BC, List<EP_File__c> scope)
     {
        EP_Send_Staging_Error_Emails_Batchhelper.execute(scope);
        system.debug('===== test data =====' + scope); 
     }
    
   
    global void finish(Database.BatchableContext BC) 
     {
     } 
}