/*Class to avoid recurssion in triggers*/ 
public class EP_CheckRecursive {
    public static boolean run = true;
    public static boolean isBeforeRunning = true;
    public static boolean isAfterRunning = true;
	public static boolean isBatchRunning = false;
    public static boolean runOnce(){
        EP_GeneralUtility.Log('Public','EP_CheckRecursive','runOnce');
        if(run){
            run=false;
            return true;
            }else{
                return run;
            }
        }
        
        public static boolean runBeforeOnce(){
            EP_GeneralUtility.Log('Public','EP_CheckRecursive','runBeforeOnce');
            if(isBeforeRunning){
                isBeforeRunning=false;
                return true; 
                }else{
                    return isBeforeRunning;
                }
            }
            
            public static boolean runAfterOnce(){
                EP_GeneralUtility.Log('Public','EP_CheckRecursive','runAfterOnce');
                if(isAfterRunning){
                    isAfterRunning=false;
                    return true; 
                    }else{
                        return isAfterRunning;
                    }
                }
                
            }