/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EP_CreateShipToExtension_Test {

    /*private static String COUNTRY_NAME = 'Australia';
    private static String COUNTRY_CODE = 'AU';
    private static String COUNTRY_REGION = 'Australia';
    private static String REGION_NAME = 'North-Australia';
    
    
    static testMethod void myUnitTest() {
        
        List<selectOPtion> rtypes = new list<selectOPtion>();
        list<profile> profileLst = [Select id from profile where Name = :EP_Common_Constant.SM_AGENT_PROFILE];
        User SMTMuser = EP_TestDataUtility.createUser(profileLst[0].id);
        insert SMTMuser; 
       
        Test.startTest();
        System.runAs(SMTMuser){
            Account acc = [Select id, Name from Account limit 1];
            ApexPages.StandardController controller = new ApexPages.StandardController(acc);
            apexpages.currentPage().getParameters().put('id', acc.id);
            EP_CreateShipToExtension ext = new EP_CreateShipToExtension(controller);
            ext.selectRecordType();
            rtypes =ext.getRecordTypes();
            ext.selectRecordType();
        
        }
        
       
        test.stopTest();
        system.assertEquals(2, rtypes.size());
        
     
    }
    
    static testMethod void createShipToAsStockAccountantTest() {
        
        List<selectOPtion> rtypes = new list<selectOPtion>();
        list<profile> profileLst = [Select id from profile where Name = :EP_Common_Constant.EP_Stock_Accountant_R1];
        User stockAccountantUser = EP_TestDataUtility.createUser(profileLst[0].id);
        insert stockAccountantUser; 
        Account acc = [Select id, Name from Account limit 1];
        acc.ownerId = stockAccountantUser.Id;
        update acc;
        
        Test.startTest();
        System.runAs(stockAccountantUser){
             
            ApexPages.StandardController controller = new ApexPages.StandardController(acc);
            apexpages.currentPage().getParameters().put('id', acc.id);
            EP_CreateShipToExtension ext = new EP_CreateShipToExtension(controller);
            ext.selectRecordType();
            rtypes =ext.getRecordTypes();
            ext.selectRecordType();
        
        }
        
       
        test.stopTest();
        system.assertEquals(1, rtypes.size());
    }
*/
    public static TestMethod void initializeData_Test(){
        Account acc= EP_TestDataUtility.getSellTo();
        Test.setCurrentPageReference(new PageReference('Page.EP_CreateShipTo')); 
        System.currentPageReference().getParameters().put('id', acc.Id);
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        EP_CreateShipToExtension cse =new EP_CreateShipToExtension(controller);
        Test.startTest();
        cse.initializeData();
        Test.stopTest();
    }

    public static TestMethod void selectRecordType_Test(){
        Account acc= EP_TestDataUtility.getShipTo();
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        EP_CreateShipToExtension cse =new EP_CreateShipToExtension(controller);
        Test.startTest();
        PageReference page= cse.selectRecordType();
        Test.stopTest();
        System.assertEquals(true, page!= null);

    }

    public static TestMethod void getRecordTypes_Test(){
        Account acc= EP_TestDataUtility.getShipTo();
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        EP_CreateShipToExtension cse =new EP_CreateShipToExtension(controller);
        Test.startTest();
        List<SelectOption> lstSelectOption = cse.getRecordTypes();
        Test.stopTest();
        System.assertEquals(2,lstSelectOption.size());

    }

    public static TestMethod void queryProfiles_Test(){
        Account acc= EP_TestDataUtility.getShipTo();
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        EP_CreateShipToExtension cse =new EP_CreateShipToExtension(controller);
        List<String> lstProfileNames=new List<String>{EP_Common_Constant.SM_AGENT_PROFILE};
        Test.startTest();
        List<Profile> lstProfileObj = cse.queryProfiles(lstProfileNames);
        Test.stopTest();
        System.assertEquals(EP_Common_Constant.SM_AGENT_PROFILE,lstProfileObj[0].Name);

    }

    public static TestMethod void queryRecordTypes_Test(){
        Account acc = EP_TestDataUtility.getShipTo();
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        EP_CreateShipToExtension cse =new EP_CreateShipToExtension(controller);
        List<String> lstRecordTypeDevNames=new List<String>{EP_Common_Constant.VMI_SHIP_TO_DEV_NAME};
        Test.startTest();
        List<RecordType> lstRecordType = cse.queryRecordTypes(lstRecordTypeDevNames);
        Test.stopTest();
        System.assertEquals(EP_Common_Constant.VMI_SHIP_TO,lstRecordType[0].Name);
    }

    public static TestMethod void getSelectOptions_Test(){
        Account acc = EP_TestDataUtility.getShipTo();
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        EP_CreateShipToExtension cse =new EP_CreateShipToExtension(controller);
        List<String> lstRecordTypeNames=new List<String>{EP_Common_Constant.VMI_SHIP_TO_DEV_NAME};
        Test.startTest();
        List<SelectOption> lstSelectOption = cse.getSelectOptions(lstRecordTypeNames);
        Test.stopTest();
        System.assertEquals(1,lstSelectOption.size());

    }


}