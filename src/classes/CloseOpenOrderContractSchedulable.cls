/*
@Author      CloudSense
@name        EP_CloseOpenOrderContractSchedulable
@CreateDate  27/07/2017
@Description Schedulable Job to close Orders (Record Type: Contract) where: Contract End Date < TODAY AND Status = 'Accepted'
			 -  If there are Orders that meet the previous criteria, close those Orders.
			 -  Check if there are any Open DrawDown Orders related to the Order ('Contract') AND Status != 'Invoiced':
			 -  If there aren't any Open DrawDown Orders --> Close the Order ('Contract').
			 -  To CLose an Order update the Status to 'Closed' and create a new record Orchestration Process.
			 -  If there are any Open DrawDown Orders  --> create an Action object.
@Version     1.0
*/

global class CloseOpenOrderContractSchedulable implements Database.Batchable<sObject>, Database.stateful, Schedulable {

	global Database.QueryLocator start(Database.BatchableContext bc){
		//Get the Id of the Record Type: Contract of the Order custom object
		String contractId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Contract').getRecordTypeId();

		//Select Orders to be closed: (Record Type: Contract) where: Contract End Date < TODAY OR Available_Quantity__c <= 0:
		return Database.getQueryLocator('SELECT csord__Status2__c, Contract_End_Date__c, RecordTypeId, Available_Quantity__c ' +
										'FROM csord__Order__c ' +
										'WHERE RecordTypeId = :contractId AND (Contract_End_Date__c < TODAY OR Available_Quantity__c <= 0) ' +
										'AND csord__Status2__c = \'Accepted\' ');
	}

	global void execute(SchedulableContext sc){
		CloseOpenOrderContractSchedulable b = new CloseOpenOrderContractSchedulable();
		Database.executeBatch(b);
	}

	global void execute(Database.BatchableContext bc, List<csord__Order__c> scope){
		//Set of Orders ('Contract') from Scope:
		Set<Id> orderSet = new Set<Id>();

		//Set of Orders that could not be Closed (because they have Open DrawDown Orders, check if they have Open Actions)
		Set<Id> orderNotClosableSet = new Set<Id>();

		//Set of Orders that could not be Closed --> create an Action object::
		Set<Id> orderToCreateActionForSet = new Set<Id>();

		//Variable to store Action object Record Type: 'Open Drawdown'
		String actionRecordTypeId = Schema.SObjectType.EP_Action__c.getRecordTypeInfosByName().get('Open DrawDown').getRecordTypeId();

		//Access Custom Setting (CS_ORDER_SETTINGS__c) that stores the ID
		// of the Orchestration Process Template used to Close Contracts:
		Id templateId = CS_ORDER_SETTINGS__c.getOrgDefaults().Contract_Close_Orchestrator_Process_Id__c;

		//List of Orchestration Processes to be inserted: (one for every Order to be closed)
		List<CSPOFA__Orchestration_Process__c> processList = new List<CSPOFA__Orchestration_Process__c>();

		//List of Action objects to be inserted in case of Order ('Contract') that could not be closed:
		List<EP_Action__c> actionList = new List<EP_Action__c>();

		//Add Order ID's to the Set
		for(csord__Order__c order :scope){
			orderSet.add(order.Id);
		}

		//0) List of Open DrawDown Orders related to the Orders ('Contract'):
		List<csord__Order__c> openDrawDownOrdersList = [SELECT Id, csord__Primary_Order__c, csord__Status2__c, OrderNumber__c
														FROM csord__Order__c
														WHERE csord__Primary_Order__c IN :orderSet AND csord__Status2__c != 'Invoiced'];
		//1) Map:  ID of Order ('Contract')  VS  Set of DrawDown Order IDs
		Map <Id, Set<Id>> orderMap = new Map <Id, Set<Id>>();
		for(csord__Order__c order : scope) {
			Set<Id> drawDownOrderSet = new Set<Id>(); //Set of DrawDown Order Ids
			//Check if the Map doesn't have duplicate keys:
			if(!orderMap.containsKey(order.Id)) {
				for(csord__Order__c drawDownOrder : openDrawDownOrdersList){
					if (drawDownOrder.csord__Primary_Order__c == order.Id) {
						drawDownOrderSet.add(drawDownOrder.Id);
					}
				}
				orderMap.put(order.Id, drawDownOrderSet);
			}
		}

		//1.1) Loop through the Map and check if each Order has any Open DrawDown Orders (Status != 'Invoiced'):
		// 1st Verification --> (Check if there are any Open DrawDown Orders per Order 'Contract'):
		for(Id orderId : orderMap.keySet()) {
			if (orderMap.get(orderId) != null && orderMap.get(orderId).size() > 0){
				//Remove Order from Set:
				orderSet.remove(orderId);
				//Add Order to Set of Orders that cannot be Closed:
				orderNotClosableSet.add(orderId);
			}
		}
		
		//2) If there are any Open DrawDown Orders  --> create an Action object.
		if(!orderNotClosableSet.isEmpty()) {
			//2nd Verification --> Check if there are any existing Action objects
			// (with Record Type: 'Open Drawdown' AND Status Not 03-Completed) linked to Contract in question):
			List<EP_Action__c> allOpenActionsList = [SELECT EP_CSOrder__c, EP_Status__c, RecordTypeId
													 FROM EP_Action__c
													 WHERE EP_CSOrder__c IN :orderNotClosableSet
													 AND EP_Status__c != '03-Completed' AND RecordTypeId = :actionRecordTypeId];

			//2.1) Map:  ID of Order ('Contract')  VS  Set of Action IDs
			Map <Id, Set<Id>> actionMap = new Map <Id, Set<Id>>();
			for (Id orderId : orderNotClosableSet) {
				Set<Id> actionSet = new Set<Id>(); //Set of Action Ids
				//Check if the Map doesn't have duplicate keys:
				if (!actionMap.containsKey(orderId)) {
					for (EP_Action__c action : allOpenActionsList) {
						if (action.EP_CSOrder__c == orderId) {
							actionSet.add(action.Id);
						}
					}
					actionMap.put(orderId, actionSet);
				}
			}

			//2.2)  Loop through the Set of Orders that could not bel Closed and
			//   check the Action Map to see if each Order has any Open Action (Status != '03-Completed'):
			for(Id orderId : orderNotClosableSet) {
				if (!actionMap.containsKey(orderId) || actionMap.get(orderId) == null || actionMap.get(orderId).size() == 0){
					orderToCreateActionForSet.add(orderId);
				}
			}
		}

		//3) Loop through Set of Order ID's with no Open DrawDown Orders --> Close Open Orders ('Contract'):
		for(Id orderId : orderSet) {
			//3.1) Create a new record of Orchestration Process:
			CSPOFA__Orchestration_Process__c process = new CSPOFA__Orchestration_Process__c();
			process = CloseOpenOrderContractSchedulable.returnOrchestrationProcess(orderId, templateId);
			processList.add(process);
			//3.2) Update Status field to 'Closed':
			for(csord__Order__c order : scope) {
				if(orderId == order.Id) {
					order.csord__Status2__c = 'Closed';
				}
			}
		}
		if (!processList.isEmpty()){
			INSERT processList;
			UPDATE scope;
		}

		//4) If there are any Open DrawDown Orders then create an Action object:
		for(Id orderId : orderToCreateActionForSet) {
			//Variable to register Order Number field from Open DrawDown Orders:
			String pendingDrawDownOrders = '';
			//Variable to Count Set size:
			Integer counter = 1;
			//Loop through the list of Open DrawDown Orders and get Order Number field:
			for(csord__Order__c drawDownOrder : openDrawDownOrdersList){
				if(drawDownOrder.csord__Primary_Order__c == orderId) {
					if(counter < openDrawDownOrdersList.size()) {
						pendingDrawDownOrders += drawDownOrder.OrderNumber__c + ', ';
					}else {
						pendingDrawDownOrders += drawDownOrder.OrderNumber__c;
					}
				}
				counter ++;
			}
			//Create a new record of Action
			EP_Action__c action = new EP_Action__c();
			action.RecordTypeId = actionRecordTypeId;
			action.EP_CSOrder__c = orderId;
			action.EP_Status__c = '01-New';
			action.Pending_DrawDown_Orders__c = pendingDrawDownOrders;
			actionList.add(action);
		}
		if (!actionList.isEmpty()){
			INSERT actionList;
		}
	}


	global void finish(Database.BatchableContext BC) {
	}


	/*
	@Author      CloudSense
	@name        returnOrchestrationProcess
	@CreateDate  30/07/2017
	@Description Auxiliary method to return a new record of Orchestration Process based on an Order and a Orchestration Process Template
	@Version     1.0
	*/
	private static CSPOFA__Orchestration_Process__c returnOrchestrationProcess(Id orderId, Id OrchestratorTemplateId){
		//get order number
		csord__Order__c order = [SELECT Id, csord__Order_Number__c FROM csord__Order__c WHERE Id = :orderId LIMIT 1];

		//setup Orchestrator record
		CSPOFA__Orchestration_Process__c process = new CSPOFA__Orchestration_Process__c();
		process.Order__c = orderId;
		process.Name ='Orchestration_Process_Order_Num:' + order.csord__Order_Number__c;
		process.CSPOFA__Orchestration_Process_Template__c = OrchestratorTemplateId;

		return process;
	}
}