@isTest
public class EP_PricingResponseHelper_UT
{
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    static testMethod void doBasicDataSetup_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        EP_OrderMapper orderMapperObj = new EP_OrderMapper();
        localObj.orderObject = orderMapperObj.getCSRecordById(EP_TestDataUtility.getNonConsignmentOrderPositiveScenario().id);
        Test.startTest();
        localObj.doBasicDataSetup();
        Test.stopTest();
        System.AssertNotEquals(Null,localObj.priceBookEntryIdMap);
    }
    static testMethod void getOrderObject_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        EP_OrderMapper orderMapperObj = new EP_OrderMapper();
        localObj.orderObject = orderMapperObj.getCSRecordById(EP_TestDataUtility.getNonConsignmentOrderPositiveScenario().id);
        Test.startTest();
        csord__Order__c result = localObj.getOrderObject();
        Test.stopTest();
        System.AssertEquals(localObj.orderObject.id,result.id);
    }
    static testMethod void setPriceBookEntries_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        EP_OrderMapper orderMapperObj = new EP_OrderMapper();
        localObj.orderObject = orderMapperObj.getCSRecordById(EP_TestDataUtility.getNonConsignmentOrderPositiveScenario().id);
        Id accountId = localObj.orderObject.AccountId__c;
        Test.startTest();
        localObj.setPriceBookEntries(accountId);
        Test.stopTest();
        System.AssertNotEquals(Null,localObj.priceBookEntryIdMap );
    }
    static testMethod void createDataMaps_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        EP_OrderMapper orderMapperObj = new EP_OrderMapper();
        csord__Order__c orderObject = orderMapperObj.getCSRecordById(EP_TestDataUtility.getNonConsignmentOrderPositiveScenario().id);
        LIST<EP_PricingResponseStub.LineItem> priceLineItems = new LIST<EP_PricingResponseStub.LineItem>{EP_TestDataUtility.createPriceLineItem()};
        String orderNumber = orderObject.OrderNumber__c;
        Test.startTest();
        localObj.createDataMaps(priceLineItems,orderNumber);
        Test.stopTest();
        System.AssertNotEquals(Null,localObj.orderObject);
        System.AssertNotEquals(Null,localObj.orderItemMap);
    }
    /*static testMethod void setOrderItemAttributes_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        EP_OrderMapper orderMapperObj = new EP_OrderMapper();
        LIST<EP_PricingResponseStub.LineItem> priceLineItems = new LIST<EP_PricingResponseStub.LineItem>{EP_TestDataUtility.createPriceLineItem()};
        csord__Order__c orderObject = orderMapperObj.getCSRecordById(EP_TestDataUtility.getNonConsignmentOrderPositiveScenario().id);
        String orderNumber = orderObject.OrderNumber__c;
        OrderItem ordItem = [SELECT Id , EP_OrderItem_Number__c,orderId FROM OrderItem WHERE OrderId =:orderObject.id LIMIT 1];
        String parentId = String.valueOf(ordItem.EP_OrderItem_Number__c);
        localObj.orderItemMap.put(parentId,ordItem);
        priceLineItems[0].lineItemInfo.lineItemId = parentId;
        Test.startTest();
        localObj.setOrderItemAttributes(priceLineItems,orderNumber);
        Test.stopTest();
        System.AssertNotEquals(Null,localObj.priceBookEntryIdMap);
    }*/
    static testMethod void setOrderItemJSONAttributes_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        EP_PricingResponseStub.lineItemInfo lineItemInfoObj = EP_TestDataUtility.createLineItemInfo();
        Test.startTest();
        localObj.setOrderItemJSONAttributes(lineItemInfoObj);
        Test.stopTest();
        //System.AssertEquals(lineItemInfoObj.itemId,lineItemInfoObj.orderLineItem.EP_WinDMS_Line_ItemId__c);
        //System.AssertEquals(Decimal.valueOf(lineItemInfoObj.unitPrice),lineItemInfoObj.orderLineItem.EP_Pricing_Response_Unit_Price__c);
    }
    static testMethod void setInvoiceItemJSONAttributes_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        EP_PricingResponseStub.InvoiceDetails invoiceDetailsObj = EP_TestDataUtility.createInvoiceDetails();
        EP_PricingResponseStub.InvoiceComponent ordInvoiceItem = invoiceDetailsObj.invoiceComponent[0];
        Test.startTest();
        localObj.setInvoiceItemJSONAttributes(ordInvoiceItem);
        Test.stopTest();
        System.AssertEquals(ordInvoiceItem.Name,ordInvoiceItem.invoiceItem.EP_Invoice_Name__c);
    }
    /*static testMethod void processOrderItems_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        LIST<EP_PricingResponseStub.LineItem> priceLineItems = new LIST<EP_PricingResponseStub.LineItem>{EP_TestDataUtility.createPriceLineItem()};
        csord__Order__c orderObject = EP_TestDataUtility.getCurrentVMIOrder();
        String orderNumber = orderObject.OrderNumber__c;
        localObj.orderObject = orderObject;
        OrderItem orderItemObj = [SELECT Id , EP_OrderItem_Number__c,orderId,EP_Product_Name__c FROM OrderItem WHERE OrderId =:orderObject.id LIMIT 1];
        String parentId = String.valueOf(orderItemObj.EP_OrderItem_Number__c);
        localObj.orderItemMap.put(parentId,orderItemObj);
        String productName = orderItemObj.EP_Product_Name__c;
        String entryKeyStr = localObj.getPricebookEntryKey(productName);
        list<PricebookEntry> pricebookEntry =  [SELECT Id, Pricebook2Id FROM PricebookEntry where Pricebook2Id =:orderObject.Pricebook2Id__c];
        priceLineItems[0].lineItemInfo.lineItemId = parentId;
        localObj.priceBookEntryIdMap.put(entryKeyStr, pricebookEntry.get(0));
        Test.startTest();
        localObj.createDataMaps(priceLineItems,orderNumber);
        localObj.setOrderItemId(priceLineItems[0]);
        localObj.processOrderItems(priceLineItems,orderNumber);
        Test.stopTest();
        System.AssertEquals(priceLineItems[0].lineItemInfo.itemId,priceLineItems[0].lineItemInfo.orderLineItem.EP_WinDMS_Line_ItemId__c);
    }
    static testMethod void setOrderItemId_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        EP_OrderMapper orderMapperObj = new EP_OrderMapper();
        csord__Order__c orderObject = orderMapperObj.getCSRecordById(EP_TestDataUtility.getNonConsignmentOrderPositiveScenario().id);
        LIST<EP_PricingResponseStub.LineItem> priceLineItems = new LIST<EP_PricingResponseStub.LineItem>{EP_TestDataUtility.createPriceLineItem()};
        String orderNumber = orderObject.OrderNumber__c;
        OrderItem ordItem = [SELECT Id , EP_OrderItem_Number__c,orderId FROM OrderItem WHERE OrderId =:orderObject.id LIMIT 1];
        String parentId = String.valueOf(ordItem.EP_OrderItem_Number__c);
        localObj.orderItemMap.put(parentId,ordItem);
        priceLineItems[0].lineItemInfo.lineItemId = parentId;
        Test.startTest();
        localObj.createDataMaps(priceLineItems,orderNumber);
        localObj.setOrderItemId(priceLineItems[0]);
        Test.stopTest();
        orderItem existingOrderItem = localObj.orderItemMap.get(priceLineItems[0].lineItemInfo.lineItemId);
        System.AssertEquals(existingOrderItem.id,priceLineItems[0].lineItemInfo.orderLineItem.Id);
    }
    static testMethod void setInvoiceItem_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        EP_PricingResponseStub.LineItem priceLineItem = EP_TestDataUtility.createPriceLineItem();
        EP_OrderMapper orderMapperObj = new EP_OrderMapper();
        csord__Order__c orderObject = EP_TestDataUtility.getCurrentVMIOrder();
        localObj.orderObject = orderObject;
        OrderItem orderItemObj = [SELECT Id , EP_OrderItem_Number__c,orderId,EP_Product_Name__c FROM OrderItem WHERE OrderId =:orderObject.id LIMIT 1];
        String parentId = String.valueOf(orderItemObj.EP_OrderItem_Number__c);
        localObj.orderItemMap.put(parentId,orderItemObj);
        String productName = orderItemObj.EP_Product_Name__c;
        String entryKeyStr = localObj.getPricebookEntryKey(productName);
        list<PricebookEntry> pricebookEntry =  [SELECT Id, Pricebook2Id FROM PricebookEntry where Pricebook2Id =:orderObject.Pricebook2Id__c];
        localObj.priceBookEntryIdMap.put(entryKeyStr, pricebookEntry.get(0));
        Test.startTest();
        Map<string, orderItem> result = localObj.getExistingOrderItems(orderObject.orderNumber__c);
        localObj.setInvoiceItem(priceLineItem,parentId);
        Test.stopTest();
        EP_PricingResponseStub.InvoiceComponent ordInvoiceItem = priceLineItem.lineItemInfo.invoiceDetails.InvoiceComponent[0];
        System.AssertEquals(localObj.orderItemMap.get(parentId).id,ordInvoiceItem.invoiceItem.EP_Parent_Order_Line_Item__c);
    }*/
    /*static testMethod void setPriceBookEntryId_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();        
        EP_OrderMapper orderMapperObj = new EP_OrderMapper();
        csord__Order__c orderObject = EP_TestDataUtility.getCurrentVMIOrder();
        localObj.orderObject = orderObject;
        OrderItem orderItemObj = [SELECT id , EP_Product_Name__c , OrderId FROM OrderItem WHERE OrderId =:orderObject.id LIMIT 1];
        String productName = orderItemObj.EP_Product_Name__c;
        String entryKeyStr = localObj.getPricebookEntryKey(productName);
        list<PricebookEntry> pricebookEntry =  [SELECT Id, Pricebook2Id FROM PricebookEntry where Pricebook2Id =:orderObject.Pricebook2Id__c];
        localObj.priceBookEntryIdMap.put(entryKeyStr, pricebookEntry.get(0));
        Test.startTest();
        String priceBookEntryId = localObj.setpriceBookEntryId(productName);
        Test.stopTest();
        System.AssertNotEquals(Null,priceBookEntryId);
    }*/
    static testMethod void getExistingOrderItems_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        EP_OrderMapper orderMapperObj = new EP_OrderMapper();
        csord__Order__c orderObject = orderMapperObj.getCSRecordById(EP_TestDataUtility.getNonConsignmentOrderPositiveScenario().id);
        String orderNumber = orderObject.orderNumber__c;
        Test.startTest();
        Map<string, orderItem> existingOrderItemsMap = localObj.getExistingOrderItems(orderNumber);
        Test.stopTest();
        System.AssertNotEquals(Null,existingOrderItemsMap);
    }
    static testMethod void setAccountComponentXML_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        EP_PricingResponseStub.LineItem priceLineItem = EP_TestDataUtility.createPriceLineItem();
        Test.startTest();
        localObj.setAccountComponentXML(priceLineItem);
        Test.stopTest();
        //System.AssertNotEquals(Null,priceLineItem.lineItemInfo.orderLineItem.EP_Accounting_Details__c);
    }
    static testMethod void setProducts_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        EP_OrderMapper orderMapperObj = new EP_OrderMapper();
        localObj.orderObject = orderMapperObj.getCSRecordById(EP_TestDataUtility.getNonConsignmentOrderPositiveScenario().id);
        Id accountId = localObj.orderObject.AccountId__c;
        Product2 newProductObj = new Product2();
        newProductObj.Name = 'New Product';
        newProductObj.ProductCode = 'New Product';
        newProductObj.IsActive = True;
        newProductObj.EP_Is_System_Product__c = true;
        insert newProductObj;
        List<EP_PricingResponseStub.LineItem> priceLineItems = new List<EP_PricingResponseStub.LineItem>{EP_TestDataUtility.createPriceLineItem()};
        for(EP_PricingResponseStub.LineItem priceLineItem : priceLineItems) {
            for(EP_PricingResponseStub.InvoiceComponent invComp: priceLineItem.lineItemInfo.invoiceDetails.invoiceComponent) {
                invComp.type = newProductObj.ProductCode;
            }
        }
        localObj.lineItems = priceLineItems;
        Test.startTest();
        List<Product2> result = localObj.setProducts(accountId);
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);
    }
    /*static testMethod void getPricebookEntryKey_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        EP_OrderMapper orderMapperObj = new EP_OrderMapper();
        csord__Order__c orderObj = orderMapperObj.getCSRecordById(EP_TestDataUtility.getNonConsignmentOrderPositiveScenario().id);
        localObj.orderObject = orderObj;
        OrderItem orderItemObj = [SELECT id , EP_Product_Name__c , OrderId FROM OrderItem WHERE OrderId =:orderObj.id LIMIT 1];
        String productName = orderItemObj.EP_Product_Name__c;
        Test.startTest();
        String PricebookEntryKey = localObj.getPricebookEntryKey(productName);
        Test.stopTest();
        System.AssertNotEquals(Null,PricebookEntryKey);
    }*/
    static testMethod void createMissingPriceBookEntries_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        EP_OrderMapper orderMapperObj = new EP_OrderMapper();
        csord__Order__c localOrderObject = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        localObj.orderObject = localOrderObject;
        Id accountId = localOrderObject.AccountId__c;
        List<Product2> productsForNewPBEntry = new List<Product2>();
        Product2 newProductObj = new Product2();
        newProductObj.Name = 'productName';
        newProductObj.IsActive = True;
        newProductObj.EP_Is_System_Product__c = true;
        insert newProductObj;
        productsForNewPBEntry.add(newProductObj);
        List<PriceBookEntry> listPriceBookEntryBeforeIns = [SELECT Id,CreatedDate FROM PriceBookEntry];
        Test.startTest();
        localObj.createMissingPriceBookEntries(productsForNewPBEntry,localOrderObject);
        Test.stopTest();
        List<PriceBookEntry> listPriceBookEntryAfterIns = [SELECT Id,CreatedDate FROM PriceBookEntry];
        System.AssertNotEquals(listPriceBookEntryBeforeIns.size(),listPriceBookEntryAfterIns.size());
    }
    static testMethod void createNewProducts_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        Set<string> productNameSet = new Set<string>{EP_Common_Constant.TAX_PRODUCT_NAME};
        Test.startTest();
        LIST<Product2> result = localObj.createNewProducts(productNameSet);
        Test.stopTest();
        Product2 productObj = [SELECT Id , Name FROM Product2 WHERE Name =:EP_Common_Constant.TAX_PRODUCT_NAME];
        System.AssertNotEquals(null,productObj);
    }
    static testMethod void getProductsName_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        localObj.lineItems = new List<EP_PricingResponseStub.LineItem>{EP_TestDataUtility.createPriceLineItem()};
        Test.startTest();
        Set<string> result = localObj.getProductsName();
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);
    }
    static testMethod void setOrderStatus_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        //Order orderObject = EP_TestDataUtility.getRetrospectivePositiveScenario();
        csord__Order__c orderObject = EP_TestDataUtility.getCurrentVMIOrder();
        orderObject.Status__c = EP_Common_Constant.PLANNED_STATUS;
        update orderObject;
        Test.startTest();
        localObj.setOrderStatus(orderObject);
        Test.stopTest();
        //Just Check if State machin Allow this Transition
        System.AssertEquals(true,EP_Common_Constant.PLANNED_STATUS.equalsIgnorecase(orderObject.Status__c));
    }
    static testMethod void doPostUpdateActions_test() {
        EP_PricingResponseHelper localObj = new EP_PricingResponseHelper();
        csord__Order__c orderObject = EP_TestDataUtility.getRetrospectivePositiveScenario();
        localObj.orderDomain = new EP_OrderDomainObject(orderObject);
        Test.startTest();
        localObj.doPostUpdateActions();
        Test.stopTest();
        //No Assertion as the method only delegates to other class methods,hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void getOrderItemList_test() {
        List<EP_PricingResponseStub.LineItem> priceLineItems = new List<EP_PricingResponseStub.LineItem>{EP_TestDataUtility.createPriceLineItem()};
        Test.startTest();
        List<orderItem> result = EP_PricingResponseHelper.getOrderItemList(priceLineItems);
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);
    }
}