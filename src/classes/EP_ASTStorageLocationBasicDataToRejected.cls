/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationBasicDataToRejected>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 02-Basic Data Setup to 08-Rejected>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationBasicDataToRejected extends EP_AccountStateTransition {

    public EP_ASTStorageLocationBasicDataToRejected () {
        finalState = EP_AccountConstant.REJECTED;
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationBasicDataToRejected',' isGuardCondition');        
        return true;
    }

}