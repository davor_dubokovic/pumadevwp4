/*********************************************************************************************
     @Author <Soumya Raj>
     @name <EP_FE_OrderProductDataTest >
     @CreateDate <>
     @Description < >  
     @Version <1.0>
*********************************************************************************************/
@isTest
private class EP_FE_OrderProductData_Test{

/*********************************************************************************************
     @Author <Soumya Raj>
     @name <EP_FE_OrderProductDataTest >
     @CreateDate <>
     @Description < >  
     @Version <1.0>
*********************************************************************************************/    
static void basicdataSetup()
    {
        EP_PortalOrderTestUtility.createTestData();
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        Test.loadData(EP_Order_Fieldset_Sfdc_Nav_Intg__c.SobjectType,'EP_OrderStatusUpdate_FieldSet');
        Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_OrderStatusNAVMapping__c.SobjectType,'EP_OrderStatusUpdateNAV');
    }

/*********************************************************************************************
     @Author <Soumya Raj>
     @name <EP_FE_OrderProductDataTest >
     @CreateDate <>
     @Description < >  
     @Version <1.0>
*********************************************************************************************/    
static testmethod void EP_FE_OrderProductDatamethod()
    {
        
        basicdataSetup();
        EP_Country__c c=[Select id from EP_Country__c]; 
        EP_Region__c r = [Select id from EP_Region__c];
        Id pricebookId = Test.getStandardPricebookId();
        Account billToAcc =EP_TestDataUtility.createBillToAccount();
        billToAcc.EP_Billing_Basis__c = 'Ordered';
        billToAcc.EP_Email__c = 'asd@as.com';
        billToAcc.EP_Delivery_Pickup_Country__c = c.id;
        billToAcc.EP_Cntry_Region__c = r.id;
        billToAcc.EP_PriceBook__c = pricebookId;
        insert billToAcc;
        billToAcc.EP_Status__c = '02-Basic Data Setup';
        update billToAcc;
        billToAcc.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update billToAcc;
        
        Product2  productObj = new product2(Name = 'P7', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true);
        Database.insert(productObj);
        /*PricebookEntry standardPrice9 = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = productObj.Id,
        UnitPrice = 10000, IsActive = true);
        System.debug('standardPrice9###'+standardPrice9);
        Database.insert(standardPrice9);*/

        PricebookEntry prodPricebookEntry = [SELECT Id FROM PricebookEntry  WHERE Product2.id =: productObj.id LIMIT : EP_Common_Constant.ONE];

        Order o = new Order();
        o.AccountId = billToAcc.id;
        o.Pricebook2Id = pricebookId;
        o.EffectiveDate = System.today();
        o.Status = EP_Common_Constant.ORDER_DRAFT_STATUS;
        o.Type = 'Non-Consignment';
        insert o;

        csord__Order_Line_Item__c oitm = new csord__Order_Line_Item__c();
        oitm.orderId__c = o.Id;
        oitm.PricebookEntryId__c = prodPricebookEntry.id;
        oitm.Quantity__c = 100;
        oitm.UnitPrice__c = 10000;
        System.debug('oitm####'+oitm);
        Database.insert(oitm);
        
        Test.starttest(); 
        EP_FE_OrderProductData prodData = new EP_FE_OrderProductData();
        EP_FE_OrderProductData.EP_FE_OrderItem ordItem = new EP_FE_OrderProductData.EP_FE_OrderItem(oitm,true);
        EP_FE_OrderProductData.EP_FE_TaxItem  taxItem = new EP_FE_OrderProductData.EP_FE_TaxItem('Diesel', 50.00);

        EP_FE_OrderProductData.EP_FE_OrderItem ordItem1 = new EP_FE_OrderProductData.EP_FE_OrderItem();
        EP_FE_OrderProductData.EP_FE_OrderItem  ordItem2 = new EP_FE_OrderProductData.EP_FE_OrderItem(oitm); 
        test.stoptest();            
    }
    
}