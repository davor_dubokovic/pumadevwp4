/* 
  @Author <Ashok Arora> 
   @name <EP_LeadTriggerHelper>
   @CreateDate <10/11/2015>
   @Description <This class handles requests from LeadTriggerhandler> 
   @Version <1.0>
*/
public with sharing class EP_LeadTriggerHelper {
    private static final string CLASSNAME = 'EP_LeadTriggerHelper'; 
    private static final string PAYMENT_METHOD_AND_TERM_METHODNAME = 'mapPaymentMethodAndTerm';
    private static final string UPDATE_ACCEPTED_RECORD_TYPE_METHODNAME = 'mapPaymentMethodAndTerm';
    private static final string CREATE_CASE_MTD = 'createCase';
    private static final String POUPLATE_LEAD_DATA_MTD = 'populateLeadData';
    private static final String UPDATE_PROD_CASE = 'updateCasePrdIntIn';
    
    /*
    @Author <Kamendra Singh>
    @CreateDate <08/06/2016>
    @Description <get Business Inquiry and Business Inquiry Onhold record type(Req-PUM-AU-CRM-001).> 
    */
    private static  RecordType BIrecType = EP_Common_Util.getRecordTypeForGivenSObjectAndName(EP_Common_Constant.LEAD_OBJ,Label.EP_CRM_LEAD_RECTYPE_BI);
    private static RecordType BIonholdrecType = EP_Common_Util.getRecordTypeForGivenSObjectAndName(EP_Common_Constant.LEAD_OBJ,Label.EP_CRM_LEAD_RECTYPE_BI_ONHOLD);
    
    /* Create case Record*/
    /*public static void  createCase(list<Lead>lNewLeads){
        List<AssignmentRule> aRules;
        Database.DMLOptions dmlOpts;
        List<Lead>lLeadsWOCase = new list<Lead>();
        try{
            list<Case >lCase = new list<Case>();
            RecordType recType = EP_Common_Util.getRecordTypeForGivenSObjectAndName(EP_Common_Constant.CASE_OBJ_REC
                                                    ,EP_Common_Constant.NEW_PROSPECTIVE_CUSTOMER_REQUEST_REC_TYPE);
            for(Lead lead : lNewLeads){*/
                /* @Author <Kamendra Singh
                *@Description <add filter on Business Inquiry and Business Inquiry Onhold record type(Req-PUM-AU-CRM-001).
                *CreateDate <08/06/2016>
                */
                /*if(String.isBlank(lead.Case__c)&& lead.RecordTypeId != BIrecType.Id && lead.RecordTypeId != BIonholdrecType.Id){
                   lCase.add(new Case(Origin = lead.LeadSource//EP_Common_Constant.CASE_ORIGIN_WEB
                                    ,Status = EP_Common_Constant.CASE_STATUS_NEW_APPLICATION
                                    ,SuppliedEmail  = lead.Email
                                    ,Priority = EP_Common_Constant.CASE_PRIORITY_MEDIUM
                                    ,RecordTypeId = recType.Id
                                    ,EP_Sys_Products_Interested_In__c = lead.EP_Products_Interested_In__c
                                    ,EP_Country_Code__c = lead.EP_Dlvry_Cntry_Code__c
                                    ,EP_Region_Name__c = lead.EP_Region_Name__c));
                    lLeadsWOCase.add(lead);
                    
                }
                
            }
            aRules = EP_Common_Util.getAssignmentRules(EP_Common_Constant.CASE_OBJ); 
              //Creating the DMLOptions for "Assign using active assignment rules" checkbox
              dmlOpts = new Database.DMLOptions();
               if (aRules != null && !aRules.isEmpty()){
                dmlOpts.assignmentRuleHeader.assignmentRuleId= aRules[0].id;
              }
              dmlOpts.EmailHeader.triggerAutoResponseEmail = true;
              DataBase.insert(lCase,dmlOpts);
            
             
            for(Integer i = 0; i < lLeadsWOCase.size(); i++ ){
                lLeadsWOCase[i].Case__c = lCase[i].id;
            }
        }
        catch(Exception handledException){
            EP_LoggingService.logHandledException (handledException
                                                    , EP_Common_Constant.EPUMA
                                                    , CREATE_CASE_MTD
                                                    , CLASSNAME
                                                    , ApexPages.Severity.ERROR);
        }
    }*/
    
    /*
        This method updates lead record type once status changes to accepted
    */
    /*public static void updateAcceptedRecordType(List<Lead>lNewLeads){
        
        Id leadNewRecordTypeId;
        Id leadAcceptedRecordTypeId;
        try{
            leadNewRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.LEAD_OBJ
                                                                    ,EP_Common_Constant.LEAD_NEW_TYPE);
            leadAcceptedRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.LEAD_OBJ
                                                                    ,EP_Common_Constant.LEAD_ACCEPTED_TYPE);
            //IF LEAD STATUS IS ACCEPTED AND RECORDTYPE IS NEW THEN UPDATE RECORD TYPE TO ACCEPTED
            for(Lead lead : lNewLeads){
                if(EP_Common_Constant.LEAD_STATUS_ACCEPTED.equalsIgnoreCase(lead.Status)
                    && lead.RecordTypeId == leadNewRecordTypeId){
                    lead.RecordTypeId = leadAcceptedRecordTypeId;//UPDATE RECORD TYPE TO ACCEPTED
                }
            }
        }   
        catch(Exception handledException){
        EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA, UPDATE_ACCEPTED_RECORD_TYPE_METHODNAME, CLASSNAME, ApexPages.Severity.ERROR);
        }
    }*/
    
    /*
        This method maps requested payment method and payment term on connverted Account from lead
    */  
    public static void mapPaymentMethodAndTerm(map<Id,Lead>mNewLeads){
        
        MAP<Id,Id>mConvertedAccountLead = new map<Id,Id>();
        set<String>sPaymentMethods = new set<String>();
        List<Account>lAccounts = new List<Account>();
        map<String,Id>mPaymentMethods = new map<String,Id>();
        Integer nRows;
        EP_Payment_Term__c paymentTerm;
        try{
            //GET THE CONVERTED ACCOUNTS IF LEAD IS CONVERTED
            for(Lead lead : mNewLeads.values()){
                if(lead.isConverted
                    && String.isNotBlank(lead.ConvertedAccountId)){
                    mConvertedAccountLead.put(lead.ConvertedAccountId,lead.id);
                    sPaymentMethods.add(lead.EP_Requested_Payment_Method__c);
                }
            }
            nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
            //GET RELATED PAYMENT METHODS
            if(!sPaymentMethods.isEmpty()){
                for(EP_Payment_Method__c paymentMethod: [Select Name 
                                                        ,Id
                                                  From EP_Payment_Method__c
                                                  Where Name IN:sPaymentMethods
                                                  Limit :nRows]){
                    mPaymentMethods.put(paymentMethod.Name,paymentMethod.Id);
                }
            }
            
            
            //UPDATE PAYMENT METHOD AND PAYMENT TERM ON CONVERTED ACCOUNTS
            if(!mConvertedAccountLead.isEmpty()){
                paymentTerm = [select id 
                               from EP_Payment_Term__c
                               where Name =:System.label.EP_PrePayment
                               limit :EP_Common_Constant.ONE];
                nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
                for(Account account : [Select Id                                        
                                        ,EP_Requested_Payment_Terms__c
                                        ,currencyISOCode
                                    From Account
                                    Where Id IN :mConvertedAccountLead.keyset() 
                                    limit :nRows]){
                    //MAP PRE-PAYMENT TERM
                    account.EP_Payment_Term_Lookup__c = paymentTerm.Id;                    
                    
                    //MAP DELIVERY\PICKUP COUNTRY
                    account.EP_Delivery_Pickup_Country__c = mNewLeads.get(mConvertedAccountLead.get(account.Id)).EP_Delivery_Country__c;
                    //MAP REGION
                    account.EP_Cntry_Region__c= mNewLeads.get(mConvertedAccountLead.get(account.Id)).EP_Region__c;
                    //ADD ACCOUNTS TO UPDATE
                    lAccounts.add(account);
                    
                    
                
                }
                if(!lAccounts.isEmpty()){
                    update lAccounts;
                }
            }   
        }
        catch(Exception handledException){
            //EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA, PAYMENT_METHOD_AND_TERM_METHODNAME, CLASSNAME, ApexPages.Severity.ERROR);
        }
        
    }
    
    /*
     * To set Lead data from country object
     */
    /*public static void populateLeadData(List<Lead> leads){
        try{
            Integer nRows;
            set<Id> countryIds = new set<Id>();
            for(Lead le : leads){
                if(le.EP_CountryLookUp__c != null){
                    countryIds.add(le.EP_CountryLookUp__c);
                }   
            }
            nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
            Map<Id, EP_Country__c> countryMap = new Map<Id, EP_Country__c>([SELECT id,CurrencyIsoCode, Name From EP_Country__c WHERE Id IN : countryIds limit :nRows]);
            for(Lead le : leads){
                if(le.EP_CountryLookUp__c != null && countryMap.containsKey(le.EP_CountryLookUp__c)){
                    le.CurrencyIsoCode = countryMap.get(le.EP_CountryLookUp__c).CurrencyIsoCode;
                    le.Country = countryMap.get(le.EP_CountryLookUp__c).Name;
                }
            }
        }
        catch(Exception handledException){
            EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA, POUPLATE_LEAD_DATA_MTD, CLASSNAME, ApexPages.Severity.ERROR);
        }
    }*/
    
    /*
    * This method is updating case's product interested in? field if we are changing Product interasted in? field on lead 
    */
    /*public static void updateCasePrdIntIn(map<Id,Lead>mOldLeads
                                            ,map<Id,Lead>mNewLeads){
                                            
        list<Case>lcase = new list<Case>();
        Case caseRec;
        try{
        
             for(Id lead : mNewLeads.keySet()){
                if((mNewLeads.get(lead).EP_Products_Interested_In__c
                    != mOldLeads.get(lead).EP_Products_Interested_In__c
                     || mNewLeads.get(lead).EP_Region_Name__c
                        != mOldLeads.get(lead).EP_Region_Name__c
                     || mNewLeads.get(lead).EP_Dlvry_Cntry_Code__c
                        != mOldLeads.get(lead).EP_Dlvry_Cntry_Code__c   
                    )
                    && String.isNotBlank(mNewLeads.get(lead).case__c)){
                    caseRec = (Case)Case.SObjectType.newSObject(mNewLeads.get(lead).case__c);
                    caseRec.EP_Sys_Products_Interested_In__c = mNewLeads.get(lead).EP_Products_Interested_In__c;
                    caseRec.EP_Region_Name__c = mNewLeads.get(lead).EP_Region_Name__c;
                    caseRec.EP_Country_Code__c = mNewLeads.get(lead).EP_Dlvry_Cntry_Code__c;
                    lCase.add(caseRec);
                }
            }
            if(!lCase.isEmpty()){
                Database.update(lCase);
            }  
             
        }
        catch(Exception handledException){
            EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA, UPDATE_PROD_CASE, CLASSNAME, ApexPages.Severity.ERROR);
        }
    }*/
}