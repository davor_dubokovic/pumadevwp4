@isTest
private class EP_Account_Notification_Settings_Ctrl_UT {
    
    @TestSetup
    static void data(){
        EP_AccountNotificationTestDataUtility.insertEmailTemplate(EP_AccountNotificationTestDataUtility.getFolder('EP_Notifications_Email_Templates').id);
    }

    @isTest static void save_Positive() {
        EmailTemplate emailObj = [select id from EmailTemplate where developerName = 'UserTemplate'];
        Account acc = EP_TestDataUtility.getSellTo();
        EP_Notification_Type__c notifyType = EP_AccountNotificationTestDataUtility.createNotificationType(emailObj.Id);
        EP_AccountNotificationTestDataUtility.createAccountNotification(acc.id,emailObj.Id,'',
                                                                            EP_TestDataUtility.createTestRecordsForContact(acc).Id,notifyType.Id);
        EP_Account_Notification_Settings_Ctrl controller = new EP_Account_Notification_Settings_Ctrl(new ApexPages.standardController(acc));
        Test.startTest();
        controller.save();
        Test.stopTest();
        List<EP_Notification_Account_Settings__c> lsAccNot = [select id from EP_Notification_Account_Settings__c];
        system.assert(!lsAccNot.isEmpty());
    }

    @isTest static void save_Negative(){
        Account acc = EP_TestDataUtility.getSellTo();
        EP_Account_Notification_Settings_Ctrl controller = new EP_Account_Notification_Settings_Ctrl(new ApexPages.standardController(acc));
        Test.startTest();
        controller.save();
        Test.stopTest();
        List<EP_Notification_Account_Settings__c> lsAccNot = [select id from EP_Notification_Account_Settings__c];
        system.assert(lsAccNot.isEmpty());
    }
}