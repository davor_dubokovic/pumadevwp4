/*
   @Author          CloudSense
   @Name            OrderSyncWithWinDmsHandler
   @CreateDate      18/12/2017
   @Description     This class is responsible for initiating the Order Sync With WinDms
   @Version         1.1
 
*/

global class OrderSyncWithWinDmsHandler implements CSPOFA.ExecutionHandler{
    
    /**
    * @Author       CloudSense
    * @Name         execute
    * @Date         18/12/2017
    * @Description  Method to process the step and and call Sync with WinDms
    * @Param        list<SObject>
    * @return       NA
    */  
    public List<sObject> process(List<SObject> data)
    {
        system.debug('mategr orch 1');
        List<sObject> result = new List<sObject>();
        //collect the data for all steps passed in, if needed
        List<CSPOFA__Orchestration_Step__c> stepList= (List<CSPOFA__Orchestration_Step__c>)data;
        Map<Id,CSPOFA__Orchestration_Step__c> stepMap = new Map<Id,CSPOFA__Orchestration_Step__c>();
        List<Id> orderIdList = new List<Id>();
        Map<Id,Id> orderIdCrIdMap = new Map<Id,Id>();
        
        List<CSPOFA__Orchestration_Step__c> extendedList = [Select
                                                                id,CSPOFA__Orchestration_Process__r.Order__c,CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c,CSPOFA__Status__c,CSPOFA__Completed_Date__c,CSPOFA__Message__c
                                                            from 
                                                                CSPOFA__Orchestration_Step__c 
                                                            where 
                                                            id in :stepList];
                                                            
        system.debug('extended list is :' +extendedList);
        /**for(CSPOFA__Orchestration_Step__c step:extendedList){
            orderIdList.add(step.CSPOFA__Orchestration_Process__r.Order__c);
            system.debug('Order Id is :' +step.CSPOFA__Orchestration_Process__r.Order__c) ;
            //mark step Status, Completed Date, and write optional step Message
          
        }
        system.debug('mategr orch 4');
        system.debug('result is :' +result);
        system.debug('orderIdList is :' +orderIdList);
        system.debug('mategr orch 8'); */
        
                                          
        for(CSPOFA__Orchestration_Step__c step:extendedList){
            try{
                
                syncOrderWithWinDms(step.CSPOFA__Orchestration_Process__r.Order__c);
            }Catch(Exception e){
               
             system.debug('exception is :' + e);
             EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, 'process', 'OrderSyncWithWinDmsHandler',apexPages.severity.ERROR);
            
          }
          
           step.CSPOFA__Status__c ='Complete';
           step.CSPOFA__Completed_Date__c=Date.today();
           step.CSPOFA__Message__c = 'Custom step succeeded';
           result.add(step);
            
        }
       
        system.debug(' final result is :' +result);
     return result;        
        
        
    }

    @RemoteAction
    global static PageReference syncOrderWithWinDms(Id orderId) {
        Boolean enqueueJob  = false;
        String companyName = EP_Common_Constant.BLANK;
        EP_OrderMapper mapper = new EP_OrderMapper();
        csord__Order__c order = mapper.getCsRecordById(orderId);
        List<Id> lstOrderItemIds = new List<Id>();
        companyName = order.csord__Account__r.EP_Puma_Company_Code__c;
        for(csord__Order_Line_Item__c orderItemObj: order.csord__Order_Line_Items__r) {
            lstOrderItemIds.add(orderItemObj.Id);
        } 
        EP_OutboundMessageService outboundService = new EP_OutboundMessageService(order.Id, 'SFDC_TO_WINDMS_ORDER_SYNC', companyName);
         if(enqueueJob) {
            outboundService.setEnqueueJob(enqueueJob);
        }
        outboundService.sendOutboundMessage(EP_Common_Constant.ORDER_WINDMS,lstOrderItemIds);

        return null;
    }
        
      
}