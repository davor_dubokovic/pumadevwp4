@isTest
private class EP_OrderImportHelper_UT {
    static EP_File__c tempFile;
    static EP_OrderImportContext context;
    static void setupData(){
        tempFile =  new EP_File__c(Name = 'TestFile',EP_CheckSum_Key__c = '12swerdERT345sdErrr');
        database.insert(tempFile);
        StaticResource stObj = [SELECT Id, Name, ContentType, Body FROM StaticResource where Name = 'EP_Third_Party_RO_CSV_Test_Data']; 
        context = new EP_OrderImportContext(tempFile);
        context.strCSVFileName = stObj.Name;
        context.blbCSVFile = stObj.Body;     
    }
    static testMethod void hasValidFilePositiveTest() {
        setupData();
        system.debug('$$Context$$ ' + context);
        EP_OrderImportHelper localObj = new EP_OrderImportHelper(context);
    	Test.startTest();
    	boolean result = localObj.hasValidFile();
    	Test.stopTest();
    	system.assert(result);
    }
    static testMethod void hasValidFileTest1() {
    	setupData();	
    	EP_OrderImportHelper localObj = new EP_OrderImportHelper(context);
    	try{
    	Test.startTest();
    		boolean result = localObj.hasValidFile();
    	Test.stopTest();
    	}catch(Exception ex){
    		system.assert(ex.getMessage().contains(system.Label.EP_CSV_IMPORT_ERROR_MSG));
    	}    	
    }
    static testMethod void hasValidFileTest2() { //ToDo :  Create Duplicate
    	setupData();	
    	EP_OrderImportHelper localObj = new EP_OrderImportHelper(context);
    	try{
    	Test.startTest();
    		boolean result = localObj.hasValidFile();
    	Test.stopTest();
    	}catch(Exception ex){
    		//system.assert(ex.getMessage().contains(system.Label.EP_CSV_File_Exists_Error_MSG));
    	}    	
    }
    static testMethod void processCSVDataPositiveTest() {
    	setupData();
        EP_OrderImportHelper localObj = new EP_OrderImportHelper(context);
    	Test.startTest();
    		localObj.processCSVData();
    	Test.stopTest();
    	//As CSV is valid it should be processed successfully without throwing any exception, hence adding dummy assert
    	system.assert(true);
    	
    }
    static testMethod void processCSVDataNegativeTest() {
        setupData();
        EP_OrderImportHelper localObj = new EP_OrderImportHelper(context);
    	try{
	    	Test.startTest();
	    		localObj.processCSVData();
	    	Test.stopTest();
    	}catch(Exception ex){
    		system.assert(ex.getMessage().contains(system.label.EP_HEADER_FILE_ERROR_MSG));
    	}
    }
    static testMethod void processImportRequestPositiveTest() {
    	setupData();
        context.CSVParseSuccess = true;
        EP_OrderImportHelper localObj = new EP_OrderImportHelper(context);
    	localObj.hasValidFile();
    	Test.startTest();
    		localObj.processImportRequest();
    	Test.stopTest();
    	ApexPages.Message[] pageMessages = ApexPages.getMessages();
    	System.assertNotEquals(0, pageMessages.size());
    	Boolean messageFound = checkMessage(system.Label.EP_CSV_Parse_Success_MSG, ApexPages.Severity.CONFIRM);		
		system.assert(messageFound);
	}
    static testMethod void initDataValidationPositiveTest() {
        setupData();
        EP_OrderImportHelper localObj = new EP_OrderImportHelper(context);
        localObj.newfileRecord = tempFile;
        Test.startTest();
            localObj.initDataValidation();
        Test.stopTest();
        //As no logic is performed in this method adding a dummy assert
        system.assert(true);
    }
    //ToDO: Add file import custom setting data
    static testMethod void initPositiveTest() {
        setupData();
        EP_OrderImportHelper localObj = new EP_OrderImportHelper(context);
        localObj.newfileRecord = tempFile;
        Test.startTest();
            localObj.init(context);
        Test.stopTest();
        //As no logic is performed in this method adding a dummy assert
        system.assert(true);
    }
    static testMethod void setObjectFieldMapPositiveTest() {
        setupData();
        EP_OrderImportHelper localObj = new EP_OrderImportHelper(context);
        localObj.newfileRecord = tempFile;
        Test.startTest();
            localObj.setObjectFieldMap();
        Test.stopTest();
        //As no logic is performed in this method adding a dummy assert
        system.assert(true);
    }
    static testMethod void parseCSVRowsPositiveTest() {
        setupData();                
        EP_OrderImportHelper localObj = new EP_OrderImportHelper(context);
        context.strCSVFileContents = localObj.parseCSVforCommaAndNewline(context.blbCSVFile.toString());
        list<string> csvFileLinesList = context.strCSVFileContents.split(EP_Common_Constant.NEW_LINE);
        localObj.newfileRecord = tempFile;
        Test.startTest();
            localObj.parseCSVRows(csvFileLinesList);
        Test.stopTest();
        //As this method delegates to other method adding a dummy assert
        system.assert(true);
    }
    static testMethod void parseCSVRowsNegativeTest() {
        setupData();
        StaticResource statObj = [SELECT Id, Name, ContentType, Body FROM StaticResource where Name = 'EP_Third_Party_RO_CSV_InvalidTestData']; 
        context.strCSVFileName = statObj.Name;
        context.blbCSVFile = statObj.Body;     
        EP_OrderImportHelper localObj = new EP_OrderImportHelper(context);
        context.strCSVFileContents = localObj.parseCSVforCommaAndNewline(context.blbCSVFile.toString());
        list<string> csvFileLinesList = context.strCSVFileContents.split(EP_Common_Constant.NEW_LINE);
        localObj.newfileRecord = tempFile;
        try{
            Test.startTest();
            localObj.parseCSVRows(csvFileLinesList);
            Test.stopTest();
        }catch(Exception ex){           
            Boolean messageFound = checkMessage(system.Label.EP_CSV_Header_Error_MSG, ApexPages.Severity.ERROR);     
            system.assert(messageFound);
        }
    }
    static testMethod void parseCSVColumnPositiveTest() {
        setupData(); 
        EP_OrderImportHelper localObj = new EP_OrderImportHelper(context);
        context.strCSVFileContents = localObj.parseCSVforCommaAndNewline(context.blbCSVFile.toString());
        list<string> csvFileLinesList = context.strCSVFileContents.split(EP_Common_Constant.NEW_LINE);
        localObj.newfileRecord = tempFile;
        Test.startTest();
        localObj.parseCSVColumn(csvFileLinesList);
        Test.stopTest();
        //Asserting the sObjectList is filled as a valid CSV was passed
        system.assert(!localObj.sObjectList.isEmpty());
    }
    static testMethod void parseCSVColumnNegativeTest() {
        setupData(); 
        StaticResource statObj = [SELECT Id, Name, ContentType, Body FROM StaticResource where Name = 'EP_Third_Party_RO_CSV_InvalidTestData']; 
        context.strCSVFileName = statObj.Name;
        context.blbCSVFile = statObj.Body; 
        EP_OrderImportHelper localObj = new EP_OrderImportHelper(context);
        context.strCSVFileContents = localObj.parseCSVforCommaAndNewline(context.blbCSVFile.toString());
        list<string> csvFileLinesList = context.strCSVFileContents.split(EP_Common_Constant.NEW_LINE);
        localObj.newfileRecord = tempFile;
        try{
            Test.startTest();
            localObj.parseCSVColumn(csvFileLinesList);
            Test.stopTest();
        }catch(Exception ex){           
            Boolean messageFound = checkMessage(system.Label.EP_CSV_No_of_Column_Error_Msg, ApexPages.Severity.ERROR);     
            system.assert(messageFound);
        }
    }
	
	//Method to check Apex Page Messages displayed to the user on VF
	private static boolean checkMessage(string msg, ApexPages.Severity severity){
		ApexPages.Message[] pageMessages = ApexPages.getMessages();
    	System.assertNotEquals(0, pageMessages.size());
		for(ApexPages.Message message : pageMessages) {
		    if(message.getDetail() == msg && message.getSeverity() == severity) {
		        return true;        
		    }
		}
		return false;	
	}
}