/**
    * @author           Accenture
    * @name             EP_Import_Retail_Order_Context 
    * @date             16/04/2018
    * @description      Class to initialize the variables for XML file 
    * @param            NA    
    * @return           NA
    */
public with sharing class EP_Import_Retail_Order_Context {
    public String strXMLFileName        {get;set;}
    public Transient Blob blbXMLFile    {get;set;}
    
    
     /**
    * @author           Accenture
    * @name             EP_Import_Retail_Order_Context 
    * @date             16/04/2018
    * @description      Constructor of main class 
    * @param            NA    
    * @return           NA
    */
    public EP_Import_Retail_Order_Context () {
        init();
    }    

    /**
    * @author           Accenture
    * @name             init
    * @date             16/04/2018
    * @description      Method to initialize variables
    * @param            NA    
    * @return           NA
    */
    private void init() {
        blbXMLFile=null;
        strXMLFileName='';
        
    }
}