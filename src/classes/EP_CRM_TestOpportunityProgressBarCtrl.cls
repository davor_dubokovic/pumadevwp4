/* ================================================
 * @Class Name : TestOpportunityProgressBarCtrl
 * @author : Candidate@467073
 * @Purpose: This class is used to test test workOrderItem visual force page
 * @created date: 16/04/2016
 ================================================*/
@isTest
private class EP_CRM_TestOpportunityProgressBarCtrl{
    /* This method gets all the related records based on the family selectedon UI.
    * @param : 
    * @return: 
    */    
    static testmethod void getStages(){
        //Run as Sales User
        Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' limit 1]; 
        User u=new User();
        u.firstName='Test First';
        u.lastname='Sales CRM';
        u.profileid=p.id;
        u.Alias = 'salesman';
        u.Email='standardusercrm@accenture.com'; 
        u.EmailEncodingKey='UTF-8';
        u.LanguageLocaleKey='en_US'; 
        u.LocaleSidKey='en_AU';
        u.TimeZoneSidKey='Australia/Brisbane';
        u.UserName='standardusercrm@accenture.com';
        u.CurrencyIsoCode='AUD';
        insert u;
        //User u=[Select id from User where Profile.Name Like 'EP CRM Sales%' LIMIT 1];
        System.runAs(u){
        // Insert Account
        Account acc=new Account();
        acc.name='Test Accenture';
        insert acc;
        String oppStages=Label.EP_CRM_OPPORTUNITY_STAGES; // get stages used from label.
        List<String> sOppstages=new List<String>();
        if (!String.ISBLANK(oppStages)) {
            for (String s: oppStages.split(',')) {
                s = s.trim();
                sOppstages.add(s);
            }
        }
        //Insert Opportunity
        Opportunity opp=new Opportunity();
        opp.name='Test';
        opp.Stagename=sOppstages[0];
        opp.Account=acc;
        opp.CloseDate=Date.Today() + 5;
        insert opp;
        // Validate picklist values in Progress bar 
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        PageReference pageRef = Page.EP_CRM_Progress_Page;
        Test.setCurrentPage(pageRef);
        Test.startTest();
        EP_CRM_ProgressbarClass ctrlr=new EP_CRM_ProgressbarClass (sc);
        ctrlr.getPicklistValues();
        Test.stopTest();

        }
    }
}