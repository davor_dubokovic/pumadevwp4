/*
*  @Author <Accenture>
*  @Name <EP_ASTVMIShipToAccountSetupToActive>
*  @CreateDate <24/02/2017>
*Novasuite Fixes -- comments added
*  @Description <Handles VMI Ship To Account status change from 04-Account Set-up to 05-Active>
*  @Version <1.0>
*/
public class EP_ASTVMIShipToAccountSetupToActive extends EP_AccountStateTransition {
    /*
    *  @Author <Accenture>
    *  @Name constructor of class EP_ASTVMIShipToAccountSetupToActive
    *  @return boolean
    */
    public EP_ASTVMIShipToAccountSetupToActive() {
        finalState = EP_AccountConstant.ACTIVE;
    }
    /*
    *  @Author <Accenture>
    *  @Name isTransitionPossible
    *  @return boolean
    */
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToAccountSetupToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }
    /*
    *  @Author <Accenture>
    *  @Name isRegisteredForEvent
    *  @return boolean
    */
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToAccountSetupToActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
    /*
     *  @Author <Accenture>
     *  @Name isGuardCondition
     *  @return boolean
    */
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToAccountSetupToActive','isGuardCondition');
        //L4_45352_START
        EP_AccountService service =  new EP_AccountService(this.account);
        if(this.account.localaccount.EP_Synced_With_All__c){
        //L4_45352_END    
        	return true;
        }
        return false;
    }
     /*
     *  @Author <Accenture>
     *  @Name doOnExit
     */
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToAccountSetupToActive','doOnExit');

    }
}