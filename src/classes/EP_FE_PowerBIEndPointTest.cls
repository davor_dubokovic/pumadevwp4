@isTest
public class EP_FE_PowerBIEndPointTest {
    
    
    public static EP_FE_OAuthApps__c app;
    public static EP_FE_PowerBIEndpoint controller = new EP_FE_PowerBIEndpoint();
    
    @testSetup public static void setUp()
    {   
        app = new EP_FE_OAuthApps__c();
        app.Name = 'PowerBI';
        app.Client_Id__c = '94b39fb5-b802-4aa0-b6ab-8532e7c53453';
        app.Client_Secret__c = 'K99lwvGO/qwUuNKTqhd7Kh2BttsasY59x3czuvixbgw=';
        app.Authorization_URL__c = 'https://login.windows.net/common/oauth2/authorize';
        app.Access_Token_URL__c = 'https://login.microsoftonline.com/common/oauth2/token';
        app.Resource_URI__c = 'https://analysis.windows.net/powerbi/api';
        app.grant_type__c ='password';
        app.User_Name__c='powerbidev@coolfurnace.net';
        app.Password__c='Trafigura^123!';
        app.Group_Id__c='d126a7df-d739-4fd0-893f-d7de2045e612';
        app.Report_Id__c='2f88b05a-fac1-4b55-a23c-77227a5ef97f';
        app.PowerBI_URL__c='https://api.powerbi.com/v1.0/myorg/';
        insert app;
        
        DateTime dt = (DateTime) JSON.deserialize('"2018-01-24T09:45:36.933"', DateTime.class);
        EP_FE_PowerBIToken__c powerBIToken = new EP_FE_PowerBIToken__c(Token__c='testToken',
                                                                       Token_Expires_On__c=System.now(),
                                                                       Token_Is_Active__c=true);
        insert powerBIToken;
        
        //controller.application_name = 'PowerBI';
    }
    public static testMethod void testCalloutPositive()
    {   
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new EP_MockPowerBIResponseGenerator());
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = h.send(req);
        EP_FE_PowerBIEndpoint.GetEmbedToken();
        // Verify response received contains fake values
        String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/json');
        String actualValue = res.getBody();
        //String expectedValue = '{"example":"test"}';
        System.assertNotEquals(actualValue, null);
        System.assertEquals(200, res.getStatusCode());
        
        Test.stopTest();
    }
    public static testMethod void testCalloutNegative()
    {    
        EP_FE_PowerBIToken__c tokendata = [SELECT Token_Expires_On__c,Token__c FROM EP_FE_PowerBIToken__c]; 
        try {
            delete tokendata;
        } catch (DmlException e) {
            // Process exception here
        }
        
        Test.startTest();
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new EP_MockPowerBIResponseGenerator());
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = h.send(req);
        
        EP_FE_PowerBIEndpoint.GetEmbedToken();
        
        // Verify response received contains fake values
        String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/json');
        String actualValue = res.getBody();
        //String expectedValue = '{"example":"test"}';
        System.assertNotEquals(actualValue, null);
        System.assertEquals(200, res.getStatusCode());
        
        Test.stopTest();
    }
     public static testMethod void testCalloutDateComparison()
    {    
        EP_FE_PowerBIToken__c powerBITokenToUpdate=[SELECT Token_Is_Active__c, Token_Expires_On__c 
                                                                FROM EP_FE_PowerBIToken__c WHERE Token_Is_Active__c=true LIMIT 1];
                    
                    powerBITokenToUpdate.Token_Expires_On__c=System.now().addDays(4);
                    update powerBITokenToUpdate;
                    
        Test.startTest();
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new EP_MockPowerBIResponseGenerator());
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = h.send(req);
        
        EP_FE_PowerBIEndpoint.GetEmbedToken();
        
        // Verify response received contains fake values
        String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/json');
        String actualValue = res.getBody();
        //String expectedValue = '{"example":"test"}';
        System.assertNotEquals(actualValue, null);
        System.assertEquals(200, res.getStatusCode());
        
        Test.stopTest();
    }
}