/* 
   @Author Spiros Markantonatos
   @name <EP_RetrieveExceptionLogsV2>
   @CreateDate <23/12/2016>
   @Description <This class will be executed by external system to export exception logs. Latest version of the interface is including new date/time filters>
   @Version <1.0>
*/
@RestResource(urlMapping = '/v4/ExceptionLogs/*')
global with sharing class EP_RetrieveExceptionLogsV4 {
    // Const
    
    private static final String RETRIEVE_EXCEPTION_LOGS = 'retrieveExceptionLogs';
    private static final String EP_RETRIEVE_EXCEPTION_LOGS = 'EP_RetrieveExceptionLogsV2';
    private static final String EP_NAME_STR = 'NAME';
    private static final String EP_APP_NAME_STR = 'EP_Application_Name__c';
    private static final String EP_BROWSER_STR = 'EP_Browser__c' ;
    private static final String EP_CLASSNAME = 'EP_Class_Name__c';
    private static final String EP_DEVICE_STR = 'EP_Device__c';
    private static final String EP_COMPONENT = 'EP_Component__c';
    private static final String EP_EXCEP_CODE = 'EP_Exception_Code__c';
    private static final String EP_EXP_DES = 'EP_Exception_Description__c';
    private static final String EP_EXP_DETAILS = 'EP_Exception_Details__c';
    private static final String EP_EXP_TIMESTAMP = 'EP_Exception_Log_Timestamp__c';
    private static final String EP_EXP_TYPE = 'EP_Exception_Type__c';
    private static final String EP_HANDLED_STR = 'EP_Handled__c';
    private static final String EP_METHOD_NAME = 'EP_Method_Name__c';
    private static final String EP_OPERATING_SYS = 'EP_OperatingSystem__c';
    private static final String EP_ORG_ID = 'EP_OrgID__c';
    private static final String EP_PERFORMANED_ACTION = 'EP_PerformedAction__c';
    private static final String EP_SEVERITY = 'EP_Severity__c';
    private static final String EP_RUNNING_USER = 'EP_Running_User__c';
    private static final String EP_USER_LOCALE = 'EP_User_Locale__c';
    private static final String EP_USER_PRO_ID = 'EP_User_Profile_ID__c';
    private static final String EP_USR_ROLE_ID = 'EP_User_Role_ID__c';
    private static final String EP_USER_SESSION_ID = 'EP_User_Session_ID__c';
    private static final String EP_WAS_HANDLED = 'EP_WasHandled__c';
    private static final String EP_AUDIT_USER = 'EP_WS_Audit_User__c';
    private static final String EP_CALLING_APP = 'EP_WS_Calling_Application__c';
    private static final String EP_CALLING_AREA = 'EP_WS_Calling_Area__c';
    private static final String EP_TRANS_ID = 'EP_WS_Transaction_ID__c';
    private static final String EP_FROM_DATE_WHERE_CLAUSE = 'CreatedDate >= :dtFrom';
    private static final String EP_TO_DATE_WHERE_CLAUSE = 'CreatedDate <= :dtTo';
    private static final String EP_LIMIT_CLAUSE = 'LIMIT';
    
    // Properties
    public static String strExceptionLogObjectName {
    	get {
    		if (strExceptionLogObjectName == NULL) strExceptionLogObjectName = 'EP_Exception_log__c';
    		return strExceptionLogObjectName;
    	}
    	set;
    }
    
    // Methods
    
    /*************************************************************************
    *@Description : This method is used to construct and return the 
    				SELECT clause of the query used by the interface
    *@Params      : none                                                      *
    *@Return      : String (SELECT Statement)                                 *     
    **************************************************************************/       
    public static String getObjectFullSelectStatement() {
       
        String strSelectStatement = EP_Common_Constant.BLANK;
        
        strSelectStatement += EP_NAME_STR + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_APP_NAME_STR + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_BROWSER_STR + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_CLASSNAME + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_DEVICE_STR + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_COMPONENT + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_EXCEP_CODE + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_EXP_DES + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_EXP_DETAILS + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_EXP_TIMESTAMP + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_EXP_TYPE + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_HANDLED_STR + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_METHOD_NAME + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_OPERATING_SYS + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_ORG_ID + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_PERFORMANED_ACTION + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_SEVERITY + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_RUNNING_USER + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_USER_LOCALE + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_USER_PRO_ID + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_USR_ROLE_ID + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_USER_SESSION_ID + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_WAS_HANDLED + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_AUDIT_USER + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_CALLING_APP + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_CALLING_AREA + EP_Common_Constant.COMMA_STRING + EP_Common_Constant.SPACE_STRING;
        strSelectStatement += EP_TRANS_ID;
        
        
        return strSelectStatement;
        
    }
    
    /*************************************************************************
    *@Description : This get method is used to retrieve exception logs by 
    				querying from the exception log object based on the date 
    				passed from the url.
    				This version of the interface will not rely/update the
    				isExported flag on the exception record
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/       
    @HttpGet
    global static void retrieveExceptionLogs() {
        String strResponse = EP_Common_Constant.BLANK;
        
        //Initializing ExceptionList will be used to add exception objects after sucessfull callOut and updating to database
        List <EP_Exception_log__c> lExceptionLogUpdate = new List <EP_Exception_log__c> ();
        
        try {
            // Retrieve filter from date from the URL
            DateTime dtFrom = EP_MonitorExportLibClassV3.returnQueryDateFilterFromRequest(EP_Common_Constant.FROM_DATE_TIME_FILTER_NAME);
            // Retrieve filter to date from the URL
            DateTime dtTo = EP_MonitorExportLibClassV3.returnQueryDateFilterFromRequest(EP_Common_Constant.TO_DATE_TIME_FILTER_NAME);
            // Retrieve limit value from the URL
            Integer intLimit = EP_MonitorExportLibClassV3.returnQueryLimitFromRequest(EP_Common_Constant.limitException);
            
            if(dtFrom != NULL)
            {
                // Query EP_Exception_log__c records based on record create date
                String strQuery = EP_Common_Constant.SELECT_STRING + 
                                    EP_Common_Constant.SPACE_STRING +
                                        getObjectFullSelectStatement() +
                                            EP_Common_Constant.SPACE_STRING +
                                                EP_Common_Constant.FROM_STRING +
                                                    strExceptionLogObjectName + 
                                                        EP_Common_Constant.SPACE_STRING +
                                                            EP_Common_Constant.WHERE_STRING +
                                                                EP_Common_Constant.SPACE_STRING +
                                                                    EP_FROM_DATE_WHERE_CLAUSE;
                                                                    
                // Append option to date where clause filter
                if (dtTo != NULL)
                {
                	strQuery += EP_Common_Constant.SPACE_STRING + 
                					EP_Common_Constant.AND_STRING + 
                						EP_Common_Constant.SPACE_STRING + 
                							EP_TO_DATE_WHERE_CLAUSE;       
                }                                       
                
                // Append LIMIT clause
                strQuery += EP_Common_Constant.SPACE_STRING + EP_LIMIT_CLAUSE + EP_Common_Constant.SPACE_STRING + intLimit;                                        
                
                List <EP_Exception_log__c> lExceptions = Database.query(String.escapeSingleQuotes(strQuery));
                
                strResponse = EP_MonitorExportLibClassV3.returnPayloadBody(lExceptions,
                                                                            EP_Common_Constant.EXCEPTION_LOGS,
                                                                                EP_Common_Constant.EXCEPTION_LOGS + 
                                                                                    EP_Common_Constant.NULL_VAR);
            } else {
                strResponse = EP_Common_Constant.GENERIC_FILTER_RESPONSE_FALSE;
            }
        } catch (Exception ex) {
            EP_LoggingService.logServiceException(ex, 
                                            UserInfo.getOrganizationId(), 
                                                EP_Common_Constant.EPUMA, 
                                                    RETRIEVE_EXCEPTION_LOGS, 
                                                        EP_RETRIEVE_EXCEPTION_LOGS, 
                                                            EP_Common_Constant.ERROR, 
                                                                UserInfo.getUserId(), 
                                                                    EP_Common_Constant.TARGET_SF, 
                                                                        EP_Common_Constant.BLANK, 
                                                                            EP_Common_Constant.BLANK);
            strResponse = EP_Common_Constant.RESPONSE_FALSE;
        }
        
        RestContext.response.responseBody = Blob.valueOf(strResponse);
    }
    
}