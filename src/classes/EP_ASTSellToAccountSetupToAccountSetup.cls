/**
* @author <Accenture>
* @name EP_ASTSellToAccountSetupToAccountSetup 
*/
public with sharing class EP_ASTSellToAccountSetupToAccountSetup extends EP_AccountStateTransition{
    /**
    * @author <Accenture>
    * @name contructor of class EP_ASTSellToAccountSetupToAccountSetup 
    */
    public EP_ASTSellToAccountSetupToAccountSetup() {
        finalState = EP_AccountConstant.ACCOUNTSETUP;
    }
    /**
    * @author <Accenture>
    * @name isTransitionPossible 
    */
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToAccountSetupToAccountSetup','isTransitionPossible');
        return super.isTransitionPossible();
    }
    /**
    * @author <Accenture>
    * @name isRegisteredForEvent 
    */
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToAccountSetupToAccountSetup','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
    /**
    * @author <Accenture>
    * @name isGuardCondition
    */
    public override boolean isGuardCondition(){
        //L4_45352_START
        /*
        EP_GeneralUtility.Log('Public','EP_ASTSellToAccountSetupToAccountSetup','isGuardCondition');
        system.debug('Checking Guard conditions for Basic Data Setup to Account Set-up.......');
        system.debug('## 1. Checking if all the review actions are completed for sell to and its related ship to if any......');
        EP_AccountService service = new EP_AccountService(this.account);
        if(!service.isReviewActionsCompleted()){
            accountEvent.setEventMessage('Please complete all review actions');
            return false;
        }
        system.debug('## 2. Checking if  if Delivery type is Delivery, then one Ship To with  Account set setup status is required.....');
        if(EP_AccountConstant.DELIVERY.equalsIgnoreCase(this.account.localAccount.EP_Delivery_Type__c)){
            if(!service.isSetUpShipToAssociated()){
                accountEvent.setEventMessage(System.Label.EP_No_Set_Up_Ship_To);
                return false;
            }    
        }*/
        //L4_45352_END
        return true;
    }
}