/* 
   @Author <Amit Singh>
   @name <EP_ProductListTriggerHelper>
   @CreateDate <19/09/2016>
   @Description <This is helper class of EP_ProdutListTrigger Handler> 
   @Version <1.0>
*/
public with sharing class EP_ProductListTriggerHelper {
	//Variable Declaration for Exception Handling
    private static final String CLASS_NAME = 'EP_ProductListTriggerHelper';
    private static final String CHECK_UNIQUE_METHOD = 'checkForUniqueName';
    
    /*
	 * @author <Amit Singh>
	 * @description <This method validate the unique name of pricebook. 
	 				 This method exeutes when user insert new pricebook or update existing pricebook.>
	 * @name <checkForUniqueName>
	 * @date <19/09/2016>
	 * @param List<Pricebook2>, Map<Id, Pricebook2>
	 * @return void
	 */
    public static void checkForUniqueName(List<Pricebook2> listOfPriceBook, Map<Id, Pricebook2> mapOfIdOldPricebook){
        Set<String> setOfPriceBookNames = new Set<String>(); 
        List<Pricebook2> listofPriceBooksToValidate = new List<Pricebook2>();
        Integer nRows = EP_Common_Util.getQueryLimit();
        try{
            for(Pricebook2 priceBookInstance : listOfPriceBook){
                //Condition-1:: Check for new Pricebook (pb.id == nulll)
                //Condition-2:: if pricebook update then check for name change
                if(priceBookInstance.id == null || (priceBookInstance.id != null && mapOfIdOldPricebook.get(priceBookInstance.id).Name != priceBookInstance.Name)){
                    //Collect pricebook Name in set to query records
                    setOfPriceBookNames.add(priceBookInstance.Name);
                    listofPriceBooksToValidate.add(priceBookInstance);
                }
            }
            
            Map<String, PriceBook2> mapOfNamePriceBooks = new Map<String, PriceBook2>();
            if(!setOfPriceBookNames.isEmpty() && mapOfNamePriceBooks != null){
            	//Query existing pricebooks 
            	for(Pricebook2 priceBookInstance: [Select id, Name from Pricebook2 where Name in:setOfPriceBookNames limit:nRows]){
                    mapOfNamePriceBooks.put(priceBookInstance.Name, priceBookInstance);
                }
            }
            
            if(!listofPriceBooksToValidate.isEmpty()){
                for(PriceBook2 priceBookInstance : listofPriceBooksToValidate){
                    //If pricebook with same name found then show error message
                    if(mapOfNamePriceBooks.containsKey(priceBookInstance.Name)){
                        priceBookInstance.Name.AddError(system.label.EP_ProductList_Name_Duplicate_Error_Message);
                    }
                }
            }
        }
        catch(Exception e){
            EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, CHECK_UNIQUE_METHOD, CLASS_NAME,apexPages.severity.ERROR);
        }
    }
}