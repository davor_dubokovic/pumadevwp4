/* 
    @Author : Accenture
    @name : EP_OrderSyncPayloadCtrlExtn
    @CreateDate : 02/08/2017
    @Description : This class is use to Hold the property of the Header for Inbound Messages
    @Version : 1.0
*/
public virtual class EP_MessageHeader {
    public String MsgID;
    public String InterfaceType;
    public String SourceGroupCompany;
    public String DestinationGroupCompany;
    public String SourceCompany;
    public String DestinationCompany;
    public String CorrelationID;
    public String DestinationAddress;
    public String SourceResponseAddress;
    public String SourceUpdateStatusAddress;
    public String DestinationUpdateStatusAddress;
    public String MiddlewareUrlForPush;
    public String EmailNotification;
    public String ErrorCode;
    public String ErrorDescription;
    public String ProcessingErrorDescription;
    public String ContinueOnError;
    public String ComprehensiveLogging;
    public String TransportStatus;
    public String ProcessStatus;
    public String UpdateSourceOnReceive;
    public String UpdateSourceOnDelivery;
    public String UpdateSourceAfterProcessing;
    public String UpdateDestinationOnDelivery;
    public String CallDestinationForProcessing;
    public String ObjectType;
    public String ObjectName;
    public String CommunicationType;
}