/* 
  @Author <Vikas Malik (Accenture)>
   @name <EP_Common_Util>
   @CreateDate <08/10/2015>
   @Description <This Class will include the Commom Methods which will be used across the code>
   @Version <1.0>
 
*/
public with sharing class EP_Common_Util {

    public static Boolean isValidAddress = false;
    public static Boolean isAddressCR = false;
    private static Final String getRecodTypeDevNameFromID_method ='getRecodTypeDevNameFromID';
    private static Final String CLASSNAME = 'EP_Common_Util';
    private static Final String checkPortalUser_method = 'checkPortalUser';
    private static Final String getRecordTypeIdForGivenSObjectAndName_method = 'getRecordTypeIdForGivenSObjectAndName';
    private static Final String getRecordTypeForGivenSObjectAndName_method = 'getRecordTypeForGivenSObjectAndName';
    private static Final String fetchRecordTypeId_method = 'fetchRecordTypeId';
    private static Final String checkIsRecordsDefined_method = 'checkIsRecordsDefined';
    private static Final String fetchRecordTypeName_method = 'fetchRecordTypeName';
    private static Final String getAssignmentRules_method = 'getAssignmentRules';
    private static Final String getFieldsLabels_method = 'getFieldsLabels';
    private static Final String createIntegrationStatusByObjAPIName_method = 'createIntegrationStatusByObjAPIName';
    private static Final String getQueryLimit_method = 'getQueryLimit';
    private static Final String getPrefixObjTypeMap_method = 'getPrefixObjTypeMap';
    private static Final String getUserInfo_method = 'getUserInfo';
    private static Final String createDataSet_method = 'createDataSet';
    private static Final String createOption_method = 'createOption';
    private static Final String createApexMessage_method = 'createApexMessage';
    
    /**
        Author: Vikas Malik(Accenture)
        Description: This method will give the Record Type Developer Name for provided recordTypeId 
        Method Name: getRecodTypeDevNameFromID
        Created Date: 08/10/2015
        Params: String 
        Return Type: String
    */  
    public static string getRecodTypeDevNameFromID(string recTypeID){
        string rtypeDevName ;
        try{
            //Fetch Record Types from Argument passed
            List<RecordType> listOfRecordTypes = [Select SobjectType, Name, Id, DeveloperName From RecordType WHERE id=: recTypeID LIMIT :EP_Common_Constant.ONE];
            if (!listOfRecordTypes.isEmpty()){      //recTypes != null &&
                rtypeDevName = listOfRecordTypes[0].DeveloperName;
            }
        }Catch(Exception handledException){
            rtypeDevName = null;
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, getRecodTypeDevNameFromID_method, CLASSNAME, ApexPages.Severity.ERROR);
        }
        return rtypeDevName;
    }
    
    /**
        Author: Accenture
        Description: This method will check whether user is a portal user or not
        Method Name: getRecodTypeDevNameFromID
        Created Date: 08/10/2015
        Params: Id
        Return Type: Boolean
    */
    public static boolean checkPortalUser(Id userId){
        boolean isPortalUser = false;
        
        try{
            //get user details
            User loggedInUser = [Select id, contactId, isPortalEnabled From User Where id =: userId Limit :EP_Common_Constant.ONE];
            //checking if user is a portal user
            if(loggedInUser.contactId != null && loggedInUser.isPortalEnabled){ 
                isPortalUser = true;
            }else{
                isPortalUser = false;
            }
        }
        catch(Exception ex){
           EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, checkPortalUser_method, CLASSNAME, ApexPages.Severity.ERROR);
        }
        return isPortalUser;
    }
    
    /**
        Author: Accenture
        Description: This method will give the Record Type for provided sobject and DeveloperName
        Method Name: getRecordTypeIdForGivenSObjectAndName
        Created Date: 08/10/2015
        Params: Id
        Return Type: Boolean
    */  
    public static Id getRecordTypeIdForGivenSObjectAndName(String sobjectType, String recTypeDevName){
        
        try{
            //get record types from Developer name and object type
            List<RecordType> listOfRecordTypes = [Select SobjectType, Name, Id, DeveloperName From RecordType WHERE DeveloperName = :recTypeDevName AND SobjectType = :sobjectType LIMIT 1];
            if (!listOfRecordTypes.isEmpty()){ //recordTypes != null &&
                return listOfRecordTypes[0].Id;
            }
        }
        catch(Exception handledException){
           EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, getRecordTypeIdForGivenSObjectAndName_method, CLASSNAME, ApexPages.Severity.ERROR);
        }
        return null;
    }
    /**
        Author: Accenture
        Description: This method will give the Record Type for provided sobject and DeveloperName
        Method Name: getRecordTypeForGivenSObjectAndName
        Created Date: 08/10/2015
        Params: String, String
        Return Type: RecordType
    */  
    public static RecordType getRecordTypeForGivenSObjectAndName(String sobjectType, String recTypeDevName){
        
        try{
            //get record types from Developer name and object type
            List<RecordType> listOfRecordTypes = [Select SobjectType, Name, Id, DeveloperName From RecordType WHERE DeveloperName = :recTypeDevName AND SobjectType = :sobjectType LIMIT 1];
            if (!listOfRecordTypes.isEmpty()){  //(recTypes != null &&
                return listOfRecordTypes[0];
            }
        }
        catch(Exception handledException){
           EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, getRecordTypeForGivenSObjectAndName_method, CLASSNAME, ApexPages.Severity.ERROR);
        }
        return null;
    }
    /**
        Author: Accenture
        Description: This method will give the RecordTypeId for provided sobject and Record type Name
        Method Name: fetchRecordTypeId
        Created Date: 08/10/2015
        Params: String, String
        Return Type: Id
    */
    public static Id fetchRecordTypeId(String ObjectAPIName
                                    ,String RecordTypeName){
        
        Id recordTypeId;  
                                   
        try{
            //Dynamic query to get record type id
            recordTypeId =  Schema.getGlobalDescribe().get(ObjectAPIName).getDescribe().getRecordTypeInfosByName().get(RecordTypeName).getRecordTypeId();
        }catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, fetchRecordTypeId_method, CLASSNAME, ApexPages.Severity.ERROR);
        }
        return recordTypeId;
        
    } 
     /**
        Author: Accenture
        Description: This method determines whether RecordTypeId is defined for the Object
        Method Name: checkIsRecordsDefined
        Created Date: 08/10/2015
        Params: String
        Return Type: Boolean        
    */
    public static Boolean checkIsRecordsDefined(String ObjectAPIName){
        Boolean objectRecordTypeFound;   
                                 
        try{
            //dynamic query to check if record type id has been found
            objectRecordTypeFound =  Schema.getGlobalDescribe().get(ObjectAPIName).getDescribe().fields.getMap().containsKey(EP_Common_Constant.REC_TYPE_ID ); //'RecordTypeId'
        }catch(Exception ex){
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, checkIsRecordsDefined_method, CLASSNAME, ApexPages.Severity.ERROR);
            return false;
        }
        return objectRecordTypeFound;
    }
    
    /**
        Author: Accenture
        Description: This method will give the RecordTypeName for provided sobject and Record type Id
        Method Name: fetchRecordTypeName
        Created Date: 08/10/2015
        Params: String, Id
        Return Type: String
    */
    public static String fetchRecordTypeName(String ObjectAPIName
                                    ,Id recordTypeId){
        
        String recordTypeName,strErrMsg;                            
        try{
            //Dynamic Apex to get record type name
            recordTypeName =  Schema.getGlobalDescribe().get(ObjectAPIName).getDescribe().getRecordTypeInfosById().get(recordTypeId).getName();
        }catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, fetchRecordTypeName_method, CLASSNAME, ApexPages.Severity.ERROR);
        }
        return recordTypeName;
        
    } 
    
    
    
    
    /**
        Author: Accenture
        Description: This method will return the Active Assignment Rules for a given object type
        Method Name: getAssignmentRules
        Created Date: 08/10/2015
        Params: String
        Return Type: List<AssignmentRule>       
    */  
    public static List<AssignmentRule> getAssignmentRules(String sobjectType){
              
        List<AssignmentRule> listOfAssignmentRules = new List<AssignmentRule>();                      
        try{
            //fetch Active assignment rules 
            listOfAssignmentRules = [select id from AssignmentRule where SobjectType = :sobjectType and Active = true LIMIT 1];
        }catch(Exception ex){
           EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, getAssignmentRules_method, CLASSNAME, ApexPages.Severity.ERROR);
        }
        return listOfAssignmentRules;
    }
    
    
    /**
        Author: Accenture
        Description: This method returns the Label for all fields for a given object
        Method Name: getFieldsLabels
        Created Date: 08/10/2015
        Params: String
        Return Type: Map<String,String>
    */
    public static Map<String,String> getFieldsLabels(String sobjectType){
        Map<String,String> mapOfFieldApiNameAndLabel = new Map<String,String>();
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType objSchema = schemaMap.get(sobjectType);
        //field name and field properties map
        Map<String, Schema.SObjectField> mapOfFieldAndFieldProperties = objSchema.getDescribe().fields.getMap();
        
        try{
           // get Field labels from map
            for (String fieldName: mapOfFieldAndFieldProperties.keySet()) {
                mapOfFieldApiNameAndLabel.put(fieldName,mapOfFieldAndFieldProperties.get(fieldName).getDescribe().getLabel());
            }
         }catch(Exception handledException){
           EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, getFieldsLabels_method, CLASSNAME, ApexPages.Severity.ERROR);
         }
        return mapOfFieldApiNameAndLabel;
    }
    /**
        Author: Accenture
        Description: This method returns the Integration status by Object
        Method Name: createIntegrationStatusByObjAPIName
        Created Date: 08/10/2015
        Params: 
        Return Type: Map<String,String>
    */
    public static map<String,String> createIntegrationStatusByObjAPIName(){
        map<String,String> mapOfApiNameAndName = new map<String,String>();
        
        try{
            //Iterating on custom setting 'EP_Integration_Status_Update__c'
            for(EP_Integration_Status_Update__c integrationCS : EP_Integration_Status_Update__c.getAll().values()){
                mapOfApiNameAndName.put(integrationCS.EP_API_Name__c.toUpperCase()
                                        ,integrationCS.Name);
        }
        }catch(Exception handledException){
           EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, createIntegrationStatusByObjAPIName_method, CLASSNAME, ApexPages.Severity.ERROR);
        }
        return mapOfApiNameAndName;
    }
    
    /**
        Author: Accenture
        Description: This method return salesforce query limit
        Method Name: getQueryLimit
        Created Date: 08/10/2015
        Params: 
        Return Type: Integer
    */
    public static Integer getQueryLimit()
    {
        Integer queryRows;
        
        try{
            queryRows= Limits.getLimitQueryRows() - Limits.getQueryRows();
        }catch(Exception handledException){
           EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, getQueryLimit_method, CLASSNAME, ApexPages.Severity.ERROR);
        }
        return queryRows;
    }
    /*
        Author: Accenture
        Description: THIS METHOD RETURNS URER DETALS
        Method Name: getUserInfo
        Created Date: 08/10/2015
        Param
        Return Type: User
    */
    public static User getUserInfo( Id UserId )
    {
        User loggedInUser;
        String strErrMsg ;
        try{
            if( UserId != null )
            {    // get user details
                loggedInUser = [Select Id, Name, ProfileId, Profile.Name from User where Id =: UserId Limit 1];
            }
        }catch(Exception handledException){
           EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, getUserInfo_method, CLASSNAME, ApexPages.Severity.ERROR);
        }
        return loggedInUser;
    }
    
    /*
        Author: Accenture
        Description: This method creates dataset for acknowledgement
        Method Name: createDataSet
        Created Date: 08/10/2015
        Params: String,String,String,String, EP_AcknowledmentGenerator.cls_dataset, String
        Return Type: EP_AcknowledmentGenerator.cls_dataset
    */
    public static EP_AcknowledmentGenerator.cls_dataset createDataSet(String Name
                                      ,String seqId
                                      ,String errorCode
                                      ,String errorDescription){
      EP_AcknowledmentGenerator.cls_dataset dataSet;
      
      try{
      //create data set for acknowledgement
      dataSet = new EP_AcknowledmentGenerator.cls_dataset();
      dataSet.name = name;
      dataSet.seqId = seqId;
      dataSet.errorCode = errorCode;
      dataSet.errorDescription = errorDescription;
      }catch(Exception handledException){
           EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, createDataSet_method, CLASSNAME, ApexPages.Severity.ERROR);
      }
      return dataset;
    }
    
     /*
        Author: Accenture
        Description: Creates an option for custom picklist field.
        Method Name: createOption
        Created Date: 08/10/2015
        Params: String, String
        Return Type: SelectOption 
     */
    public static SelectOption createOption(string objId, string value){
        
        SelectOption listOption;
        try{
        //Select option list with objId as key value as value of key
            listOption = new SelectOption(objId,value);
        }catch(Exception handledException){
           EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, createOption_method, CLASSNAME, ApexPages.Severity.ERROR);
        } 
        return listOption;
    }
    
    /**
        Author: Accenture
        Description: This method will create page messages.
        Method Name: createApexMessage
        Created Date: 08/10/2015
        Params: ApexPages.Severity, String
        Return Type: ApexPages.Message
    */
    public static ApexPages.Message createApexMessage(ApexPages.Severity severity, String summary){
        
        ApexPages.Message pageMessage;
        try{
        //return pageMessage for Visualforce page with its Severity and Description
          pageMessage = new ApexPages.Message(severity,summary);
        }catch(Exception handledException){
           EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, createApexMessage_method, CLASSNAME, ApexPages.Severity.ERROR);
        }
        return pageMessage;
    }
    
    /**
        Author: Sandeep Kumar
        Description: This method returns fields of fieldSetName from objectName 
        Method Name: getFieldSetMember
        Created Date: 01/18/2015
        Params: String, String
        Return Type: List<Schema.FieldSetMember> 
    */
    public static List<Schema.FieldSetMember> getFieldSetMember(String objectName, String fieldSetName){
        return Schema.getGlobalDescribe().get(objectName)
                                        .getDescribe().FieldSets.getMap().get(fieldSetName).getFields();
    }
	
	  /** L4 Change Start **/
    /**
    * @author       Accenture
    * @name         addCommonErrorToallRecord
    * @date         05/12/2017
    * @description  This method add error message to all recods.
    * @param        List<SObject>, string
    * @return       NA
    */  
    public static void addCommonErrorToallRecord(List<SObject> records, string errorMessage){
        for(SObject rec : records){
            rec.addError(errorMessage);
        }
    }
    /** L4 Change End **/
}