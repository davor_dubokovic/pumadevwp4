/* 
  @Author <Ashok Arora>
   @name <EP_LeadTriggerHandler>
   @CreateDate <10/11/2015>
   @Description <This class handles requests from Lead Trigger> 
   @Version <1.0>
   @Modified By Shailla Gokhale, for S2S purpose
*/
public with sharing class EP_LeadTriggerHandler {
    public static Boolean isExecuteBeforeUpdate = false;
    public static Boolean isExecuteAfterUpdate = false;
    public static Boolean isInsertRecursiveTrigger = False;
    
    /*
        This method handles before update requests from Lead trigger
    */
    /*public static void doBeforeUpdate(List<Lead>lOldLeads
                                      ,List<Lead>lNewLeads
                                      ,map<Id,Lead>mOldLeads
                                      ,map<Id,Lead>mNewLeads){
    
        isExecuteBeforeUpdate = true;//SET TRUE TO AVOID RE-EXECUTION
    
        //UPDATE RECORDT TYPE OF LEAD TO ACEPTED ONCE IT IS APPROVED
        //EP_LeadTriggerHelper.updateAcceptedRecordType(lNewLeads);
        //To populate lead data from country object
        //EP_LeadTriggerHelper.populateLeadData(lNewLeads);
  }*/
  /*
  *This method handles before insert requests from Lead trigger
  */
   public static void doBeforeInsert(List<Lead> leadsData){
        //To populate lead data from country object on before insert operation
        //EP_LeadTriggerHelper.populateLeadData(leadsData);
        //EP_LeadTriggerHelper.createCase(leadsData);
   }
  
  /*
    This method handles before update requests from Lead trigger
  */
  public static void doAfterUpdate(List<Lead>lOldLeads
                                      ,List<Lead>lNewLeads
                                      ,map<Id,Lead>mOldLeads
                                      ,map<Id,Lead>mNewLeads){
    
    isExecuteAfterUpdate = true;//SET TRUE TO AVOID RE-EXECUTION
    
    //MAP PAYMENT METHOD FROM CONVERTED LEAD AND DEFAULT PAYMENT TERM ON CONVERTED ACCOUNT
    EP_LeadTriggerHelper.mapPaymentMethodAndTerm(mNewLeads);
    //UPDATE PRODUCT INTERESTED IN ON CASE
    //EP_LeadTriggerHelper.updateCasePrdIntIn(mOldLeads
                        //,mNewLeads);
  }
  
    /* This method is execute after Lead record creation.    
  * @description : This method will create Lead records on the secondary connection org    
  * @param : Lead record list    
  * @return: NA    
  */    
  public static void afterInsertConnectionLead(list<Lead> TriggerNew){        
  try{       
  // Define connection id     
  Id networkId = ConnectionHelper.getConnectionId(System.Label.Connection_Name);         
  List<Lead> localAccounts = new List<Lead>();     
  Set<Id> sharedAccountSet = new Set<Id>();            
  // only share records created in this org, do not add contacts received from another org.     
  for (Lead newLead : TriggerNew) {             
  localAccounts.add(newLead);                   
  }        
  //if (sharedAccountSet.size() > 0) {             
  List<PartnerNetworkRecordConnection> leadConnections =  new  List<PartnerNetworkRecordConnection>();                         
  for (Lead newLead : localAccounts) {                  
  // if (sharedAccountSet.contains(newLead.ParentId)) {                                       
  PartnerNetworkRecordConnection newConnection = new PartnerNetworkRecordConnection(                           
  ConnectionId = networkId,                           
  LocalRecordId = newLead.Id,                           
  SendClosedTasks = false,                           
  SendOpenTasks = false,                           
  SendEmails = false                                                 
  //ParentRecordId = newLead.ParentId 
  );                                                                                                 
  leadConnections.add(newConnection);                               
  //  }             
  }               
  if (leadConnections.size() > 0 ) {                    
  database.insert(leadConnections);             
  }                    
  }        
  catch(exception ex){            
  ex.getmessage();        
  }        
 }   
}