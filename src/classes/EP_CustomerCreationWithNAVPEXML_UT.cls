@isTest
public with sharing class EP_CustomerCreationWithNAVPEXML_UT {
    
    static testMethod void init_testPositive() {
    	EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
    	system.debug('KK--->'+objAccDomain.localAccount);
        EP_TestDataUtility.createBankAccount(objAccDomain.localAccount.ParentId);
        EP_CustomerCreationWithNAVPEXML objLocal = new EP_CustomerCreationWithNAVPEXML();
        objLocal.recordId =objAccDomain.localAccount.ParentId;
        Test.startTest();
        objLocal.init();
        Test.stopTest();
        
        System.assertEquals( true,objLocal.lstShipToAddresses.size()>0);
         
    }
    
    static testMethod void init_testNegative() {
    	account objAcc =EP_TestDataUtility.createBillToAccount() ;
    	insert objAcc;

        EP_TestDataUtility.createBankAccount(objAcc.id);
        EP_CustomerCreationWithNAVPEXML objLocal = new EP_CustomerCreationWithNAVPEXML();
        objLocal.recordId =objAcc.id;
        Test.startTest();
        objLocal.init();
        Test.stopTest();
        
        System.assertEquals( true,objLocal.lstShipToAddresses.size()==0);
         
    }
    static testMethod void createPayload_testPositive() {
    	EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        EP_TestDataUtility.createBankAccount(objAccDomain.localAccount.ParentId);
        EP_CustomerCreationWithNAVPEXML objLocal = new EP_CustomerCreationWithNAVPEXML();
        objLocal.recordId =objAccDomain.localAccount.ParentId;
        Test.startTest();
        objLocal.init();
        objLocal.isEncryptionEnabled =true;
        objLocal.createPayload();
        Test.stopTest(); 
        
        System.assertEquals( true,objLocal.lstShipToAddresses.size()>0);
        
    }
    static testMethod void createPayload_testNegative() {
    	EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        EP_TestDataUtility.createBankAccount(objAccDomain.localAccount.ParentId);
        EP_CustomerCreationWithNAVPEXML objLocal = new EP_CustomerCreationWithNAVPEXML();
        objLocal.recordId =objAccDomain.localAccount.id;
        Test.startTest();
        objLocal.init();
        objLocal.isEncryptionEnabled =true;
        objLocal.createPayload();
        Test.stopTest(); 
         
        System.assertEquals( 'MSG',objLocal.MSGNode.getname());
        
    }
    static testMethod void addShipToAddresses_testPositive() {
        EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        EP_TestDataUtility.createBankAccount(objAccDomain.localAccount.ParentId);
        EP_CustomerCreationWithNAVPEXML objLocal = new EP_CustomerCreationWithNAVPEXML();
        objLocal.recordId =objAccDomain.localAccount.ParentId;
        Test.startTest();
        objLocal.init();
        DOM.Document tempDoc = new DOM.Document();
        Dom.XMLNode customersNode = tempDoc.createRootElement(EP_AccountConstant.CUSTOMERS,null, null);
        objLocal.addShipToAddresses(customersNode); 
        Test.stopTest(); 
        
        System.assertEquals( true,customersNode.getChildren().size()>=0);
        
    }
    static testMethod void addShipToAddresses_testNegative() {
        EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        EP_TestDataUtility.createBankAccount(objAccDomain.localAccount.ParentId);
        EP_CustomerCreationWithNAVPEXML objLocal = new EP_CustomerCreationWithNAVPEXML();
        objLocal.recordId =objAccDomain.localAccount.id;
        DOM.Document tempDoc = new DOM.Document();
        Dom.XMLNode customersNode = tempDoc.createRootElement(EP_AccountConstant.CUSTOMERS,null, null);
        Test.startTest();
        objLocal.init();
        
        
        objLocal.addShipToAddresses(customersNode); 
        Test.stopTest(); 
        
        System.assertEquals( true,customersNode.getChildren().size()<=0);
        
    }
    static testMethod void addCustBankNode_testNegative() {
        EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
    	system.debug('KK--->'+objAccDomain.localAccount);
        EP_TestDataUtility.createBankAccount(objAccDomain.localAccount.ParentId);
        EP_CustomerCreationWithNAVPEXML objLocal = new EP_CustomerCreationWithNAVPEXML();
        objLocal.recordId =objAccDomain.localAccount.ParentId;
        DOM.Document tempDoc = new DOM.Document();
        Dom.XMLNode customersNode = tempDoc.createRootElement(EP_AccountConstant.CUSTOMERS,null, null);
        Test.startTest();
        objLocal.init(); 
        
        
        objLocal.addCustBankNode(customersNode);
        Test.stopTest(); 
        
        System.assertEquals( true,customersNode.getChildElements().size()>=1);
        
    }
    static testMethod void addCustBankNode_testPositive() {
        EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
    	system.debug('KK--->'+objAccDomain.localAccount);
        EP_TestDataUtility.createBankAccount(objAccDomain.localAccount.ParentId);
        EP_CustomerCreationWithNAVPEXML objLocal = new EP_CustomerCreationWithNAVPEXML();
        objLocal.recordId =objAccDomain.localAccount.id;
        Test.startTest();
        objLocal.init(); 
        DOM.Document tempDoc = new DOM.Document();
        Dom.XMLNode customersNode = tempDoc.createRootElement(EP_AccountConstant.CUSTOMERS,null, null);
        objLocal.addCustBankNode(customersNode);
        Test.stopTest(); 
        
        System.assertEquals( true,customersNode.getChildElements().size()<=0);
        
    }
}