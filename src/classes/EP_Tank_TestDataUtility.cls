@isTest
public class EP_Tank_TestDataUtility {    
    
    /** 
    *  Method to create tank record with status New and Tank Dip Entry Mode as Portal Dip Entry
    *  @name <createTank>
    *  @param Number of tank records to be created, boolean value to specify if records should be inserted or not.
    *  @return Tank list.
    *  @throws NA
    */
    public static List<EP_Tank__c> createTank(integer cont, boolean doInsert){
        //Creating sell To Account
        List<Account> sellToList =  new List<Account>();
        sellToList = EP_AccountTestDataUtility.createSellToAccountWithSetupStatus(cont,true);
        
        //Creating VMI ship to account for the above created sell To
        List<Account> shipToList = EP_AccountTestDataUtility.populateVMIShipToObject(sellToList,false); 
        for(Account acc : shipToList){
            acc.EP_Ship_To_Opening_Days__c = 'Monday; Tuesday; Wednesday; Thursday';
        }
        database.insert(shipToList);

        //Getting sell To account product Map
        map<id,list<id>> sellTo_ProductIdMap = EP_AccountTestDataUtility.getProductsforSellToAccounts(sellToList);

        EP_Tank__c tankObj = new EP_Tank__c();
        List<EP_Tank__c> tankList =  new List<EP_tank__c>();
        for(Account acc: shipToList) {
            tankObj = new EP_Tank__c();
            tankObj.EP_Ship_To__c = acc.Id;
            tankObj.EP_Safe_Fill_Level__c = 9000;
            tankObj.EP_Deadstock__c = 1000;
            tankOBJ.EP_Product__c = sellTo_ProductIdMap.get(acc.ParentId)[0];
            /* #L4_45352_Start*/
            //tankObj.EP_Tank_Status__c = EP_Common_Constant.NEW_TANK_STATUS;
            /* #L4_45352_END*/
            tankObj.EP_Tank_Dip_Entry_Mode__c = EP_Common_Constant.TANK_DIP_ENTRY_MODE;
            tankObj.EP_Capacity__c = 10000;                
            tankList.add(tankObj);
        }
        if(doInsert){
            insert tankList;           
        }
        return tankList; 
    }
    
    public static EP_Tank__c createTank(Account shipTo, boolean doInsert){
        EP_Tank__c tankObj = new EP_Tank__c();
        tankObj = new EP_Tank__c();
        tankObj.EP_Ship_To__c = shipTo.Id;
        tankObj.EP_Safe_Fill_Level__c = 9000;
        tankObj.EP_Deadstock__c = 1000;
        //tankOBJ.EP_Product__c = sellTo_ProductIdMap.get(acc.ParentId)[0];
        /* #L4_45352_Start*/
        //tankObj.EP_Tank_Status__c = EP_Common_Constant.NEW_TANK_STATUS;
        /* #L4_45352_END*/
        tankObj.EP_Tank_Dip_Entry_Mode__c = EP_Common_Constant.TANK_DIP_ENTRY_MODE;
        tankObj.EP_Capacity__c = 10000; 
        if(doInsert){
            insert tankObj;           
        }
        return tankObj;  
    }
    
    
    /**
    *  Method to get shipto ids from Tank records
    *  @name <creatTankAction>
    *  @param tankList List of tank
    *  @return Set of ShipTo Ids
    *  @throws NA
    */
    public static set<Id> getShipToAccIds (List<EP_Tank__c> tankList){ 
        set<Id> shipToAccIds = new set<Id>();
        if(!tankList.isEmpty()){
            for(EP_Tank__c tank : tankList){
                shipToAccIds.add(tank.EP_Ship_To__c);
            }
        }
        return shipToAccIds;
    }
}