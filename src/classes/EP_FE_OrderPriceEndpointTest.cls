@isTest 
/*********************************************************************************************
     @Author <>
     @name <EP_FE_OrderPriceEndpointTest>
     @CreateDate <>
     @Description <  >  
     @Version <1.0>
    *********************************************************************************************/
private class EP_FE_OrderPriceEndpointTest{
/*********************************************************************************************
     @Author <>
     @name <Create>
     @CreateDate <>
     @Description <  >  
     @Version <1.0>
*********************************************************************************************/
    static testMethod void  testCreate(){
        Test.StartTest();

        //Order orderOb = EP_FE_TestDataUtility.createNonVMIOrder();
        //EP_FE_OrderPriceEndpoint ob = new EP_FE_OrderPriceEndpoint();
        EP_FE_OrderPriceResponse resp = new EP_FE_OrderPriceResponse ();

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.httpMethod = 'Get';
        req.requestUrI = '/services/apexrest/FE/V1/order/price/track';
        RestContext.Request = req ; 
        RestContext.Response = res ;

        resp = EP_FE_OrderPriceEndpoint.calculateOrderPrice();
        //Order ord = EP_FE_OrderPriceEndpoint.fetchOrderWithAllPricingInformation(orderOb.id);
        //Boolean canSeePrices= EP_FE_OrderPriceEndpoint.canSeePrices(orderOb);
        //Boolean isOrderPricingReady= EP_FE_OrderPriceEndpoint.isOrderPricingReady(orderOb);
        //EP_FE_OrderPriceEndpoint.caclulatePricesForOrderData(resp,orderOb);
        Test.StopTest();
    }
/*********************************************************************************************
     @Author <>
     @name <EP_FE_OrderPriceEndpointTest>
     @CreateDate <>
     @Description <  >  
     @Version <1.0>
*********************************************************************************************/
    static testMethod void testPriceWithOrderId() {    
        //To Call rest api
        //EP_FE_OrderPriceEndpoint ob = new EP_FE_OrderPriceEndpoint();
        EP_FE_OrderPriceResponse response = new EP_FE_OrderPriceResponse ();
        
        Test.startTest(); 

        csord__order__c newOrder = EP_FE_TestDataUtility.createNonVMIOrder(); 

        RestRequest req = new RestRequest();  
        RestResponse res = new RestResponse(); 
        req.httpMethod = 'Get';  
        req.requestURI = '/services/apexrest/FE/V1/order/price/track';  
        RestContext.request = req; 
        RestContext.Response = res ;

        req.params.put('orderId', newOrder.Id);

        response = EP_FE_OrderPriceEndpoint.calculateOrderPrice();
        Test.stopTest();       
    } 
 }