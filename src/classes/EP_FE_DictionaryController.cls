/* 
  @Author <Nicola Tassini>
   @name <EP_FE_DictionaryController>
   @CreateDate <27/04/2016>
   @Description <Support controller to generate the dictionary for the FrontEnd application>  
   @Version <1.0>
*/
public virtual with sharing class EP_FE_DictionaryController {

    public static final String LANGUAGE_EN = 'en';
    public static final String CONTENT_TYPE = 'application/json';
    public String language {get;set;}
    public List<String> customLabels {get;set;}
    public List<ObjectFieldLabel> objectFieldLabels {get;set;}
    public String contentType {get; set;}

    private static final String LABEL_CUSTOM = 'label';
    private static final String LABEL_OBJECTFIELD = 'field';
/*
*
*/
    public EP_FE_DictionaryController() {
        this.customLabels = new List<String>();
        this.objectFieldLabels = new List<ObjectFieldLabel>();
        contentType = CONTENT_TYPE;
    }

/* 
  @Author <Nicola Tassini>
   @name <EP_FE_DictionaryController>
   @CreateDate <27/04/2016>
   @Description <Support controller to generate the dictionary for the FrontEnd application>  
   @Version <1.0>
*/
    public void init() {
        // Set the language to English
        language = LANGUAGE_EN;

        Set<String> customLabelsSet = new Set<String>();
        Set<ObjectFieldLabel> objectFieldLabelsSet = new Set<ObjectFieldLabel>();

        List<EP_FE_Dictionary_Configurations__mdt> allDictionaryConfigurations = null;
        try {
            allDictionaryConfigurations = [SELECT DeveloperName, EP_FE_FieldAPIName__c, EP_FE_ObjectAPIName__c, EP_FE_Type__c 
                FROM EP_FE_Dictionary_Configurations__mdt limit 10000];
        } catch(Exception e) {
            //TODO MANAGE EXCEPTION
            boolean exe =true;
        }

        if(allDictionaryConfigurations != null) {
            // Read all the configuration
            for(EP_FE_Dictionary_Configurations__mdt dictionaryConfiguration : allDictionaryConfigurations) {
                if(LABEL_CUSTOM.equalsIgnoreCase(dictionaryConfiguration.EP_FE_Type__c)) {
                    customLabelsSet.add(dictionaryConfiguration.DeveloperName.toLowerCase());
                } else{ 
                    if(LABEL_OBJECTFIELD.equalsIgnoreCase(dictionaryConfiguration.EP_FE_Type__c) 
                        && dictionaryConfiguration.EP_FE_ObjectAPIName__c != null && dictionaryConfiguration.EP_FE_FieldAPIName__c != null) {
                    objectFieldLabelsSet.add(new ObjectFieldLabel(dictionaryConfiguration.EP_FE_ObjectAPIName__c.toLowerCase(), 
                        dictionaryConfiguration.EP_FE_FieldAPIName__c.toLowerCase()));
                    }
                }
            }

            customLabels = new List<String>(customLabelsSet);
            objectFieldLabels = new List<ObjectFieldLabel>(objectFieldLabelsSet);
        }
    }

    // Inner class to wrap the
/* 
  @Author <Nicola Tassini>
   @name <ObjectFieldLabel >
   @CreateDate <27/04/2016>
   @Description <>  
   @Version <1.0>
*/     
    public with sharing class ObjectFieldLabel {
        public String objectAPIName {get;set;}
        public String fieldAPIName {get;set;}
/* 
  @Author <Nicola Tassini>
   @name <ObjectFieldLabel >
   @CreateDate <27/04/2016>
   @Description <>  
   @Version <1.0>
*/ 
        public ObjectFieldLabel(String objectAPIName, String fieldAPIName) {
            this.objectAPIName = objectAPIName;
            this.fieldAPIName = fieldAPIName;
        }
/* 
  @Author <Nicola Tassini>
   @name <ObjectFieldLabel >
   @CreateDate <27/04/2016>
   @Description <>  
   @Version <1.0>
*/ 
        public Boolean equals(Object obj) {
            if(obj instanceof ObjectFieldLabel) {
                ObjectFieldLabel ofLabel= (ObjectFieldLabel) obj;
                return (objectAPIName.equalsIgnoreCase(ofLabel.objectAPIName) && fieldAPIName.equalsIgnoreCase(ofLabel.fieldAPIName));
            }
            return false;
        }
/* 
  @Author <Nicola Tassini>
   @name <ObjectFieldLabel >
   @CreateDate <27/04/2016>
   @Description <>  
   @Version <1.0>
*/ 
        public Integer hashCode() {
            return objectAPIName.hashCode() + fieldAPIName.hashCode();
        }

    }
}