/*
    @Author          Accenture
    @Name            EP_SendStagingErrorEmailsBatchhelper_UT
    @CreateDate      18/04/2018
    @Description     This is a Unit test class for EP_Send_Staging_Error_Emails_Batchhelper.
    @Version         1.0
    @Reference       NA
*/

@isTest
public class EP_SendStagingErrorEmailsBatchhelper_UT{
    
@testSetup static void testdata(){
         string rcrdType = [select id from recordtype where developername = 'EP_Retail_Order'].id;
         EP_File__c obj = new EP_File__c();
         obj.Name = 'Sample2';
         obj.EP_CheckSum_Key__c = '4455';
         obj.EP_EmailSent__c = false;
         insert obj;
         EP_RO_Import_Staging__c childObj = new EP_RO_Import_Staging__c();
         childObj.recordtypeid = rcrdType;
         childObj.EP_LineNr__c = 'Line1';
         childObj.EP_CompanyCode__c = 'EPA';
         childObj.EP_SenderCode__c = 'testcode';
         childObj.EP_Unit_Price__c = 9000;
         childObj.EP_Product_Description__c = 'desc test';
         childObj.EP_CreatedDt__c = system.today();
         childObj.EP_CreatedBy__c = 'skp';
         childObj.EP_Remark_Reason__c = 'sample reason';
         childObj.EP_Product_Code__c = 'sample productcode';
         childObj.EP_Sell_To_Number__c = '5555';
         childObj.EP_Ordered_quantity__c = 20;
         childObj.EP_File_Name__c = obj.id;
         childObj.EP_Status__c = 'Error';
         insert childObj;
         system.debug('======'+ childObj);
         }
         
         private static testmethod void startcheck(){
         
         EP_Send_Staging_Error_Emails_Batchhelper localObj = new EP_Send_Staging_Error_Emails_Batchhelper();
         Test.startTest();
         string str = EP_Send_Staging_Error_Emails_Batchhelper.start();
         Test.stopTest();
         
         }
         
         private static testmethod void executecheck(){
         
         List <EP_File__c> lstFileRecord = new List <EP_File__c>();
         lstFileRecord = [select Name, EP_CheckSum_Key__c, (select EP_LineNr__c, EP_SupplyLocation__r.EP_Email__c, EP_Remark_Reason__c, EP_Product_Code__c from RO_Import_Stagings__r where EP_SupplyLocation__r.EP_Email__c != Null and EP_Status__c ='Error') from EP_File__c where EP_StagingErrorCount__c > 0 and EP_EmailSent__c = False];
         EP_Send_Staging_Error_Emails_Batchhelper localObj = new EP_Send_Staging_Error_Emails_Batchhelper();
         Test.startTest();
         EP_Send_Staging_Error_Emails_Batchhelper.execute(lstFileRecord);
         Test.stopTest();
         }

}