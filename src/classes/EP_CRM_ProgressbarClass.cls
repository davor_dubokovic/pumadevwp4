/* ================================================
 * @Class Name : EP_CRM_ProgressbarClass
 * @author : Kamendra Singh
 * @Purpose: 
 * @created date: 17/04/2016
 ================================================*/
public with sharing class EP_CRM_ProgressbarClass {

    private final Opportunity mysObject;
    public String oppStageList {get;set;}
    public String oppStageNum {get;set;}
    public static final String RIGHT_SQUARE =  ']';
    public static final String LEFT_SQUARE =  '[\'' ; 
    public static final String BACK_SLASH  =  '\', \'' ; 
    public static final String COMMA  =  ',' ;
    

/* ================================================
 * @Constructor Name : getPicklistValues 
 * @author : Kamendra Singh
 * @Purpose: 
 * @created date: 
 ================================================*/       
    public EP_CRM_ProgressbarClass(ApexPages.StandardController stdController) {
        this.mysObject = (Opportunity)stdController.getRecord();
        oppStageList = LEFT_SQUARE ;
        oppStageNum = LEFT_SQUARE ;
        getPicklistValues();
    }

/* ================================================
 * @Method Name : getPicklistValues 
 * @author : Kamendra Singh
 * @Purpose: 
 * @created date: 
 ================================================*/ 
    public void getPicklistValues()
    {
        //Set Stage Number 
        Integer i = 1;
        String oppStages=Label.EP_CRM_OPPORTUNITY_STAGES;
        List<String> sOppstages=new List<String>();
        if (!String.ISBLANK(oppStages)) {
            for (String s: oppStages.split(COMMA)) {
                s = s.trim();
                sOppstages.add(s);
            }
        }
        
        //Query for All Stages in list that aren't 0% Probable (aka 'Closed Lost')
        for (OpportunityStage os : [select MasterLabel, SortOrder from OpportunityStage 
            where IsActive=true and DefaultProbability!=null and DefaultProbability!=0.0 and MasterLabel in :sOppstages  order by SortOrder])
        {
            oppStageList += os.MasterLabel + BACK_SLASH;
            oppStageNum += i + BACK_SLASH;
            i += 1;
        }

        oppStageList = oppStageList.substring(0, oppStageList.length()-3);
        oppStageList = oppStageList + RIGHT_SQUARE;

        oppStageNum = oppStageNum.substring(0, oppStageNum.length()-3);
        oppStageNum = oppStageNum + RIGHT_SQUARE ;
    }
}