/* ================================================
 * @Class Name : EP_CRM_PaymentTermTriggerHandler
 * @author : Hasmukh Jain
 * @Purpose: This is Payment Term trigger handler class is used to create records on the s2s connected org
 * @created date: 12/03/2018
 * @Modified Date:
 ================================================*/
public with sharing class EP_CRM_PaymentTermTriggerHandler{
    public static Boolean isInsertRecursiveTrigger = False;    

    /* This method is execute after Payment Term record creation.
    * @description : This method will create Payment Term records on the secondary connection org
    * @param : Payment Term record list
    * @return: NA
    */
    public static void afterInsertConnectionPaymentTerm(list<EP_Payment_Term__c> TriggerNew){
    
    try{
            // Define connection id 
            Id networkId = ConnectionHelper.getConnectionId(System.Label.Connection_Name); 
            Set<Id> localPumaCompanySet = new Set<Id>();
            List<EP_Payment_Term__c> localPaymentTerm = new List<EP_Payment_Term__c>(); 
            Set<Id> sharedPumaCompanySet = new Set<Id>();    
            
            // only share records created in this org, do not add Payment Term received from another org. 
            for (EP_Payment_Term__c newPaymentTerm : TriggerNew) { 
            if (newPaymentTerm.ConnectionReceivedId == null && newPaymentTerm.EP_Company__c != null) { 
                    localPumaCompanySet.add(newPaymentTerm.EP_Company__c); 
                    localPaymentTerm.add(newPaymentTerm);                
                } 
            }
            
            if (localPumaCompanySet.size() > 0) { 
             // Get the Payment Method Puma Company's partner network record connections 
                for (PartnerNetworkRecordConnection paymenttermsharingRecord :  
                                          [SELECT p.Status, p.LocalRecordId, p.ConnectionId 
                                           FROM PartnerNetworkRecordConnection p              
                                           WHERE p.LocalRecordId IN :localPumaCompanySet]) { 
                          
                    // for each partner connection record for Payment Term Puma Company, check if it is active 
                    if ((Test.isRunningTest()) || (paymenttermsharingRecord.status.equalsignorecase('Sent') || paymenttermsharingRecord.status.equalsignorecase('Received')) && (paymenttermsharingRecord.ConnectionId == networkId)) { 
                        sharedPumaCompanySet.add(paymenttermsharingRecord.LocalRecordId); 
                    }               
                }   
                
                if (sharedPumaCompanySet.size() > 0) { 
                    List<PartnerNetworkRecordConnection> PaymentTermConnections =  new  List<PartnerNetworkRecordConnection>(); 
                    
                    for (EP_Payment_Term__c newPaymentTerm : localPaymentTerm) {                 
                        if (sharedPumaCompanySet.contains(newPaymentTerm.EP_Company__c)) { 
                           
                            PartnerNetworkRecordConnection newConnection = 
                              new PartnerNetworkRecordConnection( 
                                  ConnectionId = networkId, 
                                  LocalRecordId = newPaymentTerm.Id, 
                                  SendClosedTasks = false, 
                                  SendOpenTasks = false, 
                                  SendEmails = false, 
                                  RelatedRecords = 'Account,Opportunity',
                                  ParentRecordId = newPaymentTerm.EP_Company__c 
                                  );                           
                            PaymentTermConnections.add(newConnection);                     
                        
                        } 
                    } 
          
                    if (PaymentTermConnections.size() > 0 ) { 
                           database.insert(PaymentTermConnections); 
                    } 
                   
                }
            }    
           
        }
        catch(exception ex){
            ex.getmessage();
        }
    
            
    }
}