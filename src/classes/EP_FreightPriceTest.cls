/**
  @Author <Ashok Arora>
   @name <EP_FreightPriceTest>
   @CreateDate <17/11/2015>
   @Description <THIS IS THE TEST CLASS FOR FRIEGHT PRICE VALIDATION RULES>
   @Version <1.0>
  
 */
@isTest
private class EP_FreightPriceTest {
    
    /*
        This method tests EP_VAL001_Min_Vol_OR_Dist_required
        Either Min Distance or Min Volume has to be populated
    */
    static testMethod void MinVolORDistReqdTest() {
        
        Product2 product;
        Integer count;
        List<EP_Freight_Price__c>lFreightPrices = new list<EP_Freight_Price__c>();
        DataBase.SaveResult[] lSaveResult = new list<DataBase.SaveResult>();
        product = EP_TestDataUtility.createTestRecordsForProduct();
        Profile sysAdmin = [Select id from Profile
                            Where Name = 'System Administrator' 
                            limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        Test.startTest();
            System.runAs(adminUser){
                //CREATE FREIGHT WITHOUT SPECIFYING MINIMUM DISTANCE AND VOLUME
                for(count = 0; count < 200; count++){
                    lFreightPrices.add(EP_TestDataUtility.createFreight(null,product.id));
                    lFreightPrices[count].EP_Min_Distance__c = null;
                    lFreightPrices[count].EP_Min_Volume__c  = null;
                    
                }
                lSaveResult = DataBase.insert(lFreightPrices,false);
            }
        
        Test.stopTest();
        //VALIDATION ERROR FOR EITHER MINIMUM DIATANCE OR VOLUME IS REQUIRED
        for(DataBase.SaveResult saveResult : lSaveResult){
            System.assert(!saveResult.isSuccess());
            System.assert(saveResult.getErrors()[0].getMessage().contains('Either Min Distance or Min Volume has to be populated'));
        }
    }
   
    /*
        This method tests EP_VAL002_Max_Vol_OR_Dist_required
        Either Maximum Distance or Maximum Distance has to be populated
    */
    static testMethod void MaxVolORDistReqdTest() {
        
        Product2 product;
        Integer count;
        List<EP_Freight_Price__c>lFreightPrices = new list<EP_Freight_Price__c>();
        DataBase.SaveResult[] lSaveResult = new list<DataBase.SaveResult>();
        product = EP_TestDataUtility.createTestRecordsForProduct();
        Profile sysAdmin = [Select id from Profile
                            Where Name = 'System Administrator' 
                            limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        Test.startTest();
            System.runAs(adminUser){
                //CREATE FREIGHT PRICE WITH MAX DISTANCE AND MAX VOLUME NOT REQUIRED
                for(count = 0; count < 200; count++){
                    lFreightPrices.add(EP_TestDataUtility.createFreight(null,product.id));
                    lFreightPrices[count].EP_Max_Distance__c = null;
                    lFreightPrices[count].EP_Max_Volume__c  = null;
                    
                }
                lSaveResult = DataBase.insert(lFreightPrices,false);
            }
        
        Test.stopTest();
        //VALIDATION ERROR FOR EITHER MAXIMUM DISTANCE OR VOLUME IS REQUIRED
        for(DataBase.SaveResult saveResult : lSaveResult){
            System.assert(!saveResult.isSuccess());
            System.assert(saveResult.getErrors()[0].getMessage().contains('Either Maximum Distance or Maximum Volume has to be populated'));
        }
    }
    
    /*
        This method tests EP_VAL003_Max_Dist_Less_Than_Min_Dist
        Max Distance should not be less than the Min Distance
    */
    static testMethod void MaxDistLessThanMinDistTest() {
        // TO DO: implement unit test
        Product2 product;
        Integer count;
        List<EP_Freight_Price__c>lFreightPrices = new list<EP_Freight_Price__c>();
        DataBase.SaveResult[] lSaveResult = new list<DataBase.SaveResult>();
        product = EP_TestDataUtility.createTestRecordsForProduct();
        Profile sysAdmin = [Select id from Profile
                            Where Name = 'System Administrator' 
                            limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        Test.startTest();
            System.runAs(adminUser){
                //SET MIN DISTANCE GREATER THAN MAX DISTANCE
                for(count = 0; count < 200; count++){
                    lFreightPrices.add(EP_TestDataUtility.createFreight(null,product.id));
                    lFreightPrices[count].EP_Max_Distance__c = 5;
                    lFreightPrices[count].EP_Min_Distance__c  = 10;
                    
                }
                lSaveResult = DataBase.insert(lFreightPrices,false);
            }
        
        Test.stopTest();
        //VALIDATION ERROR FOR MAX DISTANCE SHOULD BE GREATER THAN MIN DISATNCE
        for(DataBase.SaveResult saveResult : lSaveResult){
            System.assert(!saveResult.isSuccess());
            System.assert(saveResult.getErrors()[0].getMessage().contains('Max Distance should not be less than the Min Distance'));
        }
    }
    
    /*
        This method tests EP_VAL004_Max_Vol_Less_Than_Min_Vol
        Max Volume should not be less than the Min Volume
    */
    static testMethod void MaxVolLessThanMinVolTest() {
        // TO DO: implement unit test
        Product2 product;
        Integer count;
        List<EP_Freight_Price__c>lFreightPrices = new list<EP_Freight_Price__c>();
        DataBase.SaveResult[] lSaveResult = new list<DataBase.SaveResult>();
        product = EP_TestDataUtility.createTestRecordsForProduct();
        Profile sysAdmin = [Select id from Profile
                            Where Name = 'System Administrator' 
                            limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        Test.startTest();
            System.runAs(adminUser){
                //SET MIN VOLUME GREATER THAN MAX VOLUME
                for(count = 0; count < 200; count++){
                    lFreightPrices.add(EP_TestDataUtility.createFreight(null,product.id));
                    lFreightPrices[count].EP_Max_Volume__c  = 5;
                    lFreightPrices[count].EP_Min_Volume__c  = 10;
                    
                }
                lSaveResult = DataBase.insert(lFreightPrices,false);
            }
        
        Test.stopTest();
        //VALIDATION ERROR FOR MAX VOLUME SHOULD BE GREATER THAN MIN VOLUME
        for(DataBase.SaveResult saveResult : lSaveResult){
            System.assert(!saveResult.isSuccess());
            System.assert(saveResult.getErrors()[0].getMessage().contains('Max Volume should not be less than the Min Volume'));
        }
    }
    
    /*
        This method tests EP_VAL005_Distance_UOM_Required
        Distance UOM required if Min Distance or Max distance is populated
    */
    static testMethod void UOMReqdTest() {
        // TO DO: implement unit test
        Product2 product;
        Integer count;
        List<EP_Freight_Price__c>lFreightPrices = new list<EP_Freight_Price__c>();
        DataBase.SaveResult[] lSaveResult = new list<DataBase.SaveResult>();
        product = EP_TestDataUtility.createTestRecordsForProduct();
        Profile sysAdmin = [Select id from Profile
                            Where Name = 'System Administrator' 
                            limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        Test.startTest();
            System.runAs(adminUser){
                //SPECIFY MIN AND MAX DISTANCE AND SET DISTANCE UOM AS BLANK
                for(count = 0; count < 200; count++){
                    lFreightPrices.add(EP_TestDataUtility.createFreight(null,product.id));
                    lFreightPrices[count].EP_Distance_UOM__c = '';
                    
                }
                lSaveResult = DataBase.insert(lFreightPrices,false);
            }
        Test.stopTest();
        //VALIDATION ERROR FOR DISTANCE UOM IS REQUIRED WHEN MIN OR MAX DISTNACE IS SPECIFIED
        for(DataBase.SaveResult saveResult : lSaveResult){
            System.assert(!saveResult.isSuccess());
            System.assert(saveResult.getErrors()[0].getMessage().contains('Distance UOM required if Min Distance or Max distance is populated'));
        }
    }
    /*
        This method tests EP_VAL006_Volume_UOM_Required
        Volume UOM is required if Min or Max volume is specified
    */
    static testMethod void VolumeUOMReqdTest() {
        // TO DO: implement unit test
        Product2 product;
        Integer count;
        List<EP_Freight_Price__c>lFreightPrices = new list<EP_Freight_Price__c>();
        DataBase.SaveResult[] lSaveResult = new list<DataBase.SaveResult>();
        product = EP_TestDataUtility.createTestRecordsForProduct();
        Profile sysAdmin = [Select id from Profile
                            Where Name = 'System Administrator' 
                            limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        Test.startTest();
            System.runAs(adminUser){
                //SPECIFY MIN AND MAX DIATNACE AND SET VOLUME UOM AS BLANK
                for(count = 0; count < 200; count++){
                    lFreightPrices.add(EP_TestDataUtility.createFreight(null,product.id));
                    lFreightPrices[count].EP_Volume_UOM__c = '';
                    
                }
                lSaveResult = DataBase.insert(lFreightPrices,false);
            }
        
        Test.stopTest();
        //VALIDATION ERROR FOR Volume UOM is required if Min or Max volume is specified
        for(DataBase.SaveResult saveResult : lSaveResult){
            System.assert(!saveResult.isSuccess());
            System.assert(saveResult.getErrors()[0].getMessage().contains('Volume UOM is required if Min or Max volume is specified'));
        }
    }
}