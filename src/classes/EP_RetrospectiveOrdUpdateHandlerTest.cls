/*
   @Author          CS
   @name            EP_RetrospectiveOrdUpdateHandlerTest
   @CreateDate      26/04/2018
   @Description     Retrospective Order Update Handler Test
   @Version         1.0
*/
@isTest
public class EP_RetrospectiveOrdUpdateHandlerTest {

	// TMS Order XML data
	public static string jsonData {

		get {

			return '{' +
			'  "MSG": {' +
			'    "HeaderCommon": {' +
			'      "MsgID": "3_a",' +
			'      "InterfaceType": "InterfaceType",' +
			'      "SourceGroupCompany": "SourceGroupCompany",' +
			'      "DestinationGroupCompany": "DestinationGroupCompany",' +
			'      "SourceCompany": "SourceCompany",' +
			'      "DestinationCompany": "DestinationCompany",' +
			'      "CorrelationID": "CorrelationID",' +
			'      "DestinationAddress": "",' +
			'      "SourceResponseAddress": "https://api.eu.coolfurnace.net/WP4-DEV/IntegrationService/api/response/v2/ipaas/ack",' +
			'      "SourceUpdateStatusAddress": "https://api.eu.coolfurnace.net/WP4-DEV/IntegrationService/api/response/v2/ipaas/ack",' +
			'      "DestinationUpdateStatusAddress": "",' +
			'      "MiddlewareUrlForPush": "MiddlewareUrlForPush",' +
			'      "EmailNotification": "EmailNotification",' +
			'      "ErrorCode": "ErrorCode",' +
			'      "ErrorDescription": "ErrorDescription",' +
			'      "ProcessingErrorDescription": "ProcessingErrorDescription",' +
			'      "ContinueOnError": "true",' +
			'      "ComprehensiveLogging": "true",' +
			'      "TransportStatus": "TransportStatus",' +
			'      "ProcessStatus": "ProcessStatus",' +
			'      "UpdateSourceOnReceive": "true",' +
			'      "UpdateSourceOnDelivery": "true",' +
			'      "UpdateSourceAfterProcessing": "true",' +
			'      "UpdateDestinationOnDelivery": "true",' +
			'      "CallDestinationForProcessing": "true",' +
			'      "ObjectType": "ObjectType",' +
			'      "ObjectName": "ObjectName",' +
			'      "CommunicationType": "CommunicationType"' +
			'    },' +
			'    "Payload": {' +
			'      "any0": {' +
			'        "salesContracts": {' +
			'          "salesContract": [' +
			'            {' +
			'              "seqId": "rhdsd13213232lkdls3232",' +
			'              "identifier": {' +
			'                "companyCode": "EPA",' +
			'                "contractNr": "567898765",' +
			'                "entrprsId": "122172827492749"' +
			'              },' +
			'              "versionNr": "20180315142010.123",' +
			'              "lines": {' +
			'                "line": [' +
			'                  {' +
			'                    "seqId": "rhdsd13213232lkdls3232",' +
			'                    "identifier": {' +
			'                      "lineId": "1"' +
			'                    },' +
			'                    "itemId": "3134",' +
			'                    "consumedQty": "2000"' +
			'                  },' +
			'                  {' +
			'                    "seqId": "rhdsd13213232lkdls3233",' +
			'                    "identifier": {' +
			'                      "lineId": "2"' +
			'                    },' +
			'                    "itemId": "3256",' +
			'                    "consumedQty": "250"' +
			'                  }' +
			'                ]' +
			'              }' +
			'            }' +
			'          ]' +
			'        }' +
			'      }' +
			'    },' +
			'    "StatusPayload": "StatusPayload"' +
			'  }' +
			'}';
		}
		
		set {
            jsonData = value;
        }
	}

	@testsetup
	private static void setupTestData() {

		EP_CS_InboundMessageSetting__c msgSetting = new EP_CS_InboundMessageSetting__c();

		msgSetting.Name = 'NAV_TO_SFDC_CONTRACT_QUANTITY_UPDATE';
		msgSetting.Source__c = 'NAV';
		msgSetting.Location__c = 'GBL';
		msgSetting.ProcessName__c = 'SCN';

		insert msgSetting;
		
		EP_RetrospectiveOrdUpdateHandlerTest.insertCustomSetting();
		
        //Get the Record Type ID of the Order 'Contract':
		String contractId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Contract').getRecordTypeId();

		String COUNTRY_NAME = 'Australia';
		String COUNTRY_CODE = 'AU';
		String COUNTRY_REGION = 'Australia';
		String REGION_NAME = 'North-Australia';

		EP_Country__c country = EP_TestDataUtility.createCountryRecord(COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
		insert country;

		EP_Region__c region = EP_TestDataUtility.createCountryRegion(REGION_NAME, country.Id);
		insert region;

		Company__c comp = EP_TestDataUtility.createCompany('EPUMA');
		insert comp;

		Account storageLoc = EP_TestDataUtility.createStorageLocAccount(country.Id, null);
		Account sellToAccount = EP_TestDataUtility.createSellToAccountWithPickupContry(NULL, NULL, country.Id, region.Id);

		sellToAccount.EP_Puma_Company__c = comp.id;
		sellToAccount.EP_Requested_Payment_Terms__c = 'PrePayment';
		sellToAccount.EP_Requested_Packaged_Payment_Term__c = 'PrePayment';
		sellToAccount.EP_Alternative_Payment_Method__c  = 'Cash Payment';

		insert storageLoc;
		insert sellToAccount;

		EP_Payment_Term__c paymentTerm =  EP_TestDataUtility.createPaymentTerm();
		insert paymentTerm;

		Contact  contactRec = EP_TestDataUtility.createTestRecordsForContact(sellToAccount);
		Contract contractRec = EP_TestDataUtility.createContract(storageLoc.Id, comp.Id, paymentTerm.Id, storageLoc);

		// contractRec.DrowDownQuantity__c = 2000;
		contractRec.EP_Contract_Number__c = '567898765';

		upsert contactRec;
		upsert contractRec;
		
		EP_RetrospectiveOrdUpdateHandlerTest.insertOrder();
	}
	
	/*
	@Author      CloudSense
	@name        insertCustomSetting
	@CreateDate  27/07/2017
	@Description Auxiliary method to insert the Custom Setting (CS ORDER SETTINGS) used to store the Id of the Orchestration Process Template:
	@Version     1.0
	*/
	private static void insertCustomSetting(){
		//Call the method to Insert a record of Orchestration Process Template (which will be used to Close Orders):
		EP_RetrospectiveOrdUpdateHandlerTest.insertOrchestrationProcessTemplate();

		//Create Custom Setting with the Id of the previously inserted Orchestration Process Template:
		CS_ORDER_SETTINGS__c customSetting = new CS_ORDER_SETTINGS__c();
		customSetting.Contract_Close_Orchestrator_Process_Id__c = [SELECT Id FROM CSPOFA__Orchestration_Process_Template__c].Id;

		INSERT customSetting;
		System.assertEquals(1, [SELECT COUNT() FROM CS_ORDER_SETTINGS__c], '1 Custom Setting should have been inserted');
	}

	/*
	@Author      CloudSense
	@name        insertCustomSetting
	@CreateDate  27/07/2017
	@Description Auxiliary method to insert a record of Orchestration Process Template (which will be used to Close Orders):
	@Version     1.0
	*/
	private static void insertOrchestrationProcessTemplate(){
		//Create a new record of Orchestration Process Template:
		CSPOFA__Orchestration_Process_Template__c template = new CSPOFA__Orchestration_Process_Template__c();
		template.Name = 'CSC Close Contract Template';

		INSERT template;
		System.assertEquals(1, [SELECT COUNT() FROM CSPOFA__Orchestration_Process_Template__c],'1 Orchestration Process Template should have been inserted');
	}
	
	/*
	@Author      CloudSense
	@name        insertOrders
	@CreateDate  27/07/2017
	@Description Auxiliary method to insert records of data to use in the test methods:
	@Version     1.0
	*/
	private static void insertOrder() {

		String contractId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Contract').getRecordTypeId();
		 
		Date contractStartDate = Date.newInstance(2018, 4, 25);
		Date contractEndDate = Date.Today().addDays(-1);

		//3) Populate the List of records of Order to be inserted:
		csord__Order__c order = new csord__Order__c();
		order.RecordTypeId = contractId;
		order.csord__Identification__c = 'spokeServiceAmsterdam';
		order.csord__Status2__c = 'Accepted';
		order.Contract_Start_Date__c = contractStartDate;
		order.Contract_End_Date__c = contractEndDate;
		order.EP_Total_Order_Quantity__c = '2000';
		order.csord__Primary_Order__c = null;
		insert  order;
		
		System.assertEquals(1, [SELECT COUNT() FROM csord__Order__c WHERE RecordTypeId =: contractId], ' 1 record of Order (Record Type: \'Contract\') should have been inserted');
	}

	@IsTest
	public static void processRequest() {

		csord__Order__c before = [SELECT Id, Name, OrderNumber__c, EP_Total_Order_Quantity__c FROM csord__Order__c LIMIT 1];

		EP_RetrospectiveOrdUpdateHandler obj = new EP_RetrospectiveOrdUpdateHandler();

		Test.startTest();
		
		csord__Order__c order = [SELECT OrderNumber__c FROM csord__Order__c LIMIT 1];

		obj.processRequest(EP_RetrospectiveOrdUpdateHandlerTest.jsonData.replace('567898765', order.OrderNumber__c));

		Test.stopTest();

		csord__Order__c after = [SELECT Id, Name, OrderNumber__c, EP_Total_Order_Quantity__c FROM csord__Order__c LIMIT 1];

		System.assertEquals(2000, Decimal.valueOf(before.EP_Total_Order_Quantity__c), 'Invalid Contract Quantity');
		System.assertEquals(2250, Decimal.valueOf(after.EP_Total_Order_Quantity__c), 'Invalid Contract Quantity');
	}

	@IsTest
	public static void processRequestError() {

		csord__Order__c before = [SELECT Id, Name, OrderNumber__c, EP_Total_Order_Quantity__c FROM csord__Order__c LIMIT 1];

		EP_RetrospectiveOrdUpdateHandler obj = new EP_RetrospectiveOrdUpdateHandler();

		Test.startTest();

		string errorResponse = obj.processRequest('Invalid response');

		Test.stopTest();

		csord__Order__c after = [SELECT Id, Name, OrderNumber__c, EP_Total_Order_Quantity__c FROM csord__Order__c LIMIT 1];

		System.assertEquals(2000, Decimal.valueOf(before.EP_Total_Order_Quantity__c), 'Invalid Contract Quantity');
		System.assertEquals(2000, Decimal.valueOf(after.EP_Total_Order_Quantity__c), 'Invalid Contract Quantity');
		System.assertNotEquals(null, errorResponse, 'Invalid error message');
	}
}