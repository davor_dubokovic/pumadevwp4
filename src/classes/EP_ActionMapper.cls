/**
   @Author          CR Team
   @name            EP_ActionMapper
   @CreateDate      12/20/2016
   @Description     This class contains all SOQLs related to EP_Action__c Object
   @Version         1.0
   @reference       NA
   */
   /*Novasuite Fix documentation changes done*/
/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
    */
    public with sharing class EP_ActionMapper {
    /*
    *  @Author <Accenture>
    *  @Name constructor of class EP_ActionMapper
    */
    public EP_ActionMapper() {

    }
    //Bulkification Implementation -- Start
    public Map<Id,Integer> accountIdCountMap = new Map<Id,Integer>();
    Map<Id,Integer> accountIdCountMapTank = new Map<Id,Integer>();
    /*
    *  @Author <Accenture>
    *  @Name constructor of class EP_ActionMapper
    *  @param Set<Id>
    */
    public EP_ActionMapper(Set<Id> accountIds) {
        accountIdCountMap = getCountOfIncompleteReviewActionsonShipToBulk(accountIds);
        accountIdCountMapTank = getCountOfIncompleteReviewActionsonTanksBulk(accountIds);
    }
    //Bulkification Implementation -- End
    /**
    *  @name            getActionsByActionNameAndParentId
    *  @param           Id, Set<String>
    *  @return          List<EP_Action__c>
    *  @throws          NA
    */
    public List<EP_Action__c> getActionsByActionNameAndParentId(Id accountId, Set<String> actionNames){
        return [Select Id, Name, EP_Account__c, EP_Status__c from EP_Action__c where EP_Account__c =:accountId and EP_Action_Name__c in : actionNames];
    } 
    
    
    
    /**This method returns the count of open action In sell-To and it's child hierarchy
    *  @name            getOpenActionCountInSellToHierarchy
    *  @param           Id
    *  @return          Integer
    *  @throws          NA
    */
    public static Integer getOpenActionCountInSellToHierarchy(Id SellToId){
        EP_GeneralUtility.Log('Public','EP_ActionMapper','getOpenActionCountInSellToHierarchy');
        Set<String> actionStatus =  new set<String>{EP_Common_Constant.ACT_NEW_STATUS, 
            EP_Common_Constant.ACT_ASSIGNED_STATUS};
            Integer openActionCount = [Select count() 
            from EP_Action__c 
            where EP_Status__c in: actionStatus 
            AND (EP_Account__c =: SellToId 
            OR (EP_Account__r.ParentID =: SellToId
            AND EP_Account__r.EP_Status__c =: EP_Common_Constant.STATUS_BASIC_DATA_SETUP)
            OR (EP_Tank__r.EP_Ship_To__r.ParentID =: SellToId 
            AND EP_Tank__r.EP_Ship_To__r.EP_Status__c =: EP_Common_Constant.STATUS_BASIC_DATA_SETUP))];
            return openActionCount;
            
        }
        
   /**This method is used to get count of Basic Data Setup Action of Sell To Parents Accounts
    *  @name            getCountOfActionsforSellTo
    *  @param           set<id>, set<string>, Integer
    *  @return          list<AggregateResult>
    *  @throws          NA
    */
    public list<AggregateResult> getCountOfActionsforSellTo(set<id> idSet,set<string> stringSet) {
        EP_GeneralUtility.Log('Public','EP_ActionMapper','getCountOfActionsforSellTo');
        list<AggregateResult> aggResObjList = new list<AggregateResult>();
        for(list<AggregateResult> aggResList: [SELECT EP_Account__r.ParentId parentID,
            count (Id) noOfReviews 
            FROM EP_Action__c 
            WHERE EP_Account__r.ParentId IN :idSet 
            AND EP_Status__c IN :stringSet
            AND EP_Account__r.EP_Status__c = :EP_Common_Constant.STATUS_BASIC_DATA_SETUP
            GROUP BY EP_Account__r.ParentId
            ]){
            aggResObjList.addAll(aggResList);                               
        }                                
        return aggResObjList ;
    }
    /** 
    *  @name            getKYCReview
    *  @param           id
    *  @return          List<EP_Action__c>
    *  @throws          NA
    */
    public static List<EP_Action__c> getKYCReview(Id accountId){
        EP_GeneralUtility.Log('Public','EP_ActionMapper','getKYCReview');
        List<EP_Action__c> actionList = new list<EP_Action__c>();
        for(list<EP_Action__c> aList : [SELECT Id,EP_Status__c,EP_Account__c FROM  EP_Action__c 
            Where EP_Account__c =:accountId AND RecordType.Name =: EP_Common_Constant.ACT_KYC_REVIEW_RT_DEV ]){
            
            actionList.addAll(aList);                               
        }
        return actionList;
    }
    /** 
    *  @name            getKYCRejectedReviewCount
    *  @param           id
    *  @return          integer
    *  @throws          NA
    */
    public Integer getKYCRejectedReviewCount(Id accountId){
        EP_GeneralUtility.Log('Public','EP_ActionMapper','getKYCRejectedReviewCount');
        return [SELECT count() FROM EP_Action__c 
        Where EP_Account__c =:accountId AND RecordType.DeveloperName =: EP_Common_Constant.ACT_KYC_REVIEW_RT_DEV 
        AND EP_Status__c = :EP_Common_Constant.ACT_REJECTED_STATUS];
    }
    /** 
    *  @name            getCountOfCompletedReviewsOnShipTo
    *  @param           Account
    *  @return          integer
    *  @throws          NA
    */
    public integer getCountOfCompletedReviewsOnShipTo(Account account){
        EP_GeneralUtility.Log('Public','EP_ActionMapper','getCountOfCompletedReviewsOnShipTo');
        Set<String> actionStatus =  new set<String>{EP_Common_Constant.ACT_NEW_STATUS,EP_Common_Constant.ACT_ASSIGNED_STATUS};
        return [Select count() from EP_Action__c  where EP_Status__c in: actionStatus 
        AND (EP_Account__c =: account.Id  OR EP_Tank__r.EP_Ship_To__c =: account.id)];
       
    }
    /** 
    *  @name            getCountOfCSCReviewCompletedOnShipTo
    *  @param           Account
    *  @return          integer
    *  @throws          NA
    */
    public integer getCountOfCSCReviewCompletedOnShipTo(Account account){
        EP_GeneralUtility.Log('Public','EP_ActionMapper','getCountOfCSCReviewCompletedOnShipTo');
        Set<String> actionStatus =  new set<String>{EP_Common_Constant.ACT_NEW_STATUS,EP_Common_Constant.ACT_ASSIGNED_STATUS};
        return [Select count() from EP_Action__c  where
        Recordtype.developerName =:EP_Common_Constant.ACT_CSC_REVIEW_RT_DEV
        AND EP_Status__c in: actionStatus 
        AND EP_Account__c =: account.Id];
    }
    /** 
    *  @name            getCountOfCSCReviewCompletedOnTanks
    *  @param           Account
    *  @return          integer
    *  @throws          NA
    */
    public integer getCountOfCSCReviewCompletedOnTanks(Account account){
        EP_GeneralUtility.Log('Public','EP_ActionMapper','getCountOfCSCReviewCompletedOnTanks');
        Set<String> actionStatus =  new set<String>{EP_Common_Constant.ACT_NEW_STATUS,EP_Common_Constant.ACT_ASSIGNED_STATUS};
        return [ select count() FROM EP_Action__c
        WHERE recordtype.developerName =:EP_Common_Constant.ACT_TANK_CSC_REVIEW_RT_DEV
        AND EP_Status__c IN :actionStatus
        AND EP_Tank__r.EP_Ship_To__c =: account.Id
        AND EP_Tank__r.EP_Tank_Status__c =: EP_Common_Constant.DEFAULT_NON_VMI_ORDER_STATUS];    
    }
    
    //Bulkification Implementation -- Start
    /** 
    *  @name            getCountOfIncompleteReviewActionsonShipTo
    *  @param           Account
    *  @return          integer
    *  @throws          NA
    */
    public integer getCountOfIncompleteReviewActionsonShipTo(Account account){
        EP_GeneralUtility.Log('Public','EP_ActionMapper','getCountOfIncompleteReviewActionsonShipTo');
        Set<String> actionStatus =  new set<String>{EP_Common_Constant.ACT_NEW_STATUS,EP_Common_Constant.ACT_ASSIGNED_STATUS};
        if (accountIdCountMap.containsKey(account.Id)) {

            return accountIdCountMap.get(account.Id);
        }

        return [Select count() from EP_Action__c where EP_Status__c in: actionStatus AND (EP_Account__c =: account.Id)];
    }
    /*L4_45352 - No reviews on tank now
    public integer getCountOfIncompleteReviewActionsonTanks(Account account){
        EP_GeneralUtility.Log('Public','EP_ActionMapper','getCountOfIncompleteReviewActionsonTanks');
        Set<String> actionStatus =  new set<String>{EP_Common_Constant.ACT_NEW_STATUS,EP_Common_Constant.ACT_ASSIGNED_STATUS};
        if (accountIdCountMapTank.containsKey(account.Id)) {

            return accountIdCountMapTank.get(account.Id);
        }


        return [Select count() from EP_Action__c where EP_Status__c in: actionStatus AND (EP_Tank__r.EP_Ship_To__c =: account.Id)];
    }
	L4_45352*/

    public map<Id,Integer> getCountOfIncompleteReviewActionsonShipToBulk(Set<Id> accountIds){
        EP_GeneralUtility.Log('Public','EP_ActionMapper','getCountOfIncompleteReviewActionsonShipToBulk');
        Set<String> actionStatus =  new set<String>{EP_Common_Constant.ACT_NEW_STATUS,EP_Common_Constant.ACT_ASSIGNED_STATUS};
        map<Id,Integer> mapAccountIdCount = new Map<Id,Integer>();
        for (AggregateResult groupedResult :[SELECT EP_Account__c,count(Id) FROM EP_Action__c WHERE EP_Status__c In: actionStatus AND EP_Account__c =: accountIds GROUP BY EP_Account__c]){
            mapAccountIdCount.put(String.valueOf(groupedResult.get('EP_Account__c')),Integer.valueOf(groupedResult.get('expr0')));
        }
         return mapAccountIdCount;
    }
    /** 
    *  @name            getCountOfIncompleteReviewActionsonTanksBulk
    *  @param           Set<Id>
    *  @return          map<Id,Integer>
    *  @throws          NA
    */
    public map<Id,Integer> getCountOfIncompleteReviewActionsonTanksBulk(Set<Id> accountIds){
        EP_GeneralUtility.Log('Public','EP_ActionMapper','getCountOfIncompleteReviewActionsonTanksBulk');
        Set<String> actionStatus =  new set<String>{EP_Common_Constant.ACT_NEW_STATUS,EP_Common_Constant.ACT_ASSIGNED_STATUS};
        map<Id,Integer> mapAccountIdCountTanks = new Map<Id,Integer>();

        for (AggregateResult groupedResult :[SELECT EP_Tank__r.EP_Ship_To__c,count(Id) FROM EP_Action__c WHERE EP_Status__c IN: actionStatus AND EP_Tank__r.EP_Ship_To__c =: accountIds GROUP BY EP_Tank__r.EP_Ship_To__c]){
            mapAccountIdCountTanks.put(String.valueOf(groupedResult.get('EP_Tank__r.EP_Ship_To__c')),Integer.valueOf(groupedResult.get('expr0')));
        }
         return mapAccountIdCountTanks;
    }
    //Bulkification Implementation -- End
    /**This method is used to get Actions that are New and associated to pricebook 
    *  @name            getActionsAssociatedToPriceBook
    *  @param           Id PriceBookId
    *  @return          list<EP_Action__C>
    *  @throws          NA
    */
    public static List<EP_Action__c> getActionsAssociatedToPriceBook(Id PriceBookId){
    /***L4-45352 start****/
        EP_GeneralUtility.Log('Public','EP_ActionMapper','getActionsAssociatedToPriceBook');
        List<EP_Action__c> ListActions = new List<EP_Action__c>();
        for (EP_Action__c NewActions : [select id,EP_Status__c,EP_Product_List__c from EP_Action__c where EP_Product_List__c =:PriceBookId and EP_Status__c =: EP_Common_Constant.ACT_NEW_STATUS]){
           ListActions.add(NewActions); 
        }
         return ListActions;
    }
    /**This method is used to get Actions associated to Accounts
    *  @name            getActionRecordsByAccountID
    *  @param           set<id> accountIds
    *  @return          list<EP_Action__C>
    *  @throws          NA
    */
        public list<EP_Action__C> getActionRecordsByAccountID(set<id> accountIds){
            List<EP_Action__c> actionlist = new list <EP_Action__c>();
            actionList = [SELECT id, EP_Account__c,EP_Status__c FROM EP_Action__c WHERE EP_Account__c IN: accountIds];
            return actionlist;
        }
        
        /****************************************************************
* @author       Accenture                                       *
* @name         getActionsforUserCompanyAssociation             *
* @description  method to execute logic for 
Before Insert of credit holding                 *
* @param        actionrecorType and Companyid                                              *
* @return       list of actionids                                              *
****************************************************************/ 
        Public list<Ep_Action__c> getActionsforUserCompanyAssociation(string actionRecordType, id companyId){
            EP_GeneralUtility.Log('Public','EP_CompanyMapper','getActionsforUserCompanyAssociation');
            return [select id from EP_Action__c where EP_Puma_Company__c = :companyId and Action_Type__c =:actionRecordType];
            
        }  
         /**This method is used to get Actions associated to Accounts
    	*  @name            getKycDetailsForSellTo
    	*  @param           <id> acountId
    	*  @return          <EP_Action__C>
    	*  @throws          List<EP_Action__c>
    	*/
        public List<EP_Action__c> getKycDetailsForSellTo(Id sellToId){
            EP_GeneralUtility.Log('Public','EP_ActionMapper','getKycDetailsForSellTo');
            List<EP_Action__c> reviewList = new List<EP_Action__c>();
            for(EP_Action__c action : [select id,EP_Type_of_KYC__c,LastModifiedBy.Name,LastModifiedDate,EP_Status__c,EP_Reason__c,EP_Account__c 
                                       /**Bug-71691**/
                                       ,EP_KYC_Status_NAV__c,EP_KYC_Type_NAV__c
                                       from EP_Action__c where EP_Account__c = :sellToId and Action_Type__c =: EP_AccountConstant.KYC_Review]){
                                       /**Bug-71691**/
                  reviewList.add(action);                       
            }
            return reviewList;
        }
        
      /***L4-45352 end****/
        /**This method is used to get Actions associated to Accounts
        *  @name            getConsignmentReview
        *  @param           Id
        *  @return          List<EP_Action__c>
        */
        public static List<EP_Action__c> getConsignmentReview(Id shipToId){
             EP_GeneralUtility.Log('Public','EP_ActionMapper','getConsignmentReview');
        	 List<EP_Action__c> consignmentReview = new List<EP_Action__c>();
             consignmentReview = [select id,EP_Status__c,recordType.developerName 
                     from EP_Action__c 
                     where recordType.developerName = 'EP_Consignment_Location_Review' AND EP_Account__c =: shipToId 
                     Limit 1];
            return consignmentReview;
        }
}