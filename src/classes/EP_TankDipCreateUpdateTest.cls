/**
 * @author <Kamal Garg>
 * @name <EP_TankDipCreateUpdateTest>
 * @createDate <15/12/2015>
 * @description <Test class for testing Tank Dip staus based on Account & Tank status> 
 * @version <1.0>
 */
@isTest
private class EP_TankDipCreateUpdateTest{
    //static String recType= Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get('PlaceHolder').getRecordTypeId();
    /**
     * @author Kamal Garg
     * @date 15/12/2015
     * @description Test Method to test that Dip entry is optional for a CSC user with unblocked customer account and shipto and blocked tank_via Submit Tank Dips Link
     */
    private static void updateAccountStatus(){
    
        EP_ActionTriggerHandler.isExecuteAfterUpdate = true;
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = true;
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;
        
        
    } 
    static testMethod void testDipEntryWithUnblockedCustomerAccountUnblockedShipToBlockedTank() {
        String recType = EP_Common_Util.fetchRecordTypeId('EP_Tank_Dip__c','Placeholder');
        Profile cscAgentProfile = [SELECT Id FROM Profile WHERE Name='EP_CSC' Limit 1];
        User cscAgentUser = EP_TestDataUtility.createUser(cscAgentProfile.id);
        Product2 prod = EP_TestDataUtility.createTestRecordsForProduct();
        System.runAs(cscAgentUser){
            // create test record for Account with RecordType='Sell To'
            Account sellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
            insert sellToAccount;
            // create test record for Account with RecordType='VMI Ship To'
            Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.VMI_SHIP_TO);
            Account vmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId);
            insert vmiShipToAccount;
            EP_Tank__c tankObj1= EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount.Id,prod.Id);
            insert tankObj1;
            List<Account> accountsToBeUpdated = new List<Account>();
            updateAccountStatus();
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            accountsToBeUpdated.add(sellToAccount);            
            vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;            
            accountsToBeUpdated.add(vmiShipToAccount);           
            upsert accountsToBeUpdated;
            //Updating the tank record to be 'Basic Data Setup'
            tankObj1.EP_Tank_Status__c= EP_Common_Constant.BASIC_DATA_SETUP;
            update tankObj1;
            
            //Updating the tank record to be 'Operational'
            tankObj1.EP_Tank_Status__c = 'Operational';
            update tankObj1;
            accountsToBeUpdated = new List<Account>();
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            accountsToBeUpdated.add(sellToAccount);
            
            vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            vmiShipToAccount.EP_Tank_Dips_Schedule_Time__c = '01:00';
            vmiShipToAccount.EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
            accountsToBeUpdated.add(vmiShipToAccount);
            
            upsert accountsToBeUpdated;
            // create test records for EP_Tank_Dip__c of RecordType='PlaceHolder' with EP_Reading_Date_Time__c='system.now()-5'
            /*EP_Tank_Dip__c tankDip = EP_TestDataUtility.createTestRecordsForTankDip(tankObj1.Id,system.now()); 
            tankDip.RecordTypeId = recType;
            tankDip.EP_Tank_Dip_Exported__c = False;
            insert tankDip;*/
            
            EP_Tank_Dip__c tDip = new EP_Tank_Dip__c();
            tDip.EP_Tank__c = tankObj1.id;
            tDip.EP_Ambient_Quantity__c = 40;
            tDip.RecordTypeId = recType;
            tDip.EP_Reading_Date_Time__c = System.now()-2;
            tDip.EP_Tank_Dip_Entered_In_Last_14_Days__c = true;
            insert tDip;
            
            tankObj1.EP_Tank_Status__c = EP_Common_Constant.TANK_STOPPED_STATUS;
            tankObj1.EP_Reason_Blocked__c ='Credit Failed';
            update tankObj1;
            
            // getting 'EP_SelectTankDipSitePage_R1' PageReference
            PageReference pageRef = Page.EP_SelectTankDipSitePage_R1;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
            // creating instance of 'EP_SelectTankDipDatePageExtnClass_R1' apex class
            EP_SelectTankDipDatePageExtnClass_R1 selectTankDipDatePageExtnClass = new EP_SelectTankDipDatePageExtnClass_R1();
            // calling 'retrieveMissingSiteDates' api to populate tankdips dates to be displayed
            selectTankDipDatePageExtnClass.retrieveMissingSiteDates();          
            // selecting first date
            selectTankDipDatePageExtnClass.tankDipDates.get(0).dateSelected = true;
            // getting 'EP_TankDipSubmitPage_R1' PageReference 
            pageRef = Page.EP_TankDipSubmitPage_R1;
            Test.setCurrentPage(pageRef);
            // setting parameters to be passed with the page rendering
            ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
            ApexPages.currentPage().getParameters().put('date', selectTankDipDatePageExtnClass.tankDipDates.get(0).dateText);
            // creating instance of 'EP_TankDipSubmitPageControllerClass_R1' apex class
            EP_TankDipSubmitPageControllerClass_R1 tankDippageObj = new EP_TankDipSubmitPageControllerClass_R1();
            // verifying the results. Checkbox should not be checked if dip entry is optional
            system.debug('-------- Access Record Methos ----- ' + tankDippageObj.displayedTankDipLines);
            system.assertEquals(tankDippageObj.displayedTankDipLines.size(), 1);
            system.assertEquals(tankDippageObj.displayedTankDipLines.get(0).blnAddTankDip, false);
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 15/12/2015
     * @description Test Method to test that Dip entry is optional for a CSC user with unblocked customer account and blocked shipto and tank_via Submit Tank Dips Button
     */
    static testMethod void testDipEntryWithUnblockedCustomerAccountBlockedShipToBlockedTank() {
        Profile cscAgentProfile = [SELECT Id FROM Profile WHERE Name='EP_CSC' Limit 1];
        User cscAgentUser = EP_TestDataUtility.createUser(cscAgentProfile.id);
        Product2 prod = EP_TestDataUtility.createTestRecordsForProduct();
        System.runAs(cscAgentUser){
            // create test record for Account with RecordType='Sell To'
            Account sellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
            insert sellToAccount;
            // create two test records for Account with RecordType='VMI Ship To'
            Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.VMI_SHIP_TO);
            List<Account> vmiShipToAccountsToBeInserted = new List<Account>();
            vmiShipToAccountsToBeInserted.add(EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId));
            vmiShipToAccountsToBeInserted.add(EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId));
            insert vmiShipToAccountsToBeInserted;
            // getting vmiShipToAccount to be selected from UI
            Account vmiShipToAccount = vmiShipToAccountsToBeInserted.get(0);
            createTank(vmiShipToAccountsToBeInserted, prod);
            List<Account> accountsToBeUpdated = new List<Account>();
            
            updateAccountStatus();
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            accountsToBeUpdated.add(sellToAccount);            
            for(Account acct : vmiShipToAccountsToBeInserted){
                acct.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                accountsToBeUpdated.add(acct);
            }
            upsert accountsToBeUpdated;
            accountsToBeUpdated = new List<Account>();
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            accountsToBeUpdated.add(sellToAccount);
            
            for(Account acct : vmiShipToAccountsToBeInserted){
                acct.EP_Status__c = EP_Common_Constant.STATUS_BLOCKED;
                acct.EP_Tank_Dips_Schedule_Time__c = '01:00';
                acct.EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
                acct.EP_Reason_Blocked__c = 'Maintenance';
                accountsToBeUpdated.add(acct);
            }
            
            upsert accountsToBeUpdated;
            
            // getting 'EP_SelectTankDipSitePage_R1' PageReference
            PageReference pageRef = Page.EP_SelectTankDipSitePage_R1;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
            // creating instance of 'EP_SelectTankDipDatePageExtnClass_R1' apex class
            EP_SelectTankDipDatePageExtnClass_R1 selectTankDipDatePageExtnClass = new EP_SelectTankDipDatePageExtnClass_R1();
            // calling 'retrieveMissingSiteDates' api
            selectTankDipDatePageExtnClass.retrieveMissingSiteDates();
            // getting 'EP_TankDipSubmitPage_R1' PageReference
            pageRef = Page.EP_TankDipSubmitPage_R1;
            Test.setCurrentPage(pageRef);
            // setting parameters to be passed with the page rendering
            ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
            // creating instance of 'EP_TankDipSubmitPageControllerClass_R1' apex class
            EP_TankDipSubmitPageControllerClass_R1 tankDippageObj = new EP_TankDipSubmitPageControllerClass_R1();
            // verifying the results. Checkbox should not be checked if dip entry is optional
            system.assertEquals(tankDippageObj.displayedTankDipLines.size(), 1);            
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 15/12/2015
     * @description Test Method to test that Dip entry is optional for a CSC user with unblocked shipto and blocked customer account and tank_via Submit Tank Dips Link
     */
    static testMethod void testDipEntryWithBlockedCustomerAccountUnblockedShipToBlockedTank() {
        Profile cscAgentProfile = [SELECT Id FROM Profile WHERE Name='EP_CSC' Limit 1];
        User cscAgentUser = EP_TestDataUtility.createUser(cscAgentProfile.id);
        Product2 prod = EP_TestDataUtility.createTestRecordsForProduct();
        System.runAs(cscAgentUser){
            // create test record for Account with RecordType='Sell To'
            Account sellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
            insert sellToAccount;
            // create two test records for Account with RecordType='VMI Ship To'
            Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.VMI_SHIP_TO);
            List<Account> vmiShipToAccountsToBeInserted = new List<Account>();
            vmiShipToAccountsToBeInserted.add(EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId));
            vmiShipToAccountsToBeInserted.add(EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId));
            insert vmiShipToAccountsToBeInserted;
            // getting vmiShipToAccount to be selected from UI
            Account vmiShipToAccount = vmiShipToAccountsToBeInserted.get(0);
           
            createTank(vmiShipToAccountsToBeInserted, prod);
            List<Account> accountsToBeUpdated = new List<Account>();
            
            updateAccountStatus();
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            accountsToBeUpdated.add(sellToAccount);            
            for(Account acct : vmiShipToAccountsToBeInserted){
                acct.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                accountsToBeUpdated.add(acct);
            }
            upsert accountsToBeUpdated;
            
            accountsToBeUpdated = new List<Account>();          
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BLOCKED;
            sellToAccount.EP_Reason_Blocked__c = 'Maintenance';
            accountsToBeUpdated.add(sellToAccount);
            
            for(Account acct : vmiShipToAccountsToBeInserted){
                acct.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
                acct.EP_Tank_Dips_Schedule_Time__c = '01:00';
                acct.EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
                accountsToBeUpdated.add(acct);
            }
            upsert accountsToBeUpdated;
            // getting 'EP_SelectTankDipSitePage_R1' PageReference
            PageReference pageRef = Page.EP_SelectTankDipSitePage_R1;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
            // creating instance of 'EP_SelectTankDipDatePageExtnClass_R1' apex class
            EP_SelectTankDipDatePageExtnClass_R1 selectTankDipDatePageExtnClass = new EP_SelectTankDipDatePageExtnClass_R1();
            // calling 'retrieveMissingSiteDates' api
            selectTankDipDatePageExtnClass.retrieveMissingSiteDates();
            // getting 'EP_TankDipSubmitPage_R1' PageReference
            pageRef = Page.EP_TankDipSubmitPage_R1;
            Test.setCurrentPage(pageRef);
            // setting parameters to be passed with the page rendering
            ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
            // creating instance of 'EP_TankDipSubmitPageControllerClass_R1' apex class
            EP_TankDipSubmitPageControllerClass_R1 tankDippageObj = new EP_TankDipSubmitPageControllerClass_R1();
            // verifying the results. Checkbox should not be checked if dip entry is optional
            system.assertEquals(tankDippageObj.displayedTankDipLines.size(), 1);            
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 15/12/2015
     * @description Test Method to test that Dip entry is optional for a CSC user with blocked customer account, shipto and Tank_via Submit Tank Dips Button
     */
    static testMethod void testDipEntryWithBlockedCustomerAccountBlockedShipToBlockedTank() {
        Profile cscAgentProfile = [SELECT Id FROM Profile WHERE Name='EP_CSC' Limit 1];
        User cscAgentUser = EP_TestDataUtility.createUser(cscAgentProfile.id);
        Product2 prod = EP_TestDataUtility.createTestRecordsForProduct();
        System.runAs(cscAgentUser){
            // create test record for Account with RecordType='Sell To'
            Account sellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
            insert sellToAccount;
            // create two test records for Account with RecordType='VMI Ship To'
            Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.VMI_SHIP_TO);
            List<Account> vmiShipToAccountsToBeInserted = new List<Account>();
            vmiShipToAccountsToBeInserted.add(EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId));
            vmiShipToAccountsToBeInserted.add(EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId));
            insert vmiShipToAccountsToBeInserted;
            // getting vmiShipToAccount to be selected from UI
            Account vmiShipToAccount = vmiShipToAccountsToBeInserted.get(0);         
            createTank(vmiShipToAccountsToBeInserted, prod);
            List<Account> accountsToBeUpdated = new List<Account>();            
            updateAccountStatus();
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            accountsToBeUpdated.add(sellToAccount);            
            for(Account acct : vmiShipToAccountsToBeInserted){
                acct.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                accountsToBeUpdated.add(acct);
            }
            upsert accountsToBeUpdated;
            accountsToBeUpdated = new List<Account>();
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BLOCKED;
            sellToAccount.EP_Reason_Blocked__c = 'Maintenance';
            accountsToBeUpdated.add(sellToAccount);
            
            for(Account acct : vmiShipToAccountsToBeInserted){
                acct.EP_Status__c = EP_Common_Constant.STATUS_BLOCKED;
                acct.EP_Reason_Blocked__c = 'Maintenance';
                acct.EP_Tank_Dips_Schedule_Time__c = '01:00';
                acct.EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
                accountsToBeUpdated.add(acct);
            }
            upsert accountsToBeUpdated;
            // getting 'EP_SelectTankDipSitePage_R1' PageReference
            PageReference pageRef = Page.EP_SelectTankDipSitePage_R1;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
            // creating instance of 'EP_SelectTankDipDatePageExtnClass_R1' apex class
            EP_SelectTankDipDatePageExtnClass_R1 selectTankDipDatePageExtnClass = new EP_SelectTankDipDatePageExtnClass_R1();
            // calling 'retrieveMissingSiteDates' api
            selectTankDipDatePageExtnClass.retrieveMissingSiteDates();
            // getting 'EP_TankDipSubmitPage_R1' PageReference
            pageRef = Page.EP_TankDipSubmitPage_R1;
            Test.setCurrentPage(pageRef);
            // setting parameters to be passed with the page rendering
            ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
            // creating instance of 'EP_TankDipSubmitPageControllerClass_R1' apex class
            EP_TankDipSubmitPageControllerClass_R1 tankDippageObj = new EP_TankDipSubmitPageControllerClass_R1();
            // verifying the results. Checkbox should not be checked if dip entry is optional
            system.assertEquals(tankDippageObj.displayedTankDipLines.size(), 1);            
            
        }
    }
    Private static void createTank(list<Account> AccList, Product2 Prod)
    {
        List<EP_Tank__c> tanksToBeInsertedList = new List<EP_Tank__c>();
        for(Account acc : AccList){
        EP_Tank__c tankObj1= EP_TestDataUtility.createTestEP_Tank(acc.Id,prod.Id);
        //tankObj1.EP_Tank_Code__c = '9876';
        tanksToBeInsertedList.add(tankObj1);
        }
        insert tanksToBeInsertedList;
        
    }
    
}