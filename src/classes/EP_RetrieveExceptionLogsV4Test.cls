/* 
   @Author Spiros Markantonatos
   @name <EP_RetrieveExceptionLogsV4Test>
   @CreateDate <23/01/2017>
   @Description <This is the test class of the EP_RetrieveExceptionLogsV4 class which is implementing the API returning log records to the ePuma monitoring system>
   @Version <1.0>
 
*/
@isTest
private class EP_RetrieveExceptionLogsV4Test {
	
	// Properties
    private static EP_Exception_log__c testExceptionLogRecord;
    
    // Methods
    /*
    This method is used to generate a test exception log record
    */
    private static void generateTestExceptionLogRecord() {
    	Test.startTest();
	    	EP_TestDataUtility.WrapperCustomerHierarchy testAccountHierarchy = 
	                                                                    EP_TestDataUtility.createCustomerHierarchyForNAV(1);
			Account testSellToAccount = testAccountHierarchy.lCustomerAccounts[0];
			            
			testExceptionLogRecord = new EP_Exception_log__c();
			
			testExceptionLogRecord.EP_Application_Name__c = 'TEST';
			testExceptionLogRecord.EP_Browser__c = 'IE';
			testExceptionLogRecord.EP_Class_Name__c = 'TESTCLASS';
			testExceptionLogRecord.EP_Device__c = 'ANDROID';
			testExceptionLogRecord.EP_WS_Audit_User__c = UserInfo.getUserId();
			testExceptionLogRecord.EP_WS_Calling_Area__c = 'NA';
			testExceptionLogRecord.EP_WS_Transaction_ID__c = testSellToAccount.Id;
			testExceptionLogRecord.EP_Component__c = 'BE';
			testExceptionLogRecord.EP_Exception_Code__c = '1';
			testExceptionLogRecord.EP_Exception_Description__c = 'TEST EXCEPTION';
			testExceptionLogRecord.EP_Exception_Details__c = 'TEST EXCEPTION';
			testExceptionLogRecord.EP_Exception_Log_Timestamp__c = System.Now();
			testExceptionLogRecord.EP_Exception_Type__c = 'TEST';
			testExceptionLogRecord.EP_Handled__c = TRUE;
			testExceptionLogRecord.EP_Is_Exported__c = FALSE;
			testExceptionLogRecord.EP_Method_Name__c = 'TEST METHOD';
			testExceptionLogRecord.EP_OperatingSystem__c = 'ANDROID';
			testExceptionLogRecord.EP_OrgID__c = UserInfo.getOrganizationId();
			testExceptionLogRecord.EP_PerformedAction__c = 'NA';
			testExceptionLogRecord.EP_Severity__c = 'High';
			testExceptionLogRecord.EP_Running_User__c = UserInfo.getUserId();
			testExceptionLogRecord.EP_User_Locale__c = 'en/EN';
			testExceptionLogRecord.EP_User_Profile_ID__c = UserInfo.getProfileId();
			testExceptionLogRecord.EP_User_Role_ID__c = UserInfo.getUserRoleId();
			testExceptionLogRecord.EP_User_Session_ID__c = UserInfo.getUserId();
			testExceptionLogRecord.EP_WasHandled__c = TRUE;
			testExceptionLogRecord.EP_WS_Calling_Application__c = 'TEST';
			
			Database.insert(testExceptionLogRecord);
		Test.stopTest();
    }
	
	/*
    This method is used to callout the API
    */
    private static String simulateRestCallout(List<String> lParamNames, List<String> lParamValues) {
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		
		req.requestURI = '/services/apexrest/v4/ExceptionLogs';
		req.httpMethod = 'GET';
		
		for (Integer i = 0; i < lParamNames.size(); i++)
		{
			req.addParameter(lParamNames[i], lParamValues[i]);
		}
		
		RestContext.request = req;
		RestContext.response = res;
		
		EP_RetrieveExceptionLogsV4.retrieveExceptionLogs();
		
		String strResponse = res.responseBody.toString();
		
		return strResponse;
    }
	
	/*
    This method will test that the class is able to successfully construct the select statement used by the interface
    */
    private static testMethod void testInterfaceSelectStatementGeneration() {
    	
    	// The API class should return the query select statement without an error (method returns BLANK when an error has occured)
    	String strSelectStatement = EP_RetrieveExceptionLogsV4.getObjectFullSelectStatement();
    	System.assertNotEquals(EP_Common_Constant.BLANK, strSelectStatement);
    	
    }
    
    /*
    This method will test the happy path where the system is meant to return a single exception record
    */
    private static testMethod void testSuccessfullInterfaceCallout() {
		
		List<String> lParamNames = new List<String>();
		lParamNames.add(EP_Common_Constant.FROM_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.TO_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.limitException);
		
		Date dToday = System.Today();
		Date dYesterday = System.Today().addDays(-1);
		
		List<String> lParamValues = new List<String>();
		// Param 1 = Yesterday
		lParamValues.add(String.valueOf(dYesterday));
		// Param 2 = Today
		lParamValues.add(String.valueOf(dToday));
		// Param 3 = 100
		lParamValues.add('100');
		
		// Generate the test exception record
		generateTestExceptionLogRecord();
		// Callout the REST API
		String strResponse = simulateRestCallout(lParamNames, lParamValues);
		
		// The response should contain the ID of the test exception record
		System.assertEquals(TRUE, (strResponse.indexOf(testExceptionLogRecord.Id) > -1));
		
    }
	
	/*
    This method will not return any records as the query limit is set to zero
    */
    private static testMethod void testZeroLimitInterfaceCallout() {
		
		List<String> lParamNames = new List<String>();
		lParamNames.add(EP_Common_Constant.FROM_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.TO_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.limitException);
		
		Date dToday = System.Today();
		Date dYesterday = System.Today().addDays(-1);
		
		List<String> lParamValues = new List<String>();
		// Param 1 = Yesterday
		lParamValues.add(String.valueOf(dYesterday));
		// Param 2 = Today
		lParamValues.add(String.valueOf(dToday));
		// Param 3 = 0
		lParamValues.add('0');
		
		// Generate the test exception record
		generateTestExceptionLogRecord();
		// Callout the REST API
		String strResponse = simulateRestCallout(lParamNames, lParamValues);
		// The response should contain NULL records
		System.assertEquals(TRUE, (strResponse.indexOf(EP_Common_Constant.NULL_VAR) > -1));
		
    }
    
    /*
    This method will attempt to return records created in the past. Since there are no records it should not return any results
    */
    private static testMethod void testPastRecordSearchInterfaceCallout() {
		
		List<String> lParamNames = new List<String>();
		lParamNames.add(EP_Common_Constant.FROM_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.TO_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.limitException);
		
		Date dThreeDaysAgo = System.Today().addDays(-3);
		Date dTwoDaysAgo = System.Today().addDays(-2);
		
		List<String> lParamValues = new List<String>();
		// Param 1 = Three Days Ago
		lParamValues.add(String.valueOf(dThreeDaysAgo));
		// Param 2 = Two Days Ago
		lParamValues.add(String.valueOf(dTwoDaysAgo));
		// Param 3 = 10
		lParamValues.add('10');
		
		// Generate the test exception record
		generateTestExceptionLogRecord();
		
		// Callout the REST API
		String strResponse = simulateRestCallout(lParamNames, lParamValues);
		
		// The response should contain NULL records
		System.assertEquals(TRUE, (strResponse.indexOf(EP_Common_Constant.NULL_VAR) > -1));
		
    }
    
    /*
    This method will ensure that the interface returns the appropriate error when receiving an invalid param. This method will simulate an invalid date param
    */
    private static testMethod void testInvalidDateParamErrorInterfaceCallout() {
		
		List<String> lParamNames = new List<String>();
		lParamNames.add(EP_Common_Constant.FROM_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.TO_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.limitException);
		
		List<String> lParamValues = new List<String>();
		// Param 1 = Invalid Date
		lParamValues.add('AA-BB-CCCC');
		// Param 2 = Invalid Date
		lParamValues.add('AA-BB-CCCC');
		// Param 3 = 10
		lParamValues.add('10');
		
		// Generate the test exception record
		generateTestExceptionLogRecord();
		
		// Callout the REST API
		String strResponse = simulateRestCallout(lParamNames, lParamValues);
		
		// The response should contain the invalid filter response
		System.assertEquals(TRUE, (strResponse.indexOf(EP_Common_Constant.GENERIC_FILTER_RESPONSE_FALSE) > -1));
		
    }
    
    /*
    This method will ensure that the interface returns the appropriate error when an unexpected error occurs
    */
    private static testMethod void testUnexpectedErrorInterfaceCallout() {
		
		List<String> lParamNames = new List<String>();
		lParamNames.add(EP_Common_Constant.FROM_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.TO_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.limitException);
		
		Date dToday = System.Today();
		Date dYesterday = System.Today().addDays(-1);
		
		List<String> lParamValues = new List<String>();
		// Param 1 = Yesterday
		lParamValues.add(String.valueOf(dYesterday));
		// Param 2 = Today
		lParamValues.add(String.valueOf(dToday));
		// Param 3 = 100
		lParamValues.add('100');
		
		// Generate the test exception record
		generateTestExceptionLogRecord();
		
		// Generate an unexpected error
		EP_RetrieveExceptionLogsV4.strExceptionLogObjectName = 'EP_Exception_log123__c';
		
		// Callout the REST API
		String strResponse = simulateRestCallout(lParamNames, lParamValues);
		
		// The response should contain the invalid filter response
		System.assertEquals(TRUE, (strResponse.indexOf(EP_Common_Constant.RESPONSE_FALSE) > -1));
		
    }
    
    /*
    This method will attempt to return records without providing the mandatory FROM DATE param. The interface should return a generic error
    */
    private static testMethod void testNoMandatoryParamsSearchInterfaceCallout() {
		
		// Param list is initialised, however no values are added
		List<String> lParamNames = new List<String>();
		List<String> lParamValues = new List<String>();
		
		// Generate the test exception record
		generateTestExceptionLogRecord();
		// Callout the REST API
		String strResponse = simulateRestCallout(lParamNames, lParamValues);
		
		// The response should contain the generic error
		System.assertEquals(TRUE, (strResponse.indexOf(EP_Common_Constant.GENERIC_FILTER_RESPONSE_FALSE) > -1));
		
    }
	
}