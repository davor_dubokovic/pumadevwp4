/**
   * @Author <Ashok Arora>
   * @name <EP_StorageLocationHandler>
   * @CreateDate <03/11/2015>
   * @Description <This class handles requests from Stock Holding Location Trigger>  
   * @Version <1.0>
   */
   public with sharing class EP_StorageLocationTriggerHandler {

    public static Boolean isExecuteBeforeUpdate = false;
    
    private static final String DO_BEFORE_INSERT = 'doBeforeInsert';
    private static final String DO_BEFORE_UPDATE = 'doBeforeUpdate';
    private static final String DO_BEFORE_DELETE = 'doBeforeDelete';
    private static final String DO_BEFORE_INSERT_UPDATE = 'doBeforeInsertUpdate';
    
    public static boolean executeStorageLocationTrigger = true;
    /**
     * @author <Ashok Arora>
     * @date <03/11/2015>
     * @description <This method performs logic which need to pre execute 
    *               on before insert event of supply option record.>
     * @param List<EP_Stock_Holding_Location__c>
     * @return NONE
     */
     public static void doBeforeInsert(List<EP_Stock_Holding_Location__c>listOfNewStorageLocations){
        EP_GeneralUtility.Log('Public','EP_StorageLocationTriggerHandler','doBeforeInsert');
        if(executeStorageLocationTrigger){
	        try{
		        // Common Method to Validate Supply Option
		        EP_StorageLocationTriggerHelper.validateSupplyOption(listOfNewStorageLocations);
		    }
		    catch(Exception e){
		        EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, DO_BEFORE_INSERT, EP_StorageLocationTriggerHandler.class.getName(), ApexPages.severity.ERROR);
		    }
        }
	}
    /**
     * @author <Ashok Arora>
     * @date <03/11/2015>
     * @description <This method performs logic which need to pre execute 
    *               on before update event of supply option record.>
     * @param List<EP_Stock_Holding_Location__c>
     * @return NONE
     */
     public static void doBeforeUpdate(List<EP_Stock_Holding_Location__c>listOfNewStorageLocations
      ,map<id,EP_Stock_Holding_Location__c>mapOfOldStorageLocations
      ,map<id,EP_Stock_Holding_Location__c>mapOfNewStorageLocations
      ){
        EP_GeneralUtility.Log('Public','EP_StorageLocationTriggerHandler','doBeforeUpdate');
        if(executeStorageLocationTrigger){
            isExecuteBeforeUpdate = true;//set true to avoid re-execution
            try{
                // Common Method to Validate Supply Option
                EP_StorageLocationTriggerHelper.validateSupplyOption(listOfNewStorageLocations);
            }
            catch(Exception e){
                EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, DO_BEFORE_UPDATE, EP_StorageLocationTriggerHandler.class.getName(), ApexPages.severity.ERROR);
            }
        }
    }

     /**L4_45352_start**/
     /**
     * @author <Accenture>
     * @description <This method performs logic which need to execute 
     *               on before delete event of supply option record.>
     * @param List<EP_Stock_Holding_Location__c>
     * @return NONE
     */
     public static void doBeforeDelete(List<EP_Stock_Holding_Location__c> listOfStorageLocations)
     {        
        EP_GeneralUtility.Log('Public','EP_StorageLocationTriggerHandler','doBeforeDelete');
        if(executeStorageLocationTrigger){
            try{
                EP_StorageLocationTriggerHelper.isSellToSupplyOptionPrimaryonShipTo(listOfStorageLocations);
            }
            catch(Exception e){
                EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, DO_BEFORE_DELETE, EP_StorageLocationTriggerHandler.class.getName(), ApexPages.severity.ERROR);
            }
        }
    }
    /**L4_45352_end**/    
}