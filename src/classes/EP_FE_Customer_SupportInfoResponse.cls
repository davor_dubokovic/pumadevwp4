/* 
  @Author <Ram Rai>
   @name <EP_FE_Customer_SupportInfoResponse >
   @CreateDate <21/04/2016>
   @Description <This class >  
   @Version <1.0>
*/
global with sharing class EP_FE_Customer_SupportInfoResponse extends EP_FE_Response {
  public EP_PortalOrderHelper.supportAddressClass address;
  public List<EP_PortalOrderHelper.supportNumberClass> numbers;
}