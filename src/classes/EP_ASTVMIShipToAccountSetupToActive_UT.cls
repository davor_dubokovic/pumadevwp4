@isTest
public class EP_ASTVMIShipToAccountSetupToActive_UT
{

    static final string EVENT_NAME = '04-AccountSet-upTo04-AccountSet-up';
    static final string INVALID_EVENT_NAME = '08-ProspectTo04-Account Set-up';
    
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    
    static testMethod void isTransitionPossible_positive_test() {
        Account acc = EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_WINDMS__c = true;
        update acc;
        EP_AccountDomainObject obj = new EP_AccountDomainObject(acc.id);
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTVMIShipToAccountSetupToActive ast = new EP_ASTVMIShipToAccountSetupToActive();
        obj.localAccount.EP_Integration_Status__c = EP_Common_constant.SYNC_STATUS;
        update obj.localaccount;
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isTransitionPossible_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASAccountSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTVMIShipToAccountSetupToActive ast = new EP_ASTVMIShipToAccountSetupToActive();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    
    static testMethod void isRegisteredForEvent_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASAccountSetupDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTVMIShipToAccountSetupToActive ast = new EP_ASTVMIShipToAccountSetupToActive();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isRegisteredForEvent_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASAccountSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTVMIShipToAccountSetupToActive ast = new EP_ASTVMIShipToAccountSetupToActive();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    
    static testMethod void isGuardCondition_positive_test() {
        Account acc = EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_WINDMS__c = true;
        acc.EP_Integration_Status__c = EP_Common_constant.SYNC_STATUS;
        update acc;
        EP_AccountDomainObject domainObj = new EP_AccountDomainObject(acc.id);
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTVMIShipToAccountSetupToActive ast = new EP_ASTVMIShipToAccountSetupToActive();
        ast.setAccountContext(domainObj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isGuardCondition_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASAccountSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTVMIShipToAccountSetupToActive ast = new EP_ASTVMIShipToAccountSetupToActive();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void doOnExit_test(){
        EP_ASTVMIShipToAccountSetupToActive ast = new EP_ASTVMIShipToAccountSetupToActive();
        ast.doOnExit();
        // No Processing Logic, hence adding a dummy assert.
        system.Assert(true); 
    }
    
}