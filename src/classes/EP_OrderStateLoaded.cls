/*   
     @Author Aravindhan Ramalingam
     @name <EP_OrderStateLoaded .cls>     
     @Description <Order State for Loaded status, common for all order types>   
     @Version <1.1> 
     */

     public class EP_OrderStateLoaded extends EP_OrderState {
        
        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            super.setOrderDomainObject(currentOrder);
        }
        
        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_OrderStateLoaded','doOnEntry');
            
        }

        public static String getTextValue()
        {
            return EP_OrderConstant.OrderState_Loaded;
        }
}