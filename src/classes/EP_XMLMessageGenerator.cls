/* 
   @Author <Accenture>
   @name <EP_OrderSyncControllerExtension>
   @CreateDate <02/08/2017>
   @Description <class to use call a VF page to that will return the XML> 
   @Version <1.0>
*/
public with sharing class EP_XMLMessageGenerator {
   /**
	* @Author : Accenture
	* @Name : generateXML
	* @Date : 02/07/2017
	* @Description :This method will be use load VF page (which contains XML message Structure) and Convert the page content into string
	* @Param : Id , string, string, string, string
	* @return :string
	*/     
    public static string generateXML(Id recordId , string messageId, string messageType, string vfPageName, string companyCode) {
        EP_GeneralUtility.Log('Public','EP_XMLMessageGenerator','generateXML');
        try {
            PageReference aPage = new PageReference('/apex/'+vfPageName);
            aPage.setRedirect(true);
	        aPage.getParameters().put(EP_Common_Constant.ID,recordId);
	        aPage.getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,messageId);
	        aPage.getParameters().put(EP_Common_Constant.PARAM_MESSAGE_TYPE,messageType);
	        aPage.getParameters().put(EP_Common_Constant.PARAM_SECRETCODE,messageType+messageId+recordId+vfPageName);
	        aPage.getParameters().put(EP_Common_Constant.SOURCE_COMPANY,companyCode);
	        //Added condition to accomodate test class as it does not support getContent method
	        string content ='';
	        if (!Test.IsRunningTest()) {
		        content=aPage.getContent().toString();
		   	}
		   	else {
		        content=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
		   	}
            return content;
        } catch(exception exp) {
            throw new EP_IntegrationException(EP_IntegrationException.EP_IntegrationErrorCode.GENERIC_EXCEPTION, exp.getMessage());
        }
    }
}