/*
   @Author <Pooja Dhiman>
   @name <EP_CustomerFinanceCreationWS>
   @CreateDate <06/20/2016>
   @Description <This is the method that handles HttpPost request for Invoice, Payment, and Credit memo Creations>
   @Version <1.0>
*/
@RestResource(urlMapping='/v1/CustomerFinanceCreation/*')
global with sharing class EP_CustomerFinanceCreationWS {
    
    /**
	 * @author <Vinay Jaiswal>
	 * @description <This is a 'HTTPPOST' method which recieves generic finance json from external system>
	 * @date <06/20/2016>
	 * @param <Not Required>
	 * @return void
	*/
    @HttpPost
    global static void generateDocumentTypeBasedOnObject(){
        RestRequest request = RestContext.request;
        RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
        String requestBody = request.requestBody.toString();
        String resultData = EP_GenericFinance_Json.manageDocuments(requestBody);
        EP_DebugLogger.printDebug('[EP_CustomerFinanceCreationWS]: resultData ' + resultData);
        if(String.isNotBlank(resultData)){
             RestContext.response.responseBody = Blob.valueOf(resultData);
        }
    }
}