@isTest
public class EP_AccountType_UT{
    
    static testMethod void setCustomerActivationDate_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.setCustomerActivationDate(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    
    static testMethod void validateOverdueInvoice_PositiveScenariotest() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.validateOverdueInvoice(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void insertPlaceHolderTankDips_test() {
        EP_AccountType localObj = new EP_AccountType();
        Test.startTest();
        localObj.insertPlaceHolderTankDips();
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void setCountry_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.setCountry(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    
    static testMethod void isBankAccountRequired_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.isBankAccountRequired(account);
        Test.stopTest();
        System.AssertEquals(false, result);
    }
    
    static testMethod void hasPackagedProduct_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.hasPackagedProduct(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void isPackagedPaymentTermProvided_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.isPackagedPaymentTermProvided(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isProductListLinkedWithDifferentSellTo_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.isProductListLinkedWithDifferentSellTo(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void hasSameCompanyOnProducts_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.hasSameCompanyOnProducts(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void hasSameCompanyOnProductList_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.hasSameCompanyOnProductList(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void syncCustomerToPricingEngine_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.syncCustomerToPricingEngine(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    
    static testMethod void isSupplyOptionAttached_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.isSupplyOptionAttached(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void hasCompletedReviews_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.hasCompletedReviews(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void getBillTo_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Account result = localObj.getBillTo(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    
    static testMethod void isProductListAttached_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.isProductListAttached(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void getSupplyLocationList_test() {
        EP_AccountType localObj = new EP_AccountType();
        Id accountId = EP_TestDataUtility.getShipToPositiveScenario().id;
        Test.startTest();
        LIST<EP_Stock_Holding_Location__c> result = localObj.getSupplyLocationList(accountId);
        Test.stopTest();
        System.assertEquals(true, result.size() == 0);
    }     
    static testMethod void hasSetUpShipTo_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.hasSetUpShipTo(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isDirectDebitPayment_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.isDirectDebitPayment(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isKYCReviewRejected_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.isKYCReviewRejected(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void setPackagePaymentTermValue_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.setPackagePaymentTermValue(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void setCountryLookup_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.setCountryLookup(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void doBeforeInsertHandle_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.doBeforeInsertHandle(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void doBeforeUpdateHandle1_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.doBeforeUpdateHandle(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void doBeforeUpdateHandle_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account newAccount;
        Account oldAccount;
        Test.startTest();
        localObj.doBeforeUpdateHandle(newAccount,oldAccount);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void sendCustomerCreateRequest_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.sendCustomerCreateRequest(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void sendCustomerEditRequest_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.sendCustomerEditRequest(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void doAfterInsertHandle_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.doAfterInsertHandle(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void doAfterUpdateHandle_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.doAfterUpdateHandle(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void doAfterUpdateHandle1_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account newAccount;
        Account oldAccount;
        Test.startTest();
        localObj.doAfterUpdateHandle(newAccount,oldAccount);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    //L4 45526 Start
    static testMethod void doBeforeDeleteHandle_test() {
        Account acct = EP_TestDataUtility.createStockHoldingLocation();
        acct.EP_Status__c =EP_AccountConstant.DEACTIVATE ;
        insert acct;
        EP_AccountType localObj = new EP_AccountType();
        Test.startTest();
        localObj.doBeforeDeleteHandle(acct);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    //L4 45526 End
    static testMethod void getSupplyOptions_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        LIST<EP_Stock_Holding_Location__c> result = localObj.getSupplyOptions(account);
        Test.stopTest();
        system.assertEquals(null, result);
    }
    static testMethod void getPriceBook_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Pricebook2 result = localObj.getPriceBook(account);
        Test.stopTest();
        system.assertEquals(null, result.Id);
    }
    
    static testMethod void setCompanyFromParent_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.setCompanyFromParent(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    
    /*****Defect Fix- 43911 START********/  
    static testMethod void setOwnerFromParent_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.setOwnerFromParent(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    /*****Defect Fix- 43911 END********/
    static testMethod void doActiveAccount_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.doActiveAccount(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void isShipToCSCReviewCompleted_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.isShipToCSCReviewCompleted(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void hasCompletedReviewActions_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.hasCompletedReviewActions(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void hasCompletedReviewActionsOnTanks_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.hasCompletedReviewActionsOnTanks(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isTankCSCReviewCompleted_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.isTankCSCReviewCompleted(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isPrimarySupplyOptionAttached_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.isPrimarySupplyOptionAttached(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void insertPlaceHolderTankDips1_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.insertPlaceHolderTankDips(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void deletePlaceHolderTankDips_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.deletePlaceHolderTankDips(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    
    static testMethod void getSellTo_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Account result = localObj.getSellTo(account);
        Test.stopTest();
        System.AssertEquals(null, result);
    }

    static testMethod void hasBillTo_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.hasBillTo(account);
        Test.stopTest();
        System.AssertEquals(false,result);
    }

    static testMethod void isShipToActive_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.isShipToActive(account);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void setSentNAVWINDMFlag_test() {
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.setSentNAVWINDMFlag(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    
    static testMethod void getCustomerPONumber_Test(){
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        string result = localObj.getCustomerPONumber(account);
        Test.stopTest();
        system.assertEquals(EP_Common_Constant.BLANK, result);
    }
    
    static testMethod void getPricebookId_Test(){
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellToASProspectDomainObject().getAccount();
        Test.startTest();
        string result = localObj.getPricebookId(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    
    static testMethod void getPricebookEntry_Test(){
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Map<string,PricebookEntry> result = localObj.getPricebookEntry(account);
        Test.stopTest();
        system.assert(true, result.isEmpty());
    }
    
    static testMethod void getPricebookEntry_WithKeyTest(){
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Map<string,PricebookEntry> result = localObj.getPricebookEntry(account, 'Test');
        Test.stopTest();
        system.assert(true, result.isEmpty());
    }
    
    static testMethod void isCurrencySameOnProducts_test(){
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellToASProspectDomainObject().getAccount();
        Test.startTest();
        boolean result = localObj.isCurrencySameOnProducts(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.Assert(true);
    }
    
    static testMethod void isRouteAllocationAvailable_Test(){
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getShipTo();
        Test.startTest();
        Boolean result = localObj.isRouteAllocationAvailable(account);
        Test.stopTest();
        system.assert(true, result);
    }
    
    static testMethod void isPickupEnabledSupplyOptionAttached_Test(){
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.isPickupEnabledSupplyOptionAttached(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.assert(true, result);
    }
    
    static testMethod void sendCustomerCreateRequestToNAV_test(){
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.sendCustomerCreateRequestToNAV(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.assert(true);
    }
    
    static testMethod void sendCustomerCreateRequestToWinDMS_test(){
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.sendCustomerCreateRequestToWinDMS(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.assert(true);
    }
    
    static testMethod void isRecommendedCreditLimitRequired_test(){
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.isRecommendedCreditLimitRequired(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.assert(true);
    }
    
    static testMethod void isSellToSynced_test(){
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.isSellToSynced(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.assert(true);
    }
    
    static testMethod void isSellToActive_test(){
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        Boolean result = localObj.isSellToActive(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.assert(true);
    }

 // Changes made for CUSTOMER MODIFICATION L4 Start    
    static testMethod void sendCustomerEditRequestToNAV_test(){
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.sendCustomerEditRequestToNAV(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.assert(true);
    }

    static testMethod void sendCustomerEditRequestToWinDMS_test(){
        EP_AccountType localObj = new EP_AccountType();
        Account account = EP_TestDataUtility.getSellTo();
        Test.startTest();
        localObj.sendCustomerEditRequestToWinDMS(account);
        Test.stopTest();
        //Virtual Void Method, No Processing Logic, hence adding a dummy assert.
        system.assert(true);
    }
// Changes made for CUSTOMER MODIFICATION L4 End    
}