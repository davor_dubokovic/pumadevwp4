/*
*  @Author <Accenture>
*  @Name <EP_VMIShipToASBasicDataSetup>
*  @CreateDate <>
*  @Description <VMI Ship To Account State for 02-Basic Data Setup Status>
*  @Version <1.0>
*/
public with sharing class EP_VMIShipToASBasicDataSetup extends EP_AccountState{
    /***NOvasuite fix constructor removed**/

    /**
    * @author <Accenture>
    * @name setAccountDomainObject
    * @Param EP_AccountDomainObject 
    */
    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASBasicDataSetup','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }
    
    //L4_45352_Start
    /**
    * @author <Accenture>
    * @name doOnEntry
    */
    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASBasicDataSetup','doOnEntry');
        EP_AccountService service = new EP_AccountService(this.account);
        if(service.isSellToSynced() && !EP_IntegrationUtil.ISERRORSYNC){                      
            if(!this.account.localaccount.EP_Synced_PE__c){ //WP2-Pricing Engine Callout changes  
                system.debug('-Calling--PE--');
                service.doActionSyncCustomerToPricingEngine();
            }
            else if(!this.account.localaccount.EP_Synced_NAV__c){ //WP2-Pricing Engine Callout changes                
                system.debug('-Calling--NAV--');
                service.doActionSendCreateRequestToNav();
            }
            else if(this.account.localaccount.EP_Synced_NAV__c && !this.account.localaccount.EP_Synced_WinDMS__C){
                system.debug('-Calling--WinDMS--');
                service.doActionSendCreateRequestToWinDMS();
            }
        }
    }
    //L4_45352_END  
     /**
    * @author <Accenture>
    * @name doOnExit
    */
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASBasicDataSetup','doOnExit');
        
    }
     /**
    * @author <Accenture>
    * @name doTransition
    * @return boolean
    */
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASBasicDataSetup','doTransition');
        return super.doTransition();
    }
     /**
    * @author <Accenture>
    * @name isInboundTransitionPossible
    * @return boolean
    */
    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASBasicDataSetup','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();
        
    }
    
}