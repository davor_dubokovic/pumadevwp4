/**
    @Author         Accenture
    @Name           EP_RunMapper 
    @Createdate     12/28/2016
    @Description    This class contains all SOQLs related to Run Object
    @Version        1.0
    @Reference      NA
*/
public with sharing class EP_RunMapper {
    
    /**     
        * @Author       Accenture
        * @Name         EP_RunMapper 
        * @Description  Constructor
    */
    public EP_RunMapper() { 
    } 
    
    /**         
        * @Author       Accenture
        * @Date         02/03/2017
        * @Name         getActiveRunsByRoute
        * @Description  This method is used to return List of run records filder by route id
        * @Param        Id 
        * @Return       list<EP_Run__c> 
        * @Throws       NA
    */
    public list<EP_Run__c> getActiveRunsByRoute(Id routeId) {
        EP_GeneralUtility.Log('Public','EP_RunMapper','getActiveRunsByRoute');
        List<EP_Run__c> listOfRun = [SELECT Id, EP_Route__c, EP_Run_Number__c, Name 
                                     FROM EP_Run__c 
                                     WHERE EP_Route__c =: routeId AND EP_Run_End_Date__c >= Today];
        return listOfRun; 
    } 
     
    /**
    * @author       Accenture
    * @name         getRunList
    * @date         05/05/2017
    * @description  This method returns run records 
    * @param        Id
    * @return       list<EP_Run__c>>
    */
    public list<EP_Run__c> getRunList(list<EP_Run__c> newRuns) {
        EP_GeneralUtility.Log('Public','EP_RunMapper','getActiveRunsByRoute');
        List<EP_Run__c> runList = [SELECT name,EP_Route__r.Name,EP_Run_Number__c,EP_Run_Start_Date__c,EP_Run_End_Date__c, EP_Instance_Number__c
                                   FROM EP_Run__c 
                                   WHERE ID IN : newRuns ];         
        return runList; 
    }
     /**
    * @author       Accenture
    * @name         getRunsByRoute
    * @date         05/05/2017
    * @description  This method returns run records based on RouteId   
    * @param        Id
    * @return       list<EP_Run__c>>
    */
    
    
    public list<EP_Run__c> getRunsByRoute(Id routeId) {
        EP_GeneralUtility.Log('Public','EP_RunMapper','getActiveRunsByRoute');
        List<EP_Run__c> runList = [SELECT Id, EP_Route__c, EP_Run_Number__c, Name, EP_Instance_Number__c
                                     FROM EP_Run__c 
                                     WHERE EP_Route__c =: routeId ];
        return runList; 
    }
    /**
    * @author       Accenture
    * @name         getRunsByRunId
    * @date         05/05/2017
    * @description  This method returns run records  
    * @param        set<Id>
    * @return       list<EP_Run__c>>
    */  
    
    public list<EP_Run__c> getRunsByRunId(set<Id> runIdSet) {
        EP_GeneralUtility.Log('Public','EP_RunMapper','getActiveRunsByRoute');
        List<EP_Run__c> listOfRun = [SELECT Id, EP_Route__c, EP_Run_Number__c, Name
                                     FROM EP_Run__c 
                                     WHERE Id in : runIdSet];
        return listOfRun; 
    }
    /**
    * @author       Accenture
    * @name         getAssociatedOrdersWithRun
    * @date         05/05/2017
    * @description  This method returns run records assoicated with order records ( For Deletion of Run  functionality)
    * @param        List <EP_Run__c>
    * @return       set<Id>
    */
        
    public set<Id>  getAssociatedOrdersWithRun(List <EP_Run__c> oldRunList) {
        set<Id> runIdsSet = new set<Id>();
        /*for (Order order: [Select Id, EP_Run__c from Order where EP_Run__c in : oldRunList]){
            runIdsSet.add(order.EP_Run__c);                        
        } */
        return  runIdsSet;
    }
    
 }