/**
   @Author          CR Team
   @name            EP_BusinessChannelSegmentMapper
   @CreateDate      12/21/2016
   @Description     This class contains all SOQLs related to EP_Business_Channel_Segment__c Object
   @Version         1.0
   @reference       NA
   */
   public with sharing class EP_BusinessChannelSegmentMapper {

    /**
    *  Constructor. 
    *  @name            EP_BusinessChannelSegmentMapper
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */ 
    public EP_BusinessChannelSegmentMapper() {

    }      
    
    /** This method returns Business Channel Segment Records in a MAP
    *  @name            getRecordsByIDsAsMap 
    *  @param           set<Id> 
    *  @return          Map<Id, EP_Business_Channel_Segment__c>
    *  @throws          NA
    */
    public Map<Id, EP_Business_Channel_Segment__c> getRecordsByIDsAsMap(set<id> idSet){
        EP_GeneralUtility.Log('Public','EP_BusinessChannelSegmentMapper','getRecordsByIDsAsMap');
        Map<Id, EP_Business_Channel_Segment__c> recordMap = new Map<Id, EP_Business_Channel_Segment__c>(
            [SELECT Id, Name, EP_Code__c FROM EP_Business_Channel_Segment__c WHERE ID IN :idSet]
            );
        return recordMap;
    }
}