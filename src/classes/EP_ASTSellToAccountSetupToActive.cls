public with sharing class EP_ASTSellToAccountSetupToActive extends EP_AccountStateTransition{
    public EP_ASTSellToAccountSetupToActive() {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToAccountSetupToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToAccountSetupToActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToAccountSetupToActive','isGuardCondition');
        if(EP_Common_constant.SYNC_STATUS.equalsignorecase(this.account.localaccount.EP_Integration_Status__c)||this.account.localaccount.EP_Is_Dummy__c){
        	return true;
        }
        return false;
    }
}