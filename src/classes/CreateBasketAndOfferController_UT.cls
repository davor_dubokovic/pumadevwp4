@isTest(SeeAllData=true)
private class CreateBasketAndOfferController_UT
{
	@isTest
	static void createBasket_test() {

		Account acc = new Account(Name = 'testAccount', billingCountry = 'US');
        INSERT acc;

		ApexPages.StandardController sc = new ApexPages.StandardController(acc);
		CreateBasketAndOfferController controller = new CreateBasketAndOfferController(sc);

		controller.createBasket();

		String basketName = 'Order for ' + acc.Name;
		cscfga__Product_Basket__c createdBasket = [SELECT ID, name, csbb__Account__c FROM cscfga__Product_Basket__c WHERE name= :basketName];

		System.assertEquals(basketName, createdBasket.name);
		System.assertEquals(acc.id, createdBasket.csbb__Account__c);

		CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();
		String iFrame = '<iframe id="mle-iframe" src="'+csOrderSetting.MLE_Org_URL__c+'?batchSize='+csOrderSetting.MLE_BatchSize__c+'&id=' + createdBasket.id + '&productDefinitionId=' 
		+csOrderSetting.OrderLineItemProductDefinitionLineId__c +'&showHeader='+csOrderSetting.MLE_ShowHeader__c+'&sideBar='+csOrderSetting.MLE_SideBar_Selected__c+'&embedded='
		+csOrderSetting.MLE_Embedded__c+'&cssoverride='+csOrderSetting.CSS_Resource_Path__c+'&scriptplugin='+csOrderSetting.Javascript_Resource_Path__c
        +'" width="'+csOrderSetting.MLE_IFRAME_WIDTH__c+'" height="'+csOrderSetting.MLE_HEIGHT__c+'" frameBorder="'+csOrderSetting.MLE_FrameBorder__c+'"></iframe>';

		// verify connectConfigurations() is executed properly
// 		System.assertEquals(createdBasket.id, controller.attribute1.cscfga__Value__c);
//		System.assertEquals(iFrame, controller.attribute7.cscfga__Value__c);
 		System.assertEquals(null, controller.attribute1.cscfga__Value__c);
		System.assertEquals(null, controller.attribute7.cscfga__Value__c);
	}

	@isTest
	static void createBasketForOpenOrderContract_test() {
		//Insert test Account:
		Account acc = new Account(Name = 'testAccount', billingCountry = 'US');
		INSERT acc;
		
		//Instantiate a new Apex Page Standard Controller:
		ApexPages.StandardController sc = new ApexPages.StandardController(acc);
		//Assign the tested Class as the controller of the new Apex Page:
		CreateBasketAndOfferController controller = new CreateBasketAndOfferController(sc);
		
		//Call the method (being tested) that returns the Page Reference:
		PageReference configPage = controller.createBasketForOpenOrderContract();
		
		//Query Basket object and check if the field: csbb__Account__c has the Id of the test Account:
		String basketName = 'Contract for ' + acc.Name;
		cscfga__Product_Basket__c createdBasket = [SELECT Id, name, csbb__Account__c FROM cscfga__Product_Basket__c WHERE name= :basketName];
		
		System.assertEquals(basketName, createdBasket.name);
		System.assertEquals(acc.id, createdBasket.csbb__Account__c);
		
		//Get Custom Setting that stores the Name of the Parent Order:
		CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();
		
		//Query the Product Configuration object and check if its Id is in the Page Parameters:
		cscfga__Product_Configuration__c parentConfiguration = [SELECT Id, Name FROM cscfga__Product_Configuration__c
																WHERE name = : csOrderSetting.Parent_Order_Name__c
																AND cscfga__Product_Basket__c = :createdBasket.Id];
		
		System.assertEquals(parentConfiguration.Id,  configPage.getParameters().get('configId'));
		System.assertEquals(createdBasket.Id,  configPage.getParameters().get('linkedId'));
		System.assertEquals('Contract', configPage.getParameters().get('type'));
		System.assertEquals(csOrderSetting.OpenOrderContractScreenFlow__c, configPage.getParameters().get('ScreenFlowName') );
	
	}
		
		@isTest
	static void editBasket_test() {

		Account acc = new Account(Name = 'testAccount', billingCountry = 'US');
        INSERT acc;

		ApexPages.StandardController sc = new ApexPages.StandardController(acc);
		CreateBasketAndOfferController controller = new CreateBasketAndOfferController(sc);

		controller.editBasket();

		String basketName = 'Order for ' + acc.Name;
		cscfga__Product_Basket__c createdBasket = [SELECT ID, name, csbb__Account__c FROM cscfga__Product_Basket__c WHERE name= :basketName];

		System.assertEquals(basketName, createdBasket.name);
		System.assertEquals(acc.id, createdBasket.csbb__Account__c);

	}
}