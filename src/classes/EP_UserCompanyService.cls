/****************************************************************
@Author      Accenture                                          *
@name        EP_UserCompanyService                            *
@CreateDate  1/11/2017                                          *
@Description Service class for Credit Holding Object            *
@Novasuite fixes - Required Documentation
@Version     1.0                                                *
****************************************************************/
public class EP_UserCompanyService{
    public EP_User_CompanyDomainObject UserCompanyDomainObject;
    private EP_User_Company__c localUserCompanyObj;

    

/****************************************************************
* @author       Accenture                                       * 
* @name         EP_UserCompanyService                         *
* @description  Constructor for setting UserCompanyDomain     *
* @param        EP_User_CompanyDomainObject                    *
* @return       NA                                              *
****************************************************************/ 
    public EP_UserCompanyService(EP_User_CompanyDomainObject UserCompanyDomainObject){
    	EP_GeneralUtility.Log('Public','EP_UserCompanyService','EP_UserCompanyService');
        this.UserCompanyDomainObject = UserCompanyDomainObject;
        this.localUserCompanyObj = UserCompanyDomainObject.getUserCompanyObject();
    }

/****************************************************************
* @author       Accenture                                       *
* @name         doAfterInsertHandle                             *
* @description  method to execute logic for 
                After Insert of credit holding                 *
* @param        NA                                              *
* @return       NA                                              *
****************************************************************/ 
    Public void doAfterInsertHandle(){
        EP_GeneralUtility.Log('Public','EP_UserCompanyService','doAfterInsertHandle');
        EP_ActionShareUtility EP_ActionShareUtilityobj = new EP_ActionShareUtility();
        EP_ActionShareUtilityobj.shareReviewRecords(localUserCompanyObj.Company__c, localUserCompanyObj.user__c);
    }
    
    /****************************************************************
* @author       Accenture                                       *
* @name         doAfterDeleteHandle                             *
* @description  method to execute logic for 
                Before Insert of credit holding                 *
* @param        NA                                              *
* @return       NA                                              *
****************************************************************/ 
    
    Public void doAfterDeleteHandle(){
        EP_GeneralUtility.Log('Public','EP_UserCompanyService','doAfterDeleteHandle');
        EP_ActionShareUtility EP_ActionShareUtilityobj = new EP_ActionShareUtility();
        EP_ActionShareUtilityobj.removesharedAccess(localUserCompanyObj.Company__c, localUserCompanyObj.user__c);
    }
    

   
}