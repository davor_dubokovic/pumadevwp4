@isTest
public class EP_AcknowledgementHandler_UT
{
	static String ERROR_CODE = '200';
	static String MSG_TYPE = 'COMMON_ACKNOWLEDGEMENT';
	static String MESSAGE_ID = 'SFD-GBL-CPE-24012017-16:00:20-024242';
	static String STATUS = '';
	static String ERROR_DESC = 'Null Pointer Exception';

	@testSetup static void init() {
        List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
        List<EP_PROCESS_NAME_CS__c> lProcessNameCustomSetting = Test.loadData(EP_PROCESS_NAME_CS__c.sObjectType, 'EP_PROCESS_NAME_CS_TestData');
    }

	static testMethod void processRequest_test() {
		EP_AcknowledgementHandler localObj = new EP_AcknowledgementHandler();
		EP_AcknowledgementStub acknowledgement = EP_TestDataUtility.createAcknowledgementStub();
		acknowledgement.MSG.HeaderCommon.CorrelationID=MESSAGE_ID;
		Account acc = EP_TestDataUtility.getSellTo();
		EP_AcknowledgementStub.dataset ackStub = EP_TestDataUtility.createAcknowledgementDataset();
		EP_IntegrationRecord__c  record = new EP_IntegrationRecord__c ();
		record.EP_Message_ID__c = MESSAGE_ID;
		record.EP_Object_Type__c ='Account';
		record.EP_Object_ID__c=acc.Id;
		record.EP_Status__c='SENT';
		record.EP_Source__c='Source';
		record.EP_Target__c='Target';
		record.EP_SeqId__c=ackStub.seqId;
		Database.insert(record);
		
        string jsonstring = JSON.serialize(acknowledgement);
		Test.startTest();
		String result = localObj.processRequest(jsonstring);
		Test.stopTest();
		System.assertEquals(true, result!=null);
	}
	
	static testMethod void processRequest_test2() {
		EP_AcknowledgementHandler localObj = new EP_AcknowledgementHandler();
        string jsonstring = JSON.serialize(EP_TestDataUtility.createAcknowledgementStub());
        jsonstring= jsonstring.substring(0,jsonstring.length()-2);
		Test.startTest();
		String result = localObj.processRequest(jsonstring);
		Test.stopTest();
		System.assertEquals(true, result!=null);
	}
	
	static testMethod void processIntegrationRecords_test1() {
		EP_AcknowledgementStub acknowledgement = EP_TestDataUtility.createAcknowledgementStub();
		acknowledgement.MSG.HeaderCommon.CorrelationID =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
		Test.startTest();
		LIST<EP_IntegrationRecord__c> result = EP_AcknowledgementHandler.processIntegrationRecords(acknowledgement);
		Test.stopTest();
		System.assertEquals(true, result!=null);
	}
	static testMethod void processIntegrationRecords_test2() {
		EP_AcknowledgementStub acknowledgement = EP_TestDataUtility.createAcknowledgementStub();
		acknowledgement.MSG.HeaderCommon.CorrelationID =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
		acknowledgement.MSG.HeaderCommon.ProcessStatus =EP_Common_Constant.FAILURE;
		Test.startTest();
		LIST<EP_IntegrationRecord__c> result = EP_AcknowledgementHandler.processIntegrationRecords(acknowledgement);
		Test.stopTest();
		System.assertEquals(true, result!=null);
	}

	static testMethod void fillDatasetMap_test() {
		LIST<EP_AcknowledgementStub.dataset> pDataset = EP_TestDataUtility.createAcknowledgementDatasets();
		String ackType = EP_Common_Constant.PROCESSING;
		Test.startTest();
		Map<string,EP_AcknowledgementStub.dataset> result = EP_AcknowledgementHandler.fillDatasetMap(pDataset,ackType);
		Test.stopTest();
		System.assertEquals(true, result.get(pDataset[0].seqId).finalStatus==EP_Common_Constant.SYNC_STATUS);
	}
	static testMethod void fillDatasetMap1_test() {
		LIST<EP_AcknowledgementStub.dataset> pDataset = EP_TestDataUtility.createAcknowledgementDatasets();
		pDataset[0].errorDescription = ERROR_DESC;
		String ackType = EP_Common_Constant.PROCESSING;
		Test.startTest();
		Map<string,EP_AcknowledgementStub.dataset> result = EP_AcknowledgementHandler.fillDatasetMap(pDataset,ackType);
		Test.stopTest();
		System.assertEquals(true,result.get(pDataset[0].seqId).finalStatus==EP_Common_Constant.ERROR_SYNC_STATUS);
	}
	static testMethod void fillDatasetMap2_test() {
		LIST<EP_AcknowledgementStub.dataset> pDataset = EP_TestDataUtility.createAcknowledgementDatasets();
		String ackType = EP_Common_Constant.STAGING;
		Test.startTest();
		Map<string,EP_AcknowledgementStub.dataset> result = EP_AcknowledgementHandler.fillDatasetMap(pDataset,ackType);
		Test.stopTest();
		System.assertEquals(true,result.get(pDataset[0].seqId).finalStatus== EP_Common_Constant.ACKNWLDGD);
	}
	static testMethod void fillDatasetMap3_test() {
		LIST<EP_AcknowledgementStub.dataset> pDataset = EP_TestDataUtility.createAcknowledgementDatasets();
		pDataset[0].errorDescription = 'ERROR_DESC';
		String ackType = EP_Common_Constant.STAGING;
		Test.startTest();
		Map<string,EP_AcknowledgementStub.dataset> result = EP_AcknowledgementHandler.fillDatasetMap(pDataset,ackType);
		Test.stopTest();
		System.assertEquals(true,result.get(pDataset[0].seqId).finalStatus==EP_Common_Constant.ERR_ACKNWLDGD);
	}
	static testMethod void getErrorDescription_test() {
		String transportStatus = '';
		String errorCode = '';
		String errorDescription = '';
		Test.startTest();
		String result = EP_AcknowledgementHandler.getErrorDescription(EP_Common_Constant.FAILURE,transportStatus,errorCode,errorDescription);
		Test.stopTest();
		System.assertEquals(true, EP_Common_Constant.PROCESS_STATUS == result);
	}
	static testMethod void getErrorDescription1_test() {
		string processStatus = '';
		String errorCode = '';
		String errorDescription = '';
		Test.startTest();
		String result = EP_AcknowledgementHandler.getErrorDescription(processStatus,EP_Common_Constant.FAILURE,errorCode,errorDescription);
		Test.stopTest();
		System.assertEquals(true, EP_Common_Constant.TRANSPORT_STATUS == result);
	}
	static testMethod void getErrorDescription2_test() {
		string processStatus = '';
		String transportStatus = '';
		String errorDescription = 'Error';
		Test.startTest();
		String result = EP_AcknowledgementHandler.getErrorDescription(processStatus,transportStatus,ERROR_CODE,errorDescription);
		Test.stopTest();
		System.assertEquals(true, result==errorDescription);
	}
	static testMethod void getIntegrationRecords_test() {
		String pErrorDesciption;
		Account acc = EP_TestDataUtility.getSellTo();
		EP_AcknowledgementStub.dataset ackStub = EP_TestDataUtility.createAcknowledgementDataset();
		List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
		ackDatasets.add(ackStub);
		String ackType = EP_Common_Constant.PROCESSING;
		Map<string,EP_AcknowledgementStub.dataset> lmapDataset = EP_AcknowledgementHandler.fillDatasetMap(ackDatasets,ackType);
		
		//createAcknowledgementDatasets
		EP_IntegrationRecord__c  record = new EP_IntegrationRecord__c ();
		record.EP_Message_ID__c = MESSAGE_ID;
		record.EP_Object_Type__c ='Account';
		record.EP_Object_ID__c=acc.Id;
		record.EP_Status__c='SENT';
		record.EP_Source__c='Source';
		record.EP_Target__c='Target';
		record.EP_SeqId__c=ackStub.seqId;
		Database.insert(record);
		//Changes for #59187 for Transport Status
		EP_MessageHeader headerCommon = new EP_MessageHeader();
		headerCommon.CorrelationID = MESSAGE_ID;
		headerCommon.TransportStatus = 'New';
		
		Boolean isFailure = true;
		Test.startTest();
		LIST<EP_IntegrationRecord__c> result = EP_AcknowledgementHandler.getIntegrationRecords(headerCommon,EP_Common_Constant.SENT_STATUS,ERROR_DESC,lmapDataset,isFailure);
		Test.stopTest();
		System.assertEquals(true, result != null);
		System.assertEquals(result.get(0).EP_Status__c, 'SENT');
		System.assertEquals(result.get(0).EP_Transport_Status__c, 'New');
	}
	static testMethod void createAcknowledgement_test1() {
		String messageType = MSG_TYPE;
		Boolean processingFailed = true;
		String failureReason = ERROR_DESC;
		EP_MessageHeader headerCommon = new EP_MessageHeader();
		List<EP_AcknowledgementStub.dataset> listDataset = EP_TestDataUtility.createAcknowledgementDatasets();
		Test.startTest();
		String result = EP_AcknowledgementHandler.createAcknowledgement(messageType,processingFailed,failureReason,headerCommon,listDataset);
		Test.stopTest();
		System.assertEquals(true, result != null);
	}
	static testMethod void createAcknowledgement_test2() {
		String messageType = MSG_TYPE;
		Boolean processingFailed = true;
		String failureReason = ERROR_DESC;
		EP_MessageHeader headerCommon = new EP_MessageHeader();
		//List<EP_AcknowledgementStub.dataset> listDataset = EP_TestDataUtility.createAcknowledgementDatasets();
		List<EP_AcknowledgementStub.dataset> listDataset ;
		Test.startTest();
		String result = EP_AcknowledgementHandler.createAcknowledgement(messageType,processingFailed,failureReason,headerCommon,listDataset);
		Test.stopTest();
		System.assertEquals(true, result != null);
	}
	static testMethod void createAcknowledgement1_test() {
		String messageType = MSG_TYPE;
		Boolean processingFailed = false;
		String failureReason = '';
		EP_MessageHeader headerCommon = new EP_MessageHeader();
		List<EP_AcknowledgementStub.dataset> listDataset = EP_TestDataUtility.createAcknowledgementDatasets();
		listDataset[0].errorDescription = ERROR_DESC;
		Test.startTest();
		String result = EP_AcknowledgementHandler.createAcknowledgement(messageType,processingFailed,failureReason,headerCommon,listDataset);
		Test.stopTest();
		System.assertEquals(true, result != null);
	}
	static testMethod void createAcknowledgement2_test() {
		String messageType = MSG_TYPE;
		Boolean processingFailed = false;
		String failureReason = '';
		EP_MessageHeader headerCommon = new EP_MessageHeader();
		List<EP_AcknowledgementStub.dataset> listDataset = EP_TestDataUtility.createAcknowledgementDatasets();
		Test.startTest();
		String result = EP_AcknowledgementHandler.createAcknowledgement(messageType,processingFailed,failureReason,headerCommon,listDataset);
		Test.stopTest();
		System.assertEquals(true, result != null);
	}
	static testMethod void generateMessageId_test() {
		Test.startTest();
		String result = EP_AcknowledgementHandler.generateMessageId(MSG_TYPE);
		Test.stopTest();
		System.assert(true, result != null);
	}
	static testMethod void checkForPartialSuccess_PositiveScenariotest() {
		LIST<EP_AcknowledgementStub.dataset> listDataset = EP_TestDataUtility.createAcknowledgementDatasets();
		listDataset[0].errorDescription = ERROR_DESC;
		Test.startTest();
		Boolean result = EP_AcknowledgementHandler.checkForPartialSuccess(listDataset);
		Test.stopTest();
		System.AssertEquals(true,result);
	}
	static testMethod void checkForPartialSuccess_NegativeScenariotest() {
		LIST<EP_AcknowledgementStub.dataset> listDataset = EP_TestDataUtility.createAcknowledgementDatasets();
		Test.startTest();
		Boolean result = EP_AcknowledgementHandler.checkForPartialSuccess(listDataset);
		Test.stopTest();
		System.AssertEquals(false,result);
	}
	
	static testMethod void processTransportStatus_test1() {
		Account acc = EP_TestDataUtility.getSellTo();
		EP_AcknowledgementStub.dataset ackStub = EP_TestDataUtility.createAcknowledgementDataset();
		EP_AcknowledgementStub acknowledgement = EP_TestDataUtility.createAcknowledgementStub();
		acknowledgement.MSG.HeaderCommon.MsgID ='MSG-123-CCL-12324';
		acknowledgement.MSG.HeaderCommon.ProcessStatus =EP_Common_Constant.SUCCESS;
		acknowledgement.MSG.HeaderCommon.TransportStatus = 'Sent';
		acknowledgement.MSG.HeaderCommon.ErrorDescription = 'Transport Error - The requested service, https://172.20.8.88/EPUMA_IPaaSToNav_PushAsync/EPUMA_IPaaSToNav_PushAsync.svc could not be activated. See the servers diagnostic trace logs for more information. \r\nServer stack trace: \r\n   at System.ServiceModel.Channels.HttpChannelUtilities.';
		//Changes for #59187 for Transport Status
		acknowledgement.MSG.Payload.any0 = null;
		
		EP_IntegrationRecord__c  record = new EP_IntegrationRecord__c ();
		record.EP_Message_ID__c = acknowledgement.MSG.HeaderCommon.MsgID;
		record.EP_Object_Type__c ='Account';
		record.EP_Object_ID__c = acc.Id;
		record.EP_Status__c = 'SENT';
		record.EP_Source__c = 'Source';
		record.EP_Target__c = 'Target';
		record.EP_SeqId__c = ackStub.seqId;
		Database.insert(record);
		
		Test.startTest();
			LIST<EP_IntegrationRecord__c> result = EP_AcknowledgementHandler.processTransportStatus(acknowledgement);
		Test.stopTest();
		
		System.assertEquals(result.get(0).EP_Transport_Status__c, 'Sent');
		System.assertNotEquals('', result.get(0).EP_Error_Description__c);
		System.assertEquals(EP_Common_Constant.ERROR_SENT_STATUS, result.get(0).EP_Status__c);
		System.assertEquals(EP_Common_Constant.MIDDLEWARE_TRANS_ERROR, result.get(0).EP_Integration_Error_Type__c);
	}
	
	static testmethod void updateIntegrationRecords_Test() {
		Account acc = EP_TestDataUtility.getSellTo();
		EP_AcknowledgementStub.dataset ackStub = EP_TestDataUtility.createAcknowledgementDataset();

		EP_IntegrationRecord__c  record = new EP_IntegrationRecord__c ();
		record.EP_Message_ID__c = 'MSG-123-CCL-12324';
		record.EP_Object_Type__c ='Account';
		record.EP_Object_ID__c = acc.Id;
		record.EP_Status__c = 'SENT';
		record.EP_Source__c = 'Source';
		record.EP_Target__c = 'Target';
		record.EP_SeqId__c = ackStub.seqId;
		Database.insert(record);
		EP_AcknowledgementHandler.acknowledgementType = EP_Common_Constant.STAGING;
		
		Test.startTest();
			EP_AcknowledgementHandler handler = new EP_AcknowledgementHandler();		
			handler.updateIntegrationRecords(new list<EP_IntegrationRecord__c>{record}, '{JSONSTRING}' );
		Test.stopTest();
		
		EP_IntegrationRecord__c  updatedrecord = [select id, EP_Staging_Response_Message__c from EP_IntegrationRecord__c where Id=:record.id];
		System.assertNotEquals('', updatedrecord.EP_Staging_Response_Message__c);
	}
	static testMethod void processIntegrationRecords_test3() {
		EP_AcknowledgementStub acknowledgement = EP_TestDataUtility.createAcknowledgementStub();
		acknowledgement.MSG.HeaderCommon.CorrelationID ='MSG-123-CCL-12324';
		acknowledgement.MSG.HeaderCommon.ProcessStatus =EP_Common_Constant.SUCCESS;
		//Changes for #59187 for Transport Status
		acknowledgement.MSG.Payload.any0 = null;
		Test.startTest();
		LIST<EP_IntegrationRecord__c> result = EP_AcknowledgementHandler.processIntegrationRecords(acknowledgement);
		Test.stopTest();
		System.assertEquals(true, result!=null);
	}
	static testmethod void processBusinessAcknowledgement() {
		Account acc = EP_TestDataUtility.getSellTo();
		EP_AcknowledgementStub.dataset ackStub = EP_TestDataUtility.createAcknowledgementDataset();
		EP_AcknowledgementStub acknowledgement = EP_TestDataUtility.createAcknowledgementStub();
		acknowledgement.MSG.HeaderCommon.CorrelationID ='MSG-123-CCL-12324';
		acknowledgement.MSG.HeaderCommon.ProcessStatus =EP_Common_Constant.SUCCESS;
		EP_IntegrationRecord__c  record = new EP_IntegrationRecord__c ();
		record.EP_Message_ID__c = acknowledgement.MSG.HeaderCommon.CorrelationID;
		record.EP_Object_Type__c ='Account';
		record.EP_Object_ID__c = acc.Id;
		record.EP_Status__c = 'SENT';
		record.EP_Source__c = 'Source';
		record.EP_Target__c = 'Target';
		record.EP_SeqId__c = ackStub.seqId;
		Database.insert(record);
		
		Test.startTest();
			LIST<EP_IntegrationRecord__c> result = EP_AcknowledgementHandler.processBusinessAcknowledgement(acknowledgement);
		Test.stopTest();
		
		System.assertEquals(EP_Common_Constant.SYNC_STATUS, result.get(0).EP_Status__c);
	}
}