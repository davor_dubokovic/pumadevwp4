@isTest
public class EP_ConsignmentType_UT
{


static testMethod void hasCreditIssue_NegativeScenariotest() {
    EP_ConsignmentType localObj = new EP_ConsignmentType();
    csord__Order__c orderObj = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
    Test.startTest();
    Boolean result = localObj.hasCreditIssue(orderObj);
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void findRecordType_test() {
    EP_ConsignmentType localObj = new EP_ConsignmentType();
    EP_VendorManagement vmType = new EP_VendorManagement();
    Test.startTest();
    Id result = localObj.findRecordType(vmType);
    Test.stopTest();
    System.AssertEquals(null,result);

}
static testMethod void calculatePrice_test() {
    EP_ConsignmentType localObj = new EP_ConsignmentType();
    csord__Order__c orderObj = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
    Test.startTest();
    localObj.calculatePrice(orderObj);
    Test.stopTest();
    //Empty method
    System.Assert(true);
}
}