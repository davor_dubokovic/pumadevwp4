/*
    @Author          Accenture
    @Name            EP_Import_Retail_Order_Page_Controller
    @CreateDate      13/04/2018
    @Description     This is a controller class for EP_Import_Retail_Order_Page.
    @Version         1.0
    @Reference       NA
*/
public with sharing class EP_Import_Retail_Order_Page_Controller{

    public EP_Import_Retail_Order_Context ctx {get;set;}
    public EP_Import_Retail_Order_Helper xmlHelper;
    
    
    /*
    @Author          Accenture
    @Name            EP_Import_Retail_Order_Page_Controller
    @CreateDate      13/04/2018
    @Description     Constructor definition
    @Version         1.0
    @Reference       NA
    */
    public EP_Import_Retail_Order_Page_Controller(ApexPages.StandardController controller)
    {
        ctx = new EP_Import_Retail_Order_Context ();
    }
    
    /*
    @Author          Accenture
    @Name            parseXMLFile
    @CreateDate      13/04/2018
    @Description     Method used by command button on page and for initiating helper class
    @Version         1.0
    @Reference       NA
    */
    public pagereference parseXMLFile() {
      try{
            EP_Import_Retail_Order_Helper.processRequest(ctx.blbXMLFile.toString());
            //EP_ROImportStagingFactory.ProcessStagingRecords(ctx.fileRecordId,EP_Common_Constant.EP_RETAIL_ORDER);
        }catch (exception exp){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,system.Label.EP_XML_IMPORT_ERROR_MSG));
        } 
       return null;
    } 
}