@isTest 
private class EP_CRM_PipelineReportBeanTest {
	static Map < String, User > userMap = new Map < String, User > ();
	static Map < String, UserRole > roleMap = new Map < String, UserRole > ();
	
	static { 
    	for(User userObj: [SELECT Id, Name, EP_CRM_Geo1__c, EP_CRM_Geo2__c FROM User]) {
    		userMap.put(userObj.Name, userObj);
    	}
    	for(UserRole roleObj: [SELECT Id, Name FROM UserRole]) {
    		roleMap.put(roleObj.Name, roleObj);
    	}
	}
	
	@testSetup 
	static void loadData() {
		List < UserRole > roleList = new List < UserRole > ();
		roleList.add(EP_CRM_TestDataFactory.createUserRole('Global Executive', null));
		roleList.add(EP_CRM_TestDataFactory.createUserRole('MEAP Executive', null));
		roleList.add(EP_CRM_TestDataFactory.createUserRole('Columbia Executive', null));
		roleList.add(EP_CRM_TestDataFactory.createUserRole('Columbia Executive Sales Manager', null));
		roleList.add(EP_CRM_TestDataFactory.createUserRole('Columbia Executive Manager Retail', null));
		roleList.add(EP_CRM_TestDataFactory.createUserRole('Columbia Manager HighStreet', null));
		roleList.add(EP_CRM_TestDataFactory.createUserRole('Africa Sales Retail', null));
    	insert roleList;
    	System.assertNotEquals(roleList.get(0).Id, null);
    	System.assertNotEquals(roleList.get(1).Id, null);
    	System.assertNotEquals(roleList.get(2).Id, null);
    	System.assertNotEquals(roleList.get(3).Id, null);
    	System.assertNotEquals(roleList.get(4).Id, null);
    	System.assertNotEquals(roleList.get(5).Id, null);
    	System.assertNotEquals(roleList.get(6).Id, null);
    	
    	Id profileId = EP_CRM_TestDataFactory.getProfileIdByName('System Administrator');
    	System.assertNotEquals(profileId, null);
    	
    	List < User > userList = new List < User > ();
    	userList.add(EP_CRM_TestDataFactory.createTestUser(null, profileId, 'Sales', 'User'));
    	userList.add(EP_CRM_TestDataFactory.createTestUser(null, profileId, 'Manager', 'User'));
    	insert userList;
    	System.assertNotEquals(userList.get(0).Id, null);
    	System.assertNotEquals(userList.get(1).Id, null);
	}
	
    @isTest 
    static void testBeanDefaultValues() {
    	Test.startTest();
    		EP_CRM_PipelineReportBean bean = new EP_CRM_PipelineReportBean();
    		
    		// Check default value of opportunityObj
    		System.assertNotEquals(bean.opportunityObj, null);
    		
    		// Check default value of wrapperList
    		System.assertNotEquals(bean.wrapperList, null);
    		System.assertEquals(bean.wrapperList.size(), 0);
    		
    		// Check default value of reportDateValue
    		System.assertNotEquals(bean.reportDateValue, null);
    		System.assertEquals(bean.reportDateValue, system.today());
    		
    		// Check default value of String list of month
    		System.assertNotEquals(bean.getMonthInString(), null);
    		System.assertEquals(bean.getMonthInString().size(), 12);
    		
    		// Check default value of reportDateValue
    		System.assertNotEquals(bean.selectedDateRange, null);
    		System.assertEquals(bean.selectedDateRange, String.valueOf(system.today().year()));
    		
    		// Check default value of String list of month
    		System.assertNotEquals(bean.getDateRangeOptions(), null);
    		System.assertEquals(bean.getDateRangeOptions().size(), 5);
    		
    		// Check default value of customer segment
    		System.assertNotEquals(bean.getUoMOptions(), null);
    		System.assert(bean.getUoMOptions().size() > 0);
    		
    		// Check default value of opportunityObj
    		System.assertEquals(bean.getBitumenSelected(), false);
    		
    		// Check default value of customer segment
    		System.assertNotEquals(bean.getOpenOpportunityMap(), null);
    		System.assert(bean.getOpenOpportunityMap().size() > 0);
    		
    		// Check default value of customer segment
    		System.assertNotEquals(bean.getClosedOpportunityMap(), null);
    		System.assert(bean.getClosedOpportunityMap().size() > 0);
    		
    		// Check default value of customer segment
    		System.assertNotEquals(bean.getTotalOpenOpportunityAmountMap(), null);
    		System.assertNotEquals(bean.getTotalClosedOpportunityAmountMap(), null);
    		
    		// Check default value of customer segment
    		System.assertEquals(bean.getCurrentMonthTotalClosedAmount(), 0);
    		System.assertEquals(bean.getTotalOpenOpportunityAmount(), 0);
    		System.assertEquals(bean.getTotalClosedOpportunityAmount(), 0);
    		System.assertEquals(bean.getTotalFullYearAmount(), 0);
    	Test.stopTest();
    }
    
    @isTest 
    static void testUserAuthorization() {
    	User testUser = userMap.get('Sales User');
    	testUser.UserRoleId = roleMap.get('Global Executive').Id;
    	update testUser;
    	System.assertNotEquals(testUser, null);
    	System.assertNotEquals(testUser.Id, null);
    	
    	Test.startTest();
	    	System.runAs(testUser) {
	    		testUser = EP_CRM_DatabaseSOQL.getUserDetails();
	    		EP_CRM_PipelineReportBean bean = new EP_CRM_PipelineReportBean(testUser);
	    		System.assertEquals(bean.getUserRoleName(), 'Global Executive');
	    		System.assertEquals(bean.getRoleId(), roleMap.get('Global Executive').Id);
	    		System.assertEquals(bean.getUserHasRole(), true);
	    		System.assertEquals(bean.getUserName(), 'Sales User');
	    		System.assertEquals(bean.getGeo1(), EP_CRM_TestDataFactory.GEO1);
	    		System.assertEquals(bean.getGeo2(), EP_CRM_TestDataFactory.GEO2);
	    		System.assertEquals(bean.getRoleType(), 'Global Executive');
	    		System.assertEquals(bean.getReportHeader(), EP_CRM_Constants.GLOBAL_REPORT_HEADER);
	    		System.assertEquals(bean.getRoleSegmentValue(), EP_CRM_Constants.BLANK);
	    		System.assertEquals(bean.getRoleSegmentType(), false);
	    		System.assertEquals(bean.getAmericaUser(), false);
	    		System.assertEquals(bean.getAfricaUser(), false);
	    		System.assertEquals(bean.getAustraliaUser(), true);
	    		System.assertEquals(bean.getGlobalUser(), true);
	    		System.assertEquals(bean.getRegionalExecutiveUser(), false);
	    		System.assertEquals(bean.getCountryExecutiveUser(), false);
	    		System.assertEquals(bean.getExecutiveSalesManagerUser(), false);
	    		System.assertEquals(bean.getExecutiveManagerUser(), false);
	    		System.assertEquals(bean.getManagerUser(), false);
	    		System.assertEquals(bean.getSalesUser(), false);
	    	}
				    	
	    	// Update User with different Role
	    	testUser.UserRoleId = roleMap.get('MEAP Executive').Id; 
	    	update testUser;
    		System.assertEquals(testUser.UserRoleId, roleMap.get('MEAP Executive').Id);
    		
    		System.runAs(testUser) {
	    		testUser = EP_CRM_DatabaseSOQL.getUserDetails();
	    		EP_CRM_PipelineReportBean bean = new EP_CRM_PipelineReportBean(testUser);
	    		System.assertEquals(bean.getRoleType(), EP_CRM_Constants.REGIONAL_EXECUTIVE);
	    		System.assertEquals(bean.getReportHeader(), EP_CRM_Constants.REGIONAL_REPORT_HEADER);
	    		System.assertEquals(bean.getRoleSegmentValue(), EP_CRM_Constants.BLANK);
	    		System.assertEquals(bean.getRoleSegmentType(), false);
	    		System.assertEquals(bean.getAmericaUser(), false);
	    		System.assertEquals(bean.getAfricaUser(), false);
	    		System.assertEquals(bean.getAustraliaUser(), true);
	    		System.assertEquals(bean.getGlobalUser(), false);
	    		System.assertEquals(bean.getRegionalExecutiveUser(), true);
	    		System.assertEquals(bean.getCountryExecutiveUser(), false);
	    		System.assertEquals(bean.getExecutiveSalesManagerUser(), false);
	    		System.assertEquals(bean.getExecutiveManagerUser(), false);
	    		System.assertEquals(bean.getManagerUser(), false);
	    		System.assertEquals(bean.getSalesUser(), false);
	    	}
	    	
	    	// Update User with different Role
	    	testUser.UserRoleId = roleMap.get('Columbia Executive').Id; 
	    	update testUser;
    		System.assertEquals(testUser.UserRoleId, roleMap.get('Columbia Executive').Id);
    		
    		System.runAs(testUser) {
	    		testUser = EP_CRM_DatabaseSOQL.getUserDetails();
	    		EP_CRM_PipelineReportBean bean = new EP_CRM_PipelineReportBean(testUser);
	    		System.assertEquals(bean.getRoleType(), EP_CRM_Constants.COUNTRY_EXECUTIVE);
	    		System.assertEquals(bean.getReportHeader(), EP_CRM_Constants.COUNTRY_REPORT_HEADER);
	    		System.assertEquals(bean.getGlobalUser(), false);
	    		System.assertEquals(bean.getRegionalExecutiveUser(), false);
	    		System.assertEquals(bean.getCountryExecutiveUser(), true);
	    		System.assertEquals(bean.getExecutiveSalesManagerUser(), false);
	    		System.assertEquals(bean.getExecutiveManagerUser(), false);
	    		System.assertEquals(bean.getManagerUser(), false);
	    		System.assertEquals(bean.getSalesUser(), false);
	    	}
	    	
	    	// Update User with different Role
	    	testUser.UserRoleId = roleMap.get('Columbia Executive Sales Manager').Id; 
	    	update testUser;
    		System.assertEquals(testUser.UserRoleId, roleMap.get('Columbia Executive Sales Manager').Id);
    		
    		System.runAs(testUser) {
	    		testUser = EP_CRM_DatabaseSOQL.getUserDetails();
	    		EP_CRM_PipelineReportBean bean = new EP_CRM_PipelineReportBean(testUser);
	    		System.assertEquals(bean.getRoleType(), EP_CRM_Constants.EXECUTIVE_SALES_MANAGER);
	    		System.assertEquals(bean.getReportHeader(), EP_CRM_Constants.EXECUTIVE_SALES_MANAGER_REPORT_HEADER);
	    		System.assertEquals(bean.getGlobalUser(), false);
	    		System.assertEquals(bean.getRegionalExecutiveUser(), false);
	    		System.assertEquals(bean.getCountryExecutiveUser(), false);
	    		System.assertEquals(bean.getExecutiveSalesManagerUser(), true);
	    		System.assertEquals(bean.getExecutiveManagerUser(), false);
	    		System.assertEquals(bean.getManagerUser(), false);
	    		System.assertEquals(bean.getSalesUser(), false);
	    	}
	    	
	    	// Update User with different Role
	    	testUser.UserRoleId = roleMap.get('Columbia Executive Manager Retail').Id; 
	    	update testUser;
    		System.assertEquals(testUser.UserRoleId, roleMap.get('Columbia Executive Manager Retail').Id);
    		
    		System.runAs(testUser) {
	    		testUser = EP_CRM_DatabaseSOQL.getUserDetails();
	    		EP_CRM_PipelineReportBean bean = new EP_CRM_PipelineReportBean(testUser);
	    		System.assertEquals(bean.getRoleType(), EP_CRM_Constants.EXECUTIVE_MANAGER);
	    		System.assertEquals(bean.getReportHeader(), EP_CRM_Constants.EXECUTIVE_MANAGER_REPORT_HEADER);
	    		System.assertEquals(bean.getGlobalUser(), false);
	    		System.assertEquals(bean.getRegionalExecutiveUser(), false);
	    		System.assertEquals(bean.getCountryExecutiveUser(), false);
	    		System.assertEquals(bean.getExecutiveSalesManagerUser(), false);
	    		System.assertEquals(bean.getExecutiveManagerUser(), true);
	    		System.assertEquals(bean.getManagerUser(), false);
	    		System.assertEquals(bean.getSalesUser(), false);
	    	}
	    	
	    	// Update User with different Role
	    	testUser.UserRoleId = roleMap.get('Columbia Manager HighStreet').Id; 
	    	testUser.EP_CRM_Geo1__c = EP_CRM_Constants.AMERICAS;
	    	update testUser;
    		System.assertEquals(testUser.UserRoleId, roleMap.get('Columbia Manager HighStreet').Id);
    		
    		System.runAs(testUser) {
	    		testUser = EP_CRM_DatabaseSOQL.getUserDetails();
	    		EP_CRM_PipelineReportBean bean = new EP_CRM_PipelineReportBean(testUser);
	    		System.assertEquals(bean.getRoleType(), EP_CRM_Constants.SALES_MANAGER);
	    		System.assertEquals(bean.getReportHeader(), EP_CRM_Constants.MANAGER_REPORT_HEADER);
	    		System.assertEquals(bean.getRoleSegmentValue(), EP_CRM_Constants.HIGH_STREET);
	    		System.assertEquals(bean.getRoleSegmentType(), true);
	    		System.assertEquals(bean.getAmericaUser(), true);
	    		System.assertEquals(bean.getAfricaUser(), false);
	    		System.assertEquals(bean.getAustraliaUser(), false);
	    		System.assertEquals(bean.getGlobalUser(), false);
	    		System.assertEquals(bean.getRegionalExecutiveUser(), false);
	    		System.assertEquals(bean.getCountryExecutiveUser(), false);
	    		System.assertEquals(bean.getExecutiveSalesManagerUser(), false);
	    		System.assertEquals(bean.getExecutiveManagerUser(), false);
	    		System.assertEquals(bean.getManagerUser(), true);
	    		System.assertEquals(bean.getSalesUser(), false);
	    	} 	
	    	
	    	// Update User with different Role
	    	testUser.UserRoleId = roleMap.get('Africa Sales Retail').Id;  
	    	testUser.EP_CRM_Geo1__c = EP_CRM_Constants.AFRICA;
	    	update testUser;
    		System.assertEquals(testUser.UserRoleId, roleMap.get('Africa Sales Retail').Id);
    		
    		System.runAs(testUser) {
	    		testUser = EP_CRM_DatabaseSOQL.getUserDetails();
	    		EP_CRM_PipelineReportBean bean = new EP_CRM_PipelineReportBean(testUser);
	    		System.assertEquals(bean.getRoleType(), EP_CRM_Constants.SALES_USER);
	    		System.assertEquals(bean.getReportHeader(), EP_CRM_Constants.SALES_REPORT_HEADER);
	    		System.assertEquals(bean.getRoleSegmentValue(), EP_CRM_Constants.RETAIL);
	    		System.assertEquals(bean.getRoleSegmentType(), true);
	    		System.assertEquals(bean.getAmericaUser(), false);
	    		System.assertEquals(bean.getAfricaUser(), true);
	    		System.assertEquals(bean.getAustraliaUser(), false);
	    		System.assertEquals(bean.getGlobalUser(), false);
	    		System.assertEquals(bean.getRegionalExecutiveUser(), false);
	    		System.assertEquals(bean.getCountryExecutiveUser(), false);
	    		System.assertEquals(bean.getExecutiveSalesManagerUser(), false);
	    		System.assertEquals(bean.getExecutiveManagerUser(), false);
	    		System.assertEquals(bean.getManagerUser(), false);
	    		System.assertEquals(bean.getSalesUser(), true);
	    	}
	    	
	    	// Update User with No Role
	    	testUser.UserRoleId = null; 
	    	update testUser;
    		System.assertEquals(testUser.UserRoleId, null);
    		
    		System.runAs(testUser) {
	    		testUser = EP_CRM_DatabaseSOQL.getUserDetails();
	    		EP_CRM_PipelineReportBean bean = new EP_CRM_PipelineReportBean(testUser);
	    		System.assertEquals(bean.getRoleType(), EP_CRM_Constants.BLANK);
	    		System.assertEquals(bean.getRoleId(), null);
	    		System.assertEquals(bean.getReportHeader(), EP_CRM_Constants.NA);
	    		System.assertEquals(bean.getRoleSegmentValue(), EP_CRM_Constants.BLANK);
	    		System.assertEquals(bean.getRoleSegmentType(), false);
	    		System.assertEquals(bean.getAmericaUser(), false);
	    		System.assertEquals(bean.getAfricaUser(), true);
	    		System.assertEquals(bean.getAustraliaUser(), false);
	    		System.assertEquals(bean.getGlobalUser(), false);
	    		System.assertEquals(bean.getRegionalExecutiveUser(), false);
	    		System.assertEquals(bean.getCountryExecutiveUser(), false);
	    		System.assertEquals(bean.getExecutiveSalesManagerUser(), false);
	    		System.assertEquals(bean.getExecutiveManagerUser(), false);
	    		System.assertEquals(bean.getManagerUser(), false);
	    		System.assertEquals(bean.getSalesUser(), false);
	    	}
    	Test.stopTest();
    }
}