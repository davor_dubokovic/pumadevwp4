/* 
@Author      CloudSense
@name        ROSchedulingBatch
@CreateDate  30/04/2018
@Description Batch to schedule the EP_RetrospectiveOrderBatch batch dynamically
@Version     1.0
*/ 
global class ROSchedulingBatch implements Schedulable {
	global void execute(SchedulableContext sc) {
		scheduleBatchs();
	}

	/* @name scheduleBatchs
     * @date  30/04/2018
     * @description method that schedules the EP_RetrospectiveOrderBatch jobs
     * @param NA
     * @return List of Id of created jobs
    */
	global List<Id> scheduleBatchs(){
		List<Id> scheduledJobs = new List<Id>();
		Integer batchSize = getSanitizedInteger(CS_RO_Processing_Settings__c.getOrgDefaults().Batch_Size__c, 1, 200);
		Integer executions_per_day = getSanitizedInteger(CS_RO_Processing_Settings__c.getOrgDefaults().Executions_Per_Day__c, 1, 24);

		for(Integer minutesFromNow : getMinutesFromNowList(executions_per_day)){
			Id jobId = scheduleBatch(minutesFromNow, batchSize);
			scheduledJobs.add(jobId);
		}

		return scheduledJobs;
	}

	/* @name scheduleBatch
     * @date  30/04/2018
     * @description method that schedules one instance of EP_RetrospectiveOrderBatch
     * @param Integer minutesFromNow, Integer batchSize
     * @return Id of created jobs
    */
	global Id scheduleBatch(Integer minutesFromNow, Integer batchSize){
		return System.scheduleBatch(new EP_RetrospectiveOrderBatch(), 
											'EP_RetrospectiveOrderBatch ' + minutesFromNow, 
											minutesFromNow, 
											batchSize);
	}

	/* @name getSanitizedInteger
     * @date  30/04/2018
     * @description method that handles the limits of integer values
     * @param Decimal value, Integer min, Integer max
     * @return Integer value already sanitized
    */
	private Integer getSanitizedInteger(Decimal value, Integer min, Integer max){
		Integer intValue = min;

		if (value == null || value.intValue() < min){
			intValue = min;
		}else if (value.intValue() > max){
			intValue = max;
		}else{
			intValue = value.intValue();
		}

		return intValue;
	}

	/* @name getMinutesFromNowList
     * @date  30/04/2018
     * @description method that calculates the interval, in minutes, to schedule jobs
     * @param Integer executions_per_day
     * @return List Integers with the number of minutes that divide the day equally by the number of executions per day
    */
	private Integer[] getMinutesFromNowList(Integer executions_per_day){
		Integer[] minutesFromNowList = new Integer[executions_per_day];

		Integer interval = 24 / executions_per_day;

		for(Integer index = 0; index < executions_per_day; index++){
			minutesFromNowList[index] =  interval * 60 * index;
		}

		return minutesFromNowList;
	}
}