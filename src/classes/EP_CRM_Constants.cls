/* ================================================
 * @Class Name : EP_CRM_Constants 
 * @author : TCS
 * @Purpose: This constants class is used to store all constants like message information, picklist strings, conversion rates and role names for opportunity volume report.
 * @created date: 15/11/2017
 ================================================*/
public with sharing class EP_CRM_Constants {
    public static final String PIPELINE_REPORT = 'Pipeline Report';
    public static final String ADOPTION_REPORT = 'Adoption Rate Report';
    public static final String GLOBAL_EXECUTIVE = 'Global Executive';
    public static final String EXECUTIVE_SALES_MANAGER = 'Executive Sales Manager';
    public static final String EXECUTIVE_MANAGER = 'Executive Manager';
    public static final String EXECUTIVE = 'Executive';
    public static final String REGIONAL_EXECUTIVE = 'Regional Executive';
    public static final String COUNTRY_EXECUTIVE = 'Country Executive';
    public static final String SALES_MANAGER = 'Manager';
    public static final String SALES_USER = 'Sales';
    public static final String MEAP = 'MEAP';
    public static final String AFRICA = 'AFRICA';
    public static final String US = 'US';
    public static final String AMERICAS = 'Americas';
    public static final String HIGHSTREET = 'HighStreet';
    public static final String HIGH_STREET = 'High Street';
    public static final String FUELS = 'Fuels';
    public static final String FUEL_CARD = 'Fuel Card';
    public static final String CHEMICAL_PRODUCTS = 'Chemical Products';
    public static final String LPG = 'LPG';
    public static final String NON_FUELS = 'Non-Fuels';
    public static final String LUBRICANTS = 'Lubricants';
    public static final String RETAIL = 'Retail';
    public static final String BITUMEN = 'Bitumen';
    public static final String GLOBAL_REPORT_HEADER = 'Global Report for COO';
    public static final String REGIONAL_REPORT_HEADER = 'Regional Report for COO';
    public static final String COUNTRY_REPORT_HEADER = 'Country GM Report';
    public static final String EXECUTIVE_SALES_MANAGER_REPORT_HEADER = 'Executive Sales Manager Report';
    public static final String EXECUTIVE_MANAGER_REPORT_HEADER = 'Executive Manager Report';
    public static final String MANAGER_REPORT_HEADER = 'Manager Report';
    public static final String SALES_REPORT_HEADER = 'Sales User Report';
    public static final String NA = 'NA';
    public static final Decimal ONE = 1.0;
    public static final Decimal KG_PER_LITRE = 1.0;
    public static final Decimal GALLON_PER_LITRE = 0.26417205;
    public static final Decimal CUBES_PER_LITRE = 0.001;
    public static final Decimal LITRE_PER_KG = 1.0;
    public static final Decimal METRIC_TON_PER_KG = 0.001;
    public static final Decimal KG_PER_METRIC_TON = 1000;
    public static final Decimal LITRE_PER_GALLON = 3.7854;
    public static final Decimal KG_PER_GALLON = 3.7854;
    public static final Decimal METRIC_TON_PER_GALLON = 0.0038;
    public static final Decimal CUBES_PER_GALLON = 0.0038;
    public static final Decimal LITRE_PER_CUBES = 1000;
    public static final Decimal GALLON_PER_CUBES = 264.172052;
    public static final String LITRE = 'Litre';
    public static final String KILOGRAM = 'Kilogram';
    public static final String CUBIC_MTR = 'Cubic Mtr';
    public static final String CUBIC_LT = 'Cubic LT';
    public static final String GALLON = 'Gallon';
    public static final String METRIC_TON = 'Metric Ton';
    public static final String UNIT = 'Unit';
    public static final String DRUM = 'Drum';
    public static final String GRAM = 'Gram';
    public static final String MASS = 'Bulk';
    public static final String METRE = 'Metre';
    public static final String BARREL = 'Barrel';
    public static final String BLANK = '';
    public static final String NONE = '--None--';
    public static final String NO_ROLE_ASSIGNED_ERROR = 'User does not have any role assigned. Kindly contact System Administrator.';
    public static final String UOM_VALUE_MISSING_ERROR = 'UoM Field must be selected.';
    public static final String MULTIPLE_PRODUCTS_WITH_DIFFERENT_UOM_ERROR = 'UoM cannot be set for multiple products with different UoM default values.';
}