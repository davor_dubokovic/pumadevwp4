/*
 * @ Description : Helper class for USER Trigger
 * @ class Name  : EP_UserTriggerHelper_R1
 */
public with sharing class EP_UserTriggerHelper_R1 {
    private static final string CLOSE_BRACKET = ')';
    /*
     * this method returns map of Timezone from user's timezone field
     */
    private static Map<String, String> timeZoneMap {
        get {
            if (timeZoneMap == NULL) {
                timeZoneMap = new Map<String, String>();
                Schema.DescribeFieldResult fieldResult = User.TimeZoneSidKey.getdescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for(Schema.PicklistEntry p : ple) {
                    timeZoneMap.put(p.getValue(), p.getlabel());
                }
            }
            return timeZoneMap;
        }
        set;
    }  
    /*
     * this method calculate user's timeZone offset from Timezone
     */
    public static void calculateUserTimeZoneUTCOffset(List<User> lUsers) {
        String strTimeZone;
        Double dblOffset;        
        for (User u : lUsers) {           
            strTimeZone = timeZoneMap.get(u.TimeZoneSidKey);            
            if (strTimeZone.indexOf(CLOSE_BRACKET) > -1) {
                strTimeZone = strTimeZone.substring(1, 10);
            }            
            // Extract the offset from the timezone side key
            dblOffset = EP_PortalLibClass_R1.calcualteUTCOffsetFromTimezone(strTimeZone);
            u.EP_User_UTC_Offset__c = dblOffset;           
        } // End for       
    }   
}