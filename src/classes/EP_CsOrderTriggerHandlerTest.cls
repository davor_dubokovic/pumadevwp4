/*
@Author      CloudSense
@name        EP_CsOrderTrigger
@CreateDate  15/01/2018
@Description CS Order Trigger
@Version     1.0
*/

@isTest
private class EP_CsOrderTriggerHandlerTest {

	@isTest
    static void contractOrderUpdate_test() {
		//Get the Id of the Record Type: Contract of the Order custom object
		String contractId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Contract').getRecordTypeId();
		
		//1) Insert test Account:
		Account acc = new Account(Name = 'testAccount', billingCountry = 'US');
		INSERT acc;
		
		//2) Insert an Order (Record Type: Contract):
		csord__Order__c order = new csord__Order__c();
		//Link the Order to the inserted Account:
		order.csord__Account__c = acc.Id;
		//Give this Order the Record Type: 'Contract'
		order.RecordTypeId = contractId;
		order.csord__Identification__c = 'spokeServiceAmsterdam';
		INSERT order;
		
		//3) Insert another Order, this time is a DrawDown Order:
		// Use method from EP_TestDataUtility, which inserts a record of Order and an Order Line Item:
		// It also inserts a record of the Basket object (which is indifferent for this test case)
		List<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
		//Select an Order from the Wrapper:
		csord__Order__c drawDownOrder = orderWrapperList[0].SFOrder;
		//Link the DrawDown Order to the Order 'Contract':
		drawDownOrder.csord__Primary_Order__c = order.Id;
		UPDATE drawDownOrder;
		
		//4) Select inserted Order (Record Type: 'Contract') and UPDATE it to activate the trigger:
		List<csord__Order__c> orderList = [SELECT Id, RecordTypeId, csord__Primary_Order__c, EP_Delivered_Quantity__c FROM csord__Order__c WHERE RecordTypeId = :contractId];
		UPDATE orderList[0];
		
		Test.startTest();
			//Select inserted Order Line Item Order to get the 'Quantity' field:
			List<csord__Order_Line_Item__c> orderLineItemList = [SELECT Id, Name, csord__Order__c, Quantity__c FROM csord__Order_Line_Item__c];

			//Select updated Order (Record Type: 'Contract') and assert if the 'Delivered Quantity' field was populated:
			List<csord__Order__c> orderList2 = [SELECT Id, RecordTypeId, csord__Primary_Order__c, EP_Delivered_Quantity__c FROM csord__Order__c WHERE RecordTypeId = :contractId];
		
			System.assertEquals(orderLineItemList[0].Quantity__c, orderList2[0].EP_Delivered_Quantity__c , 'The Delivered Quantity in the Order (Contract) ' +
																'should be the equal to the SUM of the Quantity field in all Order Line Items');
		Test.stopTest();
    }
	
	@isTest
    static void contractOrderUpdate_testDeleteOrder() {
		//Get the Id of the Record Type: Contract of the Order custom object
		String contractId = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Contract').getRecordTypeId();
		
		//1) Insert test Account:
		Account acc = new Account(Name = 'testAccount', billingCountry = 'US');
		INSERT acc;
		
		//2) Insert an Order (Record Type: Contract):
		csord__Order__c order = new csord__Order__c();
		//Link the Order to the inserted Account:
		order.csord__Account__c = acc.Id;
		//Give this Order the Record Type: 'Contract'
		order.RecordTypeId = contractId;
		order.csord__Identification__c = 'spokeServiceAmsterdam';
		INSERT order;
		
		//3) Insert another Order, this time is a DrawDown Order:
		// Use method from EP_TestDataUtility, which inserts a record of Order and an Order Line Item:
		// It also inserts a record of the Basket object (which is indifferent for this test case)
		List<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
		//Select an Order from the Wrapper:
		csord__Order__c drawDownOrder = orderWrapperList[0].SFOrder;
		//Link the DrawDown Order to the Order 'Contract':
		drawDownOrder.csord__Primary_Order__c = order.Id;
		UPDATE drawDownOrder;
		
		//4) DELETE inserted DrawDown Order to activate the trigger:
		// (This will also delete the Order Line Item linked to the DrawDown Order)
		List<csord__Order__c> drawDownOrderList = [SELECT Id, RecordTypeId, csord__Primary_Order__c FROM csord__Order__c WHERE RecordTypeId != :contractId];
		DELETE drawDownOrderList;

		Test.startTest();
			//Select updated Order (Record Type: 'Contract') and assert if the 'Delivered Quantity' field was populated:
			List<csord__Order__c> orderList = [SELECT Id, RecordTypeId, csord__Primary_Order__c, EP_Delivered_Quantity__c FROM csord__Order__c WHERE RecordTypeId = :contractId];
			
			System.assertEquals(NULL, orderList[0].EP_Delivered_Quantity__c, 'The Delivered Quantity in the Order (Contract) ' +
																			'should be the equal to the SUM of the Quantity field in all Order Line Items');
		Test.stopTest();
	}
	
}