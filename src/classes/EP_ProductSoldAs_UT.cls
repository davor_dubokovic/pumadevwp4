@isTest
private class EP_ProductSoldAs_UT{

    static testMethod void getShipTos_test() {
        EP_ProductSoldAs localObj = new EP_ProductSoldAs();
        List<Account> lstAccount = [Select recordtype.developerName from Account where id =: EP_TestDataUtility.getShipTo().Id];
        Test.startTest();
        LIST<Account> result = localObj.getShipTos(lstAccount);
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
    
    static testMethod void getTanksForBulkProduct_test() {
        EP_ProductSoldAs localObj = new EP_ProductSoldAs();
        Map<Id,EP_Tank__c> mapOperationalTanks = new Map<Id,EP_Tank__c>([SELECT Id,EP_Ship_To__c,EP_Product__r.EP_Product_Sold_As__c FROM EP_Tank__c WHERE EP_Ship_To__c=:EP_TestDataUtility.getShipToPositiveScenario().id]);
        Test.startTest();
        Map<Id,EP_Tank__c> result = localObj.getTanksForBulkProduct(mapOperationalTanks);
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
 }