/* 
  @Author <Accenture>
   @name <EP_TimepickerComponentController2>
   @CreateDate <17/12/2015>
   @Description <This class return the hours and minutes as a list>
   @Version <1.0>
 
*/
public with sharing class EP_TimepickerComponentController2 {
    
    /*
    @Description <This method return the hours as a list>
    */
    public List<SelectOption> getHours() {
        List<SelectOption> options = new List<SelectOption>();
        String strHour;
        SelectOption sOpts;
        // Default value is blank
        options.add(new SelectOption(EP_Common_Constant.BLANK,EP_Common_Constant.BLANK));
        try{
            for(Integer i=0; i<24; i++) {    
                strHour = String.valueof(i);
                if (strHour.length() == 1) strHour = EP_Common_Constant.ZERO_STRING + strHour;
                sOpts =  new SelectOption(String.valueof(i), strHour);
                options.add(sOpts);
            }
        }
        catch (exception exp){
                string str= (exp.getMessage());  
        }
        return options;
    }
    
    /*
    @Description <This method return the minutes as a list>
    */
    public List<SelectOption> getMinutes() {
        List<SelectOption> options = new List<SelectOption>();
        String strMinute;
        SelectOption sOpts;
        
        // Default value is blank
        options.add(new SelectOption(EP_Common_Constant.BLANK,EP_Common_Constant.BLANK));
        try{
            for(integer i=0; i<60; i++) { 
                strMinute = String.valueof(i);
                if (strMinute.length() == 1) strMinute = EP_Common_Constant.ZERO_STRING + strMinute;  
                sOpts =  new SelectOption(String.valueof(i), strMinute);
                options.add(sOpts);
            }
        }
        catch (exception exp){
                string str= (exp.getMessage());  
         }
        return options;
    }
    
    /*
     @Description This routine is used to assign the new hour/minute value to the variables 
                   when user changes the selected time
    */
    public void updateTime(){
       
    }
    
}