/*********************************************************************************************
@Author <Prayank Sahu>
@name <EP_FE_PowerBIResponse >
@CreateDate <01/11/2018>
@Description <This class will provide AccessToken,Embed Token and Refresh token to get Report from Power BI >  
@Version <1.0>
*********************************************************************************************/

global with sharing class EP_FE_PowerBIResponse extends EP_FE_Response {
    
    
    
    
    /** The JSON result from a successful oauth call */
    
    /** The access token */
    public String context {get; set;}
    
    /** The refresh token */
    public String token{get; set;}
    
    /** The token expiry date*/
    public String tokenId{get;set;}
    
    public String expiration{get;set;}
    
    public String PowerBIURL{get;set;}
    public String GROUPID{get;set;}
    public String REPORTID{get;set;}
    
    //Error Logging Methode Parameters
    global static final String  CLASS_NAME = 'EP_FE_PowerBIEndpoint'; 
    global static final String  METHOD_GETACCESSTOKEN = 'getAccessToken';
    global static final String  METHOD_GENERATEEMBEDTOKEN = 'generateEmbedToken';
    global static final String  ERROR ='Error'; 
    global static final String  AREA = 'POWERBIAPI';
    global static final String  FUNCTION = 'Generate embed token from PowerBI API';
    global static final String  FUNCTIONACCESSTOKEN = 'Get Access token';
    global static final String  ISSUE = 'Raised exception';
    global static final Integer ERROR_FETCHING_PowerBIResponse = -1; //Error in fetching access token
    

}