@isTest
public class EP_OAuthControllerTest {
    
    public static PageReference pageRef = Page.EP_PowerBI_Report;
    public static EP_OAuthApp_pbi__c app;
    public static EP_OAuthController controller = new EP_OAuthController();
    
    @testSetup public static void setUp()
    {   
        app = new EP_OAuthApp_pbi__c();
        app.Name = 'PowerBI2';
        app.EP_Token_Expires_On__c = '0';
        app.EP_Client_Id__c = 'clientId';
        app.EP_Client_Secret__c = 'clientSecret';
        app.EP_Authorization_URL__c = 'https://login.windows.net/common/oauth2/authorize';
        app.EP_Access_Token_URL__c = 'https://login.microsoftonline.com/common/oauth2/token';
        app.EP_Resource_URI__c = 'https://analysis.windows.net/powerbi/api';
        insert app;
        
        controller.application_name = 'PowerBI2';
    }
    
    public static testMethod void createController()
    {       
        System.assertNotEquals(controller, null);       
    }   
    
    public static testMethod void checkAccessTokenNotNull()
    {
        System.assertNotEquals(controller.PBIAccess_token, null);   
    }
    
    public static testMethod void checkRefreshTokenNotNull()
    {
        System.assertNotEquals(controller.PBIRefresh_token, null);  
    }
    
    public static testMethod void checkExpiresOnNotNull()
    {
        System.assertNotEquals(controller.PBIExpires_on, null); 
    }
    
    public static testMethod void checkHasTokenReturnsFalse()
    {
        System.assertEquals(controller.getHasToken(), false);   
    }
    
    public static testMethod void checkHasTokenReturnsTrue()
    {
        controller.PBIAccess_token =  'testToken';
        System.assertEquals(controller.getHasToken(), false);   
    }
    
    public static testMethod void getAuthURLReturnSuccess()
    {
        Test.setCurrentPage(pageRef);
        String authUrl = controller.getAuthUrl();
        
        System.assertEquals(authUrl.contains('https://login.windows.net/common/oauth2/authorize?'), true);
    }
    
    public static testMethod void redirectOnCallbackCreatesCookies()
    {   
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new EP_MockOAuthResponseGenerator());
        
        EP_OAuthControllerTest.getAuthURLReturnSuccess();
        pageRef = new PageReference('https://pumaenergyapac--gdevap04--c.cs57.visual.force.com/apex/EP_PowerBI_Report?code=testCode');
        Test.setCurrentPage(pageRef);
        controller.isCallback = true;

        PageReference ref = controller.redirectOnCallback(pageRef);
        
        String accessCookie = controller.PBIAccess_token;
        String refreshCookie =  controller.PBIRefresh_token;
        
        System.assertEquals('accessCookieToken',accessCookie);
        System.assertEquals('refreshCookieToken',refreshCookie);
        Test.stopTest();
    }

    public static testMethod void refreshToken()
    {   
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new EP_MockOAuthResponseGenerator());
        
        EP_OAuthControllerTest.getAuthURLReturnSuccess();
        pageRef = new PageReference('https://pumaenergyapac--gdevap04--c.cs57.visual.force.com/apex/EP_PowerBI_Report?code=testCode');
        Test.setCurrentPage(pageRef);
        controller.isCallback = true;

        controller.refreshAccessToken(pageRef);
        
        String accessCookie = controller.PBIAccess_token;
        String refreshCookie =  controller.PBIRefresh_token;
        
        System.assertEquals('accessCookieToken',accessCookie);
        System.assertEquals('refreshCookieToken',refreshCookie);
        Test.stopTest();
    }
}