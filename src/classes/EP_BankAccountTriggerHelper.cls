/**
 * @Author <Ashok Arora>
 * @Name <EP_BankAccountTriggerHelper>
 * @CreateDate <28/10/2015>
 * @Description <This apex class handles requests coming from EP_BankAccountTriggerHandler as a part of Trigger Framework>
 * @Version <1.0>
 */
public with sharing class EP_BankAccountTriggerHelper{
    
    private static final String CLASSNAME = 'EP_BankAccountTriggerHelper';
    private static final String DO_ACTIVE_SYNC_BANK_ACCOUNT = 'doActiveSyncBankAccount';
    private static final String VALIDATE_IBAN_MTD = 'validateIBAN';
    private static final String POPULATE_NAV_BANK_ACC_CODE_FLD = 'populateNAVBankAccountCodeField';
    private static final String RESTRICT_USER_TO_UPSERT_BANK_ACC = 'restrictUserTOUPSERTBankAccount';
    
    /**
     * @author <Ashok Arora>
     * @description <THIS METHOD CHANGES STATUS OF BANK TO ACTIVE AFTER SYNC WITH NAV>
     * @name <doActiveSyncBankAccount>
     * @date <28/10/2015>
     * @param Map<Id,EP_Bank_Account__c>, Map<Id,EP_Bank_Account__c>
     * @return void
     */
    public static void doActiveSyncBankAccount(Map<Id,EP_Bank_Account__c> mapIdOldBankAccount ,Map<Id,EP_Bank_Account__c> mapIdNewBankAccount){
        try{
            for(Id bankAccountId : mapIdNewBankAccount.keySet()){
                if(EP_Common_Constant.SENT_STATUS.equalsIgnoreCase(mapIdOldBankAccount.get(bankAccountId).EP_Integration_Status__c)
                        && EP_Common_Constant.SYNC_STATUS.equalsIgnoreCase(mapIdNewBankAccount.get(bankAccountId).EP_Integration_Status__c)){
                    mapIdNewBankAccount.get(bankAccountId).EP_Bank_Account_Status__c = EP_COMMON_CONSTANT.BANK_STATUS_ACTIVE;
                }
            }
        }catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, DO_ACTIVE_SYNC_BANK_ACCOUNT, CLASSNAME, ApexPages.Severity.ERROR );
        }
    }
    
    /**
     * @author <Ashok Arora>
     * @description <This method is used to restict user either add new Account record or modify existing Account record>
     * @name <restrictUserTOUPSERTBankAccount>
     * @date <28/10/2015>
     * @param List<EP_Bank_Account__c>
     * @return void
     */
    public static void restrictUserTOUPSERTBankAccount(List<EP_Bank_Account__c> listNewAccounts){
        Profile userProfile = [Select id, Name from Profile where Id=: UserInfo.getProfileId() limit 1];
        Set<Id> setAccountIds= new Set<Id>();
        try{ 
            for (EP_Bank_Account__c bankAccount : listNewAccounts){
                setAccountIds.add(bankAccount.EP_Account__c);
            }
            if(!setAccountIds.isEmpty()){
                Integer statusNum;
                
                Map<Id,Account> mapIdAccount = new Map<ID, Account>([Select Id, EP_Status__C from Account 
                WHere id in : setAccountIds limit :EP_Common_Util.getQueryLimit()]);
                for(EP_Bank_Account__c bankAccount: listNewAccounts){
                    if(mapIdAccount.containsKey(bankAccount.EP_Account__c) 
                        && String.isNotBlank(mapIdAccount.get(bankAccount.EP_Account__c).EP_Status__c)){
                        statusNum = Integer.valueOf(mapIdAccount.get(bankAccount.EP_Account__c).EP_Status__c.substring(0,2));
                        if(!(EP_Common_Constant.INTEG_USER_PROFILE.equalsIgnoreCase(userProfile.Name) 
                             || EP_Common_Constant.ADMIN_PROFILE.equalsIgnoreCase(userProfile.Name)) && statusNum > 4){
                            bankAccount.addError(system.label.EP_Bank_Account_Edit_Validation);
                        }
                    }
                }
            }
        }catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, RESTRICT_USER_TO_UPSERT_BANK_ACC, CLASSNAME, ApexPages.Severity.ERROR); 
        }
    }
    
    /**
     * @author <Ashok Arora>
     * @description <This method is used to populate NAV Bank Account Code Field>
     * @name <populateNAVBankAccountCodeField>
     * @date <28/10/2015>
     * @param List<EP_Bank_Account__c>
     * @return void
     */
    public static void populateNAVBankAccountCodeField(List<EP_Bank_Account__c> listNewAccounts){
        Set<Id> setAccountIds = new Set<Id>();
        for(EP_Bank_Account__c bankAccount : listNewAccounts){
            setAccountIds.add(bankAccount.EP_Account__c);
        }
        List<EP_Bank_Account__c> listOfBankAccountsToBeUpdated = new List<EP_Bank_Account__c>();
        try{
            if(!setAccountIds.isEmpty()){
                /**
                * Changed By : Amit Singh , Date : 12-Oct-2016
                * Added Composit Id field in Query,
                * To Create Unique ID now Composit ID will be used
                */
                Map<Id, Account> mapIdAccount = new Map<Id, Account>([Select id,Name, AccountNumber, EP_Composite_Id__c 
                from Account Where id in :setAccountIds limit :EP_Common_Util.getQueryLimit()]);
                EP_Bank_Account__c bankAccountObject;
                for(EP_Bank_Account__c bankAccount : listNewAccounts){
                	//L4 #58828 & #58829 Code Changes Start
                    if(mapIdAccount.containsKey(bankAccount.EP_Account__c) && String.isBlank(bankAccount.EP_NAV_Bank_Account_Code__c)){
                        bankAccountObject = new EP_Bank_Account__c(id = bankAccount.Id,
                        EP_NAV_Bank_Account_Code__c = mapIdAccount.get(bankAccount.EP_Account__c).EP_Composite_Id__c +EP_Common_Constant.STRING_HYPHEN+bankAccount.Name); 
                        listOfBankAccountsToBeUpdated.add(bankAccountObject);
                    }
                    //L4 #58828 & #58829 Code Changes End
                }
                Database.update(listOfBankAccountsToBeUpdated, false);
            }
        }catch(Exception e){
            EP_LoggingService.logHandledException(e, EP_Common_Constant.EPUMA,POPULATE_NAV_BANK_ACC_CODE_FLD, CLASSNAME, ApexPages.Severity.ERROR);
        }
    }
    
    /**
     * @author <Ashok Arora>
     * @description <THIS METHOD VALIDATES IBAN USING SEPA TOOLKIT>
     * @name <validateIBAN>
     * @date <28/10/2015>
     * @param List<EP_Bank_Account__c>
     * @return void
     */
    public static void validateIBAN(List<EP_Bank_Account__c> listNewBankAccounts){
        try{
            for(EP_Bank_Account__c bankAccount : listNewBankAccounts){
                //IF IBAN IS NOT BLANK THEN VALIDATE USING SEPA TOOLKIT
                if(String.isNotBlank(bankAccount.EP_IBAN__c)){
                    //REMOVE WHITESPACES FROM IBAN
                    bankAccount.EP_IBAN__c = bankAccount.EP_IBAN__c.deleteWhiteSpace();
                    if(!SEPA_Toolkit.SEPAUtilities.ValidateIBAN(bankAccount.EP_IBAN__c)){
                        //IF INVALID THEN THROW ERROR
                        bankAccount.addError(Label.EP_Invalid_IBAN_Err);
                    }
                }
            }
        }catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA,VALIDATE_IBAN_MTD, CLASSNAME, ApexPages.Severity.ERROR);
        }
    }
}