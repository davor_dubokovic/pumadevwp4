@isTest 
private class EP_CRM_SelectCustomReportControllerTest {
    @isTest 
    static void testRedirectToReport() {
        Test.setCurrentPage(Page.SelectCustomReportPage);
                
        Test.startTest();
            EP_CRM_SelectCustomReportController controller = new EP_CRM_SelectCustomReportController();
            PageReference pref = controller.redirectToReport();
            System.assertEquals(pref, null);
            
            ApexPages.currentPage().getParameters().put('reportName', EP_CRM_Constants.PIPELINE_REPORT);
            pref = controller.redirectToReport();
            System.assertNotEquals(pref, null);
            System.assertEquals(pref.getUrl(), '/apex/pipelinereportpage');
        Test.stopTest();
    }  
    
    @isTest 
    static void testRedirectToAdoptionReport() {
        Test.setCurrentPage(Page.SelectCustomReportPage);
                
        Test.startTest();
            EP_CRM_SelectCustomReportController controller = new EP_CRM_SelectCustomReportController();
            PageReference pref = controller.redirectToAdoptionReport();
            System.assertEquals(pref, null);
            
            ApexPages.currentPage().getParameters().put('reportName', EP_CRM_Constants.ADOPTION_REPORT);
            pref = controller.redirectToAdoptionReport();
            System.assertNotEquals(pref, null);
            System.assertEquals(pref.getUrl(), '/apex/adoptionratereport');
        Test.stopTest();
    }    
    
}