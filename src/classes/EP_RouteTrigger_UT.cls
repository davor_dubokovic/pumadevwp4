@isTest
public class EP_RouteTrigger_UT
{   
    static testMethod void beforeUpdate_test() {
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(5);
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,routeRecords);
        EP_RouteService routeService = new EP_RouteService(routeDomObj);
        List<EP_Route__c> routeList = new List<EP_Route__c>();
        for(EP_Route__c route : routeRecords.values()){
            route.EP_Route_Name__c = 'test'+route.EP_Route_Name__c;
            routeList.add(route);
        }
        Test.startTest();
        update routeList;
        Test.stopTest();
        
        // This delegates to another services so no assert required.
        System.assert(true);
    }
    
    static testMethod void beforeDelete_test() {
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(5);
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords);
        EP_RouteService routeService = new EP_RouteService(routeDomObj);
        List<EP_Route__c> routeList = new List<EP_Route__c>();
        for(EP_Route__c route : routeRecords.values()){
            routeList.add(route);
        }
        Test.startTest();
        delete routeList;
        Test.stopTest();
        
        // This delegates to another services so no assert required.
        System.assert(true);
    
    }
    
 }