/**
    @Author         Accenture
    @Name           EP_ContractMapper 
    @Createdate     12/21/2016 
    @Description    This class contains all SOQLs related to Contract Object
    @Version        1.0
    @Reference      NA
*/ 
public with sharing class EP_ContractMapper {    
    
    /**
    * @author       Accenture
    * @name         getRecordsByContractNumber
    * @date         04/16/2018
    * @description  This method will be used to get Records By Set of Contract Number
    * @param        set<string>
    * @return       list<Contract>
    */ 
    public list<Contract> getRecordsByContractNumber(set<string> contractUniqueKeySet) {
    	return [select Id, EP_Contract_Number__c from Contract where EP_Contract_Number__c IN : contractUniqueKeySet];
    }
    
    /**
    * @author       Accenture
    * @name         mapContractNumber
    * @date         04/16/2018
    * @description  This method will be used to get map of Records By Set of Contract Number
    * @param        set<string>
    * @return       list<Contract>
    */ 
    public map<string, Contract> mapContractNumber(set<string> contractUniqueKeySet) {
    	map<string, Contract> ContractNumberMap = new map<string, Contract>();
    	for(Contract contractObj : getRecordsByContractNumber(contractUniqueKeySet)) {
    		ContractNumberMap.put(contractObj.EP_Contract_Number__c, contractObj);
    	}
    	return ContractNumberMap;
    }
}