@isTest
private class EP_CutOffMatrixHelper_UT {
	
	@isTest static void getDayName_test() {
		EP_CutOffMatrixHelper cmh = new EP_CutOffMatrixHelper();

		System.AssertEquals('Monday',cmh.getDayName(1));
		System.AssertEquals('Tuesday',cmh.getDayName(2));
		System.AssertEquals('Wednesday',cmh.getDayName(3));
		System.AssertEquals('Thursday',cmh.getDayName(4));
		System.AssertEquals('Friday',cmh.getDayName(5));
		System.AssertEquals('Saturday',cmh.getDayName(6));
		System.AssertEquals('Sunday',cmh.getDayName(7));
		System.AssertEquals('Monday',cmh.getDayName(8));
	}
	
	@isTest static void getTodayNum_test() {
		EP_CutOffMatrixHelper cmh = new EP_CutOffMatrixHelper();

		System.AssertEquals(1,cmh.getTodayNum('Monday'));
		System.AssertEquals(2,cmh.getTodayNum('Tuesday'));
		System.AssertEquals(3,cmh.getTodayNum('Wednesday'));
		System.AssertEquals(4,cmh.getTodayNum('Thursday'));
		System.AssertEquals(5,cmh.getTodayNum('Friday'));
		System.AssertEquals(6,cmh.getTodayNum('Saturday'));
		System.AssertEquals(7,cmh.getTodayNum('Sunday'));
		System.AssertEquals(0,cmh.getTodayNum('MMMonday'));
	}

	@isTest static void getCutoffTime_test() {
		EP_CutOffMatrixHelper cmh = new EP_CutOffMatrixHelper();

		DeliveryDay__c dDay = new DeliveryDay__c(Monday__c = Time.newInstance(1, 0, 0, 0),
												 Tuesday__c = Time.newInstance(2, 0, 0, 0),
												 Wednesday__c = Time.newInstance(3, 0, 0, 0),
												 Thursday__c = Time.newInstance(4, 0, 0, 0),
												 Friday__c = Time.newInstance(5, 0, 0, 0),
												 Saturday__c = Time.newInstance(6, 0, 0, 0),
												 Sunday__c = Time.newInstance(7, 0, 0, 0));

		System.AssertEquals(1,cmh.getCutoffTime('Monday', dDay).hour());
		System.AssertEquals(2,cmh.getCutoffTime('Tuesday', dDay).hour());
		System.AssertEquals(3,cmh.getCutoffTime('Wednesday', dDay).hour());
		System.AssertEquals(4,cmh.getCutoffTime('Thursday', dDay).hour());
		System.AssertEquals(5,cmh.getCutoffTime('Friday', dDay).hour());
		System.AssertEquals(6,cmh.getCutoffTime('Saturday', dDay).hour());
		System.AssertEquals(7,cmh.getCutoffTime('Sunday', dDay).hour());
		System.AssertEquals(1,cmh.getCutoffTime('SSSunday', dDay).hour());
	}


	@isTest static void getDeliveryDaysByTerminalAndDeliveryType_test() {
		EP_CutOffMatrixHelper cmh = new EP_CutOffMatrixHelper();

		cmh.cutoffMatrix = new CutOffMatrix__c();
		cmh.deliveryDayList = new List<DeliveryDay__c>();

		for (Integer i = 1; i < 8; i++) {
			cmh.deliveryDayList.add(new DeliveryDay__c(Order_Day_Name__c = cmh.getDayName(i), OrderDayNum__c = i));
		}

		List<DeliveryDay__c> ddList = cmh.getDeliveryDaysByTerminalAndDeliveryType();

		Integer j = 0;
		for (DeliveryDay__c dd : ddList) {
			DeliveryDay__c dd_res = cmh.deliveryDayList.get(j);
			System.AssertEquals(dd_res.Order_Day_Name__c,dd.Order_Day_Name__c);
			System.AssertEquals(dd_res.OrderDayNum__c,dd.OrderDayNum__c);
			j++;
		}
	}


	@isTest static void createDeliveryDaysList_test() {
		EP_CutOffMatrixHelper cmh = new EP_CutOffMatrixHelper();

		cmh.cutoffMatrix = new CutOffMatrix__c();
		cmh.deliveryDayList = new List<DeliveryDay__c>();

		for (Integer i = 1; i < 8; i++) {
			cmh.deliveryDayList.add(new DeliveryDay__c(Order_Day_Name__c = cmh.getDayName(i), OrderDayNum__c = i));
		}

		List<DeliveryDay__c> ddList = cmh.createDeliveryDaysList();

		Integer j = 0;
		for (DeliveryDay__c dd : ddList) {
			DeliveryDay__c dd_res = cmh.deliveryDayList.get(j);
			System.AssertEquals(dd_res.Order_Day_Name__c,dd.Order_Day_Name__c);
			System.AssertEquals(dd_res.OrderDayNum__c,dd.OrderDayNum__c);
			j++;
		}
	}

	@isTest static void getDayOfTheWeek_test() {
		EP_CutOffMatrixHelper cmh = new EP_CutOffMatrixHelper();

		DateTime d = DateTime.newInstance(2018, 5, 20);

		//MM/DD/YYY
		System.AssertEquals(d.format('EEEE'),cmh.getDayOfTheWeek('05/20/2018'));
	}

	@isTest static void checkCutOffTimeOk_test() {
		EP_CutOffMatrixHelper cmh = new EP_CutOffMatrixHelper();

		cmh.cutoffMatrix = new CutOffMatrix__c(Time_Zone__c = 'GMT');

		Time cutOffT = Time.newInstance(20, 0, 0, 0);
		String localTime = '19:00:00';
		String localDate = '02/28/2018';
		String localGmtOffset = '0';
		System.AssertEquals(true, cmh.checkCutOffTimeOk(cutOffT, localTime, localDate, localGmtOffset));

		localGmtOffset = '-120';
		System.AssertEquals(false, cmh.checkCutOffTimeOk(cutOffT, localTime, localDate, localGmtOffset));
	}
	
}