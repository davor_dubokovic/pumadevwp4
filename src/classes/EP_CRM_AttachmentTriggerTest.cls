@isTest 
private class EP_CRM_AttachmentTriggerTest {
static Map < String, User > userMap = new Map < String, User > ();
static Map < String, UserRole > roleMap = new Map < String, UserRole > ();
static List < Account > accountList = new List < Account > ();

static { 
      for(User userObj: [SELECT Id, Name, EP_CRM_Geo1__c, EP_CRM_Geo2__c FROM User]) {
        userMap.put(userObj.Name, userObj);
      }
      for(UserRole roleObj: [SELECT Id, Name FROM UserRole]) {
        roleMap.put(roleObj.Name, roleObj);
      }
  }
  
   @testSetup 
  static void loadData() {
  

      //UserRole testRole3 = EP_CRM_TestDataFactory.createUserRole('Belize Executive', null);
      UserRole testRole3 = EP_CRM_TestDataFactory.createUserRole('Belize Executive', roleMap.get('Global Executive').ID);
      insert testRole3;
      System.assertNotEquals(testRole3, null);
      System.assertNotEquals(testRole3.Id, null);
      
      
      Id profileId = EP_CRM_TestDataFactory.getProfileIdByName('System Administrator');
      System.assertNotEquals(profileId, null);
      
      List < User > userList = new List < User > ();
      
      userList.add(EP_CRM_TestDataFactory.createTestUser(testRole3.Id, profileId, 'Executive', 'User'));
      insert userList;
      System.assertNotEquals(userList.get(0).Id, null);
      
      }
        
     
  
   @isTest 
  static void testExecutiveRoleUser() {
          User testUser = userMap.get('Executive User');
          testUser.UserRoleId = roleMap.get('Belize Executive').Id;
          update testUser;
          System.assertNotEquals(testUser.UserRoleId, null);
          
           Test.startTest();
            System.runAs(testUser) {
            
       
      // Create Puma Company
      Company__c pumaCompany = EP_CRM_TestDataFactory.createPumaCompany('Puma Company', new User());
      insert pumaCompany;
      System.assertNotEquals(pumaCompany, null);
      System.assertNotEquals(pumaCompany.Id, null);
      
      // Create Account List
      //Account a = new Account(Name='newAcct'); 
      
      for(Integer i = 1; i <= 2; i++) {
        accountList.add(EP_CRM_TestDataFactory.createAccount('Test Account - ' + i, pumaCompany));
      }
      insert accountList;
      System.debug('Inserted accountList: '+ accountList.size());
      list<Attachment> lstAttch  = new list<Attachment>();    
            for(Account a:accountList){
            System.debug('in for  '+ a.Id);  
            Attachment attach=new Attachment(); 
            attach.Name='Unit Test Attachment'; 
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body'); 
            attach.body=bodyBlob; 
            attach.parentId=a.Id; 
            attach.ContentType = 'application/msword'; 
            attach.IsPrivate = false; 
            attach.Description = 'Test'; 
            insert attach;
            System.debug('Inserted: '+ attach.Id);   
            lstAttch.add(attach);                       
        }        
                
        /*List<Account> acct = [SELECT id, Name, 
        (SELECT Id FROM CombinedAttachments) 
        FROM Account where id = :a.Id]; 
        
        System.debug('Account Query : '+acct); 
        
        for(Account a1 : acct){ 
            System.debug('Id: '+a1.Id + ' Name: '+a1.Name + ' CombAttach: '+ a1.CombinedAttachments);     
            System.assert(a1.CombinedAttachments.size()>0);        
        } */
        
        delete lstAttch;        
           
          Test.stopTest();
      }
     }   
        
}