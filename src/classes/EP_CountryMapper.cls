/**
   @Author          CR Team
   @name            EP_CompanyMapper
   @CreateDate      12/21/2016
   @Description     This class contains all SOQLs related to EP_Country__c Object
   @Version         1.0
   @reference       NA
   */
   public with sharing class EP_CountryMapper {
    
    
    /**
    *  Constructor. 
    *  @name            EP_CountryMapper
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */  
    public EP_CountryMapper() {
        
    }

    /** This method returns Country Records by Ids
    *  @name            getRecordsByIds
    *  @param           set<Id> 
    *  @return          list<EP_Country__c>
    *  @throws          NA
    */
    public list<EP_Country__c> getRecordsByIds(set<id> idSet) {
        EP_GeneralUtility.Log('Public','EP_CountryMapper','getRecordsByIds');
        list<EP_Country__c> countryObjList =  new list<EP_Country__c>(); 
        for(List<EP_Country__c> countryList: [SELECT id, CurrencyIsoCode, Name 
            FROM EP_Country__c 
            WHERE id IN :idSet 
                                            //LIMIT :limitVal
                                            ]){
            countryObjList.addAll(countryList);                                  
        }                                    
        return countryObjList ;
    }
    

    /** This method returns Country Records by name
    *  @name            getRecordsByNames
    *  @param           set<id>
    *  @return          list<EP_Country__c>
    *  @throws          NA
    */
    public list<EP_Country__c> getRecordsByNames(list<String> stringList) {
        EP_GeneralUtility.Log('Public','EP_CountryMapper','getRecordsByNames');
        list<EP_Country__c> countryObjList =  new list<EP_Country__c>();
        for(List<EP_Country__c> countryList: [SELECT Id, Name, EP_Country_Code__c FROM EP_Country__c WHERE NAME IN :stringList]){
            countryObjList.addAll(countryList);
        }                                         
        return countryObjList;
    }
    
    public EP_Country__c getCountryById(Id countryId){
        EP_GeneralUtility.Log('Public','EP_CountryMapper','getCountryById');
        EP_Country__c country = [SELECT id,CurrencyIsoCode, Name From EP_Country__c WHERE Id=: countryId ];
        return country;      
    }
    
    public list<EP_Country__c> getRecordsByCountryCode(set<string> setCountryCodes){
        EP_GeneralUtility.Log('Public','EP_CountryMapper','getRecordsByCode');
        return [SELECT Id, name, EP_Country_Code__c FROM EP_Country__c WHERE EP_Country_Code__c IN:setCountryCodes]; 
    }
	
	/**
	* @author 			Accenture
	* @name				getCountryCodesMapByCodes
	* @date 			03/13/2017
	* @description 		Will return the map of country records for the set of country codes found in wrapper instance.
	* @param 			Set<String>
	* @return 			Map<String,Id>
	*/
    public Map<String,Id> getCountryCodesMapByCodes(Set<String> setCountryCodes){
        EP_GeneralUtility.Log('Private','EP_CountryMapper','getCountryCodesMapByCodes');
        Map<String,Id> mapCountryCodeCountryIDs = new Map<String,Id>();
        if(!setCountryCodes.isEmpty()){
        	for(EP_Country__c cntry : getRecordsByCountryCode(setCountryCodes)){
                mapCountryCodeCountryIDs.put(cntry.EP_Country_Code__c.toUpperCase(),cntry.id);
            }
        }
        return mapCountryCodeCountryIDs;
    }    
}