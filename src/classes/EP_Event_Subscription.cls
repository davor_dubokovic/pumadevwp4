/****************************************************************
* @author       Accenture                                       *
* @name         EP_Event_Subscription                           *
* @Created Date 28/12/2017                                      *
* @description  Virtual Class to insert Event                   *
****************************************************************/
public virtual class EP_Event_Subscription {
    public Database.SaveResult saveResultobj;

    public subscriptionEvent subscriptionEventObj;

/****************************************************************
* @author       Accenture                                       *
* @name         EP_Event_Subscription                           *
* @description  Constructor                                     *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public EP_Event_Subscription() {
        subscriptionEventObj = new subscriptionEvent();
    }

/****************************************************************
* @author       Accenture                                       *
* @name         subscribeEvent                                  *
* @description  method to insert event                          *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public virtual void subscribeEvent(){
        EP_GeneralUtility.Log('public','EP_Event_Subscription','subscribeEvent');
        List<EP_Notification_Account_Settings__c> settingList = new List<EP_Notification_Account_Settings__c>();
        if(subscriptionEventObj.eventType !=null && subscriptionEventObj.sellToId !=null)
            settingList = EP_Account_Notification_SettingsMapper.getNotificationIdByCode(subscriptionEventObj.eventType,subscriptionEventObj.sellToId);
        if(!settingList.isEmpty())
            publishEvent(settingList[0]);
    }

/****************************************************************
* @author       Accenture                                       *
* @name         publishEvent                                    *
* @description  method to publish event                         *
* @param        String                                          *
* @return       void                                            *
****************************************************************/
    public void publishEvent(EP_Notification_Account_Settings__c accNotification){
        EP_GeneralUtility.Log('public','EP_Event_Subscription','publishEvent');
        EP_Notification_Framework__e eventObj = new EP_Notification_Framework__e();
        if(null!=subscriptionEventObj.attachment)
            eventObj.EP_Attachment_Id__c = subscriptionEventObj.attachment.Id;
        eventObj.EP_Account_Notification_Id__c = accNotification.id;
        eventObj.EP_What_Id__c = subscriptionEventObj.whatId;
        saveResultobj = EventBus.publish(eventObj);
        if(saveResultobj.isSuccess()){
            logNotification(accNotification);
        }
    }

/****************************************************************
* @author       Accenture                                       *
* @name         logNotification                                 *
* @description  method to create notifiction log record         *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    @TestVisible private void logNotification(EP_Notification_Account_Settings__c accNotification){
        System.debug('accNotification = '+accNotification);
        EP_Notification_Log__c notificationLog = new EP_Notification_Log__c(EP_Message_Date__c = System.Now(),
                                                                                EP_Notification_Type__c = accNotification.EP_Notification_Type__r.name,
                                                                                EP_Recipient_Contact__c = accNotification.EP_Recipient_Contact__c,
                                                                                EP_Recipient_User__c = accNotification.EP_Recipient_User__c,
                                                                                EP_Document_Name__c = subscriptionEventObj.attachment.name);
        Database.insert(notificationLog,true);
    }

/****************************************************************
* @author       Accenture                                       *
* @name         EP_Event_Subscription                           *
* @Created Date 28/12/2017                                      *
* @description  Inner   Class to insert Event                   *
****************************************************************/
    public class subscriptionEvent{
        public Attachment attachment{get;set;}
        public string eventType{get;set;}
        public string sellToId{get;set;} 
        public string whatId{get;set;}
    }
}