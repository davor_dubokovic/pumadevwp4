/* 
@Author      CloudSense
@name        EP_RetrospectiveOrderBatch
@CreateDate  23/07/2018
@Description Asyncronous batch process. 
@Version     1.0
*/
global class EP_RetrospectiveOrderBatch implements Database.Batchable<sObject>,Database.AllowsCallouts { 
    private static final string ORDER_STATUS = 'Order Submitted';
    private static final string ORDER_EPOC  = 'Retrospective';
    private static final string ORDER_CURRENT  = 'Current';
    private static final string PROCESSING_ERROR = 'Error';
    private static final string PROCESSING_PROCESSED = 'Processed';
    private static final string ORDER_TYPE_SALES = 'Sales';
    private static final string ORDER_DELIVERY_TYPE = 'Consumption';
    private static final string ORDER_PRODUCT_CATEGORY = 'Bulk';

    private static final String QUERY_STRING = 'Select Id, EP_Order_Number__c, EP_Staging_Order_Identifier__c, '+
    											'EP_Ordered_quantity__c, EP_Delivery_Reference_No__c, ' + 
    											'EP_SupplyLocation__c, EP_Sell_To__c, EP_BOL_Number__c, EP_Ship_To__c, '+
												'EP_Transporter__c, EP_Loading_Date__c, EP_Delivered_Date__c, '+
												'EP_Contract_Number__c, EP_Customer_PO_Number__c, '+
												'EP_Ambient_Loaded_Quantity__c, EP_Standard_Loaded_Quantity__c, '+
    											'EP_Ambient_Delivered_Quantity__c, EP_Standard_Delivered_Quantity__c, '+
    											'EP_Product_Code__c, EP_Delivery_Docket_Number__c, RecordType.DeveloperName, EP_File_Name__c '+
												'From EP_RO_Import_Staging__c '+
												//'Where EP_Status__c = \'Processed\' And (RecordType.DeveloperName = \'EP_Third_Party_Order\' OR RecordType.DeveloperName = \'EP_Retail_Order\') ';
    											'Where EP_Status__c = \'Pending\' And (RecordType.DeveloperName = \'EP_Third_Party_Order\' OR RecordType.DeveloperName = \'EP_Retail_Order\') ';

    Map<String, List<EP_RO_Import_Staging__c>> mapOrderIdentifier = new Map<String, List<EP_RO_Import_Staging__c>>();
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        DateTime dtCurrentTime = System.now();
        System.debug('>>> Query String'+ QUERY_STRING);
        return Database.getQueryLocator(QUERY_STRING);
    }
    	
    global void execute(Database.BatchableContext BC, List<EP_RO_Import_Staging__c> scope){
    	EP_CheckRecursive.run = false;
    	List<EP_RO_Import_Staging__c> lstImportStagingInsert = new List<EP_RO_Import_Staging__c>();
    	List<EP_RO_Import_Staging__c> lstImportStagingUpdate = new List<EP_RO_Import_Staging__c>();

    	for(EP_RO_Import_Staging__c sObj : scope) {
    		if(sObj.EP_Order_Number__c == null) {
				lstImportStagingInsert.add(sObj); 
				System.debug('>>> List Insert'+ lstImportStagingInsert);
    		} else {
				lstImportStagingUpdate.add(sObj); 
				System.debug('>>> List Update'+ lstImportStagingUpdate);
    		}
    	}

    	//Insert - List grouped by EP_Staging_Order_Identifier__c field
    	if(lstImportStagingInsert.Size() > 0) {
			insertOrder(lstImportStagingInsert);
    	}

		//Update - List grouped by EP_Staging_Order_Identifier__c field
		if(lstImportStagingUpdate.Size() > 0) {
			updateOrder(lstImportStagingUpdate);
		}
    }

    @TestVisible
    private static void insertOrder(List<EP_RO_Import_Staging__c> lstImportStagingInsert) {
    	Map<String, csord__Order__c> mapOrder = new Map<String, csord__Order__c>();
    	Map<String, Contract> mapContract = new Map<String, Contract>();
    	List<csord__Order__c> lstOrderInsert = new List<csord__Order__c>();
    	List<csord__Order_Line_Item__c> lstOrderLineItemInsert = new List<csord__Order_Line_Item__c >();
    	List<Database.UpsertResult> insertResults = new List<Database.UpsertResult>();
    	List<EP_RO_Import_Staging__c> lstImportErro = new List<EP_RO_Import_Staging__c>();
    	Set<Id> idOrder = new Set<Id>();
    	Set<String> setContractNumber = new Set<String>();
    	
    	Map<String, List<EP_RO_Import_Staging__c>> mapImportStagingInsert = groupImportStaging(lstImportStagingInsert);

    	for(EP_RO_Import_Staging__c importStaging : lstImportStagingInsert) {
    		setContractNumber.add(importStaging.EP_Contract_Number__c);
    	}

    	List<Contract> listContract = [ Select Id, EP_Contract_Number__c From Contract where EP_Contract_Number__c IN :setContractNumber ]; 

    	for(Contract contract : listContract) {
    		mapContract.put(contract.EP_Contract_Number__c, contract);
    	}

    	RecordType OrderRecordType = [Select Id From RecordType Where DeveloperName = 'Order'];

    	for(String sObj : mapImportStagingInsert.keySet()) {	
	        if(mapImportStagingInsert.containsKey(sObj)) {
	        	lstImportStagingInsert = mapImportStagingInsert.get(sObj);
	        	Contract contractObject = mapContract.get(lstImportStagingInsert[0].EP_Contract_Number__c);
	        	
	        	if(contractObject != null) {
	        		lstOrderInsert.add(orderSObject(lstImportStagingInsert[0], contractObject.Id, OrderRecordType.Id));
	        	} else {
	        		lstOrderInsert.add(orderSObject(lstImportStagingInsert[0], '', OrderRecordType.Id));
	        	}
	        }
    	}

        if(lstOrderInsert.Size() > 0) {
            insertResults = Database.upsert(lstOrderInsert, csord__Order__c.csord__External_Identifier__c);
    	}
        
    	for(Database.UpsertResult result : insertResults) {
    		System.debug('>>> Insert Order'+result);
    		if(result.isSuccess()){
     			idOrder.add(result.getId());   			
    		}
    	}

    	List<csord__Order__c> lstOrder = [Select Id, csord__Identification__c, csord__External_Identifier__c, OrderNumber__c  From csord__Order__c Where Id IN :idOrder];

    	for(csord__Order__c order : lstOrder){
    		mapOrder.put(order.csord__External_Identifier__c, order);
    	}

    	for(String sObj : mapImportStagingInsert.keySet()) {	
	        if(mapImportStagingInsert.containsKey(sObj)) {
	        	//Insert - EP_RO_Import_Staging__c List grouped by EP_Staging_Order_Identifier__c field 
	        	lstImportStagingInsert = mapImportStagingInsert.get(sObj);
	        	Integer index = 0;
			    for(EP_RO_Import_Staging__c importStaging : lstImportStagingInsert) {
			    	index++;
			    	csord__Order__c order = mapOrder.get(importStaging.EP_Staging_Order_Identifier__c);
			    	lstImportErro.add(importStaging);

			    	if(order != null) {
			    		//Order Line Item to Insert
				        lstOrderLineItemInsert.add(orderLineItemSObject(importStaging, order, index));  
				        System.debug('>>> Identifier - Item '+ importStaging.EP_Staging_Order_Identifier__c); 
			    	}    		
		    	}
	        }
    	}
        
        if(lstOrderLineItemInsert.Size() > 0) {
            List<Database.UpsertResult> insertResultsItem = Database.upsert(lstOrderLineItemInsert, csord__Order_Line_Item__c.csord__External_Identifier2__c);
            List<Database.SaveResult> updateOrderRules = Database.update(lstOrderInsert, false);  //Update to actived the rules after insert Order line item 
        }

        //Technical error when trying insert Order
    	for(EP_RO_Import_Staging__c importRO : lstImportErro){
    		if(!mapOrder.containsKey(importRO.EP_Staging_Order_Identifier__c)){
    			importRO.EP_Status__c = PROCESSING_ERROR;
    		} else {
    			csord__Order__c orderSobj = mapOrder.get(importRO.EP_Staging_Order_Identifier__c);

    			if(orderSobj != null ) {
	    			importRO.Order__c = orderSobj.Id;
	    			importRO.EP_Status__c = PROCESSING_PROCESSED;
	    			//importRO.EP_Status__c = 'Pending'; 
    			}
    		} 
    	}

    	if(lstImportErro.Size() > 0) {
    		List<Database.SaveResult> updateROImport = Database.update(lstImportErro, false); 
    	}
    }

    @TestVisible
    private static void updateOrder(List<EP_RO_Import_Staging__c> lstImportStagingUpdate) {
    	Map<String, EP_RO_Import_Staging__c> mapImportStaging = new Map<String, EP_RO_Import_Staging__c>();
    	List<csord__Order__c> lstOrderUpdate = new List<csord__Order__c>();
    	List<csord__Order_Line_Item__c> lstOrderLineItemUpdate = new List<csord__Order_Line_Item__c>();
    	List<EP_RO_Import_Staging__c> lstStagingUpdate = new List<EP_RO_Import_Staging__c>();

    	for(EP_RO_Import_Staging__c lstImport : lstImportStagingUpdate) {
    		mapImportStaging.put(lstImport.EP_Order_Number__c, lstImport);
    	} 

    	List<csord__Order__c> lstOrder = [Select Id, OrderNumber__c, EP_Order_Epoch__c From csord__Order__c Where OrderNumber__c IN :mapImportStaging.Keyset()];

    	for(csord__Order__c order : lstOrder) {
    		EP_RO_Import_Staging__c orderImport = mapImportStaging.get(order.OrderNumber__c);

    		if(order != null && orderImport != null){
	    		order.EP_Stock_Holding_Location__c = orderImport.EP_SupplyLocation__c;
	    		order.EP_BOL_Number__c = orderImport.EP_BOL_Number__c;
	    		order.EP_Transporter__c = orderImport.EP_Transporter__c;
	    		order.EP_Customer_PO_Number__c = orderImport.EP_Customer_PO_Number__c;
	    		order.EP_Delivery_Reference_Number__c = orderImport.EP_Delivery_Reference_No__c;
	    		order.EP_Delivery_Docket_Number__c = orderImport.EP_Delivery_Docket_Number__c;
	    		order.Actual_Delivery_Date__c =  orderImport.EP_Delivered_Date__c;
	    		order.EP_Pricing_Date__c = orderImport.EP_Delivered_Date__c;
	    		order.Delivery_From_Date__c = orderImport.EP_Delivered_Date__c;
	    		
	    		if(order.EP_Order_Epoch__c == ORDER_CURRENT) {
	    			order.EP_Order_Epoch__c = ORDER_EPOC;	    			
	    		}

	    		lstOrderUpdate.add(order);

	    		orderImport.EP_Status__c = PROCESSING_PROCESSED; 
	    		//orderImport.EP_Status__c = 'Pending'; 
    		}

    		lstStagingUpdate.add(orderImport);
    	}

    	List<csord__Order_Line_Item__c> lstOrderLineItem = [Select Id, Quantity__c, EP_Ambient_Loaded_Quantity__c From csord__Order_Line_Item__c Where Order_Line_Number__c IN :mapImportStaging.Keyset()];

    	for(csord__Order_Line_Item__c orderLineItem : lstOrderLineItem) {
    		EP_RO_Import_Staging__c orderImport = mapImportStaging.get(orderLineItem.Order_Line_Number__c);

    		if(orderLineItem != null && orderImport != null){
	    		orderLineItem.Quantity__c = orderImport.EP_Ordered_quantity__c;
	    		orderLineItem.EP_Ambient_Loaded_Quantity__c = orderImport.EP_Ambient_Loaded_Quantity__c;
			    orderLineItem.EP_Standard_Loaded_Quantity__c = orderImport.EP_Standard_Loaded_Quantity__c;
			    orderLineItem.EP_Ambient_Delivered_Quantity__c = orderImport.EP_Ambient_Delivered_Quantity__c;
			    orderLineItem.EP_Standard_Delivered_Quantity__c = orderImport.EP_Standard_Delivered_Quantity__c;

	    		lstOrderLineItemUpdate.add(orderLineItem);
    		}
    	}    	

    	List<Database.SaveResult> updateOrder = Database.update(lstOrderUpdate, false); 
    	List<Database.SaveResult> updateOrderLineItem = Database.update(lstOrderLineItemUpdate, false); 
    	List<Database.SaveResult> updateStaging = Database.update(lstStagingUpdate, false); 
    }

	//The grouped records utilise the EP_Staging_Order_Identifier__c field to identify.
    @TestVisible
    private static Map<String, List<EP_RO_Import_Staging__c>> groupImportStaging(List<EP_RO_Import_Staging__c> lstImport ) {
    	Map<String, List<EP_RO_Import_Staging__c>> mapOrderIdentifier = new Map<String, List<EP_RO_Import_Staging__c>> ();

    	for(EP_RO_Import_Staging__c sObj : lstImport) {
			List<EP_RO_Import_Staging__c> lstImportStaging = new List<EP_RO_Import_Staging__c>();
			for(EP_RO_Import_Staging__c sObj2 : lstImport) {
				if(sObj.EP_Staging_Order_Identifier__c == sObj2.EP_Staging_Order_Identifier__c) {
					lstImportStaging.add(sObj2);
				}
			}
			mapOrderIdentifier.put(sObj.EP_Staging_Order_Identifier__c, lstImportStaging);
    	}
    	return mapOrderIdentifier;
    }

    @TestVisible
    private static csord__Order__c orderSObject(EP_RO_Import_Staging__c importStagingInsert, String contractId, ID OrderRecordTypeId) {
    	csord__Order__c order = new csord__Order__c();
    	
    	if(importStagingInsert.RecordType.DeveloperName == 'EP_Retail_Order') {
    		order.EP_Type2__c = ORDER_TYPE_SALES;
    		order.EP_Delivery_Type__c = ORDER_DELIVERY_TYPE;
    		order.EP_Order_Product_Category__c = ORDER_PRODUCT_CATEGORY;
    	} else {
    		order.EP_BOL_Number__c = importStagingInsert.EP_BOL_Number__c;	
    		order.EP_Transporter__c = importStagingInsert.EP_Transporter__c;
    		order.Delivery_From_Date__c = importStagingInsert.EP_Delivered_Date__c;
    	}

    	order.RDW_NAV_Order_Sync__c = true;
    	order.EP_Import_File_Name__c = importStagingInsert.EP_File_Name__c;

    	order.csord__Status2__c = ORDER_STATUS;  
	    order.EP_Stock_Holding_Location__c = importStagingInsert.EP_SupplyLocation__c;
	    order.EP_Sell_To__c = importStagingInsert.EP_Sell_To__c;
	    order.csord__Account__c = importStagingInsert.EP_Sell_To__c;
	    order.EP_ShipTo__c = importStagingInsert.EP_Ship_To__c; 
	    order.EP_Expected_Loading_Date__c = importStagingInsert.EP_Loading_Date__c;
	    order.Actual_Delivery_Date__c = importStagingInsert.EP_Delivered_Date__c;
	    order.EP_Pricing_Date__c = importStagingInsert.EP_Delivered_Date__c;
	    if(contractId != '') {
	    	order.EP_Purchase_Contract_Info__c =  contractId;
		}
	    order.EP_Customer_PO_Number__c = importStagingInsert.EP_Customer_PO_Number__c;	
	    order.EP_Delivery_Reference_Number__c = importStagingInsert.EP_Delivery_Reference_No__c;
	    order.EP_Order_Epoch__c = ORDER_EPOC;
		order.csord__Identification__c = importStagingInsert.EP_Staging_Order_Identifier__c;
		order.csord__External_Identifier__c = importStagingInsert.EP_Staging_Order_Identifier__c;
    	return order; 
    }

    @TestVisible
    private static csord__Order_Line_Item__c orderLineItemSObject(EP_RO_Import_Staging__c importStaging, csord__Order__c order, Integer index) {
	    csord__Order_Line_Item__c  orderLineItem = New csord__Order_Line_Item__c();

	    if(importStaging.RecordType.DeveloperName != 'EP_Retail_Order') {
	    	orderLineItem.Quantity__c = importStaging.EP_Ordered_quantity__c;	    	
	    }

	    orderLineItem.EP_Ambient_Loaded_Quantity__c = importStaging.EP_Ambient_Loaded_Quantity__c;
	    orderLineItem.EP_Standard_Loaded_Quantity__c = importStaging.EP_Standard_Loaded_Quantity__c;
	    orderLineItem.EP_Ambient_Delivered_Quantity__c = importStaging.EP_Ambient_Delivered_Quantity__c;
	    orderLineItem.EP_Standard_Delivered_Quantity__c = importStaging.EP_Standard_Delivered_Quantity__c;
	    orderLineItem.EP_Product_Id__c = importStaging.EP_Product_Code__c;
	    orderLineItem.csord__Identification__c = importStaging.EP_Staging_Order_Identifier__c;
		orderLineItem.csord__External_Identifier2__c = importStaging.EP_Staging_Order_Identifier__c  + '_' + index;
	    orderLineItem.Status__c = ''; 
	    orderLineItem.csord__Order__c = order.Id;
    	return orderLineItem;
    }

	global void finish(Database.BatchableContext BC){
    }
}