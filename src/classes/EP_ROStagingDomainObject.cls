/* 
   @Author      Accenture
   @name        EP_ROStagingDomainObject
   @CreateDate  04/16/2018
   @Description This is the domain class for EP_RO_Import_Staging__c Object
   @Version     1.0
*/
public class EP_ROStagingDomainObject{
    public EP_RO_Import_Staging__c localROStaging;
    public EP_RO_Import_Staging__c oldROStaging;
    public EP_ROImportStagingMapper roStagingMapper = new EP_ROImportStagingMapper();
    
    /**
    * @author       Accenture
    * @name         EP_ROStagingDomainObject
    * @date         04/16/2018
    * @description  Constructor for setting new EP_RO_Import_Staging__c and old EP_RO_Import_Staging__c objects which will be passed by the trigger
    * @param        EP_RO_Import_Staging__c,EP_RO_Import_Staging__c
    * @return       NA
    */ 
    public EP_ROStagingDomainObject(EP_RO_Import_Staging__c newROStaging, EP_RO_Import_Staging__c oldROStaging){
        this.localROStaging = newROStaging;
        this.oldROStaging = oldROStaging;
    }
    /**
    * @author       Accenture
    * @name         EP_ROStagingDomainObject
    * @date         04/16/2018
    * @description  Constructor for setting new EP_RO_Import_Staging__c objects which will be passed by the trigger
    * @param        EP_RO_Import_Staging__c
    * @return       NA
    */ 
    public EP_ROStagingDomainObject(EP_RO_Import_Staging__c roStagingObj){
        localROStaging = roStagingObj;       
    }
    /**
    * @author       Accenture
    * @name         doActionAfterUpdate
    * @date         04/16/2018
    * @description  This method is to be called thorugh trigger to execute all after update event logic/actions
    * @param        NA
    * @return       string
    */ 
    public void doActionBeforeUpdate(){
        lockRecordForEditing();
        updateConsolidateStatus();
    } 
    /**
    * @author       Accenture
    * @name         doActionAfterUpdate
    * @date         04/16/2018
    * @description  This method is to be called thorugh trigger to execute all after update event logic/actions
    * @param        NA
    * @return       string
    */ 
    public void doActionAfterUpdate(){
        ProcessStagingRecords();
    }
    /**
    * @author       Accenture
    * @name         processStagingRecords
    * @date         04/16/2018
    * @description  This method is used to process staging record that are modifed for data validation
    * @param        NA
    * @return       string
    */
    private void processStagingRecords(){
    	if( (EP_Common_Constant.ERROR.equalsIgnoreCase(oldROStaging.EP_Status__c) 
    		|| EP_Common_Constant.STATUS_DUPLICATE.equalsIgnoreCase(oldROStaging.EP_Status__c)) 
    		&& 
    		(EP_Common_Constant.STATUS_PENDING.equalsIgnoreCase(localROStaging.EP_Status__c) 
    		|| EP_Common_Constant.CRLITEM_STATUS_REJECTED.equalsIgnoreCase(localROStaging.EP_Status__c) )
    		)
        	EP_ROImportStagingFactory.ProcessStagingRecords(localROStaging.EP_File_Name__c,localROStaging.EP_Record_Type_Dev_Name__c);    
    }  
    /**
    * @author       Accenture
    * @name         lockRecordForEditing
    * @date         04/16/2018
    * @description  This method is used to process staging record that are modifed for data validation
    * @param        NA
    * @return       string
    */
    private void lockRecordForEditing(){
        EP_File__c fRecord = new EP_ROImportFileMapper().getFileRecordById(localROStaging.EP_File_Name__c);
        if(fRecord.EP_In_Process__c){
            localROStaging.addError('Record is locked for editing');
        }
    }
    private void updateConsolidateStatus() {
    	if(EP_Common_Constant.CRLITEM_STATUS_REJECTED.equalsIgnoreCase(localROStaging.EP_Status__c)) {
    		localROStaging.EP_Consolidated_Status__c = EP_Common_Constant.ERROR;
    	}
    }
}