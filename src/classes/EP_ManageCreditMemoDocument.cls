/**
 *  @Author <Pooja Dhiman>
 *  @Name <EP_ManageCreditMemoDocument>
 *  @CreateDate <31/05/2016>
 *  @Description <This is the apex class used to generate PDF for credit memo>
 *  @Version <1.0>
 */
public with sharing class EP_ManageCreditMemoDocument {
    
    
    private static final string CLASS_NAME = 'EP_ManageCreditMemoDocument';
    private static final string GENERATE_AT_METHOD = 'generateAttachmentForCreditMemo';
    
    /**
     * @author <Pooja Dhiman>
     * @name <generateAttachmentForCreditMemo>
     * @date <04/07/2016>
     * @description <This method is used to generate document for Credit Memo>
     * @version <1.0>
     * @param EP_ManageDocument.Documents
     */
    public static void generateAttachmentForCreditMemo(EP_ManageDocument.Documents documents) {

        List<EP_Customer_Credit_Memo__c> lstCustomerCreditMemos = new List<EP_Customer_Credit_Memo__c>();
        Map<String, List<Attachment>> creditMemoKeyAttachListMap = new Map<String, List<Attachment>>();
        //set containg 'AccountNumber' field values of 'Account' standard object received from Input JSON
        Set<String> acctNumSet = new Set<String>();
        Map<String, String> creditKeyBillToMap = new Map<String, String>();
        try
        {
            List<Attachment> creditMemoKeyAttachment = null;	
            for(EP_ManageDocument.Document doc : documents.document) {
                EP_ManageDocument.CustomerCreditMemoWrapper custCreditMemoWrap =            
                    EP_DocumentUtil.fillCustomerCreditMemoWrapper(doc.documentMetaData.metaDatas);
                EP_Customer_Credit_Memo__c creditMemo = generateCustomerCreditMemo(custCreditMemoWrap);
                lstCustomerCreditMemos.add(creditMemo);
                
                String creditMemoKey = creditMemo.EP_Credit_Memo_Key__c;
                String compositKey = custCreditMemoWrap.issuedFromId + EP_Common_Constant.HYPHEN + custCreditMemoWrap.issuedToId;
                acctNumSet.add(compositKey);
                creditKeyBillToMap.put(creditMemoKey, compositKey.toUpperCase());
                
                Attachment att = EP_DocumentUtil.generateAttachment(doc);
                if(!(creditMemoKeyAttachListMap.containsKey(creditMemoKey))) {
                    creditMemoKeyAttachment = new List<Attachment>();
                    creditMemoKeyAttachListMap.put(creditMemoKey, creditMemoKeyAttachment);
                }
                creditMemoKeyAttachListMap.get(creditMemoKey).add(att);
            }
                
            Map<String, Id> acctNumIdMap = EP_DocumentUtil.getAccountCompositIdMap(acctNumSet, 
            new Set<String>{EP_Common_Constant.RT_BILL_TO_DEV_NAME, EP_Common_Constant.SELL_TO_DEV_NAME});
            
            List<EP_Customer_Credit_Memo__c> creditMemoToUpdate = new List<EP_Customer_Credit_Memo__c>();
            //L4 #45304 Changes Start - Collect all BillTo to get existing credit memo records
            set<String> setOfBillTo = new set<String>();
            for(EP_Customer_Credit_Memo__c creditMemo : lstCustomerCreditMemos) {
                String creditMemoKey = creditMemo.EP_Credit_Memo_Key__c;
                creditMemo.EP_Bill_To__c = acctNumIdMap.get(creditKeyBillToMap.get(creditMemo.EP_Credit_Memo_Key__c));
                creditMemoToUpdate.add(creditMemo);
                setOfBillTo.add(creditMemo.EP_Bill_To__c);
            }
            //L4 #45304 Changes End
            Schema.SObjectField f = EP_Customer_Credit_Memo__c.Fields.EP_Credit_Memo_Key__c;
            List<Database.upsertResult> uResults = Database.upsert(creditMemoToUpdate, f, false);
            for(Database.upsertResult result : uResults) {
                    if(result.isSuccess() && result.isCreated()) {
                        System.debug('Successfully inserted/updated CreditMemo. CreditMemo ID: ' + result.getId());
                    } else {
                        for(Database.Error err : result.getErrors()) {
                            System.debug('The following error has occurred.');
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Credit Memo fields that affected this error: ' + err.getFields());
                        }
                    }
                }
                
            Map<String, Id> creditMemoNrIdMap = new Map<String, Id>();
            Integer nRows = EP_Common_Util.getQueryLimit();
            //L4 #45304 Changes Start - Old Code Fix - Added billTo filter because Can't fire SOQL without any filter on Object
            creditMemoToUpdate = [SELECT Id, EP_Credit_Memo_Key__c FROM EP_Customer_Credit_Memo__c where EP_Bill_To__c In : setOfBillTo limit: nRows];
            //L4 #45304 Changes End
            for(EP_Customer_Credit_Memo__c creditMemo : creditMemoToUpdate) {
                creditMemoNrIdMap.put(creditMemo.EP_Credit_Memo_Key__c, creditMemo.Id);
            }
            List<Attachment> attachments = new List<Attachment>();
            for(String key : creditMemoKeyAttachListMap.keySet()) {
                List<Attachment> attachList = creditMemoKeyAttachListMap.get(key);
                for(Attachment att : attachList) {
                    att.ParentId = creditMemoNrIdMap.get(key);
                    attachments.add(att);
                }
            }
                
            EP_DocumentUtil.deleteAttachments(attachments);
            
            Database.SaveResult[] results = Database.insert(attachments, false);
            for (Integer i = 0; i < results.size(); i++) {
                if (results[i].isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted attachment. Attachment ID: ' + results[i].getId());
                    EP_ManageDocument.successRecIdMap.put(attachments[i].Name, results[i].getId());
                } else {
                    // Operation failed, so get all errors
                    for(Database.Error err : results[i].getErrors()) {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Attachment fields that affected this error: ' + err.getFields());
                        EP_ManageDocument.errDesMap.put(attachments[i].Name, err.getMessage());
                        EP_ManageDocument.errCodeMap.put(attachments[i].Name, String.valueOf(err.getStatusCode()));
                    }
                }
            }
        }
        catch(Exception e){
             EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, GENERATE_AT_METHOD , CLASS_NAME,apexPages.severity.ERROR);
             throw e;
        }
    }  

    /**
     * @author <Pooja dhiman>
     * @name <generateCustomerCreditMemo>
     * @date <03/07/2016>
     * @description <This method is used to generate 'EP_Customer_Credit_Memo__c' custom object record>
     * @version <1.0>
     * @param EP_ManageDocument.CustomerCreditMemoWrapper
     * @return EP_Invoice__c
     */
    private static EP_Customer_Credit_Memo__c generateCustomerCreditMemo(EP_ManageDocument.CustomerCreditMemoWrapper custCreditMemoWrap) {
        EP_Customer_Credit_Memo__c creditMemo = new EP_Customer_Credit_Memo__c();
        creditMemo.EP_Credit_Memo_Number__c = custCreditMemoWrap.docNr;
        creditMemo.EP_NavSeqId__c = custCreditMemoWrap.SeqId;
        /** TR Datetime 59846 Start **/
        creditMemo.EP_Credit_Memo_Issue_Date__c = EP_DateTimeUtility.convertStringToDate(custCreditMemoWrap.CreditMemoIssueDate);//Date.valueOf(custCreditMemoWrap.CreditMemoIssueDate);
        /** TR Datetime 59846 End **/
		//L4 #45304 Changes Start - Added entryNr in composit key formation
        creditMemo.EP_Credit_Memo_Key__c = EP_DocumentUtil.generateUniqueKey(new Set<String>{custCreditMemoWrap.issuedToId, custCreditMemoWrap.docNr, custCreditMemoWrap.issuedFromId, custCreditMemoWrap.entryNr});
        //L4 #45304 Changes End        
        creditMemo.CurrencyIsoCode = custCreditMemoWrap.Currencie;
        return creditMemo;
    }
}