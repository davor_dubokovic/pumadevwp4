/*
*  @Author <Accenture>
*  @Name <EP_ASTNonVMIShipToActiveToBlocked>
*  @CreateDate <28/02/2017>
*  @Description <Handles VMI Ship To Account status change from 05-Active to 06-Blocked>
*  @NovaSuite Fix -- comments added
*  @Version <1.0>
*/
public class EP_ASTNonVMIShipToActiveToBlocked extends EP_AccountStateTransition {
     /**
    * @author <Accenture>
    * @name Constructor of class EP_ASTNonVMIShipToActiveToBlocked
    */
    public EP_ASTNonVMIShipToActiveToBlocked () {
        finalState = EP_AccountConstant.BLOCKED;
    }
     /**
    * @author <Accenture>
    * @name isTransitionPossible
    */
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToActiveToBlocked','isTransitionPossible');
        return super.isTransitionPossible();
    }
     /**
    * @author <Accenture>
    * @name isRegisteredForEvent
    */
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToActiveToBlocked','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
     /**
    * @author <Accenture>
    * @name isGuardCondition
    */
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToActiveToBlocked','isGuardCondition');
        //Code changes for L4 #45362 Start
        //Check if any inflight orders are exits for the ShipTo, Don't allow to Blocked the shipTo if inflight order exists
        EP_AccountService service = new EP_AccountService(this.account);
        if(service.hasInfligtOrders()) {
        	accountEvent.isError = true;
            accountEvent.setEventMessage(System.label.EP_Close_In_flight_Orders_Error_Msg);
            return false;
        }
        //Code changes for L4 #45362 End
        return true;
    }
     /**
    * @author <Accenture>
    * @name doOnExit
    */
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToActiveToBlocked','doOnExit');

    }
}