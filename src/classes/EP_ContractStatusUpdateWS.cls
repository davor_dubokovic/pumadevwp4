/* 
  @Author <Accenture>
   @name <EP_ContractStatusUpdateWS>
   @CreateDate <20/04/2018>
   @Description <This is the apex RESTful webService to Update status of the Contracts synced from Nav> 
   @Version <1.0>
*/
@RestResource(urlMapping='/v1/ContractUpdateStatus/*')
global without sharing class EP_ContractStatusUpdateWS{
     /*
    This is a http post method which updates the Contract from external(NAV) system
   */
   @httpPost
   global static void processRequest(){
       EP_GeneralUtility.Log('global','EP_ContractStatusUpdateWS','processRequest');
       RestRequest request =  RestContext.request;
       string requestbody = request.requestBody.toString();
       RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
       EP_IntegrationService service = new EP_IntegrationService();
       string response = service.handleRequest(EP_Common_Constant.NAV_ORDER_UPDATE,requestBody);
       RestContext.response.responseBody = Blob.valueOf(response);
   }
   
}