/* 
   @Author 			Accenture
   @name 			EP_CustomerFundsUpdateHandler
   @CreateDate 		03/10/2017
   @Description		Stub class to update available funds of customer i.e. inbound interface to update customers funds
   @Version 		1.0								  
*/
 public class EP_CustomerFundsUpdateHandler extends EP_InboundHandler{
 	
	/**
	* @author 			Accenture
	* @name				processRequest
	* @date 			03/13/2017
	* @description 		Processess the account available fund request and sends back the acknowledgement
	* @param 			string 
	* @return 			string
	*/
	 public override string processRequest(string jsonRequest){
	 	EP_GeneralUtility.Log('public','EP_CustomerFundsUpdateHandler','processRequest');
	 	string jsonResponse ='';
	 	List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
	 	EP_MessageHeader HeaderCommon = new EP_MessageHeader();
	 	EP_IntegrationUtil.isCallout = true;
	 	try {
	 		EP_CustomerFundsUpdateStub stub = parse(jsonRequest);
			HeaderCommon = stub.MSG.HeaderCommon;
			List<EP_CustomerFundsUpdateStub.customer> customers = stub.MSG.payload.any0.availableFunds.customer;
			EP_CustomerFundsUpdateHelper.setCustomerAttributes(customers); 
			ackDatasets = createAcknowledgementDatasets(customers); 
	        jsonResponse = EP_AcknowledgementHandler.createAcknowledgement(EP_Common_Constant.NAV_TO_SFDC_CUSTOMER_FUNDS_UPDATE,false,'',HeaderCommon,ackDatasets);
    	}catch(Exception ex){
    		jsonResponse = EP_AcknowledgementHandler.createAcknowledgement(EP_Common_Constant.NAV_TO_SFDC_CUSTOMER_FUNDS_UPDATE,true,ex.getMessage(),HeaderCommon,ackDatasets);
    		EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, EP_Common_Constant.NAV_TO_SFDC_CUSTOMER_FUNDS_UPDATE,EP_CustomerFundsUpdateHandler.class.getName(), ApexPages.Severity.ERROR);
    	}
    	return jsonResponse;
    }
    
    /**
	* @author 			Accenture
	* @name				parse
	* @date 			03/13/2017
	* @description 		Method to parse i.e. deserialize the json string into EP_CustomerFundsUpdateStub stub class
	* @param 			String
	* @return 			EP_CustomerFundsUpdateStub
	*/
	@TestVisible
    private static EP_CustomerFundsUpdateStub parse(String json){
		EP_GeneralUtility.Log('private','EP_CustomerFundsUpdateHandler','Parse');
		return (EP_CustomerFundsUpdateStub) System.JSON.deserialize(json, EP_CustomerFundsUpdateStub.class);
	}
	
	/**
	* @author 			Accenture
	* @name				processAccountAvailableFund
	* @date 			03/13/2017
	* @description 		Populates all the lookup fields in the wrapper instance and returns the appropriate datasets
	* @param 			List<EP_CustomerFundsUpdateStub.Customer>
	* @return 			List<EP_AcknowledgementStub.dataset>
	*/
	@TestVisible
	private static List<EP_AcknowledgementStub.dataset> createAcknowledgementDatasets(List<EP_CustomerFundsUpdateStub.Customer> customers){
		EP_GeneralUtility.Log('private','EP_CustomerFundsUpdateHandler','createAcknowledgementDatasets');
        List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
        EP_AcknowledgementStub.dataset ackDataset = new EP_AcknowledgementStub.dataset();
        List<Account> customersToUpdate = new List<Account>();
        for(EP_CustomerFundsUpdateStub.customer cust : customers){
            if(string.isNotBlank(cust.errorDescription)){
                ackDataset = EP_AcknowledgementUtil.createDataSet(EP_COMMON_CONSTANT.CUSTOMER,cust.seqId,cust.errorDescription,cust.errorDescription);
                ackDatasets.add(ackDataset);
            } else {
                customersToUpdate.add(cust.SFAccount);
                //L4# 67333 Code Changes Start
                if(cust.holdingAccount <> null){
                     customersToUpdate.add(cust.holdingAccount);  
                }
                //L4# 67333 Code Changes End
            }
        }
        
        //doDML(customersToUpdate,ackDatasets); 
        EP_AcknowledgementUtil.doDML(customersToUpdate,ackDatasets);
        return ackDatasets;
	}
}