/*
@Author      CloudSense
@name        ROSchedulingBatch_UT
@CreateDate  07/05/2018
@Description Test class for the Schedulable Job: ROSchedulingBatch
@Version     1.0
*/
@isTest
public with sharing class ROSchedulingBatch_UT {

	@isTest
	public static void testSchedulableBatch() {

		CS_RO_Processing_Settings__c setting = new CS_RO_Processing_Settings__c();
		setting.SetupOwnerId=UserInfo.getOrganizationId();
		setting.Batch_Size__c = 1;
		setting.Executions_Per_Day__c = 24;

		INSERT setting;

		Test.startTest();
		Datetime dt = Datetime.now().addMinutes(1);

		System.schedule('ROSchedulingBatchTest', '0 ' + dt.minute() + ' * * * ?', new ROSchedulingBatch());
		Test.stopTest();

		INTEGER total = Database.countQuery('SELECT count() FROM CronTrigger WHERE CronJobDetail.Name = \'ROSchedulingBatchTest\'');
		System.assertEquals(1, total, 'No ROSchedulingBatchTest was scheduled');

		INTEGER totalBatchs = Database.countQuery('SELECT count() FROM CronTrigger WHERE CronJobDetail.Name like \'EP_RetrospectiveOrderBatch%\'');
		System.assertNotEquals(0, totalBatchs, 'No EP_RetrospectiveOrderBatch was scheduled');
	}
}