/* 
   @Author          Accenture
   @name            EP_StagingRetailOrderStub
   @CreateDate      13/04/2018
   @Description     Stub class to create or file and Staging for Retail Order i.e. inbound interface to create or update staging object record
   @Version         1.0
*/
public class EP_StagingRetailOrderStub{
    public MSG MSG;
    
    /**
    * @author           Accenture
    * @name             EP_StagingRetailOrderStub
    * @date             13/04/2018
    * @description      Constructor
    * @param            NA
    * @return           NA
    */
    public EP_StagingRetailOrderStub(){
        MSG = new MSG();
    }
    
   
    /* 
    @Author          Accenture
    @name            MSG 
    @CreateDate      13/04/2018
    @Description     Stub class of MSG
    @Version         1.0
    */
    public class MSG {
        public EP_MessageHeader HeaderCommon;  
        public Payload payload;
        public String StatusPayload;
        /*Constructor*/
        public MSG(){
            HeaderCommon = new EP_MessageHeader();
            payload = new payload();
          }
    }
    
     /* 
    @Author          Accenture
    @name            Any0 
    @CreateDate      13/04/2018
    @Description     Stub class of Any0
    @Version         1.0
    */
    public class Any0 {
        public SalesData salesData;
        /*Constructor*/
        public Any0(){
            salesData = new SalesData();
        }
        
    }
    
     /* 
    @Author          Accenture
    @name            MSG 
    @CreateDate      13/04/2018
    @Description     Stub class of Payload
    @Version         1.0
    */
    public class Payload {
        public Any0 any0;
        /*Constructor*/
        public Payload(){
            any0= new Any0();
        }
    }
    
    /* 
    @Author          Accenture
    @name            MSG 
    @CreateDate      13/04/2018
    @Description     Stub class of SalesData
    @Version         1.0
    */ 
    public class SalesData {
        public Header header;
        public Lines lines;
        /*Constructor*/
        public SalesData(){
            header = new Header();
            lines = new Lines();
        }
    }
    
    /* 
    @Author          Accenture
    @name            MSG 
    @CreateDate      13/04/2018
    @Description     Stub class of Header
    @Version         1.0
    */
    public class Header {
        public String companyCode ;
        public String transactionNr ;
        public String senderCode ;
        public String sellToId ;
        public String locationId ;
        public String posLocationId ;
        public String transactionDt ;
        public String createdDt ;
        public String createdBy ;
        public String fileName ;    
        public String processingStatus;
		public String preocessingDesc; 
    }

    /* 
    @Author          Accenture
    @name            MSG 
    @CreateDate      13/04/2018
    @Description     Stub class of Lines
    @Version         1.0
    */
    public class Lines {
        public list<Line> line;

        /*Constructor*/
        public Lines(){
            line = new list<Line>();
        }
    }
    
    /* 
    @Author          Accenture
    @name            MSG 
    @CreateDate      13/04/2018
    @Description     Stub class of Line
    @Version         1.0
    */
    public class Line {
        public String lineNr ; 
        public String itemId ; 
        public String descreption ;
        public String qty ; 
        public String unitPrice ;
        public String processingStatus;
		public String preocessingDesc; 
    }   
    
}