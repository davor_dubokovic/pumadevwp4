@isTest
private class EP_GenerateOrderRequestXML_UT {
   private static final string MESSAGE_TYPE = 'SEND_ORDER_CREDIT_CHECK_IN_PROGRESS';
   
   //#60147 User Story Start
   @testSetup
   public static void init(){
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
   }
   //#60147 User Story End
   static testMethod void createXML_Test() {
       EP_GenerateOrderRequestXML localObj = new EP_GenerateOrderRequestXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       String result = localObj.createXML();  
       Test.stopTest();
       System.assert(!String.isBlank(result));
   }

   static testMethod void init_Test() {
       EP_GenerateOrderRequestXML localObj = new EP_GenerateOrderRequestXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.recordid = localObj.OrderObj.id;
       Test.startTest();
       localObj.init();  
       Test.stopTest();
       //This method delegates to other methods, adding dummy assert
       system.assert(true);
   }

   static testMethod void getValueforNodeString_Test() {
       EP_GenerateOrderRequestXML localObj = new EP_GenerateOrderRequestXML();
       String message = null;
       Test.startTest();
       String result = localObj.getValueforNode(message);  
       Test.stopTest();
       System.assertEquals(result, '');
   }

   static testMethod void getValueforNodeBoolean_Test() {
       EP_GenerateOrderRequestXML localObj = new EP_GenerateOrderRequestXML();
       Boolean message = null;
       Test.startTest();
       String result = localObj.getValueforNode(message);  
       Test.stopTest();
       System.assertEquals(result, 'false');
   }   

   static testMethod void getValueforNodeDecimal_PositiveTest() {
       EP_GenerateOrderRequestXML localObj = new EP_GenerateOrderRequestXML();
       Decimal message = null;
       Test.startTest();
       String result = localObj.getValueforNode(message);  
       Test.stopTest();
       System.assertEquals(result, '');
   }

   static testMethod void getValueforNodeDecimal_NegativeTest() {
       EP_GenerateOrderRequestXML localObj = new EP_GenerateOrderRequestXML();
       Decimal message = 10.4;
       Test.startTest();
       String result = localObj.getValueforNode(message);  
       Test.stopTest();
       System.assertEquals(result, '10.4');
   }       

   static testMethod void createHeaderNode_Test() {
       EP_GenerateOrderRequestXML localObj = new EP_GenerateOrderRequestXML();
       localObj.messageType = MESSAGE_TYPE;
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       Test.startTest();
       localObj.init();
       localObj.createHeaderNode();  
       Test.stopTest();
       //This method delegates to other methods, adding dummy assert
       system.assert(true);
   }
   
   static testMethod void createPayload_Test() {
       EP_GenerateOrderRequestXML localObj = new EP_GenerateOrderRequestXML();
       Test.startTest();
       localObj.createPayload();  
       Test.stopTest();
       //No assert  required, as empty method, adding dummy assert       
       system.assert(true);
   }
   
   static testMethod void createStatusPayLoad_Test() {
       EP_GenerateOrderRequestXML localObj = new EP_GenerateOrderRequestXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.messageType = MESSAGE_TYPE;
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       Test.startTest();
       localObj.init();
       localObj.createStatusPayLoad();  
       Test.stopTest();
       //No assert  required, as empty method, adding dummy assert       
       system.assert(true);
   }

   static testMethod void formatDateAsStringDate_Test() {
       Date inputDate = System.today();
       EP_GenerateOrderRequestXML localObj = new EP_GenerateOrderRequestXML();
       Test.startTest();
       String result = localObj.formatDateAsString(inputDate);  
       Test.stopTest();
       System.assertNotEquals(result, EP_Common_Constant.BLANK);
   }

    static testMethod void formatDateAsStringDateTime_Test() {
       DateTime inputDate = System.now();
       EP_GenerateOrderRequestXML localObj = new EP_GenerateOrderRequestXML();
       Test.startTest();
       String result = localObj.formatDateAsString(inputDate);  
       Test.stopTest();
       System.assertNotEquals(result, EP_Common_Constant.BLANK);
   }   

   
   static testMethod void getShipToOfOrderElse_Test() {
       Date inputDate = System.today();

       EP_GenerateOrderRequestXML localObj = new EP_GenerateOrderRequestXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       Test.startTest();
       String result = localObj.getShipToOfOrder();  
       Test.stopTest();
       System.assertNotEquals(result, EP_Common_Constant.BLANK);
   }
   
   
   static testMethod void getShipToOfOrderIf_Test() {
       Date inputDate = System.today();
       csord__Order__c orderObj = EP_TestDataUtility.getTransferOrder();
       orderObj.recordTypeId = [Select Id from recordType where developerName = : EP_Common_Constant.TRANSFER_ORDER_RECORD_TYPE_DEV_NAME limit 1][0].id;
       update orderObj;
       
       EP_GenerateOrderRequestXML localObj = new EP_GenerateOrderRequestXML();
       EP_OrderMapper ordermapper = new EP_OrderMapper();
       localObj.orderobj = ordermapper.getCSRecordById(orderObj.id);  

       Test.startTest();
       String result = localObj.getShipToOfOrder();  
       Test.stopTest();
       System.assertNotEquals(result, EP_Common_Constant.BLANK);
   }
   
        

   static testMethod void setOrderLineItems_Test() {
       EP_GenerateOrderRequestXML localObj = new EP_GenerateOrderRequestXML();
       EP_OrderMapper ordermapper = new EP_OrderMapper();
       csord__Order__c orderObj = EP_TestDataUtility.getTransferOrderWithInvoiceLineItem();
       localObj.orderobj = ordermapper.getCSRecordById(orderObj.id);
       Test.startTest();
       localObj.setOrderLineItems();  
       Test.stopTest();
       //This method delegates to other methods, adding dummy assert
       system.assert(true);
   }

   static testMethod void setOrderItems_Test() {
       EP_GenerateOrderRequestXML localObj = new EP_GenerateOrderRequestXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       Test.startTest();
       localObj.setOrderItems();  
       Test.stopTest();
       //This method delegates to other methods, adding dummy assert
       system.assert(true);
   }
}