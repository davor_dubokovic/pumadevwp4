/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationActiveToInActive>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 05-Active to 07-Inactive>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationActiveToInActive extends EP_AccountStateTransition {

    public EP_ASTStorageLocationActiveToInActive () {
        finalState = EP_AccountConstant.INACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationActiveToInActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationActiveToInActive', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationActiveToInActive',' isGuardCondition');        
        return true;
    }
}