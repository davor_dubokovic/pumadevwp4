/**
 * @author <Accenture>
 * @name <EP_PortalOrderUtil>
 * @description <This class contains utility methods for Portal Order> 
 * @version <1.0>
 */
global without sharing class EP_PortalOrderUtil {
    
    // Private variables
    private static final String ClassName = 'EP_PortalOrderUtil';
    private static final String createCreditExcRequest_Method = 'createCreditExcRequest_Method';
    private static final String getAvailableDates_Method = 'getAvailableDates';
    private static final String setCustomerPoNumber_Method = 'setCustomerPoNumber';
    private static final String checkPrepayForPoNumber_Method = 'checkPrepayForPoNumber';
    
   /*
    New Method
    */
    public static Map<Id,String> getInventoryMap(List<csord__Order_Line_Item__c> orderitemlist, csord__Order__c newOrderRecord,String strSelectedPickupLocationID){
            EP_GeneralUtility.Log('Public','EP_PortalOrderUtil','getInventoryMap');
            Set<Id> setProdIds = new Set<Id>();
            Set<Id> setPricebookEntryIds = new Set<Id>();  
            Map<Id,String> inventoryMap = new Map<Id,String>();

            //Looping through the Order Lines
            for(csord__Order_Line_Item__c orderItemObj : orderitemlist) {
                //Adding PriceEntry Id 
                setPricebookEntryIds.add(orderItemObj.PricebookEntryId__c);
            }
            //Looping through the Pricebook entries
            for(PricebookEntry priceBookEntryObj : new EP_PricebookEntryMapper().getRecordsByPricebookEntry(setPricebookEntryIds)) {
                //Adding ProductId
                setProdIds.add(priceBookEntryObj.Product2Id);
            }
            //Get Stock Holding Location
            List<EP_Stock_Holding_Location__c> listStockHoldingLocation = new EP_StockHoldingLocationMapper().getActiveStockHoldingLocationBySellToId(newOrderRecord.AccountId__c);
            if(!listStockHoldingLocation.isEmpty()) {
                EP_Stock_Holding_Location__c stockholdLocationObj = listStockHoldingLocation[0];
            }
            List<EP_Inventory__c> inventoryList = new EP_InventoryMapper().getInventoryByProdId(setProdIds, newOrderRecord.EP_Stock_Holding_Location__c);
            // Get the Inventory records and looping through
            Integer invAvailable = 0;
                if(!inventoryList.isEmpty()){
                    for(EP_Inventory__c inv : inventoryList){
                        if(inv.EP_Inventory_Availability__c == EP_Common_Constant.LOW_INV){
                            invAvailable = 2;
                            inventoryMap.put(inv.EP_Product__c,inv.EP_Inventory_Availability__c);
                            break;
                        } 
                        else if ( inv.EP_Inventory_Availability__c == EP_Common_Constant.NO_INV){
                            invAvailable = 3;
                            inventoryMap.put(inv.EP_Product__c,inv.EP_Inventory_Availability__c);
                            break;
                        }
                        else if(inv.EP_Inventory_Availability__c == EP_Common_Constant.GOOD_INV){
                            invAvailable = 1;
                            inventoryMap.put(inv.EP_Product__c,inv.EP_Inventory_Availability__c);
                            continue;
                        }
                        else{
                            continue;
                        }
                    }
                }      
            return inventoryMap;
    }
    
     //#defectFix 50752 start
    /*
     *   This method is used to check whether order or Account already having CER in Open/Pending Approval State. If it exist, don't allow to create the new CER on Order 
     */
    webservice static String validateExistingCEROnOrder(Id orderId,Id accountId){  
      EP_GeneralUtility.Log('Public','EP_PortalOrderUtil','validateExistingCEROnOrder');
      String strErrorMsg = '';
      List<EP_Credit_Exception_Request__c> creditExceptionReqtsforBillToRelatedCR= new List<EP_Credit_Exception_Request__c>();
      creditExceptionReqtsforBillToRelatedCR = [SELECT Id,EP_CS_Order__c,EP_Status__c,EP_Bill_To__c FROM EP_Credit_Exception_Request__c WHERE EP_Bill_To__c =: accountId AND (EP_Status__c =: EP_Common_Constant.CREDIT_EXC_STATUS_OPEN  OR EP_Status__c =: EP_Common_Constant.CREDIT_EXC_STATUS_PENDING_APPROVAL) ]; 

      for (EP_Credit_Exception_Request__c objCreditExceptionRequest : creditExceptionReqtsforBillToRelatedCR) {
        if (objCreditExceptionRequest.EP_CS_Order__c == orderId) {
          strErrorMsg = Label.EP_Restricted_User_From_Creating_CR_For_Same_Order;
          break;  
        }
      }

      if (strErrorMsg =='' && creditExceptionReqtsforBillToRelatedCR.size() > 0 ){
        strErrorMsg = Label.EP_Restricted_User_From_Creating_CR_For_Same_Account;
      }

      return strErrorMsg;
    }
    //#defectFix 50752 end
     
     /*
        This method is used to excecute three strike rule
    */
    webservice static String createCreditExcRequest(Id orderId,Id accountId){  
        EP_GeneralUtility.Log('Public','EP_PortalOrderUtil','createCreditExcRequest');
        Account billToAccount = new EP_AccountMapper().fetchBillToAccountDetail(accountId); 
        csord__Order__c ord  = new EP_OrderMapper().getCsRecordById(orderId); //Defect 57278
        EP_Credit_Exception_Request__c creditExcRec = new EP_Credit_Exception_Request__c();
        Boolean overDueInvoice = EP_PortalOrderHelper.validateOverdueInvoice(billToAccount.Id);    
        EP_OrderDomainObject orderDomainObject = new EP_OrderDomainObject(ord); //Defect 57278
        EP_OrderService orderService = new EP_OrderService(orderDomainObject); //Defect 57278
        
        creditExcRec.EP_CS_Order__c = orderId;
        creditExcRec.EP_Bill_To__c=billToAccount.Id;
        creditExcRec.CurrencyIsoCode=ord.CurrencyIsoCode;
        creditExcRec.EP_Current_Order_Value__c=ord.EP_Total_Price__c;
        creditExcRec.EP_Request_Date__c = System.today();
        /* CAM 2.7 start */
        creditExcRec.EP_Available_Funds__c = billToAccount.EP_AvailableFunds__c;
        /* CAM 2.7 end */
        creditExcRec.EP_Overdue_Invoices__c = overDueInvoice;
        creditExcRec.EP_Total_Value_of_Open_Orders_in_SFDC__c = orderService.getCreditAmount(); //Defect 57278
        try{
            insert creditExcRec;
        }
        catch (exception exp){
              EP_LoggingService.logHandledException(exp, EP_Common_Constant.EPUMA, 
                createCreditExcRequest_Method, ClassName, ApexPages.Severity.ERROR); 
        }
        System.debug('exc created##'+creditExcRec);
        return creditExcRec.Id;
    } 

     /**
     * @author Accenture
     * @date 08/01/2016
     * @description This method validates Invoices which are overdue.
     */
    webservice static Boolean validateOverdueInvoice(String currentRecordId) {
        EP_GeneralUtility.Log('Public','EP_PortalOrderUtil','validateOverdueInvoice');
        EP_AccountDomainObject accountDomainObject = new EP_AccountDomainObject(currentRecordId);
        EP_AccountService accountService = new EP_AccountService(accountDomainObject);
        return accountService.isOverDueInvoice();
    }
        
    /*
    Get Available Dates
    */
    //Defect 43408
     public static void getAvailableDates(EP_FE_NewOrderInfoResponse response,EP_Stock_Holding_Location__c stockLoc){
        EP_GeneralUtility.Log('Public','EP_PortalOrderUtil','getAvailableDates');
        DateTime dtTomorrow;
        DateTime dtEndDate;
        DateTime dt;
        Datetime dtTargetOpenDateTime;
        Time tOpeningTime;
        Time tClosingTime;
        BusinessHours shlBusinessHours;
        Integer j = 0;//Defect 43408
        EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime dateOpeningTime;
        EP_FE_NewOrderInfoResponse.EP_FE_TerminalInfo terminalInfoRecord;
        EP_FE_NewOrderInfoResponse.EP_FE_TimePeriod timePeriodRecord;
        List < EP_FE_NewOrderInfoResponse.EP_FE_TerminalInfo > terminalInfoRecords = new List < EP_FE_NewOrderInfoResponse.EP_FE_TerminalInfo > ();
        List < EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime >  dateOpeningTimes = new List < EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime > ();
         //EP_Stock_Holding_Location__c stockLoc = [ Select Id,Name,Stock_Holding_Location__r.EP_Slotting_Enabled__c,Stock_Holding_Location__r.EP_Business_Hours__c from EP_Stock_Holding_Location__c where Id=:stockHoldingId limit 1];
         Map < String, BusinessHours > shlBusinessHoursMap = new Map < String, BusinessHours > ([SELECT Id FROM BusinessHours WHERE Id =: stockLoc.Stock_Holding_Location__r.EP_Business_Hours__c limit 1]);
         try{
             if (stockLoc.Stock_Holding_Location__r.EP_Business_Hours__c <> null) {
                system.debug('****'+system.now());
                dtTomorrow = system.now();//Defect 43408
                if(!stockLoc.EP_Sell_To__r.EP_Allow_Same_Day_Pickup__c )
                dtTomorrow = dtTomorrow.addDays(1);
                dtEndDate = dtTomorrow.addDays(5);
                dt = dtTomorrow;
                j = system.now().hour();
                if (shlBusinessHoursMap != NULL) {
                    do {
                        if (shlBusinessHoursMap.containsKey(stockLoc.Stock_Holding_Location__r.EP_Business_Hours__c)) {
                            shlBusinessHours = shlBusinessHoursMap.get(stockLoc.Stock_Holding_Location__r.EP_Business_Hours__c);

                            system.debug('------'+dt+'-'+system.now().hour()+'--'+system.now()+'----'+EP_PortalLibClass_R1.convertDateTimeToDate(system.now()));
                            
                            for (Integer i = j; i <24; i++) {
                            system.debug('---'+i);
                                dtTargetOpenDateTime = Datetime.newInstance(dt.Year(), dt.Month(),dt.Day() , i, 0, 0);
                                system.debug(system.now().day()+'-'+dt.Year()+'-'+dt.Month()+'-'+dt.Day()+'----'+dtTargetOpenDateTime);            
                                if (BusinessHours.isWithin(shlBusinessHours.Id, dtTargetOpenDateTime)) {
                                    dateOpeningTime = new EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime(
                                    date.newinstance(dt.Year(), dt.Month(),dt.Day()));
                                    tOpeningTime = Time.newInstance(i, 0, 0, 0);
                                    tClosingTime = Time.newInstance(i, 59, 59, 0);
                                    timePeriodRecord = new EP_FE_NewOrderInfoResponse.EP_FE_TimePeriod(tOpeningTime, tClosingTime);
                                    dateOpeningTime.timePeriods.add(timePeriodRecord);
                                   //system.debug('****'+dateOpeningTime +'66666----'+j); 
                                   system.debug('**dateOpeningTime**'+dateOpeningTime);
                                   if (dateOpeningTime <> Null && dateOpeningTime.timePeriods<> NULL && dateOpeningTime.timePeriods.size() > 0) {
                                        dateOpeningTimes.add(dateOpeningTime);
                                    }
                                }
                            } // End for 
                            j = 0;
                        } // End map check
                        dt = dt.addDays(1);
                    } while (dt < dtEndDate);
                } // End map check
            } // End business hours check
          }              
          catch (exception exp){
                EP_LoggingService.logHandledException(exp, EP_Common_Constant.EPUMA, 
                getAvailableDates_Method, ClassName, ApexPages.Severity.ERROR);   
          }
      terminalInfoRecord = new EP_FE_NewOrderInfoResponse.EP_FE_TerminalInfo();
     terminalInfoRecord.availableDateTimeList = dateOpeningTimes;
     terminalInfoRecords.add(terminalInfoRecord);
     response.terminalInfoList = terminalInfoRecords;
          
    }
    
  
      
  /*
      This method is used to get contracts  // RO Work
  */
  public static Map<Id,List<Contract>> fetchContracts(List<Account> lstSuppliers){
     EP_GeneralUtility.Log('Public','EP_PortalOrderUtil','fetchContracts');
     Map<Id,List<Contract>> contractMap = new Map<Id,List<Contract>>();
     for(Contract contractObj : [SELECT Id,Name,ContractNumber,Status,RecordType.DeveloperName,AccountId,StartDate,EndDate 
                            FROM Contract 
                            WHERE AccountId IN:lstSuppliers 
                            AND Status =: EP_Common_Constant.EP_CONTRACT_STATUS_ACTIVE]){
        if(contractMap.containsKey(contractObj.AccountId)) {
            contractMap.get(contractObj.AccountId).add(contractObj); 
        }
        else{
            contractMap.put(contractObj.AccountId,new List<Contract>{contractObj});
        }
     }
     return contractMap;
  }      
      /*
      * Packaged Goods Work
      */
      public static Map<String,csord__Order__c> relatedBulkOrderMap(String strAccountId) {   
        EP_GeneralUtility.Log('Public','EP_PortalOrderUtil','relatedBulkOrderMap');  
        Set<String>setStatus= new Set<String>();
        setStatus.add(EP_Common_Constant.delivered);
        setStatus.add(EP_Common_Constant.INVOICED_STATUS);
        setStatus.add(EP_Common_Constant.STATUS_CANCELLED);
        Map<String,csord__Order__c> bulkOrderMap = new Map<String,csord__Order__c>();

        for(csord__Order__c orderObj : new EP_OrderMapper().getCsOrderByAccountAndProductCategory(strAccountId,EP_Common_Constant.PRODUCT_BULK,setStatus)) { 
            bulkOrderMap.put(orderObj.OrderNumber__c,orderObj);        
        }

        return bulkOrderMap;
      }
      
      /*
      * Packaged Goods Work
      */
    public static Map<String,csord__Order__c> orgPckgdOrderMap(String strAccountId) {
        EP_GeneralUtility.Log('Public','EP_PortalOrderUtil','orgPckgdOrderMap');   
        Map<String,csord__Order__c> orgPckgdOrdrMap = new Map<String,csord__Order__c>();
        for(csord__Order__c orderObj : new EP_OrderMapper().getCSOrderByAccountAndProductCategory(strAccountId,EP_Common_Constant.PRODUCT_BULK,null)) {
            orgPckgdOrdrMap.put(orderObj.OrderNumber__c,orderObj);        
        }
        return orgPckgdOrdrMap;
    }
      
       /* 
          This method is used to set customer PO Number on Order
      */

      //TODO: Refactoring - Not part of 3.2
      public static void setCustomerPoNumber(csord__Order__c newOrderRecord) { 
            EP_GeneralUtility.Log('Public','EP_PortalOrderUtil','setCustomerPoNumber');       
            Account sellToAccountDetail = [SELECT id,EP_Customer_PO_Number__c,EP_Valid_From_date__c,EP_Valid_To_date__c FROM Account Where id =: newOrderRecord.AccountId__c  LIMIT : EP_COMMON_CONSTANT.ONE];
            Date todayDate = EP_PortalLibClass_R1.returnLocalDate(System.today());
            try{
                if(newOrderRecord.EP_Delivery_Type__c != NULL && !newOrderRecord.EP_Delivery_Type__c.equalsIgnoreCase(EP_Common_Constant.EX_RACK)){  // Condition for non ex-rack orders
                  if(newOrderRecord.EP_ShipTo__c != NULL){     // check if has ship to  
                        Account shipToAccountDetail = [SELECT id,EP_Customer_PO_Number__c,EP_Valid_From_date__c,EP_Valid_To_date__c FROM Account Where id =: newOrderRecord.EP_ShipTo__c  LIMIT : EP_COMMON_CONSTANT.ONE ];
                          System.debug('EP_Valid_From_date__c##'+shipToAccountDetail.EP_Valid_From_date__c+' and EP_Valid_To_date__c##'+shipToAccountDetail.EP_Valid_To_date__c+' and System.today()##'+System.today());
                        
                        if(shipToAccountDetail.EP_Customer_PO_Number__c != NULL && shipToAccountDetail.EP_Valid_To_date__c <> NULL && shipToAccountDetail.EP_Valid_To_date__c >= todayDate  && shipToAccountDetail.EP_Valid_From_date__c <> NULL && shipToAccountDetail.EP_Valid_From_date__c <= todayDate ){ // check if ship to has Customer PO Number and in the validity period
                            newOrderRecord.EP_Customer_PO_Number__c = shipToAccountDetail.EP_Customer_PO_Number__c;
                         }
                    }
                    if(String.isBlank(newOrderRecord.EP_Customer_PO_Number__c)){
                     //  Account sellToAccountDetail = [SELECT id,EP_Customer_PO_Number__c,EP_Valid_From_date__c,EP_Valid_To_date__c FROM Account Where id =: newOrderRecord.Account];
                       if(sellToAccountDetail.EP_Customer_PO_Number__c != NULL && sellToAccountDetail.EP_Valid_To_date__c <> NULL && sellToAccountDetail.EP_Valid_To_date__c >= todayDate  && sellToAccountDetail.EP_Valid_From_date__c <> NULL && sellToAccountDetail.EP_Valid_From_date__c <= todayDate ){
                            newOrderRecord.EP_Customer_PO_Number__c = sellToAccountDetail.EP_Customer_PO_Number__c;
                        }
                    }  
               }
                else{
                        if(sellToAccountDetail.EP_Customer_PO_Number__c != NULL && sellToAccountDetail.EP_Valid_To_date__c <> NULL && sellToAccountDetail.EP_Valid_To_date__c >= todayDate  && sellToAccountDetail.EP_Valid_From_date__c <> NULL && sellToAccountDetail.EP_Valid_From_date__c <= todayDate ){
                            newOrderRecord.EP_Customer_PO_Number__c = sellToAccountDetail.EP_Customer_PO_Number__c;
                        }
                                                                                }
            }
            catch (exception exp){
                EP_LoggingService.logHandledException(exp, EP_Common_Constant.EPUMA, 
                setCustomerPoNumber_Method, ClassName, ApexPages.Severity.ERROR);     
            }
      }
      
        /* 
          This method is used to set customer PO Number on Order
      */
      public static boolean checkPrepayForPoNumber(csord__Order__c newOrderRecord,Boolean blnIsPrepaymentCustomer,Boolean blnIsConsignmentOrder,Boolean blnIsConsumptionOrder,Boolean isDummy,Account orderAccount)
      {
          EP_GeneralUtility.Log('Public','EP_PortalOrderUtil','checkPrepayForPoNumber');       
          Boolean showErrorMsg = false;
          try{
               if(blnIsPrepaymentCustomer && (!blnIsConsignmentOrder || blnIsConsumptionOrder) && !isDummy && orderAccount.EP_Is_Customer_Reference_Mandatory__c && String.isBlank(newOrderRecord.EP_Customer_PO_Number__c)){
                  showErrorMsg = true;
                } 
           } 
           
           catch (exception exp){
               EP_LoggingService.logHandledException(exp, EP_Common_Constant.EPUMA, 
                checkPrepayForPoNumber_Method, ClassName, ApexPages.Severity.ERROR);   
           }
            return showErrorMsg;
      }
      
      /*
    Get delivery start date
    */
    //Defect 43407
     public static string getDeliveryStartDate(EP_Stock_Holding_Location__c stockLoc){
        EP_GeneralUtility.Log('Public','EP_PortalOrderUtil','getDeliveryStartDate');       
        Date deliveryStartDate,dateOpeningTime;
        DateTime estimatedDeliveryDateTime;
        BusinessHours shlSchedulingHours;
        
        Map < String, BusinessHours > shlSchedulingHoursMap = new Map < String, BusinessHours > ([SELECT Id FROM BusinessHours WHERE Id =: stockLoc.Stock_Holding_Location__r.EP_Scheduling_Hours__c limit 1]);

       if (shlSchedulingHoursMap != null && stockLoc.Stock_Holding_Location__r.EP_Scheduling_Hours__c <> null  && shlSchedulingHoursMap.containsKey(stockLoc.Stock_Holding_Location__r.EP_Scheduling_Hours__c)) {
          // preparing estimate date time from current time, adding trip durating days
          estimatedDeliveryDateTime = system.now();
          estimatedDeliveryDateTime = estimatedDeliveryDateTime.addDays(Integer.valueOf(stockLoc.EP_Trip_Duration__c));
          //dt = estimatedDeliveryDateTime;
          shlSchedulingHours = shlSchedulingHoursMap.get(stockLoc.Stock_Holding_Location__r.EP_Scheduling_Hours__c);
          deliveryStartDate = getEstimatedDeliveryDateWithinBusinessHours(estimatedDeliveryDateTime,shlSchedulingHours);
        } // End map check and business hrs check
                
       return string.valueOf(deliveryStartDate); 
    }

    private static Date getEstimatedDeliveryDateWithinBusinessHours(DateTime estimatedDeliveryDateTime,BusinessHours shlSchedulingHours){
      EP_GeneralUtility.Log('Public','EP_PortalOrderUtil','getEstimatedDeliveryDateWithinBusinessHours');       
      Date businessDayDt;
      DateTime targetDeliveryDateTime;
      Integer currentHour = system.now().hour(); 
      for (Integer iteratingHour = currentHour; iteratingHour < 24; iteratingHour++) {
        targetDeliveryDateTime = Datetime.newInstance(estimatedDeliveryDateTime.Year(), estimatedDeliveryDateTime.Month(),estimatedDeliveryDateTime.Day() , iteratingHour, 0, 0);
        system.debug(estimatedDeliveryDateTime+'----'+targetDeliveryDateTime+shlSchedulingHours.Id);            
        if (BusinessHours.isWithin(shlSchedulingHours.Id, targetDeliveryDateTime)) {
            businessDayDt = targetDeliveryDateTime.date();
            system.debug('----'+businessDayDt);
           break;
        }
        currentHour = 0; 
      }
      businessDayDt = (businessDayDt==null)?getEstimatedDeliveryDateWithinBusinessHours(estimatedDeliveryDateTime.addDays(1),shlSchedulingHours): businessDayDt;
      return businessDayDt;
    }

    /** this method return Available Products Quantities Map
     *  @date      02/03/2017
     *  @name      getAvailableProductQuantitiesMap
     *  @param     List<EP_Order_Configuration__c> listOrderConfigurations
     *  @return    Map<String,List<String>>
     *  @throws    NA
     */
     public static Map<String,List<String>> getAvailableProductQuantitiesMap (List<EP_Order_Configuration__c> listOrderConfigurations) {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageUtil','getAvailableProductQuantitiesMap');
        Map<String,List<String>> availableProductQuantitiesMap = new Map<String,List<String>>();

        for (EP_Order_Configuration__c orderConfigObj : listOrderConfigurations) {
            String productQuantityKey = constructProductQuantityLimitKey(orderConfigObj.EP_Product__c, orderConfigObj.EP_Volume_UOM__c);
            if (EP_Common_Constant.RT_NON_MIXING_VALUE.equalsIgnoreCase(orderConfigObj.RecordType.DeveloperName)) {
                if (availableProductQuantitiesMap.containsKey(productQuantityKey)) {
                    availableProductQuantitiesMap.get(productQuantityKey).add(String.valueOf(orderConfigObj.EP_Min_Quantity__c));
                }
                else {
                    availableProductQuantitiesMap.put(productQuantityKey,new List<String>{String.valueOf(orderConfigObj.EP_Min_Quantity__c)});
                }
            } 
        }
        return availableProductQuantitiesMap;
    }   


    /** this method return Available Products Range Map
     *  @date      02/03/2017
     *  @name      getProductRangeMap
     *  @param     List<EP_Order_Configuration__c> listOrderConfigurations
     *  @return    Map<String,String>
     *  @throws    NA
     */
     public static Map<String,String> getProductRangeMap (List<EP_Order_Configuration__c> listOrderConfigurations) {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageUtil','getProductRangeMap');
        Map<String,String> productRangeMap = new Map<String,String>();
        for (EP_Order_Configuration__c orderConfigObj : listOrderConfigurations) {
            String productQuantityKey = constructProductQuantityLimitKey(orderConfigObj.EP_Product__c, orderConfigObj.EP_Volume_UOM__c); 
            if (EP_Common_Constant.RT_MIXING_RANGE.equalsIgnoreCase(orderConfigObj.RecordType.DeveloperName)) {
                productRangeMap.put(productQuantityKey, orderConfigObj.EP_Min_Quantity__c + EP_Common_Constant.HYPHEN_STRING + orderConfigObj.EP_Max_Quantity__c);
            } 
            else if (EP_Common_Constant.RT_COUNTRY_RANGE.equalsIgnoreCase(orderConfigObj.RecordType.DeveloperName)) {
                if (!productRangeMap.containsKey(productQuantityKey)) {
                    productRangeMap.put(productQuantityKey, orderConfigObj.EP_Min_Quantity__c + EP_Common_Constant.HYPHEN_STRING + orderConfigObj.EP_Max_Quantity__c);
                }
            }
        }
        return productRangeMap;
    }
        

    /** this method return Available Products Range Map
     *  @date      02/03/2017
     *  @name      getProductRangeMap
     *  @param     List<EP_Order_Configuration__c> listOrderConfigurations
     *  @return    Map<String,String>
     *  @throws    NA
     */
     public static string constructProductQuantityLimitKey(String strProductID, String strUoM) {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageUtil','constructProductQuantityLimitKey');
        return strProductID + EP_Common_Constant.UNDERSCORE_STRING + strUoM;
    } 

    public static List<EP_Country_Region_Group_Mapping__mdt> getCountryRegionGroupMapping(){
      return [SELECT EP_Group_Name__c, EP_Region__c FROM EP_Country_Region_Group_Mapping__mdt];
    }

    public static Map<Id,EP_Tank__c> getOperationalTanks(List<EP_Tank__c> lstTank) {
      Map<Id,EP_Tank__c> mapOperationalTank = new Map<Id,EP_Tank__c>();
      for (EP_Tank__c tnk : lstTank) {
        if (tnk.EP_Tank_Status__c == EP_Common_Constant.TANK_OPERATIONAL_STATUS) {
          mapOperationalTank.put(tnk.Id,tnk);
        }
      }
      return mapOperationalTank;
    }
}