/*   
     @Author Aravindhan Ramalingam
     @name <EP_OrderStatePaymentReview.cls>     
     @Description <Order State for Awaiting Payment status, common for all order types>   
     @Version <1.1> 
     */

     public class EP_OrderStatePaymentReview extends EP_OrderState {
        
        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            super.setOrderDomainObject(currentOrder);
        }

        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_OrderStatePaymentReview','doOnEntry');
            
        }
        
        public static String getTextValue()
        {
            return EP_OrderConstant.OrderState_Awaiting_Payment;
        }
    }