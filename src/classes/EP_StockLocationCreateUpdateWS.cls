/* 
   @Author       	Accenture
   @name       		EP_StockLocationCreateUpdateWS
   @CreateDate     	15/12/2017
   @Description    	Web service class to create or modify storage location i.e. inbound interface to create update storage location
   @Version     	1.0
*/
@RestResource(urlMapping='/v1/StockLocationSync/*')
global without sharing class EP_StockLocationCreateUpdateWS {
	/**
  	* @author       Accenture
  	* @name        	UpsertStockLocation
  	* @date       	15/12/2017
  	* @description  Processess the storage location request and sends back the acknowledgement
  	* @param       	NA 
  	* @return       NA
  	*/
    @HttpPost
    global static void UpsertStockLocation(){
      RestRequest req = RestContext.request;
        String requestBody = req.requestBody.toString();
        RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
        EP_IntegrationService service = new EP_IntegrationService();
        string response =service.handleRequest(EP_Common_Constant.NAV_TO_SFDC_STOCK_LOCATION_SYNC,requestBody);
        RestContext.response.responseBody = blob.valueOf(response);
    } 
}