/* 
   @Author Accenture
   @name <EP_LoggingService>
   @CreateDate <23/11/2015>
   @Description <This class will be executed by external system to export exception logs>
   @Version <1.0>
 
*/
@RestResource(urlMapping = '/v1/ExceptionLogs/*')
global with sharing class EP_RetrieveExceptionLogs {
    
    // Const
    private static final String RETRIEVE_EXCEPTION_LOGS = 'retrieveExceptionLogs';
    private static final String EP_RETRIEVE_EXCEPTION_LOGS = 'EP_RetrieveExceptionLogs';
    
    /**************************************************************************
    *@Description : This get method is used to retrieve exception logs by querying from the exception  
                    log object based on the date passed from the url and isExported value should be false                                  *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/       
    @HttpGet
    global static void retrieveExceptionLogs() {
        String strResponse = EP_Common_Constant.BLANK;
        String dateFromURL = EP_Common_Constant.BLANK;
        Savepoint sp = Database.setSavepoint();
        //Initializing ExceptionList will be used to add exception objects after sucessfull callOut and updating to database
        List < EP_Exception_log__C > updatedElogList = new List < EP_Exception_log__C > ();
        
        try {
            JSONGenerator gen = JSON.createGenerator(true);
            //Retrieving date(dateException) value from the url
            dateFromURL= RestContext.request.params.get(EP_Common_Constant.dateException);
            //System.debug('dateFromURL   '+dateFromURL); 
            if(String.isNotBlank(dateFromURL)){
                String[] dts = dateFromURL.split(EP_Common_Constant.hyphen);
                
                //Converting date format to YYYYMMDD  (Expecting date format from URL  should be MMDDYYYY Eg-11-19-2015)
                Date convDate= date.newinstance(Integer.valueOf(dts[2]), Integer.valueOf(dts[0]), Integer.valueOf(dts[1]));
                
                //Querying EP_Exception_log__c  records   based on date and isExported value should be FALSE
                List < EP_Exception_log__c > listExceptions = [Select EP_Application_Name__c, EP_Class_Name__c, EP_Exception_Description__c, EP_Is_Exported__c, EP_Method_Name__c, EP_OrgID__c from EP_Exception_log__C WHERE EP_Is_Exported__c = : FALSE AND CreatedDate >:convDate LIMIT 20000];
                
                if (!listExceptions.isEmpty()) {
                //Converting list of exceptions to JSON format
                    strResponse = JSON.serialize(listExceptions);
                    //Adding  Exception log data to the response string
                   strResponse = EP_Common_Constant.EXCEPTION_LOGS + strResponse + EP_Common_Constant.COMMA_STRING;
                   strResponse += EP_Common_Constant.RESULTS + listExceptions.size() + EP_Common_Constant.RIGHT_CURLY_BRACE;
                    for (EP_Exception_log__C expLog: listExceptions) {
                        expLog.EP_Is_Exported__c = True;
                        updatedElogList.add(expLog);
                    }
                } else {
                //Adding 'No Exception log data' to the response string
                    strResponse = EP_Common_Constant.EXCEPTION_LOGS + EP_Common_Constant.NO_DATA_TO_EXPORT + EP_Common_Constant.COMMA_STRING;
                    strResponse += EP_Common_Constant.RESULTS + listExceptions.size() + EP_Common_Constant.RIGHT_CURLY_BRACE;
    
                }
            } else {
                strResponse = EP_Common_Constant.GENERIC_FILTER_RESPONSE_FALSE;
            }
            
            Database.update(updatedElogList);//Updating Exception log records
            RestContext.response.responseBody = Blob.valueOf(strResponse);
        } catch (Exception ex) {
            Database.rollback(sp);//Roll back when exception occurs
            EP_LoggingService.logServiceException(ex, UserInfo.getOrganizationId(), EP_Common_Constant.EPUMA, RETRIEVE_EXCEPTION_LOGS, EP_RETRIEVE_EXCEPTION_LOGS, EP_Common_Constant.ERROR, UserInfo.getUserId(), EP_Common_Constant.TARGET_SF, EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);
            
        }
    }
}