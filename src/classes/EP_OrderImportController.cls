/*
    @Author          Accenture
    @Name            EP_OrderImportController
    @CreateDate      
    @Description     This is a controller extension for RO Order Import VF page
    @Version         1.0
    @Reference       NA
*/
public with sharing class EP_OrderImportController {
	public EP_OrderImportContext ctx {get;set;}
	public EP_OrderImportHelper csvHelper{get;set;}
	public EP_File__c fileRecord;
    
	/**
     * @author       Accenture
     * @name         EP_OrderImportController
     * @date         04/16/2018
     * @description  Constructor
     * @param        ApexPages.StandardController
     * @return       NA
     */ 
    public EP_OrderImportController(ApexPages.StandardController controller) {
    	EP_GeneralUtility.Log('private','EP_OrderImportController','EP_OrderImportController');
    	fileRecord = (EP_File__c) controller.getRecord();
    	ctx = new EP_OrderImportContext(fileRecord);
    	csvHelper = new EP_OrderImportHelper(ctx);    	
    }
   
    /**
     * @author       Accenture
     * @name         parseCSVFile
     * @date         04/16/2018
     * @description  This method will parse the CSV file and populate the values in the local inner class
     * @param        NA
     * @return       PageReference
     */ 
    public PageReference parseCSVFile() {
    	EP_GeneralUtility.Log('private','EP_OrderImportController','parseCSVFile');
    	try {
    		if(csvHelper.hasValidFile()) {
    			csvHelper.processCSVData();
	    		csvHelper.processImportRequest();
	    		csvHelper.initDataValidation();
    		}
    	} catch (exception exp){
    		ctx.CSVParseSuccess = false;
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,exp.getMessage()));
    		EP_LoggingService.logHandledException(exp, EP_Common_Constant.EPUMA, 'parseCSVFile',EP_OrderImportController.class.getName(), ApexPages.Severity.ERROR);
    	}
   		return null;
    } 
}