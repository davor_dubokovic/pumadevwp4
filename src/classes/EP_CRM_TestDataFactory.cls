@isTest 
public class EP_CRM_TestDataFactory {
    public static final String GEO1 = 'MEAP';
    public static final String GEO2 = 'Australia';
    public static final String GEO3 = 'National';
    public static final String GEO4 = 'National';
    private static final String DEFAULT_CURRENCY_ISO_CODE = 'USD';
    
    public static List < String > getSegmentList() {
        List < String > segmentList = new List < String > ();
        for(SelectOption segmentOption: EP_CRM_ReportUtility.getPickValues(new Opportunity(), 'EP_CRM_Customer_Segment__c', null)) {
            segmentList.add(segmentOption.getValue());
        }
        return segmentList;
    }
    
    private static Map<String, Id> profileIdByName = new Map<String, Id>();
    
    public static Id getProfileIdByName(String profileName) {
        if (String.isBlank(profileName)) {
            throw new EP_CRM_CommonException('Profile Name can not be Blank.');
        }
        if (profileIdByName.containsKey(profileName)) {
            return profileIdByName.get(profileName);
        } else {
            try {
                Id profileId = [SELECT Id FROM Profile WHERE Name =: profileName].Id;
                profileIdByName.put(profileName, profileId);
                return profileId;
            } catch(QueryException qEx) {
                System.debug('ERROR:' + qEx.getMessage() + ', in line' + qEx.getLineNumber());
                throw new EP_CRM_CommonException('Profile Id Not Found Exception.');
            }
        }
    }
    
    public static UserRole createUserRole(String roleName, Id parentRoleId) {
        UserRole testRole = new UserRole();
        testRole.Name = roleName;
        testRole.parentRoleId = parentRoleId;
        /* testRole.ContactAccessForAccountOwner = 'Read';
        testRole.OpportunityAccessForAccountOwner = 'Read';
        testRole.CaseAccessForAccountOwner = 'Read'; */
        return testRole;
    }   
    
    public static User createTestUser(Id roleId, Id profileId, String firstName, String lastName) {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ', '').replace(':', '').replace('-', '');
        // Generate randomInt to be used for generating unique username 
        Integer randomInt = Integer.valueOf(math.rint(math.random() * 1000000));
        // prepare unique userName
        String uniqueName = orgId + dateString + randomInt;
        // prepare User details
        User testUser = new User(firstname = firstName,
            lastName = lastName,
            email = uniqueName + '@test' + orgId + '.org',
            Username = uniqueName + '@test' + orgId + '.org',
            EmailEncodingKey = 'ISO-8859-1',
            Alias = uniqueName.substring(18, 23),
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US',
            ProfileId = profileId,
            UserRoleId = roleId,
            EP_CRM_Geo1__c = GEO1, 
            EP_CRM_Geo2__c = GEO2);
        return testUser;
    }
    
    public static Company__c createPumaCompany(String companyName, User userObj) {
        Company__c companyObj = new Company__c();
        companyObj.Name = companyName;
        companyObj.CurrencyIsoCode = DEFAULT_CURRENCY_ISO_CODE;
        companyObj.EP_CRM_Business_Support_Manager__c = userObj.Id;
        companyObj.EP_CRM_CSC_Supervisor__c = userObj.Id;
        companyObj.EP_CRM_Other_User__c = userObj.Id;
        return companyObj;
    }
    
    public static Account createAccount(String accountName, Company__c pumaCompany) {
        Account accountObj = new Account();
        accountObj.Name = accountName;
        accountObj.EP_CRM_Geo1__c = GEO1;
        accountObj.EP_CRM_Geo2__c = GEO2;
        accountObj.BillingCity = 'Mumbai';
        accountObj.BillingCountry = 'India';
        accountObj.BillingState = 'MH';
        accountObj.BillingStreet = 'LBS';
        accountObj.BillingPostalCode = '400079';
        accountObj.EP_Puma_Company__c = pumaCompany.Id;
        return accountObj;
    }
    
    public static EP_Payment_Method__c createPaymentMethod(Company__c pumaCompany) {
        EP_Payment_Method__c paymentMtd = new EP_Payment_Method__c();
        paymentMtd.Name = 'Cash Payment';
        paymentMtd.EP_Payment_Method_Code__c = 'CSH';
        //paymentMtd.CurrencyIsoCode = 'GBP - British Pound';
        paymentMtd.CurrencyIsoCode = 'USD';
        paymentMtd.EP_Company__c = pumaCompany.Id;
        insert paymentMtd;        
        return paymentMtd;
    }
    
    //08/02/2018 - created as a part of ePuma Alignment 
    public static EP_Payment_Term__c createPaymentTerm(Company__c pumaCompany) {
        EP_Payment_Term__c paymentTerm = new EP_Payment_Term__c();
        paymentTerm.Name = 'Cash Payment';
        paymentTerm.EP_Payment_Term_Code__c= 'CSH';
        //paymentTerm.CurrencyIsoCode = 'GBP - British Pound';
        paymentTerm.CurrencyIsoCode = 'USD';
        paymentTerm.EP_Company__c = pumaCompany.Id;
        insert paymentTerm;        
        return paymentTerm;
    }
    
    public static Opportunity createOpportunity(String opptyName, Account accountObj, EP_Payment_Method__c payMtd, EP_Payment_Term__c payTerm) {
        Opportunity opptyObj = new Opportunity();        
        
        // Prospect Information
        opptyObj.Name = opptyName;
        opptyObj.AccountId = accountObj.Id;
        opptyObj.StageName = 'Prospecting';
        opptyObj.Description = 'Test Opportunity';
        opptyObj.CloseDate = System.today();
        opptyObj.LeadSource = 'Phone';
        opptyObj.Probability = 5;
        opptyObj.EP_CRM_Last_Activity_Date__c = System.now();
        opptyObj.EP_CRM_Geo1__c = GEO1;
        opptyObj.EP_CRM_Geo2__c = GEO2;
        opptyObj.EP_CRM_Geo3__c = GEO3;
        opptyObj.EP_CRM_Geo4__c = GEO4;
        // Qualification Information
        opptyObj.EP_CRM_Customer_Segment__c = 'Retail';
        opptyObj.EP_CRM_Industry__c = 'White flag';
        opptyObj.EP_CRM_Product_Interest_Group__c = 'Fuels;Fuel Card;';
        opptyObj.EP_CRM_Primary_Product_Interest__c = 'Fuels';
        opptyObj.EP_CRM_UoM__c = 'Litre';
        opptyObj.EP_CRM_Volume__c = 500;
        opptyObj.EP_CRM_Annual_volume__c = opptyObj.EP_CRM_Volume__c * 12; 
        opptyObj.EP_CRM_Current_Supplier__c = 'Castrol'; 
        opptyObj.EP_CRM_Current_Supplier_Agreement__c = 'No';
        opptyObj.EP_CRM_Supplier_Agreement_Expiry__c = null; // Must provide value if current supplier agreement is set to Yes
        // Analysis Information
        opptyObj.EP_CRM_Payment_Method__c = payMtd.id;
        opptyObj.Payment_Term__c = payTerm.id;
        //opptyObj.EP_CRM_Requested_Payment_Term__c = 'Prepayment';
        opptyObj.Customer_Value_Add_Propositions_Segment__c = '';
        opptyObj.Customer_Value_Add_Propositions_Product__c = '';
        opptyObj.EP_CRM_Customer_Value_Add_Propositions__c = '';
        opptyObj.Billing_Method__c = 'Per Order';
        opptyObj.Delivery_Type__c = 'Delivery';
        opptyObj.EP_CRM_Equipment_Required__c = true;
        opptyObj.EP_CRM_Equipment__c = 'Test Equipment';
        opptyObj.EP_CRM_HSE_Site_survey__c = true;
        opptyObj.EP_CRM_HSE_Site_Detail__c = 'Test HSE Site Detail';
        opptyObj.EP_CRM_CVP_Detail__c = 'Test CVP Detail';
        opptyObj.EP_CRM_Licenses_And_Permits__c = true;
        opptyObj.EP_CRM_Branding_required__c = true;
        opptyObj.EP_CRM_Branding_detail__c = 'Test Branding Detail';
        opptyObj.EP_CRM_Expected_Contract_Duration__c = '<1 years';
        // Proposal Information
        opptyObj.EP_CRM_Proposal_Submission_Date__c = System.today();
        opptyObj.EP_CRM_Business_Case_Capex_Approved__c = true;
        opptyObj.EP_CRM_Price_Type__c = 'Fixed Price';
        opptyObj.EP_CRM_Business_Case_Detail__c = 'Test Case';
        opptyObj.EP_CRM_Pricing_Approval__c = true;
        opptyObj.EP_CRM_Freight_Rate_Received__c = true;
        opptyObj.EP_CRM_Margin_unit_of_measure__c = 100;
        opptyObj.EP_CRM_DOA_Approved__c = true;
        // Commit Information
        opptyObj.EP_CRM_Credit_Approval_requested__c = true;
        opptyObj.EP_CRM_Credit_Approval_Requested_Date__c = System.today();
        opptyObj.EP_CRM_KYC_Requested__c = true;
        opptyObj.EP_CRM_KYC_Requested_Date__c = System.today();
        opptyObj.Is_Activated_Contract__c = true;
        // Closed Won Information
        opptyObj.EP_CRM_Site_Survey_Completed__c = true;
        opptyObj.EP_CRM_Supplier_Agreement_Executed__c = true;
        opptyObj.EP_CRM_Credit_Approval_Received__c = true;
        opptyObj.EP_CRM_Credit_Approved_Date__c = System.today();
        opptyObj.EP_CRM_KYC_Done__c = true; 
        opptyObj.EP_CRM_KYC_Approved_Date__c = System.today();
        // Closed Lost Information
        opptyObj.EP_CRM_Lost_Reason_Category__c = '';
        opptyObj.EP_CRM_Lost_Reason__c = '';
        return opptyObj;
    }
    
    public static Contract createContract(Opportunity opptyObj) {
        Contract contractObj = new Contract();
        contractObj.Opportunity__r = opptyObj;
        contractObj.AccountId = opptyObj.AccountId;
        contractObj.StartDate = system.today() - 5;
        contractObj.EP_CRM_Contract_Expiry_Date__c = system.today() + 15;
        contractObj.EP_CRM_Contract_Expiry_Notification_Date__c = system.today() + 10;
        contractObj.Status = 'Draft';
        return contractObj;
    }
}