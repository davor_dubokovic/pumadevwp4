/* 
  @Author <CloudSense>
   @name <TrafiguraEditConfigurationController>
   @CreateDate <10/12/2017>
   @Description Class used to forward to Edit Configuration Page
   @Version <1.0>
 */

global class TrafiguraEditConfigurationController {
    
public csord__Order__c  order{get;set;}

     global TrafiguraEditConfigurationController() {
         
     }
     
    /* @name <openEditOrderPage >
     * @date  <10/12/2017>
     * @description <method to forward to the edit configuration page>
     * @param NA
     * @return String: PageReference
    */
     public PageReference openEditOrderPage(){ 
     try{
         order = [SELECT
                        Id, Name,csord__Identification__c,csordtelcoa__Product_Configuration__c, EP_Is_Credit_Exception_Cancelled_Sync__c, Cancellation_Check_Done__c
                   FROM 
                        csord__Order__c
                    WHERE 
                        Id = :ApexPages.currentPage().getParameters().get('id')];
         if(order.Cancellation_Check_Done__c){
	         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Cannot cancel a cancelled Order'));
             PageReference pr = new PageReference(ApexPages.currentPage().getParameters().get('retUrl'));
             return null;
         }
        system.debug('order is :' +order);
        order.EP_Is_Credit_Exception_Cancelled_Sync__c = true;
        update order;
	     
       } catch(Exception handledException){
           EP_loggingService.loghandledException(handledException, EP_Common_Constant.EPUMA, 'openEditOrderPage', 'TrafiguraEditConfigurationController',apexPages.severity.ERROR);
      }
	     
    PageReference pr=new PageReference ('/apex/customconfiguration?configId='+order.csordtelcoa__Product_Configuration__c+'&linkedId='+order.csord__Identification__c);
    return pr;
    }

    /* @name <openEditOrderPageWithId >
     * @date  <10/12/2017>
     * @description <method to forward to the edit configuration page>
     * @param Id
     * @return String: PageReference
    */
     webservice static String openEditOrderPageWithId(Id orderId){
     csord__Order__c order;
     try{    
          order = [SELECT 
                        Id, Name,csord__Identification__c,csordtelcoa__Product_Configuration__c, EP_Is_Credit_Exception_Cancelled_Sync__c, Cancellation_Check_Done__c
                   FROM 
                        csord__Order__c
    
                    WHERE 
                        Id = :orderId];
                       
	      system.debug('order is :' +order);
        
	      order.EP_Is_Credit_Exception_Cancelled_Sync__c = true;
        update order;
       } catch(Exception handledException){
           EP_loggingService.loghandledException(handledException, EP_Common_Constant.EPUMA, 'openEditOrderPageWithId', 'TrafiguraEditConfigurationController',apexPages.severity.ERROR);
      }
     String pr = '/apex/customconfiguration?configId='+String.valueOf(order.csordtelcoa__Product_Configuration__c)+'&linkedId='+String.valueOf(order.csord__Identification__c);
     return pr;
    }

    /* @name <openEditContractOrderPageWithId >
     * @date  <10/12/2017>
     * @description <method to forward to the edit configuration page>
     * @param Id
     * @return String: PageReference
    */
     webservice static String openEditContractOrderPageWithId(Id orderId){
     csord__Order__c order;
     try{    
          order = [SELECT 
                        Id, Name,csord__Identification__c,csordtelcoa__Product_Configuration__c, EP_Is_Credit_Exception_Cancelled_Sync__c, Cancellation_Check_Done__c
                   FROM 
                        csord__Order__c
    
                    WHERE 
                        Id = :orderId];
                       
	      system.debug('order is :' +order);
        
	      order.EP_Is_Credit_Exception_Cancelled_Sync__c = true;
        update order;
       } catch(Exception handledException){
           EP_loggingService.loghandledException(handledException, EP_Common_Constant.EPUMA, 'openEditOrderPageWithId', 'TrafiguraEditConfigurationController',apexPages.severity.ERROR);
      }
     String pr = '/apex/OpenOrderContract?configId=' + String.valueOf(order.csordtelcoa__Product_Configuration__c) +
                '&linkedId=' + String.valueOf(order.csord__Identification__c) +
                '&ScreenFlowName=' + String.valueOf(CS_ORDER_SETTINGS__c.getInstance().OpenOrderContractScreenFlow__c);
     return pr;
    }

}