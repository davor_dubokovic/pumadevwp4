@isTest
public class EP_RouteMapper_UT
{   
    static testMethod void getRecordsByStorageLocation_test() {
        EP_RouteMapper localObj = new EP_RouteMapper();
        Set<String> Str = new Set<String>();
        list<EP_Route__c> listRouteLocation = [SELECT Id,EP_Storage_Location__c FROM EP_Route__c];
        for(EP_Route__c routeMapperObj : listRouteLocation){
            Str.add(routeMapperObj.id);
        }
        Integer limitVal = 1;
        Test.startTest();
        list<EP_Route__c> result = localObj.getRecordsByStorageLocation(Str,limitVal);
        Test.stopTest();
        System.AssertEquals(Str.size(),result.size());
    }
    
    static testMethod void getRecordsByStorageLocation_test2() {
        EP_RouteMapper localObj = new EP_RouteMapper();
        Set<Id> idSet= new Set<Id>();
        list<EP_Route__c> listRouteLocation = [SELECT Id,EP_Storage_Location__c FROM EP_Route__c];
        for(EP_Route__c routeMapperObj : listRouteLocation){
            idSet.add(routeMapperObj.id);
        }
        Test.startTest();
        list<EP_Route__c> result = localObj.getRecordsByStorageLocation(idSet);
        Test.stopTest();
        System.AssertEquals(idSet.size(),result.size());
    }
   
    
    static testMethod void getRoutesWithOrdersForNameCodeChange_test(){
        EP_RouteMapper mapper = new EP_RouteMapper();
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        List<EP_Route__c> routeList = new List<EP_Route__c>();
        for(EP_Route__c route : routeRecords.values()){
            routeList.add(route);
        }
        csord__Order__c submittedOrderObj = EP_TestDataUtility.getOrderStateSubmittedDomainObjectPositiveScenario().getOrder();
        submittedOrderObj.EP_Route__c = routeList[0].id;
        update submittedOrderObj;
        
        Test.startTest();
        csord__Order__c planningOrderObj = EP_TestDataUtility.getConsignmentOrderPlanningStatus();
        planningOrderObj.EP_Route__c = routeList[0].id;
        update planningOrderObj;
        Map<Id,List<Order>> routeWithOrder =  mapper.getRoutesWithOrdersForNameCodeChange(routeRecords.keySet());
        Test.stopTest();
        System.assert(routeWithOrder != NULL);
    }
    
    static testMethod void getAssociatedOrdersWithRoute_test(){
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();
        EP_RouteMapper mapper = new EP_RouteMapper();
        Set<Id> routeSet = new Set<Id>();
        for(EP_Run__c run : runRecords){
            routeSet.add(run.EP_Route__c);
        }
        csord__Order__c submittedOrderObj = EP_TestDataUtility.getOrderStateSubmittedDomainObjectPositiveScenario().getOrder();
        submittedOrderObj.EP_Route__c = runRecords[0].EP_Route__c;
        submittedOrderObj.EP_Run__c = runRecords[0].id;
        update submittedOrderObj;
        
        Test.startTest();
        csord__Order__c planningOrderObj = EP_TestDataUtility.getConsignmentOrderPlanningStatus();
        planningOrderObj.EP_Route__c = runRecords[0].EP_Route__c;
        planningOrderObj.EP_Run__c = runRecords[0].id;
        update planningOrderObj;
        
        Map<Id,List<Order>> routeWithOrder =  mapper.getAssociatedOrdersWithRoute(routeSet);
        Test.stopTest();
        System.assert(routeWithOrder != NULL);
    }
    
    
    static testMethod void getRouteRecordsById_test(){
        EP_RouteMapper mapper = new EP_RouteMapper();
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        List<EP_Route__c> routeList = new List<EP_Route__c>();
        for(EP_Route__c route : routeRecords.values()){
            routeList.add(route);
        }
        Test.startTest();
        Map<Id,EP_Route__c> routeMap =  mapper.getRouteRecordsById(routeRecords.keySet());
        Test.stopTest();
        System.assert(routeMap != NULL);
    }
    
    static testMethod void getRoutesWithAccountForStatusChange_test(){
        EP_RouteMapper mapper = new EP_RouteMapper();
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        List<EP_Route__c> routeList = new List<EP_Route__c>();
        for(EP_Route__c route : routeRecords.values()){
            routeList.add(route);
        }
        csord__Order__c ord = [SELECT Id,EP_ShipTo__r.Id FROM csord__Order__c LIMIT : EP_Common_Constant.ONE];
        EP_AccountMapper accMapper = new EP_AccountMapper();
        Account acc = accMapper.getAccountRecord(ord.EP_ShipTo__r.Id);  
        EP_Route__c route = routeList[0];
        route.EP_Company__c = acc.EP_Puma_Company__c;
        update route;
        acc.EP_Default_Route__c = routeList[0].id;
        update acc;    
        Test.startTest();
        Set<Id> routeSet =  mapper.getRoutesWithAccountForStatusChange(routeRecords.keySet());
        Test.stopTest();
        System.assert(routeSet != NULL);
    }
}