/*   
     @Author <Accenture>
     @name <EP_FE_Customer_SupportInfoTest.cls>     
     @Description <This class is the test class for EP_FE_Customer_SupportInfo>   
     @Version <1.1> 
*/
@isTest
Private class EP_FE_Customer_SupportInfoTest{
	
	private static User sysAdmUser = EP_FE_TestDataUtility.getRunAsUser();

    /*********************************************************************************************
    *@Description : This is the Test method to call local support order method .                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
    static testMethod void testreturnLocalSupportNumber(){
      
         
        EP_FE_Customer_SupportInfoResponse response = new EP_FE_Customer_SupportInfoResponse(); 

        RestRequest req = new RestRequest();   
        req.httpMethod = 'Get';  
        req.requestURI = '/services/apexrest/FE/V1/support/customerinfo/';  
        RestContext.request = req;

        Test.startTest();
        System.RunAs(sysAdmUser){
        response = EP_FE_Customer_SupportInfo.returnLocalSupportNumber();
        } 
        Test.stopTest();
        
      }
}