/**
 *  @Author <Kamal Garg>
 *  @Name <EP_ManageAccountStatementDocument>
 *  @CreateDate <04/07/2016>
 *  @Description <This is the apex class used to generate PDF for Customer Account Statement record>
 *  @Version <1.0>
 */
public with sharing class EP_ManageAccountStatementDocument {

    private static final string CLASS_NAME = 'EP_ManageAccountStatementDocument ';
    private static final string GENERATE_AT_METHOD = 'generateAttachmentForAccountStatement';
	//L4 #45308 changes start
    private static set<Id> setBillToIds; 
    //L4 #45308 changes End
    /**
     * @author <Pooja Dhiman>
     * @name <generateAttachmentForAccountStatement>
     * @date <04/07/2016>
     * @description <This method is used to generate document for Account Statement>
     * @version <1.0>
     * @param EP_ManageDocument.Documents
     */
    public static void generateAttachmentForAccountStatement(EP_ManageDocument.Documents documents) {
        
        Map<String, List<Attachment>> customerAccountAttachListMap = new Map<String, List<Attachment>>();
        Map<String, EP_Customer_Account_Statement__c> CustAccstmtNrIdMap = new Map<String, EP_Customer_Account_Statement__c>();
        //45309 Start
        list<EP_AttachmentNotificationService.attachmentWrapper> attachmentWrapperList = new list <EP_AttachmentNotificationService.attachmentWrapper>();
        map<Id,Attachment> mapIdAtt = new map<Id,Attachment>();
        //45309 End
        try
        {           
            //L4 #45308 changes start            
            customerAccountAttachListMap = getCustomerAccountAttachListMap(documents);
            CustAccstmtNrIdMap = Ep_AccountStatementMapper.getCustAccstmtNrIdMap(setBillToIds);
            //L4 #45308 changes end
 
            List<Attachment> attachments = new List<Attachment>();
            for(String key : customerAccountAttachListMap.keySet()) {
                List<Attachment> attachList = customerAccountAttachListMap.get(key);
                for(Attachment att : attachList) {
                    att.ParentId = CustAccstmtNrIdMap.get(key).id;
                    mapIdAtt.put(att.Id,att);
                    attachments.add(att);
                    //L4 #45308 changes start
                    attachmentWrapperList.add(new EP_AttachmentNotificationService.attachmentWrapper(att,CustAccstmtNrIdMap.get(key).EP_Bill_To__c,EP_Common_Constant.NOTIFICATION_CLASS_CustomerStatement));
                    //L4 #45308 changes End
                }
            }
           
            EP_DocumentUtil.deleteAttachments(attachments); 
 
            //L4 #45308 changes start
            validateAttachCreated(attachments);
            EP_AttachmentNotificationService.sendAttachmentNotification(attachmentWrapperList);
            //L4 #45308 changes end
        }
        catch(Exception ex){
             EP_loggingService.loghandledException(ex,EP_Common_Constant.EPUMA, GENERATE_AT_METHOD , CLASS_NAME,apexPages.severity.ERROR);
             throw ex;
        }
    }
    
    /*L4 #45308 changes start*/
    /**
     * @author <Accenture>
     * @name <validateAttachCreated>
     * @date <03/09/2018>
     * @description <This method is used to validate attachment  successfully cretad records>
     * @version <1.0>
     * @param list<Attachment>
     * @return NA    
     */
    private static void validateAttachCreated(list<Attachment> attachments){
    	if(!attachments.isEmpty()){
    		Database.SaveResult[] results = Database.insert(attachments, false);
            for (Integer i = 0; i < results.size(); i++) {
                if (results[i].isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed                    
                    EP_ManageDocument.successRecIdMap.put(attachments[i].Name, results[i].getId());
                } else {
                    // Operation failed, so get all errors
                    for(Database.Error err : results[i].getErrors()) {                       
                        EP_ManageDocument.errDesMap.put(attachments[i].Name, err.getMessage());
                        EP_ManageDocument.errCodeMap.put(attachments[i].Name, String.valueOf(err.getStatusCode()));
                    }
                }
            }
    	}
    }
    /**
     * @author <Accenture>
     * @name <setCustomerAccountAttachListMap>
     * @date <03/09/2018>
     * @description <This method is used to generate 'Map<String, List<Attachment>>'>
     * @version <1.0>
     * @param EP_ManageDocument.Document doc
     * @return Map<String, List<Attachment>>      
     */
     private static Map<String,List<Attachment>> getCustomerAccountAttachListMap(EP_ManageDocument.Documents documents){
        List<EP_Customer_Account_Statement__c> lstCustAccStatment = new List<EP_Customer_Account_Statement__c>();
        Map<String, List<Attachment>> customerAccountAttachListMap = new Map<String, List<Attachment>>();
        setBillToIds = new set<Id>();
        for(EP_ManageDocument.Document doc : documents.document) {
                EP_ManageAccountStatementDocument.CustomerAccountStatementWrapper custAccountStmtWrap = EP_DocumentUtil.fillCustomerAccountStatementWrapper(doc.documentMetaData.metaDatas);
                EP_Customer_Account_Statement__c oCustAccStmt = generateCustomerAccountStatement(custAccountStmtWrap);
              
               oCustAccStmt.EP_Document_UUID__c = doc.documentUUId;
                setBillToIds.add(oCustAccStmt.EP_Bill_To__c);              
                lstCustAccStatment.add(oCustAccStmt);
                        
                String customerAccStmtKey = oCustAccStmt.EP_Customer_Account_Statement_Key__c;
                Attachment att = EP_DocumentUtil.generateAttachment(doc);
                if(!(customerAccountAttachListMap.containsKey(customerAccStmtKey))){                    
                    customerAccountAttachListMap.put(customerAccStmtKey, new list<Attachment>());
                }
                customerAccountAttachListMap.get(customerAccStmtKey).add(att);
        } 
        
        Schema.SObjectField Keyfield = EP_Customer_Account_Statement__c.Fields.EP_Customer_Account_Statement_Key__c;           
        List<Database.upsertResult> uResults = Database.upsert(lstCustAccStatment, Keyfield, true);
        
        return customerAccountAttachListMap;
     }
    /*L4 #45308 changes end*/
 
    
    /**
     * @author <Kamal Garg>
     * @name <generateCustomerAccountStatement>
     * @date <04/07/2016>
     * @description <This method is used to generate 'EP_Customer_Account_Statement__c' custom object record>
     * @version <1.0>
     * @param EP_ManageAccountStatementDocument.CustomerAccountStatementWrapper
     * @return EP_Customer_Account_Statement__c
     */
    private static EP_Customer_Account_Statement__c generateCustomerAccountStatement(EP_ManageAccountStatementDocument.CustomerAccountStatementWrapper custAcctStatWrap) {
        EP_Customer_Account_Statement__c acctStatObj = new EP_Customer_Account_Statement__c();
        acctStatObj.EP_Statement_Issue_Date__c = Date.valueOf(custAcctStatWrap.IssueDate);
        acctStatObj.EP_Statement_NAV_ID__c = custAcctStatWrap.SeqId;
		//L4 #45308 changes start
        acctStatObj.EP_Type__c = custAcctStatWrap.type_Z;
        acctStatObj.EP_ePuma_Company_Code__c = custAcctStatWrap.issuedFromId;       
        //L4 #45308 changes end
        if(String.isNotBlank(custAcctStatWrap.type_Z) && EP_Common_Constant.ALL_TYPE.equals(custAcctStatWrap.type_Z)) {
			/*L4 45308 code change start*/
            acctStatObj.EP_Statement_Start_Date__c = Date.valueOf(custAcctStatWrap.fromDate);
            acctStatObj.EP_Statement_End_Date__c = Date.valueOf(custAcctStatWrap.toDate);

            acctStatObj.EP_Customer_Account_Statement_Key__c = EP_DocumentUtil.generateUniqueKey(new Set<String>{EP_Common_Constant.ALL_TYPE, 
                                                                                                                                    custAcctStatWrap.BillTo, 
                                                                                                                                    custAcctStatWrap.fromDate, 
                                                                                                                                    custAcctStatWrap.toDate, 
                                                                                                                                    custAcctStatWrap.issuedFromId});
            acctStatObj.RecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMER_ACCOUNT_STATEMENT_OBJECT, 
                                                                            EP_Common_Constant.CUSTOMER_ACCOUNT_STATEMENT_RECORD_TYPE_NAME);
            acctStatObj.EP_Closing_Balance__c = Decimal.valueOf(custAcctStatWrap.closingBalance.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK));
            acctStatObj.EP_Opening_Balance__c = Decimal.valueOf(custAcctStatWrap.openingBalance.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK));
            acctStatObj.EP_Total_Credits__c = Decimal.valueOf(custAcctStatWrap.totalCredits.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK));
            acctStatObj.EP_Total_Debits__c = Decimal.valueOf(custAcctStatWrap.totalDebits.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK));
        } else if(String.isNotBlank(custAcctStatWrap.type_Z) && EP_Common_Constant.OPEN_TYPE.equals(custAcctStatWrap.type_Z)) {
            acctStatObj.EP_Statement_Start_Date__c = NULL;
            acctStatObj.EP_Statement_End_Date__c = NULL;
            acctStatObj.EP_Customer_Account_Statement_Key__c = EP_DocumentUtil.generateUniqueKey(new Set<String>{EP_Common_Constant.OPEN_TYPE, 
                                                                                                                                    custAcctStatWrap.BillTo, 
                                                                                                                                    custAcctStatWrap.fromDate, 
                                                                                                                                    custAcctStatWrap.issuedFromId});
            acctStatObj.RecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMER_ACCOUNT_STATEMENT_OBJECT, 
                                                                            EP_Common_Constant.CUSTOMER_OPEN_ITEM_STATEMENT_RECORD_TYPE_NAME);
            acctStatObj.EP_Overdue_Amount__c = Decimal.valueOf(custAcctStatWrap.totalOverdue.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK));
            acctStatObj.EP_Current_Amount__c = Decimal.valueOf(custAcctStatWrap.totalCurrent.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK));
            acctStatObj.EP_Total_Due_Amount__c = Decimal.valueOf(custAcctStatWrap.totalDueAmount.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK));
            acctStatObj.EP_Closing_Balance__c = NULL;
            acctStatObj.EP_Opening_Balance__c = NULL;
            acctStatObj.EP_Total_Credits__c = NULL;
            acctStatObj.EP_Total_Debits__c = NULL;
            //L4 #45308 changes start
            //acctStatObj.EP_Total_Orders_In_Progress__c = Decimal.valueOf(custAcctStatWrap.totalOrderInProgress.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK));
            //L4 #45308 changes end
        }
        acctStatObj.EP_Bill_To__c = EP_DocumentUtil.getBillToId(custAcctStatWrap.BillTo, custAcctStatWrap.issuedFromId, 
                                                new Set<String>{EP_Common_Constant.RT_BILL_TO_DEV_NAME, EP_Common_Constant.SELL_TO_DEV_NAME});
        acctStatObj.EP_Statement_Pull_Status__c = EP_Common_Constant.COMPLETE;
        acctStatObj.CurrencyIsoCode = custAcctStatWrap.currencyId;
        return acctStatObj;
    }
    
    /**
     *  @Author <Kamal Garg>
     *  @Name <CustomerAccountStatementWrapper>
     *  @CreateDate <04/07/2016>
     *  @Description <This is the wrapper class containing 'Customer Account Statement' record information>
     *  @Version <1.0>
     */
    public with sharing class CustomerAccountStatementWrapper {
        public String BillTo;
        public String IssueDate;
        /*L4 45308 code change start*/
        //public String StatementStartDate;
        //public String StatementEndDate;
        public String fromDate;
        public String toDate;
        /*L4 45308 code change End*/
        public String SeqId;
        public String customerName;
        public String type_Z;
        public String issuedFromId;
        public String openingBalance;
        public String totalCredits;
        public String totalDebits;
        public String closingBalance;
        public String totalOrderInProgress;
        public String totalCurrent;
        public String totalOverdue;
        public String totalDueAmount;
        public String currencyId;
    }
    
}