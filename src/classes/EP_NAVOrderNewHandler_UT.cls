@isTest
public class EP_NAVOrderNewHandler_UT
{
	public static final String TEXT = 'Test';
	public static final String ERRORCD = '12345';
	public static final String RESPONSETYPESTR = 'TestResponseType';
	public static final String ACC_NAME = 'Account1';
	//For state machine to give proper status
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    	List<EP_OrderStatusNAVMapping__c> orderStatusMapping =  Test.loadData(EP_OrderStatusNAVMapping__c.sObjectType,'EP_OrderStatusNAVMapping_TestData');
    } 
	/*static testMethod void processRequest_test() {
		EP_NAVOrderNewHandler localObj = new EP_NAVOrderNewHandler();
		EP_NAVOrderNewStub stub = EP_TestDataUtility.createNAVOrderNewStub();
		EP_NAVOrderNewHelper localHelperObj = new EP_NAVOrderNewHelper();
		EP_OrderMapper ordMapper = new EP_OrderMapper();
        EP_NAVOrderNewStub.orderWrapper orderWrapper = EP_NAVOrderNewHelper_UT.createNAVOrderNewStubWithData(); 
		orderWrapper.sfOrder.Status__c = 'Submitted';
		update orderWrapper.sfOrder;
		
		EP_NAVOrderNewStub.OrderLine ordItem  = orderWrapper.OrderLines.OrderLine.get(0);
		orderWrapper.isCloned = false;
		list<OrderItem> orderItems = [select Id,priceBookEntry.Id from OrderItem where order.Id =: orderWrapper.sfOrder.id]; 
        PricebookEntry priceBookEntry = [select id from PricebookEntry where Id =: orderItems.get(0).priceBookEntry.Id limit 1];
        String priceBookEntryKey =  ordItem.itemId + orderWrapper.sfOrder.priceBook2Id__c + orderWrapper.sfOrder.currencyISOCode;
		localHelperObj.pricebookEntryWithProdCode.put(priceBookEntryKey,priceBookEntry);
		EP_AccountDomainObject accDomain = new EP_AccountDomainObject(orderWrapper.sfOrder.EP_ShipTo__c);
        EP_AccountDomainObject accDomainSellTo = new EP_AccountDomainObject(orderWrapper.sfOrder.AccountId__c);
        orderWrapper.shipToId = accDomain.getAccount().AccountNumber;
        orderWrapper.sellToId = accDomainSellTo.getAccount().AccountNumber;
		orderWrapper.clientId = orderWrapper.clientId.toUpperCase();
		orderWrapper.processingStatus = orderWrapper.sfOrder.Status__c;
		
		//orderWrapper.sfOrder = new order();
		orderWrapper.Identifier.orderId = null;
        //orderWrapper.orderRefNr = 'orderRefNr';
        stub.MSG.Payload.any0.order = orderWrapper;
		String requestBody = System.JSON.serialize(stub);
		
		Test.startTest();
		String result = localObj.processRequest(requestBody);
		Test.stopTest();
		
		System.AssertNotEquals(Null,result);
	}*/
	static testMethod void processRequest_NegativeTest() {
		EP_NAVOrderNewHandler localObj = new EP_NAVOrderNewHandler();
		EP_NAVOrderNewStub stub = EP_TestDataUtility.createNAVOrderNewStub();
		
		String requestBody = System.JSON.serialize(stub);
		
		Test.startTest();
		String result = localObj.processRequest(requestBody);
		Test.stopTest();
		
		System.AssertNotEquals(Null,result);
	}
	static testMethod void processOrder_test() {
		EP_NAVOrderNewStub.OrderWrapper orderWrapper = EP_TestDataUtility.createNAVOrderWrapper();
		orderWrapper.sfOrder = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
		orderWrapper.sfOrder.EP_Order_Comments__c = TEXT;
		orderWrapper.sfOrder.EP_SeqId__c = 'SEQ-123';
		
		Test.startTest();
		EP_NAVOrderNewHandler.processOrder(orderWrapper);
		Test.stopTest();
		
		Order ordObj = [SELECT Id, EP_Order_Comments__c,EP_SeqId__c FROM Order WHERE Id =:orderWrapper.sfOrder.id ];
		System.AssertEquals(TEXT,ordObj.EP_Order_Comments__c);
		System.AssertEquals(true,EP_NAVOrderNewHandler.ackResponseList.size() > 0);
	}
	
	/*static testMethod void processOrderLineItems_PositiveTest() {
		EP_NAVOrderNewStub.OrderWrapper orderWrapper = EP_NAVOrderNewHelper_UT.createNAVOrderNewStubWithData();
		List<csord__Order_Line_Item__c> orderitems = [select Id, OrderId__c, EP_SeqId__c, Quantity__c 
														//, EP_Eligible_for_Rebate__c 
														from csord__Order_Line_Item__c where OrderId__c =: orderWrapper.sfOrder.Id];
		orderWrapper.orderLines.OrderLine[0].orderLineItem = orderitems.get(0);
		orderWrapper.orderLines.OrderLine[0].orderLineItem.Quantity__c = 300;
		Test.startTest();
		EP_NAVOrderNewHandler.processOrderLineItems(orderWrapper);
		Test.stopTest();
		
		OrderItem orderItemObj = [SELECT Id ,Quantity FROM OrderItem WHERE id =: orderWrapper.orderLines.OrderLine[0].orderLineItem.Id];
		System.AssertEquals(orderItemObj.Quantity,orderWrapper.orderLines.OrderLine[0].orderLineItem.Quantity__c);
	}*/
	
	static testMethod void getInvoiceItems_test() {
		EP_NAVOrderNewStub.OrderWrapper orderWrapper = EP_TestDataUtility.createNAVOrderWrapper();
		
		Test.startTest();
		LIST<csord__Order_Line_Item__c> result = EP_NAVOrderNewHandler.getInvoiceItems(orderWrapper);
		Test.stopTest();
		
		System.AssertEquals(true,result.size() > 0);
	}
	static testMethod void upsertOrderLineItems_test() {
		csord__Order__c orderbj  = EP_TestDataUtility.getSalesOrderPositiveScenario();
		LIST<csord__Order_Line_Item__c> orderItemsToBeProcess = new LIST<csord__Order_Line_Item__c>();
		For(csord__Order_Line_Item__c orderItemObj: [SELECT Id, EP_SeqId__c
													//, EP_Eligible_for_Rebate__c 
													FROM csord__Order_Line_Item__c WHERE OrderId__c =:orderbj.Id]){
		  //orderItemObj.EP_Eligible_for_Rebate__c = True;
		  orderItemsToBeProcess.add(orderItemObj);
		}
		
		Test.startTest();
		EP_NAVOrderNewHandler.upsertOrderLineItems(orderItemsToBeProcess);
		Test.stopTest();
		
		OrderItem ordItemObj = [SELECT Id ,EP_SeqId__c, EP_Eligible_for_Rebate__c FROM OrderItem WHERE Id =:orderItemsToBeProcess[0].id ];
		System.AssertEquals(true,ordItemObj.EP_Eligible_for_Rebate__c);
	}
	
	static testMethod void upsertOrderLineItems_Negativetest() {
		csord__Order__c orderbj  = EP_TestDataUtility.getSalesOrderPositiveScenario();
		LIST<csord__Order_Line_Item__c> orderItemsToBeProcess = new LIST<csord__Order_Line_Item__c>();
		for(csord__Order_Line_Item__c orderItemObj: [SELECT Id, EP_SeqId__c, Description__c, OrderId__c, PriceBookEntryId__c 
													//, EP_Eligible_for_Rebate__c
													FROM csord__Order_Line_Item__c WHERE OrderId__c =:orderbj.Id]){
			orderItemObj.Description__c = 'DescriptionDescriptionDescriptionDescriptionDescrDescription';
		  	orderItemsToBeProcess.add(orderItemObj);
		}
		
		Test.startTest();
		EP_NAVOrderNewHandler.upsertOrderLineItems(orderItemsToBeProcess);
		Test.stopTest();
		
		System.AssertEquals(true,EP_NAVOrderNewHandler.ackResponseList.size() > 0);
	}
	
	/*static testMethod void processInvoiceLineItems_test() {
		EP_NAVOrderNewStub.orderWrapper orderWrapper = EP_NAVOrderNewHelper_UT.createNAVOrderNewStubWithData(); 
		
		Test.startTest();
		EP_NAVOrderNewHandler.processInvoiceLineItems(orderWrapper);
		Test.stopTest();
		
		System.AssertEquals(true,EP_NAVOrderNewHandler.ackResponseList.size() > 0);
	}*/
	static testMethod void createResponse_test() {
		EP_NAVOrderNewStub.OrderWrapper orderWrapper = EP_TestDataUtility.createNAVOrderWrapper();
		String ResponseType = RESPONSETYPESTR;
		String seqId = orderWrapper.seqId;
		String errorCode = ERRORCD;
		List<Account> listAccounts = new List<Account>{new Account(Name=ACC_NAME),new Account()};
        Database.SaveResult[] srList = Database.insert(listAccounts, false);
        String errorDescription = srList[0].getErrors()[0].getMessage();
		
		Test.startTest();
		EP_NAVOrderNewHandler.createResponse(ResponseType,seqId,errorCode,errorDescription);
		Test.stopTest();
		
		System.AssertEquals(true,EP_NAVOrderNewHandler.ackResponseList.size() > 0);
	}
	static testMethod void processUpsertErrors_test() {
		String ResponseType = RESPONSETYPESTR;
		List<csord__Order__c> orderList = new List<csord__Order__c>{EP_TestDataUtility.getSalesOrder()};
        orderList[0].Status__c = '';
        List<Database.Error> errorList = new List<Database.Error>();
        Database.UpsertResult[] srList = Database.upsert(orderList, false);
        errorList.add(srList[0].getErrors()[0]);
		
		Test.startTest();
		EP_NAVOrderNewHandler.processUpsertErrors(ResponseType,errorList,'seqId');
		Test.stopTest();
		
		System.AssertEquals(true,EP_NAVOrderNewHandler.ackResponseList.size() > 0);
	}
}