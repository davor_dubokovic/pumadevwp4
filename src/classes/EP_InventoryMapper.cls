/**
   @Author          Shakti Mehtani
   @name            EP_InventoryMapper
   @CreateDate      01/01/2017
   @Description     This class contains all SOQLs related to Inventory Object 
   @NovaSuite Fix -- comments added
   @Version         1.0
   @reference       NA
   */

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    *************************************************************************************************************
    */
    public with sharing class EP_InventoryMapper {
      /***NOvasuite fix constructor removed**/ 
    /**
   @Author          Accenture
   @name            getInventoryByProdId
   @CreateDate      01/01/2017
   @Description     This class returns Inventory from product Id
   @Param           Set<Id>,Id
   @reference       list<EP_Inventory__c>
   */
    public list<EP_Inventory__c> getInventoryByProdId(Set<Id> setProdIds,Id stocHoldLocId) {
        EP_GeneralUtility.Log('Public','EP_InventoryMapper','getInventoryByProdId');

        List<EP_Inventory__c> lstInventory = [SELECT id,EP_Storage_Location__c,EP_Product__c,EP_Inventory_Availability__c 
        FROM EP_Inventory__c 
        WHERE EP_Storage_Location__c =: stocHoldLocId
        AND EP_Product__c IN: setProdIds];

        return lstInventory;                                    
    }
    

    /**  This method is used to get list of inventory records for the Set Ids which is passed 
     *  @date            17/01/2017
     *  @name            getInventoryByLocationId
     *  @param           Id storageLocationId,String productSoldAs
     *  @return          list<EP_Inventory__c>
     *  @throws          NA
     */
     public list<EP_Inventory__c> getInventoryByLocationId(Id storageLocationId,String productSoldAs) {
        EP_GeneralUtility.Log('Public','EP_InventoryMapper','getInventoryByLocationId');

        List<EP_Inventory__c> lstInventory = [Select EP_Product__c,EP_Product__r.EP_Product_Sold_As__c from  Ep_Inventory__c 
        where EP_Storage_Location__c =: storageLocationId
        AND EP_Product__r.EP_Product_Sold_As__c =: productSoldAs ];

        return lstInventory;                                    
    }


    /**  This method is used to get list of inventory records for Stock Holding Location
     *  @date            1/03/2017
     *  @name            getInventoryByStockHoldingLocationId
     *  @param           Id stockHoldinglocationId
     *  @return          list<EP_Inventory__c>
     *  @throws          NA
     */
     /* TFS fix 45559,45560,45567,45568 start EP_NAV_ID__c deprecated, EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c and EP_Vendor_Type__c is replaced by EP_VendorType__c*/
     public List<EP_Inventory__c> getInventoryByStockHoldingLocationId(Id stockHoldinglocationId) {
        EP_GeneralUtility.Log('Public','EP_InventoryMapper','getInventoryByStockHoldingLocationId');

        List<EP_Inventory__c> lstInventory =  [SELECT id,EP_Supplier__r.id,EP_Supplier__r.EP_VendorType__c,EP_Supplier__r.Name,EP_Supplier__r.RecordType.DeveloperName 
        FROM EP_Inventory__c 
        WHERE EP_Storage_Location__c =: stockHoldinglocationId 
        AND EP_Supplier__r.RecordType.DeveloperName =: EP_Common_Constant.RT_VENDOR_DEV_NAME 
        AND EP_Supplier__r.EP_VendorType__c =: EP_Common_Constant.THIRD_PARTY_STCK_SUPPLIER];

        return lstInventory;                                        
    }
    /**  This method is used to get suppliers
     *  @date            1/03/2017
     *  @name            getSuppliers
     *  @param           Id stockHoldinglocationId
     *  @return          list<EP_Inventory__c>
     *  @throws          NA
     */
    public list<EP_Inventory__c> getSuppliers(Id stockHoldinglocationId){

        list<EP_Inventory__c> inventoryList = new list<Ep_Inventory__c>(); 
        inventoryList = [SELECT id,EP_Supplier__r.id,EP_Supplier__r.EP_VendorType__c,EP_Supplier__r.Name,EP_Supplier__r.RecordType.DeveloperName 
                            FROM EP_Inventory__c 
                            WHERE EP_Storage_Location__c =: stockHoldinglocationId 
                            AND EP_Supplier__r.RecordType.DeveloperName =: EP_Common_Constant.RT_VENDOR_DEV_NAME 
                            AND EP_Supplier__r.EP_VendorType__c =: EP_Common_Constant.THIRD_PARTY_STCK_SUPPLIER];
        return inventoryList; 
    }
    /* TFS fix 45559,45560,45567,45568 end*/
    public list<EP_Inventory__c> getInventoryByStorageLocAndProductSoldAs(String storageLocId, String productCategory){
        return  [Select EP_Product__c,EP_Product__r.EP_Product_Sold_As__c from  Ep_Inventory__c where EP_Storage_Location__c =: storageLocId AND EP_Product__r.EP_Product_Sold_As__c =:productCategory];
    } 
   
}