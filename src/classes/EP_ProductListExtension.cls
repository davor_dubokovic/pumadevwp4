/***L4-45352 start****/
/***************************************************************
*  @Author <Accenture>                                         *
*  @Name <EP_ProductListExtension >                            *
*  @Description <Controller class to Add/ Edit/ Delete Products 
*  from detail page of price book, Delete part of the product 
*  have few validations stated in below code> 
* Novasuite Fixes - Required Documentation
*  @Version <1.0>                                              *
****************************************************************/
/* 
*  @Author <Accenture>                                         *
*  @Name EP_ProductListExtension
*/
public with sharing class EP_ProductListExtension { 
Public EP_ProductListExtensionContext ctx{get;set;} 
Public EP_ProductListExtensionHelper ProductListHelper;
    
/****************************************************************
* @author       Accenture                                       *
* @name         EP_ProductListExtension                         *
* @description  Standard constructor of class                   *
* @param        Standard Controller                             *
* @return       NA                                              *
****************************************************************/
    public EP_ProductListExtension(ApexPages.StandardController controller) {
        try{
            String currentPageUrl = ApexPages.currentPage().getURL();
            PriceBook2 PriceBook  = (Pricebook2)controller.getRecord();
            ctx = new EP_ProductListExtensionContext (PriceBook);
            ProductListHelper = new EP_ProductListExtensionHelper(ctx);
            ctx.PriceBookId = PriceBook.id;
            ctx.ListProd = ctx.PriceBookEntryMapper.getALLRecordsByPriceBookId(ctx.PriceBookId);
            system.debug('ctx.ListProd>>'+ctx.ListProd);
            
        }
        catch(exception ex){
          system.debug('Error = '+ex.getMessage()+' at line = '+ex.getLineNumber());
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage() + EP_Common_Constant.ATLINE +ex.getLineNumber())); 
        }
    }

/****************************************************************
* @author       Accenture                                       *
* @name         AddProducts                                     *
* @description  This method is used to delete PriceBookEntry    *
* @param        NA                                              *
* @return       NA                                              *
****************************************************************/
    Public Pagereference  DeleteProducts(){        
        try{           
           ProductListHelper.DeleteProducts(integer.valueof(ctx.PriceBookdelId));
        }
        catch(exception ex){
          system.debug('Error = '+ex.getMessage()+' at line = '+ex.getLineNumber());
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage() + EP_Common_Constant.ATLINE +ex.getLineNumber())); 

        }
        return null;
    }
/****************************************************************
* @author       Accenture                                       *
* @name         CreatePriceBookReview                           *
* @description  This method will create PriceBook review if method
*  IsPriceBookReviewRequired returns true                       *
* @param        NA                                              *
* @return       NA                                              *
****************************************************************/
    Public Void CreatePriceBookReview(){
        ProductListHelper.CreatePriceBookReview();
    }
}
/***L4-45352 end****/