/* 
   @Author <Amit Singh>
   @name <EP_ActionTriggerHandler>
   @CreateDate <24/04/2016>
   @Description <TThis class is the handler class of Action trigger >
   @Version <1.0>
   */
   public without sharing class EP_ActionTriggerHandler {
    Public static Boolean isExecuteAfterUpdate = false;
    Public static Boolean isExecuteBeforeUpdate = false;
    Public static Boolean isExecuteAfterInsert = false;
    Public static Boolean isExecuteBeforeInsert = false;
    public static boolean isEscapeOwnerValidation = false;
    /*
     * Method for executing logic before insertion 
     */
     public static void doBeforeInsert(List<EP_Action__c> lnewActions){
      EP_GeneralUtility.Log('Public','EP_ActionTriggerHandler','doBeforeInsert');
      EP_ActionTriggerHelper.assignQueueAndRecordType(lnewActions);
      //EP_ActionTriggerHelper.assignQueueChangeActions(lNewActions);
    }
    
    /*
     * Method for executing Logic before Update
     */
     public static void doBeforeUpdate(map<Id,EP_Action__c>mNewActions,map<Id,EP_Action__c>mOldActions){
      EP_GeneralUtility.Log('Public','EP_ActionTriggerHandler','doBeforeUpdate');
          isExecuteBeforeUpdate = true; 
          if(!isEscapeOwnerValidation){
            /*DEFECT# 28204 START*/
            EP_ActionTriggerHelper.restrictActionOwnerChange(mOldActions,mNewActions);
          }                                               
          /*DEFECT# 28204 END*/
          //EP_ActionTriggerHelper.cmpltAndCrtNxtAction(mNewActions,mOldActions);      
    }

   /*
    * Method for executing Logic After Update
    */
    public static void doAfterApdate(map<Id,EP_Action__c>mNewActions,map<Id,EP_Action__c>mOldActions){  
      EP_GeneralUtility.Log('Public','EP_ActionTriggerHandler','doAfterApdate');      
            isExecuteAfterUpdate = true;
            if(!system.isBatch()){
                /***L4-45352 start****/
                //EP_ActionTriggerHelper.reSyncSellToShipToIfErrorSync(mNewActions,mOldActions);     
                set<Id> PriceBookIds = new set<Id>();
                map<Id,Id> SellToAccountSet = new map<Id,Id>();
                for(EP_Action__c actionId: mNewActions.values()){
                    if(actionId.EP_Product_List__c!=null && mOldActions!=null && actionId.EP_Status__c!= mOldActions.get(actionId.id).EP_Status__c && actionId.EP_Status__c == EP_Common_Constant.ACT_COMPLETED_STATUS){
                        PriceBookIds.add(actionId.EP_Product_List__c);
                    }
                    /*Defect 78421 start**/
                        if(mOldActions!=null && actionId.EP_Status__c!= mOldActions.get(actionId.id).EP_Status__c && actionId.EP_Status__c == EP_Common_Constant.ACT_COMPLETED_STATUS && actionId.EP_Record_Type_Name__c != EP_Common_Constant.ACT_BSMGM_REVIEW_RT){
                            SellToAccountSet.put(actionId.id,actionId.EP_Account__c);
                        }
                   /*Defect 78421 end**/
                }
                if(SellToAccountSet.size()>0){
                    EP_ActionTriggerHelper.isBSMGMRequired(SellToAccountSet);
                }
                if(PriceBookIds.size()>0){
                    EP_ActionTriggerHelper.ActivateAllPriceBookEntries(PriceBookIds);
                }
                EP_ActionTriggerHelper.updateShipToStatusToAccountSetup(mNewActions.values());
                /***L4-45352 end****/
                /**Defect 43917 START**/
                    //EP_ActionTriggerHelper.createPricingRvwSupplyLoc(mNewActions,mOldActions);
                /**Defect 43917 END**/  
            }
    }
    
    /*
    * Method for executing Logic After Insert
    */
    /***L4-45352 start****/
    public static void doAfterInsert(List<EP_Action__c> lnewActions){
        EP_GeneralUtility.Log('Public','EP_ActionTriggerHandler','doAfterInsert');
            isExecuteAfterInsert = true;
            if(!system.isBatch()){  
                EP_ActionTriggerHelper.DeactivateAllPriceBookEntries(lnewActions); 
                EP_ActionTriggerHelper.shareActionRecordWithUsers(lnewActions);
            }
     } 
     /***L4-45352 end****/
  }