@isTest
public class EP_AccountMapper_UT
{
    static EP_AccountDataWrapper adw = new EP_AccountDataWrapper();
    static EP_AccountMapper localObj = new EP_AccountMapper();

    static testMethod void getRecordsByIds_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        Set<id> idSet = new Set<id>{acct.Id};
        Test.startTest();
        LIST<Account> result = localObj.getRecordsByIds(idSet);
        Test.stopTest();
        System.AssertEquals(result.size(), 1);
        System.AssertEquals(result[0].id, acct.Id);
    }

    static testMethod void getRecordsByParentIds_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        
        Set<id> idSet = new Set<id>{acct.parentId};
        
        Test.startTest();
        LIST<Account> result = localObj.getRecordsByParentIds(idSet);
        Test.stopTest();
        System.AssertEquals(result.size(), 3);
    }

    static testMethod void getAccountWithShipToTanksById_test() {
        Account acct = EP_TestDataUtility.getShipToPositiveScenario();
        //Id accountId = acct.parentId;
        LIST<string> shipToTypes = new List<String>{EP_Common_Constant.VMI_SHIP_TO_DEV_NAME};
        String status;
        
        Test.startTest();
        LIST<Account> result = localObj.getAccountWithShipToTanksById(acct.Id, shipToTypes, status);
        Test.stopTest();
        System.AssertEquals(result.size(), 1);
        System.AssertEquals(result[0].id, acct.Id);
    }
    
    
    static testMethod void getBankAccountsByAccountId_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        Id AccountId = acct.id;
        
        Test.startTest();
        LIST<EP_Bank_Account__c> result = localObj.getBankAccountsByAccountId(AccountId);
        Test.stopTest();
        system.assertEquals(result.size(), 0);
    }

    static testMethod void getShipToAccountsByAccountId_test() {
        Account acct = EP_TestDataUtility.getShipTo();
        Id accountId = acct.parentId;
        LIST<string> shipToTypes = new List<String>{EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME};
        String status;
        Test.startTest();
        LIST<Account> result = localObj.getShipToAccountsByAccountId(accountId, shipToTypes, status);
        Test.stopTest();
        System.AssertEquals(result.size(), 1);
        System.AssertEquals(result[0].id, acct.Id);
    }

    static testMethod void getShipToWithTanksExcludingSellToById_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        
        Id AccountId = acct.id;
        String status = acct.EP_Status__c;
        
        Test.startTest();
        LIST<Account> result = localObj.getShipToWithTanksExcludingSellToById(AccountId,status);
        Test.stopTest();
        System.AssertEquals(result.size(), 1);
        System.AssertEquals(result[0].id, acct.Id);
    }
    
    static testMethod void getShipToAccounts_test() {
        Account acct = EP_TestDataUtility.getShipTo();
        
        Set<id> idSet_1 = new Set<id>{acct.parentId};
        
        
        Set<id> idSet_2;
        
        
        Set<id> idSet_3 = new Set<id>{acct.recordTypeId};
        
        Test.startTest();

        LIST<Account> result = localObj.getShipToAccounts(idSet_1,idSet_2,idSet_3);
        Test.stopTest();
        System.AssertEquals(result.size(), 1);
        System.AssertEquals(result[0].id, acct.Id);
        
        
        
    }
    static testMethod void getShipToAccountsWithTanks_test() {
        Account acct = EP_TestDataUtility.getShipTo();
        
        Set<id> idSet_1 = new Set<id>{acct.parentId};
        
        Test.startTest();
        LIST<Account> result = localObj.getShipToAccountsWithTanks(idSet_1);
        Test.stopTest();
        System.AssertEquals(result.size(), 1);
        System.AssertEquals(result[0].id, acct.Id);
        
        
        
    }
    //Need to remove actual method
    static testMethod void getRecordsHavingActiveParent_test() {
        Account acct = EP_TestDataUtility.getShipTo();
        
        Set<id> idSet = new Set<id>{acct.id};
        
        Test.startTest();
        LIST<Account> result = localObj.getRecordsHavingActiveParent(idSet);
        Test.stopTest();
        System.AssertEquals(result.size(), 0);
        
        
        
    }
    
    static testMethod void getShipToRecordsWithSupplyOptionAndTanks_test() {
        Account acct = EP_TestDataUtility.getShipTo();

        
        Set<Id> accountIds = new Set<Id>{acct.id};
        
        Test.startTest();
        LIST<Account> result = localObj.getShipToRecordsWithSupplyOptionAndTanks(accountIds);
        Test.stopTest();
        System.AssertEquals(result.size(), 1);
        System.AssertEquals(result[0].id, acct.Id);
        
        
        
    }

    static testMethod void getAccountRecordById_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        
        Id accountId = acct.id;
        
        Test.startTest();
        Account result = localObj.getAccountRecordById(accountId);
        Test.stopTest();
        System.AssertEquals(result.id, accountId);
        
        
        
    }
    static testMethod void getAccountRecord_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        
        Id accountId = acct.Id;
        
        Test.startTest();
        Account result = localObj.getAccountRecord(accountId);
        Test.stopTest();
        System.AssertEquals(result.id, accountId);
        
        
        
    }
    static testMethod void getAccountByAcountId_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        
        Id AccountId = acct.Id;
        
        Test.startTest();
        Account result = localObj.getAccountByAcountId(AccountId);
        Test.stopTest();
        System.AssertEquals(result.id, accountId);
     }
    
    static testMethod void getAccountRecordTypes_test() {
        
        
        LIST<String> listOfRecordTypeDeveloperNames = new List<String>{'EP_Non_VMI_Ship_To'};
        
        Test.startTest();
        LIST<RecordType> result = localObj.getAccountRecordTypes(listOfRecordTypeDeveloperNames);
        Test.stopTest();
        System.AssertNotEquals(result.size(), 0);
        
        
        
    }
    //waiting for response
    static testMethod void fetchBillToAccountDetail_test() {
        adw.accountType = 'SellTo';
        adw.billToRequired = true; 
        Account acct = EP_AccountTestDataUtility.getAccount(adw);

        
        
        Id accountId = acct.id;
        
        Test.startTest();
        Account result = localObj.fetchBillToAccountDetail(accountId);
        Test.stopTest();
        System.AssertEquals(acct.EP_Bill_To_Account__c, result.id);
        
        
        
    }
    //waiting for response. Need account setup-vmi shipTo
    static testMethod void getChildShipTosWithTankAndSupplyOption_test() {
        adw.accountType = EP_AccountConstant.SHIP_To;
        adw.vendorType = EP_AccountConstant.NON_VMI;
        adw.status = EP_Common_Constant.STATUS_SET_UP;
        adw.scenarioType = EP_Common_Constant.POSITIVE;
        adw.addSupplyOption = true;
        adw.isTankRequired = true;
        
        Account acct = EP_AccountTestDataUtility.getAccount(adw);
        
        system.debug('===status='+acct.EP_Status__c);
        Account parentAccount = new Account(id = acct.parentId);
        Test.startTest();
        LIST<Account> result = localObj.getChildShipTosWithTankAndSupplyOption(parentAccount);
        Test.stopTest();

        System.AssertEquals(result.size(), 1);
        System.AssertEquals(result[0].id, acct.Id);
        
        
        
    }
    
    static testMethod void getCountChildRecordsByStatusAndRecordType_test() {
       
        adw.accountType = EP_AccountConstant.SHIP_To;
        adw.vendorType = EP_AccountConstant.NON_VMI;
        adw.status = EP_Common_Constant.STATUS_SET_UP;
        adw.scenarioType = EP_Common_Constant.POSITIVE;
        adw.addSupplyOption = true;
        adw.isTankRequired = true;
        Account acct = EP_AccountTestDataUtility.getAccount(adw);
        Set<String> statusSet = new Set<String>{acct.EP_Status__c};
        Set<String> recordTypeSet = new Set<String>{EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME};
        
        Test.startTest();
        Integer result = localObj.getCountChildRecordsByStatusAndRecordType(acct.parentId, statusSet, recordTypeSet);
        Test.stopTest();
        System.AssertEquals(result, 1);
    }
    
    //waiting for response. Create active storage-shipTo
    static testMethod void getShipToStorageLocations_test() {
        Account acct = EP_TestDataUtility.getShipTo();
        
        Id accountId = acct.id;
        
        Test.startTest();
        LIST<Account> result = localObj.getShipToStorageLocations(accountId);
        Test.stopTest();
        
        System.AssertEquals(result.size(), 0);
    }

    static testMethod void getExistingSellToAccount_test() {
        adw.accountType=EP_AccountConstant.SELL_TO;
        adw.addSupplyOption = true;
        adw.billToRequired = false; 
        adw.addPackagedPoduct = true;
        Account acct = EP_AccountTestDataUtility.getAccount(adw);
        Test.startTest();
        Account result = localObj.getExistingSellToAccount();
        Test.stopTest();
        System.AssertEquals(result.id, acct.Id);
    }

    static testMethod void getExistingShipToAccount_test() {
        
        Account acct = EP_TestDataUtility.getVmiShipTo();
        Test.startTest();
        Account result = localObj.getExistingShipToAccount();
        Test.stopTest();
        System.AssertEquals(result.id, acct.Id);
    }

    //waiing for response. Create storage location account
    static testMethod void getsupplyLocationPricingList_test() {
        Account acct = EP_TestDataUtility.getShipTo();
        
        String strCompanyCode = acct.EP_Puma_Company_Code__c;
        
        Test.startTest();
        LIST<account> result = localObj.getsupplyLocationPricingList(strCompanyCode);
        Test.stopTest();
        System.AssertEquals(result.size(), 0);
        
    }
    //waiting for response. Create vendor record
    static testMethod void getTransporterPricingList_test() {
        adw.accountType = EP_AccountConstant.VENDOR;
        Account acct = EP_AccountTestDataUtility.getAccount(adw);
        
        Test.startTest();
        LIST<account> result = localObj.getTransporterPricingList(EP_Common_Constant.EPUMA);
        Test.stopTest();
        System.AssertEquals(result.size(), 2);
        
        
        
    }
    //waiting for response. Create active vendor record.
    static testMethod void getAccountsVendorId_test() {
        
        
        Set<string> vendorIdSet;
        
        Test.startTest();
        LIST<Account> result = localObj.getAccountsVendorId(vendorIdSet);
        Test.stopTest();
        System.AssertEquals(result.size(), 0);
        
        
    }
    static testMethod void getAllShipTos_test() {
        Account acct = EP_TestDataUtility.getShipTo();
        Id accountId = acct.id;
        
        Test.startTest();
        LIST<Account> result = EP_AccountMapper.getAllShipTos(accountId);
        Test.stopTest();
        System.AssertEquals(result.size(), 0);
        
        
    }
    static testMethod void getChildShiptoByStatus_test() {
        Account acct = EP_TestDataUtility.getShipTo();
        Id accountId = acct.id;
        
        
        String Status = acct.EP_Status__c;
        
        Test.startTest();
        LIST<Account> result = EP_AccountMapper.getChildShiptoByStatus(accountId,Status);
        Test.stopTest();
        System.AssertEquals(result.size(), 0);        
        
    }

    static testMethod void getRecordsByCompositeId_test() {
        Test.startTest();
        Set<string> composite_Key = new Set<String>{'EP_Composite_Id__c'};
        List<Account> result = localObj.getRecordsByCompositeId(composite_Key);
        Test.stopTest();
        System.AssertEquals(result.size(), 0);
    }

    static testMethod void getCustomersMapByCompositeKey_test() {
        Test.startTest();
        Account sellTo = EP_TestDataUtility.getSellTo();
        sellTo.EP_Composite_Id__c = '866gyg';
        update sellTo;
        Set<String> setOfCustomerIDs = new Set<String>{sellTo.EP_Composite_Id__c};
        Map<String,Account> result = localObj.getCustomersMapByCompositeKey(setOfCustomerIDs);
        Test.stopTest();
        System.Assert(result.size()>0);
    }

    static testMethod void getTransportersDetails_test() {
        Test.startTest();
        Account sellTo = EP_TestDataUtility.getSellTo();
        sellTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        sellTo.EP_Source_Entity_ID__c = '090998';
        update sellTo;
        Set<String> setOfCustomerIDs = new Set<String>{sellTo.EP_Source_Entity_ID__c};
        
        map<String, Account> result = localObj.getTransportersDetails(setOfCustomerIDs);
        Test.stopTest();
        System.Assert(result.size()>0);
    }

    static testMethod void getAccountsMapByCompositeKey_test() {
        Test.startTest();
        Account sellTo = EP_TestDataUtility.getSellTo();
        sellTo.EP_Composite_Id__c = '866gyg';
        update sellTo;
        Set<String> setOfCustomerIDs = new Set<String>{sellTo.EP_Composite_Id__c};
        map<String, Account> result = localObj.getAccountsMapByCompositeKey(setOfCustomerIDs);
        Test.stopTest();
        system.assert(result.size()>0);
    }

    static testMethod void getDummyAccountWithShipToTanksById_test() {
        Test.startTest();
        Account shipTo = EP_TestDataUtility.getShipTo();
        Id AccountId = shipTo.parentId;
        List<string> shipToTypes = new List<String>{shipTo.recordType.developerName}; 
        string status = shipTo.EP_Status__c;

        List<Account> result = localObj.getDummyAccountWithShipToTanksById(AccountId, shipToTypes, status);
        Test.stopTest();
        system.assertEquals(result.size(), 1);
    }

    static testMethod void getSellToRecsByFilterSet_test() {
        Test.startTest();
        Account shipTo = EP_TestDataUtility.getShipTo();
        list<String> rtDevNameSet = new List<String>{shipTo.recordType.developerName};
        set<Id> custIdSet = new Set<Id>{shipTo.parentId};
        set<Id> parentRTIdSet = new set<Id>{shipTo.recordTypeId};
        set<String> statusSet = new set<string>{shipTo.EP_Status__c};

        List<Account> result = localObj.getSellToRecsByFilterSet(rtDevNameSet, custIdSet, parentRTIdSet, statusSet);
        Test.stopTest();
        system.assertEquals(result.size(), 0);
    }

    static testMethod void getSuppliersOfSupplyLocation_test() {
        Test.startTest();
        Account sellTo = EP_TestDataUtility.getSellTo();
        String strSupplyLocationId;
        List<Account> result = localObj.getSuppliersOfSupplyLocation(strSupplyLocationId);
        Test.stopTest();
        system.assertEquals(result.size(), 0);
    }

    static testMethod void getShipToRecordsSentToNAVandLS_test() {
        Test.startTest();
        Account sellTo = EP_TestDataUtility.getSellTo();
        sellTo.EP_SENT_TO_NAV_WINDMS__c = true;
        database.update(sellTo);
        Id sellToId;
        List<Account> result = localObj.getShipToRecordsSentToNAVandLS(sellToId);
        Test.stopTest();
        system.assertEquals(result.size(), 1);
    }

    static testMethod void getTransporterAccount_test() {
        Test.startTest();
        Account sellTo = EP_TestDataUtility.getSellTo();
        Id AccountId = sellTo.id;
        Account result = localObj.getTransporterAccount(AccountId);
        Test.stopTest();
        system.assertEquals(result.id, sellTo.id);
    }
    //Commented inOrder to increase coverage
    /*static testMethod void getRouteAllocations_test() {
        
        Account account = EP_TestDataUtility.getShipTo();
        Map<Id,EP_Route__c> routeRec = EP_TestDataUtility.getRouteObjects(1);
        EP_Route__c route = routeRec.values()[0];
        route.EP_Company__c = account.EP_Puma_Company__c;
        update route;
        account.EP_Default_Route__c = route.id;
        delete [Select Id from EP_Action__c];
        EP_Route_Allocation__c routeAllocation = new EP_Route_Allocation__c(EP_Delivery_Window_Start_Date__c = 2,Delivery_Window_End_Date__c=10,EP_Route__c=route.id,
                                                                                EP_Ship_To__c=account.id);
        insert routeAllocation;       
        Test.startTest();
        list<EP_Route_Allocation__c> routeList = localObj.getRouteAllocations(account);
        Test.stopTest();
        system.assert(routeList.size() >0);   
    }

    static testMethod void getRouteAllocationsBulk_test(){
        system.debug('+++++++++++++++Limits.getLimitQueries() initial  '+Limits.getLimitQueries()); 
        List<EP_Route_Allocation__c> routeList = new List<EP_Route_Allocation__c>();
        Account account = EP_TestDataUtility.getShipTo();
        Map<Id,EP_Route__c> routeRec = EP_TestDataUtility.getRouteObjects(1);
        EP_Route__c route = routeRec.values()[0];
        route.EP_Company__c = account.EP_Puma_Company__c;
        update route;
        account.EP_Default_Route__c = route.id;
        delete [Select Id from EP_Action__c];
        List<Account> accList = new List<Account>{account};
        EP_Route_Allocation__c routeAllocation = new EP_Route_Allocation__c(EP_Delivery_Window_Start_Date__c = 2,Delivery_Window_End_Date__c=10,EP_Route__c=route.id,
                                                                                EP_Ship_To__c=account.id);
        EP_Route_Allocation__c routeAllocationobj = new EP_Route_Allocation__c(EP_Delivery_Window_Start_Date__c = 2,Delivery_Window_End_Date__c=10,EP_Route__c=route.id,
                                                                                EP_Ship_To__c=account.id);
        routeList.add(routeAllocation);
        routeList.add(routeAllocationobj);
        insert routeList;   
        Test.startTest();
        Map<String,list<EP_Route_Allocation__c>> routeMap = localObj.getRouteAllocationsBulk(accList);
        Test.stopTest();
        system.assert(routeMap!=null);
    }*/

    static testMethod void getAllShipToAndActionsFromSellTo_test(){
        Account account = EP_TestDataUtility.getShipTo();
        Account accParent = account.clone();
        account.Parentid = accParent.id;
        //update account;
        List<id> accId = new List<Id>{account.id};
        Test.startTest();
        List<Account> accList = EP_AccountMapper.getAllShipToAndActionsFromSellTo(accId);
        Test.stopTest();
        system.assert(accList.size()>0);
    }

    static testMethod void getAllShipToFromSellTo_test(){
        Account account = EP_TestDataUtility.getSellToASBasicDataSetUp();
        Account accParent = account.clone();
        account.Parentid = accParent.id;
        update account;
        set<id> accId = new set<Id>{account.id};
        Test.startTest();
        List<Account> accList = EP_AccountMapper.getAllShipToFromSellTo(accId);
        Test.stopTest();
        system.assert(accList.size()>0);
    }

    static testMethod void getSellToAccountWithRelatedProdOption_test(){
        Account account = EP_TestDataUtility.getSellToASBasicDataSetUp();
        account.EP_Puma_Company__c = null;
        update account;
        EP_Product_Option__c product = EP_TestDataUtility.createProductOption(account.id,Test.getStandardPricebookId());
        Test.startTest();
        Account acc = EP_AccountMapper.getSellToAccountWithRelatedProdOption(account.id);
        Test.stopTest();
        system.assert(acc!=null);
    }

    static testMethod void getAccountSyncValues_test(){
        Account account = EP_TestDataUtility.getSellToASBasicDataSetUp();
        Test.startTest();
        Account acc = localObj.getAccountSyncValues(account.id);
        Test.stopTest();
        system.assert(acc!=null);
    }

    static testMethod void getShipToRecords_test(){
        Test.startTest();
        Account account = EP_TestDataUtility.getShipTo();
        Account accParent = account.clone();
        account.Parentid = accParent.id;
        //update account;
        set<id> accId = new set<Id>{account.id};
        //Test.startTest();
        List<Account> accList = localObj.getShipToRecords(accId);
        Test.stopTest();
        system.assert(accList.size()>0);
    }

    static testMethod void getShipToWithTanksById_test(){
        Account account = EP_TestDataUtility.getShipTo();
        List<Account> accList = new List<Account>{account};
        EP_TestDataUtility.createStockLocation(account.id,true);
        Test.startTest();
        EP_AccountMapper accMapper = new EP_AccountMapper(accList);
        Account acc = accMapper.getShipToWithTanksById(account.id);
        Test.stopTest();
        system.assert(acc!=null);
    }

    static testMethod void getChildShiptoByStatus1_test(){
        Account account = EP_TestDataUtility.getShipTo();
        Account acc = account.clone();
        account.ParentId = acc.id;
        //update account;
        set<String> status = new set<string>{'01-Prospect'};
        Test.startTest();
        List<Account> accList = EP_AccountMapper.getChildShiptoByStatus(acc.id,status);
        Test.stopTest();
        system.assert(accList.size()>0);
    }

    static testMethod void getMapOfRecordsByIds_test(){
        Account account = EP_TestDataUtility.getShipTo();
        set<id> accID = new set<id>{account.id};
        test.startTest();
        Map<Id,Account> mapAcc = localObj.getMapOfRecordsByIds(accID);
        Test.stopTest();
        system.assert(mapAcc.size()>0);
    }
}