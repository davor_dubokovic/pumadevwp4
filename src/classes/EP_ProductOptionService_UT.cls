/* 
      @Author <Kamendra Singh>
       @name <EP_ProductOptionService_UT>
       @CreateDate <24/11/2016>
       @Description  <This is apex test class for EP_ProductOptionService> 
       @Version <1.0>
    */
    @isTest
    public class EP_ProductOptionService_UT{
        /**
        * @author kamendra.singh@accenture.com
        * @date 24/11/2017
        * @description create test data for all required object in test methods
        */
        @TestSetup
        static void initData(){
            //create Price Book 
            PriceBook2 tempPricebook = EP_TestDataUtility.createPriceBookWithCompany();
            
            //Create Sell To Account
            Account tempSellToAcc = EP_TestDataUtility.createSellToAccount(null,null);
            tempSellToAcc.EP_Puma_Company__c = temppricebook.EP_Company__c;
            tempSellToAcc.EP_Status__c= EP_Common_Constant.STATUS_ACTIVE;
            tempSellToAcc.CurrencyIsoCode = 'AUD';
            database.insert(tempSellToAcc);
            
            //create Ship To Account
            Account tempShipToAcc = EP_TestDataUtility.createShipToAccount(tempSellToAcc.Id,EP_TestDataUtility.VMI_SHIP_RT );
            database.insert(tempShipToAcc);
            
            //create Product2 
            Product2 tempProduct = EP_TestDataUtility.createProduct2(true); 
            
            //Create PriceBookEntry
            PricebookEntry tempPriceBookEntry = EP_TestDataUtility.createPricebookEntry(tempProduct.id,tempPricebook.id);
            tempPriceBookEntry.EP_Attached_to_Standard_Product_List__c = true;
            tempPriceBookEntry.EP_Is_Sell_To_Assigned__c = false;
            database.insert(tempPriceBookEntry);
            
            //Create Tank...
            EP_Tank__c  tempTank = EP_TestDataUtility.createTestEP_Tank(tempShipToAcc.id,tempProduct.id);
            database.insert(tempTank);
            
            //Create Product Option
            EP_Product_Option__c objProdOption = EP_TestDataUtility.createProductOption(tempSellToAcc.Id,temppricebook.id);
        }
        
        /**
        * @author kamendra.singh@accenture.com
        * @date 24/11/2017
        * @description Test to check positive scenario of iscurrencySameOnProductsAndSellTo method.
        */
        static testMethod void iscurrencySameOnProductsAndSellTo_PositiveTest(){
            list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
            
            EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
            
            Test.startTest();
            EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
            Boolean result = ProductOptionService.iscurrencySameOnProductsAndSellTo(lstProdOption[0]);
            Test.stopTest();
            //Asert to check result
            System.assertEquals(result,true);
        }
        
        /**
        * @author kamendra.singh@accenture.com
        * @date 24/11/2017
        * @description Test to check Negative scenario of iscurrencySameOnProductsAndSellTo method.
        */
        static testMethod void iscurrencySameOnProductsAndSellTo_NegativeTest(){
            Account tempAcc = [select id,CurrencyIsoCode from Account where recordTypeId =:EP_TestDataUtility.SELLTO_RT];
            tempAcc.CurrencyIsoCode = 'GBP';
            database.update(tempAcc);
            
            list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
            
            EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
            
            Test.startTest();
            EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
            Boolean result = ProductOptionService.iscurrencySameOnProductsAndSellTo(lstProdOption[0]);
            Test.stopTest();
            //Asert to check result
            System.assertEquals(result,false);
        }
        
        /**
        * @author kamendra.singh@accenture.com
        * @date 24/11/2017
        * @description Test to check Positive scenario of isProductAssociatedWithOperationalTank method.
        */
        static testMethod void isProductAssociatedWithOperationalTank_PositiveTest(){
            list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
            
            EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
            
            Test.startTest();
            EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
            Boolean result = ProductOptionService.isProductAssociatedWithOperationalTank(lstProdOption[0]);
            Test.stopTest();
            //Asert to check result
            System.assertEquals(result,true);
        }   
        /**
        * @author kamendra.singh@accenture.com
        * @date 24/11/2017
        * @description Test to check Negative scenario of isProductAssociatedWithOperationalTank method.
        */
        static testMethod void isProductAssociatedWithOperationalTank_NegativeTest(){
            EP_Tank__c tempTank = [select Id from EP_Tank__c];
            database.delete(tempTank);
            
            list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
            
            EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
            
            Test.startTest();
            EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
            Boolean result = ProductOptionService.isProductAssociatedWithOperationalTank(lstProdOption[0]);
            Test.stopTest();
            //Asert to check result
            System.assertEquals(result,false);
        }
        /**
        * @author kamendra.singh@accenture.com
        * @date 24/11/2017
        * @description Test to check positive scenario of getRelatedPriceBookEntry method.
        */
        static testMethod void getRelatedPriceBookEntry_PositiveTest(){
            list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
            EP_Product_Option__c tempProdOption = new EP_Product_Option__c(Sell_To__c=lstProdOption[0].Sell_To__c,Price_List__c=lstProdOption[0].Price_List__c);

            EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
            
            database.delete(lstProdOption);
            
            Test.startTest();
            EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
            list<PriceBookEntry> result = ProductOptionService.getRelatedPriceBookEntry(tempProdOption);
            Test.stopTest();
            //Asert to check result
            System.assertEquals(result.size(),1);
        }
        
        /**
        * @author kamendra.singh@accenture.com
        * @date 24/11/2017
        * @description Test to check Negative scenario of getRelatedPriceBookEntry method.
        */
        static testMethod void getRelatedPriceBookEntry_NegativeTest(){
            list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
            EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
            Test.startTest();
            EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
            list<PriceBookEntry> result = ProductOptionService.getRelatedPriceBookEntry(lstProdOption[0]);
            Test.stopTest();
            //Asert to check result
            System.assertEquals(result.size(),0);
        }
        
        /**
        * @author kamendra.singh@accenture.com
        * @date 24/11/2017
        * @description Test to check Positive scenario of checkPriceBookEntryStatus method.
        */
        static testMethod void checkPriceBookEntryStatus_PositiveTest(){
            Product2 tempProduct = [select id from Product2];
        
            //create Price Book 
            PriceBook2 tempPricebook = EP_TestDataUtility.createPricebook2(false);
            tempPricebook.Name = 'PriceBook B';
            database.insert(tempPricebook);
            
            //Create PriceBookEntry
            PricebookEntry tempPriceBookEntry = EP_TestDataUtility.createPricebookEntry(tempProduct.id,tempPricebook.id);
            tempPriceBookEntry.EP_Attached_to_Standard_Product_List__c = true;
            tempPriceBookEntry.EP_Is_Sell_To_Assigned__c = false;
            database.insert(tempPriceBookEntry);
            
            list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
            
            EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
            
            Test.startTest();
            EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
            Boolean result = ProductOptionService.checkPriceBookEntryStatus(tempPriceBookEntry);
            Test.stopTest();
            //Asert to check result
            System.assertEquals(result,true);
        }
        
        /**
        * @author kamendra.singh@accenture.com
        * @date 24/11/2017
        * @description Test to check Negative scenario of checkPriceBookEntryStatus method.
        */
        static testMethod void checkPriceBookEntryStatus_NegativeTest(){
            Product2 tempProduct = [select id from Product2];
        
            //create Price Book 
            PriceBook2 tempPricebook = EP_TestDataUtility.createPricebook2(false);
            tempPricebook.Name = 'PriceBook B';
            database.insert(tempPricebook);
            
            //Create PriceBookEntry
            PricebookEntry tempPriceBookEntry = EP_TestDataUtility.createPricebookEntry(tempProduct.id,tempPricebook.id);
            tempPriceBookEntry.EP_Attached_to_Standard_Product_List__c = true;
            tempPriceBookEntry.EP_Is_Sell_To_Assigned__c = true;
            database.insert(tempPriceBookEntry);
            
            list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
            EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
            
            Test.startTest();
            EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
            Boolean result = ProductOptionService.checkPriceBookEntryStatus(tempPriceBookEntry);
            Test.stopTest();
            //Asert to check result
            System.assertEquals(result,false);
        }
        /**
    * @author kamendra.singh@accenture.com
    * @date 24/11/2017
    * @description Test to check getAccountIdToAccount method.
    */
    static testMethod void getAccountIdToAccount_test(){
        list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
        
        EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
        
        Test.startTest();
        EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
        map<Id,Account> result = ProductOptionService.getAccountIdToAccount();
        Test.stopTest();
        
        System.assert(result != null);
    }
    /**
    * @author kamendra.singh@accenture.com
    * @date 24/11/2017
    * @description Test to check getPriceBookEntry method.
    */
    static testMethod void getPriceBookEntry_Test(){
        list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
        EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
        
        Test.startTest();
        EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
        map<Id,list<PriceBookEntry>> result = ProductOptionService.getPriceBookEntry();
        Test.stopTest();
        
        System.assert(result != null);
    }
    /**
    * @author kamendra.singh@accenture.com
    * @date 24/11/2017
    * @description Test to check getIdTolstPriceBookEntry method.
    */
    static testMethod void getIdTolstPriceBookEntry_Test(){
        list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
        list<PricebookEntry> lstPricebookEntry = [select id,Pricebook2Id from PricebookEntry];
        EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
        
        Test.startTest();
        EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
        Map<Id,list<PriceBookEntry>> result = ProductOptionService.getIdTolstPriceBookEntry(lstPricebookEntry);
        Test.stopTest();
        
        System.assert(result != null);
    }
    /**
    * @author kamendra.singh@accenture.com
    * @date 24/11/2017
    * @description Test to check getSellTolstTanks method.
    */
    static testMethod void getSellTolstTanks_Test(){
        list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
        EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
        
        Test.startTest();
        EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
        map<Id,list<EP_Tank__c>> result = ProductOptionService.getSellTolstTanks();
        Test.stopTest();
        
        System.assert(result != null);
    }
    /**
    * @author kamendra.singh@accenture.com
    * @date 24/11/2017
    * @description Test to check returnSetAccIds method.
    */
    static testMethod void returnSetAccIds_Test(){
        list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
        EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
        
        Test.startTest();
        EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
        set<Id> result = ProductOptionService.returnSetAccIds();
        Test.stopTest();
        
        System.assertEquals(result.size(), 1);
    }
    /**
    * @author kamendra.singh@accenture.com
    * @date 24/11/2017
    * @description Test to check returnSetPriceBookIds method.
    */
    static testMethod void returnSetPriceBookIds_Test(){
        list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
        EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
        
        Test.startTest();
        EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
        set<Id> result = ProductOptionService.returnSetPriceBookIds();
        Test.stopTest();
        //Asert 
        System.assertEquals(result.size(), 1);
    }
    /**
    * @author kamendra.singh@accenture.com
    * @date 24/11/2017
    * @description Test to check getPriceBookTolstProductOption method.
    */
    static testMethod void getPriceBookTolstProductOption_Test(){
        list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
        EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
        
        Test.startTest();
            EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
            map<Id,list<EP_Product_Option__c>> result = ProductOptionService.getPriceBookTolstProductOption();
        Test.stopTest();
        //Asert 
        System.assert(result != null);
    }
    /**
    * @author kamendra.singh@accenture.com
    * @date 24/11/2017
    * @description Test to check doBeforeInsertHandle method.
    */
    static testMethod void doBeforeInsertHandle_PositiveTest(){
        Account tempAccount  =  [select id,CurrencyIsoCode from Account where recordTypeId =:EP_TestDataUtility.SELLTO_RT];
        tempAccount.CurrencyIsoCode = 'AUD';
        database.update(tempAccount);
        
        list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
        EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
        
        Test.startTest();
            EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
            ProductOptionService.doBeforeInsertHandle();
        Test.stopTest();
        //No Asserts requred , As this methdods delegates to other methods
    }
    /**
    * @author kamendra.singh@accenture.com
    * @date 24/11/2017
    * @description Test to check doBeforeUpdateHandle method.
    */
    static testMethod void doBeforeUpdateHandle_Test(){
        
        map<Id,EP_Product_Option__c> triggerOldMap =  new map<Id,EP_Product_Option__c>();
        Product2 tempProduct = [select id from Product2];
        
        //create Price Book 
        PriceBook2 tempPricebook = EP_TestDataUtility.createPricebook2(false);
        tempPricebook.Name = 'PriceBook B';
        database.insert(tempPricebook);
        
        //Create PriceBookEntry
        PricebookEntry tempPriceBookEntry = EP_TestDataUtility.createPricebookEntry(tempProduct.id,tempPricebook.id);
        tempPriceBookEntry.EP_Attached_to_Standard_Product_List__c = true;
        tempPriceBookEntry.EP_Is_Sell_To_Assigned__c = true;
        database.insert(tempPriceBookEntry);
        
        list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
        
        for(EP_Product_Option__c prodOption : lstProdOption){
            EP_Product_Option__c objProdOption = new EP_Product_Option__c(id=prodOption.Id,Price_List__c = tempPricebook.id);
            if(!triggerOldMap.containskey(objProdOption.id)){               
                triggerOldMap.put(objProdOption.id,objProdOption);
            }
        }
        
        EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption,triggerOldMap);
        
        Test.startTest();
            EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
            ProductOptionService.doBeforeUpdateHandle();
        Test.stopTest();
        //No Asserts requred , As this methdods delegates to other methods
    }
    /**
    * @author kamendra.singh@accenture.com
    * @date 24/11/2017
    * @description Test to check doBeforeDeleteHandle method.
    */
    static testMethod void doActionOnBeforeDelete_Test(){
        list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
        EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
        
        Test.startTest();
            EP_ProductOptionService ProductOptionService = new EP_ProductOptionService(ProductOptionDomain);
            ProductOptionService.doBeforeDeleteHandle();
        Test.stopTest();
        //No Asserts requred , As this methdods delegates to other methods
    }

    }