/*
*  @Author <Accenture>
*  @Name <EP_ASTNonVMIShipToInActiveToActive>
*  @CreateDate <28/02/2017>
*  @Description <Handles VMI Ship To Account status change from 07-Inactive to 05-Active>
*  @Version <1.0>
*/
public class EP_ASTNonVMIShipToInActiveToActive extends EP_AccountStateTransition {

    public EP_ASTNonVMIShipToInActiveToActive () {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToInActiveToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToInActiveToActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToInActiveToActive','isGuardCondition');
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToInActiveToActive','doOnExit');

    }
}