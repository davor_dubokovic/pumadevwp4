public with sharing class EP_ASTSellToActiveToActive  extends EP_AccountStateTransition{
    public EP_ASTSellToActiveToActive() {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToActiveToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToActiveToActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

	public override boolean isGuardCondition(){
    	EP_GeneralUtility.Log('Public','EP_ASTSellToActiveToActive','isGuardCondition');
    	EP_AccountService service = new EP_AccountService(this.account);
        if(service.isDeliveryTypeChanged() && EP_AccountConstant.DELIVERY.equalsIgnoreCase(this.account.localAccount.EP_Delivery_Type__c)){
		    if(!service.isSetUpShipToAssociated()){
		        accountEvent.isError = true;
		        accountEvent.setEventMessage(System.Label.EP_No_Set_Up_Ship_To);
		        return false;
		    }    
		}
        return true;
    }
}