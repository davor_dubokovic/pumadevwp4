/*
 *  @Author <Accenture>
 *  @Name <EP_VendorASActive>
 *  @CreateDate <>
 *  @Description <Vendor Account State for 05-Active Status>
 *  @Version <1.0>
 */
 public with sharing class EP_VendorASActive extends EP_AccountState{
     
    public EP_VendorASActive() {
        
    }

    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_VendorASActive','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }

    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_VendorASActive','doOnEntry');
        
    }  

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_VendorASActive','doOnExit');
        
    }
    
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_VendorASActive','doTransition');
        return super.doTransition();
    }

    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_VendorASActive','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    }

}