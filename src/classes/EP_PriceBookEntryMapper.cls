/**
   @Author          CR Team
   @name            EP_PriceBookEntryMapper
   @CreateDate      12/21/2016
   @Description     This class contains all SOQLs related to PriceBookEntry Object
   @Novasuite Fixes - Required Documentation
   @Version         1.0
   @reference       NA
*/

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    12/28/2016          Shakti Mehtani          1) Added method getRecordsByFilterSet_1()
    *************************************************************************************************************
*/
public with sharing class EP_PriceBookEntryMapper {
     /***NOvasuite fix constructor removed**/ 
    
     /***L4-45352 start****/
    /* *  Returns All Price Book Entry Records By PriceBook Ids.
    *  @name             getRecordsByPricebookIds
    *  @param            set<id>
    *  @return           list<PriceBookEntry>
    *  @throws           NA
    */   
    public List<PriceBookEntry> getALLRecordsByPriceBookId(Id PricebookId){
        list<PriceBookEntry> priceBkEntryObjList = new list<PriceBookEntry>([SELECT id,Product2.Name, Name,Product2.ProductCode, Pricebook2Id, EP_Is_Sell_To_Assigned__c, 
                                                           Product2.EP_Company_Lookup__c , Product2.EP_Product_Sold_As__c,
                                                           Product2Id,ProductCode,CurrencyIsoCode,UnitPrice, Product2.EP_Is_System_Product__c,isActive,PriceBook2.id
                                                    FROM PriceBookEntry
                                                    WHERE Pricebook2Id =:PricebookId
                                                        ]);
                                    
        return priceBkEntryObjList;
    }
    
    // Requirement 59441 - Start
     /***L4-45352 end****/
     /* *  Returns Price Book Entry Records By PriceBook Ids.
    *  @name             getRecordsByPricebookIds
    *  @param            set<id>
    *  @return           list<PriceBookEntry>
    *  @throws           NA
    */   
    public List<PriceBookEntry> getActiveRecordsByPriceBookId(Id PricebookId){
        list<PriceBookEntry> priceBkEntryObjList = new list<PriceBookEntry>([SELECT id, Name, Pricebook2Id, EP_Is_Sell_To_Assigned__c, 
                                                           Product2.EP_Company_Lookup__c , Product2.EP_Product_Sold_As__c,
                                                           Product2Id,ProductCode,CurrencyIsoCode,UnitPrice, Product2.EP_Is_System_Product__c
                                                    FROM PriceBookEntry
                                                    WHERE Pricebook2Id =:PricebookId
                                                        AND isActive = true]);
                                    
        return priceBkEntryObjList;
    }
    // Requirement 59441 - End

    /* *  Returns Price Book Entry Records By PriceBook Ids.
    *  @name             getRecordsByPricebookIds
    *  @param            set<id>
    *  @return           list<PriceBookEntry>
    *  @throws           NA
    */   
    public list<PriceBookEntry> getRecordsByPricebookIds(set<id> idSet_1) {
    // Requirement 59441 - Start
        list<PriceBookEntry> priceBkEntryObjList = new list<PriceBookEntry>();
        for(list<PriceBookEntry> priceBkEntryList :[SELECT id, 
                                                           Name, 
                                                           /***L4-45352 start****/
                                                           isActive, 
                                                           /***L4-45352 end****/
                                                           Pricebook2Id, 
                                                           EP_Is_Sell_To_Assigned__c, 
                                                           Product2.EP_Company_Lookup__c , 
                                                           Product2.EP_Product_Sold_As__c,
                                                           Product2Id,
                                                           product2.name,
                                                           Product2.EP_Is_System_Product__c,
                                                           currencyisoCode
                                                    FROM PriceBookEntry
                                                    WHERE Pricebook2Id IN :idSet_1 
                                        ]){
            priceBkEntryObjList.addAll(priceBkEntryList );                                
        }                               
        return priceBkEntryObjList;
    }    
    // Requirement 59441 - end

    /* *  Returns Price Book Entry Records By PriceBook Ids, CountryISOCode, EP_Product_Sold_As__c and isActive.
    *  @author           Shakti Mehtani
    *  @date             12/28/2016
    *  @name             getRecordsByFilterSet_1
    *  @param            set<id>, set<id>, set<id> 
    *  @return           list<PriceBookEntry>
    *  @throws           NA
    */   
    public list<PriceBookEntry> getRecordsByFilterSet_1(set<id> idSet_1, set<String> stringSet_2, set<String> stringSet_3, Boolean isActive) {
    // Requirement 59441 - start
        list<PriceBookEntry> priceBkEntryObjList= new list<PriceBookEntry>();
         
        for(list<PriceBookEntry> priceBkEntryList :[ SELECT 
                                                    ID
                                                    , Product2Id
                                                    , PriceBook2.Name
                                                    , Product2.Name
                                                    , Product2.EP_Unit_Of_Measure__c
                                                    , Product2.EP_Product_Sold_As__c
                                                    , Pricebook2Id
                                                    , UnitPrice
                                                    , EP_Unit_Of_Measure__c
                                                    , Product2.EP_Is_System_Product__c
                                                FROM PriceBookEntry 
                                                WHERE PriceBook2Id IN :idSet_1
                                                AND Product2.IsActive = :isActive
                                                AND CurrencyIsoCode IN :stringSet_2
                                                AND Product2.EP_Product_Sold_As__c IN :stringSet_3
                                            ]){
            priceBkEntryObjList.addAll(priceBkEntryList );                         
        }                               
        return priceBkEntryObjList;
    }
    // Requirement 59441 - end
    /*  @author           Accenture
    *  @name             getRecordsByPricebookEntry
    *  @param            Set<Id>
    *  @return           List<PriceBookEntry>
    */
    public List<PriceBookEntry> getRecordsByPricebookEntry(Set<Id> setPricebookEntryIds) {

        List<PriceBookEntry>  lstPriceBookEntry = [SELECT CurrencyIsoCode, Pricebook2Id, Product2Id 
                                            FROM PricebookEntry 
                                            WHERE Id IN: setPricebookEntryIds];

        return lstPriceBookEntry;        
    }
        
    /* *  Returns number of packaged products for a pricebook
    *  @author           Accenture
    *  @date             2/20/2017
    *  @name             getNumberOfPackagedProducts
    *  @param            Id
    *  @return           Integer
    *  @throws           NA
    */
    public Integer getNumberOfPackagedProducts(Id priceBookId) {
        return [Select count() from PricebookEntry 
                where Pricebook2Id = :priceBookId AND Product2.EP_Product_Sold_As__c = :EP_Common_Constant.PRODUCT_PACKAGED];
    }
    
    /* *  Returns number of pricebookentries of same pricebook attached on account where company is different 
    *  @author           Accenture
    *  @date             2/20/2017
    *  @name             getCountOfProductsRelatedToOtherCompany
    *  @param            Account
    *  @return           Integer
    *  @throws           NA
    */
    public Integer getCountOfProductsRelatedToOtherCompany(account account){
        return [Select count() from PriceBookEntry 
                                          where PriceBook2Id=: account.EP_PriceBook__c 
                                             AND  Product2.EP_Company_Lookup__c !=: account.EP_Puma_Company__c];
    }
    
    /**
    * @author       Accenture
    * @name         getPriceBookEntries
    * @date         2/20/2017
    * @description  This method returns list of PriceBookEntry Records By PriceBook Ids, CountryISOCode and ProductCode
    * @param        set<string> productCodeSet,  set<Id> priceBook2IdSet, set<string> currencyISOCodeSet
    * @return       list<PriceBookEntry>
    */        
    public list<PriceBookEntry> getPriceBookEntries(set<string> productCodeSet, 
                                                    set<Id> priceBook2IdSet, 
                                                        set<string> currencyISOCodeSet) {
        // Requirement 59441 - start
        list<PriceBookEntry> priceBkEntryObjList = new list<PriceBookEntry>();
        for(list<PriceBookEntry> priceBkEntry : [Select Id ,
                                                    ProductCode,
                                                    CurrencyIsoCode,
                                                    Pricebook2Id ,
                                                    UnitPrice,
                                                    Product2Id,
                                                    Product2.EP_Product_Sold_As__c,
                                                    Product2.EP_Is_System_Product__c
                                                    From PriceBookEntry
                                                    Where ProductCode IN: productCodeSet
                                                    AND PriceBook2Id IN: priceBook2IdSet
                                                    AND IsActive = true 
                                                    AND CurrencyISOCode IN: currencyISOCodeSet]) {
            priceBkEntryObjList.addAll(priceBkEntry);
        }
        return priceBkEntryObjList;
    }
    // Requirement 59441 - end
    
    /* Requirement 59441 - Start
    * @author       Accenture
    * @name         getPriceBookEntriesforExRack
    * @param        Set<Id>,Id,Order
    * @return       list<PriceBookEntry>
    */
    public list<PriceBookEntry> getPriceBookEntriesforExRack(Set<Id> availableProductIds,Id pricebookId,Order orderObj) {
        list<PriceBookEntry> priceBkEntryObjList = new list<PriceBookEntry>();
        /*priceBkEntryObjList = [SELECT ID, Product2Id, PriceBook2.Name, Product2.Name, Product2.EP_Unit_Of_Measure__c,Pricebook2Id, UnitPrice, EP_Unit_Of_Measure__c ,Product2.EP_Product_Sold_As__c, Product2.EP_Is_System_Product__c
                               FROM PriceBookEntry WHERE PriceBook2Id =: pricebookId
                               AND Product2.IsActive = TRUE
                               AND (Product2Id IN : availableProductIds OR Product2.Name=: EP_Common_Constant.TAX_PRODUCT_NAME)
                               AND CurrencyIsoCode =: orderObj.CurrencyIsoCode
                               AND Product2.EP_Product_Sold_As__c =: orderObj.EP_Order_Product_Category__c];*/
        return priceBkEntryObjList;               
    }
    // Requirement 59441 - end

    public list<PriceBookEntry> getCsPriceBookEntriesforExRack(Set<Id> availableProductIds,Id pricebookId,csord__Order__c orderObj) {
        list<PriceBookEntry> priceBkEntryObjList = new list<PriceBookEntry>();
        priceBkEntryObjList = [SELECT ID, Product2Id, PriceBook2.Name, Product2.Name, Product2.EP_Unit_Of_Measure__c,Pricebook2Id, UnitPrice, EP_Unit_Of_Measure__c,Product2.EP_Product_Sold_As__c, Product2.EP_Is_System_Product__c
                               FROM PriceBookEntry WHERE PriceBook2Id =: pricebookId
                               AND Product2.IsActive = TRUE
                               AND (Product2Id IN : availableProductIds OR Product2.Name=: EP_Common_Constant.TAX_PRODUCT_NAME)
                               AND CurrencyIsoCode =: orderObj.CurrencyIsoCode
                               AND Product2.EP_Product_Sold_As__c =: orderObj.EP_Order_Product_Category__c];
        return priceBkEntryObjList;                       
    }
    
}