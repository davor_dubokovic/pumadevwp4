/*   
     @Author Aravindhan Ramalingam
     @name <EP_OrderStateInvoiced.cls>     
     @Description <Order State for Invoiced status>   
     @Version <1.1> 
     */

     public class EP_OrderStateInvoiced extends EP_OrderState {

        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            super.setOrderDomainObject(currentOrder);
        }

        
        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_OrderStateInvoiced','doOnEntry');
        }
        
        public static String getTextValue()
        {
            return EP_OrderConstant.OrderState_Invoiced;
        }
    }