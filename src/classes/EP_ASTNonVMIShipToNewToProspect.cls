/*
*  @Author <Accenture>
*  @Name <EP_ASTNonVMIShipToNewToProspect>
*  @CreateDate <28/02/2017>
*  @Description <Handles VMI Ship To Account status change from New to 01-Prospect>
*  @Version <1.0>
*/
public class EP_ASTNonVMIShipToNewToProspect extends EP_AccountStateTransition {

    public EP_ASTNonVMIShipToNewToProspect () {
        finalState = EP_AccountConstant.PROSPECT;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToNewToProspect','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToNewToProspect','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToNewToProspect','isGuardCondition');
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToNewToProspect','doOnExit');

    }
}