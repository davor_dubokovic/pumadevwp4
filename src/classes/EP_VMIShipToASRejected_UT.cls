@isTest
public class EP_VMIShipToASRejected_UT
{
    static final string EVENT_NAME = '08-RejectedTo08-Rejected';
    static final string INVALID_EVENT_NAME = '08-ProspectTo04-Account Set-up';
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    }
    
    static testMethod void setAccountDomainObject_test() {
        EP_VMIShipToASRejected localObj = new EP_VMIShipToASRejected();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASRejectedDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        system.assertEquals(obj.getAccount().Id,localObj.account.getAccount().Id);
    }
    static testMethod void doOnEntry_test() {
        EP_VMIShipToASRejected localObj = new EP_VMIShipToASRejected();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASRejectedDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //Method has no implementation, hence adding dummy assert
        system.assert(true);
    }
    static testMethod void doOnExit_test() {
        EP_VMIShipToASRejected localObj = new EP_VMIShipToASRejected();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASRejectedDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        //Method has no implementation, hence adding dummy assert
        system.assert(true);
    }
    static testMethod void doTransition_PositiveScenariotest() {
        EP_VMIShipToASRejected localObj = new EP_VMIShipToASRejected();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASRejectedDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_VMIShipToASRejected localObj = new EP_VMIShipToASRejected();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASRejectedDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        List<Exception> excList = new List<Exception>();
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
            }
        catch(Exception e){
            excList.add(e);
        }
        Test.stopTest();
        System.Assert(excList.size()>0);
    }
    
    /* isInboundTransitionPossible does not have negative scenerio because it does not have implementation. It always returns true */
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_VMIShipToASRejected localObj = new EP_VMIShipToASRejected();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASRejectedDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    //L4_45352_Start
    static testMethod void doOnEntry1_test() {
      Profile sysAdmin = [Select id from Profile
                            Where Name = 'System Administrator' 
                            limit :EP_Common_Constant.ONE];
        User sysAdminUser = EP_TestDataUtility.createUser(sysAdmin.id);
    
        EP_VMIShipToASRejected localObj = new EP_VMIShipToASRejected();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        account acc = obj.getAccount();
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_WINDMS__c = true;
        acc.EP_Status__c = '02-Basic Data Setup';
        update acc;
        
        List<Ep_action__c> action = [select id,EP_Account__c,EP_Status__c,EP_Tank__c,Action_Type__c from ep_action__c where EP_Account__c=:acc.id];
        
        
        system.runas(sysAdminUser){
            
                action[0].OwnerId = sysAdminUser.id;
                action[0].Ep_status__c = '  03-Rejected';
                action[0].EP_Reason__c = 'Rejected';
                
                Update action[0];
            
        
        }
        
        EP_AccountService service = new EP_AccountService(obj);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //Method has no implementation, hence adding dummy assert
        system.assert(true);
    }
    //L4_45352_end
    
}