/* 
   * @Author <Accenture>
   * @name <EP_CreditExceptionUpdateHandler>
   * @CreateDate <03/14/2017>
   * @Description <This is a class to Transform JSON string data into SOBject(Credit Exception and Order) for Credit Exception Status Update from WINDMS Rest Service Request> 
   * @Version <1.0>
*/ 
public with sharing class EP_CreditExceptionUpdateHandler extends EP_InboundHandler {
    private static boolean processingFailed = false;
    private static string failureReason;
    private static final String DATASET_NAME = 'CRED_EXCEPTIONs';
    private static final String CS_INBOUND_MESSAGE = 'CREDIT_EXCEPTION_STATUS_UPDATE';
    private static final String CLASS_NAME = 'EP_CreditExceptionUpdateHandler';
    @testVisible private static list <EP_AcknowledgementStub.dataSet> ackResponseList  = new List <EP_AcknowledgementStub.dataSet>();
    private static EP_CreditExceptionUpdateHelper creditExceptionUpdateHelper = new EP_CreditExceptionUpdateHelper();
    /**  
     * @name : processRequest  
     * @description :This method process the JSON Input string and returns jsonResponse. This method will be called from global class 'EP_OrderStatusUpdateWS'.   
     * @param String -It takes JSON requestBody.
     * @return string - It returns json Response.
     */
    public override String processRequest(String requestBody){
        EP_GeneralUtility.Log('Public',CLASS_NAME,'processRequest');
        EP_MessageHeader headerCommon = new EP_MessageHeader();
        try {
            EP_CreditExceptionUpdateStub stub = (EP_CreditExceptionUpdateStub )  System.JSON.deserialize(requestBody, EP_CreditExceptionUpdateStub.class);
            list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionList = stub.MSG.Payload.Any0.creditExceptionStatus.status;
            headerCommon = stub.MSG.HeaderCommon;

            creditExceptionUpdateHelper = new EP_CreditExceptionUpdateHelper();
            creditExceptionUpdateHelper.setCreditExceptionAttributes(creditExceptionList);
            processCreditExceptions(creditExceptionList);
            
        } catch (exception exp ) {
            failureReason = exp.getMessage();
            EP_LoggingService.logServiceException(exp, UserInfo.getOrganizationId(),  EP_Common_constant.EPUMA, 'processRequest', CLASS_NAME, EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF, EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);
            createResponse(null, exp.getTypeName(), exp.getMessage()); 
        }
        
        string jsonResponse = EP_AcknowledgementHandler.createAcknowledgement(CS_INBOUND_MESSAGE, processingFailed, failureReason, headerCommon, ackResponseList);
        return jsonResponse;
    }

    /**
    * @Author       Accenture
    * @Name         processCreditExceptions
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void processCreditExceptions(list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionList) {
        EP_GeneralUtility.Log('private',CLASS_NAME,'processCreditExceptions');
        Map<ID, string> credExId_SeqIdMap = new Map<ID, string>();
        List<EP_Credit_Exception_Request__c> creditExcObjList = new List<sObject>();
        for(EP_CreditExceptionUpdateStub.CreditExceptionStatusType crdExcpRec : creditExceptionList){
            if(string.isNotEmpty(crdExcpRec.failureReason)) {
                createResponse(crdExcpRec.seqId,EP_Common_Constant.ERROR_SYNC_SENT_STATUS, crdExcpRec.failureReason);
            } else {
                creditExcObjList.add(crdExcpRec.creditExcpObj);
                credExId_SeqIdMap.put(crdExcpRec.creditExcpObj.Id,crdExcpRec.seqId);
            }
        }
        updateCreditException(creditExcObjList, credExId_SeqIdMap);
    }

    /**
    * @Author       Accenture
    * @Name         createResponse
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void createResponse(string seqId, string errorCode, string errorDescription) {
        EP_GeneralUtility.Log('Private',CLASS_NAME,'createResponse');
        if(string.isNotBlank(errorDescription)) processingFailed = true;
        ackResponseList.add(EP_AcknowledgementUtil.createDataSet(DATASET_NAME, seqId, errorCode, errorDescription));
    }
    
    
    /**
    * @Author       Accenture
    * @Name         updateCreditException
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void updateCreditException(List<sObject> sObjectsList, map<ID, string> credExId_SeqIdMap) {
        EP_GeneralUtility.Log('private',CLASS_NAME,'updateCreditException');
        if( !sObjectsList.isEmpty() ){
            list<Database.saveResult> saveResult = database.update(sObjectsList,true);
            for( integer index = 0 ; index < saveResult.size() ; index++ ) {
                if(saveResult[index].isSuccess()){
                    createResponse(credExId_SeqIdMap.get(saveResult[index].getId()),EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);
                }else {
                    processUpsertErrors(saveResult[index].getErrors(), credExId_SeqIdMap.get(saveResult[index].getId()));
                }
            }
        }
    }
    /**
    * @Author       Accenture
    * @Name         processUpsertErrors
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void processUpsertErrors(list<Database.Error> errorList,string seqId) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderUpdateHandler','processUpsertErrors');
        for(Database.Error err : errorList) {
            createResponse(seqId,String.valueOf(err.getStatusCode()), err.getMessage() ); 
        }
    }
}