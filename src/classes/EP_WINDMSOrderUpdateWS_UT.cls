@isTest
public class EP_WINDMSOrderUpdateWS_UT
{
	@testSetup static void init() {
        List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
    }
    
	static testMethod void updateOrder_test() {
		EP_WINDMSOrderUpdateStub stub = EP_TestDataUtility.createWINDMSOrderUpdateStub();
        string jsonRequest = JSON.serialize(stub);
        system.debug('**jsonRequest**' + jsonRequest);
        Test.startTest();
        RestRequest req = new RestRequest(); 
   		RestResponse res = new RestResponse();
   		req.requestURI = '/services/apexrest/v1/OrderUpdate';  //Request URL
		req.httpMethod = EP_Common_Constant.POST;//HTTP Request Type
		req.requestBody = Blob.valueof(jsonRequest);
		RestContext.request = req;
		RestContext.response= res;
   		EP_WINDMSOrderUpdateWS.processRequest();
   		string response = res.responseBody.toString();
   		Test.stopTest();
   		system.assertEquals(true, response !=null);
    }
}