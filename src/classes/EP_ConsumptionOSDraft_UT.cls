@isTest
public class EP_ConsumptionOSDraft_UT
{
    @testSetup static void init() {
        Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
    }
    static testMethod void getTextValue_test() {
        Test.startTest();
        String result = EP_ConsumptionOSDraft.getTextValue();
        Test.stopTest();
        System.AssertEquals(true,result==EP_OrderConstant.OrderState_DRAFT);
    }
    static testMethod void doOnEntry_test() {
        EP_ConsumptionOSDraft localObj = new EP_ConsumptionOSDraft();
        EP_OrderDomainObject obj = EP_TestDataUtility.getConsumptionOSDraftDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        csord__Order__c orderObj = localObj.order.getOrder();
        System.AssertNotEquals(Null,orderObj.EP_Actual_Delivery_Date_Time__c);
    }
    static testMethod void doOnExit_test() {
        EP_ConsumptionOSDraft localObj = new EP_ConsumptionOSDraft();
        EP_OrderDomainObject obj = EP_TestDataUtility.getConsumptionOSDraftDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        System.assert(True);
        //Dummy Assert, No operation performs on this method
    }    
      
    static testMethod void setOrderContext_test() {
        EP_ConsumptionOSDraft localObj = new EP_ConsumptionOSDraft();
        EP_OrderDomainObject obj = EP_TestDataUtility.getConsumptionOSDraftDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
        Test.startTest();
        localObj.setOrderContext(obj,oe);
        Test.stopTest();
        System.AssertEquals(true,localObj.orderEvent.getEventName() == 'UserSubmit');
    }
    static testMethod void setOrderDomainObject_test() {
        EP_ConsumptionOSDraft localObj = new EP_ConsumptionOSDraft();
        EP_OrderDomainObject obj = EP_TestDataUtility.getConsumptionOSDraftDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.setOrderDomainObject(obj);
        Test.stopTest();
        System.assert(True);
        //Dummy Assert, as this method delegates Super class's method.   
    }
}