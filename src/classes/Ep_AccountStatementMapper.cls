/*
   @Author          Accenture
   @Name            EP_AccountStatementItemMapper
   @CreateDate      03/09/2018
   @Description     This class contains all SOQLs related to Customer Account Statement object
   @Version         1.0
   @Reference       NA
*/
/***L4-45308 code changes start****/
public class Ep_AccountStatementMapper {
	/**
     * @date <03/09/2018>
     * @description This method is used to get Map<String, EP_Customer_Account_Statement__c>.
     * @param set<Id> setBillToIds
     * @return Map<String, EP_Customer_Account_Statement__c>
     */
     public  static Map<String, EP_Customer_Account_Statement__c> getCustAccstmtNrIdMap(set<Id> setBillToIds){
     	EP_GeneralUtility.Log('public','EP_CustomerAccountStatementUtility','getCustAccstmtNrIdMap');
     	Map<String, EP_Customer_Account_Statement__c> CustAccstmtNrIdMap = new Map<String, EP_Customer_Account_Statement__c>();
     	Integer nRows = EP_Common_Util.getQueryLimit();
        for(EP_Customer_Account_Statement__c obj : [SELECT Id, EP_Customer_Account_Statement_Key__c,EP_Bill_To__c FROM EP_Customer_Account_Statement__c  where EP_Bill_To__c IN : setBillToIds limit:nRows]) {
            if(!CustAccstmtNrIdMap.containskey(obj.EP_Customer_Account_Statement_Key__c)){
            	CustAccstmtNrIdMap.put(obj.EP_Customer_Account_Statement_Key__c, obj);
            }            
        }
     	return CustAccstmtNrIdMap;
     }  
}
/***L4-45308 code changes End****/