@isTest
/*
   Below is test class for coverage of "EP_ExceptionLog_BatchClass" 
   and "EP_ExceptionLog_BatchClass_Schedule" class
*/
private with sharing class EP_ExceptionLog_BatchClass_Schedule_Test{

    public static String CRON_EXP = '0 0 2 ? * SUN';
    
    /*
      Below is test method for coverage of "EP_ExceptionLog_BatchClass" 
      and "EP_ExceptionLog_BatchClass_Schedule" class
    */
    private static TestMethod void executeTest(){
        //List<sObject> ls = Test.loadData(EP_Exception_Log__c.sObjectType,'Exception_Log_Test_Data'); 
        List<sObject> ls = Test.loadData(EP_Exception_Log__c.sObjectType,'Exception_Log_Test_Data');
        test.StartTest();
        EP_ExceptionLog_BatchClass_Schedule  sch = new EP_ExceptionLog_BatchClass_Schedule();
        String jobId = system.schedule('Exception Log Deletion hourly test', CRON_EXP, sch);
        // Getting the information from the CronTrigger object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime  FROM CronTrigger WHERE id = :jobId];
        // Verify the expressions are the same
        System.assertEquals(CRON_EXP, ct.CronExpression);
        // Verify that job has not run
        System.assertEquals(0, ct.TimesTriggered);
        test.stopTest();
        
        // Verify that job has  run
        System.assertEquals(0, ct.TimesTriggered);
        
    }
    
   
}