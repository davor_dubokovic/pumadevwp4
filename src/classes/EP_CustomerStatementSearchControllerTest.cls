/* 
   @Author <Vinay Jaiswal>
   @name <EP_CustomerStatementSearchControllerTest>
   @CreateDate <07/06/2016>
   @Description <This test class is used for EP_CustomerStatementItemSearchController class>
   @Version <1.0>
 
*/
@isTest
private class EP_CustomerStatementSearchControllerTest {
    private static final String OBJ_ID = 'id';
    private static final String sCreditMemo = 'Credit Memo';
    private static final String sPaymentIn = 'Payment In';
    private static final String sPaymentOut = 'Payment Out';
    private static final String sCustomerInvoice = 'Customer Invoice';
    private static Profile cscAgent = [Select id from Profile
                                    Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE
                                    limit :EP_Common_Constant.ONE];
    private static User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
    private static Account billToAccount = EP_TestDataUtility.createBillToAccount();
    private static Id shpToRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,
                                                                           EP_Common_Constant.NON_VMI_SHIP_TO);
                                                                                                                           
    private static EP_Freight_Matrix__c freightMatrix;
    private static Id rectypeCustAccStmtId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMER_ACCOUNT_STATEMENT_OBJECT, EP_Common_Constant.CUSTOMER_ACCOUNT_STATEMENT_RECORD_TYPE_NAME);
    
    /* 
    ** CustomerStatementSearchControllerTest method
    */
    static testMethod void CustomerStatementSearchControllerTest() {
        // TO DO: implement unit test
        System.runAs(cscUser){
            createTestData();
            system.assert(billToAccount.id <> null);
            EP_Customer_Account_Statement_Item__c oCasiPreviousInvoice = EP_TestDataUtility.createCASItemForPreviousSearch(billToAccount.id, sCustomerInvoice);
            Database.insert(oCasiPreviousInvoice);
            EP_Customer_Account_Statement_Item__c oCasiPreviousPayment = EP_TestDataUtility.createCASItemForPreviousSearch(billToAccount.id, sPaymentIn);
            Database.insert(oCasiPreviousPayment);
            EP_Customer_Account_Statement_Item__c oCasiCurrentInvoice = EP_TestDataUtility.createCASItemForSearch(billToAccount.id, sCustomerInvoice);
            Database.insert(oCasiCurrentInvoice);
            EP_Customer_Account_Statement_Item__c oCasiCurrentPayment = EP_TestDataUtility.createCASItemForSearch(billToAccount.id, sPaymentIn);
            Database.insert(oCasiCurrentPayment);
            EP_Customer_Account_Statement__c oCasPreviousInvoice = EP_TestDataUtility.createCASForPreviousSearch(billToAccount.id);
            oCasPreviousInvoice.RecordTypeId = rectypeCustAccStmtId;
            Database.insert(oCasPreviousInvoice);
            EP_Customer_Account_Statement__c oCasCurrentInvoice = EP_TestDataUtility.createCASForSearch(billToAccount.id);
            oCasCurrentInvoice.RecordTypeId = rectypeCustAccStmtId;
            Database.insert(oCasCurrentInvoice);
            System.currentPagereference().getParameters().put(OBJ_ID, billToAccount.id);
            EP_CustomerStatementItemSearchController oStatementController = new EP_CustomerStatementItemSearchController();   
            oStatementController.datefrom = system.today().adddays(-10);
            oStatementController.dateTo = system.today();
            oStatementController.searchCustomerStatement();
        }
    }
    
    /* 
    ** CustomerStatementSearchControllerTest1 method 
    ** Automation Test : 19776, 19779
    */
    static testMethod void CustomerStatementSearchControllerTest1() {
         String sResult = 'The selected Date From cannot be after the selected Date To';
         System.runAs(cscUser){
             createTestData();
             System.currentPagereference().getParameters().put(OBJ_ID, billToAccount.id);
             EP_CustomerStatementItemSearchController oStatementController = new EP_CustomerStatementItemSearchController();   
             oStatementController.datefrom = system.today().adddays(10);
             oStatementController.dateTo = system.today();
             oStatementController.searchCustomerStatement();
             system.assert(ApexPages.hasMessages());
             system.assertEquals(ApexPages.getMessages().size(), 1);
             system.assert(ApexPages.getMessages().get(0).getDetail().contains(sResult));
         }
    }
    
     /* 
    ** CustomerStatementSearchControllerTest2 method
    ** Automation Test : 19778
    */
    static testMethod void CustomerStatementSearchControllerTest2() {
         String sResult = 'The selected date range cannot be longer than two months in total';
         System.runAs(cscUser){
             createTestData();
             System.currentPagereference().getParameters().put(OBJ_ID, billToAccount.id);
             EP_CustomerStatementItemSearchController oStatementController = new EP_CustomerStatementItemSearchController();   
             oStatementController.datefrom = system.today().adddays(-62);
             oStatementController.dateTo = system.today();
             oStatementController.searchCustomerStatement();
             system.assert(ApexPages.hasMessages());
             system.assertEquals(ApexPages.getMessages().size(), 1);
             system.assert(ApexPages.getMessages().get(0).getDetail().contains(sResult));
         }
    }
    
    /* 
    ** CustomerStatementSearchControllerTest3 method
    ** Automation Test : 19777
    */
    static testMethod void CustomerStatementSearchControllerTest3() {
        String sResult = 'The selected date range cannot be in the future';
         System.runAs(cscUser){
             createTestData();
             System.currentPagereference().getParameters().put(OBJ_ID, billToAccount.id);
             EP_CustomerStatementItemSearchController oStatementController = new EP_CustomerStatementItemSearchController();   
             oStatementController.datefrom = system.today().adddays(1);
             oStatementController.dateTo = system.today().adddays(2);
             oStatementController.searchCustomerStatement();
             system.assert(ApexPages.hasMessages());
             system.assertEquals(ApexPages.getMessages().size(), 1);
             system.assert(ApexPages.getMessages().get(0).getDetail().contains(sResult));
         }
    }
    
    /* 
    ** CustomerStatementSearchControllerTest4 method
    */
    static testMethod void CustomerStatementSearchControllerTest4() {
         String sResult = 'The selected date range cannot be in the future';
         System.runAs(cscUser){
             createTestData();
             System.currentPagereference().getParameters().put(OBJ_ID, billToAccount.id);
             EP_CustomerStatementItemSearchController oStatementController = new EP_CustomerStatementItemSearchController();   
             oStatementController.datefrom = system.today();
             oStatementController.dateTo = system.today().adddays(1);
             oStatementController.searchCustomerStatement();
             system.assert(ApexPages.hasMessages());
             system.assertEquals(ApexPages.getMessages().size(), 1);
             system.assert(ApexPages.getMessages().get(0).getDetail().contains(sResult));
         }
    }
    
    /* 
    ** CustomerStatementSearchControllerTest5 method
    ** Automation Test : 19928
    */
    static testMethod void CustomerStatementSearchControllerTest5() {
         String sResult = 'The system was not able to calculate the opening/closing balance of this customer as no '+
                                        'Customer Account Statements have been found in Salesforce. This could be because NAV has not '+ 
                                        'generated statements for this customer yet. If you do believe that there should be account statements '+ 
                                        'available for the selected customer, please contact your system administrator'; 
         
         System.runAs(cscUser){  
             createTestData(); 
             System.currentPagereference().getParameters().put(OBJ_ID, billToAccount.id);       
             EP_CustomerStatementItemSearchController oStatementController = new EP_CustomerStatementItemSearchController();   
             oStatementController.datefrom = system.today().addDays(-30);
             oStatementController.dateTo = system.today().adddays(-29);
             oStatementController.lstWrapperCustAccStatement = NULL;
             oStatementController.lstWrapperCustAccStatementItem = NULL;
             oStatementController.searchCustomerStatement();
         }
    }
    //L4# 78534 - Test Class Fix - Start 
    /* 
    ** CustomerStatementSearchControllerTest6 method
    */
   static testMethod void CustomerStatementSearchControllerTest6() {
        EP_CustomerStatementItemSearchController oStatementController = NULL;
         System.runAs(cscUser){  
             //createTestData(); 
             System.currentPagereference().getParameters().put(OBJ_ID, billToAccount.id);       
             oStatementController = new EP_CustomerStatementItemSearchController();   
             oStatementController.datefrom = NULL;
             oStatementController.dateTo = system.today().adddays(-29);
             oStatementController.searchCustomerStatement();
             system.assert(ApexPages.hasMessages());
             system.assertEquals(ApexPages.getMessages().size(), 1);
         }
    }
    //L4# 78534 - Test Class Fix - End 
    /* 
    ** CustomerAccountStatementUtility method
    */
    static testMethod void CustomerAccountStatementUtility() {
        Date datefrom = NULL;
        Date dateTo = NULL;
        EP_WrapperAccountStatement oWrapAccountStatement = NULL;
         System.runAs(cscUser){  
             createTestData();      
             oWrapAccountStatement = new EP_WrapperAccountStatement();  
             datefrom = system.today().addDays(-60);
             dateTo = system.today();
             EP_CustomerAccountStatementUtility.getAccountStatementItemList(billToAccount.id , datefrom, dateTo, 'invoice');
             oWrapAccountStatement = EP_CustomerAccountStatementUtility.getAccountStatementItemListEnhanced(billToAccount.id , datefrom, dateTo, 'invoice');
         }
    }
    
    /* 
    ** CustomerAccountStatementUtility1 method
    */
    static testMethod void CustomerAccountStatementUtility1() {
        EP_WrapperAccountStatement oWrapAccountStatement = NULL;
         System.runAs(cscUser){  
             createTestData();      
             oWrapAccountStatement = new EP_WrapperAccountStatement();  
             oWrapAccountStatement = EP_CustomerAccountStatementUtility.getAccountOverdueInvoices(billToAccount.Id);
         }
    }
    
    /* 
    ** CustomerAccountStatementUtility2 method
    */
    static testMethod void CustomerAccountStatementUtility2() {
        Date datefrom = NULL;
        Date dateTo = NULL;
        EP_WrapperAccountStatement oWrapAccountStatement = NULL;
        EP_WrapperAccountStatementItem oWrapAccountStatementItem = NULL;
         System.runAs(cscUser){  
             createTestData();      
             oWrapAccountStatement = new EP_WrapperAccountStatement();  
             oWrapAccountStatementItem = new EP_WrapperAccountStatementItem();  
             datefrom = system.today().addDays(-30);
             dateTo = system.today().adddays(-29);
             EP_CustomerAccountStatementUtility.totalDebitsAccountStatementItem(billToAccount.id , datefrom, dateTo, 'invoice');
         }
    }
    
    /* 
    ** CustomerAccountStatementUtility2 method
    */
    static testMethod void WrapperAccountStatementItemtest() {
        EP_WrapperAccountStatementItem oWrapAccountStatementItem = NULL;
        System.runAs(cscUser){  
             createTestData();      
             oWrapAccountStatementItem = new EP_WrapperAccountStatementItem(); 
             //oWrapAccountStatementItem.wCustAccStatementItemId = ''; 
             oWrapAccountStatementItem.wAccEntryDocNr = 'Test';
             oWrapAccountStatementItem.wAccEntryType = 'Test';
             oWrapAccountStatementItem.wAccEntryCurr = 'Test';
             oWrapAccountStatementItem.wAccEntryAmount = 0.0;
             oWrapAccountStatementItem.wAccEntryDueDate = 'Test';
             oWrapAccountStatementItem.wAccEntryIssueDate = 'Test';
             oWrapAccountStatementItem.wAccEntryDesc = 'Test';
             oWrapAccountStatementItem.wAccEntryDescFull = 'Test';
             oWrapAccountStatementItem.wClosingBalance = 0.0;
             oWrapAccountStatementItem.wAccRemainingBalance = 0.0;
             //oWrapAccountStatementItem.wAccRelatedOrders
             //oWrapAccountStatementItem.wAccBillTo 
             oWrapAccountStatementItem.wAccPDFLink = 'Test';
        }
    }

    /* 
    ** Method used for data cration
    */
    static void createTestData() {
        string billToAccountNr = '10000';
        string sellToAccountNr = '20000';
        string nonVmiShipToAccountNr = '30000';
        string Nav_Stock_Location = '41001';
        Product2  productObj = new product2(Name = EP_Common_Constant.Diesel, CurrencyIsoCode = EP_Common_Constant.GBP,
                                             isActive = TRUE, family=EP_Common_Constant.HYDROCARBON_LIQUID_PRODUCT_FAMILY,
                                             Eligible_for_Rebate__c = true, ProductCode='30005');
        Product2  taxProd= new product2(Name = EP_Common_Constant.TAX_PRODUCT_NAME, 
                                        CurrencyIsoCode = EP_Common_Constant.GBP, isActive = TRUE);                           
        List<Product2> prodList = new List<Product2>{productObj,taxProd};                   
        Database.insert(prodList);
        
        PricebookEntry prodPricebookEntry = [SELECT Id, Pricebook2Id FROM PricebookEntry WHERE Product2.Id=:productObj.Id LIMIT:EP_Common_Constant.ONE];
        Id pricebookId = prodPricebookEntry.Pricebook2Id;
        
        EP_Country__c contry1 = EP_TestDataUtility.createCountryRecord(EP_Common_Constant.India,
                                                                       EP_Common_Constant.India_Code,
                                                                       EP_Common_Constant.Asia);
        Database.insert(contry1);
        EP_Region__c reg ;
        reg = EP_TestDataUtility.createRegion('East',contry1.id );
        Database.insert(reg); 
        
        billToAccount.EP_Delivery_Pickup_Country__c = contry1.id;
        billToAccount.EP_Cntry_Region__c = reg.id;
        billToAccount.EP_Country__c = contry1.id;
        Database.insert(billToAccount);
        billToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(billToAccount);
        billToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        billToAccount.AccountNumber = billToAccountNr;
        Database.update(billToAccount); 
        System.assert(billToAccount != null );   
        // create sell to Account
        Account  sellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
        sellToAccount.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
        sellToAccount.EP_Bill_To_Account__c = billToAccount.id;
        sellToAccount.AccountNumber = sellToAccountNr;
        sellToAccount.EP_Delivery_Pickup_Country__c = contry1.id;
        sellToAccount.EP_Cntry_Region__c = reg.id;
        sellToAccount.EP_Country__c = contry1.id;
        sellToAccount.EP_AvailableFunds__c = 40000000;
        sellToAccount.EP_PriceBook__c = pricebookId;
        sellToAccount.EP_Alternative_Payment_Method__c = 'Application in Currency';
        sellToAccount.EP_Recommended_Credit_Limit__c = 50000;
        Database.insert(sellToAccount,true);
        ID sellToSHLRTID= EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c', 'Sell-To Supply Location');
        
        EP_Stock_holding_location__c SHL1 = EP_TestDataUtility.createSellToStockLocation(sellToAccount.id,true,null,sellToSHLRTID); //Add one SHL
        Database.insert(SHL1,false);
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Sell TO status
        Database.update(sellToAccount);
        freightMatrix = EP_TestDataUtility.createFreightMatrix();  
        Database.insert(freightMatrix);
          // create non vmi ship to account
        Account nonVmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, shpToRecordTypeId );
        nonVmiShipToAccount.EP_Freight_Matrix__c = freightMatrix.id;
        nonVmiShipToAccount.EP_Delivery_Type__c =  EP_Common_Constant.DELIVERY;
        nonVmiShipToAccount.AccountNumber = nonVmiShipToAccountNr;
        nonVmiShipToAccount.EP_PriceBook__c = pricebookId;
        nonVmiShipToAccount.EP_Ship_To_Type__c = EP_Common_Constant.NON_CONSIGNMENT;
        nonVmiShipToAccount.EP_Transportation_Management__c = EP_Common_Constant.TRANSPORT_OWN;
        Database.insert(nonVmiShipToAccount,false);
        
        Account storageLocAccPrimary = EP_TestDataUtility.createStockHoldingLocation();
        storageLocAccPrimary.EP_Nav_Stock_Location_Id__c = Nav_Stock_Location;
        Database.insert(storageLocAccPrimary);
        
        
        //EP_Stock_Holding_Location__c primaryStockHolding = EP_TestDataUtility.createSellToStockLocation(sellToAccount.id,true,null,sellToSHLRTID); //Add one SHL
        //Database.insert(primaryStockHolding);
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(sellToAccount);

        List<EP_Tank__c> tanksToBeInsertedList = new List<EP_Tank__c>();
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(nonVmiShipToAccount.Id,productObj.Id);
        tanksToBeInsertedList.add(tankObj);        
        Database.insert(tanksToBeInsertedList);
        
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(nonVmiShipToAccount);

        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(nonVmiShipToAccount);
        
    }
}