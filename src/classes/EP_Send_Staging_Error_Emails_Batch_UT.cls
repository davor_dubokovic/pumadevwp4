/*
    @Author          Accenture
    @Name            EP_Send_Staging_Error_Emails_Batch_UT
    @CreateDate      18/04/2018
    @Description     This is a Unit test class for EP_Send_Staging_Error_Emails_Batch.
    @Version         1.0
    @Reference       NA
*/

@isTest
public class EP_Send_Staging_Error_Emails_Batch_UT{
        
    static testMethod void batchtest()
    {    
         string rcrdType = [select id from recordtype where developername = 'EP_Retail_Order'].id;
         EP_File__c obj = new EP_File__c();
         obj.Name = 'Sample';
         obj.EP_CheckSum_Key__c = '3344';
         obj.EP_EmailSent__c = false;
         insert obj;
         EP_RO_Import_Staging__c childObj = new EP_RO_Import_Staging__c();
         childObj.recordtypeid = rcrdType;
         childObj.EP_LineNr__c = 'Line1';
         childObj.EP_CompanyCode__c = 'EPA';
         childObj.EP_SenderCode__c = 'testcode';
         childObj.EP_Unit_Price__c = 9000;
         childObj.EP_Product_Description__c = 'desc test';
         childObj.EP_CreatedDt__c = system.today();
         childObj.EP_CreatedBy__c = 'skp';
         childObj.EP_Remark_Reason__c = 'sample reason';
         childObj.EP_Product_Code__c = 'sample productcode';
         childObj.EP_Sell_To_Number__c = '5555';
         childObj.EP_Ordered_quantity__c = 20;
         childObj.EP_File_Name__c = obj.id;
         childObj.EP_Status__c = 'Error';
         insert childObj;
         
                  
         List <EP_File__c> lstFileRecord = new List <EP_File__c>{obj};
         
         
         
         EP_Send_Staging_Error_Emails_Batch localObj = new EP_Send_Staging_Error_Emails_Batch();
         Test.startTest();
         database.executebatch(localObj);
         Test.stopTest();
    
    }
}