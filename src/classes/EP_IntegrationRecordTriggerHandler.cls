/* 
  @Author <Jyotsna Yadav>
  @name <EP_IntegrationRecordTriggerHandler>
  @CreateDate <28/10/2015>
  @Description <Handler class for Integration Record Trigger>
  @Version <1.0>
 
*/
public without sharing class EP_IntegrationRecordTriggerHandler {
    
    private static final string CLASS_NAME = 'EP_IntegrationRecordTriggerHandler';
    private static final string doBeforeInsert = 'doBeforeInsert';
    private static final string doBeforeUpdate = 'doBeforeUpdate';
    private static final string doAfterInsert = 'doAfterInsert';
    private static final string doAfterUpdate = 'doAfterUpdate';
    
    /*
    This method handles before insert requests from Integration Record trigger
    */
    public static void doBeforeInsert(List<EP_IntegrationRecord__c> newIntegrationRecords){
        EP_IntegrationRecordTriggerHelper.updateIntegrationErrorType(newIntegrationRecords);
    }
    /*
    This method handles before update requests from Integration Record trigger
    */
    public static void doBeforeUpdate(List<EP_IntegrationRecord__c> newIntegrationRecords){
        EP_IntegrationRecordTriggerHelper.updateIntegrationErrorType(newIntegrationRecords);
    }
    /*
        This method handles after insert requests from Integration Record trigger
    */  
    public static void  doAfterInsert(List<EP_IntegrationRecord__c> newIntegrationRecords){
        List<EP_IntegrationRecord__c> intRecListForUpdate = new List<EP_IntegrationRecord__c>();
        
        try{
            for( EP_IntegrationRecord__c intRec :  newIntegrationRecords){
                if( EP_Common_Constant.SENT_STATUS.equalsIgnoreCase(intRec.EP_Status__c) || EP_Common_Constant.ERROR_SENT_STATUS.equalsIgnoreCase(intRec.EP_Status__c)){
                    intRecListForUpdate.add( intRec );
                }
            }
            if( !intRecListForUpdate.isEmpty() ){
                EP_IntegrationRecordTriggerHelper.updateSentStatus(intRecListForUpdate);
            }
        }catch(Exception e){
            EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, doAfterInsert, CLASS_NAME,apexPages.severity.ERROR);
        }   
    } 
    /*
        This method handles after update requests from Integration Record trigger
    */
    public static void doAfterUpdate(List<EP_IntegrationRecord__c> oldIntegrationRecords, 
                                        List<EP_IntegrationRecord__c> newIntegrationRecords,
                                            Map<Id,EP_IntegrationRecord__c> oldIntegrationRecordsMap,
                                                Map<Id,EP_IntegrationRecord__c> newIntegrationRecordsMap) {
        Map<Id,EP_IntegrationRecord__c> recordToUpdateMap = new Map<Id,EP_IntegrationRecord__c>();
        list<EP_IntegrationRecord__c> recordToUpdateResend = new list<EP_IntegrationRecord__c>();
        //collectect for unique Message Ids
        set<String> uniqueMessageIds = new set<String>();
        //Start Changes for #59186 
        List<EP_IntegrationRecord__c> intRecListForUpdate = new List<EP_IntegrationRecord__c>();
        //End Changes for #59186
        boolean hasErrorSent = false;
        try{
        //Iterating on the updated records to send the record for processing whose status has been changed.
            for( EP_IntegrationRecord__c intRec : newIntegrationRecordsMap.values() ){
                /**DEFECT - 30074,INTEGRATIONRECORDCREATION START**/
                hasErrorSent = false;
                string oldStatus = oldIntegrationRecordsMap.get(intRec.Id).EP_Status__c;
                string newStatus = intRec.EP_Status__c;
                if( EP_Common_Constant.ERROR_SENT_STATUS.equalsIgnoreCase(newStatus)
                     && intRec.EP_Resend__c
                     && intRec.EP_IsLatest__c
                     && EP_IntegrationUtil.isManualRetry  )/**DEFECT - 30074,INTEGRATIONRECORDCREATION END**/
                {
                    uniqueMessageIds.add( intRec.EP_Message_ID__c);
                    recordToUpdateResend.add( setResendFalse(intRec.id) );
                    
                }
                //Start Changes for #59186
                //Added this Condition if Transport Status has error then update the Object Records(Order/Account) integration Status as 'Error Sent' Status, Currenty these reocords were updating as Error-Sync and Failure on Account
                if( oldStatus != newStatus &&  EP_Common_Constant.ERROR_SENT_STATUS.equalsIgnoreCase(newStatus)) {
                    intRecListForUpdate.add( intRec );
                    hasErrorSent = true;
                } 
                
                //End Changes for #59186 
                if(oldStatus != newStatus && !hasErrorSent){      
                    if( 
                        EP_Common_Constant.SENT_STATUS.equalsIgnoreCase( oldStatus ) || 
                        EP_Common_Constant.ACKNWLDGD.equalsIgnoreCase( oldStatus ) ||
                        EP_Common_Constant.ERR_ACKNWLDGD.equalsIgnoreCase( oldStatus ) || 
                        EP_Common_Constant.ERROR_SENT_STATUS.equalsIgnoreCase( oldStatus ) || 
                        ( 
                            (EP_Common_Constant.ACKNWLDGD.equalsIgnoreCase( newStatus ) || EP_Common_Constant.ERR_ACKNWLDGD.equalsIgnoreCase( newStatus ) ) &&
                            ( !EP_Common_Constant.SYNC_STATUS.equalsIgnoreCase( oldStatus ) && ! EP_Common_Constant.ERROR_SYNC_STATUS.equalsIgnoreCase( oldStatus ) )
                        )
                    )
                    {
                        system.debug( '===');
                        if( intRec.EP_IsLatest__c && 
                            ( 
                                EP_Common_Constant.SYNC_STATUS.equalsIgnoreCase( newStatus ) ||
                                 EP_Common_Constant.ERROR_SYNC_STATUS.equalsIgnoreCase( newStatus ) ||
                                (    /**OBSERVATION ERROR SENT STATUS UPDATE START**/
                                    (EP_Common_Constant.ERROR_SENT_STATUS.equalsIgnoreCase( newStatus ) || EP_Common_Constant.ACKNWLDGD.equalsIgnoreCase( newStatus ) || EP_Common_Constant.ERR_ACKNWLDGD .equalsIgnoreCase( newStatus ) ) && 
                                    /**OBSERVATION ERROR SENT STATUS UPDATE END**/
                                    ( !EP_Common_Constant.SYNC_STATUS.equalsIgnoreCase( oldStatus ) && !EP_Common_Constant.ERROR_SYNC_STATUS.equalsIgnoreCase( oldStatus ) )
                                )   
                            )
                        )
                        {
                            system.debug('**intRec**'+ intRec);
                            recordToUpdateMap.put(intRec.Id,intRec);  
                        }
                    }
                    /*
                    else if( oldIntegrationRecordsMap.get(intRec.Id).EP_Status__c.equalsIgnoreCase(EP_Common_Constant.ERROR_SENT_STATUS) && intRec.EP_Status__c.equalsIgnoreCase(EP_Common_Constant.SENT_STATUS))
                    {}
                    else
                    {
                        //Throw exception
                        //intRec.addError(Label.EP_IntRec_Sent_Status_Update_Error);
                        //EP_loggingService.loghandledException(ex,'app name','send ok','integrationutil',apexPages.severity.ERROR);
                    }*/
                }
            }
            if( !recordToUpdateMap.isEmpty() ){
                EP_IntegrationRecordTriggerHelper.updateSyncStatus(recordToUpdateMap);
                // MC 04/05/2018 Creation of Action records for failour (WP4)
                EP_IntegrationRecordTriggerHelper.createActionRecords(recordToUpdateMap);
            }
            
            if( !recordToUpdateResend.isEmpty() ){
                EP_IntegrationRecordTriggerHelper.resendMessage(recordToUpdateResend, uniqueMessageIds); 
            }
            //Start Changes for #59186
            if( !intRecListForUpdate.isEmpty() ){
                EP_IntegrationRecordTriggerHelper.updateSentStatus(intRecListForUpdate);
            }
            //End Changes for #59186
        }catch(Exception e){
            EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, doAfterUpdate, CLASS_NAME,apexPages.severity.ERROR);
        } 
  
    } 
    
    /*
    CREATE INTEGRATION RECORD WITH RESEND STATUS FALSE
   */ 
    private static EP_IntegrationRecord__c setResendFalse(Id integrationRecId){
        return new EP_IntegrationRecord__c(EP_Resend__c = FALSE, Id = integrationRecId);
    }
    
}