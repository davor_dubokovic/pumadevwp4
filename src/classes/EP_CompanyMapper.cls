/**
   @Author          CR Team
   @name            EP_CompanyMapper
   @CreateDate      12/21/2016
   @Description     This class contains all SOQLs related to Company__c Object
   @Version         1.0
   @reference       NA
   */
/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    23/03/2018          CS                      Added new field (isReprice__c) to the queries
    *************************************************************************************************************
    */
    public with sharing class EP_CompanyMapper {

    /** This method returns Company Records
    *  @name            getRecordsByIds
    *  @param           set<Id>, Integer 
    *  @return          list<Company__c>
    *  @throws          NA
    */
    public list<Company__c> getRecordsByIds(set<id> idSet) {
        EP_GeneralUtility.Log('Public','EP_CompanyMapper','getRecordsByIds');
        list<Company__c> companyObjList = new list<Company__c>();
        for(list<Company__c> companyListTeam : [SELECT Id, EP_Company_Code__c,EP_Reminder_Notification_Interval_in_hrs__c,EP_Tank_Dips_Submission_Time__c, isReprice__c
            FROM Company__c 
            WHERE Id IN: idSet 
            //LIMIT :limitVal
            ]){
            companyObjList.addAll(companyListTeam);
        }                   
        return companyObjList ;
    }
    
    /** This method returns Company Records
    *  @name            getCompanyDetailsByCompanyCode
    *  @param           String 
    *  @return          Company__c
    *  @throws          NA
    */
    public Company__c getCompanyDetailsByCompanyCode(String compCode) {
        EP_GeneralUtility.Log('Public','EP_CompanyMapper','getCompanyDetailsByCompanyCode');
        Company__c company;
        List<Company__c> companyList = [SELECT Name,EP_Company_Street__c, EP_Company_State__c,EP_Company_Code__c,EP_Company_City__c, EP_Company_Country__c, EP_Company_Country__r.Name, 
        EP_Company_Post_Code__c,EP_ABN__c, EP_Phone__c, EP_Fax__c, EP_Email__c, isReprice__c FROM Company__c 
        WHERE EP_Company_Code__c=:compCode LIMIT:EP_Common_Constant.ONE];
        if(companyList.size() > 0) {
            company = companyList.get(0);
        }
        return company;
    }
    //#L4 45526 start
    /** This method returns Company Records
    *  @name            getCompanyDetailsByCompanyCodes
    *  @param           set<string> 
    *  @return          map<string,Company__c>
    *  @throws          NA
    */
    public map<string,Company__c> getCompanyDetailsByCompanyCodes(set<string> setCompCode) {
        EP_GeneralUtility.Log('Public','EP_CompanyMapper','getCompanyDetailsByCompanyCodes');
        map<string,Company__c> mapCompanyCodeCompany = new map<string,Company__c>();
        if(!setCompCode.isEmpty()){
	        for(Company__c objCompany:[SELECT Name,EP_Company_Street__c, EP_Company_State__c,EP_Company_Code__c,EP_Company_City__c, EP_Company_Country__c, EP_Company_Country__r.Name, 
	        							EP_Company_Post_Code__c,EP_ABN__c, EP_Phone__c, EP_Fax__c, EP_Email__c, isReprice__c FROM Company__c 
	        							WHERE EP_Company_Code__c=:setCompCode ]){
	        	mapCompanyCodeCompany.put(objCompany.EP_Company_Code__c,objCompany);
			}
        }
        return mapCompanyCodeCompany;
    }
    //#L4 45526 END
    
    /** This method returns Company Records
    *  @name            getExistingCompany
    *  @param           NA 
    *  @return          Company__c
    *  @throws          NA
    */
    public Company__c getExistingCompany() {
        EP_GeneralUtility.Log('Public','EP_CompanyMapper','getExistingCompany');
        return [Select Id, Name from Company__c limit 1];
    }

        /**BugFix-71967**/
       /**
       * @author                  Accenture
       * @name                           getCompanyRecordById
       * @date                    01/05/2018
       * @description             To get company record
       * @param                   id
       * @return                  Company__c
       */
       public static Company__c getCompanyRecordById(string CompanyId){
              EP_GeneralUtility.Log('Public','EP_CompanyMapper','getCompanyRecordById');
        Company__c company;
        List<Company__c> companyList = [SELECT Name,EP_Company_Street__c,EP_Company_Code__c, EP_Company_State__c,EP_Company_City__c, EP_Company_Country__c, EP_Company_Country__r.Name, 
        EP_Company_Post_Code__c,EP_ABN__c, EP_Phone__c, EP_Fax__c, EP_Email__c,EP_Disallow_DD_for_PP_customers__c FROM Company__c 
        WHERE Id =:CompanyId LIMIT:EP_Common_Constant.ONE];
        if(companyList.size() > 0) {
            company = companyList.get(0);
        }        
        return company;
       }      
       /**BugFix-71967**/
}