/* 
@Author      Accenture
@name        EP_SellToAsDeactivate
@CreateDate  11/29/2017
@Description Deactivate State Class for SellTo - #45361
@NovaSuite Fix -- comments added
@Version     1.0
*/
public class EP_SellToAsDeactivate extends EP_AccountState{
  /***NOvasuite fix constructor removed**/ 
  /* 
    @Author      Accenture
    @name        doOnEntry
    */
  public override void doOnEntry() {
    EP_GeneralUtility.Log('Public','EP_SellToAsDeactivate','doOnEntry');
   	if(this.account.isStatuschanged()){
        EP_AccountService accService = new EP_AccountService(this.account);
        accService.doActionSyncCustomerToPricingEngine();
        accService.doActionSendCreateRequestToWinDMS();
    }
  }
}