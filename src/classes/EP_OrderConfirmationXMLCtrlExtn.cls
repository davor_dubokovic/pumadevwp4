/**
  @Author : Accenture
  @Name : EP_OrderConfirmationXMLCtrlExtn
  @CreateDate : 02/15/2017
  @Description : This Controller Extension is used to generate XML message to send Order Confirmation to OPMS
  @Version 1.0
*/
public with sharing class EP_OrderConfirmationXMLCtrlExtn {
    @testvisible private map<id, Account> relatedAccountsMap = new map<id, Account>();
    private csord__Order__c ord;
    private string messageType;
    private string secretCode;
    
    public string messageId{get;set;}
    public string docTitle {get;set;}
    public Boolean isPriceDisplayed{get;set;}
    public csord__Order__c orderDetails{get;set;}
    public String documentType{get;set;}
    public Company__c companyDetails;
    public Account storageLocAcct;
    public Account shipToAcct;
    public Account transporterAcct;
    public list<csord__Order_Line_Item__c> orderLineItems;
    public String paymentType {get; set;}
    public String docPurpose {get; set;} 

   /**
    * @Author       Accenture
    * @Name         EP_OrderConfirmationXMLCtrlExtn
    * @Date         02/08/2017
    * @Description  The extension constructor initializes the members variable orderObject by using the getRecord method from the standard controller. This method will be use setup the values for Header in XML message
    * @Param        ApexPages.StandardController
    * @return       
    */
    public EP_OrderConfirmationXMLCtrlExtn (ApexPages.StandardController stdController) {
        secretCode = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_SECRETCODE);
        Id orderId = stdController.getRecord().Id;
        this.ord = [SELECT Id, csord__Identification__c, EP_Puma_Company_Code__c, EP_Document_Title__c, OrderNumber__c, pricingResponseJson__c  FROM csord__Order__c WHERE Id = :orderId];
        this.messageType = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGE_TYPE);
        this.messageId =  ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGEID);
        docTitle = EP_Common_Constant.BLANK;
        setOrderDetails();
        setRelatedAccountDetails();
        setOrderPaymentType();
    }
    
    /**
    * @Author       Accenture
    * @Name         getCompanyDetails
    * @Date         02/08/2017
    * @Description  This method is used to Company details for Order's Company
    * @Param        Sobject
    * @return       
    */
    public Company__c getCompanyDetails() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getCompanyDetails');
        EP_CompanyMapper mapperObj = new EP_CompanyMapper();
        companyDetails = mapperObj.getCompanyDetailsByCompanyCode(this.ord.EP_Puma_Company_Code__c);
        return companyDetails;
    }
    
   /**
    * @Author       Accenture
    * @Name         setOrderDetails
    * @Date         02/08/2017
    * @Description  This method is used set Order details
    * @Param        
    * @return       
    */
    @TestVisible
    private void setOrderDetails() {
        EP_GeneralUtility.Log('Private','EP_OrderConfirmationXMLCtrlExtn','setOrderDetails');
        this.orderDetails = new csord__Order__c();
        EP_OrderMapper mapperObj = new EP_OrderMapper();
        List<csord__Order__c> ordList = new List<csord__Order__c>();
        ordList = mapperObj.getCSRecordsByIds(new Set<Id>{this.ord.Id});
        if(!ordList.isEmpty()){
            this.orderDetails = ordList[0];
            isPriceDisplayed =  this.orderDetails.EP_Sell_To__r.EP_Display_Price__c;
            setDocTitle();
            setDocPurpose(); 
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         setRelatedAccountDetails
    * @Date         02/08/2017
    * @Description  This method is used to set all Accounts detail which are associated with Order
    * @Param :
    * @return       
    */
    @TestVisible
     private void setRelatedAccountDetails() {
        EP_GeneralUtility.Log('Private','EP_OrderConfirmationXMLCtrlExtn','setRelatedAccountDetails');
        set<id> accountIdSet = new set<id>();
        relatedAccountsMap = new map<id, Account>();
        
        // MC CS: 18/04/2018 Issue number 85329- adding Pickup_Supply_Location__r.Stock_Holding_Location__c for ex-rack orders
        if(this.orderDetails.Stock_Holding_Location__c == null){
            accountIdSet.add(this.orderDetails.Pickup_Supply_Location__r.Stock_Holding_Location__c);
        } else{
            accountIdSet.add(this.orderDetails.Stock_Holding_Location__r.Stock_Holding_Location__c);
        } 
        accountIdSet.add(this.orderDetails.EP_ShipTo__c);
        accountIdSet.add(this.orderDetails.EP_Transporter__c);
        accountIdSet.add(this.orderDetails.csord__Account__c);
        
        EP_AccountMapper mapperObj = new EP_AccountMapper();
        for(Account accObj : mapperObj.getRecordsByIds(accountIdSet)) {
            relatedAccountsMap.put(accObj.id, accObj);
        }
    }

    /**
    * @Author       Cloudsense
    * @Name         setOrderPaymentType
    * @Date         28/04/2018
    * @Description  This method is used to set PaymentType detail for Contract
    * @Param :
    * @return       
    */
    @TestVisible
    private void setOrderPaymentType(){
        
        String paymentType = orderDetails.EP_Payment_Method_Name__c;
        String paymentMethod;

        if (String.isBlank(orderDetails.pricingResponseJson__c) == false && orderDetails.RecordType.DeveloperName == 'Contract') {
            String pricingJson = orderDetails.pricingResponseJson__c;
            List<Account> accList = [SELECT id, EP_Payment_Method__r.EP_Payment_Method_Code__c, 
                                            EP_Payment_Term_Lookup__r.EP_Payment_Term_Code__c from Account where id = :orderDetails.csord__Account__c];
            paymentMethod = accList[0].EP_Payment_Method__r.EP_Payment_Method_Code__c;
            if (accList[0].EP_Payment_Term_Lookup__r.EP_Payment_Term_Code__c.equals(EP_Common_Constant.COD) ||
                accList[0].EP_Payment_Term_Lookup__r.EP_Payment_Term_Code__c.equals(EP_Common_Constant.CHKOD)) {
                paymentType = EP_Common_Constant.COD;
            } else if(accList[0].EP_Payment_Term_Lookup__r.EP_Payment_Term_Code__c.equals(EP_Common_Constant.PREPAYMENT_CODE)) {
                paymentType = EP_Common_Constant.Pre_Payment;
            } else {
                paymentType = EP_Common_Constant.Credit;
            }
        } else {
            paymentType = '';
        }
    }

    /**
    * @Author       Cloudsense
    * @Name         setDocPurpose
    * @Date         28/04/2018
    * @Description  This method is used to set PaymentType detail
    * @Param :
    * @return       
    */
    @TestVisible
    private void setDocPurpose(){
        
        if (orderDetails.RecordType.DeveloperName == 'Order' && orderDetails.EP_Document_Title__c == EP_Common_Constant.SALESQUOTES_TITLE){
            docPurpose = EP_Common_Constant.OPMS_ORDER_QUOTE;
        } else if (orderDetails.RecordType.DeveloperName == 'Contract' && orderDetails.EP_Document_Title__c == EP_Common_Constant.EP_OPEN_ORDER_CONTRACT_QUOTATION){
            docPurpose = EP_Common_Constant.OPMS_CONTRACT_QUOTE;
        } else if (orderDetails.RecordType.DeveloperName == 'Order' && orderDetails.EP_Document_Title__c == EP_Common_Constant.PROFORMAINVOICE_TITLE){
            docPurpose = EP_Common_Constant.OPMS_ORDER_PROFRMA;
        } else if (orderDetails.RecordType.DeveloperName == 'Contract' && orderDetails.EP_Document_Title__c == EP_Common_Constant.PROFORMAINVOICE_TITLE){
            docPurpose = EP_Common_Constant.OPMS_CONTRACT_PROFRMA;
        } else if (orderDetails.RecordType.DeveloperName == 'Order' && orderDetails.EP_Document_Title__c == EP_Common_Constant.ORDERCONFIRM_TITLE){
            docPurpose = EP_Common_Constant.OPMS_ORDER_CONFRMATION;
        } else if (orderDetails.RecordType.DeveloperName == 'Contract' && orderDetails.EP_Document_Title__c == EP_Common_Constant.EP_OPEN_ORDER_CONTRACT_CONFIRMATION){
            docPurpose = EP_Common_Constant.OPMS_CONTRACT_CONFRMATION;
        }
        
    }
    
    /**
    * @Author       Accenture
    * @Name         getStorageLocAcct
    * @Date         02/08/2017
    * @Description  This method is used to get Storage Location details from Account
    * @Param :
    * @return        Account
    */
     public Account getStorageLocAcct() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getStorageLocAcct');
        Account storageLocAcct = new Account();
        if(this.orderDetails.Stock_Holding_Location__c != null && relatedAccountsMap.containsKey(this.orderDetails.Stock_Holding_Location__r.Stock_Holding_Location__c)) {
            storageLocAcct = relatedAccountsMap.get(this.orderDetails.Stock_Holding_Location__r.Stock_Holding_Location__c);
        // MC CS: 18/04/2018 Issue number 85329- adding Pickup_Supply_Location__r.Stock_Holding_Location__c for ex-rack orders
        } else if (this.orderDetails.Pickup_Supply_Location__c != null && relatedAccountsMap.containsKey(this.orderDetails.Pickup_Supply_Location__r.Stock_Holding_Location__c)){
            storageLocAcct = relatedAccountsMap.get(this.orderDetails.Pickup_Supply_Location__r.Stock_Holding_Location__c);
        }
        return storageLocAcct;
    }

    /**
    * @Author       Accenture
    * @Name         getShipToAcct
    * @Date         02/08/2017
    * @Description  This method is used get ShipTo details from the Order
    * @Param :
    * @return        Account
    */
     public Account getShipToAcct() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getShipToAcct');
        Account shipToAcct = new Account();
        if(this.orderDetails.Ep_ShipTo__c == null && relatedAccountsMap.containsKey(this.orderDetails.EP_Pickup_Location__c)){
            shipToAcct = relatedAccountsMap.get(this.orderDetails.EP_ShipTo__c);
        } else {
            if(relatedAccountsMap.containsKey(this.orderDetails.EP_ShipTo__c)) {
            shipToAcct = relatedAccountsMap.get(this.orderDetails.EP_ShipTo__c);
            }
        }
        return shipToAcct;
    }
    
    /**
    * @Author       Accenture
    * @Name         getTransporterAcct
    * @Date         02/08/2017
    * @Description  This method is used to get Transporter details for Order
    * @Param :
    * @return        Account
    */
    public Account getTransporterAcct() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getTransporterAcct');
        Account transporterAcct = new Account();
        if(relatedAccountsMap.containsKey(this.orderDetails.EP_Transporter__c)) {
            transporterAcct = relatedAccountsMap.get(this.orderDetails.EP_Transporter__c);
        }
        return transporterAcct;
    }
     
   /**
    * @Author       Accenture
    * @Name         getOrderLineItems
    * @Date         02/08/2017
    * @Description  This method is used to get Order Items details for Order
    * @Param :
    * @return        list<OrderItem>
    */
    public List<csord__Order_Line_Item__c> getOrderLineItems() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getOrderLineItems');
        this.orderLineItems = new List<csord__Order_Line_Item__c>();
        EP_OrderItemMapper ordItemMapper = new EP_OrderItemMapper();
        //Repeat this for every Child node

        String pricingJson = '';

        list<Attachment> lineItems = [SELECT Id, Name,body, ParentId, Parent.Type FROM Attachment where ParentId =: this.ord.csord__Identification__c];
        System.debug('getOrderLineItems lineItems = ' + lineItems);

        if (String.isNotBlank(this.ord.pricingResponseJson__c)) {
            pricingJson = this.ord.pricingResponseJson__c;
        } else if (lineItems.size() >0 && String.isNotBlank(lineItems.get(0).body.toString())) {
            pricingJson = lineItems.get(0).body.toString();
        }

        /*List<cscfga__Product_Configuration__c> lineItems = [SELECT pricingResponseJson__c 
                                                                FROM cscfga__Product_Configuration__c 
                                                                WHERE cscfga__Product_Basket__c = :this.ord.csord__Identification__c 
                                                                AND name = :EP_OrderConstant.PUMAORDERLINEDEFNAME LIMIT 1];

        
        EP_PricingResponseStub stub = (EP_PricingResponseStub) System.JSON.deserialize(lineItems.get(0).pricingResponseJson__c , EP_PricingResponseStub.class);*/
        if(pricingJson != ''){
            EP_PricingResponseStub stub = (EP_PricingResponseStub) System.JSON.deserialize(pricingJson , EP_PricingResponseStub.class);

            for(csord__Order_Line_Item__c orderLineitem : ordItemMapper.getCSOrderById(new set<Id>{this.ord.Id})) {
                for (EP_PricingResponseStub.LineItem li : stub.Payload.any0.pricingResponse.priceDetails.priceLineItems.lineItem) {
                    List<String> quantityList = li.lineItemInfo.quantity.split(',');
                        String receivedQuantity = '';
                        for (Integer i = 0; i < quantityList.size(); i++) {
                            receivedQuantity = receivedQuantity + quantityList[i];
                        }
                        System.debug('getOrderLineItems receivedQuantity = ' + receivedQuantity);
                        System.debug('getOrderLineItems orderLineitem.Quantity__c = ' + orderLineitem.Quantity__c);
                        System.debug('getOrderLineItems orderLineitem.EP_Product_Code__c = ' + orderLineitem.EP_Product_Code__c);
                        System.debug('getOrderLineItems li.lineItemInfo.itemId = ' + li.lineItemInfo.itemId);
                        
                    if (li.lineItemInfo.itemId.equals(String.valueOf(orderLineitem.EP_Product_Code__c)) && (orderLineitem.Quantity__c == Decimal.valueOf(receivedQuantity))) {
                        //receivedQuantity.equals(String.valueOf(orderLineitem.Quantity__c))
                        System.debug('getOrderLineItems entered IF statement');
                        System.debug('getOrderLineItems Decimal.valueOf(li.lineItemInfo.invoiceDetails.invoiceComponent[0].taxPercentage) = ' + Decimal.valueOf(li.lineItemInfo.invoiceDetails.invoiceComponent[0].taxPercentage));
                        System.debug('getOrderLineItems li.lineItemInfo.invoiceDetails.invoiceComponent[0].amount = ' + li.lineItemInfo.invoiceDetails.invoiceComponent[0].amount);
                            orderLineitem.EP_Tax_Percentage__c = Decimal.valueOf(li.lineItemInfo.invoiceDetails.invoiceComponent[0].taxPercentage);
                            orderLineitem.csord__Line_Description__c = li.lineItemInfo.invoiceDetails.invoiceComponent[0].amount;
                            orderLineitem.UnitPrice__c = orderLineItem.ListPrice__c + orderLineItem.EP_VAT__c;
                        }
                    System.debug('getOrderLineItems orderLineitem.EP_Tax_Percentage__c = ' + orderLineitem.EP_Tax_Percentage__c);
                    System.debug('getOrderLineItems orderLineitem.csord__Line_Description__c = ' + orderLineitem.csord__Line_Description__c);
                    System.debug('getOrderLineItems orderLineitem.UnitPrice__c = ' + orderLineitem.UnitPrice__c);
                //li.lineItemInfo.invoiceDetails.invoiceComponent[0].taxAmount;
                }
                orderLineItems.add(orderLineitem);
            }
        }

        return orderLineItems;
    }

    /**
    * @Author       Accenture
    * @Name         getSellToAcct
    * @Date         02/08/2017
    * @Description  This method is used to get SellTo details for Order
    * @Param :
    * @return        Account
    */
    public Account getSellToAcct() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getSellToAcct');
        Account sellToAcct = new Account();
        system.debug('pep sell to' + sellToAcct);
        if(relatedAccountsMap.containsKey(this.orderDetails.csord__Account__c)) {
            sellToAcct = relatedAccountsMap.get(this.orderDetails.csord__Account__c);
            system.debug('pep ' + sellToAcct);
        }
        return sellToAcct;
    }

    /**
    * @Author       Accenture
    * @Name         setDocTitle
    * @Date         02/08/2017
    * @Description  This method is used to set Doc Title for ORDER_CONFIRMATION on VF page
    * @Param        
    * @return       NA
    */   
    public void setDocTitle() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','setDocTitle');
        this.docTitle = ord.EP_Document_Title__c;
        if(this.orderDetails.csord__Status2__c != null && this.orderDetails.csord__Status2__c.equals(EP_Common_Constant.ORDER_DRAFT_STATUS) ){
            this.docTitle = EP_OrderConfirmationConstant.ORDER_QUOTE_TITLE;
        }
        documentType = EP_Common_Constant.ORDER_CONFIRMATION_TAG;
        If(EP_Common_Constant.PICKUPADVICE_TITLE.equalsIgnoreCase(this.docTitle)){
            documentType = EP_Common_Constant.PICKUPADVICE_TITLE;
        } 
    }
    
   /**
    * @Author       Accenture
    * @Name         checkPageAccess
    * @Date         02/08/2017
    * @Description  This method will be use to redirect user at Error Page if they are trying to access this page without passing secret Code.
    * @Param        
    * @return       PageReference
    */
    public PageReference checkPageAccess() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','checkPageAccess');
        PageReference pageRef = null;
        if(! EP_OutboundMessageUtil.isAuthorized(this.secretCode)) {
            pageRef =  EP_OutboundMessageUtil.redirectToErrorPage();
        }
        return pageRef;
    }
}