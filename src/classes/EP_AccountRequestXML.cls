/**  
*  @Author           Accenture
*  @Class name       EP_AccountRequestXML
*  @Novasuite Fixes -- Required Documentation
*  @description      extends EP_GenerateRequestXML, used for customer Interfaces
*/  
public virtual class EP_AccountRequestXML Extends EP_GenerateRequestXML{
	
	public Account objAccount ;
    /**  
    *  @Author      Accenture
    *  @ name       init
    */ 
    public virtual override void init(){
        EP_GeneralUtility.Log('Public','EP_AccountRequestXML','init');
        system.debug('3333:-->'+recordId);
        MSGNode = doc.createRootElement('MSG', null, null);
        EP_AccountMapper objAccMap = new EP_AccountMapper(); 
        objAccount = objAccMap.getAccountRecord(recordId);
        

    } 
    /**  
    *  @Author      Accenture
    *  @ name       createStatusPayLoad
    */
    public override void createStatusPayLoad(){
        system.debug('**EP_AccountRequestXML** createStatusPayLoad()');
        MSGNode.addChildElement(EP_AccountConstant.STATUS_PAYLOAD,null,null);
    }
    /**  
    *  @Author      Accenture
    *  @ name       setEncryption
    */
    public virtual void setEncryption(){
        EP_CS_OutboundMessageSetting__c outboundMsgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(messageType);
        if(outboundMsgSetting != null){
            isEncryptionEnabled = outboundMsgSetting.Payload_Encoded__c;
        }
    }
    
}