/**
* @author <Accenture>
* @name EP_ASTSellToBasicDataSetupToRejected  
*/ 
public with sharing class EP_ASTSellToBasicDataSetupToRejected  extends EP_AccountStateTransition {
   /*
    *  @Author <Accenture>
    *  @Name constructor of class EP_ASTSellToBasicDataSetupToRejected
    */
    public EP_ASTSellToBasicDataSetupToRejected() {
        finalState = EP_AccountConstant.REJECTED;   
    }
    /**
    * @author <Accenture>
    * @name isTransitionPossible
    * @return Boolean
    */ 
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBasicDataSetupToRejected','isTransitionPossible');
        return super.isTransitionPossible();
    }
    /**
    * @author <Accenture>
    * @name isRegisteredForEvent 
    * @return Boolean
    */
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBasicDataSetupToRejected','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
    /**
    * @author <Accenture>
    * @name isGuardCondition 
    * @return Boolean
    */
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBasicDataSetupToRejected','isGuardCondition');
        //L4_45352 - Removed guard condition 
        return true; 
    }
}