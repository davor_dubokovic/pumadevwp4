@isTest
public with sharing class EP_ContractSyncWithWinDMSXML_UT {
	private static final string MESSAGE_TYPE = 'SFDC_TO_WINDMS_CONTRACT_SYNC'; 

    @testSetup
    public static void init(){
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
    	Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
    }

   static testMethod void createXML_Test() {
       EP_OrderSyncWithWinDMSXML localObj = new EP_OrderSyncWithWinDMSXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrderPositiveScenario();
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       String result = localObj.createXML();  
       Test.stopTest();
       System.assert(!String.isBlank(result));
   }

   static testMethod void createPayload_Test() {
       EP_OrderSyncWithWinDMSXML localObj = new EP_OrderSyncWithWinDMSXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrderPositiveScenario();
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       localObj.init();
       localObj.createPayload();  
       Test.stopTest();
       System.assert(true, localObj.MSGNode != null);
   }
}