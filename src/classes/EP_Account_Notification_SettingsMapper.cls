/****************************************************************
* @author       Accenture                                       *
* @name         EP_Account_Notification_SettingsMapper          *
* @Created Date 21/12/2017                                      *
* @description  Class to show query Notifications Data          *
****************************************************************/
public with sharing class EP_Account_Notification_SettingsMapper {
    private static final string QUERY1 = 'select id, name from ';
    private static final string NAME = 'name';
    
    /****************************************************************
    * @author       Accenture                                       *
    * @name         queryAccountNotificationSettings                *
    * @description  method to get all account notification data     *
    * @param        Account accId                                   *
    * @return       List<EP_Notification_Account_Settings__c>       *
    ****************************************************************/
    public static List<EP_Notification_Account_Settings__c> queryAccountNotificationSettings(id accId){
        return([select id,EP_Additional_Recipients__c,EP_Enabled__c,EP_Notification_Code__c,EP_Notification_Type__r.Name,EP_Notification_Template_Contact__c,
                        EP_Notification_Template_User__c,EP_Notification_Type__c,EP_Recipient_Contact__c,EP_Recipient_User__c,EP_Sell_To__c,
                        EP_Recipient_Contact__r.id,EP_Recipient_User__r.id
                    From EP_Notification_Account_Settings__c Where EP_Sell_To__c =: accId]);
    }

    /****************************************************************
    * @author       Accenture                                       *
    * @name         getNameFromId                                   *
    * @description  method to get name from id                      *
    * @param        set<Id>,String                                  *
    * @return       map<String,String>                              *
    ****************************************************************/
    public static map<String,String> getNameFromId(set<Id> idSet, String objectName, map<String,String> mapIdName){
        string queryString = QUERY1 + objectName + ' where id =:  idSet';
        system.debug('idSet = '+idSet);
        for(sobject obj : Database.query(queryString)){
            mapIdName.put((String)obj.get(EP_Common_Constant.ID),(String)obj.get(NAME));
        }
        return mapIdName;
    }

    /****************************************************************
    * @author       Accenture                                       *
    * @name         getFolderId                                     *
    * @description  method to get folder id                         *
    * @param        String                                          *
    * @return       Id                                              *
    ****************************************************************/
    public static Id getFolderId(String Notifications_Email_Templates){
        return([SELECT id,Name,Type FROM Folder WHERE DeveloperName =: Notifications_Email_Templates].id);
    }
    
    /****************************************************************
    * @author       Accenture                                       *
    * @name         getNotificationSettingsForSellToByOrderNumber                                     *
    * @description  method to get Notification Settings For SellTo By OrderNumberset                     *
    * @param        String                                          *
    * @return       map<string, EP_Notification_Account_Settings__c>                                              *
    ****************************************************************/
    public static map<string, list<EP_Notification_Account_Settings__c>> getNotificationSettingsForSellToByOrderNumber(set<string> orderIdSet){
        map<string, list<EP_Notification_Account_Settings__c>> notificationSettingMap = new map<string, list<EP_Notification_Account_Settings__c>>();
        for(EP_Notification_Account_Settings__c setting : [select id, EP_Sell_To__c, EP_Notification_Type__r.Name from EP_Notification_Account_Settings__c where EP_Sell_To__c IN (select EP_Sell_To__c from csord__Order__c where Id =: orderIdSet)]){
            list<EP_Notification_Account_Settings__c> tempSetting = new list<EP_Notification_Account_Settings__c>();
            if(notificationSettingMap.containsKey(setting.EP_Sell_To__c)){
                tempSetting  = notificationSettingMap.get(setting.EP_Sell_To__c);
            }
            tempSetting.add(setting);
            notificationSettingMap.put(setting.EP_Sell_To__c, tempSetting);
        }
        return notificationSettingMap ;
    }

    /****************************************************************
    * @author       Accenture                                       *
    * @name         getNotificationIdByCode                         *
    * @description  method to get account notification id           *
    * @param        String, String                                  *
    * @return       List                                            *
    ****************************************************************/
    public static List<EP_Notification_Account_Settings__c> getNotificationIdByCode(String notificationCode, String accId){
        return([select id,EP_Notification_Type__r.name,EP_Recipient_Contact__c,EP_Recipient_User__c from EP_Notification_Account_Settings__c where EP_Sell_To__c =: accId 
                              and EP_Notification_Code__c=: notificationCode and EP_Enabled__c = True limit 1]);
    }

    //45463 Start
    /****************************************************************
    * @author       Accenture                                       *
    * @name         getFolderId                                     *
    * @description  method to get folder id                         *
    * @param        String                                          *
    * @return       Id                                              *
    ****************************************************************/
    public static map<Id,List<EP_Notification_Account_Settings__c>> getNotificationSettingByAcc(List<Id> accSet){
        map<Id, list<EP_Notification_Account_Settings__c>> notificationSettingMap = new map<Id, list<EP_Notification_Account_Settings__c>>();
        list<EP_Notification_Account_Settings__c> tempSetting;
        for(EP_Notification_Account_Settings__c setting : [select id, EP_Sell_To__c,EP_Notification_Code__c,EP_Enabled__c, EP_Notification_Type__r.Name 
                                                                from EP_Notification_Account_Settings__c where EP_Sell_To__c =: accSet]){
            tempSetting = new list<EP_Notification_Account_Settings__c>();
            if(!notificationSettingMap.containsKey(setting.EP_Sell_To__c))
                notificationSettingMap.put(setting.EP_Sell_To__c,tempSetting);
            notificationSettingMap.get(setting.EP_Sell_To__c).add(setting);
        }
        return notificationSettingMap;
    }
    //45463 End
}