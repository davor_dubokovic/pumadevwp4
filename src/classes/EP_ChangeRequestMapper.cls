/* 
    @Author <Amit SIngh (Accenture)>
    @name <EP_ChangeRequestMapper >
    @CreateDate <02/28/2017>
    @Description <This class is a mapper class for Change Request Object>
    @Version <1.0>
    */
    public with sharing class EP_ChangeRequestMapper {

        public EP_ChangeRequestMapper (){}

        public List<EP_ChangeRequest__c> queryChangeRequestRecordsWithChild(Id reqID){
            EP_GeneralUtility.Log('Public','EP_ChangeRequestMapper','queryChangeRequestRecordsWithChild');
            return [Select Id,EP_Object_Type__c, EP_Record_Type__c, EP_Process_Step__c
            ,EP_Requestor__c
            ,EP_Requestor__r.Profile.Name
            ,EP_Region__c
            ,EP_Delivery_Country__c
            ,(select Id
            ,EP_Action__c
            ,EP_Request_Status__c
            ,EP_Review_Step__c
            ,EP_New_Value__c
            ,EP_Original_Value__c
            ,EP_Change_Request__c
            ,EP_Source_Field_API_Name__c
            from Change_Request_Lines__r)
            From EP_ChangeRequest__c Where ID =:reqID];
        }
    }