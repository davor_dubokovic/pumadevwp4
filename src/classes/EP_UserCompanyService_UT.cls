@isTest
private class EP_UserCompanyService_UT {
	
	static testMethod void doAfterInsertHandle_test(){
		company__c company = EP_TestDataUtility.createCompany('1234');
		insert company;
		EP_User_Company__c userCompObj = EP_TestDataUtility.createUserCompany(UserInfo.getUserId(),company.id);
		EP_User_CompanyDomainObject userCompDomain = new EP_User_CompanyDomainObject(userCompObj);
		EP_UserCompanyService service = new EP_UserCompanyService(userCompDomain);
		Test.startTest();
		service.doAfterInsertHandle();
		Test.stopTest();
		//Method is delegating to other method hence a dummy assert is added
        system.assert(true);
	}

	static testMethod void doAfterDeleteHandle_test(){
		company__c company = EP_TestDataUtility.createCompany('1234');
		insert company;
		EP_User_Company__c userCompObj = EP_TestDataUtility.createUserCompany(UserInfo.getUserId(),company.id);
		EP_User_CompanyDomainObject userCompDomain = new EP_User_CompanyDomainObject(userCompObj);
		EP_UserCompanyService service = new EP_UserCompanyService(userCompDomain);
		Test.startTest();
		service.doAfterDeleteHandle();
		Test.stopTest();
		//Method is delegating to other method hence a dummy assert is added
        system.assert(true);
	}
}