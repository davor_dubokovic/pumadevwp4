@isTest
public class EP_ASTVMIShipToNewToProspect_UT
{
static final string EVENT_NAME = 'NewTo01-Prospect';
static final string INVALID_EVENT_NAME = '08-ProspectTo08-Rejected';
static testMethod void isTransitionPossible_positive_test() {
	EP_AccountDomainObject obj = EP_TestDataUtility.getAccountPositiveTestScenario();
	EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
	EP_ASTVMIShipToNewToProspect ast = new EP_ASTVMIShipToNewToProspect();
	ast.setAccountContext(obj,ae);
	Test.startTest();
	boolean result = ast.isTransitionPossible();
	Test.stopTest();
	System.AssertEquals(false,result);
}
static testMethod void isTransitionPossible_negative_test() {
	EP_AccountDomainObject obj = EP_TestDataUtility.getAccountNegativeTestScenario();
	EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
	EP_ASTVMIShipToNewToProspect ast = new EP_ASTVMIShipToNewToProspect();
	ast.setAccountContext(obj,ae);
	Test.startTest();
	boolean result = ast.isTransitionPossible();
	Test.stopTest();
	System.AssertEquals(false,result);
}


static testMethod void isRegisteredForEvent_positive_test() {
	EP_AccountDomainObject obj = EP_TestDataUtility.getAccountPositiveTestScenario();
	EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
	EP_ASTVMIShipToNewToProspect ast = new EP_ASTVMIShipToNewToProspect();
	ast.setAccountContext(obj,ae);
	Test.startTest();
	boolean result = ast.isRegisteredForEvent();
	Test.stopTest();
	System.AssertEquals(false,result);
}
static testMethod void isRegisteredForEvent_negative_test() {
	EP_AccountDomainObject obj = EP_TestDataUtility.getAccountNegativeTestScenario();
	EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
	EP_ASTVMIShipToNewToProspect ast = new EP_ASTVMIShipToNewToProspect();
	ast.setAccountContext(obj,ae);
	Test.startTest();
	boolean result = ast.isRegisteredForEvent();
	Test.stopTest();
	System.AssertEquals(false,result);
}


static testMethod void isGuardCondition_positive_test() {
	EP_AccountDomainObject obj = EP_TestDataUtility.getAccountPositiveTestScenario();
	EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
	EP_ASTVMIShipToNewToProspect ast = new EP_ASTVMIShipToNewToProspect();
	ast.setAccountContext(obj,ae);
	Test.startTest();
	boolean result = ast.isGuardCondition();
	Test.stopTest();
	System.AssertEquals(true,result);
}
static testMethod void isGuardCondition_negative_test() {
	EP_AccountDomainObject obj = EP_TestDataUtility.getAccountNegativeTestScenario();
	EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
	EP_ASTVMIShipToNewToProspect ast = new EP_ASTVMIShipToNewToProspect();
	ast.setAccountContext(obj,ae);
	Test.startTest();
	boolean result = ast.isGuardCondition();
	Test.stopTest();
	System.AssertEquals(true,result);
}


}