/* ================================================
 * @Class Name : EP_CRM_AccountTriggerHandler 
 * @author : Kamendra Singh
 * @Purpose: This is Account trigger handler class and used to update account related opportunites.
 * @created date: 28/07/2016
 * @Modified Date: 29/08/2017 For S2S Purpose
 ================================================*/
public with sharing class EP_CRM_AccountTriggerHandler{
    public static Boolean isInsertRecursiveTrigger = False;    
    Public static Map<ID,Schema.RecordTypeInfo> rt_Map = Account.sObjectType.getDescribe().getRecordTypeInfosById();//Added as a change for Account Integration   
                  
    /* This method is execute after account record creation.
    * @description : This method will create account records on the secondary connection org
    * @param : Account record list
    * @return: NA
    */
    public static void afterInsertConnectionAccount(list<Account> TriggerNew){
    
    try{
        
        // Define connection id 
        Id networkId = ConnectionHelper.getConnectionId(System.Label.Connection_Name);     
        Map<ID,Schema.RecordTypeInfo> rt_Map = Account.sObjectType.getDescribe().getRecordTypeInfosById();
        
        List<Account> localAccounts = new List<Account>(); 
        Set<Id> sharedAccountSet = new Set<Id>();    
        
        // only share records created in this org, do not add contacts received from another org. 
        for (Account newAccount : TriggerNew) {           
            if(rt_map.get(newAccount.recordTypeID).getName().containsIgnoreCase('Prospect') && newAccount.EP_Status__c == '01-Prospect')//Record type filter changed as per new record type
            localAccounts.add(newAccount);                
        } 
        List<PartnerNetworkRecordConnection> accountConnections =  new  List<PartnerNetworkRecordConnection>(); 
            
        for (Account newAccount : localAccounts) {  
               
                PartnerNetworkRecordConnection newConnection = 
                  new PartnerNetworkRecordConnection( 
                      ConnectionId = networkId, 
                      LocalRecordId = newAccount.Id, 
                      SendClosedTasks = false, 
                      SendOpenTasks = false, 
                      SendEmails = false, 
                      RelatedRecords = 'Contact,Opportunity'
                      );                                                 
                accountConnections.add(newConnection);                 
        } 

        if (accountConnections.size() > 0 ) { 
               database.insert(accountConnections); 
        }         
       }
       catch(exception ex){
            ex.getmessage();
       }    
    }        

}