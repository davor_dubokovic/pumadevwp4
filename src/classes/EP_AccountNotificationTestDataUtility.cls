/****************************************************************
* @author       Accenture                                       *
* @name         EP_AccountNotificationTestDataUtility           *
* @Created Date 2/1/2018                                        *
* @description  Class to create test data                       *
****************************************************************/
@isTest
public with sharing class EP_AccountNotificationTestDataUtility {
	
	public static folder getFolder(String developerNameFolder){
		return([SELECT id,Name,Type FROM Folder WHERE DeveloperName =: developerNameFolder]);
	}

	public static EmailTemplate insertEmailTemplate(id foldrId){
		EmailTemplate templateObj = new EmailTemplate (developerName = 'UserTemplate', FolderId = foldrId, TemplateType= 'Text', Name = 'test');
		insert templateObj;
		return templateObj;
	}

	public static EP_Notification_Type__c createNotificationType(id templateId){
		EP_Notification_Type__c notifyType = new EP_Notification_Type__c();
		notifyType.EP_Default_Sign_Up__c = true;
		notifyType.EP_Notification_Code__c = '121';
		notifyType.EP_Notification_Template_Contact__c = templateId;
		notifyType.EP_Notification_Template_User__c  = templateId;
		insert notifyType;
		return notifyType;
	}

	public static EP_Notification_Account_Settings__c createAccountNotification(Id sellToId, Id templateId, String additionalUsers, 
																						Id conId, Id notifyTypeId){
		EP_Notification_Account_Settings__c notifyAcc = new EP_Notification_Account_Settings__c();
		notifyAcc.EP_Sell_To__c = sellToId;
		notifyAcc.EP_Notification_Template_User__c = templateId;
		notifyAcc.EP_Additional_Recipients__c = additionalUsers;
		notifyAcc.EP_Enabled__c = true;
		notifyAcc.EP_Recipient_Contact__c = conId;
		notifyAcc.EP_Notification_Template_Contact__c = templateId;
        notifyAcc.EP_Notification_Type__c = notifyTypeId;
        insert notifyAcc;
        return notifyAcc;
	}

	public static Attachment createAttachment(Id parentId){
		Attachment attachmentObject = new Attachment();
		attachmentObject.Body = Blob.ValueOf('Text');
		attachmentObject.Name = 'Test';
		attachmentObject.parentId = parentId;
		insert attachmentObject;
		return attachmentObject;
	}

	public static EP_Look_Up__c createLookUPData(String fields, String name, String objDisplay, String whereClause){
		EP_Look_Up__c lookupObj = new EP_Look_Up__c();
		lookupObj.EP_Fields__c = fields;
		lookupObj.EP_Object_Display_Name__c = objDisplay;
		lookupObj.EP_Where_Clause__c = whereClause;
		lookupObj.Name = name;
		insert lookupObj;
		return lookupObj;
	}
}