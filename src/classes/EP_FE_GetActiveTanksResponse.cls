/* 
    @Author <Ram Rai>
    @name <EP_FE_GetActiveTanksResponse>
    @CreateDate <25/04/2016>
    @Description <This is Wrapper Class used in EP_FE_GetActiveTanksResponse>  
    @Version <1.0>
*/
global with sharing class EP_FE_GetActiveTanksResponse extends EP_FE_Response {
    public List<EP_Tank__c> tanks {get; set;} // List of tanks
}