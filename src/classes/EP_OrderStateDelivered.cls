/*   
     @Author Aravindhan Ramalingam
     @name <EP_OrderStateDelivered.cls>     
     @Description <Order State for Delivered status, common for all classes>   
     @Version <1.1> 
     */

     public class EP_OrderStateDelivered extends EP_OrderState {
        
        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            super.setOrderDomainObject(currentOrder);
        }

        
        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_OrderStateDelivered','doOnEntry');
            
        }
        
        
        public static String getTextValue()
        {
            return EP_OrderConstant.OrderState_Delivered;
        }
    }