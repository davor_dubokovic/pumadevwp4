<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object is created to maintain the available inventory for a particular product at particular stock holding location</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>EP_Available_Inventory__c</fullName>
        <description>This field contains available inventory for the product</description>
        <externalId>false</externalId>
        <label>Available Inventory</label>
        <precision>16</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Company_Code__c</fullName>
        <description>Label changed from company to company code for defect 83204</description>
        <externalId>false</externalId>
        <label>Company Code</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Company__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Company</label>
        <referenceTo>Company__c</referenceTo>
        <relationshipLabel>Inventories</relationshipLabel>
        <relationshipName>Inventories</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Inventory_Availability__c</fullName>
        <externalId>false</externalId>
        <label>Inventory Availability</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Good</fullName>
                    <default>true</default>
                    <label>Good</label>
                </value>
                <value>
                    <fullName>Low</fullName>
                    <default>false</default>
                    <label>Low</label>
                </value>
                <value>
                    <fullName>None</fullName>
                    <default>false</default>
                    <label>None</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EP_Product_Code__c</fullName>
        <description>This is the product code for related product</description>
        <externalId>false</externalId>
        <formula>EP_Product__r.ProductCode</formula>
        <label>Product Code</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field contains name of the product.</description>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Inventories</relationshipLabel>
        <relationshipName>Inventories</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_SKU_Unique_Key__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Unique Key for SKU ( Product Code - Companay Code - Supply Location( NAV Location Id)</description>
        <externalId>true</externalId>
        <label>SKU Unique Key</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>EP_Source_Seq_Id__c</fullName>
        <externalId>false</externalId>
        <label>Source Seq Id</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Status__c</fullName>
        <description>The field is created as part of SF-R1-CUS-E2E002-FN015 requirement</description>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Active</fullName>
                    <default>true</default>
                    <label>Active</label>
                </value>
                <value>
                    <fullName>Inactive</fullName>
                    <default>false</default>
                    <label>Inactive</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EP_Stock_Label__c</fullName>
        <description>This picklist decides whether the inventory is owned by Puma or Third party vendor.</description>
        <externalId>false</externalId>
        <label>Stock Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Puma_owned</fullName>
                    <default>false</default>
                    <label>Puma_owned</label>
                </value>
                <value>
                    <fullName>Third_Party</fullName>
                    <default>false</default>
                    <label>Third_Party</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EP_Storage_Location__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field is used to contain storage location for the product</description>
        <externalId>false</externalId>
        <label>Storage Location</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordTypeId</field>
                <operation>equals</operation>
                <value>Storage Location</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Inventories</relationshipLabel>
        <relationshipName>Inventories</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Supplier__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field is used to capture supplier</description>
        <externalId>false</externalId>
        <label>Supplier</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordType.DeveloperName</field>
                <operation>equals</operation>
                <value>EP_Vendor</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Inventory (Supplier)</relationshipLabel>
        <relationshipName>Inventory</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Inventory</label>
    <listViews>
        <fullName>EP_All_Inventory_Items</fullName>
        <columns>NAME</columns>
        <columns>EP_Company__c</columns>
        <columns>EP_Product__c</columns>
        <columns>EP_Storage_Location__c</columns>
        <columns>EP_Inventory_Availability__c</columns>
        <columns>EP_Product_Code__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Inventory Items</label>
    </listViews>
    <nameField>
        <displayFormat>I-{00000}</displayFormat>
        <label>Inventory Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Inventories</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>EP_Cannot_modified_without_availablity</fullName>
        <active>true</active>
        <errorConditionFormula>AND(
NOT(ISNEW())
,ISBLANK(TEXT(EP_Inventory_Availability__c))
)</errorConditionFormula>
        <errorDisplayField>EP_Inventory_Availability__c</errorDisplayField>
        <errorMessage>SKU/Inventory can not be updated with blank Inventory Availability</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>EP_Inventory_Cant_Low_For_Service_Prod</fullName>
        <active>true</active>
        <description>System should not allow user to select the Inventory Availability Status as ‘Low’, if the associated Product Sold As ‘Service’ - L4- 45533</description>
        <errorConditionFormula>AND(ISPICKVAL(EP_Product__r.EP_Product_Sold_As__c , &apos;Service&apos;) ,  ISPICKVAL(EP_Inventory_Availability__c , &apos;Low&apos; ))</errorConditionFormula>
        <errorMessage>Inventory Availability can’t be set as ‘Low‘, if Product is Sold as  ‘Service’</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>EP_Inventory_availability_cannot_be_low</fullName>
        <active>true</active>
        <description>Created for defect 78078, Inventory Availability can&apos;t be set as Low if Product is Sold as Service</description>
        <errorConditionFormula>AND(ISPICKVAL(EP_Product__r.EP_Product_Sold_As__c , &apos;Service&apos;) ,ISPICKVAL(EP_Inventory_Availability__c, &apos;Low&apos;))</errorConditionFormula>
        <errorDisplayField>EP_Inventory_Availability__c</errorDisplayField>
        <errorMessage>Inventory Availability can&apos;t be set as &apos;Low&apos;, if Product is Sold as &apos;Service&apos;</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
