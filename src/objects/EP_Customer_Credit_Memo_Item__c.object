<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Customer Credit Memo Item</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>EP_Amount__c</fullName>
        <description>Amount</description>
        <externalId>false</externalId>
        <label>Amount</label>
        <precision>18</precision>
        <required>true</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_BOL_Number__c</fullName>
        <description>BOL Number</description>
        <externalId>false</externalId>
        <label>BOL Number</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Credit_Memo_Item_Key__c</fullName>
        <caseSensitive>true</caseSensitive>
        <description>Value generated on the fly:
&lt;billTo /&gt; + “_” + &lt; docId/&gt; + “_” + &lt; docDate/&gt; + “_” + &lt; lineid /&gt;</description>
        <externalId>true</externalId>
        <label>Credit Memo Item Key</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>EP_Credit_Memo_Line_Id__c</fullName>
        <externalId>false</externalId>
        <label>Credit Memo Line Id</label>
        <length>70</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Credit_Memo_Line_Type__c</fullName>
        <description>Credit Memo Line Type</description>
        <externalId>false</externalId>
        <label>Credit Memo Line Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Item</fullName>
                    <default>false</default>
                    <label>Item</label>
                </value>
                <value>
                    <fullName>Fuel Item Including Delivery</fullName>
                    <default>false</default>
                    <label>Fuel Item Including Delivery</label>
                </value>
                <value>
                    <fullName>Tax</fullName>
                    <default>false</default>
                    <label>Tax</label>
                </value>
                <value>
                    <fullName>Discount</fullName>
                    <default>false</default>
                    <label>Discount</label>
                </value>
                <value>
                    <fullName>Freight</fullName>
                    <default>false</default>
                    <label>Freight</label>
                </value>
                <value>
                    <fullName>VAT</fullName>
                    <default>false</default>
                    <label>VAT</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EP_Credit_Memo__c</fullName>
        <description>Master Detail (Customer Credit Memo)</description>
        <externalId>false</externalId>
        <label>Credit Memo</label>
        <referenceTo>EP_Customer_Credit_Memo__c</referenceTo>
        <relationshipLabel>Customer Credit Memo Items</relationshipLabel>
        <relationshipName>Customer_Credit_Memo_Items</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>EP_Description__c</fullName>
        <description>Description</description>
        <externalId>false</externalId>
        <label>Description</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Loading_Date__c</fullName>
        <description>Loading Date</description>
        <externalId>false</externalId>
        <label>Loading Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EP_NavParentLineItemId__c</fullName>
        <externalId>false</externalId>
        <label>NavParentLineItemId</label>
        <length>70</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_NavSeqId__c</fullName>
        <externalId>false</externalId>
        <label>NavSeqId</label>
        <length>70</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Parent_Line_Item__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Lookup (Customer Credit Memo Item object)</description>
        <externalId>false</externalId>
        <label>Parent Line Item</label>
        <referenceTo>EP_Customer_Credit_Memo_Item__c</referenceTo>
        <relationshipLabel>Customer Credit Memo Items</relationshipLabel>
        <relationshipName>Customer_Credit_Memo_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Percentage__c</fullName>
        <description>Percentage</description>
        <externalId>false</externalId>
        <label>Percentage</label>
        <precision>5</precision>
        <required>true</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>EP_Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Product</description>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Customer Credit Memo Items</relationshipLabel>
        <relationshipName>Customer_Credit_Memo_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Quantity__c</fullName>
        <description>Quantity</description>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Ship_To_Number__c</fullName>
        <externalId>false</externalId>
        <label>Ship To Number</label>
        <length>70</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Total_Amount__c</fullName>
        <description>Total Amount</description>
        <externalId>false</externalId>
        <label>Total Amount</label>
        <precision>18</precision>
        <required>true</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_Unit_Price__c</fullName>
        <description>Unit Price</description>
        <externalId>false</externalId>
        <label>Unit Price</label>
        <precision>18</precision>
        <required>true</required>
        <scale>4</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_UoM__c</fullName>
        <description>UoM</description>
        <externalId>false</externalId>
        <label>Quantity UoM</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>EP_Quantity_UoM_Values</valueSetName>
        </valueSet>
    </fields>
    <label>Customer Credit Memo Item</label>
    <nameField>
        <displayFormat>CRNI-{000000}</displayFormat>
        <label>Credit Memo Item ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Customer Credit Memo Items</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>EP_Only_Positive_Values_Amount</fullName>
        <active>true</active>
        <description>The Amount field can only contain positive values</description>
        <errorConditionFormula>EP_Amount__c  &lt; 0</errorConditionFormula>
        <errorDisplayField>EP_Amount__c</errorDisplayField>
        <errorMessage>The Amount field can only contain positive values</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>EP_Only_Positive_Values_TotalAmount</fullName>
        <active>true</active>
        <description>The TotalAmount field can only contain positive values</description>
        <errorConditionFormula>EP_Total_Amount__c   &lt; 0</errorConditionFormula>
        <errorDisplayField>EP_Total_Amount__c</errorDisplayField>
        <errorMessage>The Total Amount field can only contain positive values</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>EP_only_positive_values_Quantity</fullName>
        <active>true</active>
        <description>System should only allow positive values in the Quantity field</description>
        <errorConditionFormula>EP_Quantity__c &lt; 0</errorConditionFormula>
        <errorDisplayField>EP_Quantity__c</errorDisplayField>
        <errorMessage>The Quantity field can only contain positive values</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>EP_only_positive_values_UnitPrice</fullName>
        <active>true</active>
        <description>System should only allow positive values in the Unit Price field</description>
        <errorConditionFormula>EP_Unit_Price__c  &lt; 0</errorConditionFormula>
        <errorDisplayField>EP_Unit_Price__c</errorDisplayField>
        <errorMessage>The Unit Price field can only contain positive values</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
