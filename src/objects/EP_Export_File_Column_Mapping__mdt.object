<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <description>This object is used to store the mappings of the columns of the export files</description>
    <fields>
        <fullName>EP_Column_Order__c</fullName>
        <description>This field is used to control the order of the columns</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Column Order</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Default_Value__c</fullName>
        <description>This field is used to set the default row value when the query does not return an actual value</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Default Value</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Export_File_Type_API_Name__c</fullName>
        <description>This field is used to store the API Name of the metadata record that refers to the relevant export file type</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Export File Type API Name</label>
        <length>255</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Field_API_Name__c</fullName>
        <description>This field is used to store the API name of the corresponding field</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Field API Name</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Format__c</fullName>
        <description>This field is used to store the placeholder used to transport the value returned by the field. The exporter is using the String.format(placeholder, fillers) Apex method to complete the transformation</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Format</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Value_Type__c</fullName>
        <description>This field is used to define the type of value that will be exported in the file:
- General: No transformation, value is exported as is
- Auto-number: System is adding an incremental number (e.g. 1, 2, 3)
- Date: Convert Date/Time to Date</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Value Type</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>General</fullName>
                    <default>true</default>
                    <label>General</label>
                </value>
                <value>
                    <fullName>Auto-number</fullName>
                    <default>false</default>
                    <label>Auto-number</label>
                </value>
                <value>
                    <fullName>Date</fullName>
                    <default>false</default>
                    <label>Date</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Export File Column Mapping</label>
    <pluralLabel>Export File Column Mappings</pluralLabel>
    <visibility>Public</visibility>
</CustomObject>
