<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Invoice Line Item Object</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>EP_Amount__c</fullName>
        <externalId>false</externalId>
        <label>Amount</label>
        <precision>18</precision>
        <required>true</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_CSOrder_Line_Item__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Order Line Item</label>
        <referenceTo>csord__Order_Line_Item__c</referenceTo>
        <relationshipLabel>Invoice Line Items</relationshipLabel>
        <relationshipName>Invoice_Line_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_CSOrder__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Order</label>
        <referenceTo>csord__Order__c</referenceTo>
        <relationshipLabel>Invoice Line Items</relationshipLabel>
        <relationshipName>Invoice_Line_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Comments__c</fullName>
        <description>This field is add to capture comments from NAV as per L4 - #45365,#45366</description>
        <externalId>false</externalId>
        <label>Comments</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>EP_Description__c</fullName>
        <description>Description</description>
        <externalId>false</externalId>
        <label>Description</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Discount__c</fullName>
        <externalId>false</externalId>
        <label>Discount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_Freight__c</fullName>
        <externalId>false</externalId>
        <label>Freight</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_Invoice_Item_Key__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>&lt;billTo /&gt; + â€œ_â€ + &lt; docId/&gt; + â€œ_â€ + &lt; docDate/&gt; + â€œ_â€ + &lt; lineid /&gt;</description>
        <externalId>true</externalId>
        <inlineHelpText>Value generated on the fly:

&lt;billTo /&gt; + “_” + &lt; docId/&gt; + “_” + &lt; docDate/&gt; + “_” + &lt; lineid /&gt;</inlineHelpText>
        <label>Invoice Item Key</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>EP_Invoice_Line_Code__c</fullName>
        <description>Customer Invoice Line Code</description>
        <externalId>false</externalId>
        <label>Customer Invoice Line Code</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Invoice_Line_Type__c</fullName>
        <description>Customer Invoice Line Type</description>
        <externalId>false</externalId>
        <label>Customer Invoice Line Type</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Item</fullName>
                    <default>false</default>
                    <label>Item</label>
                </value>
                <value>
                    <fullName>G/L Account</fullName>
                    <default>false</default>
                    <label>G/L Account</label>
                </value>
                <value>
                    <fullName>Charge</fullName>
                    <default>false</default>
                    <label>Charge</label>
                </value>
                <value>
                    <fullName>Fixed Asset</fullName>
                    <default>false</default>
                    <label>Fixed Asset</label>
                </value>
                <value>
                    <fullName>Blank</fullName>
                    <default>false</default>
                    <label>Blank</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EP_Invoice__c</fullName>
        <description>Invoice master detail</description>
        <externalId>false</externalId>
        <label>Customer Invoice</label>
        <referenceTo>EP_Invoice__c</referenceTo>
        <relationshipLabel>Invoice Line Items</relationshipLabel>
        <relationshipName>Invoice_products</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>EP_NavSeqId__c</fullName>
        <description>Sequence Id</description>
        <externalId>false</externalId>
        <label>SeqId</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Nav_Id_Ref__c</fullName>
        <description>If line type is tax then we need to have a reference of the lineId the tax is applicable for,  most of the time this will contain the itemId which the TAX, VAT, Discount etc are applicable to.</description>
        <externalId>false</externalId>
        <label>IdRef</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Order_Line_Item__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>It might contain other charges not related to OLIs. Hence this lookup might or might not be populated depending on the type of Invoice Item.</description>
        <externalId>false</externalId>
        <label>Order Line Item</label>
        <referenceTo>OrderItem</referenceTo>
        <relationshipLabel>Invoice Line Items</relationshipLabel>
        <relationshipName>Invoice_Line_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Order__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>To be used for backend – to store Orders that will be pushed from NAV</description>
        <externalId>false</externalId>
        <label>Order</label>
        <referenceTo>Order</referenceTo>
        <relationshipLabel>Invoice Line Items</relationshipLabel>
        <relationshipName>Invoice_products</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Parent_Line_Item__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This is an optional field to capture self lookup. These will be used in case of freight or Tax against a product.</description>
        <externalId>false</externalId>
        <label>Parent Line Item</label>
        <referenceTo>EP_Invoice_Line_Item__c</referenceTo>
        <relationshipLabel>Invoice Line Items</relationshipLabel>
        <relationshipName>Invoice_Line_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Percentage__c</fullName>
        <externalId>false</externalId>
        <label>Percentage</label>
        <precision>4</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>EP_Product_Code__c</fullName>
        <description>This field is added for Reporting</description>
        <externalId>false</externalId>
        <formula>EP_Product__r.ProductCode</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product Code</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Product_Description__c</fullName>
        <description>This field is created only for Reporting</description>
        <externalId>false</externalId>
        <formula>EP_Product__r.Description</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Deduce lookup based on the Invoice Line Code if the Invoice Line Type is â€œItemâ€.</description>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Invoice Line Items</relationshipLabel>
        <relationshipName>Invoice_Line_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Quantity__c</fullName>
        <description>Quantity</description>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Ship_To__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Ship-To</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordTypeId</field>
                <operation>equals</operation>
                <value>Non-VMI Ship To, VMI Ship To</value>
            </filterItems>
            <filterItems>
                <field>Account.EP_Status__c</field>
                <operation>equals</operation>
                <value>05-Active</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Invoice Line Items (Ship-To)</relationshipLabel>
        <relationshipName>Invoice_Line_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Storage_Location__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Filter by Storage Location Account record type</description>
        <externalId>false</externalId>
        <label>Storage Location</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordTypeId</field>
                <operation>equals</operation>
                <value>Storage Location</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipName>Invoice_products</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Tax__c</fullName>
        <externalId>false</externalId>
        <label>Tax</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_Total_Amount__c</fullName>
        <externalId>false</externalId>
        <label>Total Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_UOM__c</fullName>
        <description>UOM</description>
        <externalId>false</externalId>
        <label>Quantity UOM</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>EP_Quantity_UoM_Values</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>EP_Unit_Price__c</fullName>
        <externalId>false</externalId>
        <label>Unit Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Invoice Line Item</label>
    <nameField>
        <label>Line Order Number</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Invoice Line Items</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>EP_Sales_Invoice_Storage_Loc_Required</fullName>
        <active>false</active>
        <description>Filter by Storage Location Account record type
Required only in case of Sales Invoice.</description>
        <errorConditionFormula>IF(EP_Invoice__r.RecordType.DeveloperName = &apos;EP_Sales_Invoice&apos; &amp;&amp; ISBLANK(EP_Storage_Location__c), true, false)</errorConditionFormula>
        <errorDisplayField>EP_Storage_Location__c</errorDisplayField>
        <errorMessage>Storage Location can not be empty for Sales Invoice</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
