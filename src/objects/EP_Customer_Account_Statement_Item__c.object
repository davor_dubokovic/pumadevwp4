<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>ePuma - Salesforce will track the following Customer Account Statement Items:	Invoices, Credit Notes, Payments In,	Payments Out
These items will be stored under this custom object. These items are created and managed in NAV</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>EP_Bill_To__c</fullName>
        <description>Master-Detail (Account)
Filtered – (Only Sell-To or Bill-To)</description>
        <externalId>false</externalId>
        <label>Bill-To</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordTypeId</field>
                <operation>equals</operation>
                <value>Bill To, Sell To</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Customer Account Statement Items</relationshipLabel>
        <relationshipName>Customer_Account_Statement_Items</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>EP_Credit__c</fullName>
        <description>Only displayed on the Credit Note and Payment Out page layouts</description>
        <externalId>false</externalId>
        <label>Credit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_Customer_Account_Statement_Item_Key__c</fullName>
        <caseSensitive>true</caseSensitive>
        <description>Customer Account Statement Item Key.</description>
        <externalId>true</externalId>
        <label>Statement Item Key</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>EP_Customer_Credit_Memo__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Customer Credit Memo</label>
        <referenceTo>EP_Customer_Credit_Memo__c</referenceTo>
        <relationshipLabel>Customer Account Statement Items</relationshipLabel>
        <relationshipName>Customer_Account_Statement_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Customer_Invoice__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Only displayed on the Invoice page layout</description>
        <externalId>false</externalId>
        <label>Customer Invoice</label>
        <referenceTo>EP_Invoice__c</referenceTo>
        <relationshipLabel>Customer Account Statement Items</relationshipLabel>
        <relationshipName>Customer_Account_Statement_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Customer_Other_Adjustment__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Customer Other Adjustment</label>
        <referenceTo>EP_Customer_Other_Adjustment__c</referenceTo>
        <relationshipLabel>Customer Account Statement Items</relationshipLabel>
        <relationshipName>Customer_Account_Statement_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Customer_Payment__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Only displayed on the Payment In and Payment Out page layouts</description>
        <externalId>false</externalId>
        <label>Customer Payment</label>
        <referenceTo>EP_Customer_Payment__c</referenceTo>
        <relationshipLabel>Customer Account Statement Items</relationshipLabel>
        <relationshipName>Customer_Account_Statement_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Debit__c</fullName>
        <description>Only displayed on the Invoice and Payment In page layouts</description>
        <externalId>false</externalId>
        <label>Debit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_Document_Type__c</fullName>
        <description>NAV DocType</description>
        <externalId>false</externalId>
        <label>Document Type</label>
        <length>100</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Entry_Number__c</fullName>
        <description>Nav Entry Number</description>
        <externalId>false</externalId>
        <label>Entry Number</label>
        <length>100</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Is_Reversal__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Is Reversal</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EP_NAV_Statement_Item_ID__c</fullName>
        <externalId>false</externalId>
        <label>Document ID</label>
        <length>80</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_SeqId__c</fullName>
        <description>Nav seq Id. Value comes from Customer Payment seq Id</description>
        <externalId>false</externalId>
        <label>SeqId</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Statement_Item_Due_Date__c</fullName>
        <externalId>false</externalId>
        <label>Statement Item Due Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EP_Statement_Item_Issue_Date__c</fullName>
        <description>Required Date</description>
        <externalId>false</externalId>
        <label>Statement Item Issue Date</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>Customer Account Statement Item</label>
    <listViews>
        <fullName>EP_All_Credit_Memo_Statement_Items</fullName>
        <columns>NAME</columns>
        <columns>EP_Bill_To__c</columns>
        <columns>EP_Statement_Item_Issue_Date__c</columns>
        <columns>EP_Credit__c</columns>
        <columns>EP_Debit__c</columns>
        <columns>CURRENCY_ISO_CODE</columns>
        <columns>EP_Customer_Credit_Memo__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>EP_Customer_Account_Statement_Item__c.EP_Credit_Memo</value>
        </filters>
        <label>3. All Credit Memo Statement Items</label>
    </listViews>
    <listViews>
        <fullName>EP_All_Customer_Acc_Statement_Items</fullName>
        <columns>NAME</columns>
        <columns>EP_Bill_To__c</columns>
        <columns>EP_Statement_Item_Issue_Date__c</columns>
        <columns>EP_Credit__c</columns>
        <columns>EP_Debit__c</columns>
        <columns>CURRENCY_ISO_CODE</columns>
        <columns>EP_Customer_Invoice__c</columns>
        <columns>EP_Customer_Payment__c</columns>
        <columns>EP_Customer_Credit_Memo__c</columns>
        <filterScope>Everything</filterScope>
        <label>1. All Customer Acc Statement Items</label>
    </listViews>
    <listViews>
        <fullName>EP_All_Customer_Other_Adjustment_Items</fullName>
        <columns>NAME</columns>
        <columns>EP_Bill_To__c</columns>
        <columns>EP_Statement_Item_Issue_Date__c</columns>
        <columns>EP_Credit__c</columns>
        <columns>EP_Debit__c</columns>
        <columns>CURRENCY_ISO_CODE</columns>
        <columns>EP_Customer_Other_Adjustment__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>EP_Customer_Account_Statement_Item__c.EP_Other_Customer_Adjustment</value>
        </filters>
        <label>6.	All Customer Other Adjustment Items</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>EP_All_Invoice_Statement_Items</fullName>
        <columns>NAME</columns>
        <columns>EP_Bill_To__c</columns>
        <columns>EP_Statement_Item_Issue_Date__c</columns>
        <columns>EP_Statement_Item_Due_Date__c</columns>
        <columns>EP_Credit__c</columns>
        <columns>EP_Debit__c</columns>
        <columns>CURRENCY_ISO_CODE</columns>
        <columns>EP_Customer_Invoice__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>EP_Customer_Account_Statement_Item__c.EP_Customer_Invoice</value>
        </filters>
        <label>2. All Invoice Statement Items</label>
    </listViews>
    <listViews>
        <fullName>EP_All_Payment_In_Statement_Items</fullName>
        <columns>NAME</columns>
        <columns>EP_Bill_To__c</columns>
        <columns>EP_Statement_Item_Issue_Date__c</columns>
        <columns>EP_Credit__c</columns>
        <columns>EP_Debit__c</columns>
        <columns>CURRENCY_ISO_CODE</columns>
        <columns>EP_Customer_Payment__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>EP_Customer_Account_Statement_Item__c.EP_Payment_In</value>
        </filters>
        <label>4. All Payment In Statement Items</label>
    </listViews>
    <listViews>
        <fullName>EP_All_Payment_Out_Statement_Items</fullName>
        <columns>NAME</columns>
        <columns>EP_Bill_To__c</columns>
        <columns>EP_Statement_Item_Issue_Date__c</columns>
        <columns>EP_Credit__c</columns>
        <columns>EP_Debit__c</columns>
        <columns>CURRENCY_ISO_CODE</columns>
        <columns>EP_Customer_Payment__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>EP_Customer_Account_Statement_Item__c.EP_Payment_Out</value>
        </filters>
        <label>5. All Payment Out Statement Items</label>
    </listViews>
    <nameField>
        <displayFormat>CASI-{00000000}</displayFormat>
        <label>Customer Account Statement Item ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Customer Account Statement Items</pluralLabel>
    <recordTypes>
        <fullName>EP_Credit_Memo</fullName>
        <active>true</active>
        <label>Credit Memo</label>
    </recordTypes>
    <recordTypes>
        <fullName>EP_Customer_Invoice</fullName>
        <active>true</active>
        <label>Customer Invoice</label>
    </recordTypes>
    <recordTypes>
        <fullName>EP_Other_Customer_Adjustment</fullName>
        <active>true</active>
        <label>Other Customer Adjustment</label>
    </recordTypes>
    <recordTypes>
        <fullName>EP_Payment_In</fullName>
        <active>true</active>
        <label>Payment In</label>
    </recordTypes>
    <recordTypes>
        <fullName>EP_Payment_Out</fullName>
        <active>true</active>
        <label>Payment Out</label>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>EP_NAV_Statement_Item_ID__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>EP_Bill_To__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>EP_Statement_Item_Issue_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>EP_Credit__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>EP_Debit__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CURRENCY_ISO_CODE</customTabListAdditionalFields>
        <customTabListAdditionalFields>EP_Customer_Payment__c</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>EP_Customer_Invoice_Validation</fullName>
        <active>true</active>
        <errorConditionFormula>IF(RecordType.DeveloperName  &lt;&gt; &apos;EP_Customer_Invoice&apos; , !ISBlank(EP_Customer_Invoice__c) , false)</errorConditionFormula>
        <errorDisplayField>EP_Customer_Invoice__c</errorDisplayField>
        <errorMessage>Customer Invoice cannot be set for the selected type of Customer Account Statement Item</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>EP_Customer_Payment_Validation</fullName>
        <active>true</active>
        <errorConditionFormula>IF(AND(RecordType.DeveloperName &lt;&gt; &apos;EP_Payment_In&apos;,  RecordType.DeveloperName &lt;&gt; &apos;EP_Payment_Out&apos;), !ISBlank(EP_Customer_Payment__c), false)</errorConditionFormula>
        <errorDisplayField>EP_Customer_Payment__c</errorDisplayField>
        <errorMessage>Customer Payment cannot be set for the selected type of Customer Account Statement Item</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
