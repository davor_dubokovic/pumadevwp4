<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>ePuma - Payments will be created and managed in NAV. Payments will be synced with Salesforce as and when they are created in NAV. Salesforce will store the payment in a Custom object call “Customer Payment”.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>EP_Amount_Paid__c</fullName>
        <description>Amount Paid</description>
        <externalId>false</externalId>
        <label>Amount Paid</label>
        <precision>18</precision>
        <required>true</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_Bill_To__c</fullName>
        <description>Lookup(Account) Filtered – (Only Sell-To or Bill-To)</description>
        <externalId>false</externalId>
        <label>Bill-To</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordTypeId</field>
                <operation>equals</operation>
                <value>Bill To, Sell To</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Customer Payments</relationshipLabel>
        <relationshipName>Customer_Payments</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>EP_Entry_Number__c</fullName>
        <description>Added field as per L4 #45304</description>
        <externalId>false</externalId>
        <label>Entry Number</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Is_Reversal__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Added field as per L4 #45304</description>
        <externalId>false</externalId>
        <label>Is Reversal</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EP_NavSeqId__c</fullName>
        <description>Nav sequence ID</description>
        <externalId>false</externalId>
        <label>SeqId</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Payment_Date__c</fullName>
        <description>Payment Date</description>
        <externalId>false</externalId>
        <label>Payment Date</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EP_Payment_Key__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>Payment Key</label>
        <length>150</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>EP_Payment_Method__c</fullName>
        <description>Payment Method</description>
        <externalId>false</externalId>
        <label>Payment Method</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Cheque</fullName>
                    <default>false</default>
                    <label>Cheque</label>
                </value>
                <value>
                    <fullName>Online</fullName>
                    <default>false</default>
                    <label>Online</label>
                </value>
                <value>
                    <fullName>Wire Transfer</fullName>
                    <default>false</default>
                    <label>Wire Transfer</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EP_Payment_Reference_Number__c</fullName>
        <description>According to the data mapping this value is contained with the NAV referenceNr of the XML message</description>
        <externalId>false</externalId>
        <label>Payment Reference Number</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Payment_Remaining_Balance__c</fullName>
        <externalId>false</externalId>
        <label>Payment Remaining Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>Customer Payment</label>
    <listViews>
        <fullName>EP_All_Payments</fullName>
        <columns>NAME</columns>
        <columns>EP_Payment_Date__c</columns>
        <columns>EP_Amount_Paid__c</columns>
        <columns>CURRENCY_ISO_CODE</columns>
        <columns>EP_Payment_Method__c</columns>
        <columns>EP_Bill_To__c</columns>
        <columns>EP_Payment_Reference_Number__c</columns>
        <columns>EP_Payment_Remaining_Balance__c</columns>
        <columns>EP_Is_Reversal__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>EP_Customer_Payment__c.EP_Payment</value>
        </filters>
        <label>1. All Payments</label>
    </listViews>
    <listViews>
        <fullName>EP_Customer_Payments</fullName>
        <columns>NAME</columns>
        <columns>EP_Payment_Date__c</columns>
        <columns>EP_Amount_Paid__c</columns>
        <columns>CURRENCY_ISO_CODE</columns>
        <columns>EP_Payment_Method__c</columns>
        <columns>EP_Bill_To__c</columns>
        <columns>EP_Payment_Reference_Number__c</columns>
        <columns>EP_Payment_Remaining_Balance__c</columns>
        <columns>EP_Is_Reversal__c</columns>
        <filterScope>Everything</filterScope>
        <label>3. All Customer Payments</label>
    </listViews>
    <listViews>
        <fullName>EP_Payment_Refunds</fullName>
        <columns>NAME</columns>
        <columns>EP_Payment_Date__c</columns>
        <columns>EP_Amount_Paid__c</columns>
        <columns>CURRENCY_ISO_CODE</columns>
        <columns>EP_Payment_Method__c</columns>
        <columns>EP_Bill_To__c</columns>
        <columns>EP_Payment_Reference_Number__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>EP_Customer_Payment__c.EP_Payment_Refund</value>
        </filters>
        <label>2. All Payment Refunds</label>
    </listViews>
    <nameField>
        <label>Payment ID</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Customer Payments</pluralLabel>
    <recordTypes>
        <fullName>EP_Payment</fullName>
        <active>true</active>
        <description>Payments will be stored under the “Payment” record type.</description>
        <label>Payment</label>
        <picklistValues>
            <picklist>EP_Payment_Method__c</picklist>
            <values>
                <fullName>Cheque</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Online</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Wire Transfer</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>EP_Payment_Refund</fullName>
        <active>true</active>
        <description>This record type is used to store refund payments to customers</description>
        <label>Payment Refund</label>
        <picklistValues>
            <picklist>EP_Payment_Method__c</picklist>
            <values>
                <fullName>Cheque</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Online</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Wire Transfer</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>EP_Payment_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>EP_Amount_Paid__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CURRENCY_ISO_CODE</customTabListAdditionalFields>
        <customTabListAdditionalFields>EP_Payment_Method__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>EP_Bill_To__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>EP_Payment_Reference_Number__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>EP_Payment_Remaining_Balance__c</customTabListAdditionalFields>
        <searchResultsAdditionalFields>EP_Payment_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Amount_Paid__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CURRENCY_ISO_CODE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Payment_Method__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Bill_To__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Payment_Reference_Number__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Payment_Remaining_Balance__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
