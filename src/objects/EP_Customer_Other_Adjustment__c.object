<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>EP_Adjustment_Unique_Key__c</fullName>
        <externalId>false</externalId>
        <formula>UPPER(EP_Client_ID__c + &quot;_&quot; + EP_Bill_To__r.AccountNumber  + &quot;_&quot; +  Name + &quot;_&quot; +  EP_Entry_Number__c)</formula>
        <label>Adjustment Unique Key</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Amount_Paid__c</fullName>
        <externalId>false</externalId>
        <label>Amount Paid</label>
        <precision>18</precision>
        <required>true</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_Bad_Debt_Written_Off__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Added field as per L4 #45318</description>
        <externalId>false</externalId>
        <label>Bad Debt Written Off</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EP_Bill_To__c</fullName>
        <externalId>false</externalId>
        <label>Bill-To</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordTypeName__c</field>
                <operation>equals</operation>
                <value>Sell To</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Customer Other Adjustments</relationshipLabel>
        <relationshipName>Customer_Other_Adjustments</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>EP_Client_ID__c</fullName>
        <externalId>false</externalId>
        <label>Client ID</label>
        <length>255</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Description__c</fullName>
        <externalId>false</externalId>
        <label>Description</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Docstate__c</fullName>
        <externalId>false</externalId>
        <label>Docstate</label>
        <length>255</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Document_Date__c</fullName>
        <externalId>false</externalId>
        <label>Document Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EP_Due_Date__c</fullName>
        <description>This field is created for L4 #45312.</description>
        <externalId>false</externalId>
        <label>Due Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EP_Entry_Number__c</fullName>
        <description>Added field as per L4 #45304</description>
        <externalId>false</externalId>
        <label>Entry Number</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Is_Reversal__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Added field as per L4 #45304</description>
        <externalId>false</externalId>
        <label>Is Reversal</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EP_Payment_Method__c</fullName>
        <externalId>false</externalId>
        <label>Payment Method</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Cheque</fullName>
                    <default>false</default>
                    <label>Cheque</label>
                </value>
                <value>
                    <fullName>Online</fullName>
                    <default>false</default>
                    <label>Online</label>
                </value>
                <value>
                    <fullName>Wire Transfer</fullName>
                    <default>false</default>
                    <label>Wire Transfer</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EP_Payment_Reference_Number__c</fullName>
        <externalId>false</externalId>
        <label>Payment Reference Number</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Payment_Term__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Payment Term</label>
        <referenceTo>EP_Payment_Term__c</referenceTo>
        <relationshipLabel>Customer Other Adjustments</relationshipLabel>
        <relationshipName>Customer_Other_Adjustments</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Reason_Code__c</fullName>
        <externalId>false</externalId>
        <label>Reason Code</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Remaining_Amount__c</fullName>
        <externalId>false</externalId>
        <label>Remaining Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_Sell_To__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field is created for L4 #45312.</description>
        <externalId>false</externalId>
        <label>Sell-To</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordTypeName__c</field>
                <operation>equals</operation>
                <value>Sell To</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Customer Other Adjustments (Sell-To)</relationshipLabel>
        <relationshipName>Customer_Other_Adjustments1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Source_Seq_Id__c</fullName>
        <externalId>false</externalId>
        <label>Source Seq Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Submission_Date__c</fullName>
        <externalId>false</externalId>
        <label>Submission Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>Customer Other Adjustment</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>EP_Adjustment_Unique_Key__c</columns>
        <columns>EP_Bill_To__c</columns>
        <columns>EP_Entry_Number__c</columns>
        <columns>EP_Client_ID__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Adjustment ID</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Customer Other Adjustments</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Other_Customer_Adjustment</fullName>
        <active>true</active>
        <label>Other Customer Adjustment</label>
        <picklistValues>
            <picklist>EP_Payment_Method__c</picklist>
            <values>
                <fullName>Cheque</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Online</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Wire Transfer</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
