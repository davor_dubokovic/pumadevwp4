trigger Ep_UserCompanyTrigger on EP_User_Company__c (after insert, after delete) {
    //As the Association will be done using the UI, hence bolk handling not required

    if(trigger.isInsert){
    	EP_User_CompanyDomainObject userCompDomain = new EP_User_CompanyDomainObject(trigger.new[0]);
        userCompDomain.doActionAfterInsert();    
    }
    if(trigger.isDelete){
    	EP_User_CompanyDomainObject userCompDomain = new EP_User_CompanyDomainObject(trigger.old[0]);
        userCompDomain.doActionAfterDelete();                      
    }


}