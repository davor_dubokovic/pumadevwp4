/*  
  @Author <Jai Singh>
   @name <EP_SupplyLocationTransporterTrigger>
   @CreateDate <16/06/2016>
   @Description <trigger for object EP_Supply_Location_Transporter__c>  
   @Version <1.0>
*/
trigger EP_SupplyLocationTransporterTrigger on EP_Supply_Location_Transporter__c ( before insert, before update, before delete ) 
{
    //before block
    if( trigger.isBefore)
    {
        //insert block
        if( trigger.isInsert )
        {
            EP_SupplyLocationTransprtrTriggerHandler.doBeforeInsert( trigger.new );
        }
        
        //check if execution context is from this trigger's update dml
        if( EP_SupplyLocationTransprtrTriggerHandler.isExecuteUpdate )
        {
            //update block
            if( trigger.isUpdate )
            {
                EP_SupplyLocationTransprtrTriggerHandler.doBeforeUpdate(trigger.oldMap, trigger.newMap);
            }
        }
        
        //delete block
        if( trigger.isDelete )
        {
            EP_SupplyLocationTransprtrTriggerHandler.doBeforeDelete(trigger.oldMap);
        }
    }

}