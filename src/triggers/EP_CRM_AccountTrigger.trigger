trigger EP_CRM_AccountTrigger on Account (before insert, before update,after insert,after update) {
     
    if(trigger.isAfter && trigger.isInsert && !EP_CRM_AccountTriggerHandler.isInsertRecursiveTrigger){
        EP_CRM_AccountTriggerHandler.afterInsertConnectionAccount(trigger.new);
        EP_CRM_AccountTriggerHandler.isInsertRecursiveTrigger= true;
    }               
}