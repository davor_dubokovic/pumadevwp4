/* 
@Author      CloudSense
@name        EP_CsOrderTrigger
@CreateDate  15/01/2018
@Description CS Order Trigger
@Version     1.0
*/ 
trigger EP_CsOrderTrigger on csord__Order__c (after insert, after update, before delete) {
	if(trigger.isAfter){
		if(trigger.isUpdate){
			EP_CsOrderTriggerHandler.doAfterUpdate(trigger.newmap);
			EP_CsOrderTriggerHandler.handleInvoiceCleanup(trigger.new, trigger.oldMap);
		}
	}
}