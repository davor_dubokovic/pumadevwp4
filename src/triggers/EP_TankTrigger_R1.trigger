/* 
  @Author <Ashok Arora>
  @name <EP_TankTrigger>
  @CreateDate <03/11/2015>
  @Description <This is TankTrigger>
  @Version <1.0>
 */
trigger EP_TankTrigger_R1 on EP_Tank__c (before insert,after update,before update,after insert) {
    if(trigger.isBefore){
        if(trigger.isInsert){
            //EP_AccountTriggerHandler.isRecordOnInsert = true;
            EP_TankTriggerHandler_R1.doBeforeInsert(trigger.new);
        }
        else if(trigger.isupdate && !EP_TankTriggerHandler_R1.isBeforeUpdate){
            EP_TankTriggerHandler_R1.doBeforeupdate(trigger.old,trigger.new,trigger.oldMap,trigger.newmap);
        }
        //else{}
    }
    if(trigger.isAfter) {
    	//#Bug-72191
        if(trigger.isInsert){
            EP_TankTriggerHandler_R1.doAfterInsert(trigger.new,trigger.newmap);
        }
        //#Bug-72191
        if (trigger.isUpdate
            && !EP_TankTriggerHandler_R1.isAfterUpdate) {
           EP_TankTriggerHandler_R1.doAfterUpdate(trigger.oldMap,trigger.Newmap, trigger.new);
        }
    }
    /*if(trigger.isbefore){
        if(trigger.isupdate){
            EP_TankTriggerHandler_R1.doBeforeupdate(trigger.old,trigger.new,trigger.oldMap,trigger.newmap);
        }
    }*/
}