/**
 * @author <Arpit Sethi>
 * @name <EP_OrderConfigurationTrigger>
 * @createDate <21/01/2016>
 * @description <This trigger handles events on EP_Order_Configuration__c object> 
 * @version <1.0>
 */
trigger EP_OrderConfigurationTrigger on EP_Order_Configuration__c(before insert,before update) {
    if(trigger.isBefore){
        if(trigger.isInsert || trigger.isUpdate){
            EP_OrderConfigTriggerHandler.doBeforeInsert(trigger.new);
        }
    }
}