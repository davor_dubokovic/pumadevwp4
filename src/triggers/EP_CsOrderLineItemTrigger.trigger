/*
@Author      CloudSense
@name        EP_CsOrderLineItemTrigger
@CreateDate  15/01/2018
@Description Trigger on the custom object: Order Line Item
@Version     1.0
*/
trigger EP_CsOrderLineItemTrigger on csord__Order_Line_Item__c (after insert, after update, before delete) {

	if (trigger.isBefore) {
		if (trigger.isDelete) {
			EP_CsOrderLineItemTriggerHandler.contractOrderUpdateAfterDelete(trigger.old);
		}
	}else if(trigger.isAfter){
		if (trigger.isInsert) {
			EP_CsOrderLineItemTriggerHandler.contractOrderUpdate(trigger.new);
		} else if (trigger.isUpdate) {
			EP_CsOrderLineItemTriggerHandler.contractOrderUpdate(trigger.new);
		}
	}
	
}