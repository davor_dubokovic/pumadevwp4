trigger EP_CRM_CompanyTrigger on Company__c (after insert) {
     
    if(trigger.isAfter && trigger.isInsert && !EP_CRM_CompanyTriggerHandler.isInsertRecursiveTrigger){
        EP_CRM_CompanyTriggerHandler.afterInsertConnectionCompany(trigger.new);
        EP_CRM_CompanyTriggerHandler.isInsertRecursiveTrigger= true;
    }

}