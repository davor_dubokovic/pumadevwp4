/*  
  @Author <Ashok Arora>
  @name <EP_UserTrigger >
  @CreateDate <18/11/2015>
  @Description <This is User Trigger>
  @Version <1.0>
 
*/
trigger EP_UserTrigger_R1 on User (before insert, before update, after insert, after update) {
    //if(EP_TriggerSwitch.isExecuteUserTrigger) {
        if(trigger.isBefore){
            if(trigger.isInsert){
                EP_UserTriggerHandler_R1.doBeforeInsert(trigger.new);
            }
            if(trigger.isUpdate && EP_UserTriggerHandler_R1.isExecuteBeforeUpdate){
                EP_UserTriggerHandler_R1.doBeforeUpdate(trigger.new);
            }
        }
        
        if(trigger.isAfter){
            if(trigger.isInsert){
                EP_UserTriggerHandler_R1.doAfterInsert(trigger.new);
            }
            if(trigger.isUpdate && EP_UserTriggerHandler_R1.isExecuteAfterUpdate){
                EP_UserTriggerHandler_R1.doAfterUpdate(trigger.new);
            }
        }
    //}        
}