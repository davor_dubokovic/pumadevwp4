/*
*  @Author <Accenture>
*  @Name <EP_ProductOption_Trigger>
*  @CreateDate <11/10/2017>
*  @Description <Trigger for Product Option Object>
*  @Version <1.0>
*/
trigger EP_ProductOption_Trigger on EP_Product_Option__c (before insert,before update, after insert, after update,before delete) {
    if(Trigger_Settings__c.getInstance('EP_ProductOption_Trigger')!= null && Trigger_Settings__c.getInstance('EP_ProductOption_Trigger').IsActive__c){
        if(trigger.isBefore){
            if(trigger.isInsert){
                EP_ProductOptionDomain productOptiondomain = new EP_ProductOptionDomain(trigger.new);
                productOptiondomain.doActionBeforeInsert();
            }
            if(trigger.isUpdate){
                EP_ProductOptionDomain productOptiondomain = new EP_ProductOptionDomain(trigger.new,trigger.oldmap);
                productOptiondomain.doActionBeforeUpdate();
            }
            if(trigger.isDelete){
                EP_ProductOptionDomain productOptiondomain = new EP_ProductOptionDomain(trigger.old);
                productOptiondomain.doActionOnBeforeDelete();
            }
        }
    }
}