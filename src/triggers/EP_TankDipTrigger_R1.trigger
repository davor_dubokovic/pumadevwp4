/* 
  @Author <SFDC>
  @name <EP_TankDipTrigger_R1>
  @Description <This is TankDipTrigger>
  @Version <1.0>
 
*/
trigger EP_TankDipTrigger_R1 on EP_Tank_Dip__c (before insert, before update, after insert, after update) {
    
    // 1. Calculate the local site time to ensure that the reading date/time stored in 
    // SFDC is the reading date/time of the site
    
     if (Trigger.isBefore) {
        if (Trigger.isInsert) {
            EP_TankDipTriggerHandler_R1.doBeforeInsert(Trigger.New);
        }
        if (Trigger.isUpdate) {
            EP_TankDipTriggerHandler_R1.doBeforeUpdate(Trigger.oldMap, Trigger.New);
        }    
    } // End is before check
    
    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            EP_TankDipTriggerHandler_R1.doAfterInsert(trigger.New);
        }
        if (Trigger.isUpdate) {
            EP_TankDipTriggerHandler_R1.doAfterUpdate(trigger.oldMap, trigger.New);
        }
    } // End is before check
}