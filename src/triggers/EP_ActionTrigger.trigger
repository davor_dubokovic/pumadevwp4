/* @Author <Amit Singh>
   @name <EP_ActionTrigger>
   @CreateDate <24/04/2016>
   @Description <This is the trigger for action object>
   @Version <1.0>
*/
trigger EP_ActionTrigger on EP_Action__c (before insert,before update, after update,after insert) {
    //if(EP_TriggerSwitch.isExecuteActionTrigger) {
        if(trigger.isBefore){
            if(trigger.isInsert && !EP_ActionTriggerHandler.isExecuteBeforeInsert){
                EP_ActionTriggerHandler.doBeforeInsert( trigger.new);
            }
            if(trigger.isUpdate && !EP_ActionTriggerHandler.isExecuteBeforeUpdate){
                EP_ActionTriggerHandler.doBeforeUpdate( trigger.newmap,trigger.oldMap);
            }
        
        }
        
        if(trigger.isAfter){
            system.debug('**AfterUpdate**');
            if(trigger.isUpdate && !EP_ActionTriggerHandler.isExecuteAfterUpdate){
                EP_ActionTriggerHandler.doAfterApdate( trigger.newmap,trigger.oldMap);
            }
             /***L4-45352 start****/
            if(trigger.isInsert && !EP_ActionTriggerHandler.isExecuteAfterInsert){
                EP_ActionTriggerHandler.doAfterInsert(trigger.new);
            }
             /***L4-45352 end****/
        }
    //}
   
}