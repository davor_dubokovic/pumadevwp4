/* 
  @Author <SFDC>
  @name <EP_IntegrationRecordTrigger>
  @Description <This is IntegrationRecordTrigger>
  @Version <1.0>
 
*/
trigger EP_IntegrationRecordTrigger on EP_IntegrationRecord__c (before insert,after insert,before update,after update) {
    //if(EP_TriggerSwitch.isExecuteIntegrationRecordTrigger) {
        if( trigger.isAfter ){
            if(trigger.isInsert){
                EP_IntegrationRecordTriggerHandler.doAfterInsert(Trigger.New);
            }
            if(trigger.isUpdate){
                EP_IntegrationRecordTriggerHandler.doAfterUpdate(Trigger.Old,Trigger.New,Trigger.OldMap,Trigger.NewMap);
            }
        }
        if(trigger.isBefore){
            if(trigger.isInsert){
                EP_IntegrationRecordTriggerHandler.doBeforeInsert(Trigger.New);
            }
            if(trigger.isUpdate){
                EP_IntegrationRecordTriggerHandler.doBeforeUpdate(Trigger.New);
            }
        }
    //}
}