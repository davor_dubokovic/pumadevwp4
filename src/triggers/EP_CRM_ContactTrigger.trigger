trigger EP_CRM_ContactTrigger on Contact (after insert) {
     
    if(trigger.isAfter && trigger.isInsert && !EP_CRM_ContactTriggerHandler.isInsertRecursiveTrigger){
        EP_CRM_ContactTriggerHandler.afterInsertConnectionContact(trigger.new);
        EP_CRM_ContactTriggerHandler.isInsertRecursiveTrigger= true;
    }
}