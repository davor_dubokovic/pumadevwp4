/*
*  @Author <Accenture>
*  @Name <EP_RO_Import_Staging_trigger>
*  @CreateDate <04/16/2018>
*  @Description <Trigger for EP_RO_Import_Staging__c Object>
*  @Version <1.0>
*/
trigger EP_RO_Import_Staging_trigger on EP_RO_Import_Staging__c (before update,after update) {
if(EP_CheckRecursive.run && Trigger_Settings__c.getInstance('EP_RO_Import_Staging_trigger')!= null && Trigger_Settings__c.getInstance('EP_RO_Import_Staging_trigger').IsActive__c){
    if(trigger.isBefore){
        if(EP_CheckRecursive.runBeforeOnce()){
            if(trigger.isUpdate){
                for(EP_RO_Import_Staging__c roStagingObj : trigger.newMap.values()){
                    EP_ROStagingDomainObject roDomainObj = new EP_ROStagingDomainObject(roStagingObj); 
                    roDomainObj.doActionBeforeUpdate();
                }
            }
        }
    }
    if(trigger.isAfter){        
        if(EP_CheckRecursive.runAfterOnce()){
            if(trigger.isUpdate){
                for(EP_RO_Import_Staging__c roStagingObj : trigger.newMap.values()){
                    EP_ROStagingDomainObject roDomainObj = new EP_ROStagingDomainObject(roStagingObj,trigger.oldMap.get(roStagingObj.Id));
                    roDomainObj.doActionAfterUpdate();
                }
            }
        }            
    }
}
}