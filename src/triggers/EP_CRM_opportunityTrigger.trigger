trigger EP_CRM_opportunityTrigger on Opportunity (before insert,before update,after insert,after update) {       
        
    if(trigger.isAfter && trigger.isInsert && !EP_CRM_opportunityTriggerHandler.isAfterInsertRecursiveTrigger){
        EP_CRM_opportunityTriggerHandler.afterInsertConnectionOpportunity(trigger.new);
        EP_CRM_opportunityTriggerHandler.isAfterInsertRecursiveTrigger= true;        
    }
    
    if(trigger.isBefore && trigger.isInsert && !EP_CRM_opportunityTriggerHandler.isBeforeInsertRecursiveTrigger ){
        EP_CRM_opportunityTriggerHandler.beforeInsertOpportunity(trigger.new);  
        EP_CRM_opportunityTriggerHandler.isBeforeInsertRecursiveTrigger = true;  
    }        
    
}