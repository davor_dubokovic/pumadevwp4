trigger EP_CRM_PaymentTermTrigger on EP_Payment_Term__c (after insert) {
     
    if(trigger.isAfter && trigger.isInsert && !EP_CRM_PaymentTermTriggerHandler.isInsertRecursiveTrigger){
        EP_CRM_PaymentTermTriggerHandler.afterInsertConnectionPaymentTerm(trigger.new);
        EP_CRM_PaymentTermTriggerHandler.isInsertRecursiveTrigger= true;
    }

}