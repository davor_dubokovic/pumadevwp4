trigger EP_CRM_PaymentMethodTrigger on EP_Payment_Method__c (after insert) {
     
    if(trigger.isAfter && trigger.isInsert && !EP_CRM_PaymentMethodTriggerHandler.isInsertRecursiveTrigger){
        EP_CRM_PaymentMethodTriggerHandler.afterInsertConnectionPaymentMethod(trigger.new);
        EP_CRM_PaymentMethodTriggerHandler.isInsertRecursiveTrigger= true;
    }

}